#!/bin/bash

shopt -s globstar
for D in ./**/*
do
    if [ -d "${D}" ]; then
        astyle --style=allman --indent=spaces=4 ${D}/*pp
        astyle --style=allman --indent=spaces=4 ${D}/*h
        astyle --style=allman --indent=spaces=4 ${D}/*xx
        astyle --style=allman --indent=spaces=4 ${D}/*.vert
        astyle --style=allman --indent=spaces=4 ${D}/*.frag
        astyle --style=allman --indent=spaces=4 ${D}/*.geom
        rm ${D}/*.orig
    fi
done

