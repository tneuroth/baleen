

#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include "Types/Vec.hpp"

#include <vector>

namespace TN
{

class Texture2D1
{
    unsigned int m_id;

public:

    unsigned int load( std::vector< float > & data, int width, int height );

    unsigned int id()
    {
        return m_id;
    }

    void bind();

    ~Texture2D1();
};

class Texture2D3
{
    unsigned int m_id;

public:

    unsigned int load( std::vector< float > & data, int width, int height );

    unsigned int id()
    {
        return m_id;
    }

    void bind();

    ~Texture2D3();
};


class Texture1D
{
    unsigned int m_id;

public:

    unsigned int load( std::vector< Vec3< float > > & data, int size );

    unsigned int id()
    {
        return m_id;
    }

    void bind();

    ~Texture1D();
};

}


#endif // TEXTURE2D_H

