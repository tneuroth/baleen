#ifndef PHYSICALVIEW_HPP
#define PHYSICALVIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "Plot2D.hpp"

namespace TN
{

class PhysicalView : public Widget
{

public:

    Plot2D plt;

    PhysicalView() : Widget()
    {
        plt.bkgColor( Vec4( 0, .6, .4, 1 ) );
    }

    virtual void setSize( float x, float y )
    {
        Widget::setSize( x, y );
        plt.setSize( x, y );
    }
    virtual void setPosition( float x, float y )
    {
        Widget::setPosition( x, y );
        plt.setPosition( x, y );
    }
};

}

#endif // PHYSICALVIEW_HPP
