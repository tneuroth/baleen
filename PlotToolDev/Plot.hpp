#ifndef PLOT_HPP
#define PLOT_HPP

#include "OpenGL/Widgets/Widget.hpp"

#include <string>

namespace TN
{

// plots will not hold data nor point to data, only the variable identifiers
// or keys in data map...
// rendering is to be done externally

class Plot : public Widget
{

public:

    enum PlotScale
    {
        LOG_10_SCALE,
        LOG_E_SCALE,
        LINEAR_SCALE,
    };

    enum PlotType
    {
        PLOT2D,
        PLOT3D
    };

protected:

    bool m_invalidated;
    PlotType m_plotType;
    std::string m_title;
    std::int32_t m_bitField;

public:

    const std::string & title()
    {
        return m_title;
    }
    void title( const std::string & t )
    {
        m_title = t;
    }
    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }
    PlotType plotType() const
    {
        return m_plotType;
    }
    PlotType plotType( PlotType t )
    {
        m_plotType = t;
    }

    void invalid( bool c )
    {
        m_invalidated = c;
    }
    bool invalid() const
    {
        return m_invalidated;
    }

    std::int32_t bitField() const
    {
        return m_bitField;
    }
};

}

#endif // PLOT_HPP

