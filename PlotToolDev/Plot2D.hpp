#ifndef PLOT2D_HPP
#define PLOT2D_HPP

#include "Plot.hpp"

#include "Types/Vec.hpp"
#include "Texture.hpp"

#include <limits>

namespace TN
{

class Plot2D : public Plot
{
    int m_index;

    std::vector< Vec2< float > > m_background;
    std::vector< Vec2< float > > m_axis;
    std::vector< Vec2< float > > m_tics;
    std::vector< Vec2< float > > m_grid;

    Vec3< float > m_axisColor;
    Vec3< float > m_ticColor;
    Vec3< float > m_gridColor;
    Vec3< float > m_backgroundColor;
    Vec3< float > m_plotBackgroundColor;

    Vec2< float > m_xRange;
    Vec2< float > m_yRange;

    Vec2< float > m_xRangeFull;
    Vec2< float > m_yRangeFull;

    std::vector< float > m_xTicValues;
    std::vector< float > m_yTicValues;

    Vec2< float > m_minTicDist; // pixels, override users preference if tics would be too close
    Vec2< float > m_ticSpacing; //used when user doesn't give predefined tic value

    int m_gridRows;
    int m_gridCols;

    std::string m_xLabel;
    std::string m_yLabel;

    std::string m_title;
    PlotScale  m_xScale;
    PlotScale  m_yScale;
    float m_topMargin;         // pixels
    float m_bottomMargin;
    float m_leftMargin;
    float m_rightMargin;

    bool m_maintainAspectRatio;

    Vec2< float > m_selectedValue;
    bool m_valueIsSelected;

    Texture2D1 m_plotTexture;
    Texture1D m_transferFunction;

public:

    void make()
    {

        // background
        m_background.resize( 6 );
        m_background[0] = Vec2< float > ( -1, -1 );
        m_background[1] = Vec2< float > ( -1,  1 );
        m_background[2] = Vec2< float > (  1, -1 );
        m_background[3] = m_background[1];
        m_background[4] = Vec2< float > ( 1, 1 );
        m_background[5] = m_background[2];
        m_backgroundColor = Vec3< float >( .8, .8, .8 );

        // axis
        m_axis.resize( 4 );
        m_axis[0] = Vec2< float >( -1,  1 );
        m_axis[1] = Vec2< float >( -1, -1 );
        m_axis[2] = Vec2< float >( -1, -1 );
        m_axis[3] = Vec2< float >(  1, -1 );

        // tiks

        if( plotSize().x() <= 0 || plotSize().x() > 9999 || plotSize().y() <= 0 || plotSize().y() > 9999 )
        {
            return;
        }

        //grid
        int NC = plotSize().x() / 80;
        int NR = plotSize().y() / 80;

        m_gridRows = NR;
        m_gridCols = NC;

        NR *= 2.0;
        NC *= 2.0;

        m_grid.resize( ( NR + NC )*2 );

        float dc = 2.f / NC;
        for( int c = 0; c < NC; ++c )
        {
            m_grid[ c * 2 +  0 ] = Vec2< float >( -1 + (c+1)*dc, -1 );
            m_grid[ c * 2 +  1 ] = Vec2< float >( -1 + (c+1)*dc,  1 );
        }
        float dr = 2.f / NR;
        for( int r = NC; r < NR+NC; ++r )
        {
            m_grid[ r * 2 +  0 ] = Vec2< float >( -1, -1 + (r-NC+1)*dr );
            m_grid[ r * 2 +  1 ] = Vec2< float >(  1, -1 + (r-NC+1)*dr );
        }
    }

    void initialize(
        const Vec2< float > & xRange,
        const Vec2< float > & yRange,
        const std::string   & xLabel,
        const std::string   & yLabel,
        const std::string   & title,
        bool maintainAspectRatio = false,
        PlotScale xScale = PlotScale::LINEAR_SCALE,
        PlotScale yScale = PlotScale::LINEAR_SCALE )
    {
        m_valueIsSelected = false;
        m_xRange = xRange;
        m_yRange = yRange;
        m_xRangeFull = xRange;
        m_yRangeFull = yRange;
        m_xLabel = xLabel;
        m_yLabel = yLabel;
        m_title  =  title;
        m_xScale = xScale;
        m_yScale = yScale;
        m_ticSpacing = Vec2< float >( .25f, .25f );
        m_topMargin    = 60.f;
        m_bottomMargin = 60.f;
        m_rightMargin  = 24.f;
        m_leftMargin   = 100.f;
        m_maintainAspectRatio = maintainAspectRatio;
        this->m_plotType = PlotType::PLOT2D;

        make();
    }

    Plot2D(
        int index,
        const Vec2< float > & xRange,
        const Vec2< float > & yRange,
        const std::string & xLabel,
        const std::string & yLabel,
        const std::string & title,
        bool maintainAspectRatio = false )
    {
        this->plotType( PlotType::PLOT2D );

        m_invalidated = true;
        m_index = index;

        initialize(
            xRange,
            yRange,
            xLabel,
            yLabel,
            title,
            maintainAspectRatio,
            PlotScale::LINEAR_SCALE,
            PlotScale::LINEAR_SCALE
        );
    }
    Plot2D()
    {
        m_invalidated = true;
        this->plotType( PlotType::PLOT2D );
    }

    void translate( double x, double y )
    {
        double dx = ( m_xRange.b() - m_xRange.a() ) * ( x / double( size().x() ) );
        double dy = ( m_yRange.b() - m_yRange.a() ) * ( y / double( size().y() ) );

        m_xRange.a( m_xRange.a() - dx );
        m_xRange.b( m_xRange.b() - dx );

        m_yRange.a( m_yRange.a() + dy );
        m_yRange.b( m_yRange.b() + dy );
    }

    // 0 = x, 1 = y, 2 = x,y
    void zoom( double numDegrees, int whichAxis )
    {

        double dx = ( m_xRange.b() - m_xRange.a() ) / 50.0;
        double dy = ( m_yRange.b() - m_yRange.a() ) / 50.0;
        if( numDegrees <= 0 )
        {
//            if( m_xRange.b() - dx > m_xRange.a() + dx ) {

            m_xRange.a( m_xRange.a() + dx );
            m_xRange.b( m_xRange.b() - dx );
//            }
//            if( m_yRange.b() - dy > m_yRange.a() + dy ) {

            m_yRange.a( m_yRange.a() + dy );
            m_yRange.b( m_yRange.b() - dy );
//            }
        }
        else
        {
            m_xRange.a( m_xRange.a() - dx );
            m_xRange.b( m_xRange.b() + dx );

            m_yRange.a( m_yRange.a() - dy );
            m_yRange.b( m_yRange.b() + dy );

//            m_xRange.a( std::max( m_xRangeFull.a(), m_xRange.a() ) );
//            m_yRange.a( std::max( m_yRangeFull.a(), m_yRange.a() ) );

//            m_xRange.b( std::min( m_xRangeFull.b(), m_xRange.b() ) );
//            m_yRange.b( std::min( m_yRangeFull.b(), m_yRange.b() ) );
        }
    }

    void xRange( Vec2< float > & rngX )
    {
        m_xRangeFull = rngX;
    }

    Vec2< float > zoomScale()
    {
        return Vec2< float >(
                   ( m_xRangeFull.b() - m_xRangeFull.a() ) / double( m_xRange.b() - m_xRange.a() ),
                   ( m_yRangeFull.b() - m_yRangeFull.a() ) / double( m_yRange.b() - m_yRange.a() )
               );
    }

    Vec2< float > translation()
    {
        return Vec2< float >(
                   ( m_xRangeFull.b() - double( m_xRange.b() ) ) / double( m_xRangeFull.b() - m_xRangeFull.a() ),
                   ( m_yRangeFull.b() - double( m_yRange.b() ) ) / double( m_yRangeFull.b() - m_yRangeFull.a() )
               );
    }

    void setDefaultRange()
    {
        m_xRange = m_xRangeFull;
        m_yRange = m_yRangeFull;
    }

    void selectValue( Vec2< float > _v )
    {
        m_selectedValue = _v;
        m_valueIsSelected = true;
    }

    void deselectValue()
    {
        m_valueIsSelected = false;
    }

    const std::string & xLabel() const
    {
        return m_xLabel;
    }
    const std::string & yLabel() const
    {
        return m_yLabel;
    }

    const Vec2< float > & xRange() const
    {
        return m_xRange;
    }
    const Vec2< float > & yRange() const
    {
        return m_yRange;
    }

    const Vec2< float > & xRangeFull() const
    {
        return m_xRangeFull;
    }
    const Vec2< float > & yRangeFull() const
    {
        return m_yRangeFull;
    }

    bool maintainAspectRatio() const
    {
        return m_maintainAspectRatio;
    }
    float aspectRatio()        const
    {
        return plotSize().x() / plotSize().y();
    }

    Vec2< float > plotPosition() const
    {
        return position() + Vec2< float >( m_leftMargin, m_topMargin );
    }
    Vec2< float > plotSize() const
    {
        return size() - Vec2< float >( m_leftMargin + m_rightMargin, m_topMargin + m_bottomMargin );
    }
    float plotTop()    const
    {
        return    m_topMargin;
    }
    float plotBottom() const
    {
        return m_bottomMargin;
    }
    float plotLeft()   const
    {
        return   m_leftMargin;
    }
    float plotRight()  const
    {
        return  m_rightMargin;
    }

    const std::vector< Vec2< float > > & axis() const
    {
        return m_axis;
    }
    const std::vector< Vec2< float > > & background() const
    {
        return m_background;
    }
    const Vec3< float > & backgroundColor() const
    {
        return m_backgroundColor;
    }
    const Vec3< float > & plotBackgroundColor() const
    {
        return m_plotBackgroundColor;
    }
    const std::vector< Vec2< float > > & grid() const
    {
        return m_grid;
    }
    const std::string & title() const
    {
        return m_title;
    }

    int numGridRows() const
    {
        return m_gridRows;
    }
    int numGridCols() const
    {
        return m_gridCols;
    }
    bool valueIsSelected() const
    {
        return m_valueIsSelected;
    }
    Vec2< float > selectedValue() const
    {
        return m_selectedValue;
    }

    I2 resolution()
    {
        int pltX1 = position().x() + plotLeft();
        int pltX2 = position().x() + size().x() - plotRight();
        int pltY1 = position().y() + plotBottom();
        int pltY2 = position().y() + size().y() - plotTop();

        return I2( std::abs( pltX2 - pltX1 ), std::abs( pltY2 - pltY1 ) );
    }

    const Texture1D  & transferFunction() const
    {
        return m_transferFunction;
    }
    const Texture2D1 & plotTexture() const
    {
        return m_plotTexture;
    }

    int index() const
    {
        return m_index;
    }
};

}

#endif // PLOT2D_HPP

