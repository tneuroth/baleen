#-------------------------------------------------
#
# Project created by QtCreator 2017-02-22T12:32:24
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PlotTool
TEMPLATE = app

INCLUDEPATH += ../Code/Shared/VIDI/Tyson/
INCLUDEPATH += ../Code/Shared/ThirdParty/
INCLUDEPATH += /usr/include/freetype2/
INCLUDEPATH += /usr/local/include/
INCLUDEPATH += /usr/local/cuda/include

DEFINES += "VIDI_GL_MAJOR_VERSION=4"

LIBS += -lfreetype
LIBS += -fopenmp
LIBS += -L../Code/Shared/VIDI/Tyson/Cuda -lheatmap

QMAKE_CXXFLAGS += -std=c++11 -frounding-math -fopenmp

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O0
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O3

# add the desired -O3 if not present
#QMAKE_CXXFLAGS_RELEASE += -O1
QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_CXXFLAGS_RELEASE += -ftree-vectorizer-verbose=5

SOURCES +=\
    Main.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressonwrapper.cpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/particledatamanager.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.cpp \
    MainWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.cpp \
    PlotTool.cpp

HEADERS  += \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.hpp \
    PlotTool.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/TimeSeriesDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/ExpressionWrapper.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressionsymbols.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/Expression.hpp \
    ../Code/Shared/ThirdParty/mpreal.h \
    ../Code/Shared/ThirdParty/exprtk.hpp \
    ../Code/Shared/ThirdParty/exprtk_mpfr_adaptor.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/Contour.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/ParticleFilter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/Attributes.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/LinkedViewDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/ParticleDataManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DataSetManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/ParticleDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/MetaData.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCElectron.hpp \
    ../Code/Shared/VIDI/Tyson/Types/Vec.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderParams.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot.hpp \
    MainWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Widget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ComboWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/PressButtonWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/ViewPort.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/BoxShadowFrame.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/DynamicGridLayout.hpp \
    PhysicalView.hpp \
    TemporalView.hpp \
    StatisticalView.hpp \
    FluxView.hpp \
    FluxModel.hpp \
    Plot2D.hpp \
    Plot.hpp \
    Texture.hpp

FORMS    += MainWindow.ui

DISTFILES += \
    ../Platform/GL4/shaders/pathtube.geom \
    ../Platform/GL4/shaders/vz.frag \
    ../Platform/GL4/shaders/timeStack.frag \
    ../Platform/GL4/shaders/text.frag \
    ../Platform/GL4/shaders/simpleColor.frag \
    ../Platform/GL4/shaders/quadTex.frag \
    ../Platform/GL4/shaders/points3D.frag \
    ../Platform/GL4/shaders/pathtube.frag \
    ../Platform/GL4/shaders/particleBump.frag \
    ../Platform/GL4/shaders/mesh.frag \
    ../Platform/GL4/shaders/marks.frag \
    ../Platform/GL4/shaders/heatMapRender.frag \
    ../Platform/GL4/shaders/heatMapGen.frag \
    ../Platform/GL4/shaders/flatMesh.frag \
    ../Platform/GL4/shaders/flat3D.frag \
    ../Platform/GL4/shaders/flat2DSerial.frag \
    ../Platform/GL4/shaders/flat2D.frag \
    ../Platform/GL4/shaders/densityMap.frag \
    ../Platform/GL4/shaders/colorFlag2D.frag \
    ../Platform/GL4/shaders/clusterLayer.frag \
    ../Platform/GL4/shaders/binLine.frag \
    ../Platform/GL4/shaders/binDiagnostic.frag \
    ../Platform/GL4/shaders/bin.frag \
    ../Platform/GL4/shaders/vz.vert \
    ../Platform/GL4/shaders/timeStack.vert \
    ../Platform/GL4/shaders/text.vert \
    ../Platform/GL4/shaders/simpleColor.vert \
    ../Platform/GL4/shaders/simple.vert \
    ../Platform/GL4/shaders/quadTex.vert \
    ../Platform/GL4/shaders/points3D.vert \
    ../Platform/GL4/shaders/pathtube.vert \
    ../Platform/GL4/shaders/particleBump.vert \
    ../Platform/GL4/shaders/mesh.vert \
    ../Platform/GL4/shaders/marks.vert \
    ../Platform/GL4/shaders/heatMapRender.vert \
    ../Platform/GL4/shaders/heatMapGen.vert \
    ../Platform/GL4/shaders/heatMap.vert \
    ../Platform/GL4/shaders/flatMesh.vert \
    ../Platform/GL4/shaders/flat3D.vert \
    ../Platform/GL4/shaders/flat2DSerial.vert \
    ../Platform/GL4/shaders/flat2D3.vert \
    ../Platform/GL4/shaders/flat2D.vert \
    ../Platform/GL4/shaders/densityMap.vert \
    ../Platform/GL4/shaders/colorFlag2D.vert \
    ../Platform/GL4/shaders/binLine.vert \
    ../Platform/GL4/shaders/binDiagnostic.vert \
    ../Platform/GL4/shaders/bin.vert \
    ../Platform/GL4/shaders/HeatMapSigned.frag \
    ../Platform/GL4/shaders/HeatMapSigned.vert
