#ifndef FLUXMODEL_HPP
#define FLUXMODEL_HPP

#include "Data/Managers/ParticleDataManager.hpp"

#include <vector>
#include <array>
#include <cmath>

namespace TN
{

struct FluxCalculationParams
{
    float angularResolutionFactor;
    Vec2< float > angularRangeA;
    Vec2< float > angularRangeB;

    float psinResolutionFactor;
    Vec2< float > psinRange;

    float interpolationFactor;
    bool isSigned;

    double psinEpsilon;
    int lookAhead;

    FluxCalculationParams() {}

    FluxCalculationParams( bool withSigned )
    {
        angularRangeA = Vec2< float >( 0.f, 3.14f *2.3 );
        angularRangeB = Vec2< float >( 0.f, 3.14f );
        psinRange = Vec2< float >( 0.0001f, 1.05 );

        angularResolutionFactor = 200.f;
        psinResolutionFactor = 500;

        isSigned = withSigned;

        float interpolationFactor = 1.f;

        psinEpsilon = 0.0;
        lookAhead = 0;
    }

    bool operator == ( FluxCalculationParams & other )
    {
        return ( ( angularResolutionFactor == other.angularResolutionFactor ) &&
                 ( angularRangeA == other.angularRangeA ) &&
                 ( angularRangeB == other.angularRangeB ) &&
                 ( psinResolutionFactor == other.psinResolutionFactor ) &&
                 ( psinRange == other.psinRange ) &&
                 ( interpolationFactor == other.interpolationFactor ) &&
                 ( isSigned == other.isSigned ) &&
                 ( psinEpsilon == other.psinEpsilon ) );
    }
};

struct FluxModel
{

    FluxModel() : calculationParams( true )
    {
    }

    FluxCalculationParams calculationParams;

    // the psin values where the flux is evaluated
    std::vector< float > psinFluxLineValues;

    //flux by ts, psin
    //[ time step ][ psin bin ]
    std::vector< std::vector< float > > particleFlux;
    std::vector< std::vector< float > > energyFlux;
    std::vector< std::vector< float > > momentumFlux;

    // aggregations over time by psin
    //[ psin bin ]
    std::vector< float > partFluxMin;
    std::vector< float > energyFluxMin;
    std::vector< float > momentumFluxMin;

    std::vector< float > partFluxMax;
    std::vector< float > energyFluxMax;
    std::vector< float > momentumFluxMax;

    std::vector< float > partFluxMean;
    std::vector< float > energyFluxMean;
    std::vector< float > momentumFluxMean;

    // full flux range
    Vec2< float > particleFluxRange;
    Vec2< float > energyFluxRange;
    Vec2< float > momentumFluxRange;

    // adding for each psin over t
    std::vector< float > netParticleFlux;
    Vec2< float > netParticleFluxRange;


    const int N_DISP_BINS = 256;

    std::vector< std::vector< Vec2< float > > > switchStats;
    std::vector< int > particleDirectionStates;
    std::vector< int > stepsSinceLastSwitch;
    std::vector< float > psinOfLastSwitch;

    std::vector< int > numTimesPartInRange;
    std::vector< std::vector< float > > rFiltered;
    std::vector< std::vector< float > > zFiltered;

    // In long run calculate over adjustable window like fourier transform
    // for now assume window is the whole time series
    std::vector< float > switchAveFrequencyDist;
    std::vector< float > switchDisplacementDist;
    std::vector< float > switchWavelengthDist;

    std::vector< float > switchAveFrequencyDistX;
    std::vector< float > switchDisplacementDistX;
    std::vector< float > switchWavelengthDistX;

    Vec2< float > switchAveFrequencyRange;
    Vec2< float > switchDisplacementRange;
    Vec2< float > switchWavelengthRange;

    Vec2< float > switchAveFrequencyDistRange;
    Vec2< float > switchDisplacementDistRange;
    Vec2< float > switchWavelengthDistRange;

    float numDirectionChanges;

    void resize( const ParticleDataManager< float > & particleData )
    {
        const int N_STEPS = particleData.numTimeSteps();

        netParticleFlux.assign( N_STEPS, 0 );

        switchAveFrequencyDist.assign( N_DISP_BINS, 0 );
        switchDisplacementDist.assign( N_DISP_BINS, 0 );
        switchWavelengthDist.assign( N_DISP_BINS, 0 );

        switchAveFrequencyDistX.assign( N_DISP_BINS, 0 );
        switchDisplacementDistX.assign( N_DISP_BINS, 0 );
        switchWavelengthDistX.assign( N_DISP_BINS, 0 );

        switchAveFrequencyRange     = Vec2< float >( 0, 0 );
        switchDisplacementRange     = Vec2< float >( 0, 0 );
        switchWavelengthRange       = Vec2< float >( 0, 0 );
        switchAveFrequencyDistRange = Vec2< float >( 0, 0 );
        switchDisplacementDistRange = Vec2< float >( 0, 0 );
        switchWavelengthDistRange   = Vec2< float >( 0, 0 );

        const int N_Psin_Bins = calculationParams.psinResolutionFactor;

        partFluxMin.assign( N_Psin_Bins, std::numeric_limits< float >::max() );
        partFluxMax.assign( N_Psin_Bins, -std::numeric_limits< float >::max() );
        partFluxMean.assign( N_Psin_Bins, 0 );

        energyFluxMin.assign( N_Psin_Bins,  std::numeric_limits< float >::max()  );
        energyFluxMax.assign( N_Psin_Bins, -std::numeric_limits< float >::max() );
        energyFluxMean.assign( N_Psin_Bins, 0 );

        momentumFluxMin.assign( N_Psin_Bins, std::numeric_limits< float >::max() );
        momentumFluxMax.assign( N_Psin_Bins, -std::numeric_limits< float >::max() );
        momentumFluxMean.assign( N_Psin_Bins, 0 );

        psinFluxLineValues.resize( N_Psin_Bins );

        for( int i = 0; i < N_Psin_Bins; ++i )
        {
            float r = float( i ) / ( N_Psin_Bins-1 );
            float psinI = ( 1-r )*calculationParams.psinRange.a() + r*calculationParams.psinRange.b();
            psinFluxLineValues[ i ] = psinI;
        }

        particleFlux.resize( N_STEPS );
        energyFlux.resize( N_STEPS );
        momentumFlux.resize( N_STEPS );

        for( int i = 0; i < N_STEPS; ++i )
        {
            particleFlux[ i ].assign( N_Psin_Bins, 0 );
            energyFlux[ i ].assign( N_Psin_Bins, 0 );
            momentumFlux[ i ].assign( N_Psin_Bins, 0 );
        }
    }

    void calculateFlux( const ParticleDataManager< float > & particleData )
    {
        const double simDT = 8.6632492620251972E-7;
        const double DT = 2*simDT;
        const double NSAMPLE = 500000;
        const double C = NSAMPLE / DT;
        const double ION_MASS = 2.0;

        resize( particleData );

        const std::vector< std::unique_ptr< std::vector< float > > > & psin  = particleData.values( "ions", "psin" );
        const std::vector< std::unique_ptr< std::vector< float > > > & angle = particleData.values( "ions", "poloidal_angle" );
        const std::vector< std::unique_ptr< std::vector< float > > > & KE    = particleData.values( "ions", "E" );
        const std::vector< std::unique_ptr< std::vector< float > > > & vpara = particleData.values( "ions", "vpara" );
        const std::vector< std::unique_ptr< std::vector< float > > > & vperp = particleData.values( "ions", "vperp" );
        const std::vector< std::unique_ptr< std::vector< float > > > & w0    = particleData.values( "ions", "w0" );
        const std::vector< std::unique_ptr< std::vector< float > > > & w1    = particleData.values( "ions", "w1" );
        const std::vector< std::unique_ptr< std::vector< float > > > & r    = particleData.values( "ions", "r" );
        const std::vector< std::unique_ptr< std::vector< float > > > & z    = particleData.values( "ions", "z" );

        const int N_STEPS = particleData.numTimeSteps();
        const int NP = ( *psin[ 0 ] ).size();

        switchStats.resize( NP );
        particleDirectionStates.assign( NP, 0 );
        stepsSinceLastSwitch.assign( NP, 0 );
        psinOfLastSwitch.assign( NP, 0 );
        numTimesPartInRange.assign( NP, 0 );

        numDirectionChanges = 0;

        rFiltered.resize( N_STEPS );
        zFiltered.resize( N_STEPS );

        // compute statistics
        for( int t = 0; t < N_STEPS-1; ++t )
        {
            const int NP = ( *psin[ t ] ).size();

            rFiltered[ t ].clear();
            zFiltered[ t ].clear();

            rFiltered[ t ].reserve( NP );
            zFiltered[ t ].reserve( NP );

            for( int i = 0; i < NP; ++i )
            {
                if( t == 0 )
                {
                    switchStats[ i ].clear();
                }

                const double psin1 = ( *psin[ t     ] )[ i ];
                const double psin2 = ( *psin[ t + 1 ] )[ i ];

                const double angle1 = ( *angle[ t     ] )[ i ];
                const double angle2 = ( *angle[ t + 1 ] )[ i ];

                if( psin1 < calculationParams.psinRange.a()
                        || psin1 > calculationParams.psinRange.b()
                        || psin2 < calculationParams.psinRange.a()
                        || psin2 > calculationParams.psinRange.b()
                        ||  ( ( angle1 < calculationParams.angularRangeA.a()
                                || angle1 > calculationParams.angularRangeA.b() )
                              &&
                              ( angle1 < calculationParams.angularRangeB.a()
                                || angle1 > calculationParams.angularRangeB.b() ) )
                        || std::abs( psin2 - psin1 ) > .3 )
                {
                    continue;
                }

                rFiltered[ t ].push_back( ( *r[ t ] )[ i ] );
                zFiltered[ t ].push_back( ( *z[ t ] )[ i ] );

                numTimesPartInRange[ i ]++;

                int direction = psin1 == psin2 ? 0 : ( psin1 < psin2 ? 1 : -1 );

                // initialize direction
                if( t == 0 )
                {
                    particleDirectionStates[ i ] = direction;
                }

                // if direction has changed
                if( particleDirectionStates[ i ] == -direction )
                {
                    // if this is the first switch (using 0 as a flag ) I don't count it
                    if( stepsSinceLastSwitch[ i ] != 0 )
                    {
                        switchStats[ i ].push_back(
                            Vec2< float >(
                                stepsSinceLastSwitch[ i ],
                                std::abs( psinOfLastSwitch[ i ] - psin1 )
                            )
                        );
                        ++numDirectionChanges;
                    }
                    particleDirectionStates[ i ] = direction;
                    psinOfLastSwitch[ i ] = psin1;
                    stepsSinceLastSwitch[ i ] = 1;
                }
                else if( stepsSinceLastSwitch[ i ] != 0 )
                {
                    ++stepsSinceLastSwitch[ i ];
                }
            }
        }

        // aggregate statistics

        // calculate value ranges

        float NP_COMPUTE = 0;
        for( int p = 0; p < NP; ++p )
        {
            if( numTimesPartInRange[ p ] > 0 )
            {
                ++NP_COMPUTE;

                const float N_SWITCH = switchStats[ p ].size();
                const float SWITCH_F = N_SWITCH / double( numTimesPartInRange[ p ] );

                switchAveFrequencyRange.a( std::min( switchAveFrequencyRange.a(), SWITCH_F ) );
                switchAveFrequencyRange.b( std::max( switchAveFrequencyRange.b(), SWITCH_F ) );

                for( int s = 0; s < N_SWITCH; ++s )
                {
                    switchDisplacementRange.a( std::min( switchDisplacementRange.a(), switchStats[ p ][ s ].y() ) );
                    switchDisplacementRange.b( std::max( switchDisplacementRange.b(), switchStats[ p ][ s ].y() ) );

                    switchWavelengthRange.a( std::min( switchWavelengthRange.a(), switchStats[ p ][ s ].x() ) );
                    switchWavelengthRange.b( std::max( switchWavelengthRange.b(), switchStats[ p ][ s ].x() ) );
                }
            }
        }

        // bin value

        float nCI = 1.f / numDirectionChanges;
        float nCP = 1.f / NP_COMPUTE;

        for( int p = 0; p < NP; ++p )
        {
            if( numTimesPartInRange[ p ] > 0 )
            {
                const float N_SWITCH = switchStats[ p ].size();
                const float SWITCH_F = N_SWITCH / double( numTimesPartInRange[ p ] );

                int sfBin = std::max( std::min( int( std::floor( ( ( SWITCH_F - switchAveFrequencyRange.a() )
                                                     / ( switchAveFrequencyRange.b() - switchAveFrequencyRange.a() ) )
                                                     * ( N_DISP_BINS ) ) ), N_DISP_BINS - 1 ), 0 );

                switchAveFrequencyDist[ sfBin ] += nCP;

                for( int s = 0; s < N_SWITCH; ++s )
                {
                    int wmBin = std::max( std::min( int( std::floor( ( ( switchStats[ p ][ s ].y() - switchDisplacementRange.a() )
                                                         / ( switchDisplacementRange.b() - switchDisplacementRange.a() ) )
                                                         * ( N_DISP_BINS ) ) ), N_DISP_BINS - 1 ), 0 );

                    switchDisplacementDist[ wmBin ] += nCI;

                    int wlBin = std::max( std::min( int( std::floor( ( ( switchStats[ p ][ s ].x() - switchWavelengthRange.a() )
                                                         / ( switchWavelengthRange.b() - switchWavelengthRange.a() ) )
                                                         * ( N_DISP_BINS ) ) ), N_DISP_BINS - 1 ), 0 );

                    switchWavelengthDist[ wlBin ] += nCI;
                }
            }
        }

        // calculate bin count ranges
        for( int b = 0; b < N_DISP_BINS; ++b )
        {
            float r = b / float( N_DISP_BINS - 1 );

            switchAveFrequencyDistX[ b ] = ( 1 - r ) * switchAveFrequencyRange.a() + r * switchAveFrequencyRange.b();
            switchDisplacementDistX[ b ] = ( 1 - r ) * switchDisplacementRange.a() + r * switchDisplacementRange.b();
            switchWavelengthDistX[ b ] = ( 1 - r ) * switchWavelengthRange.a() + r * switchWavelengthRange.b();

            switchAveFrequencyDistRange.a( std::min( switchAveFrequencyDistRange.a(), switchAveFrequencyDist[ b ] )  );
            switchAveFrequencyDistRange.b( std::max( switchAveFrequencyDistRange.b(), switchAveFrequencyDist[ b ] )  );

            switchDisplacementDistRange.a( std::min( switchDisplacementDistRange.a(), switchDisplacementDist[ b ] )  );
            switchDisplacementDistRange.b( std::max( switchDisplacementDistRange.b(), switchDisplacementDist[ b ] )  );

            switchWavelengthDistRange.a( std::min( switchWavelengthDistRange.a(), switchWavelengthDist[ b ] )  );
            switchWavelengthDistRange.b( std::max( switchWavelengthDistRange.b(), switchWavelengthDist[ b ] )  );
        }

        float sum1 = 0;
        float sum2 = 0;
        float sum3 = 0;
        for( int i = 0; i < N_DISP_BINS; ++i )
        {
            sum1 += switchAveFrequencyDist[ i ];
            sum2 += switchDisplacementDist[ i ];
            sum3 += switchWavelengthDist[ i ];
        }

        ////////////////////////

        const int N_PSIN_BINS = psinFluxLineValues.size();
        const double PSI_N_LENGTH = calculationParams.psinRange.b() - calculationParams.psinRange.a();

        const double TM = 1.0 / double( N_STEPS );

        particleFluxRange.a( std::numeric_limits< float >::max() );
        energyFluxRange.a( std::numeric_limits< float >::max() );
        momentumFluxRange.a( std::numeric_limits< float >::max() );

        particleFluxRange.b( -std::numeric_limits< float >::max() );
        energyFluxRange.b( -std::numeric_limits< float >::max() );
        momentumFluxRange.b( -std::numeric_limits< float >::max() );

        netParticleFluxRange.a( 0 );
        netParticleFluxRange.b( 0 );

        #pragma omp parallel for
        for( int t = 0; t < N_STEPS-1; ++t )
        {
            const int NP = ( *psin[ t ] ).size();
            for( int i = 0; i < NP; ++i )
            {
                const double r1 = ( *r[ t     ] )[ i ];
                const double r2 = ( *r[ t + 1 ] )[ i ];
                const double z1 = ( *z[ t     ] )[ i ];
                const double z2 = ( *z[ t + 1 ] )[ i ];

                const double psin1 = ( *psin[ t     ] )[ i ];
                const double psin2 = ( *psin[ t + 1 ] )[ i ];
                const double angle1 = ( *angle[ t ] )[ i ];
                const double angle2 = ( *angle[ t + 1 ] )[ i ];

                if( psin1 < calculationParams.psinRange.a()
                        || psin1 > calculationParams.psinRange.b()
                        || psin2 < calculationParams.psinRange.a()
                        || psin2 > calculationParams.psinRange.b()
                        ||  ( ( angle1 < calculationParams.angularRangeA.a()
                                || angle1 > calculationParams.angularRangeA.b() )
                              &&
                              ( angle1 < calculationParams.angularRangeB.a()
                                || angle1 > calculationParams.angularRangeB.b() ) ) )
                {
                    continue;
                }

                const double W = C *
                                 ( ( *w0[ t   ] )[ i ] * ( *w1[ t   ] )[ i ]
                                   + ( *w0[ t+1 ] )[ i ] * ( *w1[ t+1 ] )[ i ] ) / 2.0;

                const double e = W * ( ( *KE[ t   ] )[ i ] * ( *KE[ t   ] )[ i ]
                                       + ( *KE[ t+1 ] )[ i ] * ( *KE[ t+1 ] )[ i ] ) / 2.0;

                const double _vara = ( ( *vpara[ t   ] )[ i ] * ( *vpara[ t   ] )[ i ]
                                       + ( *vpara[ t+1 ] )[ i ] * ( *vpara[ t+1 ] )[ i ] ) / 2.0;

                const double _vperp = ( ( *vperp[ t   ] )[ i ] * ( *vperp[ t   ] )[ i ]
                                        + ( *vperp[ t+1 ] )[ i ] * ( *vperp[ t+1 ] )[ i ] ) / 2.0;

                const double mm = W * Vec2< double >( _vara, _vperp ).length() * ION_MASS;

                int psinBin1 =
                    std::min( int( std::floor( ( ( psin1 - calculationParams.psinRange.a() ) / ( PSI_N_LENGTH ) )
                                               * ( N_PSIN_BINS ) ) ), N_PSIN_BINS - 1 );

                int psinBin2 =
                    std::min( int( std::floor( ( ( psin2 - calculationParams.psinRange.a() ) / ( PSI_N_LENGTH ) )
                                               * ( N_PSIN_BINS ) ) ), N_PSIN_BINS - 1 );

                // not counting the flux
                if ( psinBin1 == psinBin2 )
                {
                    continue;
                }

                int innerBin = std::min( psinBin2, psinBin1 );
                int outerBin = std::max( psinBin2, psinBin1 );

                int direction = psinBin2 > psinBin1 ? 1 : -1;
                double l = ( Vec2<double>( r1, z1 ) - Vec2<double>( r2, z2 ) ).length();
                double pl = std::abs( psin1 - psin2 );

                if( pl < calculationParams.psinEpsilon )
                {
                    bool pass = false;
                    for( int lid = t+1; lid < ( t + calculationParams.lookAhead ) && lid < N_STEPS; ++lid )
                    {
                        float p_lk = ( *psin[ lid ] )[ i ];

                        int b2 =
                            std::min( int( std::floor( ( ( p_lk - calculationParams.psinRange.a() ) / ( PSI_N_LENGTH ) )
                                                       * ( N_PSIN_BINS ) ) ), N_PSIN_BINS - 1 );

                        if( direction > 0 )
                        {
                            if( b2 > psinBin1 )
                            {
                                if( p_lk - psinBin1 > calculationParams.psinEpsilon )
                                {
                                    pass = true;
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        else if( direction < 0 )
                        {
                            if( b2 < psinBin1 )
                            {
                                if( psinBin1 - p_lk >  calculationParams.psinEpsilon )
                                {
                                    pass = true;
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if( pass == false )
                    {
                        continue;
                    }
                }

                if( direction > 0 )
                {
                    for( int bin = innerBin; bin < outerBin; ++bin )
                    {
                        particleFlux[ t ][ bin ] += W;
                        energyFlux[ t ][ bin ] += e;
                        momentumFlux[ t ][ bin ] += mm;

                        partFluxMean[ bin ] += TM*W;
                        energyFluxMean[ bin ] += TM*e;
                        momentumFluxMean[ bin ] += TM*mm;
                    }
                }
                else
                {
                    for( int bin = outerBin; bin > innerBin; --bin )
                    {
                        particleFlux[ t ][ bin ] -= W;
                        energyFlux[ t ][ bin ] -= e;
                        momentumFlux[ t ][ bin ] -= mm;

                        partFluxMean[ bin ] -= TM*W;
                        energyFluxMean[ bin ] -= TM*e;
                        momentumFluxMean[ bin ] -= TM*mm;
                    }
                }
            }

            for( int i = 0; i < N_PSIN_BINS; ++i )
            {
                partFluxMin[ i ] = std::min( partFluxMin[ i ], particleFlux[ t ][ i ] );
                partFluxMax[ i ] = std::max( partFluxMax[ i ], particleFlux[ t ][ i ] );

                energyFluxMin[ i ] = std::min( energyFluxMin[ i ], energyFlux[ t ][ i ] );
                energyFluxMax[ i ] = std::max( energyFluxMax[ i ], energyFlux[ t ][ i ] );

                momentumFluxMin[ i ] = std::min( momentumFluxMin[ i ], momentumFlux[ t ][ i ] );
                momentumFluxMax[ i ] = std::max( momentumFluxMax[ i ], momentumFlux[ t ][ i ] );

                particleFluxRange.a( std::min( particleFluxRange.a(), particleFlux[ t ][ i ] ) );
                energyFluxRange.a( std::min( energyFluxRange.a(), energyFlux[ t ][ i ] ) );
                momentumFluxRange.a( std::min( momentumFluxRange.a(), momentumFlux[ t ][ i ] ) );

                particleFluxRange.b( std::max( particleFluxRange.b(), particleFlux[ t ][ i ] ) );
                energyFluxRange.b( std::max( energyFluxRange.b(), energyFlux[ t ][ i ] ) );
                momentumFluxRange.b( std::max( momentumFluxRange.b(), momentumFlux[ t ][ i ] ) );

                netParticleFlux[ t ] += particleFlux[ t ][ i ];
            }
            netParticleFluxRange.a( std::min( netParticleFluxRange.a(), netParticleFlux[ t ] ) );
            netParticleFluxRange.b( std::max( netParticleFluxRange.b(), netParticleFlux[ t ] ) );
        }
    }

    Vec2< float > fullParticleFluxRange() const
    {

    }

    Vec2< float > fullEnergyFluxRange() const
    {

    }

    Vec2< float > fullMomentumFluxRange() const
    {

    }
};

}

#endif // FLUXMODEL_HPP
