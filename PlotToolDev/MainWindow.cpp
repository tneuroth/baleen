

#include "PlotTool.hpp"
#include "MainWindow.hpp"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    plotToolWidget( this )
{
    ui->setupUi(this);
    delete ui->mainToolBar;

    QHBoxLayout * mainLayout = new QHBoxLayout;
    mainLayout->setSpacing( 0 );
    mainLayout->setMargin( 0 );
    mainLayout->addWidget( &plotToolWidget );
    QWidget *window = new QWidget();
    window->setLayout( mainLayout );
    setCentralWidget( window );

    plotToolWidget.GetRenderWindow()->renderLater();
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
}

MainWindow::~MainWindow()
{
    delete ui;
}
