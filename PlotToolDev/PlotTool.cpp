

#include "PlotTool.hpp"
#include "Data/Managers/DataSetManager.hpp"
#include "Data/Managers/DataSetManager.hpp"
#include "OpenGL/Rendering/Renderer.hpp"
#include "Data/Definitions/VisualContextModel.hpp"

#include <atomic>

#include <QWheelEvent>
#include <QMouseEvent>

namespace TN
{
PlotTool::PlotTool()
{
    qDebug() << "constructing";
    m_initializedGL = false;
    m_dataManager = new DataSetManager< float >;
    m_renderer = new Renderer;
    loadData();

    m_currentTStep = 20;

    m_timeSteps = std::vector< float >( m_dataManager->particleDataManager().numTimeSteps() );
    for( int i = 0; i < m_timeSteps.size(); ++i )
    {
        m_timeSteps[ i ] =  i;
    }

    m_tf.resize( TF_SIZE );

    for( int i = 0; i < TF_SIZE; ++i )
    {
        if( i <= TF_SIZE / 2.0 )
        {
            float r = ( 2 * i ) / float( TF_SIZE );
            m_tf[ i ] = Vec3< float >( 0, 0, 1 ) * r + Vec3< float >( 1, 1, 1 ) * ( 1 - r );
        }
        else
        {
            float r = ( 2 * ( i - TF_SIZE / 2.0 ) ) / float( TF_SIZE );
            m_tf[ i ] = Vec3< float >( 1, 1, 1 ) * r + Vec3< float >( 1, 0, 0 ) * ( 1 - r );
        }
    }

    m_heatMapTF.load( m_tf, m_tf.size() );
}

PlotTool::~PlotTool()
{
    delete m_renderer;
    delete m_dataManager;
}

void PlotTool::loadData()
{
    DataSetMetaData meta;
    meta.format = "XGC_Particles";
    meta.filePath = "../datasets/XGC/ITER";
    m_dataManager->setDataSet( meta );

    m_resolutionSlider.bkgColor( Vec4( .85, .85, .85, 1.0 ) );

    TimeSeriesDefinition timeSeries;
    timeSeries.firstIdx = 0;
    timeSeries.lastIdx = m_dataManager->particleDataManager().numTimeSteps() - 1;
    timeSeries.idxStride = 1;
    timeSeries.integrate = false;
    timeSeries.name = "all";

    std::atomic< int > filterProgress( 0 );
    std::atomic< int > derivationPrepareProgress( 0 );
    std::atomic< int > derivationCalculateProgress( 0 );
    std::atomic< int > baseProgress( 0 );
    std::atomic< int > rangeProgress( 0 );
    std::atomic< bool > status( false );

    DerivedVariable dv;
    dv.name( "w0w1" );
    dv.expression( "w0*w1*vperp" );
    const std::map< std::string, DerivedVariable > derived =  { { "w0w1", dv } };
    m_dataManager->particleDataManager().updateDerivations( "ions", derived );

    m_dataManager->particleDataManager().load(
        "ions",
    { "vpara", "vperp", "psin", "w0", "w1", "r", "z", "poloidal_angle", "E", "zeta", "w0w1" },
    timeSeries,
    true,
    true,
    filterProgress,
    derivationPrepareProgress,
    derivationCalculateProgress,
    baseProgress,
    rangeProgress,
    status );

    const int N_STEPS = m_dataManager->particleDataManager().numTimeSteps();

    m_flags.resize( N_STEPS );
    for( int i = 0; i < N_STEPS; ++i )
    {
        const std::vector< float > & z = *( m_dataManager->particleDataManager().values( "ions", "z" )[ i ] );
        const std::vector< float > & psin = *( m_dataManager->particleDataManager().values( "ions", "psin" )[ i ] );
        const int N_PART = z.size();

        m_flags[ i ].assign( N_PART, 1 );
        #pragma omp parallel for
        for( int j = 0; j < N_PART; ++j )
        {
            if( psin[ j ] > 1.0 )
            {
                m_flags[ i ][ j ] = 0;
                continue;
            }
            if( i > 0 )
            {
                if( m_flags[ i-1 ][ j ] == 0  )
                {
                    m_flags[ i ][ j ] = 0;
                    continue;
                }
            }
            if( z[ j ] <= -3.44289393 )
            {
                m_flags[ i ][ j ] = 0;
            }
            if( psin[ j ] <= 0.0002 )
            {
                m_flags[ i ][ j ] = 0;
            }
        }
    }

    m_heatMapper.resizeBuffer( m_dataManager->particleDataManager().numParticles( "ions" ) );

    qDebug() << "data loaded";
}

void PlotTool::compileShaders()
{
    const std::string SHADER_PATH =
        std::string( "../Platform/GL4/shaders" );

    timeStackProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/timeStack.vert" ).c_str() );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/timeStack.frag" ).c_str() );

    if(!timeStackProg->link())
    {
        //qDebug() << "timeStack Program linkage failed";
    }

    ///////////////////////////////////////////////////////////

    cartesianProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D.vert" ).c_str() );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg->link())
    {
        //qDebug() << "flat2D Program linkage failed";
    }

    cartesianProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D3.vert" ).c_str() );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg3->link())
    {
        //qDebug() << "flat2D3 Program linkage failed";
    }

    colorMapProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/vz.vert" ).c_str() );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/vz.frag" ).c_str() );

    if(!colorMapProg3->link())
    {
        //qDebug() << "vz Program linkage failed";
    }

    points3DProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/points3D.vert" ).c_str() );


    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/points3D.frag" ).c_str() );

    if(!points3DProg->link())
    {
        //qDebug() << "points3D Program linkage failed";
    }


    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        tubePathProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Vertex,
            ( SHADER_PATH + "/pathtube.vert" ).c_str() );


        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Fragment,
            ( SHADER_PATH + "/pathtube.frag" ).c_str() );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Geometry,
            ( SHADER_PATH + "/pathtube.geom" ).c_str() );

        if(!tubePathProgram->link())
        {
            //qDebug() << "pathtube Program linkage failed";
        }
    }

    //

    simpleProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simple.vert" ).c_str() );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!simpleProg->link())
    {
        //qDebug() << "simple Program linkage failed";
    }

    //

    simpleColorProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simpleColor.vert" ).c_str() );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/simpleColor.frag" ).c_str() );

    if(!simpleColorProg->link())
    {
        //qDebug() << "simpleColor Program linkage failed";
    }

    //

    prog3D = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/mesh.vert" ).c_str() );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/mesh.frag" ).c_str() );

    if(!simpleProg->link())
    {
        //qDebug() << "mesh Program linkage failed";
    }

    //

    flatProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat3D.vert" ).c_str() );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat3D.frag" ).c_str() );

    if(!flatProg->link())
    {
        //qDebug() << "flat3D Program linkage failed";
    }

    //

    flatProgSerial = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2DSerial.vert" ).c_str() );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2DSerial.frag" ).c_str() );

    if(!flatProgSerial->link())
    {
        //qDebug() << "flat2DSerial Program linkage failed";
    }

    //

    textProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/text.vert" ).c_str() );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/text.frag" ).c_str() );

    if(!textProg->link())
    {
        //qDebug() << "text Program linkage failed";
    }

    //

    ucProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/quadTex.frag" ).c_str() );

    if(!ucProg->link())
    {
        //qDebug() << "quadTex Program linkage failed";
    }


    //

    flatMeshProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flatMesh.vert" ).c_str() );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flatMesh.frag" ).c_str() );

    if(!flatMeshProg->link())
    {
        //qDebug() << "flatMesh Program linkage failed";
    }

    //

    heatMapGenProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapGen.vert" ).c_str() );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapGen.frag" ).c_str() );

    if(!heatMapGenProg->link())
    {
        //qDebug() << "heatmapgen Program linkage failed";
    }

    //

    heatMapRenderProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapRender.vert" ).c_str() );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapRender.frag" ).c_str() );

    if(!heatMapRenderProg->link())
    {
        //qDebug() << "flatMesh Program linkage failed";
    }

    //qDebug() << "returning";

    binDiagnosticProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/binDiagnostic.vert" ).c_str() );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/binDiagnostic.frag" ).c_str() );

    if(!binDiagnosticProgram->link())
    {
        //qDebug() << "bin diagnostic Program linkage failed";
    }

    ///

    m_heatMapProgSigned = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    m_heatMapProgSigned->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/HeatMapSigned.vert" ).c_str() );

    m_heatMapProgSigned->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/HeatMapSigned.frag" ).c_str() );

    if(!m_heatMapProgSigned->link())
    {
        qDebug() << "heatmap Program linkage failed";
    }
}

void PlotTool::initGL()
{
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor( 1, 1, 1, 1.0 );
    glEnable( GL_BLEND );

    qDebug() << "compiling shaders";
    compileShaders();

    qDebug() << "initializing renderer";
    m_renderer->init();

    qDebug() << "computing pixel density map";
    m_renderer->computePixelDensityByAngle( binDiagnosticProgram, m_pixelDensityMap );


}

void PlotTool::renderFluxView()
{
    m_fluxView.plt1.make();
    m_fluxView.plt2.make();
    m_fluxView.plt3.make();

    renderPlotBase2D( m_fluxView.plt1 );
    renderPlotBase2D( m_fluxView.plt2 );
    renderPlotBase2D( m_fluxView.plt3 );

    QVector4D extentColor( 0.5, 0.5, 0.5, 1.0 );
    QVector4D meanColor( 0.8, 0.8, 0.3, 1 );
    QVector4D fluxColor( 0.0, 0.6, 0.4, 1 );

    glLineWidth( 2 );
    glPointSize( 2 );
    renderPlotLine2D( m_fluxView.plt1, m_fluxModel.psinFluxLineValues, m_fluxModel.partFluxMax, extentColor );
    //renderPlotLine2D( m_fluxView.plt2, m_fluxModel.psinFluxLineValues, m_fluxModel.energyFluxMax, extentColor );
    renderPlotLine2D( m_fluxView.plt3, m_fluxModel.psinFluxLineValues, m_fluxModel.momentumFluxMax, extentColor );

    renderPlotLine2D( m_fluxView.plt1, m_fluxModel.psinFluxLineValues, m_fluxModel.partFluxMin, extentColor );
    //renderPlotLine2D( m_fluxView.plt2, m_fluxModel.psinFluxLineValues, m_fluxModel.energyFluxMin, extentColor );
    renderPlotLine2D( m_fluxView.plt3, m_fluxModel.psinFluxLineValues, m_fluxModel.momentumFluxMin, extentColor );

    glLineWidth( 2 );
    glPointSize( 2 );
    renderPlotLine2D( m_fluxView.plt1, m_fluxModel.psinFluxLineValues, m_fluxModel.partFluxMean, meanColor );
    //renderPlotLine2D( m_fluxView.plt2, m_fluxModel.psinFluxLineValues, m_fluxModel.energyFluxMean, meanColor );
    renderPlotLine2D( m_fluxView.plt3, m_fluxModel.psinFluxLineValues, m_fluxModel.momentumFluxMean, meanColor );


    glLineWidth( 2 );
    glPointSize( 2 );
    renderPlotLine2D( m_fluxView.plt1, m_fluxModel.psinFluxLineValues, m_fluxModel.particleFlux[ m_currentTStep ], fluxColor );
    //renderPlotLine2D( m_fluxView.plt2, m_fluxModel.psinFluxLineValues, m_fluxModel.energyFlux[ m_currentTStep ], fluxColor );
    renderPlotLine2D( m_fluxView.plt3, m_fluxModel.psinFluxLineValues, m_fluxModel.momentumFlux[ m_currentTStep ], fluxColor );

    renderPlotAnnotations( m_fluxView.plt1 );
    //renderPlotAnnotations( m_fluxView.plt2 );
    renderPlotAnnotations( m_fluxView.plt3 );

    m_renderer->renderViewPortBoxShadow( m_fluxView.viewPort(), simpleColorProg );
}

void PlotTool::renderPhysicalView()
{
    m_renderer->clearViewPort( m_physicalView.viewPort(), cartesianProg );

    m_physicalView.plt.make();
    renderPlotBase2D( m_physicalView.plt );

    const std::map< std::string, VisualContextModel< float > >  * const backgroundObjects =
        &( m_dataManager->particleDataManager().visualContextModels() );

    glViewport( 0, 0, width(), height() );
    for ( auto & obj : ( * backgroundObjects ) )
    {
        glLineWidth( 4  );
        glPointSize( 4 );
        renderPlotLine2D(
            m_physicalView.plt,
            obj.second.coordinates.find( "r" )->second,
            obj.second.coordinates.find( "z" )->second,
            QVector4D( .5, .5, .5, 1 ) );
    }

    glViewport(
        m_physicalView.plt.position().x() + m_physicalView.plt.plotLeft(),
        m_physicalView.plt.position().y() + m_physicalView.plt.plotTop(),
        m_physicalView.plt.plotSize().x(),
        m_physicalView.plt.plotSize().y() );

    m_renderer->renderHeatMapTexture(
        m_heatMapTexture,
        m_heatMapTF,
        m_heatMapProgSigned,
        m_heatMapRange.first,
        m_heatMapRange.second,
        false );

//        renderPlotLine2D(
//            m_physicalView.plt,
//            m_fluxModel.rFiltered[ m_currentTStep ],
//            m_fluxModel.zFiltered[ m_currentTStep ],
//            QVector4D( 0, .0, .0, .03 ),
//            true );

    glViewport( 0, 0, width(), height() );
    renderPlotAnnotations( m_physicalView.plt );
    m_renderer->renderViewPortBoxShadow( m_physicalView.viewPort(), simpleColorProg );
}

void PlotTool::renderTemporalView()
{
    m_renderer->clearViewPort( m_temporalView.viewPort(), cartesianProg );

    QVector4D fluxColor( 0.0, 0.0, 0.0, 1 );
    m_temporalView.plt.make();
    renderPlotBase2D(m_temporalView.plt );
    renderPlotLine2D( m_temporalView.plt, m_timeSteps, m_fluxModel.netParticleFlux, fluxColor );
    renderPlotAnnotations( m_temporalView.plt );

    m_renderer->renderViewPortBoxShadow( m_temporalView.viewPort(), simpleColorProg );
}

void PlotTool::renderStatisticalView()
{
    QVector4D fluxColor( 0.0, 0.0, 0.0, 1 );
    m_renderer->clearViewPort( m_statisticalView.viewPort(), cartesianProg );

    m_statisticalView.plt1.make();
    renderPlotBase2D( m_statisticalView.plt1 );
    renderPlotLine2D( m_statisticalView.plt1, m_fluxModel.switchAveFrequencyDistX, m_fluxModel.switchAveFrequencyDist, fluxColor );
    renderPlotAnnotations( m_statisticalView.plt1 );

    m_statisticalView.plt2.make();
    renderPlotBase2D( m_statisticalView.plt2 );
    renderPlotLine2D( m_statisticalView.plt2, m_fluxModel.switchDisplacementDistX, m_fluxModel.switchDisplacementDist, fluxColor );
    renderPlotAnnotations( m_statisticalView.plt2 );

    m_statisticalView.plt3.make();
    renderPlotBase2D( m_statisticalView.plt3 );
    renderPlotLine2D( m_statisticalView.plt3, m_fluxModel.switchWavelengthDistX, m_fluxModel.switchWavelengthDist, fluxColor );
    renderPlotAnnotations( m_statisticalView.plt3 );

    m_renderer->renderViewPortBoxShadow( m_statisticalView.viewPort(), simpleColorProg );
}

void PlotTool::renderPlotLine2D(
    Plot2D & plt,
    const std::vector< float > & x,
    const std::vector< float > & y,
    const QVector4D & color,
    bool justPoints
)
{
    glViewport(
        plt.position().x() + plt.plotLeft(),
        plt.position().y() + plt.plotTop(),
        plt.plotSize().x(),
        plt.plotSize().y() );

    m_camera.setSize( plt.plotSize().x(), plt.plotSize().y() );

    float w = plt.xRange().b() - plt.xRange().a();
    float h = plt.yRange().b() - plt.yRange().a();

    float cX = ( plt.xRange().a() + plt.xRange().b() ) / 2.f;
    float cY = ( plt.yRange().a() + plt.yRange().b() ) / 2.f;

    QMatrix4x4 TR;
    TR.setToIdentity();

    if( plt.maintainAspectRatio() )
    {
        TR = m_camera.proj() * TR;
        float scaleFactor = plt.aspectRatio() < w/h ? ( plt.aspectRatio()*2.f ) / w : 2.f / h;
        TR.scale( scaleFactor );
        TR.translate( -cX, -cY, 0.f );
    }
    else
    {
        float scaleFactorX = 2.0 / w;
        float scaleFactorY = 2.0 / h;
        TR.scale( scaleFactorX, scaleFactorY );
        TR.translate( -cX, -cY, 0.f );
    }

    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        color,
        TR,
        GL_POINTS );

    if( ! justPoints )
    {
        m_renderer->renderPrimitivesFlatSerial(
            x,
            y,
            flatProgSerial,
            color,
            TR,
            GL_LINE_STRIP );
    }

}

void PlotTool::renderPlotAnnotations( Plot2D & plt )
{
    glViewport( 0, 0, width(), height() );

    Vec3< float > textColor( .1, .1, .1  );

    m_renderer->renderText( false, textProg, width(), height(),
                            plt.yLabel(),
                            plt.position().x() + 20,
                            plt.position().y() + plt.size().y() / 2.0 - 40,
                            1.0,
                            textColor, true );

    m_renderer->renderText( false, textProg, width(), height(),
                            plt.xLabel(),
                            plt.position().x() + plt.plotLeft() + plt.plotSize().x() / 2.0 - 10,
                            plt.position().y() + 20,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            plt.title(),
                            plt.position().x() + plt.plotLeft(),
                            plt.position().y() + plt.size().y() - 30,
                            1.0,
                            textColor, false );

    int NR = plt.numGridRows();
    int NC = plt.numGridCols();

    const float w = plt.xRange().b() - plt.xRange().a();
    const float h = plt.yRange().b() - plt.yRange().a();

    float cX = ( plt.xRange().a() + plt.xRange().b() ) / 2.f;
    float cY = ( plt.yRange().a() + plt.yRange().b() ) / 2.f;

    QMatrix4x4 TR;
    TR.setToIdentity();
    m_camera.setSize( width(), height() );
    TR = m_camera.proj() * TR;
    float scaleFactor = plt.aspectRatio() < w/h ? ( plt.aspectRatio()*2.f ) / w : 2.f / h;
    TR.scale( scaleFactor );
    TR.translate( -cX, -cY, 0.f );

    QVector4D ul, lr;
    ul = TR.inverted() * QVector4D( -1, 1, 0, 1 );
    lr = TR.inverted() * QVector4D(  1,-1, 0, 1 );

    for( int r = 0; r <= NR; ++r )
    {
        float y = ( float( r ) / NR ) * ( plt.yRange().b() -  plt.yRange().a() ) + plt.yRange().a();
        if( plt.maintainAspectRatio() )
        {
            y = lr.y() + ( ul.y() - lr.y() ) * ( float( r ) / NR );
        }
        QString yl( QString::number( y, 'g', 2 ) );

//            renderText(
//                plt.position().x() + plt.plotLeft() - yl.size()*8.5 - ( yl.size()==1 ? 8 : 0),
//                height() - ( plt.plotPosition().y() + plt.plotSize().y()*( float( r ) / NR ) - 5 ),
//                yl );

        m_renderer->renderText( false, textProg, width(), height(),
                                yl.toStdString(),
                                plt.position().x() + plt.plotLeft() - yl.size()*8.5 - ( yl.size()==1 ? 8 : 0),
                                plt.plotPosition().y() + plt.plotSize().y()* float( r ) / NR - 5,
                                1.0,
                                textColor, false );
    }

    for( int c = 0; c <= NC; ++c )
    {
        float x = ( float( c ) / NC ) * ( plt.xRange().b() -  plt.xRange().a() ) + plt.xRange().a();
        QString xl( QString::number( x, 'g', 2 ) );

        m_renderer->renderText( false, textProg, width(), height(),
                                xl.toStdString(),
                                plt.position().x() + plt.plotLeft() + ( float( c ) / NC ) * plt.plotSize().x() - xl.size()*5,
                                plt.position().y() + 44,
                                1.0,
                                textColor, false );
    }

    if( plt.valueIsSelected() )
    {
        QString sx = QString::number( plt.selectedValue().x(), 'g', 3 );
        QString sy = QString::number( plt.selectedValue().y(), 'g', 3 );

        QString valueXLabel( QString( plt.xLabel().c_str() ) + " = " + sx );
        QString valueYLabel( QString( plt.yLabel().c_str() ) + " = " + sy );

        m_renderer->renderText( false, textProg, width(), height(),
                                valueXLabel.toStdString(),
                                plt.position().x() + plt.size().x() - 32*8.0,
                                plt.position().y() + plt.plotTop() + plt.plotSize().y() + 10,
                                1.0,
                                textColor, false );

        m_renderer->renderText( false, textProg, width(), height(),
                                valueYLabel.toStdString(),
                                plt.position().x() + plt.size().x() - 17*8.0,
                                plt.position().y() + plt.plotTop() + plt.plotSize().y() + 10,
                                1.0,
                                textColor, false );
    }
}

void PlotTool::renderPlotBase2D(
    Plot2D & plt )
{
    glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() );
    m_camera.setSize( plt.size().x(), plt.size().y() );

    m_renderer->renderPrimitivesFlat(
        plt.background(),
        cartesianProg,
        QVector4D( 0.80, 0.80, 0.80, 1.f ),
        QMatrix4x4(),
        GL_TRIANGLES
    );

//        m_renderer->renderPrimitivesColored(
//            plt.boxShadow().verts(),
//            plt.boxShadow().colors(),
//            simpleColorProg,
//            QMatrix4x4(),
//            GL_TRIANGLES
//        );

    glLineWidth( 1.0 );
    m_renderer->renderPopSquare( simpleColorProg );

    glViewport(
        plt.position().x() + plt.plotLeft(),
        plt.position().y() + plt.plotTop(),
        plt.plotSize().x(),
        plt.plotSize().y() );

    m_camera.setSize( plt.plotSize().x(), plt.plotSize().y() );

    m_renderer->renderPrimitivesFlat(
        plt.background(),
        cartesianProg,
        QVector4D( 1.f, 1.0f, 1.0f, 1.f ),
        QMatrix4x4(),
        GL_TRIANGLES
    );

    glLineWidth( 1.0 );
    m_renderer->renderPrimitivesFlat(
        plt.grid(),
        cartesianProg,
        QVector4D( 0.9f, 0.9f, 0.9f, 1.0f ),
        QMatrix4x4(),
        GL_LINES
    );
    glLineWidth( 1.0 );

    glLineWidth( 3.0 );
    m_renderer->renderPrimitivesFlat(
        plt.axis(),
        cartesianProg,
        QVector4D( 0.9f, 0.9f, 0.9f, 1.0f ),
        QMatrix4x4(),
        GL_LINES
    );
    glLineWidth( 1.0 );
    m_renderer->renderPopSquare( simpleColorProg );
}

void PlotTool::render()
{
    if( m_initializedGL == false )
    {
        initGL();
        m_initializedGL = true;
        qDebug() << "initialized GL";
    }

    glClearColor( .8, .8, .8, 1.0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    applyLayout();

    static FluxCalculationParams calcParams;
    // for testing, so it's changed...and will recalculate

    static int tStep = m_currentTStep;

    if( tStep != m_currentTStep )
    {
        Vec2< float > xR( 9999, -9999 );
        Vec2< float > yR( 9999, -9999 );

        const std::map< std::string, VisualContextModel< float > >  * const backgroundObjects =
            &( m_dataManager->particleDataManager().visualContextModels() );

        for ( auto & obj : ( * backgroundObjects ) )
        {
            for( int i = 0; i < obj.second.coordinates.find( "r" )->second.size(); ++i )
            {
                xR.a( std::min( obj.second.coordinates.find( "r" )->second[ i ], xR.a() ) );
                xR.b( std::max( obj.second.coordinates.find( "r" )->second[ i ], xR.b() ) );

                yR.a( std::min( obj.second.coordinates.find( "z" )->second[ i ], yR.a() ) );
                yR.b( std::max( obj.second.coordinates.find( "z" )->second[ i ], yR.b() ) );
            }
        }

        m_heatMapper.buffer(
            *( m_dataManager->particleDataManager().values( "ions", "r" )[ m_currentTStep ] ),
            *( m_dataManager->particleDataManager().values( "ions", "z" )[ m_currentTStep ] ),
            *( m_dataManager->particleDataManager().values( "ions", "w0w1" )[ m_currentTStep ] ),
            m_flags[ m_currentTStep ]
        );

        m_heatMapper.allocateResultDevice( m_physicalView.plt.resolution().a() / 4, m_physicalView.plt.resolution().b()/4 );
        m_heatMapper.compute( xR.a(), xR.b(), yR.a(), yR.b(), m_heatImage, m_heatMapRange, true );
        m_heatMapTexture.load( m_heatImage, m_physicalView.plt.resolution().a() / 4, m_physicalView.plt.resolution().b() / 4 );
        tStep = m_currentTStep;
    }

    if( !( calcParams == m_fluxModel.calculationParams ) )
    {
        qDebug() << "recalculating flux";
        m_fluxModel.calculateFlux( m_dataManager->particleDataManager() );
        calcParams = m_fluxModel.calculationParams;
        qDebug() << "done calculating flux";

        m_fluxView.plt1.initialize(
            m_fluxModel.calculationParams.psinRange,
            Vec2< float >( m_fluxModel.particleFluxRange.a(), m_fluxModel.particleFluxRange.b() ),
            "Psin",
            "Particle Flux",
            "Ion Particle Flux Out Through Magnetic Flux Surfaces"
        );

        m_fluxView.plt2.initialize(
            m_fluxModel.calculationParams.psinRange,
            Vec2< float >( m_fluxModel.energyFluxRange.a(), m_fluxModel.energyFluxRange.b() ),
            "Psin",
            "Kinetic Energy Flux",
            "Ion Kinetic Energy Flux Out Through Magnetic Flux Surfaces"
        );

        m_fluxView.plt3.initialize(
            m_fluxModel.calculationParams.psinRange,
            Vec2< float >( m_fluxModel.momentumFluxRange.a(), m_fluxModel.momentumFluxRange.b() ),
            "Psin",
            "Momentum Flux",
            "Ion Momentum Flux Out Through Magnetic Flux Surfaces"
        );

        qDebug() << "Part Flux Range:  " << m_fluxModel.particleFluxRange.a() <<  " " << m_fluxModel.particleFluxRange.b();

        const std::map< std::string, VisualContextModel< float > >  * const backgroundObjects =
            &( m_dataManager->particleDataManager().visualContextModels() );

        Vec2< float > xR( 9999, -9999 );
        Vec2< float > yR( 9999, -9999 );

        for ( auto & obj : ( * backgroundObjects ) )
        {
            for( int i = 0; i < obj.second.coordinates.find( "r" )->second.size(); ++i )
            {
                xR.a( std::min( obj.second.coordinates.find( "r" )->second[ i ], xR.a() ) );
                xR.b( std::max( obj.second.coordinates.find( "r" )->second[ i ], xR.b() ) );

                yR.a( std::min( obj.second.coordinates.find( "z" )->second[ i ], yR.a() ) );
                yR.b( std::max( obj.second.coordinates.find( "z" )->second[ i ], yR.b() ) );
            }
        }

        m_physicalView.plt.initialize(
            xR,
            yR,
            "r",
            "z",
            "Flux Calculation Region on Poliodal Plane",
            true
        );

        m_temporalView.plt.initialize(
            Vec2< float >(
                0,
                m_dataManager->particleDataManager().numTimeSteps()-1 ),
            m_fluxModel.netParticleFluxRange,
            "t",
            "flux",
            "Net Volumetric Particle Flux"
        );

        ///////////////////////////////////////////////

        m_statisticalView.plt1.initialize(
            m_fluxModel.switchAveFrequencyRange,
            m_fluxModel.switchAveFrequencyDistRange,
            "Avg. Frequency",
            "Count",
            "Distribution of Per-Particle Avg. Psi-n Direction Change Freq."
        );

        m_statisticalView.plt2.initialize(
            m_fluxModel.switchDisplacementRange,
            m_fluxModel.switchDisplacementDistRange,
            "Magnitude",
            "Count",
            "Distribution of Psi-n Displacements Between Direction Changes"
        );

        m_statisticalView.plt3.initialize(
            m_fluxModel.switchWavelengthRange,
            m_fluxModel.switchWavelengthDistRange,
            "Length",
            "Count",
            "Distribution of Time Steps Between Direction Changes"
        );
    }

    renderPhysicalView();
    renderFluxView();
    renderStatisticalView();
    renderTemporalView();

    m_renderer->renderSlider( m_resolutionSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_lookAheadSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_thresholdSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_smoothSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_angleCenterSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_angleRangeSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_psinRangeSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_psinCenterSlider, cartesianProg, simpleColorProg );

    glViewport( 0, 0, width(), height() );
    Vec3< float > textColor( .1, .1, .1  );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Psi-n Resolution",
                            m_resolutionSlider.position().x(),
                            m_resolutionSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Look Ahead",
                            m_lookAheadSlider.position().x(),
                            m_lookAheadSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Zoom",
                            m_smoothSlider.position().x(),
                            m_smoothSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "P-sin Displacement Threshold",
                            m_thresholdSlider.position().x(),
                            m_thresholdSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Poloid Angle Center",
                            m_angleCenterSlider.position().x(),
                            m_angleCenterSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Poloidal Angle Width",
                            m_angleRangeSlider.position().x(),
                            m_angleRangeSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Psi-n Center",
                            m_psinCenterSlider.position().x(),
                            m_psinCenterSlider.position().y() + 45,
                            1.0,
                            textColor, false );

    m_renderer->renderText( false, textProg, width(), height(),
                            "Psi-n Width",
                            m_psinRangeSlider.position().x(),
                            m_psinRangeSlider.position().y() + 45,
                            1.0,
                            textColor, false );
}

void PlotTool::applyLayout()
{
    const float CP_H = 80;

    const float SEPARATION = 10;

    const float TV_H = 280;
    const float SV_W = 600;
    const float PV_W = 900;

    const float W = width();
    const float H = height();

    m_temporalView.setPosition( SEPARATION, SEPARATION );
    m_temporalView.setSize( W-SEPARATION*2, TV_H );

    m_physicalView.setSize( PV_W, H - CP_H - SEPARATION *2 - m_temporalView.size().y() );
    m_physicalView.setPosition( SEPARATION, H-CP_H-m_physicalView.size().y() );

    m_statisticalView.setSize( SV_W, m_physicalView.size().y() );
    m_statisticalView.setPosition( W - m_statisticalView.size().x() - SEPARATION, m_physicalView.position().y() );

    m_fluxView.setPosition( m_physicalView.position().x() + m_physicalView.size().x() + SEPARATION, m_physicalView.position().y() );
    m_fluxView.setSize(
        W - m_physicalView.size().x() - m_statisticalView.size().x() - SEPARATION*4,
        m_physicalView.size().y() );

    const float SPACING = 30;
    const float SLIDER_WIDTH = ( m_fluxView.size().x() - SPACING * 3 ) / 4.0;

    m_resolutionSlider.setSize( SLIDER_WIDTH, 30 );
    m_lookAheadSlider.setSize( SLIDER_WIDTH, 30 );
    m_thresholdSlider.setSize( SLIDER_WIDTH, 30 );
    m_smoothSlider.setSize( SLIDER_WIDTH, 30 );

    m_resolutionSlider.setPosition( m_fluxView.position().x(), height() - 70 );
    m_lookAheadSlider.setPosition( m_fluxView.position().x() + SLIDER_WIDTH + SPACING, height() - 70 );
    m_thresholdSlider.setPosition( m_fluxView.position().x() + 2 * ( SLIDER_WIDTH + SPACING), height() - 70 );
    m_smoothSlider.setPosition( m_fluxView.position().x() + 3 * ( SLIDER_WIDTH + SPACING), height() - 70 );

    const float P_SLIDER_W = ( m_physicalView.size().x() - SPACING * 3 - 30 ) / 4.0;

    m_angleCenterSlider.setSize( P_SLIDER_W, 30 );
    m_angleRangeSlider.setSize( P_SLIDER_W, 30 );
    m_psinRangeSlider.setSize( P_SLIDER_W, 30 );
    m_psinCenterSlider.setSize( P_SLIDER_W, 30 );

    m_angleCenterSlider.setPosition( m_physicalView.position().x() + 30, height() - 70 );
    m_angleRangeSlider.setPosition( m_physicalView.position().x() + 30 + P_SLIDER_W + SPACING, height() - 70 );
    m_psinRangeSlider.setPosition( m_physicalView.position().x() + 30 + 2*( P_SLIDER_W + SPACING ), height() - 70 );
    m_psinCenterSlider.setPosition( m_physicalView.position().x() + 30 + 3*( P_SLIDER_W + SPACING ), height() - 70 );
}

void PlotTool::wheelEvent( QWheelEvent *e )
{
    double numDegrees = e->angleDelta().y() / -9000000.0;
    Vec2< float > pos( m_previousMousePosition.x(), height() - m_previousMousePosition.y()  );

    std::vector< Plot2D * > plts(
    {
        &m_fluxView.plt1,
        &m_fluxView.plt2,
        &m_fluxView.plt3,
        &m_physicalView.plt,
        &m_statisticalView.plt1,
        &m_statisticalView.plt2,
        &m_statisticalView.plt3
    } );

    m_camera.setSize( width(), height() );

    for( auto & pltp : plts )
    {
        auto & plt = *pltp;
        if(  pos.x() > plt.position().x() + plt.plotLeft()
                && pos.x() < plt.position().x() + plt.plotLeft() + plt.plotSize().x()
                && pos.y() > plt.position().y() + plt.plotTop()
                && pos.y() < plt.position().y() + plt.plotTop() + plt.plotSize().y() )
        {
            plt.zoom( numDegrees, 2 );
        }
    }
    renderLater();
}

void PlotTool::mouseDoubleClickEvent( QMouseEvent *e )
{

}

void PlotTool::mouseMoveEvent( QMouseEvent *e )
{
    QPointF mouseCurr = e->localPos();
    m_previousMousePosition = mouseCurr;
    Vec2< float > pos( mouseCurr.x(), height() - mouseCurr.y()  );

    m_previousMousePosition = mouseCurr;

    bool sliderDown = false;
    if( m_resolutionSlider.isPressed() )
    {
        sliderDown = true;
        m_resolutionSlider.translate( pos );
    }
    if( m_lookAheadSlider.isPressed() )
    {
        sliderDown = true;
        m_lookAheadSlider.translate( pos );
    }
    if( m_thresholdSlider.isPressed() )
    {
        sliderDown = true;
        m_thresholdSlider.translate( pos );
    }
    if( m_smoothSlider.isPressed() )
    {
        sliderDown = true;
        m_smoothSlider.translate( pos );
    }
    if( m_angleCenterSlider.isPressed() )
    {
        sliderDown = true;
        m_angleCenterSlider.translate( pos );
    }
    if( m_angleRangeSlider.isPressed() )
    {
        sliderDown = true;
        m_angleRangeSlider.translate( pos );
    }
    if( m_psinRangeSlider.isPressed() )
    {
        sliderDown = true;
        m_psinRangeSlider.translate( pos );
    }
    if( m_psinCenterSlider.isPressed() )
    {
        sliderDown = true;
        m_psinCenterSlider.translate( pos );
    }

    if( e->buttons() & Qt::LeftButton && m_temporalView.plt.pointInViewPort( pos ) )
    {
        float left = m_temporalView.plt.position().x() + m_temporalView.plt.plotLeft();
        float right = m_temporalView.plt.position().x() + m_temporalView.plt.size().x() - m_temporalView.plt.plotRight();
        if( pos.x() > left )
        {
            float r = pos.x() - left;
            int newStep = ( r / ( right - left ) ) * m_dataManager->particleDataManager().numTimeSteps();
            newStep = std::min( std::max( newStep, 0 ), m_dataManager->particleDataManager().numTimeSteps() - std::max( m_fluxModel.calculationParams.lookAhead, 1 ) );
            m_currentTStep = newStep;
            renderLater();
        }
    }

    std::vector< Plot2D * > plts(
    {
        &m_fluxView.plt1,
        &m_fluxView.plt2,
        &m_fluxView.plt3,
        &m_physicalView.plt,
        &m_statisticalView.plt1,
        &m_statisticalView.plt2,
        &m_statisticalView.plt3
    }  );

    m_camera.setSize( width(), height() );

    for( auto & pltp : plts )
    {
        auto & plt = *pltp;
        if(  pos.x() > plt.position().x() + plt.plotLeft()
                && pos.x() < plt.position().x() + plt.plotLeft() + plt.plotSize().x()
                && pos.y() > plt.position().y() + plt.plotTop()
                && pos.y() < plt.position().y() + plt.plotTop() + plt.plotSize().y() )
        {
            const float w = plt.xRange().b() - plt.xRange().a();
            const float h = plt.yRange().b() - plt.yRange().a();

            float cX = ( plt.xRange().a() + plt.xRange().b() ) / 2.f;
            float cY = ( plt.yRange().a() + plt.yRange().b() ) / 2.f;

            const float xr  = ( ( pos.x() - ( plt.position().x() + plt.plotLeft() ) ) / (double)plt.plotSize().x() ) * w;
            const float yr  = ( ( pos.y() - ( plt.position().y() +  plt.plotTop() ) ) / (double)plt.plotSize().y() ) * h;
            const float yrn = ( ( pos.y() - ( plt.position().y() +  plt.plotTop() ) ) / (double)plt.plotSize().y() );

            const float xra = plt.xRange().a();
            const float yra = plt.yRange().a();

            float sMRFX = 1.0 / w;
            float sMRFY = 1.0 / h;

            float sXA = xra + xr;
            float sYA = yra + yr;

            if( plt.maintainAspectRatio() )
            {
                sYA = ( yra + yr ) * w / ( h * plt.aspectRatio() );
                sMRFY = sMRFX;

                QMatrix4x4 TR;
                TR.setToIdentity();
                TR = m_camera.proj() * TR;
                float scaleFactor = plt.aspectRatio() < w/h ? ( plt.aspectRatio()*2.f ) / w : 2.f / h;
                TR.scale( scaleFactor );
                TR.translate( -cX, -cY, 0.f );

                QVector4D ul, lr;
                ul = TR.inverted() * QVector4D( -1, 1, 0, 1 );
                lr = TR.inverted() * QVector4D(  1,-1, 0, 1 );

                sYA = lr.y() + ( ul.y() - lr.y() ) * yrn;
            }

            plt.selectValue( Vec2< float >( sXA, sYA ) );

            //////////////

            if( e->buttons() & Qt::LeftButton )
            {
                plt.translate( mouseCurr.x() - m_mouseLeftStart.x(), mouseCurr.y() - m_mouseLeftStart.y()  );
                m_mouseLeftStart = mouseCurr;
            }
            renderLater();
        }
        else
        {
            plt.deselectValue();
            renderLater();
        }
    }

    if( sliderDown == true )
    {
        renderLater();
    }
}

void PlotTool::resizeEvent( QResizeEvent *e )
{
    applyLayout();
}

void PlotTool::keyPressEvent( QKeyEvent *e )
{

}

void PlotTool::mousePressEvent( QMouseEvent *e )
{
    QPointF mouseCurr = e->localPos();
    Vec2< float > pos( mouseCurr.x(), height() - mouseCurr.y() );

    if( e->buttons() & Qt::LeftButton )
    {
        m_mouseLeftStart = mouseCurr;
    }

    if( m_resolutionSlider.pointInViewPort( pos ) )
    {
        m_resolutionSlider.setPressed( true );
    }
    if( m_lookAheadSlider.pointInViewPort( pos ) )
    {
        m_lookAheadSlider.setPressed( true );
    }
    if( m_thresholdSlider.pointInViewPort( pos ) )
    {
        m_thresholdSlider.setPressed( true );
    }
    if( m_smoothSlider.pointInViewPort( pos ) )
    {
        m_smoothSlider.setPressed( true );
    }
    if( m_angleCenterSlider.pointInViewPort( pos ) )
    {
        m_angleCenterSlider.setPressed( true );
    }
    if( m_angleRangeSlider.pointInViewPort( pos ) )
    {
        m_angleRangeSlider.setPressed( true );
    }
    if( m_psinRangeSlider.pointInViewPort( pos ) )
    {
        m_psinRangeSlider.setPressed( true );
    }
    if( m_psinCenterSlider.pointInViewPort( pos ) )
    {
        m_psinCenterSlider.setPressed( true );
    }
    if( m_temporalView.plt.pointInViewPort( pos ) )
    {
        float left = m_temporalView.plt.position().x() + m_temporalView.plt.plotLeft();
        float right = m_temporalView.plt.position().x() + m_temporalView.plt.size().x() - m_temporalView.plt.plotRight();
        if( pos.x() > left )
        {
            float r = pos.x() - left;
            int newStep = ( r / ( right - left ) ) * m_dataManager->particleDataManager().numTimeSteps();
            newStep = std::min( std::max( newStep, 0 ), m_dataManager->particleDataManager().numTimeSteps() - std::max( m_fluxModel.calculationParams.lookAhead, 1 ) );
            m_currentTStep = newStep;
            renderLater();
        }
    }
}

void PlotTool::mouseReleaseEvent( QMouseEvent *e )
{
    QPointF mouseCurr = e->localPos();
    Vec2< float > pos( mouseCurr.x(), height() - mouseCurr.y() );

    bool sliderDown = false;

    if( m_resolutionSlider.isPressed() )
    {
        sliderDown = true;

    }
    if( m_lookAheadSlider.isPressed() )
    {
        sliderDown = true;

    }
    if( m_thresholdSlider.isPressed() )
    {
        sliderDown = true;

    }
    if( m_smoothSlider.isPressed() )
    {
        sliderDown = true;

    }
    if( m_angleCenterSlider.isPressed() || m_angleRangeSlider.isPressed() )
    {
        const float _PI = 3.14159265359;
        sliderDown = true;

        float center = m_angleCenterSlider.sliderPosition() * 2 * _PI;
        float width = m_angleRangeSlider.sliderPosition() * _PI + .5;

        float lower = center - width;
        float upper = center + width;

        if( width >= _PI - .2  )
        {
            m_fluxModel.calculationParams.angularRangeA = Vec2< float >( 0, 2 * _PI + .1 );
            m_fluxModel.calculationParams.angularRangeB = Vec2< float >( 0, 2 * _PI + .1 );
        }
        else
        {
            while( lower < 0 )
            {
                lower += 2*_PI;
            }
            while( upper < 0 )
            {
                upper += 2* _PI;
            }
            while( upper > 2*_PI )
            {
                upper -= 2*_PI;
            }
            while( lower > 2*_PI )
            {
                lower -= 2*_PI;
            }

            Vec2< float > A, B;

            if( upper < lower )
            {
                A = Vec2< float >( 0, upper );
                B = Vec2< float >( lower, 2*_PI );
            }
            else
            {
                A = Vec2< float >( lower, upper );
                B = A;
            }

            m_fluxModel.calculationParams.angularRangeA = A;
            m_fluxModel.calculationParams.angularRangeB = B;
        }
    }

    if( m_psinRangeSlider.isPressed() )
    {
        sliderDown = true;
        float center = m_psinCenterSlider.sliderPosition();
        float width = .1 + m_psinRangeSlider.sliderPosition() / 2.0;

        float upper = std::min( 1.00001f, center + width );
        float lower = std::max( 0.0001f, center - width );

        m_fluxModel.calculationParams.psinRange = Vec2< float >( lower, upper );
    }
    if( m_psinCenterSlider.isPressed() )
    {
        sliderDown = true;
        float center = m_psinCenterSlider.sliderPosition();
        float width = .1 + m_psinRangeSlider.sliderPosition() / 2.0;

        float upper = std::min( 1.00001, center + width/2.0 );
        float lower = std::max( 0.0001, center - width/2.0 );

        m_fluxModel.calculationParams.psinRange = Vec2< float >( lower, upper );
    }
    if( m_resolutionSlider.isPressed() )
    {
        sliderDown = true;
        int resolution = std::max( 8.0, m_resolutionSlider.sliderPosition() * 10000.0 );
        m_fluxModel.calculationParams.psinResolutionFactor = resolution;
    }
    if( m_lookAheadSlider.isPressed() )
    {
        sliderDown = true;
        m_fluxModel.calculationParams.lookAhead = std::floor( m_lookAheadSlider.sliderPosition() * 30 );
    }
    if( m_thresholdSlider.isPressed() )
    {
        sliderDown = true;
        m_fluxModel.calculationParams.psinEpsilon = m_thresholdSlider.sliderPosition() * 0.005;
    }
    if( sliderDown == true )
    {
        renderLater();
    }

    m_resolutionSlider.setPressed( false );
    m_lookAheadSlider.setPressed( false );
    m_thresholdSlider.setPressed( false );
    m_smoothSlider.setPressed( false );
    m_angleCenterSlider.setPressed( false );
    m_angleRangeSlider.setPressed( false );
    m_psinRangeSlider.setPressed( false );
    m_psinCenterSlider.setPressed( false );
}
}
