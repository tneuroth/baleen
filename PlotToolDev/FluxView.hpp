#ifndef FLUXVIEW_HPP
#define FLUXVIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "Plot2D.hpp"

namespace TN
{

class FluxView : public Widget
{

public :

    Plot2D plt1;
    Plot2D plt2;
    Plot2D plt3;

    FluxView() : Widget()
    {
        plt1.bkgColor( Vec4( 0, 1, 1, 1 ) );
        //plt2.bkgColor( Vec4( 1, 0, 1, 1 ) );
        plt3.bkgColor( Vec4( 1, 1, 0, 1 ) );
    }

    virtual void setSize( float x, float y )
    {
        Widget::setSize( x, y );
        float ph = y / 2.f;

        plt1.setSize( x, ph );
        //plt2.setSize( x, ph );
        plt3.setSize( x, ph );
    }
    virtual void setPosition( float x, float y )
    {
        Widget::setPosition( x, y );
        plt1.setPosition( x, y );
        //plt2.setPosition( x, y + plt1.size().y() );
        plt3.setPosition( x, y + plt1.size().y() /*+ plt2.size().y()*/ );
    }
};

}

#endif // FLUXVIEW_HPP
