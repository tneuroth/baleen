#ifndef TEMPORALVIEW_HPP
#define TEMPORALVIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"

namespace TN
{

class TemporalView : public Widget
{

public:

    Plot2D plt;

    TemporalView() : Widget()
    {
        plt.bkgColor( Vec4( 0, .6, .4, 1 ) );
    }

    virtual void setSize( float x, float y )
    {
        Widget::setSize( x, y );
        plt.setSize( x, y );
    }

    virtual void setPosition( float x, float y )
    {
        Widget::setPosition( x, y );
        plt.setPosition( x, y );
    }
};

}

#endif // TEMPORALVIEW_HPP
