#ifndef PLOTTOOL_HPP
#define PLOTTOOL_HPP

#include "FluxModel.hpp"
#include "OpenGL/Widgets/SliderWidget.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/Viewport/BoxShadowFrame.hpp"
#include "Types/Vec.hpp"
#include "OpenGL/Widgets/Viewport/ViewPort.hpp"
#include "OpenGL/Rendering/Cameras/Camera2D.hpp"
#include "OpenGL/Rendering/RenderParams.hpp"
#include "OpenGL/Rendering/Renderer.hpp"
#include "OpenGL/Qt/Windows/RenderWindow.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"

#include "FluxView.hpp"
#include "PhysicalView.hpp"
#include "StatisticalView.hpp"
#include "TemporalView.hpp"

#include "Cuda/Heatmap.hpp"

#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <array>
#include <atomic>
#include <vector>
#include <array>

class QKeyEvent;
class QMouseEvent;
class QGLFramebufferObject;
class QOpenGLShaderProgram;

namespace TN
{

template< class T >
class DataSetManager;

class PhasePlotDefinitionDialogue;
class ParticleFilterDialogue;

class PlotTool : public RenderWindow
{
    Q_OBJECT

    void compileShaders();
    void loadData();
    void initGL();

public:

    PlotTool();
    ~PlotTool();

protected:

    virtual void render();
    virtual void wheelEvent( QWheelEvent *e );
    virtual void mouseDoubleClickEvent( QMouseEvent *e );
    virtual void mouseMoveEvent( QMouseEvent *e );
    virtual void resizeEvent( QResizeEvent *e );
    virtual void keyPressEvent( QKeyEvent *e );
    virtual void mousePressEvent( QMouseEvent *e );
    virtual void mouseReleaseEvent( QMouseEvent *e );

private:

    // State

    int m_currentTStep;
    QPointF m_previousMousePosition;
    QPointF m_mouseLeftStart;
    bool m_initializedGL;

    // Ui

    FluxView m_fluxView;
    PhysicalView m_physicalView;
    TemporalView m_temporalView;
    StatisticalView m_statisticalView;

    SliderWidget m_resolutionSlider;
    SliderWidget m_lookAheadSlider;
    SliderWidget m_thresholdSlider;
    SliderWidget m_smoothSlider;

    SliderWidget m_angleRangeSlider;
    SliderWidget m_angleCenterSlider;

    SliderWidget m_psinRangeSlider;
    SliderWidget m_psinCenterSlider;

    void renderFluxView();
    void renderPhysicalView();
    void renderTemporalView();
    void renderStatisticalView();

    void applyLayout();

    // Model

    FluxModel m_fluxModel;
    std::vector< float > m_timeSteps;

    // Data

    DataSetManager<float> * m_dataManager;

    // Rendering Utilities

    Camera2D m_camera;
    Renderer * m_renderer;

    HeatMapper2D m_heatMapper;
    std::vector< float > m_heatImage;
    std::pair< float, float > m_heatMapRange;
    Texture2D1 m_heatMapTexture;
    Texture1D  m_heatMapTF;
    std::vector< Vec3< float > > m_tf;
    QGLFramebufferObject *m_fbo;
    const int TF_SIZE = 1024;
    std::vector< float > m_pixelDensityMap;

    std::vector< std::vector< unsigned char > > m_flags;

    std::unique_ptr< QOpenGLShaderProgram > cartesianProg;
    std::unique_ptr< QOpenGLShaderProgram > cartesianProg3;
    std::unique_ptr< QOpenGLShaderProgram > colorMapProg3;
    std::unique_ptr< QOpenGLShaderProgram > points3DProg;
    std::unique_ptr< QOpenGLShaderProgram > tubePathProgram;
    std::unique_ptr< QOpenGLShaderProgram > simpleProg;
    std::unique_ptr< QOpenGLShaderProgram > simpleColorProg;
    std::unique_ptr< QOpenGLShaderProgram > prog3D;
    std::unique_ptr< QOpenGLShaderProgram > flatProg;
    std::unique_ptr< QOpenGLShaderProgram > flatProgSerial;
    std::unique_ptr< QOpenGLShaderProgram > textProg;
    std::unique_ptr< QOpenGLShaderProgram > ucProg;
    std::unique_ptr< QOpenGLShaderProgram > flatMeshProg;
    std::unique_ptr< QOpenGLShaderProgram > timeStackProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapGenProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapRenderProg;
    std::unique_ptr< QOpenGLShaderProgram > binDiagnosticProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_heatMapProgSigned;

    std::map<
    std::string,
        std::pair< std::uint8_t, std::unique_ptr< QOpenGLShaderProgram > > >
        m_shaderMap;

    void renderPlotAnnotations(
        Plot2D & plt );

    void renderPlotBase2D(
        Plot2D & plt );

    void renderPlotLine2D(
        Plot2D & plt,
        const std::vector< float > & x,
        const std::vector< float > & y,
        const QVector4D & color,
        bool justPoints = false
    );

    struct ShaderType
    {
        enum
        {
            VERTEX_SHADER = 1,
            FRAGMENT_SHADER = 2,
            GEOMETRY_SHADER = 4,
            COMPUTE_SHADER = 8
        };
    };
};

class PlotToolWidget : public QWidget
{

public:

    PlotToolWidget(QWidget *parent) : QWidget(parent)
    {
        renWin = new PlotTool();

        QWidget *widget = QWidget::createWindowContainer(renWin);
        QHBoxLayout *layout = new QHBoxLayout(this);

        layout->setMargin(0);
        layout->addWidget(widget);
    }

    ~PlotToolWidget() {}

    virtual PlotTool *GetRenderWindow()
    {
        return renWin;
    }

private:

    PlotTool *renWin;
};

}

#endif // PLOTTOOL_HPP
