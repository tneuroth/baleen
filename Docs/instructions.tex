\documentclass[]{article}
\usepackage{verbatim}

%opening
\title{Remote Visualization Through VNC on Cooley}
\author{Tyson Neuroth\\taneuroth@ucdavis.edu}

\begin{document}

\maketitle

\section*{Preparing your Dataset}

\subsection{Expected Format}

For raw particle data from \textbf{XGC}, some conversion and preprocessing is necessary. For reference you can look at the test dataset at \verb|\home\tneuroth\test_data\ITER|\\

\noindent
\textit{Note: the test data we have had been somehow converted to hdf5.}\\

\noindent
\textbf{File Hierarchy and Contents}

\begin{verbatim}
<Dataset Folder Name>
    ParticleData
        xgc.particle.00002.h5
        xgc.particle.00004.h5
        xgc.particle.00006.h5
        ...
    Grid
        xgc.bfield.h5   
        xgc.mesh.h5
    units.m
    PreProcessed
        Grid
            bphi.bin  
            br.bin  
            bz.bin  
            psin.bin
        ParticleData
            info.electrons.txt
            info.ions.txt
            xgc.ions.sample.txt
            xgc.electrons.sample.txt
            xgc.electrons.00002.bin
            xgc.electrons.00004.bin
            ...
            xgc.ions.00002.bin
            xgc.ions.00004.bin
        Geometry
            separatrix.txt
            walls.txt
\end{verbatim}

\noindent
\textit{Note: time steps can increment by an arbitrary number, but should be uniformly spaced.}\\

\subsection{Preprocessing}

\subsubsection{Grid and Geometry}

\noindent
\textbf{Grid}\\

\noindent
The files, \verb|bphi.bin br.bin bz.bin psin.bin|, are high resolution lookup tables interpolated from the triangular mesh data. The tool will generate them from the hdf5 files if they don't already exist. It's assumed that \verb|Preprocessed->Grid| is an existing directory and that \verb|Grid->xgc.bfield.h5, xgc.mesh.h5| exist and are in the same format as our test data.\\

\noindent
\textit{Note that in our test data, the data set,} \verb|psi| \textit{from within} \verb|xgc.mesh.h5|\textit{, contained as the last entry, the psi value from which to normalize in order to convert to psin.}\\

\noindent
If you wish to create these lookup tables yourself using your preferred interpolation scheme and resolution, you could pre-generate them in the following format. 

\begin{verbatim}
std::ofstream outFile( path1, std::ios::binary );
outFile.write( (char*) & num_rows, sizeof( int ) );
outFile.write( (char*) & num_cols, sizeof( int ) );
outFile.write( (char*) & zmin, sizeof( double ) );
outFile.write( (char*) & zmax, sizeof( double ) );
outFile.write( (char*) & rmin, sizeof( double ) );
outFile.write( (char*) & rmax, sizeof( double ) );
outFile.write( (char*) values, sizeof( double ) * num_rows * num_cols );
outFile.close();
\end{verbatim}\\

\noindent
\textit{Where values is a flattened 2D array indexed through r,z as a lookup table. For empty space (where no mesh values exist), we just set the values to} \verb|NAN|.\\

\noindent
\textbf{Geometry}\\

\noindent
\verb|separatrix.txt, walls.txt| are optional for rendering the separatrix and device walls in the views of the system.\\

\noindent
The format is as follows:

\begin{verbatim}
r z
r1 z1
r2 z2
\end{verbatim}

\noindent
\textit{The first line are the labels for the points and the following lines are a series of ordered points which when connected consecutively, draw the separatrix, or outline of the device.}\\

\subsubsection{Particle Data}

If the preprocessed particle data is found, then the hdf5 particle data files are not needed. However, if the hdf5 files exist, then the system will automatically generate the preprocessed particle data files. Note, this may take a while since the hdf5 files are slow to load.\\

\noindent
You may want to consider sampling the particle data to reduce the size if it's too large, or if you are only interested in a subset of it. In this case, you will need to produce the preprocessed particle data files yourself, and the hdf5 particle files should not be necessary. A conversion tool that can convert from the hdf5 format in our test data is included, and can also be modified to perform sampling during the process.  It is located in \verb|/home/tneuroth/baleen/Preprocessing/| This will also write the files, \verb|xgc.ions.sample.txt, xgc.electrons.sample.txt| which contain the original id's of the particles in the sample/binary output files, as well as the other files needed withing the \verb|Preprocessed/ParticleData| subdirectory.\\

\noindent
The files \verb|info.electrons.txt info.ions.txt| should each look like this,\\

\noindent
\verb|NumParticles NumVariables Variable1 Variable2 ... VarableNV|\\
\verb|262144 9 f0 mu r rho_parallel w0 w1 w2 z zeta|\\

\noindent
\textit{Note that currently the system expects all of these variables to be in the files in this order, but minor changes could be made if this is not possible or convenient.}\\

\noindent
The binary files, \verb|xgc.ions.00002.bin| are in the following format:

\begin{verbatim}
\\values_t is of type float *
variable_value( t, variable_index, particle_index ) 
    = values_t[ variable_index * num_particles + particle_index ]
\end{verbatim}

\noindent
It's written in binary using C++ as follows:

\begin{verbatim}
std::string tstr = std::to_string( t );
std::string path =  this->m_basicDataInfo.location 
                 + "/Preprocessed/ParticleData/" 
                     + "xgc." + p + "." 
                     + std::string( 5 - tstr.size(), '0' ) + tstr  +  ".bin";
std::ofstream outFile( path, std::ios::out | std::ios::binary );
outFile.write( (char *) values_t, num_variables*num_particles * sizeof( float ) );
outFile.close();
\end{verbatim}

\noindent
The particle data at each time step should contain the same set of particles. Before writing the data for each time step, the data should be sorted by id since the system assumes that the index of the particle uniquely identifies the particle id.\\

\subsection{Register the Format and Location}

\noindent
The system automatically searches a "Configuration" folder to see which datasets are available, and the necessary information to open and use them.\\

\noindent
For Pre-generated histograms from \textbf{GTS}, the format should be "GTS\_Distributions", and the type should be "histograms".\\

\noindent
For raw particle from \textbf{XGC}, the format should be "XGC\_Particles", and the type should be "particle". \\

\noindent
\textbf{Execute the following commands:}

\begin{verbatim}
cd \home\tneuroth\baleen\Configuration\DATA
mkdir <dataset identifier>
cd <dataset identifier>
mkdir config
nano data.json
\end{verbatim}

\noindent
\textit{Note, the file must be named data.json.}\\

\noindent
Copy and past this into the file, and change the necessary fields. 

\begin{verbatim}
{
    "name"             : "<dataset identifier>",
    "description"      : "optional description",
    "location"         : "/home/tneuroth/test_data/ITER/",
    "type"             : "particle",
    "format"           : "XGC_Particles",
    "defaultSpatialLayout" : [ "r", "z" ]
}
\end{verbatim}


\section*{Running the Vis Tool Through VNC}

First you will need to make sure your local computer has a vnc viewer. Mac 0SX should have once installed by default which has been verified to work. vncviewer from Tiger VNC has been verified to work as well.\\

\noindent
\textbf{1)} Open two terminals.\\

\noindent
\textbf{2)} In the first terminal

\begin{verbatim}
ssh<username>@cooley.alcf.anl.gov
password: <pin><crypokey>
/home/tneuroth/start_job
<project ID>
<number of minutes you plan to use the tool>
\end{verbatim}

\noindent
After the job starts,

\begin{verbatim}
/home/tneuroth/start_vnc_session
\end{verbatim}

\noindent
The ``start\_vnc\_session" script will then print out the rest of the instructions with the exact commands to enter in the second terminal. It should look like this except with your username and a different job number:

\begin{verbatim}
1) In a local terminal, tunnel to the visualization node:

ssh -L 5900:cooleylogin2:5900 tneuroth@cooley.alcf.anl.gov 

2) start the xserver:

ssh cooleylogin2 export "DISPLAY=:0.0; /home/tneuroth/.vnc/xstartup" 

3) On your local computer, open your prefered vnc viewer application and 
connect to localhost::5900O. Make sure it supports a fall back cursor. 
If vncviewer from TigerVNC:

vncviewer -DotWhenNoCursor=1

4) Within the viewer, start your visualization application

/home/tneuroth/baleen/start

5) Place the tool so that the lower left corner is in the lower left 
corner of the window. Then from the upper right corner of the tool, 
resize to fill the whole screen.
\end{verbatim}

\end{document}
