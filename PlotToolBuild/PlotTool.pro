#-------------------------------------------------
#
# Project created by QtCreator 2017-02-22T12:32:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PlotTool
TEMPLATE = app


SOURCES +=\
        MainWindow.cpp \
    Main.cpp

HEADERS  += MainWindow.hpp

FORMS    += MainWindow.ui
