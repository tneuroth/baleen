#-------------------------------------------------
#
# Project created by QtCreator 2015-03-06T14:29:34
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = BALEEN
TEMPLATE = app

DEFINES += "VIDI_GL_MAJOR_VERSION=4"
DEFINES += "VIDI_CUDA_SUPPORT=1"

LIBS += -L/usr/local/lib
LIBS += -fopenmp -lGLEW
LIBS += -lhdf5
LIBS += -lfreetype
LIBS += -lmpfr -lgmp
LIBS += -lboost_system -lboost_filesystem
LIBS += -lCGAL
LIBS += -L../Code/Shared/VIDI/Tyson/Cuda -lhistiso
#LIBS += -L../Dependencies/local/lib -ladios2

INCLUDEPATH += ../Code/Baleen
INCLUDEPATH += ../Code/Shared/ThirdParty
INCLUDEPATH += ../Code/Shared/VIDI/Tyson
INCLUDEPATH += /opt/cuda/include
INCLUDEPATH += /usr/include/freetype2
INCLUDEPATH += ../Dependencies/local/include

QMAKE_CC = gcc
QMAKE_CXX = g++
QMAKE_LINK = g++

QMAKE_CXXFLAGS += -std=c++11 -fopenmp -frounding-math #-ftime-report -ftree-vectorizer-verbose=5

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O0
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O3
QMAKE_CXXFLAGS_RELEASE += -O3

SOURCES += \
    ../Code/Baleen/BALEEN.cpp \
    ../Code/Baleen/Main.cpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.cpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressonwrapper.cpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/particledatamanager.cpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/CubeMarcher.cpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.cpp \
    ../Code/Baleen/Dialogues/ROIDialog.cpp

HEADERS += \
    ../Code/Baleen/BALEEN.hpp \
    ../Code/Baleen/Dialogues/AdvancedParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/EditGridProjectionDialogue.hpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimeSeriesDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/SavePresetDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/S3DRawDownSampleImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Types/Vec.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Widget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ComboWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/HelpLabelWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/PressButtonWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ResizeWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/SliderWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/TimeLineWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WeightHistogramWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/BoxShadowFrame.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/ViewPort.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/SliderWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler/SamplerPanel.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler/SamplerPanelWidgets.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler/SamplerWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/DynamicGridLayout.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/RecurrencePlot.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/RecurrenceView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/SimpleTimeSeriesPlot.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WarningWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ControlPanelWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderParams.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/Expression.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressionsymbols.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/ExpressionWrapper.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParticleBasedConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/DistributionBasedConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BaseConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ProjectConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BasicDataInfo.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParameterKeys.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/Attributes.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/HistogramDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/LinkedViewDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/ParticleFilter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/TimeSeriesDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DistributionDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/MetaData.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/ParticleDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSSingleImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/SLACImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCElectron.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCHybrid.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/S3DRawImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/S3DRawDownSampleImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldInterpolator.h \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/ParticleInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldLineInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/TokamakInterpolate.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/TriangleMap.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DataSetManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DistributionGridManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/ParticleDataManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/VisualContextModel.hpp \
    ../Code/Shared/VIDI/Tyson/Data/MetadataParsers/ProjectParser.hpp \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/GridSmoother.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GenerateGeometry.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GradientUncertaintyCalculator.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/Contour.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/CubeMarcher.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/Util.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderObject.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/AORenderScene.hpp \
    ../Code/Baleen/FrVolumeCache.hpp \
    ../Code/Baleen/TimePlotCache.hpp \
    ../Code/Baleen/TimeSeriesAggregator.hpp \
    ../Code/Baleen/PhasePlotCache.hpp \
    ../Code/Baleen/Dialogues/ColorMapDialog.hpp \
    ../Code/Baleen/Dialogues/DialogHelpers.hpp \
    ../Code/Shared/VIDI/Tyson/Data/PhasePath.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/ROIDefinition.hpp \
    ../Code/Baleen/Dialogues/ROIDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Summary/Summary.hpp

OTHER_FILES += \
    ../../Cuda/* \
    ../Platform/GL3/shaders/* \
    ../Platform/GL4/shaders/* \
    ../Platform/GL4/shaders/AO/*

FORMS += ../Code/Baleen/MainWindow.ui

DISTFILES += \
    ../Code/Shared/VIDI/Tyson/Cuda/libhistiso.so \
    ../Code/Shared/VIDI/Tyson/Cuda/compile.sh \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.cu \
    GL4/shaders/LineMap.geom \
    GL4/shaders/LineMap.vert \
    GL4/shaders/LineMap.frag \
    GL4/shaders/ColorLegend.frag \
    GL4/shaders/triangulationmap.vert \
    GL4/shaders/triangulationmap.frag \
    GL4/shaders/triangulationmap.geom

