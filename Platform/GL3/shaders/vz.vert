#version 130



in vec3 posAttr;
in vec3 velAttr;

out vec3 col;
out float op;

uniform mat4 MVP;
uniform float mx;
uniform int mode;

void main(void)
{

    // if ( mode == 0 ) {
    //     col = vec3( abs( velAttr.x ) / 2.6, 0.0, 1.0 - abs( velAttr.x ) / 2.6 );
    // }
    // else {
    float mag = length( velAttr.xy ) / mx;

    vec3 c1 = vec3( 1.0, 1.0, 182.0 / 255.0 );
    vec3 c2 = vec3( 107.0 / 255.0, 39.0/ 255.0, 0.0 );

    col = velAttr;//mag*c1 + (1-mag)*c2;

    op = posAttr.z;
    // }

    gl_Position = MVP * vec4(
                      posAttr.x,
                      posAttr.y,
                      0.0,
                      1.0
                  );
}
