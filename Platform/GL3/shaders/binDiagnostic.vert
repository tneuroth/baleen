#version 130



in float xAttr;
in float yAttr;

void main(void)
{
    gl_Position = vec4( xAttr, yAttr, 0, 1.0 );
}
