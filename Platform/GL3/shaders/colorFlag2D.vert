#version 130



in float xAttr;
in float yAttr;
in float valAttr;
in float flagAttr;
in float realAttr;

out float real;
out float alpha;
out float highlight;
out float val;

uniform mat4 MVP;

void main(void)
{
    real = realAttr;
    val = valAttr;

    if( ( int( flagAttr ) & 1 ) == 0 )
    {
        alpha = 0;
    }
    else
    {
        alpha = 1;
    }

    if( ( int( flagAttr ) & 4 ) != 0 && ( int( flagAttr ) & 1 ) != 0  )
    {
        highlight = 1;
    }
    else if( ( int( flagAttr ) & 8 ) != 0 && ( int( flagAttr ) & 1 ) != 0)
    {
        highlight = 2;
    }
    else if( ( int( flagAttr ) & 2 ) != 0 && ( int( flagAttr ) & 1 ) != 0)
    {
        highlight = 3;
    }
    else
    {
        highlight = 0;
    }

    gl_Position = MVP * vec4(
                      xAttr,
                      yAttr,
                      0.0,
                      1.0
                  );
}
