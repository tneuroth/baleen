#version 130



in vec3 normal;
in vec3 fragPos;

out vec4 fragColor;

const vec3 l_pos = vec3( 0.0, 0, -10);
const vec4 material = vec4(.00, 0.0, 0.5, 1.0);

vec3 lambertian(vec3 e_n, vec3 e_pos)
{
    vec3 L = normalize(l_pos - e_pos);
    vec3 E = normalize(-e_pos); // we are in Eye Coordinates, so EyePos is (0,0,0)
    vec3 R = normalize(reflect(L, e_n));

    //calculate Ambient Term:
    vec3 Iamb = vec3(material.x);

    //calculate Diffuse Term:
    vec3 Idiff = vec3(material.y * max(dot(e_n, L), 0.0));
    Idiff = clamp(Idiff, 0.0, 1.0);

    // calculate Specular Term:
    vec3 Ispec = vec3(material.z * pow(max(dot(R, E), 0.0), material.w));
    Ispec = clamp(Ispec, 0.0, 1.0);

    // write Total Color:
    return Iamb + Idiff + Ispec;
}

void main(void)
{
    vec3 lambert = lambertian( normalize( normal ), fragPos );
    fragColor = vec4( lambert, 1 );
}
