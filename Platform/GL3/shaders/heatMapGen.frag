#version 130



out float freq;

const float unit = 1.0 / 1048576.0;

in float w;

void main(void)
{
    freq = unit * w;
}
