#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;

uniform float mx;
uniform int blurDir;


void main()
{


    int horizontalPass = blurDir;
    float sigma = 3;
    float pi = 3.14159265359;
    float texOffset = 1 / 1024.0;

    vec2 blurMultiplyVec = 0 < horizontalPass ? vec2( 1.0, 0.0 ) : vec2( 0.0, 1.0 );

    // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
    vec3 incrementalGaussian;
    incrementalGaussian.x = 1.0 / (sqrt(2.0 * pi) * sigma);
    incrementalGaussian.y = exp(-0.5 / (sigma * sigma));
    incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

    // Take the central sample first...
    float count = ( texture( tex, uv.st ).r / mx  ) * incrementalGaussian.x;
    if( count == 0  )
    {
        color = vec4( 0, 0, 0, 0 );
    }
    else
    {

        float coefficientSum = count;
        coefficientSum += incrementalGaussian.x;
        incrementalGaussian.xy *= incrementalGaussian.yz;

        for (float i = 1.0; i <= 12; i++)
        {

            count += ( texture( tex, uv.st - i * texOffset * blurMultiplyVec).r / mx ) * incrementalGaussian.x;
            count += ( texture( tex, uv.st + i * texOffset * blurMultiplyVec).r / mx ) * incrementalGaussian.x;

            coefficientSum += 2.0 * incrementalGaussian.x;
            incrementalGaussian.xy *= incrementalGaussian.yz;
        }
        color = vec4( texture( tf, count ).xyz, 1.0 );
    }
}
