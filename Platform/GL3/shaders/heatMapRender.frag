#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;
uniform float mx;
uniform bool fade;

void main()
{

    // Take the central sample first...
    float count = texture( tex, uv.st ).r / mx;
    if( count <= 0 )
    {
        color = vec4( 0, 0, 0, 0 );
    }
    else
    {
        if( fade )
        {
            color = vec4( count, count, count, .4 );
        }
        else
        {
            color = vec4( count, count, count, .8 );
        }
    }
}
