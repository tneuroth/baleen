#-------------------------------------------------
#
# Project created by QtCreator 2015-03-06T14:29:34
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = BALEEN
TEMPLATE = app

#INCLUDEPATH += /usr/include/opencv2 
INCLUDEPATH += /usr/include/freetype2 

DEFINES += "VIDI_GL_MAJOR_VERSION=4"
DEFINES += "VIDI_CUDA_SUPPORT=1"

LIBS +=  -fopenmp 
LIBS += -L/home/tneuroth/local -lGLEW
LIBS += -Wl,-rpath,/home/tneuroth/local -L/home/tneuroth/local -lhdf5
LIBS += -lfreetype
LIBS += -L/home/tneuroth/local -lCGAL
LIBS += -L/home/tneuroth/local -lmpfr 
LIBS += -lgmp
LIBS += -L/home/tneuroth/local -lboost_system -L/home/tneuroth/local -lboost_filesystem
LIBS += -L/home/tneuroth/local -lhistiso

INCLUDEPATH += ../Code/Baleen
INCLUDEPATH += ../Code/Shared/ThirdParty
INCLUDEPATH += ../Code/Shared/VIDI/Tyson
INCLUDEPATH += /home/tneuroth/dev/libs/boost/include

INCLUDEPATH += /soft/visualization/cuda-8.0.61/include
INCLUDEPATH += /home/tneuroth/dev/libs/include
INCLUDEPATH += /home/tneuroth/dev/libs/cgal/include
INCLUDEPATH += /home/tneuroth/dev/glew-2.1.0/include

QMAKE_CXXFLAGS += -std=c++11 -fopenmp -frounding-math #-ftime-report

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O0
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O3

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_CXXFLAGS_RELEASE += -ftree-vectorizer-verbose=5

SOURCES += \
    ../Code/Baleen/BALEEN.cpp \
    ../Code/Baleen/Main.cpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.cpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressonwrapper.cpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/particledatamanager.cpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/CubeMarcher.cpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.cpp

HEADERS += \
    ../Code/Baleen/BALEEN.hpp \
    ../Code/Baleen/Dialogues/AdvancedParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/EditGridProjectionDialogue.hpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimeSeriesDefinitionDialogue.hpp \
    ../Code/Shared/VIDI/Tyson/Types/Vec.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ComboWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/HelpLabelWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/PressButtonWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ResizeWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/SliderWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/TimeLineWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WeightHistogramWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Widget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/BoxShadowFrame.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/ViewPort.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderParams.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/Expression.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressionsymbols.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/ExpressionWrapper.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/Attributes.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/HistogramDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/LinkedViewDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/ParticleFilter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/TimeSeriesDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DistributionDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/MetaData.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/ParticleDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/CombImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSSingleImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/SLACImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCElectron.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldInterpolator.h \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/ParticleInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DataSetManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DistributionGridManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/ParticleDataManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/VisualContextModel.hpp \
    ../Code/Shared/VIDI/Tyson/Data/MetadataParsers/ProjectParser.hpp \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GenerateGeometry.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GradientUncertaintyCalculator.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/Contour.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/CubeMarcher.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/Util.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WarningWidget.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParticleBasedConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/DistributionBasedConfiguration.hpp \
    ../Code/Baleen/Dialogues/SavePresetDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BaseConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ProjectConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BasicDataInfo.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParameterKeys.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderObject.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/AORenderScene.hpp \
    ../Code/Baleen/FrVolumeCache.hpp \
    ../Code/Baleen/TimePlotCache.hpp \
    ../Code/Baleen/TimeSeriesAggregator.hpp \
    ../Code/Baleen/PhasePlotCache.hpp \
    ../Code/Baleen/Dialogues/ColorMapDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldLineInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/TokamakInterpolate.hpp

OTHER_FILES += \
    ../../Cuda/* \
    ../Platform/GL3/shaders/* \
    ../Platform/GL4/shaders/* \
    ../Platform/GL4/shaders/AO/*

FORMS += ../Code/Baleen/MainWindow.ui

DISTFILES += \
    ../Code/Shared/VIDI/Tyson/Cuda/libhistiso.so \
    ../Code/Shared/VIDI/Tyson/Cuda/compile.sh \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.cu \
    GL4/shaders/LineMap.geom \
    GL4/shaders/LineMap.vert \
    GL4/shaders/LineMap.frag \
    GL4/shaders/ColorLegend.frag

