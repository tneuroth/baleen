#version 130



uniform vec4 color;

in float alpha;
in float highlight;
in float val;
in float real;

out vec4 fragColor;

void main(void)
{

    float g = 1;
    float a;
    vec3 rgb;
    if( alpha == 0 )
    {
        a = alpha;
    }
    else
    {
        a = .1;
    }
    if( highlight == 0 )
    {
        rgb = color.rgb;
        rgb = vec3( 0.5, 0, 1 );
        g = 0;
    }
    else if( highlight == 1 )
    {
        rgb = vec3(1.0, 0, 0);
        g = 0.2;
        a = .3;
    }
    else if( highlight == 2 )
    {
        rgb = vec3(0, .3, 1);
        g = val / 15;
        a = .3;
        if( val > 15 )
        {
            g = 1;
            rgb = vec3( 0.2, 0, 0 );
            a = 0.9;
        }
    }
    else if( highlight == 3 )
    {
        rgb = vec3(.85, .64, .125);
        a = .2;
        g = .64;
    }
    rgb.g = g;

    if( real == 0 || highlight == 2 || highlight == 0 )
    {
        a = 0;
    }

    fragColor = vec4( rgb, a );
}
