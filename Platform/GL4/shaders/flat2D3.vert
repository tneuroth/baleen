#version 130



in vec3 posAttr;
in vec3 colAttr;

out vec3 col;

uniform mat4 MVP;

void main(void)
{
    col = colAttr;

    gl_Position = MVP * vec4(
                      posAttr.x,
                      posAttr.y,
                      0.0,
                      1.0
                  );
}
