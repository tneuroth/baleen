#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D density;
uniform sampler2D bump;
uniform sampler1D tf;
uniform bool showPtcl;
uniform bool showDensity;

void main()
{

    float d = texture( density, uv ).r;
    float s = texture( bump, uv ).r;
    if( d <= 0 )
    {
        color = vec4( 0, 0, 0, 0 );
    }
    else
    {
        if( showDensity && ( ! showPtcl ) )
        {
            color = vec4( texture( tf, d ).xyz, 1 );
        }
        else if( showPtcl && showDensity )
        {
            vec4 c = vec4( texture( tf, d ).xyz, 1 );
            color = c + vec4( s, s, s, 0 );
        }
        else if( showPtcl )
        {
            color = vec4( vec3( s, s, s )*3, s*2 );
        }
    }
}
