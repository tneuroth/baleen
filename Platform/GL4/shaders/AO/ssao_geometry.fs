#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in G2F{
    vec3 e_n;
    vec3 e_pos;
}g2f;

uniform vec3 cl;

void main()
{    
    // store the fragment position vector in the first gbuffer texture
    gPosition = g2f.e_pos;
    // also store the per-fragment normals into the gbuffer
    gNormal = normalize( g2f.e_n );
    // and the diffuse per-fragment color
    gAlbedo.rgb = cl;
}
