#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;
uniform float mx;
uniform bool fade;
uniform bool outline;
uniform bool onlyOutline;

uniform float inverseTexWidth;
uniform float inverseTexHeight;

void main()
{

    float f = texture( tex, uv.st ).r;
    vec3 cl = texture( tf, f / mx ).rgb;
    float closestNonZero = 10000;

    const int D = 6;
    float maxLength = sqrt( D*D / 2 );

    for( float r = -D/2; r <= D/2; ++r )
    {
        for( float c = -D/2; c <= D/2; ++c )
        {
            float v = texture( tex, vec2( uv.x+inverseTexWidth*r, uv.y+inverseTexHeight*c ) ).r;
            if( v != 0 )
            {
                closestNonZero = min( length( vec2( abs( r ), abs( c ) ) ), closestNonZero );
            }
        }
    }

    // it's an empty bin close to a non-empty bin
    if( fade && outline && closestNonZero > 0 && closestNonZero < 2*D )
    {
        color = vec4( .3, .3, .3, ( 1.0 - closestNonZero / maxLength ) );
    }
    else if( closestNonZero == 0 && ! onlyOutline )
    {
        if( fade )
        {
            //color = vec4( .1, .1, .1, 1 );
            float c = ( texture( tf, f / mx ).r + texture( tf, f / mx ).g + texture( tf, f / mx ).b ) / 3.0;
            color = vec4( c, c, c, 0.5 );

        }
        else
        {
            color = texture( tf, f / mx );
        }
    }
    else if( closestNonZero == 0 )
    {
        color = vec4( 1, 1, 1, 0.4 );
    }
    else
    {
        color = vec4( 0, 0, 0, 0 );
    }

}
