#version 130



in vec2 posAttr;
out vec3 col;

uniform mat4 MVP;

void main(void)
{

    col = vec3( 0, 0, 0 );

    gl_Position =

        MVP * vec4(
            posAttr.x,
            posAttr.y,
            0.0,
            1.0
        );
}
