#version 400



#define PIo2 1.57079632679

layout(lines) in;
layout(line_strip, max_vertices=2) out;

uniform sampler1D angleToNormalization;

uniform float width;
uniform float height;
uniform bool distanceWeighted;

out G2F
{
    float normalization;
    out vec2 start;
    out vec2 end;
} g2f;

void main( void )
{
    float distX = abs( gl_in[1].gl_Position.x - gl_in[0].gl_Position.x ) * width;
    float distY = abs( gl_in[1].gl_Position.y - gl_in[0].gl_Position.y ) * height;

    vec2 a = normalize( vec2( distX, distY ) );
    vec2 b = vec2( 1, 0 );
    float angle = acos( dot( a, b ) );

    float norm = texture( angleToNormalization, angle / PIo2 ).r;
    if( distanceWeighted )
    {
        float dist = max( length( vec2( distX, distY ) ), 1 );
        norm *= dist;
    }

    g2f.start = gl_in[0].gl_Position.xy;
    g2f.end = gl_in[1].gl_Position.xy;

    g2f.normalization = norm;

    gl_Position = gl_in[0].gl_Position;
    EmitVertex();
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    EndPrimitive();
}
