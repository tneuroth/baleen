#version 130



in float xAttr;
in float yAttr;
in float vAttr;

uniform mat4 MVP;

out float val;

void main(void)
{
    gl_Position = MVP * vec4(
                      xAttr,
                      yAttr,
                      0.0,
                      1.0
                  );
    val = vAttr;
}
