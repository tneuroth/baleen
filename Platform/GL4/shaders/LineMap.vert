#version 400



layout(location = 0) in float xAttr;
layout(location = 1) in float yAttr;

uniform mat4 M;

void main(void)
{
    gl_Position = M * vec4( xAttr, yAttr, 0.0, 1.0 );
}
