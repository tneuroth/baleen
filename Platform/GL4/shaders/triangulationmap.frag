#version 130



in  float val;
out vec4 fragColor;

uniform float mn;
uniform float mx;
uniform bool zeroCentered;
uniform float scale;
uniform bool signOnly;

uniform sampler1D tf;

void main()
{
    float r;
    if( zeroCentered )
    {
        float absmax = max( abs( mx ), abs( mn ) );
        r = scale*val / ( 2*absmax ) + 0.5;
    }
    else
    {
        float width = mx - mn;
        r = scale*val / width;
    }

    r = max( r, 0 );
    r = min( r, 1 );

    if( signOnly )
    {
        if( r < 0.5 )
        {
            r = 0;
        }
        else if( r > 0.5 )
        {
            r = 1;
        }
    }

    fragColor = texture( tf, r );
}
