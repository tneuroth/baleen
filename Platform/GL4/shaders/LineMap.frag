#version 400



layout(location = 0) out float freq;

const float unit = 1.0 / 67108864.0;

uniform float width;
uniform float height;

uniform bool distanceWeighted;

in G2F
{
    float normalization;
    vec2 start;
    vec2 end;
} g2f;

void main(void)
{
    float norm = g2f.normalization;

    if( ! distanceWeighted )
    {
        const float fragUnit = 1.41421356237;
        vec2 myFrag = floor( gl_FragCoord.xy );

        vec2 startFrag = vec2(
                             ( ( g2f.start.x + 1 ) *  width - 1 ) / 2.0,
                             ( ( g2f.start.y + 1 ) * height - 1 ) / 2.0
                         );

        vec2 endFrag = vec2(
                           ( ( g2f.end.x + 1 ) *  width - 1 ) / 2.0,
                           ( ( g2f.end.y + 1 ) * height - 1 ) / 2.0
                       );

        if( false && ! distanceWeighted && ( myFrag == floor( startFrag ) && myFrag == floor( endFrag ) ) )
        {
            freq = length( endFrag - startFrag ) * ( unit ) / norm;
        }
        else if( ! distanceWeighted )
        {
            freq = unit / norm;
        }
    }
    else
    {
        freq = unit / norm;
    }
}
