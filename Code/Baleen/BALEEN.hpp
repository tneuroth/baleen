

#ifndef BALEEN_H
#define BALEEN_H

#include "OpenGL/Qt/Windows/RenderWindow.hpp"
#include "OpenGL/Rendering/AORenderScene.hpp"
#include "OpenGL/Rendering/Texture.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/Sampler.hpp"
#include "OpenGL/Widgets/TimeLineWidget.hpp"
#include "OpenGL/Widgets/ResizeWidget.hpp"
#include "OpenGL/Widgets/WeightHistogramWidget.hpp"
#include "OpenGL/Widgets/HelpLabelWidget.hpp"
#include "OpenGL/Widgets/Viewport/BoxShadowFrame.hpp"
#include "OpenGL/Widgets/Sampler/SamplerWidget.hpp"
#include "Types/Vec.hpp"
#include "OpenGL/Widgets/WarningWidget.hpp"
#include "OpenGL/Widgets/Viewport/ViewPort.hpp"
#include "OpenGL/Rendering/Cameras/Camera2D.hpp"
#include "OpenGL/Rendering/Cameras/TrackBallCamera.hpp"
#include "OpenGL/Rendering/RenderParams.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Cuda/HistogramStackIso.hpp"
#include "Data/Configuration/BaseConfiguration.hpp"
#include "Data/Configuration/ProjectConfiguration.hpp"
#include "Dialogues/SavePresetDialog.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"
#include "FrVolumeCache.hpp"
#include "TimePlotCache.hpp"
#include "PhasePlotCache.hpp"
#include "Dialogues/ColorMapDialog.hpp"

#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <array>
#include <atomic>

class QKeyEvent;
class QMouseEvent;
class QGLFramebufferObject;
class QOpenGLShaderProgram;

namespace TN
{

template< class T >
class DataSetManager;

class CubeMarcher;
class LinkedViewDefinition;
class EditGridProjectionDialogue;
class HistogramDefinitionDialogue;
class PhasePlotDefinitionDialogue;
class TimePlotDefinitionDialogue;
class TimeSeriesDefinitionDialogue;
class ParticleFilterDialogue;
class ROIDialog;
class ColorMapDialog;
class ROIDefinition;

class Renderer;

enum class Theme
{
    White,
    Gray,
    Dark
};

class BALEEN : public RenderWindow
{
    Q_OBJECT

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const std::string RELATIVE_PATH_PREFIX = "../";

    // Data

    std::unique_ptr< DataSetManager<float> > m_dataSetManager;
    int m_populationDataTypeEnum;

    int m_selectedTimeStep;
    int m_numTimeSteps;


    // What's needed to find the data on disk, to know which importer to use, and to know where it came from
    // The importer is reposonsible for knowing details about what's in the data and how to load it
    BasicDataInfo m_basicDataInfo;

    std::pair< Vec2< float >, Vec2< float > > m_contextBoundingBox;

    HistogramStackIso m_histStackIso;

    void updatePartType();

    // configurations for loading and saving, associated with a dataset

    /// Dataset name->( preset name, preset )
    std::map< std::string, std::map< std::string, ProjectConfiguration > > m_savedConfigurations;

    void queryConfigurations();
    void afterSelectedDataset();
    void afterSelectedPreset();

    void reQueryPresets();
    void datasetSwitched();
    void presetSwitched();

    // State

    enum
    {
        LOGGING_DISABLED,
        LOG_LEVEL_1
    };

    int m_logLevel;
    void writeLogs( int level );
    void pushState();
    bool m_trackState;
    void synchronizeConfiguration();
    std::string m_logDirectory;
    std::string m_timeStamp;
    std::vector< std::pair< std::string, std::string > > m_loadingLogs;

    bool m_savedPreset;
    bool m_loadNewPreset;

    bool m_datasetSelected;
    bool m_presetSelected;
    bool m_timeSeriesDefined;
    bool m_histogramDefined;
    bool m_sessionInitiated;

    QPointF m_previousMousePosition;
    bool m_mouseIsPressed;

    bool m_haveInitializedOpenGL;
    bool m_haveInitializedData;
    bool m_recalculateFullTimePlot;
    bool m_recalculatePhasePlot;

    bool m_recalculateIsoVolume;
    bool m_recalculateIsoMesh;

    Vec2< float > m_upperLeftWindowCornerWorldSpace;
    Vec2< float > m_upperRightWindowCornerWorldSpace;
    Vec2< float > m_lowerLeftWindowCornerWorldSpace;
    Vec2< float > m_lowerRightWindowCornerWorldSpace;

    Vec2< float > m_miniMapSizeWorldSpace;

    bool m_uiLayoutEdited;
    bool m_uiLayoutInitialized;
    bool m_histDefinitionEdited;
    bool m_filterEdited;
    bool m_reloadGPUData;
    bool m_dataInitializedFirst;
    bool m_updatedGPUData;

    bool m_forceSpacePartitioningSelection;

    std::string m_previousFilter;

    bool m_histDialogOpen;
    bool m_timePlotDialogOpen;
    bool m_phasePlotDialogOpen;
    bool m_timeSeriesDialogOpen;
    bool m_filterDialogOpen;
    bool m_saveDialogOpen;
    bool m_roiDialogOpen;

    void clearProject();

    ////////////////////////////////////////

    // Phase Plot Textures

    std::vector< float > m_pixelDensityMap;
    float m_phasePlotBufferMax;
    std::vector< float > m_phasePlotPixelBuffer;
    Texture2D1 m_phasePlotTexture;

    Texture1D3  m_triangulationTF;
    Texture1D3  m_phasePlotTFAll;
    Texture1D3  m_phasePlotTFRegion;
    Texture1D3  m_phasePlotTFRegionBin;
    Texture1D3  m_phasePlotTFWeightBin;
    std::vector< TN::Vec3< float > > m_histTF;

    QGLFramebufferObject *m_fbo;
    const int TF_SIZE = 256;

    void loadTF( const std::string & path, Texture1D3 & tex );
    void loadTF( const std::string & path, std::vector< TN::Vec3< float > > & tf );

    ////////////////////////////////////////

    // Dialogues and Definitions

    void cleanDialogues();

    // Dialogs

    ColorMapDialog m_histColorMapDialog;
    ColorMapDialog m_plotColorMapDialog;

    SavePresetDialog m_savePresetDialog;

    std::map< std::string, std::map< std::string, ROIDialog * > >
    m_roiDialogs;

    std::map< std::string, std::map< std::string, PhasePlotDefinitionDialogue * > >
    m_phasePlotDefinitionDialogues;

    std::map< std::string, std::map< std::string, TimePlotDefinitionDialogue * > >
    m_timePlotDefinitionDialogues;

    std::map< std::string, std::map< std::string, HistogramDefinitionDialogue * > >
    m_histogramDefinitionDialogues;

//    std::map< std::string, std::map< std::string, EditGridProjectionDialogue * > >
//    m_gridProjectionDialogues;

    std::map< std::string, std::map< std::string, TimeSeriesDefinitionDialogue * > >
    m_timeSeriesDefinitionDialogues;

    std::map< std::string, std::map< std::string, ParticleFilterDialogue * > >
    m_particleFilterDialogues;

    std::unique_ptr< EditGridProjectionDialogue > m_spatialPartitioningDialog;

    // Definitions
    // need to encapsulate into configuration structure

    ProjectConfiguration m_currentConfigurations;

//    std::map< std::string, std::map< std::string, TimeSeriesDefinition > >
//    m_timeSeriesDefinitions;

//    std::map< std::string, std::map< std::string, HistogramDefinition > >
//    m_userDefinedHistograms;

//    std::map< std::string, std::map< std::string, PhasePlotDefinition > >
//    m_phasePlotDefinitions;

//    std::map< std::string, std::map< std::string, PredefinedHistogram > >
//    m_predefinedHistograms;

//    std::map< std::string, std::map< std::string, TimePlotDefinition > >
//    m_timePlotDefinitions;

//    std::map< std::string, std::map< std::string, ParticleFilter > >
//    m_particleFilters;

    ///////////////////////////////////////////////////////////////
    // Histograms & Contours

    TimePlotCache m_timePlotCache;
    FrVolumeCache m_frVolumeCache;
    PhasePlotCache m_phasePlotCache;

    double m_maxFrequencyInSelectedTS;
    std::map< int, double > m_localMaxFrequenciesInSelectedTS;

    std::map< int, double > m_gridMergingNormalizationTerm;

    float m_histogramStackRotation;
    float m_histogramStackRotationY;

    //GradientUncertaintyCalculator< float > m_gradUncertCalc;
    std::unique_ptr< CubeMarcher > m_marchCubes[2];

    int m_selectedHistogramMode;
    float m_selectedHistogramRotation;
    float m_selectedHistogramRotationY;

    std::vector< Vec2< float > > m_histogramLayoutLines;
    std::vector< Vec2< float > > m_nonEmptyHistLayoutHighlights;
    std::vector< Vec2< float > > m_selectedHistGridLines;
    std::vector< double > m_currentContourLevels;

    std::vector< Vec2< float > > m_selectedHistogramVertices;
    std::vector< Vec3< float > > m_selectedHistogramColors;

    std::vector< Vec2< float > > m_histogramGridVertices;
    std::vector< Vec3< float > > m_histogramGridColors;

    std::map< int, std::vector< double > > m_mergedGridPointHists;
    std::vector< Vec2< float > > m_colorLegendVerts;
    std::vector< Vec3< float > > m_colorLegendColors;
    const std::string m_colorLegendLabel = "Density";

    ///////////////////////////////////////

    // GPU binning

    // Eventually cacheing the raw particle data should be depreciated,
    // we need it currently for extra performance
    // but will not if we redesign the data manager to store data in full
    // contiguous blocks per variable in p,t order (which is a complicated change currently)

    bool m_phaseTrajectoriesCached;

    // These two will be static and reused for caching phase trajectories in 2 variables
    std::vector< float > m_x0Buffer;
    std::vector< float > m_x1Buffer;

    // these can be reused for all plots
    std::vector< GLint > m_phaseBufferFirsts;

    std::vector<GLsizei> m_phaseBufferCounts;
    std::vector<GLsizei> m_phaseBufferCountsRegion;
    std::vector<GLsizei> m_phaseBufferCountsBin;

    std::unique_ptr< QOpenGLBuffer > m_phaseVBO;
    std::unique_ptr< QOpenGLVertexArrayObject > m_phaseVAO;

    ///////////////////////////////////////////////////////////////

    // UI dynamics, Widgets & Viewports

    const double EQUATION_HEIGHT = 45;
    const double MENU_HEIGHT   = 28;
    const double MENU_SPACING  = 4;
    const double BUTTON_HEIGHT = 20; //( MENU_HEIGHT - MENU_SPACING * 2 );

    const double TEXT_ROW_HEIGHT  = 25.0;
    const double LABEL_ROW_HEIGHT = 32.0;
    const double DIVIDER_WIDTH    = 0.0;
    const double BUTTON_SPACING   = 2.0;

    const double RESIZE_HANDLE_WIDTH = 14;    

    const double SAMPLER_MIN_WIDTH    = 1000.0;
    const double SAMPLER_MIN_HEIGHT   =  500.0;
    const double PHASEVIEW_MIN_HEIGHT =  300.0;
    const double PHASEVIEW_MIN_WIDTH  =  300.0;
    const double DETAILVIEW_MIN_WIDTH =  200.0;
    const double DETAIVIEW_MAX_WIDTH  =  650.0;
    const double TIMELINE_MIN_HEIGHT  =  100.0;
    const double ISOVIEW_MIN_HEIGHT   =  50.0;
    const double MINIMAP_MIN_HEIGHT   =  210.0;

    const int PANEL_HT    = 76;
    const int HIST_PAD    = 40;
    const int HIST_MARGIN = 4;

    ResizeWidget m_resizeWidgetA;
    ResizeWidget m_resizeWidgetB;
    ResizeWidget m_resizeWidgetC;
    ResizeWidget m_resizeWidgetD;

    /*
    ------------------------
    |    |          |      |
    |    |A         |B     |
    |    |____C_____|______|
    |    |_______D_________|
    |    | ________________|
    ------------------------
    */

    TN::SamplerWidget m_samplerWidget;

    Sampler< Vec3< float >, Vec3< float > > m_sampler;

    Widget m_dataSetInfoWidget;
    WeightHistogramWidget m_binWeightHistWidget;
    WeightHistogramWidget m_regionWeightHistWidget;
    WeightHistogramWidget m_allWeightHistWidget;

    ViewPort m_miniMapViewPort;
    ViewPort m_histogramViewPort;
    ViewPort m_timeHistIsoViewport;
    ViewPort m_samplingViewPort;
    ViewPort m_auxViewPort;
    ViewPort m_timeLinePlotViewPort;
    ViewPort m_dataInfoPanelViewPort;

    TimeLineWidget m_timeLineWidget;

    TexturedPressButton m_timeAggregateRegionEquation;
    TexturedPressButton m_timeAggregateAllEquation;
    TexturedPressButton m_timeAggregateEquationDefinitionsAll;
    TexturedPressButton m_timeAggregateEquationDefinitions;

    TexturedPressButton m_phaseEquationAll;
    TexturedPressButton m_phaseEquationRegion;
    TexturedPressButton m_phaseEquationBin;

    PressButton m_saveProjectButton;
    PressButton m_loadProjectFileButton;

    PressButton m_saveButton;
    PressButton m_closeButton;
    PressButton m_initiateSessionButton;

    ComboWidget m_datasetCombo;
    ComboWidget m_configurationCombo;
    ComboWidget m_particleCombo;

    ComboWidget m_phasePlotBlendingCombo;
    ComboWidget m_phasePlotInterpolationCombo;
    ComboWidget m_phasePlotModeCombo;
    ComboWidget m_phasePlotCombo;

    ComboWidget m_aggregationCombo;
    ComboWidget m_distSeriesModeCombo;

    PressButton m_definePhasePlotButton;
    PressButton m_phasePlotAllSelector;
    PressButton m_phasePlotSelectionSelector;

    PressButton m_defineTimeSeriesButton;
    PressButton m_definePlotButton;

    ComboWidget m_timeSeriesCombo;

    ComboWidget m_timePlotCombo;
    ComboWidget m_timePlotModeCombo;

    PressButton m_editTimePlotButton;
    PressButton m_editPhasePlotButton;
    PressButton m_editTimeSeriesButton;

    PressButton m_timePlotBothSelector;
    PressButton m_timePlotHistogramSelector;
    PressButton m_timePlotAllSelector;

    PressButton m_selectedHist2DButton;
    PressButton m_selectedHist3DButton;

    PressButton m_negPosScalarButton;
    SliderWidget m_gridColorScaleSlider;
    SliderWidget m_smoothRadiusSlider;
    SliderWidget m_isoValueSlider;

    std::vector< ComboWidget * > m_comboRefs;

    HelpLabelWidget m_helpButtonBinning;
    HelpLabelWidget m_helpButtonData;
    HelpLabelWidget m_helpButtonLinkedView;
    HelpLabelWidget m_helpButtonIsoView;
    HelpLabelWidget m_helpButtonTimeline;
    HelpLabelWidget m_helpButtonDetailView;
    HelpLabelWidget m_helpButtonMiniMap;
    std::vector< HelpLabelWidget * > m_helpLabelWidgetRefs;

    PressButton m_defineFilterButton;
    PressButton m_editFilterButton;
    ComboWidget m_filterCombo;

    WarningWidget m_warningWidget;
    PressButton m_inspectWarningsButton;
    ComboWidget m_errorRemovalCombo;

    ///////////////////////////////////////////////////////
    // Rendering

    // Iso View

    AORenderScene m_isoAORenderScene;

    RenderObject m_isoRenderObject;
    RenderObject m_isoBoundBox;
    RenderObject m_isoTimeStepIndicator;
    RenderObject m_isoDirectionIndicator;

    /////////////////////////////////////

    // Selected Hist 3D

    AORenderScene m_hist2DAORenderScene;

    RenderObject m_selectedHist3DMesh;

    /////////////////////////////////////

    std::unique_ptr< Renderer > m_renderer;

    TrackBallCam isoCam;
    TrackBallCam detCam;
    Camera2D camera;
    QMatrix4x4 m_translation;
    TrackBallCam auxViewCamera;
    TrackBallCam timeLineViewCamera;

    Light m_light1;
    Material m_material1;

    Light m_isoLight;
    Material m_isoMaterial;

    // divergent color map points
    Vec3< float > color1;
    Vec3< float > color2;
    Vec3< float > color3;
    Vec3< float > color4;
    Vec3< float > color5;
    Vec3< float > colorn2;
    Vec3< float > colorn3;
    Vec3< float > colorn4;
    Vec3< float > colorn5;

    float m_contextOpacity;
    Vec3< float > m_contextColor;
    Vec3< float > m_textColor;
    QVector4D m_dividerColor;
    QVector4D m_boxSelectionColor;

    std::unique_ptr< QOpenGLShaderProgram > triangulationMapProgram;
    std::unique_ptr< QOpenGLShaderProgram > cartesianProg;
    std::unique_ptr< QOpenGLShaderProgram > cartesianProg3;
    std::unique_ptr< QOpenGLShaderProgram > colorMapProg3;
    std::unique_ptr< QOpenGLShaderProgram > points3DProg;
    std::unique_ptr< QOpenGLShaderProgram > tubePathProgram;
    std::unique_ptr< QOpenGLShaderProgram > simpleProg;
    std::unique_ptr< QOpenGLShaderProgram > simpleColorProg;
    std::unique_ptr< QOpenGLShaderProgram > prog3D;
    std::unique_ptr< QOpenGLShaderProgram > flatProg;
    std::unique_ptr< QOpenGLShaderProgram > flatProgSerial;
    std::unique_ptr< QOpenGLShaderProgram > textProg;
    std::unique_ptr< QOpenGLShaderProgram > ucProg;
    std::unique_ptr< QOpenGLShaderProgram > colorLegendProg;
    std::unique_ptr< QOpenGLShaderProgram > flatMeshProg;
    std::unique_ptr< QOpenGLShaderProgram > timeStackProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapGenProg;
    std::unique_ptr< QOpenGLShaderProgram > pointHeatMapGenProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapRenderProg;
    std::unique_ptr< QOpenGLShaderProgram > binDiagnosticProgram;

    std::unique_ptr< QOpenGLShaderProgram > lineMapProgram;

    std::map<
    std::string,
        std::pair< std::uint8_t, std::unique_ptr< QOpenGLShaderProgram > > >
        m_shaderMap;

    struct ShaderType
    {
        enum
        {
            VERTEX_SHADER = 1,
            FRAGMENT_SHADER = 2,
            GEOMETRY_SHADER = 4,
            COMPUTE_SHADER = 8
        };
    };

private:

    void sparsifyTemporalDistributions();

    Vec2< double > worldSpaceToScreenSpace( const Vec2< float > & pos )
    {
        double hWs = m_upperLeftWindowCornerWorldSpace.y() - m_lowerRightWindowCornerWorldSpace.y();
        double wWs = m_lowerRightWindowCornerWorldSpace.x() - m_upperLeftWindowCornerWorldSpace.x();

        double dxWs = pos.x() - m_upperLeftWindowCornerWorldSpace.x();
        double dyWs = m_upperLeftWindowCornerWorldSpace.y() - pos.y();

        double rx = dxWs / wWs;
        double ry = dyWs / hWs;

        return Vec2< double >( scaledWidth() * rx, scaledHeight() * ( 1.f - ry ) );
    }

    double scaledHeight()
    {
        return height() * devicePixelRatio();
    }

    double scaledWidth()
    {
        return width() * devicePixelRatio();
    }

    void setTheme( Theme theme )
    {
        if( theme == Theme::White )
        {

        }
        else if ( theme == Theme::Gray )
        {

        }
        else if ( theme == Theme::Dark )
        {
            
        }        
    }

public:

    BALEEN();

    ~BALEEN();

    void writeVelocity();

    bool setPreset( const std::string & preset );
    bool configure( const std::string & dataset, const std::string & preset );

    //! OpenGL initialization
    void compileShaders();
    void initGL();

    /////////////////////////////////////////////////////////////////////////

    void resetSamplerCam();

    /////////////////////////////////////////////////////////////////////////

    void clearSystem();

    Vec2< float > spatialScaling2D();

    void invalidateSampling();

    void mergeGridPoints();

    int getStepOffset();

    I2 getAdjustedHistResolution( I2 origionalResolution );

    void stackHistogram2D_DistGrid(
        FrVolume & frVolume,
        int t,
        const std::vector<int> & gMap,
        const std::vector<float> & distributionValues,
        int ROWS,
        int COLS );

    void stackHistograms2D_GPU(
        FrVolume & frVol );

    void stackHistogram2D(
        FrVolume & frVolume,
        const HistogramDefinition & histDefinition,
        const SpacePartitioningDefinition & spacePartitioning,
        const std::array< Vec2< float >, 2 > & histThresholds,
        const I2 & histResolution,
        const Vec2< double > & spatialScaling,
        const std::string & ptype,
        int t,
        int idx,
        int cellSelection
    );

    void updateMemory();

    void constructLineIndicators(
        const Vec2< float > & linePos,
        const WeightHistogramWidget & weightHistWidget,
        std::vector< TN::Vec2< float > > & lines,
        std::vector< TN::Vec2< float > > & triangles );

    void constructTimeHistogramStack2D(
        FrVolume & frVol,
        int dataType,
        BaseHistDefinition * histDefinition,
        const I2 & histResolution,
        const Vec2< double > & spatialScaling,
        const std::string & ptype,
        const std::string & dtype,
        int cellSelection );

    void constructTimeHistogramStack1D(
        Waterfall & result,
        const std::string & ptype,
        const std::string & attr,
        const SpacePartitioningDefinition & spacePartitioning,
        const TimeSeriesDefinition & timeSeries,
        int selectedCell );

    void renderHistogramMultiples(
        FrVolume & frVolume, I2 histResolution );

    void renderGridTimeSeries(
        bool all,
        bool sel );

    void renderHistogramStackIso(
        BaseHistDefinition * def,
        FrVolume & frVolume );

    void renderHistogramStack1D();

    void renderSelectedHistContour(
        const std::vector< Vec2< float > > & paths,
        const QVector4D & color);

    void renderHistogramAsHeightField( float * hist, I2 resolution );

    void renderSelectedHistogram1D();

    void renderSpatialHistStack2D();

    void prepareBinPhaseTrajectories(
        const TimeSeriesDefinition & tsrs,
        const std::string & ptype );

    void prepareRegionPhaseTrajectories(
        const TimeSeriesDefinition & tsrs,
        const int selectedCell,
        const std::string & ptype );

    void preparePhaseTrajectories(
        const std::string & ptype,
        const TimeSeriesDefinition & tsrs,
        const PhasePlotDefinition & plotDef );

    void computePhaseTrajectories(
        PhasePlotCache & cache,
        TextureLayer & result,
        int dataType,
        const std::string & ptype,
        const TimeSeriesDefinition & tsrs,
        const PhasePlotDefinition & plotDef,
        int weightType,
        const std::string & weightKey );

    QMatrix4x4 computePhaseTexture(
        PhasePlotCache & cache,
        TextureLayer & result,
        int viewMode,
        const std::string & ptype,
        const PhasePlotDefinition & plotDef,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const std::string & interpolation,
        const std::string & blending,
        const std::string & what,
        int cellSelection
    );

    void renderWeightBasedParticleSelection(
        const std::vector< int > & binWeightIds,
        const std::vector< int > & histWeightIds,
        int weightType,
        int viewMode,
        const TimeSeriesDefinition & tsrs,
        const std::string & xAttr,
        const std::string & yAttr,
        const std::string & ptyp
    );

    void renderPhasePrimitives(
        int viewMode,
        const std::string & interpolation,
        const std::string & ptype,
        const PhasePlotDefinition & plotDef,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        float width,
        float height,
        float x,
        float y,
        const QVector4D & color );

    void renderPhaseTexture( TextureLayer & layer, Texture1D3 & tf,  bool fade, Vec2< float > & position, Vec2< float > & size, bool outline, bool onlyOutline  );

    void computeTemporalPlotsAll(
        LineData & data,
        const std::string & ptype,
        const TimePlotDefinition & timePlotDef );

    void computeTemporalPlots(
        LineData & data,
        const std::string & ptype,
        const TimePlotDefinition & timePlotDef,
        const SpacePartitioningDefinition & spaceParititioing );

    void renderTemporalPlot(
        const std::vector< double > & x,
        const std::vector< double > & y,
        const Vec2< double > & yRange,
        const TimeSeriesDefinition & tsrs,
        int selectedTimeStep,
        const QVector4D & color,
        bool offsetCurrentValueText = false );

    void renderParticleTrace2D(
        const std::vector<std::unique_ptr<std::vector<float>>> &x,
        const std::vector<std::unique_ptr<std::vector<float>>> &y,
        const std::vector<std::unique_ptr<std::vector<float>>> &real,
        const std::vector< unsigned int > & highlightIndices,
        const QMatrix4x4 &M);

    void renderParticleTrace2D(
        const std::vector<std::unique_ptr<std::vector<float>>> &x,
        const std::vector<std::unique_ptr<std::vector<float>>> &y,
        const std::vector< unsigned int > & highlightIndices,
        const QMatrix4x4 &M);

    void renderParticleTrace2D(
        const std::vector<std::unique_ptr<std::vector<float>>> &x,
        const std::vector<std::unique_ptr<std::vector<float>>> &y,
        const QMatrix4x4 &M);

    void renderParticleTrace3D(
        const std::vector< std::string > & attrs,
        const std::vector< unsigned int > & highlightIndices,
        const TimeSeriesDefinition & tsrs,
        bool preserveAspect
    );

    BaseHistDefinition * validateCurrentDefinition(
        int dataTypeEnum,
        const std::string & partType,
        const std::string & pdfKey );

    TimeSeriesDefinition * getTimeSeries(
        const std::string & ptype );

    std::set< std::string > getVariablesInUse(
        BaseHistDefinition * histDef,
        SpacePartitioningDefinition * spacePartitioning,
        LinkedViewDefinition * linkedViewDef,
        TimePlotDefinition * timePlotDef,
        int linkedViewType,
        int dataTypeEnum );

    LinkedViewDefinition * getLinkedViewDefinition(
        int dataTypeEnum,
        const std::string & partType,
        const std::string & pdfKey,
        int viewType );

    TimePlotDefinition * getTimePlotDefinition(
        int dataTypeEnum,
        const std::string & partType,
        const std::string & pdfKey );

    void renderComboItems( const ComboWidget & combo );

    Vec2< double > getSelectedCellColorValueRange();
    double getSelectedBinFrequency();

    void renderDetailView(
        FrVolume & frVolume,
        BaseHistDefinition * def,
        int cellSelection,
        int binSelection );

    void renderMiniMap(
        int cellSelection,
        int binSelection,
         bool isPanning );

    void updateBoundingBoxes(
        const SpacePartitioningDefinition & spacePartitioning,
        int populationDataTypeEnum,
        const std::string & ptype );

    void recalculateSamplingLayout();

    int m_roiCell;
    bool m_roiSnapped;

    void snapDataPointsToSamplingWindow( const TN::RegionOfInterest & roi, SpacePartitioningDefinition * spacePartitioning );
    void snapLayoutToSamplingGrid();

    void renderGridSelectionContext(
        const std::vector< std::pair< std::string, double > >  & space,
        const ROIDefinition & roiDefinition,
        bool userIsPanning );

    void renderUncertaintyPlot();
    void renderTimeSeriesInfo( const std::string & ptype );
    void renderTimeLineAnnotation();

    void renderPopulation(
        int dataTypeEnum,
        const std::vector< std::string > & partitionSpace,
        const Vec2< double > & scaling,
        const QMatrix4x4 &M,
        double pointSize );

    void renderVisualContextModels(
        int dataType,
        const std::vector< std::string > & space,
        const std::vector< double > & scaling,
        const QMatrix4x4 & M,
        float lineWidth,
        const Vec2< float > & pos,
        const Vec2< float > & size,
        bool useCamera = true );

    void samplePopulation(
        BaseHistDefinition * def,
        const SpacePartitioningDefinition & spacePartitioing,
        int dataType,
        const std::string & ptype,
        const Vec2< double > & scaling );

    void renderSamplingResult( I2 histResolution );

    void getPhasePlotSizeAndLocation(
        const PhasePlotDefinition & def,
        const std::string & ptype,
        bool includeWeightBreakdown,
        Vec2< float > & size,
        Vec2< float > & location );

    void renderParticleWeightBreakdown(
        bool weightChanged,
        bool updateAll,
        const Vec2< float > & plotSize,
        const Vec2< float > & plotPosition,
        const std::string & ptype,
        const std::string & weightKey,
        std::vector< int > & binWeightIds,
        std::vector< int > & histWeightIds );

    void renderPhasePlotGrid(
        const Vec2< float > & plotSize,
        const Vec2< float > & plotPosition,
        const std::string & xLabel,
        const std::string & yLabel,
        const Vec2< double > & xRange,
        const Vec2< double > & yRange );

    void renderPhasePlot(
        int dataType,
        int viewMode,
        const std::string & ptype,
        const PhasePlotDefinition & plotDef,
        int weightType,
        const std::string & weightKey );

//    void renderLinkedView(
//        int populationDataTypeEnum,
//        int ViewModeEnum,
//        const std::string & ptype,
//        int weightType,
//        const std::string & weightKey,
//        bool userIsPanning );

    void renderSamplingLayout( bool usersIsPanning );

    void renderSamplingLayoutAnnotation(
        const std::string & xSpace,
        const std::string & ySpace );

    std::string getWarningSummary( int populationDataTypeEnum, const std::string & ptype );
    std::uint8_t getErrorRemovalOption();

    void recalculateUiLayout();

    bool parseBasicDataInfo( const std::string & dataset, BasicDataInfo & result );

    virtual void render();

    virtual void wheelEvent( QWheelEvent *e );
    virtual void mouseDoubleClickEvent( QMouseEvent *e );
    virtual void mouseMoveEvent( QMouseEvent *e );
    virtual void resizeEvent( QResizeEvent *e );
    virtual void keyPressEvent( QKeyEvent *e );
    virtual void mousePressEvent( QMouseEvent *e );
    virtual void mouseReleaseEvent( QMouseEvent *e );

    void updateSampler( BaseHistDefinition * def );

    void updateHistLayoutLines();

    void updateSelectedHistGridLines(
        const std::vector< std::int32_t > & resolution );

    void updateColorLegendGeom( const std::vector< TN::Vec3< float > > & tf );

    void setPtclOpacity( double );

    void updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived );
    void updateCustomConstants( const std::map< std::string, CustomConstant > & cc );
    void updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & variables );
    void updateBaseConstants( const std::map< std::string, BaseConstant > & cc );

    AORenderScene getIsoAORenderObject() const;
    void setIsoAORenderObject(const AORenderScene &isoAORenderObject);

public slots:


    void saveProject( bool asDefault );
    void cancelTimeSeriesDefinition();
    void cancelHistogramDefinition();
    void cancelPhasePlotDefinition();
    void cancelTimePlotDefinition();
    void cancelParticleFilterDefinition();
    void cancelROIDefinition();

    void updateUserDataDescription( const std::string & );

    void updateAttributes(
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &
    );

    void finalizePhasePlotDefinition(
        const std::string &,
        const std::map<std::string, PhasePlotDefinition > &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeTimePlotDefinition(
        const std::string &,
        const std::map<std::string, TimePlotDefinition > &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeTimeSeriesDefinition(
        const std::string &,
        const std::map<std::string, TimeSeriesDefinition> & );

    void finalizeHistogramDefinition(
        const std::string &,
        const std::map<std::string, HistogramDefinition> &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeROIDefinition(
        const std::string &,
        const std::map<std::string, ROIDefinition > &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeParticleFilterDefinition(
        const std::string &,
        const std::map<std::string, ParticleFilter> &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeProjection(
        std::vector< std::string >,
        bool );
};

class BALEENWidget : public QWidget
{
public:
    BALEENWidget(QWidget *parent) : QWidget(parent)
    {
        renWin = new BALEEN();

        QWidget *widget = QWidget::createWindowContainer(renWin);
        QHBoxLayout *layout = new QHBoxLayout(this);

        layout->setMargin(0);
        layout->addWidget(widget);
    }

    ~BALEENWidget() {}

    virtual BALEEN *GetRenderWindow()
    {
        return renWin;
    }



private:
    BALEEN *renWin;
};
}

#endif  // BALEEN_H
