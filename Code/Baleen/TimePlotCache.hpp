#ifndef TIMEPLOTCACHE_HPP
#define TIMEPLOTCACHE_HPP

#include "Types/Vec.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"

#include <vector>
#include <unordered_map>
#include <limits>
#include <array>

struct LineData
{
    enum
    {
        MEAN,
        MEAN_SQUARED,
        COUNT,
        VARIANCE,
        MEDIAN,
        SCEWNESS,
        KURTOSIS,
        AVERAGE_RECTIFIED_VALUE,
        MIN,
        MAX
    };

    static const int MAX_PLOTS = 3;
    std::vector< double > values[ MAX_PLOTS ];

    TN::Vec2< double > ranges[ MAX_PLOTS ];
    int steps;
};

struct Waterfall
{
    int steps;
    int bins;
    std::vector< float > values;
};

struct TimePlotCache
{
    TN::TimeSeriesDefinition timeSeries;
    const size_t N_CACHE = 300;
    std::vector< LineData > data;

    LineData fullAggregate;


    std::vector< double > timePoints;
    std::vector< unsigned char > free;

    bool invalidated;
    int steps;

    TimePlotCache() : data( N_CACHE ), free( N_CACHE, true ), invalidated( true )
    {}

    // if a volume is already cached for a given spatial index, then
    // a map element will exist, mapping the spatial index to the index
    // where it exists
    std::unordered_map< int, int > spatialIndexToCacheIndex;
    std::unordered_map< int, int > cacheIndexToSpatialIndex;

    LineData & get( int spatialIndex, bool & valid )
    {
        invalidated = false;

        // if we already have this volume cached
        if( spatialIndexToCacheIndex.count( spatialIndex ) )
        {
            valid = true;
            return data[ spatialIndexToCacheIndex.at( spatialIndex ) ];
        }
        else
        {
            int index = 0;

            // if we have any free slots ...
            if( spatialIndexToCacheIndex.size() < data.size() )
            {
                for( size_t i = 0, end = free.size(); i < end; ++i )
                {
                    if( free[ i ] )
                    {
                        index = i;
                        break;
                    }
                }

                cacheIndexToSpatialIndex.insert( { index, spatialIndex } );
                spatialIndexToCacheIndex.insert( { spatialIndex, index } );
                free[ index ] = false;
            }
            else
            {
                // replace the old index mappings with the new one
                int previousSpatialIndex = cacheIndexToSpatialIndex.at( index );
                spatialIndexToCacheIndex.erase( previousSpatialIndex );
                spatialIndexToCacheIndex.insert( { spatialIndex, index }  );
                cacheIndexToSpatialIndex.at( index ) = spatialIndex;
            }

            valid = false;

            for( int i = 0; i < LineData::MAX_PLOTS; ++i )
            {
                data[ index ].ranges[ i ] = TN::Vec2< double >( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max() );
            }

            return data[ index ];
        }
    }

    void invalidate()
    {
        if( ! invalidated )
        {
            spatialIndexToCacheIndex.clear();
            cacheIndexToSpatialIndex.clear();

            for( auto & f : free )
            {
                f = true;
            }
            invalidated = true;
        }
    }

    void resetFullAggregateRanges()
    {
        for( int i = 0; i < LineData::MAX_PLOTS; ++i )
        {
            fullAggregate.ranges[ i ] = TN::Vec2< double >( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max() );
        }
    }

    void setParameters( const TN::TimeSeriesDefinition & tsDef )
    {
        int _steps = tsDef.numSteps();
        bool sizeChanged = _steps != steps;
        steps = _steps;

        if( ! ( timeSeries == tsDef ) )
        {
            if( sizeChanged )
            {
                timePoints.resize( steps );
                for( int i = 0; i < LineData::MAX_PLOTS; ++i )
                {
                    fullAggregate.values[ i ].resize( steps );
                }
            }
            for( int t = tsDef.firstIdx; t <= tsDef.lastIdx; t += tsDef.idxStride )
            {
                timePoints[ ( t - tsDef.firstIdx ) / tsDef.idxStride ] = t;
            }
        }

        timeSeries = tsDef;

        for( int i = 0; i < LineData::MAX_PLOTS; ++i )
        {
            fullAggregate.ranges[ i ] = TN::Vec2< double >( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max() );
        }

        for( auto & d : data )
        {
            d.steps = steps;
            if( sizeChanged )
            {
                for( int i = 0; i < LineData::MAX_PLOTS; ++i )
                {
                    d.values[ i ].resize( steps );
                }
            }
            for( int i = 0; i < LineData::MAX_PLOTS; ++i )
            {
                d.ranges[ i ] = TN::Vec2< double >( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max() );
            }
        }
        invalidate();
    }
};

#endif // TIMEPLOTCACHE_HPP
