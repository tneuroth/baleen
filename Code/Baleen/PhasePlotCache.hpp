#ifndef PHASEPLOTCACHE_HPP
#define PHASEPLOTCACHE_HPP

#include "OpenGL/Rendering/Texture.hpp"

#include <QGLFramebufferObject>
#include <QGLFramebufferObjectFormat>
#include <vector>
#include <unordered_map>
#include <limits>

namespace TN
{

struct TextureLayer
{
    Texture2D1 tex;
    int width;
    int height;

    float maxValue;

    void computeMax()
    {
        //if( VIDI_CUDA_SUPPORT == 1 )
        //{
        // First would need to get the cuda device that the texure is actually on, since I am using separate GPU for cuda normally ...
//            cudaGraphicsResource * resource;
//            cudaGraphicsGLRegisterImage( &resource, tex, GL_TEXTURE_2D, CUDA_GRAPHICS_REGISTER_FLAGS_READ_ONLY );
//            cuGraphicsMapResources( 1, &resource, 0 );

        /// ...

//            cuGraphicsUnmapResources( 1, &resource, 0 );
        // }

        // TODO: implement OpenGL reduction and or try Cuda


        //////////////////////////////////////////////
        //this is obviously the slow way ( temporary )

        float * v = new float[ width * height ];

        tex.bind();
        glGetTexImage(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            GL_FLOAT,
            ( void * ) v );

        const int END = width * height;

        float mx = v[ 0 ];

        #pragma omp parallel for reduction(max:mx)
        for( int i = 1; i < END; ++i )
        {
            if( v[ i ] > mx )
            {
                mx = v[ i ];
            }
        }

        maxValue = mx;

        delete [] v;

        ///////////////////////////////////////////////
    }

    void resize( int w, int h )
    {
        if( tex.id() == 0 )
        {
            tex.create();
        }

        width = w;
        height = h;

        tex.loadZeros( width, height );
    }
};

struct PhasePlotCache
{
    const int N_CACHE = 144;
    std::vector< TextureLayer > data;
    std::vector< unsigned char > free;
    bool invalidated;
    int width;
    int height;

    GLuint frameBufferId;

    std::unordered_map< int, int > spatialIndexToCacheIndex;
    std::unordered_map< int, int > cacheIndexToSpatialIndex;

    TextureLayer fullPlotTexture;
    TextureLayer binPlotTexture;

    Texture1D1 angleToPixelDensityTexture;

    PhasePlotCache() : data( N_CACHE ), free( N_CACHE, true ), invalidated( true ), frameBufferId( 0 )
    {}

    ~PhasePlotCache()
    {
        if( frameBufferId != 0 )
        {
            glDeleteFramebuffers( 1, &frameBufferId );
        }
    }

    void setAngleToPixelDensityTexture( const std::vector< float > & data )
    {
        angleToPixelDensityTexture.load( data );
    }

    // must be called after openGL has already been initialized
    void initGL()
    {
        glGenFramebuffers( 1, & frameBufferId );
        data = std::vector< TextureLayer >( N_CACHE );

        fullPlotTexture.tex.create();
        binPlotTexture.tex.create();
        angleToPixelDensityTexture.create();
    }

    TextureLayer & get( int spatialIndex, bool & valid )
    {
        invalidated = false;

        // if we already have this texture cached
        if( spatialIndexToCacheIndex.count( spatialIndex ) )
        {
            valid = true;
            return data[ spatialIndexToCacheIndex.at( spatialIndex ) ];
        }
        else
        {
            int index = 0;

            // if we have any free slots ...
            if( spatialIndexToCacheIndex.size() < data.size() )
            {
                for( size_t i = 0, end = free.size(); i < end; ++i )
                {
                    if( free[ i ] )
                    {
                        index = i;
                        break;
                    }
                }
                cacheIndexToSpatialIndex.insert( { index, spatialIndex } );
                spatialIndexToCacheIndex.insert( { spatialIndex, index } );
                free[ index ] = false;
            }
            else
            {
                // replace the old index mappings with the new one
                int previousSpatialIndex = cacheIndexToSpatialIndex.at( index );
                spatialIndexToCacheIndex.erase( previousSpatialIndex );
                spatialIndexToCacheIndex.insert( { spatialIndex, index }  );
                cacheIndexToSpatialIndex.at( index ) = spatialIndex;
            }

            valid = false;
            return data[ index ];
        }
    }

    void invalidate()
    {
        if( ! invalidated )
        {
            spatialIndexToCacheIndex.clear();
            cacheIndexToSpatialIndex.clear();

            for( auto & f : free )
            {
                f = true;
            }

            invalidated = true;
        }
    }

    void bindFrameBuffer()
    {
        glBindFramebuffer( GL_FRAMEBUFFER, frameBufferId );
    }

    void releaseFrameBuffer()
    {
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }

    void setTexture( TextureLayer & texLayer )
    {
        bindFrameBuffer();
        texLayer.tex.bind();

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texLayer.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            //qDebug() << "frame buffer incomplete " << texLayer.height << " " << texLayer.width;
        }

        releaseFrameBuffer();
    }

    void setParameters( int _width, int _height )
    {
        bool sizeChanged = ( width != _width ) || ( height != _height );

        width = _width;
        height = _height;

        for( auto & d : data )
        {
            if( sizeChanged )
            {
                d.resize( width, height );
            }
        }

        fullPlotTexture.resize( width, height );
        binPlotTexture.resize( width, height );

        invalidate();
    }
};

}

#endif // PHASEPLOTCACHE_HPP
