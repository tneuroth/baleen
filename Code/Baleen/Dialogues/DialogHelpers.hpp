#ifndef DIALOGHELPERS_HPP
#define DIALOGHELPERS_HPP

#include "Data/Definitions/TimeSeriesDefinition.hpp"

#include <QComboBox>
#include <QTableWidget>
#include <QString>

#include <string>

inline int getAttrComboIndex( const std::string & attr, QComboBox * combo )
{
    int index = -1;
    for ( int i = 0; i < combo->count(); ++i )
    {
        std::string text = combo->itemText( i ).toStdString();
        std::string attrNm;
        for( int k = 0; k < text.size(); ++k )
        {
            if( k < text.size() - 1 )
            {
                if( text[ k ] == ' ' && text[ k + 1 ] == '=' )
                {
                    break;
                }
            }

            attrNm.push_back( text[ k ] );
        }
        if( attrNm == attr )
        {
            index = i;
            break;
        }
    }
    return index;
}

inline void setTableFromSeries(
    const TN::TimeSeriesDefinition & series,
    QTableWidget * table,
    const std::vector< double > & realTime,
    const std::vector< std::int32_t > & simTime,
    bool editable = true )
{
    if( (int) realTime.size() >=  series.lastIdx )
    {
        table->item( 0, 0 )->setText( QString::number( realTime[ series.firstIdx  ] ) );
        table->item( 0, 1 )->setText( QString::number( realTime[ series.lastIdx   ] ) );
        table->item( 0, 2 )->setText( QString::number( realTime[ series.firstIdx + 1 ] - realTime[ series.firstIdx ] ) );
    }
    else
    {
        table->item( 0, 0 )->setText( "NA" );
        table->item( 0, 1 )->setText( "NA" );
        table->item( 0, 2 )->setText( "NA" );

        table->item( 0, 0 )->setText( "NA" );
        table->item( 0, 1 )->setText( "NA" );
        table->item( 0, 2 )->setText( "NA" );
    }

    table->item( 1, 0 )->setText( QString::number( simTime[ series.firstIdx  ] ) );
    table->item( 1, 1 )->setText( QString::number( simTime[ series.lastIdx   ] ) );
    table->item( 1, 2 )->setText( QString::number( simTime[ series.firstIdx + 1 ] - simTime[ series.firstIdx ]  ) );

    table->item( 2, 0 )->setText( QString::number( series.firstIdx ) );
    table->item( 2, 1 )->setText( QString::number( series.lastIdx ) );
    table->item( 2, 2 )->setText( QString::number( 1 ) );
    \

    if( ! editable )
    {
        for (int i = 0; i < table->rowCount(); ++i )
        {
            for (int j = 0; j < table->columnCount(); ++j )
            {
                table->item( i, j )->setFlags( table->item( i, j )->flags() & ~Qt::ItemIsEditable );
            }
        }
    }
}

#endif // DIALOGHELPERS_HPP
