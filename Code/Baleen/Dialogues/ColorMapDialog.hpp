#ifndef COLORMAPDIALOG_HPP
#define COLORMAPDIALOG_HPP

#include <string>
#include <set>

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QImageReader>
#include <QImage>

struct ColorMapId
{
    std::string name;
    std::string category;
};

namespace TN
{

class ColorMapDialog : public QDialog
{
    Q_OBJECT

    QImage image;
    QLabel *imageLabel;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QComboBox * colorMapCombo;

    QLabel * mapComboLabel;

signals :

    void setColorMap( const std::string & );

private slots :

    void finalize()
    {
        //..
        close();
    }

    void cancel()
    {
        close();
    }

public :

    void set( const std::set< ColorMapId > & colorMaps, const QString & imPath )
    {
        colorMapCombo->clear();
        for( auto & cm : colorMaps )
        {
            colorMapCombo->addItem( cm.name.c_str() );
        }

        QImageReader reader( imPath );
        reader.setAutoTransform(true);

        image = reader.read();
        imageLabel->setPixmap(QPixmap::fromImage(image));
        imageLabel->resize( imageLabel->pixmap()->size() );
    }

    ColorMapDialog() : QDialog( 0 )
    {
        imageLabel = new QLabel;
        imageLabel->setBackgroundRole( QPalette::Base );
        imageLabel->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
        imageLabel->setScaledContents( true );

        acceptButton = new QPushButton( "save" );
        cancelButton = new QPushButton( "cancel" );

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        mapComboLabel = new QLabel( "color map" );
        colorMapCombo = new QComboBox;

        QVBoxLayout * MainLayout = new QVBoxLayout;
        QHBoxLayout * comboLayout = new QHBoxLayout;
        QHBoxLayout * finishLayout = new QHBoxLayout;

        comboLayout->addWidget( mapComboLabel );
        comboLayout->addWidget( colorMapCombo );

        finishLayout->addWidget( cancelButton );
        finishLayout->addWidget( acceptButton );

        MainLayout->addLayout( comboLayout );
        MainLayout->addWidget( imageLabel );
        MainLayout->addLayout( finishLayout );

        this->setLayout( MainLayout );
    }

    virtual ~ColorMapDialog()
    {}
};

}

#endif // COLORMAPDIALOG_HPP
