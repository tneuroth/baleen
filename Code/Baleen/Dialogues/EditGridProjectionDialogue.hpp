

#ifndef TN_GRID_PROJECTION_DIALOGUE
#define TN_GRID_PROJECTION_DIALOGUE

#include "Types/Vec.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN
{

class EditGridProjectionDialogue : public QDialog
{
    Q_OBJECT

    QWindow * m_parentWindow;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QVBoxLayout * PlotAttributeSection;

    std::set< std::string > attributes;

    std::string histKey;

    /////////////////////////////////////////////////////////////////////

    // Sampling Space

    QLabel    * spaceAttrLabel;
    QLabel    * spaceDimsLabel;
    QLabel    * spaceScalingLabel;

    QComboBox * spaceScalingCombo;
    QComboBox * spaceDimsCombo;


    QComboBox * spaceAttrCombo1;
    QComboBox * spaceAttrCombo2;
    QComboBox * spaceAttrCombo3;

    QVBoxLayout * spacec2;
    QVBoxLayout * spacec3;

    QHBoxLayout * spaceLayout;

    //////////////////////////////////////////////////////

    // Histogram description

    QVBoxLayout * histogramDescriptionLayout;
    QLabel    * histogramDescriptionLabel;
    QTextEdit * histogramDescriptionBox;

    /////////////////////////////////////////////////////////////////////

public:

    ~EditGridProjectionDialogue() {}

signals:

    void cancelDefinition();
    void finalizeProjection( std::vector< std::string >, bool );

private slots:


    bool validateNumber( QString number )
    {
        bool isNumber;
        float value = number.toDouble( &isNumber );
        if( ! isNumber )
        {
            return false;
        }
        if( std::isinf( value ) || std::isnan( value ) )
        {
            return false;
        }
        return true;
    }

    bool validateInteger( QString number )
    {
        std::string nm = number.toStdString();

        if( ! validateNumber( number ) )
        {
            return false;
        }
        if( nm.find_first_of( "123456789" ) != 0  )
        {
            return false;
        }
        if( nm.find_first_not_of( "1234567890Ee+" ) != nm.npos  )
        {
            return false;
        }
        return true;
    }

    void valueLineEditChanged( QString text )
    {
        QLineEdit * edit = (QLineEdit*) QObject::sender();

        std::string cl( "black" );

        if( validateNumber( text ) )
        {
            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
        }
        else
        {
            edit->setStyleSheet( "QLineEdit { color : gray; }" );
        }
    }

    void valueEditChanged( QString text )
    {
        QLineEdit * edit = (QLineEdit*) QObject::sender();

        std::string cl( "rgb(200,90,0)" );

        if( validateNumber( text ) )
        {
            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
        }
        else
        {
            edit->setStyleSheet( "QLineEdit { color : gray; }" );
        }
    }

    void changeSpaceDims( QString dimsStr )
    {
        int dims = std::stoi( dimsStr.toStdString() );
        if( dims > 1 )
        {
            spacec2->addWidget( spaceAttrCombo2 );
            spaceAttrCombo2->setVisible( true );
        }
        else
        {
            spacec2->removeWidget( spaceAttrCombo2 );
            spaceAttrCombo2->setVisible( false );
        }

        if( dims > 2 )
        {
            spacec2->addWidget( spaceAttrCombo3 );
            spaceAttrCombo3->setVisible( true );
        }
        else
        {
            spacec2->removeWidget( spaceAttrCombo3 );
            spaceAttrCombo3->setVisible( false );
        }
    }

    void finalize()
    {
        int spatialDims = std::stoi( spaceDimsCombo->currentText().toStdString() );
        std::vector< std::string > spatialAttrs;

        if( spatialDims >= 1 )
        {
            spatialAttrs.push_back( spaceAttrCombo1->currentText().toStdString() );
        }
        if( spatialDims >= 2 )
        {
            spatialAttrs.push_back( spaceAttrCombo2->currentText().toStdString() );
        }
        if( spatialDims >= 3 )
        {
            spatialAttrs.push_back( spaceAttrCombo3->currentText().toStdString() );
        }

        emit finalizeProjection( spatialAttrs, spaceScalingCombo->currentText() == "Preserve Aspect Ratio" );

        close();
    }

    void cancel()
    {
        emit cancelDefinition();
        close();
    }

public:

    void set( const std::set< std::string > & vars, const SpacePartitioningDefinition & sp )
    {
        attributes = vars;

        for( auto & attr : vars )
        {
            spaceAttrCombo1->addItem( attr.c_str() );
            spaceAttrCombo2->addItem( attr.c_str() );
            spaceAttrCombo3->addItem( attr.c_str() );
        }

        if( sp.attributes.size() > 0 )
        {
            if( attributes.count( sp.attributes[ 0 ] ) )
            {
                spaceAttrCombo1->setCurrentIndex( spaceAttrCombo1->findText( QString( sp.attributes[ 0 ].c_str() ) ) );
            }
        }
        if( sp.attributes.size() > 1 )
        {
            if( attributes.count( sp.attributes[ 1 ] ) )
            {
                spaceAttrCombo2->setCurrentIndex( spaceAttrCombo2->findText( QString( sp.attributes[ 1 ].c_str() ) ) );
            }
        }
        if( sp.attributes.size() > 3 )
        {
            if( attributes.count( sp.attributes[ 2 ] ) )
            {
                spaceAttrCombo3->setCurrentIndex( spaceAttrCombo3->findText( QString( sp.attributes[ 2 ].c_str() ) ) );
            }
        }

        if( sp.preserveAspectRatio )
        {
            spaceScalingCombo->setCurrentIndex( 0 );
        }
        else
        {
            spaceScalingCombo->setCurrentIndex( 1 );
        }
    }

    void update( const std::set< std::string > & vars )
    {
        attributes = vars;
        std::string selected1 = "";
        std::string selected2 = "";
        std::string selected3 = "";

        if( attributes.count( spaceAttrCombo1->currentText().toStdString() ) )
        {
            selected1 = spaceAttrCombo1->currentText().toStdString();
        }

        if( attributes.count( spaceAttrCombo2->currentText().toStdString() ) )
        {
            selected2 = spaceAttrCombo2->currentText().toStdString();
        }

        if( attributes.count( spaceAttrCombo3->currentText().toStdString() ) )
        {
            selected3 = spaceAttrCombo3->currentText().toStdString();
        }

        spaceAttrCombo1->clear();
        spaceAttrCombo2->clear();
        spaceAttrCombo3->clear();

        for( auto & attr : vars )
        {

            spaceAttrCombo1->addItem( attr.c_str() );
            spaceAttrCombo2->addItem( attr.c_str() );
            spaceAttrCombo3->addItem( attr.c_str() );
        }

        if( selected1 != "" )
        {
            spaceAttrCombo1->setCurrentIndex( spaceAttrCombo1->findText( QString( selected1.c_str() ) ) );
        }

        if( selected2 != "" )
        {
            spaceAttrCombo2->setCurrentIndex( spaceAttrCombo2->findText( QString( selected2.c_str() ) ) );
        }

        if( selected3 != "" )
        {
            spaceAttrCombo3->setCurrentIndex( spaceAttrCombo3->findText( QString( selected3.c_str() ) ) );
        }
    }

    EditGridProjectionDialogue( std::string _key, QWindow *parentWindow ) : QDialog( 0 ), m_parentWindow( parentWindow ), histKey( _key )
    {
        setStyleSheet("  QWidget { background-color: rgb( 240, 240, 240 ); } QLineEdit { background-color: white; } QComboBox { background-color: white; } QListWidget { background-color: white; }");

        PlotAttributeSection = new QVBoxLayout;


        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        // space

        spaceAttrLabel = new QLabel( "Attribute" );
        spaceDimsLabel = new QLabel( "Dimensionality" );
        spaceScalingLabel = new QLabel( "Scaling" );

        QLabel * spatialRegionLabel = new QLabel( "Grid Projection" );
        spatialRegionLabel->setStyleSheet( "QLabel { color : blue; }" );

        QHBoxLayout * frabskjdkjdskl = new QHBoxLayout;
        frabskjdkjdskl->addWidget(  spatialRegionLabel );
        frabskjdkjdskl->addStretch();

        spaceDimsCombo = new QComboBox;
        spaceScalingCombo = new QComboBox;
        spaceDimsCombo->setMinimumWidth( 100 );

        spaceScalingCombo->addItem( "Preserve Aspect Ratio" );
        spaceScalingCombo->addItem( "Normalize" );

        QHBoxLayout * frabsdsdsfl2 = new QHBoxLayout;
        frabsdsdsfl2->addWidget( spaceDimsLabel );
        frabsdsdsfl2->addWidget( spaceDimsCombo );
        frabsdsdsfl2->addWidget( spaceScalingLabel );
        frabsdsdsfl2->addWidget( spaceScalingCombo );
        frabsdsdsfl2->addStretch();

        spaceAttrCombo1 = new QComboBox;
        spaceAttrCombo2 = new QComboBox;
        spaceAttrCombo3 = new QComboBox;

        spaceAttrCombo1->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        spaceAttrCombo2->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        spaceAttrCombo3->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );

        spacec2 = new QVBoxLayout;
        spacec3 = new QVBoxLayout;
        spaceLayout = new QHBoxLayout;

        spacec2->addWidget( spaceAttrLabel  );
        spacec2->addWidget( spaceAttrCombo1 );
        spacec2->addWidget( spaceAttrCombo2 );

        spaceLayout->addLayout( spacec2 );
        spaceLayout->addLayout( spacec3 );

        spaceDimsLabel->setBuddy( spaceDimsCombo );

        //////////////////////////////////////////////////////////////////////////////

        QFrame *l1dsdds = new QFrame;
        l1dsdds->setFrameShape( QFrame::HLine );
        l1dsdds->setFrameShadow( QFrame::Sunken );
        l1dsdds->setStyleSheet( "QFrame { background-color : white; }" );
        PlotAttributeSection->addWidget( l1dsdds );
        PlotAttributeSection->addLayout( frabskjdkjdskl );
        PlotAttributeSection->addLayout( frabsdsdsfl2 );
        PlotAttributeSection->addLayout( spaceLayout );

        ///////////////////////////////////////////////////////////////////////////////

        // Description

        histogramDescriptionLayout = new QVBoxLayout;
        histogramDescriptionLabel = new QLabel( "Description" );
        histogramDescriptionLabel->setStyleSheet( "QLabel { color : blue; }" );
        histogramDescriptionBox = new QTextEdit;
        histogramDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( histogramDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        histogramDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        histogramDescriptionLayout->addWidget( histogramDescriptionBox );

        // main layout

        QHBoxLayout * higherLevelLayout = new QHBoxLayout;
        QVBoxLayout * mainLayout        = new QVBoxLayout;

        mainLayout->addLayout( PlotAttributeSection );

        QFrame *line1 = new QFrame;
        line1->setFrameShape( QFrame::HLine );
        line1->setFrameShadow( QFrame::Sunken );
        line1->setStyleSheet( "QFrame { background-color : white; }" );

        mainLayout->addWidget( line1 );

        mainLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );

        mainLayout->addLayout( histogramDescriptionLayout );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        mainLayout->addLayout( dll );

        higherLevelLayout->addLayout( mainLayout );
        setLayout( higherLevelLayout );


        spaceDimsCombo->addItem( "1" );
        spaceDimsCombo->addItem( "2" );
        spaceDimsCombo->addItem( "3" );

        spaceDimsCombo->setCurrentIndex( 1 );
        connect( spaceDimsCombo, SIGNAL( currentTextChanged(QString) ), this, SLOT( changeSpaceDims( QString ) ) );
    }
};

}

#endif

