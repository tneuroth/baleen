


#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Dialogues/ROIDialog.hpp"

#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"

#include "Dialogues/DialogHelpers.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>
#include <QHeaderView>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN
{

ROIDialog::~ROIDialog() {}

void ROIDialog::finalize()
{
    std::string name = roiNameingLineEdit->text().toStdString();

    if( name.size() == 0 )
    {
        errorMessageLabel->setText( "Invalid Name/key" );
        return;
    }
    if( ( m_status != "defined" ) && ( definedROIs.find( name ) != definedROIs.end() ) )
    {
        errorMessageLabel->setText( "Name/key already in use" );
        return;
    }
    if( name == "temp" )
    {
        errorMessageLabel->setText( "Invalid Name/key" );
        return;
    }

    if( xCombo->currentText() == yCombo->currentText() )
    {
        errorMessageLabel->setText( "x and y must be different." );
        return;
    }
    if( xCombo->currentText() == "" || yCombo->currentText() == "" )
    {
        errorMessageLabel->setText( "Must select x and y." );
        return;
    }

    int idx = modeCombo->currentIndex();
    Mode mode = idx == 0 ? Mode::Basic :
                idx == 1 ? Mode::FixedRegion :
                idx == 2 ? Mode::TemporospatialPath :
                idx == 3 ? Mode::spatialPath : Mode::Basic;


    ROIDefinition definition;

    if( mode == Mode::FixedRegion )
    {
        if( ! validateNumber( xLower->text() )
         || ! validateNumber( xUpper->text() )
         || ! validateNumber( yLower->text() )
         || ! validateNumber( xUpper->text() ) )
        {
            errorMessageLabel->setText( "upper/lower limits must be valid numbers" );
            return;
        }

        double xl = xLower->text().toDouble();
        double xu = xUpper->text().toDouble();
        double yl = yLower->text().toDouble();
        double yu = yUpper->text().toDouble();

        if( xl >= xu || yl >= yu )
        {
            errorMessageLabel->setText( "upper must be less than lower" );
            return;
        }

        definition.xROI = { xl, xu };
        definition.yROI = { yl, yu };
    }
    if( mode == Mode::TemporospatialPath || mode == Mode::spatialPath )
    {
        if( ! validateNumber( xWidthEdit->text() )
         || ! validateNumber( yWidthEdit->text() ) )
        {
            errorMessageLabel->setText( "x/y widths must be valid numbers" );
            return;
        }

        double xw = xWidthEdit->text().toDouble();
        double yw = yWidthEdit->text().toDouble();

        if( xw <= 0 || yw <= 0 )
        {
            errorMessageLabel->setText( "x/y widths must be greater than 0" );
            return;
        }

        definition.widths = { xw, yw };
    }

    definition.spatiotemporalPath = tsPathCombo->currentText().toStdString();
    definition.spatialPath = sPathCombo->currentText().toStdString();

    definition.description = roiDescriptionBox->toPlainText().toStdString();
    m_key = name;

    definition.name = m_key;
    definition.mode = modeCombo->currentIndex();
    definition.space = { xCombo->currentText().toStdString(), yCombo->currentText().toStdString() };

    qDebug() << "insert roi in defined";

    if( m_status == "undefined" )
    {
        qDebug() << "roi was undefined, inserting";
        definedROIs.insert( { m_key, definition } );
    }
    else
    {
        qDebug() << "roi was defined, setting";
        definedROIs.find( m_key )->second = definition;
    }

    qDebug() << "setting status";

    setStatus( "defined" );

    qDebug() << "emiting finalize";

    emit finalizeDefinition(
        m_key,
        definedROIs,
        baseVariables,
        derivedVariables,
        baseConstants,
        customConstants
    );

    qDebug() << "closing roi dialog";

    close();
}

void ROIDialog::cancel()
{
    emit cancelDefinition();
    close();
}

void ROIDialog::update(
    const std::map< std::string, ROIDefinition > & _roiMap,
    const std::map< std::string, BaseVariable > & _baseVarMap,
    const std::map< std::string, DerivedVariable > & _derivedVarMap,
    const std::map< std::string, CustomConstant  > & _customConstMap,
    const std::map< std::string, BaseConstant   > & _baseConstMap )
{
    if( m_status == "defined" )
    {
        for( auto & dv : _derivedVarMap )
        {
            if( ! derivedVariables.count( dv.second.name() ) )
            {
                std::string _nm = dv.second.name() + " = " + dv.second.expression();
                derivedAttributeList->addItem( _nm.c_str() );
                xCombo->addItem( _nm.c_str() );
                yCombo->addItem( _nm.c_str() );
            }
        }
        for( auto & cc : _customConstMap)
        {
            if( ! customConstants.count( cc.second.name() ) )
            {
                std::string _nm = cc.second.name()
                                  + " = " + cc.second.expression();

                if( ! m_expressionCalculator->parser().validateNumber( cc.second.expression() ) )
                {
                    _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( cc.second.expression(), _baseConstMap, _customConstMap ), 10 );
                }
                userConstantList->addItem( _nm.c_str() );
                xCombo->addItem( _nm.c_str() );
                yCombo->addItem( _nm.c_str() );
            }
        }
    }

    baseVariables = _baseVarMap;
    derivedVariables = _derivedVarMap;
    customConstants = _customConstMap;
    baseConstants = _baseConstMap;
    definedROIs = _roiMap;

    if( m_status != "defined" )
    {
        attributeList->setStyleSheet( "QListWidget { color : green }" );
        constantList->setStyleSheet( "QListWidget { color : green }" );

        constantList->clear();
        userConstantList->clear();
        derivedAttributeList->clear();
        attributeList->clear();

        for( auto & custConst : _customConstMap )
        {
            std::string _nm = custConst.second.name()
                              + " = " + custConst.second.expression();

            if( ! m_expressionCalculator->parser().validateNumber( custConst.second.expression() ) )
            {
                _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ), 10 );
            }
            userConstantList->addItem( _nm.c_str() );
        }

        for( auto & baseConst : _baseConstMap )
        {
            std::string _nm = baseConst.first + " = " + to_string_with_precision( baseConst.second.value(), 10 );
            constantList->addItem( _nm.c_str() );
        }

        for( auto & dv : _derivedVarMap )
        {
            std::string _nm = dv.second.name() + " = " + dv.second.expression();
            derivedAttributeList->addItem( _nm.c_str() );
            xCombo->addItem( _nm.c_str() );
            yCombo->addItem( _nm.c_str() );
        }

        for( auto & baseVarE : baseVariables )
        {
            std::string _nm = baseVarE.first.c_str();
            attributeList->addItem( _nm.c_str() );
            xCombo->addItem( _nm.c_str() );
            yCombo->addItem( _nm.c_str() );
        }
        if( definedROIs.count( m_key ) )
        {
            if( m_key != "temp" )
            {
                fromDefinition( definedROIs.at( m_key ) );
            }
        }
    }
}

void ROIDialog::fromDefinition( const ROIDefinition & def )
{

}

void ROIDialog::setMode( Mode md )
{
    if( md == Mode::Basic )
    {
        rangeWidget->hide();
        widthWidget->hide();
        temporoWidget->hide();
        spatialWidget->hide();
    }
    if( md == Mode::FixedRegion )
    {
        rangeWidget->show();
        widthWidget->hide();
        temporoWidget->hide();
        spatialWidget->hide();
    }
    else if( md == Mode::TemporospatialPath )
    {
        rangeWidget->hide();
        widthWidget->show();
        temporoWidget->show();
        spatialWidget->hide();

    }
    else if( md == Mode::spatialPath )
    {
        rangeWidget->hide();
        widthWidget->show();
        temporoWidget->hide();
        spatialWidget->show();
    }
}

void ROIDialog::setStatus( const std::string & status )
{
    m_status = status;
    if( status == "defined" )
    {
        acceptButton->setText( "update" );
        roiNameingLineEdit->setEnabled( false );
    }
}

void ROIDialog::updateMode( int idx )
{
    setMode( idx == 0 ? Mode::Basic :
             idx == 1 ? Mode::FixedRegion :
             idx == 2 ? Mode::TemporospatialPath :
             idx == 3 ? Mode::spatialPath : Mode::Basic );
}

void ROIDialog::setPaths(
    const std::set< std::string > & spatiotemporalPaths,
    const std::set< std::string > & spatialPaths )
{
    tsPathCombo->clear();
    for( auto & p : spatiotemporalPaths )
    {
        tsPathCombo->addItem( p.c_str() );
    }

    sPathCombo->clear();
    for( auto & p : spatialPaths )
    {
        sPathCombo->addItem( p.c_str() );
    }
}

ROIDialog::ROIDialog( std::string _key, QWindow *parentWindow ) : BaseDataDialogue( _key, parentWindow )
{
    acceptButton = new QPushButton( "create" );
    cancelButton = new QPushButton( "cancel" );
    roiNameingLabel = new QLabel( "ROI Name/Key" );
    roiNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
    roiNameingLineEdit = new QLineEdit;

    connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
    connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

    // Description

    roiDescriptionLayout = new QVBoxLayout;
    roiDescriptionLabel = new QLabel( "ROI Description" );
    roiDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
    roiDescriptionBox = new QTextEdit;
    roiDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

    QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
    kjncewionoicejnoinwe->addWidget( roiDescriptionLabel );
    kjncewionoicejnoinwe->addStretch();

    roiDescriptionLayout->addLayout( kjncewionoicejnoinwe );
    roiDescriptionLayout->addWidget( roiDescriptionBox );

    // description

    QHBoxLayout * windowLayout = new QHBoxLayout;

    m_mainLayout->addLayout( windowLayout );
    m_mainLayout->addLayout( roiDescriptionLayout );

    //////////////////////

    xLabel = new QLabel( "x-axis" );
    xCombo = new QComboBox;

    yLabel = new QLabel( "y-axis" );
    yCombo = new QComboBox;

    modeLabel = new QLabel( "Mode" );
    modeCombo = new QComboBox;

    modeCombo->addItem( "Basic" );
    modeCombo->addItem( "Fixed ROI" );
    modeCombo->addItem( "Spatiotemporal Path" );
    modeCombo->addItem( "Spatial Path" );

    connect( modeCombo, SIGNAL( currentIndexChanged( int ) ), this, SLOT( updateMode( int ) ) );

    spaceSelectionSubLayout1 = new QHBoxLayout;

    spaceSelectionSubLayout1->addWidget( modeLabel );
    spaceSelectionSubLayout1->addWidget( modeCombo );

    spaceSelectionSubLayout1->addWidget( xLabel );
    spaceSelectionSubLayout1->addWidget( xCombo );

    spaceSelectionSubLayout1->addWidget( yLabel );
    spaceSelectionSubLayout1->addWidget( yCombo );

    spaceSelectionLayout = new QVBoxLayout;
    spaceSelectionLayout->addLayout( spaceSelectionSubLayout1 );

    m_mainLayout->addLayout( spaceSelectionLayout );

    //////////////////////////////////////////////////////////////

    // Fixed Mode

    xrangeLowerLabel = new QLabel("x-lower");
    xLower = new QLineEdit;

    xrangeUpperLabel = new QLabel( "x-upper" );
    xUpper = new QLineEdit;

    yrangeLowerLabel = new QLabel( "y-lower" );
    yLower = new QLineEdit;

    yrangeUpperLabel = new QLabel( "y-upper" );
    yUpper = new QLineEdit;

    xRangeLayout = new QHBoxLayout;
    yRangeLayout = new QHBoxLayout;

    xRangeLayout->addWidget( xrangeLowerLabel );
    xRangeLayout->addWidget( xLower );
    xRangeLayout->addWidget( xrangeUpperLabel );
    xRangeLayout->addWidget( xUpper );
    yRangeLayout->addWidget( yrangeLowerLabel );
    yRangeLayout->addWidget( yLower );
    yRangeLayout->addWidget( yrangeUpperLabel );
    yRangeLayout->addWidget( yUpper );

    //

    rangeLayout = new QVBoxLayout;

    rangeLayout->addLayout( xRangeLayout  );
    rangeLayout->addLayout( yRangeLayout  );

    rangeWidget = new QWidget;
    rangeWidget->setLayout( rangeLayout );

    m_mainLayout->addWidget( rangeWidget  );
    rangeWidget->hide();

    // Temporo Mode

    tsPathLabel = new QLabel( "Spatiotemporal Path" );
    tsPathCombo = new QComboBox;

    spatioTemporoLayout = new QHBoxLayout;
    temporoWidget = new QWidget;

    spatioTemporoLayout->addWidget( tsPathLabel );
    spatioTemporoLayout->addWidget( tsPathCombo );

    temporoWidget->setLayout( spatioTemporoLayout );

    m_mainLayout->addWidget( temporoWidget );
    temporoWidget->hide();

    // Spatial Mode

    sPathLabel = new QLabel( "Spatial Path" );
    sPathCombo = new QComboBox;
    sPathCombo->addItem( "Free Select" );

    spatialPathLayout = new QHBoxLayout;
    spatialWidget = new QWidget;

    spatialPathLayout->addWidget( sPathLabel );
    spatialPathLayout->addWidget( sPathCombo );

    spatialWidget->setLayout( spatialPathLayout );
    m_mainLayout->addWidget( spatialWidget );
    spatialWidget->hide();

    // width

    widthLayout = new QHBoxLayout;
    widthWidget = new QWidget;

    xWidthLabel = new QLabel( "x-width" );
    xWidthEdit = new QLineEdit;

    yWidthLabel = new QLabel("y-width");
    yWidthEdit = new QLineEdit;

    widthLayout->addWidget( xWidthLabel );
    widthLayout->addWidget( xWidthEdit );
    widthLayout->addWidget( yWidthLabel );
    widthLayout->addWidget( yWidthEdit );

    widthWidget->setLayout( widthLayout );

    m_mainLayout->addWidget( widthWidget  );
    widthWidget->hide();

    // accept/cancel

    m_mainLayout->addWidget( errorMessageLabel );

    QHBoxLayout *dll = new QHBoxLayout;
    dll->addWidget( roiNameingLabel );
    dll->addWidget( roiNameingLineEdit );

    dll->addWidget( cancelButton );
    dll->addWidget( acceptButton );
    m_mainLayout->addLayout( dll );

    updateMode( modeCombo->currentIndex() );
}
}

