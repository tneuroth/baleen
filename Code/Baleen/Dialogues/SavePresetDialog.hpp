#ifndef SAVEPRESETDIALOG_HPP
#define SAVEPRESETDIALOG_HPP

#include "Data/Configuration/ProjectConfiguration.hpp"

#include <set>
#include <string>

#include <QDialog>
#include <QLineEdit>
#include <QTextEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>

namespace TN
{

class SavePresetDialog : public QDialog
{
    Q_OBJECT

    std::map< std::string, ProjectConfiguration > * savedConfigurations;
    ProjectConfiguration * currentConfiguration;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QLineEdit * nameEdit;
    QLineEdit * authorEdit;
    QTextEdit * descriptionEdit;

    QCheckBox * makeDefaultCheckBox;

    QLabel *nameLabel;
    QLabel *authorLabel;
    QLabel *descriptionLabel;

    QLabel *errorMessageLabel;

    bool validateName( QString name )
    {
        // make sure it's not in use, and no spaces, tabs, or newlines please!

        std::string _name = name.toStdString();

        if( name == "" )
        {
            return false;
        }

        if( name == "base" )
        {
            return false;
        }

        if( _name.find_first_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_" ) != 0  )
        {
            return false;
        }

        if( _name.find_first_not_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890" ) != name.toStdString().npos  )
        {
            return false;
        }

        return true;
    }

signals :

    void saveProject( bool );

private slots :

    void finalize()
    {
        ////qDebug() << "finalizing";

        if( currentConfiguration == nullptr )
        {
            errorMessageLabel->setText( "No preset exists to save." );
            return;
        }

        if( ! validateName( nameEdit->text() ) )
        {
            errorMessageLabel->setText( "Invalid Name" );
            return;
        }
        else
        {
            ////qDebug() << "name is valid";
            errorMessageLabel->setText( "" );
        }

        if( savedConfigurations->count( nameEdit->text().toStdString() ) )
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question( this, "", "The name is already in use. Do you want to overwrite the previous preset?", QMessageBox::Yes | QMessageBox::No );

            if( reply == QMessageBox::No )
            {
                errorMessageLabel->setText( "The name is in use." );
                return;
            }
        }

        ////qDebug() << "name is unused";
        ////qDebug() << "active type is " << currentConfiguration->activeType.c_str();

        if( currentConfiguration->activeType == "particle" )
        {
            ////qDebug() << "set name to " << nameEdit->text();
            currentConfiguration->particleBasedConfiguration.m_name = nameEdit->text().toStdString();
            currentConfiguration->particleBasedConfiguration.m_author = authorEdit->text().toStdString();
            currentConfiguration->particleBasedConfiguration.m_description = descriptionEdit->toPlainText().toStdString();
        }
        else
        {
            currentConfiguration->distributionBasedConfiguration.m_name = nameEdit->text().toStdString();
            currentConfiguration->distributionBasedConfiguration.m_author = authorEdit->text().toStdString();
            currentConfiguration->distributionBasedConfiguration.m_description = descriptionEdit->toPlainText().toStdString();
        }

        ////qDebug() << "emiting save project";

        emit saveProject( makeDefaultCheckBox->isChecked() );

        close();
    }

    void cancel()
    {
        errorMessageLabel->setText( "" );
        close();
    }

public:

    void update( const std::string & name, const std::string & author, const std::string & desc )
    {
        nameEdit->setText( name.c_str() );
        authorEdit->setText( author.c_str() );
        descriptionEdit->setText( desc.c_str() );
    }

    void setConfig( ProjectConfiguration * config )
    {
        currentConfiguration = config;
    }

    void setSavedConfigurations( std::map< std::string, ProjectConfiguration > * configs )
    {
        savedConfigurations = configs;
    }

    SavePresetDialog() : QDialog( 0 )
    {
        currentConfiguration = nullptr;

        acceptButton = new QPushButton( "save" );
        cancelButton = new QPushButton( "cancel" );

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        nameEdit = new QLineEdit();
        authorEdit = new QLineEdit();
        descriptionEdit = new QTextEdit();

        makeDefaultCheckBox = new QCheckBox( "save as default" );

        nameLabel = new QLabel( "preset name" );
        authorLabel = new QLabel( "author" );
        descriptionLabel = new QLabel( "description" );

        errorMessageLabel = new QLabel;
        errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

        QVBoxLayout * layout1 = new QVBoxLayout;
        QHBoxLayout * layout2 = new QHBoxLayout;

        QHBoxLayout * nameLayout = new QHBoxLayout;
        QHBoxLayout * authorLayout = new QHBoxLayout;

        nameLayout->addWidget( nameLabel );
        nameLayout->addWidget( nameEdit );

        authorLayout->addWidget( authorLabel );
        authorLayout->addWidget( authorEdit );

        layout2->addWidget( cancelButton );
        layout2->addWidget( acceptButton );

        layout1->addLayout( nameLayout );
        layout1->addLayout( authorLayout );
        layout1->addWidget( descriptionLabel );
        layout1->addWidget( descriptionEdit );
        layout1->addWidget( makeDefaultCheckBox );
        layout1->addWidget( errorMessageLabel );
        layout1->addLayout( layout2 );

        this->setLayout( layout1 );
    }

    virtual ~SavePresetDialog()
    {

    }
};

}


#endif // SAVEPRESETDIALOG_HPP
