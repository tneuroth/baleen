

#ifndef PHASEPLOTDEFINITIONDIALOGUE_HPP
#define PHASEPLOTDEFINITIONDIALOGUE_HPP

#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"

namespace TN
{

class PhasePlotDefinitionDialogue : public BaseDataDialogue
{
    Q_OBJECT

    std::map< std::string, PhasePlotDefinition > definedPhasePlots;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QVBoxLayout * PlotAttributeSection;
    QHBoxLayout * ConstraintSection;

    /////////////////////////////////////////////////////////////////////

    // Sampling Space

    QLabel    * spaceAttrLabel;
    QLabel    * spaceDimsLabel;

    QComboBox * spaceDimsCombo;

    QComboBox * spaceAttrCombo1;
    QComboBox * spaceAttrCombo2;
    QComboBox * spaceAttrCombo3;

    QVBoxLayout * spacec2;
    QVBoxLayout * spacec3;

    QHBoxLayout * spaceLayout;


    QComboBox * scaleMode;
    QLabel * scaleModeLabel;

    //////////////////////////////////////////////////////////////////////

    // plot Nameing

    QLabel    * plotNameingLabel;
    QLineEdit * plotNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // plot description

    QVBoxLayout * plotDescriptionLayout;
    QLabel    * plotDescriptionLabel;
    QTextEdit * plotDescriptionBox;


public:

    ~PhasePlotDefinitionDialogue();

signals:

    void cancelDefinition();

    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, PhasePlotDefinition > &,
        const std::map< std::string, BaseVariable > &,
        const std::map< std::string, DerivedVariable > &,
        const std::map< std::string, BaseConstant > &,
        const std::map< std::string, CustomConstant > & );

private slots:

    void plotDescriptionChanged();
    void changeSpaceDims( QString dimsStr );
    void finalize();
    void cancel();
    virtual void attemptDeriveVariable( ) override;

public:

    void update(
        const std::map< std::string, PhasePlotDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap );

    void fromDefinition( const PhasePlotDefinition & def );
    void setStatus( const std::string & status );

    PhasePlotDefinitionDialogue( std::string _key, QWindow *parentWindow );
};

}
#endif // PHASEPLOTDEFINITIONDIALOGUE_HPP

