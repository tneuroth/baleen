

#ifndef STATICPARTICLEFILTERDIALOGUE_HPP
#define STATICPARTICLEFILTERDIALOGUE_HPP

#include <cmath>

#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/ParticleFilter.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"

#include <QTableWidget>

namespace TN
{

class ParticleFilterDialogue : public BaseDataDialogue
{
    Q_OBJECT

    QPushButton * acceptButton;
    QPushButton * cancelButton;
    QLabel *errorMessageLabel;

    std::map< std::string, ParticleFilter > particleFilters;

    //////////////////////////////////////////////////////////////////////

    QLabel      * filterExpressionInstrLabel;
    QLabel      * filterExpressionLabel;
    QLineEdit   * filterExpressionEdit;
    QVBoxLayout * filterExpressionContainer;

    //////////////////////////////////////////////////////////////////////

    // Nameing

    QLabel    * filterNameingLabel;
    QLineEdit * filterNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // Description

    QVBoxLayout * filterDescriptionLayout;
    QLabel      * filterDescriptionLabel;
    QTextEdit   * filterDescriptionBox;

    // Time Sequence / Window for Detection

    QVBoxLayout * fullSeriesLayout;
    QLabel * fullSeriesLabel;
    QTableWidget * fullSeriesTable;
    QVBoxLayout * fullSeriesDescriptionLayout;
    QLabel    * fullSeriesDescriptionLabel;
    QTextEdit * fullSeriesDescriptionBox;

    QVBoxLayout * seriesLayout;
    QLabel * seriesLabel;
    QTableWidget * seriesTable;
    QVBoxLayout * seriesDescriptionLayout;
    QLabel    * seriesDescriptionLabel;
    QTextEdit * seriesDescriptionBox;

    I2 activeCell;
    bool seriesValid;

    std::vector< std::int32_t > simStepValues;
    std::vector< double > realTimeValues;

public:

    virtual ~ParticleFilterDialogue();

signals:

    void cancelDefinition();
    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, ParticleFilter > &,
        const std::map< std::string, BaseVariable > &,
        const std::map< std::string, DerivedVariable > &,
        const std::map< std::string, BaseConstant > &,
        const std::map< std::string, CustomConstant > & );

    void updateAttributes(
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, CustomConstant> & );

private slots:

    void setActiveCell( int i, int j );
    void seriesEdited( int i, int j );
    void finalize();
    void cancel();

public:

    void update(
        const std::map< std::string, ParticleFilter > & _filterMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap,
        const TimeSeriesDefinition & fullSeriesDefinition,
        const std::vector< double > & realTime,
        const std::vector< std::int32_t > & steps );

    void fromDefinition( const ParticleFilter & def );
    void setStatus( const std::string & status );

    ParticleFilterDialogue( std::string _key, QWindow *parentWindow );
};

}

#endif // STATICFILTERDIALOGUE_HPP
