#ifndef ROIDIALOG_HPP
#define ROIDIALOG_HPP

#include <set>

#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Data/Definitions/ROIDefinition.hpp"

namespace TN
{

class ROIDialog : public BaseDataDialogue
{

public:

    enum class Mode
    {
        Basic,
        FixedRegion,
        TemporospatialPath,
        spatialPath

    };

private:

    Q_OBJECT

    std::map< std::string, ROIDefinition > definedROIs;

    ////////////////////////////////////////////////////////////////////////

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    //////////////////////////////////////////////////////////////////////

    // Nameing

    QLabel    * roiNameingLabel;
    QLineEdit * roiNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // Description

    QVBoxLayout * roiDescriptionLayout;
    QLabel      * roiDescriptionLabel;
    QTextEdit   * roiDescriptionBox;

    ////////////////////////////////////////////////////////////////////////

    // ROI Selection

    QLabel * xLabel;
    QComboBox * xCombo;

    QLabel * yLabel;
    QComboBox * yCombo;

    QLabel * modeLabel;
    QComboBox * modeCombo;

    QHBoxLayout * spaceSelectionSubLayout1;
    QVBoxLayout * spaceSelectionLayout;

    // Fixed Mode

    QLabel * xrangeLowerLabel;
    QLineEdit * xLower;
    QLabel * xrangeUpperLabel;
    QLineEdit * xUpper;
    QLabel * yrangeLowerLabel;
    QLineEdit * yLower;
    QLabel * yrangeUpperLabel;
    QLineEdit * yUpper;

    QHBoxLayout * xRangeLayout;
    QHBoxLayout * yRangeLayout;

    QVBoxLayout * rangeLayout;
    QWidget * rangeWidget;

    // Temporo Mode

    QLabel * tsPathLabel;
    QComboBox * tsPathCombo;

    QHBoxLayout * spatioTemporoLayout;
    QWidget *  temporoWidget;

    // spatial or spatiotemporal

    QLabel * xWidthLabel;
    QLineEdit * xWidthEdit;

    QLabel * yWidthLabel;
    QLineEdit * yWidthEdit;

    QHBoxLayout * widthLayout;
    QWidget     * widthWidget;

    // Spatial Mode

    QLabel * sPathLabel;
    QComboBox * sPathCombo;

    QHBoxLayout * spatialPathLayout;
    QWidget * spatialWidget;

    ////////////////////////////////////////////////////////////////////////


public:

    virtual ~ROIDialog();

signals:

    void cancelDefinition();

    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, ROIDefinition > &,
        const std::map< std::string, BaseVariable > &,
        const std::map< std::string, DerivedVariable > &,
        const std::map< std::string, BaseConstant > &,
        const std::map< std::string, CustomConstant > & );

private slots:

    void updateMode( int index );
    void finalize();
    void cancel();

public:

    void setPaths(
        const std::set< std::string > & spatiotemporalPaths,
        const std::set< std::string > & spatialPaths );

    void update(
        const std::map< std::string, ROIDefinition > & _roiMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap );

    void fromDefinition( const ROIDefinition & def );

    void setStatus( const std::string & status );
    void setMode( Mode md );

    ROIDialog( std::string _key, QWindow *parentWindow );
};

}


#endif // ROIDIALOG_HPP
