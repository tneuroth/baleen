

#ifndef AdvancedParticleFilterDialogue_HPP
#define AdvancedParticleFilterDialogue_HPP

//#include "Vec.hpp"

//#include "data/Params/Attributes.hpp"
//#include "Filters/ParticleFilter.hpp"
//#include "data/Derivation/Expression.hpp"

//#include <QMessageBox>
//#include <QtMath>
//#include <QDialog>
//#include <QFormLayout>
//#include <QLabel>
//#include <QLineEdit>
//#include <QDialogButtonBox>
//#include <QDebug>
//#include <QCheckBox>
//#include <QPushButton>
//#include <QComboBox>
//#include <QCheckBox>
//#include <QListWidget>
//#include <QTextEdit>
//#include <QShortcut>

//#include <string>
//#include <vector>
//#include <set>
//#include <unordered_map>

//namespace TN {

//class AdvancedParticleFilterDialogue : public QDialog
//{
//    Q_OBJECT

//    QWindow * m_parentWindow;

//    const unsigned MAX_CONSTRAINTS = 9;

//    std::string filterKey;
//    std::string m_status;

//    ExpressionParser expressionParser;

//    std::map< std::string, ParticleFilter > particleFilters;
//    std::map< std::string, BaseVariable > baseVariables;
//    std::map< std::string, DerivedVariable > derivedVariables;
//    std::map< std::string, CustomConstant  > customConstants;
//    std::map< std::string, BaseConstant   > baseConstants;

//    QPushButton * acceptButton;
//    QPushButton * cancelButton;

//    QHBoxLayout * ConstraintSection;

//    QLabel *errorMessageLabel;

//    // Constraints Section

//    QVBoxLayout * rangeLayout;
//    QWidget     * rangeWidget;
//    QLabel      * rangeAttrLabel;
//    QLabel      * rangeGDLabel;

//    QPushButton * rangeAddConstraintButton;

//    QHBoxLayout * constraintColumnLabelLayuout;
//    QLabel * constraintAttrLabel;
//    QLabel * lowerBoundLabel;
//    QLabel * upperBoundLabel;
//    QLabel * startTimeLabel;
//    QLabel * endTimeLabel;
//    QLabel * timeActionLabel;

//    std::vector< QComboBox *> rangeAttrCombo;
//    std::vector< QComboBox *> timeActionCombo;
//    std::vector< QLineEdit *> rangeLineEdits1;
//    std::vector< QLineEdit *> rangeLineEdits2;
//    std::vector< QPushButton * > removeConstraintButtons;
//    std::vector< QHBoxLayout *> rangeAttrLayouts;
//    std::vector< QWidget *> rangeAttrWidgets;

//    //////////////////////////////////////////////////////////////////////

//    // Nameing

//    QLabel    * filterNameingLabel;
//    QLineEdit * filterNameingLineEdit;

//    ///////////////////////////////////////////////////////////////////////

//    // Description

//    QVBoxLayout * filterDescriptionLayout;
//    QLabel    * filterDescriptionLabel;
//    QTextEdit * filterDescriptionBox;

//    /////////////////////////////////////////////////////////////////////

//    // Data info Section

//    QListWidget * attributeList;
//    QListWidget * derivedAttributeList;
//    QListWidget * constantList;
//    QListWidget * userConstantList;

//    //QTextEdit * dataSetInfo;
//    QTextEdit * dataSetDescription;
//    //QTextEdit * selectedItemInfo;
//    QTextEdit * selectedItemDescription;

//    QWidget * infoWidget;

//    QVBoxLayout * mainInfoLayout;

//    QHBoxLayout * dataSetLayout;
//    QHBoxLayout * attributeInfoLayout;
//    QHBoxLayout * descriptionLayuout;


//    QVBoxLayout * attributeListLT;
//    QVBoxLayout * derivedAttributeListLT;
//    QVBoxLayout * constantListLT;

//    QVBoxLayout * dataSetInfoLT;
//    QVBoxLayout * dataSetDescriptionLT;
//    QVBoxLayout * selectedItemInfoLT;
//    QVBoxLayout * selectedItemDescriptionLT;

//    QLabel * attributeListLB;
//    QLabel * derivedAttributeListLB;
//    QLabel * constantListLB;

//    QLineEdit * deriveEdit;
//    QLineEdit * defineEdit;
//    QLabel * deriveLabel;
//    QLabel * defineLabel;
//    QLineEdit * deriveNameEdit;
//    QLineEdit * defineNameEdit;

////    QLabel * dataSetInfoLB;
//    QLabel * dataSetDescriptionLB;
////    QLabel * selectedItemInfoLB;
//    QLabel * selectedItemDescriptionLB;

//    QPushButton * addDerivedButton;
//    QPushButton * addConstantButton;

//    ////////////////////////////////////////////////////////////////////

//public:

//    ~AdvancedParticleFilterDialogue() {}

//signals:

//    void cancelDefinition();
//    void finalizeDefinition(
//        const std::string &,
//        const std::map< std::string, ParticleFilter > &,
//        const std::map< std::string, BaseVariable > &,
//        const std::map< std::string, DerivedVariable > &,
//        const std::map< std::string, BaseConstant > &,
//        const std::map< std::string, CustomConstant > & );

//    void updateDerivations(
//        const std::map<std::string, DerivedVariable> &,
//        const std::map<std::string, CustomConstant> & );

//private slots:

//    void selectedItemDescriptionChanged()
//    {
//        QString var = selectedItemDescriptionLB->text().split(" ").at(0);

//        {
//            auto it = baseVariables.find( var.toStdString() );
//            if( it != baseVariables.end() )
//            {
//                it->second.description( selectedItemDescription->toPlainText().toStdString() );
//                return;
//            }
//        }
//        {
//            auto it = derivedVariables.find( var.toStdString() );
//            if( it != derivedVariables.end() )
//            {
//                it->second.description( selectedItemDescription->toPlainText().toStdString() );
//                return;
//            }
//        }
//        {
//            auto it = baseConstants.find( var.toStdString() );
//            if( it != baseConstants.end() )
//            {
//                it->second.description( selectedItemDescription->toPlainText().toStdString() );
//                return;
//            }
//        }
//        {
//            auto it = customConstants.find( var.toStdString() );
//            if( it != customConstants.end() )
//            {
//                it->second.description( selectedItemDescription->toPlainText().toStdString() );
//                return;
//            }
//        }
//    }

//    void attributeFocused(QListWidgetItem *lwi )
//    {
//        QString text = lwi->text();
//        auto it = baseVariables.find( text.toStdString() );
//        if( it != baseVariables.end() )
//        {
//            std::string desc = it->second.description();
//            selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
//            selectedItemDescription->setText( desc.c_str() );
//        }
//    }
//    void constantFocused(QListWidgetItem *lwi)
//    {
//        QString text = lwi->text();
//        QString first = text.split(" ").at(0);
//        auto it = baseConstants.find( first.toStdString() );
//        if( it != baseConstants.end() )
//        {
//            std::string desc = it->second.description();
//            selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
//            selectedItemDescription->setText( desc.c_str() );
//        }
//    }
//    void userConstantFocused(QListWidgetItem *lwi)
//    {
//        QString text = lwi->text();
//        QString first = text.split(" ").at(0);
//        auto it = customConstants.find( first.toStdString() );
//        if( it != customConstants.end() )
//        {
//            std::string desc = it->second.description();
//            selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
//            selectedItemDescription->setText( desc.c_str() );
//        }
//    }
//    void derivedVariableFocused(QListWidgetItem *lwi)
//    {
//        QString text = lwi->text();
//        QString first = text.split(" ").at(0);
//        auto it = derivedVariables.find( first.toStdString() );
//        if( it != derivedVariables.end() )
//        {
//            std::string desc = it->second.description();
//            selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
//            selectedItemDescription->setText( desc.c_str() );
//        }
//    }

//    void removeDomainConstraint( int index )
//    {
//        delete rangeAttrWidgets[ index ];

//        rangeAttrCombo.erase(       rangeAttrCombo.begin() + index );
//        rangeAttrLayouts.erase(   rangeAttrLayouts.begin() + index );
//        rangeAttrWidgets.erase(   rangeAttrWidgets.begin() + index );
//        timeActionCombo.erase(     timeActionCombo.begin() + index );
//        rangeLineEdits1.erase(     rangeLineEdits1.begin() + index );
//        rangeLineEdits2.erase(     rangeLineEdits2.begin() + index );

//        removeConstraintButtons.erase( removeConstraintButtons.begin() + index );
//    }

//    void removeDomainConstraint()
//    {
//        QPushButton * btn = ( QPushButton * ) QObject::sender();
//        QWidget * prnt = btn->parentWidget();

//        for( unsigned index = 0; index < rangeAttrWidgets.size(); ++index )
//        {
//            if( rangeAttrWidgets[ index ] == prnt )
//            {
//                removeDomainConstraint( index );
//                return;
//            }
//        }
//    }

//    bool validateNumber( QString number )
//    {
//        bool isNumber;
//        double value = number.toDouble( &isNumber );
//        if( ! isNumber )
//        {
//            return false;
//        }
//        if( std::isinf( value ) || std::isnan( value ) )
//        {
//            return false;
//        }
//        return true;
//    }

//    bool validateInteger( QString number )
//    {
//        std::string nm = number.toStdString();

//        if( ! validateNumber( number ) )
//        {
//            return false;
//        }
//        if( nm.find_first_of( "123456789" ) != 0  )
//        {
//            return false;
//        }
//        if( nm.find_first_not_of( "1234567890Ee+" ) != nm.npos  )
//        {
//            return false;
//        }
//        return true;
//    }

//    bool validateName( QString name )
//    {
//        // make sure it's not in use, and no spaces, tabs, or newlines please!

//        std::string _name = name.toStdString();

//        if( name == "" )
//        {
//            return false;
//        }

//        if( TN::keyWords.find( _name ) != TN::keyWords.end() )
//        {
//            return false;
//        }

//        if( _name.find_first_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_" ) != 0  )
//        {
//            return false;
//        }

//        if( _name.find_first_not_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890" ) != name.toStdString().npos  )
//        {
//            return false;
//        }

//        if( baseVariables.find( _name ) != baseVariables.end() )
//        {
//            return false;
//        }

//        if( derivedVariables.find( _name ) != derivedVariables.end() )
//        {
//            return false;
//        }

//        if( baseConstants.find( _name ) != baseConstants.end() )
//        {
//            return false;
//        }

//        if( customConstants.find( _name ) != customConstants.end() )
//        {
//            return false;
//        }

//        return true;
//    }

//    bool validateExpression( QString expression_qstr )
//    {
//        return expressionParser.validate(
//            expression_qstr.toStdString(),
//            baseVariables,
//            derivedVariables,
//            customConstants,
//            baseConstants );
//    }

//    bool validateConstExpression( QString expression_qstr )
//    {
//        return expressionParser.validateConstExpression( expression_qstr.toStdString(), baseConstants, customConstants );
//    }

//    bool validateIsVariant( QString expression_qstr )
//    {
//        return expressionParser.isVariant( expression_qstr.toStdString(), baseVariables, derivedVariables );
//    }

//    void nameLineEditChanged( QString text )
//    {
//        QLineEdit * edit = (QLineEdit*) QObject::sender();
//        std::string cl( edit == deriveNameEdit ? "purple" : edit == defineNameEdit ? "rgb(200,90,0)" : "black" );

//        if( validateName( text ) )
//        {
//            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
//        }
//        else
//        {
//            edit->setStyleSheet( "QLineEdit { color : gray; }" );
//        }
//    }

//    void deleteListWidgetItem()
//    {

//        if( userConstantList->hasFocus() )
//        {
//            if( QMessageBox::question(this, "", "Are you sure you want to delete this constant?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes )
//            {
//                return;
//            }

//            std::string txt = userConstantList->currentItem()->text().toStdString();
//            delete userConstantList->currentItem();
//            customConstants.erase( txt );
//        }
//        if( derivedAttributeList->hasFocus() )
//        {
//            if( QMessageBox::question(this, "", "Are you sure you want to delete this expression?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes )
//            {
//                return;
//            }

//            std::string txt = derivedAttributeList->currentItem()->text().toStdString();
//            delete derivedAttributeList->currentItem();
//            derivedVariables.erase( txt );
//        }
//    }

//    void constExpressionLineEditChanged( QString text )
//    {
//        QLineEdit * edit = (QLineEdit*) QObject::sender();

//        std::string cl = "purple";

//        if( validateConstExpression( text ) )
//        {
//            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
//        }
//        else
//        {
//            edit->setStyleSheet( "QLineEdit { color : gray; }" );
//        }
//    }

//    void expressionLineEditChanged( QString text )
//    {
//        QLineEdit * edit = (QLineEdit*) QObject::sender();

//        std::string cl( edit == deriveEdit ? "purple" : "black" );

//        if( validateExpression( text ) )
//        {
//            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
//        }
//        else
//        {
//            edit->setStyleSheet( "QLineEdit { color : gray; }" );
//        }
//    }

//    void valueLineEditChanged( QString text )
//    {
//        QLineEdit * edit = (QLineEdit*) QObject::sender();

//        std::string cl( edit == defineEdit ? "rgb(20,20,20)" : "black" );

//        if( validateNumber( text ) )
//        {
//            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
//        }
//        else
//        {
//            edit->setStyleSheet( "QLineEdit { color : gray; }" );
//        }
//    }

//    void valueEditChanged( QString text )
//    {
//        QLineEdit * edit = (QLineEdit*) QObject::sender();

//        std::string cl( "rgb(20,20,20)" );

//        if( validateNumber( text ) )
//        {
//            edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
//        }
//        else
//        {
//            edit->setStyleSheet( "QLineEdit { color : gray; }" );
//        }
//    }

//    void attemptDefineConstant( )
//    {
//        if( ! validateName( defineNameEdit->text() ) )
//        {
//            return;
//        }

//        if( ! validateConstExpression( defineEdit->text() ) )
//        {
//            return;
//        }

//        CustomConstant cc;
//        cc.name( defineNameEdit->text().toStdString() );
//        cc.expression( defineEdit->text().toStdString() );

//        customConstants.insert( { cc.name(), cc } );

//        std::string _nm = cc.name() + " = " + cc.expression();
//        if( ! expressionParser.validateNumber( cc.expression() ) )
//        {
//            _nm += " = " + std::to_string( expressionParser.evaluateConstExpression( cc.expression(), baseConstants, customConstants ) );
//        }
//        userConstantList->addItem( _nm.c_str() );
//        defineNameEdit->setStyleSheet( "QLineEdit { color : gray; }" );

//        emit updateDerivations( derivedVariables, customConstants );
//    }

//    void attemptDeriveVariable( )
//    {
//        if( ! validateName( deriveNameEdit->text() ) )
//        {
//            return;
//        }

//        if( ! validateExpression( deriveEdit->text() ) )
//        {
//            return;
//        }

//        if( ! validateIsVariant( deriveEdit->text() ) )
//        {
//            return;
//        }

//        DerivedVariable dv;
//        dv.name( deriveNameEdit->text().toStdString() );
//        dv.expression( deriveEdit->text().toStdString() );
//        derivedVariables.insert( { dv.name(), dv } );
//        derivedAttributeList->addItem( ( dv.name() + " = " + dv.expression() ).c_str() );
//        deriveNameEdit->setStyleSheet( "QLineEdit { color : gray; }" );

//        for( auto & c : rangeAttrCombo )
//        {
//            c->addItem( ( dv.name() + " = " + dv.expression() ).c_str() );
//        }

//        emit updateDerivations( derivedVariables, customConstants );
//    }

//    void updateRangeLineEdits( QString str )
//    {
//        QComboBox * comboPtr = (QComboBox*) QObject::sender();
//        int index = -1;

//        for( int i = 0; i < rangeAttrCombo.size(); ++i )
//        {
//            if( comboPtr == rangeAttrCombo[ i ] )
//            {
//                index = i;
//                break;
//            }
//        }

//        if( index >= 0 )
//        {
//            const Vec2< double > & r = baseVariables.find( str.toStdString() )->second.range();
//            rangeLineEdits1[ index ]->setText( QString::number( r.a(), 'g', 6 ) );
//            rangeLineEdits2[ index ]->setText( QString::number( r.b(), 'g', 6 ) );
//        }
//    }

//    void addRangeConstraint()
//    {
//        if( rangeAttrCombo.size() >= MAX_CONSTRAINTS )
//        {
//            return;
//        }

//        rangeAttrCombo.push_back( new QComboBox() );
//        rangeAttrCombo.back()->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green; }" );

//        timeActionCombo.push_back( new QComboBox );

//        timeActionCombo.back()->addItem( "use if always met" );
//        timeActionCombo.back()->addItem( "use when met" );
//        timeActionCombo.back()->addItem( "use before violated" );

//        rangeLineEdits1.push_back( new QLineEdit() );
//        rangeLineEdits2.push_back( new QLineEdit() );

//        rangeLineEdits1.back()->setMinimumWidth( 100 );
//        rangeLineEdits2.back()->setMinimumWidth( 100 );

//        removeConstraintButtons.push_back( new QPushButton( "-" ) );
//        removeConstraintButtons.back()->setStyleSheet( "QPushButton { background-color: white; color : red; }" );
//        removeConstraintButtons.back()->setMaximumWidth( 24 );

//        connect( removeConstraintButtons.back(), SIGNAL( clicked(bool) ), this, SLOT( removeDomainConstraint() ) );

//        rangeAttrLayouts.push_back(  new QHBoxLayout() );
//        rangeAttrLayouts.back()->setMargin( 0 );
//        rangeAttrLayouts.back()->setSpacing( 0 );

//        rangeAttrLayouts.back()->addWidget( rangeAttrCombo.back() );
//        rangeAttrLayouts.back()->addWidget( rangeLineEdits1.back() );
//        rangeAttrLayouts.back()->addWidget( rangeLineEdits2.back() );
//        rangeAttrLayouts.back()->addWidget( timeActionCombo.back() );
//        rangeAttrLayouts.back()->addWidget( removeConstraintButtons.back() );
//        rangeAttrWidgets.push_back( new QWidget );
//        rangeAttrWidgets.back()->setLayout( rangeAttrLayouts.back() );
//        rangeLayout->addWidget( rangeAttrWidgets.back() );

//        for( auto & baseVarE : baseVariables )
//        {
//            rangeAttrCombo.back()->addItem( baseVarE.first.c_str() );
//            const Vec2< double > & r = baseVarE.second.range();
//            rangeLineEdits1.back()->setText( QString::number( r.a(), 'g', 6 ) );
//            rangeLineEdits2.back()->setText( QString::number( r.b(), 'g', 6 ) );
//        }
//        for( auto & derived : derivedVariables )
//        {
//            rangeAttrCombo.back()->addItem( ( derived.first + " = " + derived.second.expression() ).c_str() );
//            const Vec2< double > & r = derived.second.range();
//            rangeLineEdits1.back()->setText( QString::number( r.a(), 'g', 6 ) );
//            rangeLineEdits2.back()->setText( QString::number( r.b(), 'g', 6 ) );
//        }

//        connect(rangeAttrCombo.back(), SIGNAL(currentTextChanged(QString)), this, SLOT(updateRangeLineEdits(QString)) );
//    }

//    void finalize()
//    {
//        std::string name = filterNameingLineEdit->text().toStdString();
//        if( name.size() == 0 )
//        {
//            errorMessageLabel->setText( "Invalid Name/key" );
//            return;
//        }
//        if( ( m_status != "defined" ) && ( particleFilters.find( name ) != particleFilters.end() ) )
//        {
//            errorMessageLabel->setText( "Name/key already in use" );
//            return;
//        }
//        if( name == "temp" )
//        {
//            errorMessageLabel->setText( "Invalid Name/key" );
//            return;
//        }

//        ParticleFilter definition;
//        definition.name = filterKey;
//        definition.description = filterDescriptionBox->toPlainText().toStdString();

//        for( unsigned i = 0; i < rangeAttrCombo.size(); ++i )
//        {
//            DomainConstraint dc;
//            dc.variableName = rangeAttrCombo[ i ]->currentText().toStdString();
//            dc.timeAction   = timeActionCombo[ i ]->currentText().toStdString();

//            if( ! ( validateNumber( rangeLineEdits1[ i ]->text() ) && validateNumber( rangeLineEdits2[ i ]->text() ) ) )
//            {
//                errorMessageLabel->setText( "Domain constraint must be numeric" );
//                return;
//            }
//            Vec2< float > rng( std::stof( rangeLineEdits1[ i ]->text().toStdString() ), std::stof( rangeLineEdits2[ i ]->text().toStdString() ) );
//            if( rng.b() <= rng.a() )
//            {
//                errorMessageLabel->setText( "Null domain constraint, upper must be > lower" );
//                return;
//            }

//            dc.domain = rng;
//            definition.domainConstraints.push_back( dc );
//        }


//        if( m_status == "undefined" )
//        {
//            particleFilters.insert( { filterKey, definition } );
//        }
//        else
//        {
//            particleFilters.find( filterKey )->second = definition;
//        }

//        setStatus( "defined" );

//        emit finalizeDefinition(
//            filterKey,
//            particleFilters,
//            baseVariables,
//            derivedVariables,
//            baseConstants,
//            customConstants
//        );

//        close();
//    }

//    void cancel()
//    {
//        emit cancelDefinition();
//        close();
//    }

//public:

//    void update(
//        const std::map< std::string, ParticleFilter > & _filterMap,
//        const std::map< std::string, BaseVariable > & _baseVarMap,
//        const std::map< std::string, DerivedVariable > & _derivedVarMap,
//        const std::map< std::string, CustomConstant  > & _customConstMap,
//        const std::map< std::string, BaseConstant   > & _baseConstMap )
//    {
//        if( m_status == "defined" )
//        {
//            for( auto & dv : _derivedVarMap )
//            {
//                if( ! derivedVariables.count( dv.second.name() ) )
//                {
//                    std::string _nm = dv.second.name() + " = " + dv.second.expression();
//                    derivedAttributeList->addItem( _nm.c_str() );
//                    for( auto & c : rangeAttrCombo )
//                    {
//                        c->addItem( _nm.c_str() );
//                    }
//                }
//            }
//            for( auto & cc : _customConstMap)
//            {
//                if( ! customConstants.count( cc.second.name() ) )
//                {
//                    std::string _nm = cc.second.name()
//                            + " = " + cc.second.expression();

//                    if( ! expressionParser.validateNumber( cc.second.expression() ) )
//                    {
//                        _nm += " = " + std::to_string( expressionParser.evaluateConstExpression( cc.second.expression(), _baseConstMap, _customConstMap ) );
//                    }
//                    userConstantList->addItem( _nm.c_str() );
//                }
//            }
//        }

//        baseVariables = _baseVarMap;
//        derivedVariables = _derivedVarMap;
//        customConstants = _customConstMap;
//        baseConstants = _baseConstMap;
//        particleFilters = _filterMap;

//        if( m_status != "defined" )
//        {
//            attributeList->setStyleSheet( "QListWidget { color : green }" );
//            constantList->setStyleSheet( "QListWidget { color : green }" );

//            constantList->clear();
//            userConstantList->clear();
//            derivedAttributeList->clear();
//            attributeList->clear();

//            for( auto & c : rangeAttrCombo )
//            {
//                c->clear();
//            }

//            for( auto & baseConst : _baseConstMap )
//            {
//                std::string _nm = baseConst.first + " = " + std::to_string( baseConst.second.value() );
//                constantList->addItem( _nm.c_str() );
//            }

//            for( auto & custConst : _customConstMap )
//            {
//                std::string _nm = custConst.second.name()
//                        + " = " + custConst.second.expression();

//                if( ! expressionParser.validateNumber( custConst.second.expression() ) )
//                {
//                    _nm += " = " + std::to_string( expressionParser.evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ) );
//                }

//                userConstantList->addItem( _nm.c_str() );
//            }

//            for( auto & dv : _derivedVarMap )
//            {
//                std::string _nm = dv.second.name() + " = " + dv.second.expression();
//                derivedAttributeList->addItem( _nm.c_str() );

//                for( auto & c : rangeAttrCombo )
//                {
//                    c->addItem( _nm.c_str() );
//                }
//            }

//            for( auto & baseVarE : baseVariables )
//            {
//                std::string _nm = baseVarE.first.c_str();

//                attributeList->addItem( _nm.c_str() );

//                for( auto & c : rangeAttrCombo )
//                {
//                    c->addItem( _nm.c_str() );
//                }
//            }
//        }
//    }

//    void setParameters( const ParticleFilter & def )
//    {
//        filterDescriptionBox->setText( def.description.c_str() );

//        while( removeConstraintButtons.size() )
//        {
//            removeDomainConstraint( removeConstraintButtons.size() - 1 );
//        }

//        for( unsigned i = 0; i < def.domainConstraints.size(); ++i )
//        {
//            addRangeConstraint();

//            for( auto idx = 0; idx < rangeAttrCombo.back()->count(); ++idx )
//            {
//                if( rangeAttrCombo.back()->itemText( idx ).toStdString() == def.domainConstraints[ i ].variableName )
//                {
//                    rangeAttrCombo.back()->setCurrentIndex( idx );
//                    break;
//                }
//            }

//            rangeLineEdits1.back()->setText( QString::number( def.domainConstraints[ i ].domain.a(), 'g', 6 ) );
//            rangeLineEdits2.back()->setText( QString::number( def.domainConstraints[ i ].domain.b(), 'g', 6 ) );

//            for( auto idx = 0; idx < timeActionCombo.back()->count(); ++idx )
//            {
//                if( timeActionCombo.back()->itemText( idx ).toStdString() == def.domainConstraints[ i ].timeAction )
//                {
//                    timeActionCombo.back()->setCurrentIndex( idx );
//                    break;
//                }
//            }
//        }
//    }

//    void setStatus( const std::string & status )
//    {
//        m_status = status;
//        if( status == "defined" )
//        {
//            acceptButton->setText( "update" );
//            filterNameingLineEdit->setEnabled( false );
//        }
//    }

//    AdvancedParticleFilterDialogue( std::string _key, QWindow *parentWindow ) : QDialog( 0 ), m_parentWindow( parentWindow ), filterKey( _key ), m_status( "undefined" )
//    {
//        setStyleSheet("  QWidget { background-color: rgb( 240, 240, 240 ); } QLineEdit { background-color: white; } QComboBox { background-color: white; } QListWidget { background-color: white; }");

//        ConstraintSection    = new QHBoxLayout;

//        acceptButton = new QPushButton( "create" );
//        cancelButton = new QPushButton( "cancel" );
//        filterNameingLabel = new QLabel( "Filter Name/Key" );
//        filterNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
//        filterNameingLineEdit = new QLineEdit;

//        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
//        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

//        ///////////////////////////////////////////////////////////////////////////

//        // Type / Calculation / Expressions / Validation

//        errorMessageLabel = new QLabel;
//        errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );


//        //////////////////////////////////////////////////////////////////////////////

//        // Constraint Section

//        QVBoxLayout *rvb = new QVBoxLayout;

//        rangeWidget = new QWidget();
//        rangeLayout = new QVBoxLayout();
//        rangeLayout->setMargin( 0 );
//        rangeLayout->setSpacing( 0 );

//        rangeAttrLabel = new QLabel("Attribute");
//        rangeGDLabel   = new QLabel( "Order" );

//        constraintColumnLabelLayuout = new QHBoxLayout;

//        constraintAttrLabel = new QLabel( "Attribute" );
//        lowerBoundLabel = new QLabel( "Lower Bound" );
//        upperBoundLabel = new QLabel( "Upper Bound" );
//        timeActionLabel = new QLabel( "Option" );

//        constraintColumnLabelLayuout->addWidget( constraintAttrLabel );
//        constraintColumnLabelLayuout->addWidget( lowerBoundLabel     );
//        constraintColumnLabelLayuout->addWidget( upperBoundLabel     );
//        constraintColumnLabelLayuout->addWidget( timeActionLabel     );

//        rangeAddConstraintButton = new QPushButton( "Add Domain Constraint" );

//        QLabel * constraintsLabel = new QLabel( "Domain Constraints" );
//        constraintsLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
//        QHBoxLayout * csHb = new QHBoxLayout;
//        csHb->addWidget(  constraintsLabel );
//        csHb->addStretch();

//        rvb->setMargin( 0 );
//        rvb->addLayout( csHb );
//        rvb->addLayout( constraintColumnLabelLayuout );
//        rvb->addLayout( rangeLayout );
//        rvb->addWidget( rangeAddConstraintButton );
//        rangeWidget->setLayout( rvb );

//        ConstraintSection->addWidget( rangeWidget );

//        connect (rangeAddConstraintButton, SIGNAL(clicked()), this, SLOT(addRangeConstraint()) );

//        ///////////////////////////////////////////////////////////////////////////////

//        // Description

//        filterDescriptionLayout = new QVBoxLayout;
//        filterDescriptionLabel = new QLabel( "Filter Description" );
//        filterDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
//        filterDescriptionBox = new QTextEdit;
//        filterDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

//        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
//        kjncewionoicejnoinwe->addWidget( filterDescriptionLabel );
//        kjncewionoicejnoinwe->addStretch();

//        filterDescriptionLayout->addLayout( kjncewionoicejnoinwe );
//        filterDescriptionLayout->addWidget( filterDescriptionBox );
//        filterDescriptionLayout->addWidget( errorMessageLabel );

//        //////////////////////////////////////////////////////////////////////////////

//        // Info Widget // note should make this one a module, as it can be used in a variety of contexts

//        attributeList        = new QListWidget;
//        derivedAttributeList = new QListWidget;
//        constantList         = new QListWidget;
//        userConstantList     = new QListWidget;

//        userConstantList->setStyleSheet( "QListWidget { color : purple; }" );
//        attributeList->setStyleSheet( "QListWidget { background-color : white; }" );
//        derivedAttributeList->setStyleSheet( "QListWidget { background-color : white; color : purple; }" );
//        constantList->setStyleSheet( "QListWidget { background-color : white; }" );
//        QShortcut* shortcut1 = new QShortcut(QKeySequence(Qt::Key_Delete), userConstantList     );
//        QShortcut* shortcut2 = new QShortcut(QKeySequence(Qt::Key_Delete), derivedAttributeList );

//        connect( shortcut1, SIGNAL(activatedAmbiguously()), this, SLOT(deleteListWidgetItem()));
//        connect( shortcut2, SIGNAL(activatedAmbiguously()), this, SLOT(deleteListWidgetItem()));

//        connect( attributeList,        SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( attributeFocused(QListWidgetItem*) ) );
//        connect( constantList,         SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( constantFocused(QListWidgetItem*) ) );
//        connect( userConstantList,     SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( userConstantFocused(QListWidgetItem*) ) );
//        connect( derivedAttributeList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( derivedVariableFocused(QListWidgetItem*) ) );

//        //dataSetInfo             = new QTextEdit;
//        dataSetDescription      = new QTextEdit;
//        //selectedItemInfo        = new QTextEdit;
//        selectedItemDescription = new QTextEdit;

//        connect( selectedItemDescription, SIGNAL(textChanged()), this, SLOT( selectedItemDescriptionChanged()) );

//        //dataSetInfo->setMinimumWidth( 400 );
//        dataSetDescription->setMinimumWidth( 500 );
//        //selectedItemInfo->setMinimumWidth( 400 );
//        selectedItemDescription->setMinimumWidth( 500 );

//        //dataSetInfo->setStyleSheet( "QTextEdit { background-color : white; }" );
//        dataSetDescription->setStyleSheet( "QTextEdit { background-color : white; }" );
//        //selectedItemInfo->setStyleSheet( "QTextEdit { background-color : white; }" );
//        selectedItemDescription->setStyleSheet( "QTextEdit { background-color : white; }" );

//        infoWidget = new QWidget;

//        mainInfoLayout      = new QVBoxLayout;
//        dataSetLayout       = new QHBoxLayout;
//        attributeInfoLayout = new QHBoxLayout;
//        descriptionLayuout  = new QHBoxLayout;

//        attributeListLT           = new QVBoxLayout;
//        derivedAttributeListLT    = new QVBoxLayout;
//        constantListLT            = new QVBoxLayout;
//        dataSetInfoLT             = new QVBoxLayout;
//        dataSetDescriptionLT      = new QVBoxLayout;
//        selectedItemInfoLT        = new QVBoxLayout;
//        selectedItemDescriptionLT = new QVBoxLayout;

//        attributeListLB           = new QLabel( "Base Variables"    );
//        derivedAttributeListLB    = new QLabel( "Derived Variables" );
//        constantListLB            = new QLabel( "Stock Constants"   );
//        QLabel * userConstantsLB  = new QLabel( "Custom Constants"  );

//        //dataSetInfoLB             = new QLabel( "Dataset Info" );
//        dataSetDescriptionLB      = new QLabel( "Dataset Description" );
//        //selectedItemInfoLB        = new QLabel( "Selected Variable Name Here" );
//        selectedItemDescriptionLB = new QLabel( "Selected Item Description" );

//        QVBoxLayout * addDerivedButtonLayout  = new QVBoxLayout;
//        QVBoxLayout * addConstantButtonLayout = new QVBoxLayout;

//        addDerivedButton  = new QPushButton( "Derive" );
//        addConstantButton = new QPushButton( "Define" );

//        connect( addConstantButton, SIGNAL( clicked(bool) ), this, SLOT( attemptDefineConstant() ) );
//        connect( addDerivedButton,  SIGNAL( clicked(bool) ), this, SLOT( attemptDeriveVariable() ) );

//        deriveEdit  = new QLineEdit;
//        defineEdit  = new QLineEdit;

//        deriveLabel = new QLabel( "=" );
//        defineLabel = new QLabel( "=" );

//        deriveNameEdit = new QLineEdit;
//        defineNameEdit = new QLineEdit;

//        connect( deriveNameEdit, SIGNAL(textChanged(QString)), this, SLOT(nameLineEditChanged(QString)) );
//        connect( deriveEdit, SIGNAL(textChanged(QString)), this, SLOT(expressionLineEditChanged(QString)) );
//        connect( defineNameEdit, SIGNAL(textChanged(QString)), this, SLOT(nameLineEditChanged(QString)) );
//        connect( defineEdit, SIGNAL(textChanged(QString)), this, SLOT(constExpressionLineEditChanged(QString)) );

//        QHBoxLayout * derivedLayout1 = new QHBoxLayout;
//        QHBoxLayout * derivedLayout2 = new QHBoxLayout;

//        derivedLayout1->addWidget( addDerivedButton );
//        derivedLayout1->addWidget( deriveNameEdit );
//        derivedLayout2->addWidget( deriveLabel );
//        derivedLayout2->addWidget( deriveEdit );

//        addDerivedButtonLayout->addLayout( derivedLayout1 );
//        addDerivedButtonLayout->addLayout( derivedLayout2 );

//        QHBoxLayout * defineLayout1 = new QHBoxLayout;
//        QHBoxLayout * defineLayout2 = new QHBoxLayout;

//        defineLayout1->addWidget( addConstantButton );
//        defineLayout1->addWidget( defineNameEdit );
//        defineLayout2->addWidget( defineLabel );
//        defineLayout2->addWidget( defineEdit );

//        addConstantButtonLayout->addLayout( defineLayout1 );
//        addConstantButtonLayout->addLayout( defineLayout2 );

//        attributeListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        derivedAttributeListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        //dataSetInfoLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        constantListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        dataSetDescriptionLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        selectedItemDescriptionLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        //selectedItemInfoLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
//        userConstantsLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );

//        attributeListLT->addWidget( attributeListLB );
//        attributeListLT->addWidget( attributeList );
//        attributeListLT->addWidget( derivedAttributeListLB );
//        attributeListLT->addWidget( derivedAttributeList );
//        attributeListLT->addLayout( addDerivedButtonLayout );

//        constantListLT->addWidget( constantListLB );
//        constantListLT->addWidget( constantList );
//        constantListLT->addWidget( userConstantsLB );
//        constantListLT->addWidget( userConstantList );
//        constantListLT->addLayout( addConstantButtonLayout );

//        //dataSetInfoLT->addWidget( dataSetInfoLB );
//        dataSetDescriptionLT->addWidget( dataSetDescriptionLB );
//        //selectedItemInfoLT->addWidget( selectedItemInfoLB );
//        selectedItemDescriptionLT->addWidget( selectedItemDescriptionLB );

//        //dataSetInfoLT->addWidget( dataSetInfo );
//        dataSetDescriptionLT->addWidget( dataSetDescription );
//        //selectedItemInfoLT->addWidget( selectedItemInfo );
//        selectedItemDescriptionLT->addWidget( selectedItemDescription );

//        dataSetLayout->addLayout( dataSetInfoLT );
//        dataSetLayout->addLayout( dataSetDescriptionLT );
//        attributeInfoLayout->addLayout( attributeListLT );

//        QFrame *l1kjudfdfhsdkd = new QFrame;
//        l1kjudfdfhsdkd->setFrameShape( QFrame::VLine );
//        l1kjudfdfhsdkd->setFrameShadow( QFrame::Sunken );
//        l1kjudfdfhsdkd->setStyleSheet( "QFrame { background-color : white; }" );
//        attributeInfoLayout->addWidget( l1kjudfdfhsdkd );
//        attributeInfoLayout->addLayout( constantListLT );

//        descriptionLayuout->addLayout( selectedItemInfoLT );
//        descriptionLayuout->addLayout( selectedItemDescriptionLT );

//        QFrame *l1kjuhsdkd = new QFrame;
//        l1kjuhsdkd->setFrameShape( QFrame::HLine );
//        l1kjuhsdkd->setFrameShadow( QFrame::Sunken );
//        l1kjuhsdkd->setStyleSheet( "QFrame { background-color : white; }" );

//        QFrame *l1kjuhssddkd = new QFrame;
//        l1kjuhssddkd->setFrameShape( QFrame::HLine );
//        l1kjuhssddkd->setFrameShadow( QFrame::Sunken );
//        l1kjuhssddkd->setStyleSheet( "QFrame { background-color : white; }" );

//        mainInfoLayout->addLayout( dataSetLayout );
//        mainInfoLayout->addWidget( l1kjuhsdkd );
//        mainInfoLayout->addLayout( attributeInfoLayout );
//        mainInfoLayout->addWidget( l1kjuhssddkd );
//        mainInfoLayout->addLayout( descriptionLayuout );

//        infoWidget->setLayout( mainInfoLayout );

//        //////////////////////////////////////////////////////////////////////////////

//        // main layout

//        QHBoxLayout * higherLevelLayout = new QHBoxLayout;
//        QVBoxLayout * mainLayout        = new QVBoxLayout;

//        mainLayout->addLayout( ConstraintSection );
//        mainLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );

//        mainLayout->addLayout( filterDescriptionLayout );

//        QHBoxLayout *dll = new QHBoxLayout;
//        dll->addWidget( filterNameingLabel );
//        dll->addWidget( filterNameingLineEdit );
//        dll->addWidget( cancelButton );
//        dll->addWidget( acceptButton );
//        mainLayout->addLayout( dll );

//        higherLevelLayout->addWidget( infoWidget );
//        higherLevelLayout->addLayout( mainLayout );

//        setLayout( higherLevelLayout );
//    }
//};

//}

#endif // AdvancedParticleFilterDialogue_HPP
