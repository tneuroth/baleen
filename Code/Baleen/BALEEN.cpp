

#include "BALEEN.hpp"

#include "Dialogues/PhasePlotDefinitionDialogue.hpp"
#include "Dialogues/TimePlotDefinitionDialogue.hpp"
#include "Dialogues/TimeSeriesDefinitionDialogue.hpp"
#include "Dialogues/HistogramDefinitionDialogue.hpp"
#include "Dialogues/ParticleFilterDialogue.hpp"
#include "Dialogues/EditGridProjectionDialogue.hpp"
#include "Dialogues/ROIDialog.hpp"

#include "Algorithms/IsoContour/Contour.hpp"

#include "Algorithms/Histogram/GenerateGeometry.hpp"
#include "Data/MetadataParsers/ProjectParser.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Algorithms/Standard/Util.hpp"
#include "Data/Configuration/ParticleBasedConfiguration.hpp"
#include "Data/Configuration/DistributionBasedConfiguration.hpp"

#include "Data/Managers/DataSetManager.hpp"
#include "Algorithms/IsoContour/CubeMarcher.hpp"
#include "OpenGL/Rendering/Renderer.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"
#include <Algorithms/Standard/GridSmoother.hpp>
#include "OpenGL/Widgets/RecurrenceView.hpp"

#include <QGLFramebufferObjectFormat>
#include <QOpenGLFramebufferObject>
#include <QApplication>
#include <QDesktopServices>
#include <QCoreApplication>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QWheelEvent>
#include <QDir>
#include <QVector3D>

#include <omp.h>
#include <thread>
#include <atomic>
#include <set>
#include <chrono>
#include <ctime>

namespace TN
{

void BALEEN::sparsifyTemporalDistributions()
{
    m_dataSetManager->distributionManager().sparsify( "electrons" );
}

BALEEN::BALEEN() :
    RenderWindow(),
    m_selectedHistogramMode( 1 ),
    m_selectedHistogramRotation( 0.0 ),
    m_selectedHistogramRotationY( 0.0 ),
    m_contextColor( 0.1, 0.1, 0.1 ),
    m_haveInitializedOpenGL( false ),
    m_haveInitializedData( false ),
    m_selectedTimeStep( 0 ),
    m_histogramStackRotation( 180.0 ),
    m_histogramStackRotationY( 0.0 ),
    m_boxSelectionColor( .1, .1, .1, 1),
    m_dividerColor( 1, 1, 1, 1 ),
    m_textColor( 0.2, 0.2, 0.2 ),
    m_maxFrequencyInSelectedTS( 0 ),
    m_uiLayoutEdited( false ),
    m_histDefinitionEdited( false ),
    m_uiLayoutInitialized( false ),
    m_dataInitializedFirst( false ),
    m_histDialogOpen( false ),
    m_timePlotDialogOpen( false ),
    m_phasePlotDialogOpen( false ),
    m_timeSeriesDialogOpen( false ),
    m_filterDialogOpen( false ),
    m_saveDialogOpen( false ),
    m_roiDialogOpen( false ),
    m_filterEdited( false ),
    m_datasetSelected( false ),
    m_presetSelected( false ),
    m_sessionInitiated( false ),
    m_timeSeriesDefined( false ),
    m_histogramDefined( false ),
    m_forceSpacePartitioningSelection( true ),
    m_recalculateIsoVolume( true ),
    m_recalculateIsoMesh( true ),
    m_mouseIsPressed( false ),
    m_phaseTrajectoriesCached( false ),
    m_savedPreset( false ),
    m_loadNewPreset( false ),
    m_updatedGPUData( false ),
    m_roiSnapped( false )
{
    setTheme( Theme::White );

    m_trackState = false;
    m_logLevel = LOGGING_DISABLED;

    m_timeStamp = getTimeStamp();
    if( m_logLevel != LOGGING_DISABLED )
    {
        std::string user = getenv( "USER" );
        if( user == "" )
        {
            user = "anonymous";
        }

        m_logDirectory = RELATIVE_PATH_PREFIX + "/Log/" + user;
        boost::filesystem::path dir( m_logDirectory );
        if( ! boost::filesystem::exists( dir ) )
        {
            if (! boost::filesystem::create_directory( dir ) )
            {
                //////////qDebug() << "Log directory couldn't be created" << "\n";
            }
        }
    }

    m_selectedHist3DButton.setPressed( false );
    m_selectedHist2DButton.setPressed( true );

    m_phasePlotSelectionSelector.setPressed( false );
    m_phasePlotAllSelector.setPressed( true );

    m_spatialPartitioningDialog.reset( new EditGridProjectionDialogue( "key", this ) );
    connect( m_spatialPartitioningDialog.get(),
             SIGNAL(
                 finalizeProjection(
                     std::vector< std::string >,
                     bool
                 )
             ),
             this,
             SLOT(
                 finalizeProjection(
                     std::vector< std::string >,
                     bool
                 ) ) );

    connect( & m_savePresetDialog,
             SIGNAL( saveProject( bool ) ),
             this,
             SLOT( saveProject(bool) ) );

    ////////////////////////qDebug() << "dialog stuff in constructor";

    for( auto & cmp : m_marchCubes )
    {
        cmp.reset( new CubeMarcher );
    }

    m_renderer.reset( new Renderer );

    auxViewCamera.reset( QVector3D( -1, -1, -1 ), QVector3D( 1, 1, 1 )  );

    /////////////////////////////////////////

    // Filtering

    m_filterCombo.addItem( "None" );
    m_previousFilter = m_filterCombo.selectedItemText();
    m_reloadGPUData = false;

    /////////////////////////////////////////

    m_binWeightHistWidget.setNumBins( 32 );
    m_regionWeightHistWidget.setNumBins( 32 );
    m_allWeightHistWidget.setNumBins( 32 );

    m_binWeightHistWidget.setColor( Vec3< float >( .3, .3, .3 ) );
    m_regionWeightHistWidget.setColor( Vec3< float >( .3, .3, .3 ) );
    m_allWeightHistWidget.setColor( Vec3< float >( .3, .3, .3 ) );

    m_sampler.setSamplingMethod( SamplingMethod::PANNING_WINDOW );

    m_fbo = 0;

    Vec4 cl( 1.0, 1.0, 1.0, 1.0 );
    m_samplerWidget.visualizationWindow.bkgColor( cl );
    m_miniMapViewPort.bkgColor( cl );
    m_histogramViewPort.bkgColor( cl );
    m_timeHistIsoViewport.bkgColor( cl );
    m_auxViewPort.bkgColor( cl );
    m_dataInfoPanelViewPort.bkgColor( cl );
    m_samplerWidget.controlPanel.bkgColor( Vec4( m_dividerColor.x(), m_dividerColor.y(), m_dividerColor.z(), 0.6 ) );

//    m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo.addItem(  "local" );
//    m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo.addItem( "global" );

    m_phasePlotModeCombo.addItem( "Trajectories" );
    m_phasePlotModeCombo.addItem( "Points"       );

//    m_errorRemovalCombo.addItem("NaN,Inf");
//    m_errorRemovalCombo.addItem("NaN,Inf,Unexp.");

    m_phasePlotBlendingCombo.addItem( "Number Density" );
    m_phasePlotBlendingCombo.addItem( "Path Density" );

    m_phasePlotInterpolationCombo.addItem( "None" );
    m_phasePlotInterpolationCombo.addItem( "Linear" );

    m_comboRefs.push_back( &m_phasePlotBlendingCombo );
    m_comboRefs.push_back( &m_phasePlotInterpolationCombo );

    /****/
    //m_comboRefs.push_back( &m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo );
    /****/

    m_comboRefs.push_back( &m_configurationCombo );
    m_comboRefs.push_back( &m_datasetCombo );
    m_comboRefs.push_back( &m_phasePlotCombo );
    m_comboRefs.push_back( &m_phasePlotModeCombo );
    m_comboRefs.push_back( &m_timeSeriesCombo );
//    m_comboRefs.push_back( &m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo );
//    m_comboRefs.push_back( &m_samplerWidget.controlPanel.colorScalingPanel.smoothingCombo    );
    m_comboRefs.push_back( &m_timePlotModeCombo );
    m_comboRefs.push_back( &m_timePlotCombo );
    m_comboRefs.push_back( &m_filterCombo );
//    m_comboRefs.push_back( &m_errorRemovalCombo );
    m_comboRefs.push_back( &m_particleCombo );
    
    m_comboRefs.push_back( &m_aggregationCombo );
    m_comboRefs.push_back( &m_distSeriesModeCombo );

    m_aggregationCombo.addItem( "mean" );
    m_aggregationCombo.addItem( "rms" );
    m_aggregationCombo.addItem( "variance" );
    m_aggregationCombo.addItem( "sum" );
    m_aggregationCombo.addItem( "count" );
    m_aggregationCombo.addItem( "min" );
    m_aggregationCombo.addItem( "max" );

    m_distSeriesModeCombo.addItem( "2D-Stacked-Iso" );
    m_distSeriesModeCombo.addItem( "2D-Multiples"   );
    m_distSeriesModeCombo.addItem( "1D-Aggregation" );

    m_negPosScalarButton.setPressed( false );
    m_smoothRadiusSlider.setSliderPos( 1 / 5.0 );
    m_gridColorScaleSlider.setSliderPos( 0.0 );

    Vec3< float > immediateFlashColor( 1, .8, .5 );
    Vec3< float > optionalFlashColor( .8, .9, 1 );

    m_initiateSessionButton.flash( true );
    m_initiateSessionButton.flashColor( immediateFlashColor );

    m_datasetCombo.flashColor( immediateFlashColor );
    m_configurationCombo.flashColor( immediateFlashColor );

    m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.flashColor( immediateFlashColor );
    m_defineTimeSeriesButton.flashColor( immediateFlashColor );

    m_defineFilterButton.flashColor( optionalFlashColor );
    m_definePhasePlotButton.flashColor( optionalFlashColor );
    m_definePlotButton.flashColor( optionalFlashColor );
    m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.flashColor( immediateFlashColor );

    m_timePlotHistogramSelector.setPressed( false );
    m_timePlotAllSelector.setPressed( false );
    m_timePlotBothSelector.setPressed( true );

    m_sampler.resetToDefault();

    m_datasetCombo.flash( true );

    m_helpButtonBinning.text( "Layout" );
    m_helpButtonDetailView.text( "Selected Histogram" );
    m_helpButtonData.text( "Dataset" );
    m_helpButtonTimeline.text( "Time Series" );
    m_helpButtonIsoView.text( "Density Isosurfaces");
    m_helpButtonMiniMap.text( "Minimap" );
    m_helpButtonLinkedView.text( "Scalar Stats" );

    m_helpButtonBinning.htmlReference( "#Binning" );
    m_helpButtonDetailView.htmlReference( "#Detail" );
    m_helpButtonData.htmlReference( "#Data" );
    m_helpButtonTimeline.htmlReference( "#Timeline" );
    m_helpButtonIsoView.htmlReference( "#Iso" );
    m_helpButtonMiniMap.htmlReference( "#Minimap" );
    m_helpButtonLinkedView.htmlReference( "#LinkedPlot" );

    m_helpLabelWidgetRefs.push_back( & m_helpButtonBinning );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonData );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonLinkedView );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonIsoView );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonTimeline );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonDetailView );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonMiniMap );

    setPtclOpacity( m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.value() );

    ////////////////////////qDebug() << "before query configurations";

    queryConfigurations();

    ////////////////////////qDebug() << "after query configurations";
}

void BALEEN::reQueryPresets()
{
    std::string dirPath = RELATIVE_PATH_PREFIX + "/Configuration/DATA/";
    QStringList datasets = QDir( QString( dirPath.c_str() ) ).entryList( QDir::Dirs | QDir::NoDotAndDotDot, QDir::NoSort );

    std::string selectedPreset = m_configurationCombo.selectedItemText();
    std::string selectedDataSet = m_datasetCombo.selectedItemText();

    m_datasetCombo.clear();
    m_configurationCombo.clear();

    m_savedConfigurations.clear();

    for( auto & ds : datasets )
    {
        m_datasetCombo.addItem( ds.toStdString() );
        std::string configPath = dirPath + "/" + ds.toStdString() + "/config/";
        QDir configDir( QString( configPath.c_str() ) );
        configDir.setNameFilters( QStringList() << "*.json" );
        QStringList configs = configDir.entryList( QDir::Files, QDir::NoSort );

        m_savedConfigurations.insert( { ds.toStdString(), { } } );

        // read dataset to find out it's type
        std::string dataInfoPath = RELATIVE_PATH_PREFIX + "/Configuration//DATA//" + ds.toStdString() + "//data.json";
        QString valD;
        QFile fileD;
        fileD.setFileName( dataInfoPath.c_str());
        fileD.open(QIODevice::ReadOnly | QIODevice::Text);
        valD = fileD.readAll();
        fileD.close();
        QJsonDocument dataDoc = QJsonDocument::fromJson(valD.toUtf8());
        QJsonObject objD = dataDoc.object();

        std::string type = objD[ "type" ].toString().toStdString();

        std::string basePresetPath = RELATIVE_PATH_PREFIX + "/Configuration//GLOBAL_BASE_DEFAULT//";
        if( type == "particle" )
        {
            basePresetPath += "particle.base.default.json";
        }
        else
        {
            basePresetPath += "grid.base.default.json";
        }

        // add base configuration ...
        QString valP;
        QFile fileP;
        fileP.setFileName( basePresetPath.c_str() );
        fileP.open(QIODevice::ReadOnly | QIODevice::Text);
        valP = fileP.readAll();
        fileP.close();
        QJsonDocument dP = QJsonDocument::fromJson(valP.toUtf8());
        QJsonObject objP = dP.object();
        ProjectConfiguration baseConfig;
        baseConfig.fromJSON( objP );
        m_savedConfigurations.at( ds.toStdString() ).insert( { "base", baseConfig } );

        ////////////////////////qDebug() << "before load configs";

        // load saved configurations
        for( auto & cf : configs )
        {
            std::string presetName = cf.left( cf.lastIndexOf(".") ).toStdString();
            std::string presetPath = RELATIVE_PATH_PREFIX + "Configuration/DATA/" + ds.toStdString() + "/config/" + presetName + ".json";
            QString val;
            QFile file;
            file.setFileName(presetPath.c_str());
            file.open(QIODevice::ReadOnly | QIODevice::Text);
            val = file.readAll();
            file.close();
            QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject obj = d.object();

            ProjectConfiguration config;

            ////////////////////////qDebug() << "calling from JSON";
            ////////////////////////qDebug() << obj;
            config.fromJSON( obj );
            ////////////////////////qDebug() << "done with from JSON";

            m_savedConfigurations.at( ds.toStdString() ).insert( { presetName, config } );
        }
        ////////////////////////qDebug() << "after load configs";
    }

    if( m_datasetSelected == false )
    {
        const std::string tx = "select dataset";
        m_datasetCombo.addItem( tx );
        m_datasetCombo.selectByText( tx );
    }
    else
    {
        m_datasetCombo.selectByText( selectedDataSet );
    }

    if( m_datasetSelected == true )
    {
        if( m_presetSelected == false )
        {
            selectedPreset = "select preset";
            m_configurationCombo.addItem( selectedPreset );
        }
        for( auto & dfconf : m_savedConfigurations )
        {
            if( dfconf.first == selectedDataSet )
            {
                for( auto & config : dfconf.second )
                {
                    m_configurationCombo.addItem( config.first );
                }
            }
        }
        m_configurationCombo.selectByText( selectedPreset );
    }

    qDebug() << "done requerying preset";
}

void BALEEN::writeLogs( int level )
{
    if( level == LOGGING_DISABLED )
    {
        return;
    }

    std::string loadingLogString;
    for( auto & log : m_loadingLogs )
    {
        loadingLogString += log.first + ", " + log.second + "\n";
    }

    std::ofstream outFile( m_logDirectory + "//" + m_timeStamp + ".loadtime.json" );
    outFile << loadingLogString;
    outFile.close();

    m_currentConfigurations.writeLogs( m_logDirectory + "//" + m_timeStamp + ".states.json" );
}

void BALEEN::pushState()
{
    synchronizeConfiguration();
    m_currentConfigurations.pushStateChange();
}

void BALEEN::datasetSwitched()
{
    clearProject();

//    ////////////qDebug() << "project cleared";

    m_presetSelected = false;
    m_timeSeriesCombo.flash( false );
    m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.flash( false );
    m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.flash( false );
    m_definePlotButton.flash( false );
    m_definePhasePlotButton.flash( false );
    m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.flash( false );
    m_defineTimeSeriesButton.flash( false );

    afterSelectedDataset();

    ////////////qDebug() << "after selected dataset";
}

void BALEEN::presetSwitched()
{

}

void BALEEN::afterSelectedPreset()
{
    std::string selectedCF = m_configurationCombo.selectedItemText();

    ////////////////////////qDebug() << "configuring";

    configure( m_datasetCombo.selectedItemText(), m_configurationCombo.selectedItemText() );

    std::vector< std::string > cfs = m_configurationCombo.items();
    m_configurationCombo.clear();

    int idx = 0;
    int i = 0;
    for( auto & s : cfs )
    {
        if( s == selectedCF )
        {
            idx = i;
        }
        if( s != "select preset" )
        {
            m_configurationCombo.addItem( s );
            ++i;
        }
    }

    m_configurationCombo.selectItem( idx );

    m_presetSelected = true;
    m_configurationCombo.flash( false );

    ////////////////////////qDebug() << "after selected preset done";
}

void BALEEN::afterSelectedDataset()
{
    std::string selectedDS = m_datasetCombo.selectedItemText();
    std::vector< std::string > dsets = m_datasetCombo.items();
    m_datasetCombo.clear();

    int idx = 0;
    int i = 0;
    for( auto & s : dsets )
    {
        if( s == selectedDS )
        {
            idx = i;
        }
        if( s != "select dataset" )
        {
            m_datasetCombo.addItem( s );
            ++i;
        }
    }

    m_datasetCombo.selectItem( idx );

    m_configurationCombo.clear();
    m_configurationCombo.addItem( "select preset" );

    for( auto & dfconf : m_savedConfigurations )
    {
        if( dfconf.first == selectedDS )
        {
            for( auto & config : dfconf.second )
            {
                m_configurationCombo.addItem( config.first );
            }
        }
    }

    m_datasetSelected = true;
    m_presetSelected = false;
    m_sessionInitiated = false;

    m_datasetCombo.flash( false );
    m_configurationCombo.flash( true );

    renderLater();
}

void BALEEN::queryConfigurations()
{
    // go into data folder and query datasets and configurations
    std::string dirPath = RELATIVE_PATH_PREFIX + "/Configuration/DATA/";
    QStringList datasets = QDir( QString( dirPath.c_str() ) ).entryList( QDir::Dirs | QDir::NoDotAndDotDot, QDir::NoSort );

    m_datasetCombo.clear();
    m_datasetCombo.addItem( "select dataset" );

    // parse the base configurations for all data types

    for( auto & ds : datasets )
    {
        m_datasetCombo.addItem( ds.toStdString() );
        std::string configPath = dirPath + "/" + ds.toStdString() + "/config/";
        QDir configDir( QString( configPath.c_str() ) );
        configDir.setNameFilters( QStringList() << "*.json" );
        QStringList configs = configDir.entryList( QDir::Files, QDir::NoSort );

        m_savedConfigurations.insert( { ds.toStdString(), { } } );

        // read dataset to find out it's type
        std::string dataInfoPath = RELATIVE_PATH_PREFIX + "/Configuration//DATA//" + ds.toStdString() + "//data.json";
        QString valD;
        QFile fileD;
        fileD.setFileName( dataInfoPath.c_str());
        fileD.open(QIODevice::ReadOnly | QIODevice::Text);
        valD = fileD.readAll();
        fileD.close();
        QJsonDocument dataDoc = QJsonDocument::fromJson(valD.toUtf8());
        QJsonObject objD = dataDoc.object();

        std::string type = objD[ "type" ].toString().toStdString();

        std::string basePresetPath = RELATIVE_PATH_PREFIX + "/Configuration//GLOBAL_BASE_DEFAULT//";
        if( type == "particle" )
        {
            basePresetPath += "particle.base.default.json";
        }
        else
        {
            basePresetPath += "grid.base.default.json";
        }

        // add base configuration ...
        QString valP;
        QFile fileP;
        fileP.setFileName( basePresetPath.c_str() );
        fileP.open(QIODevice::ReadOnly | QIODevice::Text);
        valP = fileP.readAll();
        fileP.close();
        QJsonDocument dP = QJsonDocument::fromJson(valP.toUtf8());
        QJsonObject objP = dP.object();
        ProjectConfiguration baseConfig;
        baseConfig.fromJSON( objP );
        m_savedConfigurations.at( ds.toStdString() ).insert( { "base", baseConfig } );

        ////////////////////////qDebug() << "before load configs";

        // load saved configurations
        for( auto & cf : configs )
        {
            std::string presetName = cf.left( cf.lastIndexOf(".") ).toStdString();
            std::string presetPath = RELATIVE_PATH_PREFIX + "Configuration/DATA/" + ds.toStdString() + "/config/" + presetName + ".json";
            QString val;
            QFile file;
            file.setFileName(presetPath.c_str());
            file.open(QIODevice::ReadOnly | QIODevice::Text);
            val = file.readAll();
            file.close();
            QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject obj = d.object();

            ProjectConfiguration config;

            ////////////////////////qDebug() << "calling from JSON";
            ////////////////////////qDebug() << obj;
            config.fromJSON( obj );
            ////////////////////////qDebug() << "done with from JSON";

            m_savedConfigurations.at( ds.toStdString() ).insert( { presetName, config } );
        }
        ////////////////////////qDebug() << "after load configs";
    }
    m_datasetCombo.selectItem( 0 );
}

void BALEEN::loadTF( const std::string & path, std::vector< TN::Vec3< float > > & tf )
{
    std::ifstream inFile( path );
    std::string line;

    tf.clear();
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        tf.push_back( Vec3< float >(
                          std::stof( rgb[ 0 ] ),
                          std::stof( rgb[ 1 ] ),
                          std::stof( rgb[ 2 ] ) ) );
    }
}

void BALEEN::loadTF( const std::string & path, Texture1D3 & tex )
{
    std::ifstream inFile( path );
    std::string line;

    std::vector< Vec3< float > > colors;
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        colors.push_back( Vec3< float >(
                              std::stof( rgb[ 0 ] ),
                              std::stof( rgb[ 1 ] ),
                              std::stof( rgb[ 2 ] ) ) );

        tex.load( colors );
    }
}

void BALEEN::resetSamplerCam()
{
    float cX = ( m_contextBoundingBox.second.a() + m_contextBoundingBox.first.a() ) / 2.0;
    float cY = ( m_contextBoundingBox.second.b() + m_contextBoundingBox.first.b() ) / 2.0;

    Vec2< float > screenCenter = { scaledWidth() / 2, scaledHeight() / 2 };
    Vec2< float > viewCenter   =
    {
        m_samplerWidget.visualizationWindow.position().x() + m_samplerWidget.visualizationWindow.size().x() / 2.0,
        m_samplerWidget.visualizationWindow.position().y() + m_samplerWidget.visualizationWindow.size().y() / 2.0
    };

    double conversionRatio = ( m_upperLeftWindowCornerWorldSpace.y() - m_lowerLeftWindowCornerWorldSpace.y() ) / ( ( double )scaledHeight() );
    Vec2< float > offsets = ( viewCenter - screenCenter ) * conversionRatio;

    camera.setTranslation( Vec2< float >( -cX + offsets.x(), -cY + offsets.y() ) );

    float xWidth = m_contextBoundingBox.second.a() - m_contextBoundingBox.first.a();
    float yWidth = m_contextBoundingBox.second.b() - m_contextBoundingBox.first.b();

    float dataAspect = xWidth / yWidth;
    float viewAspect = m_samplerWidget.visualizationWindow.size().x() / ( double )  m_samplerWidget.visualizationWindow.size().y();

    float zoom;

    if( dataAspect >= viewAspect )
    {
        zoom = xWidth / ( scaledWidth() );
    }
    else
    {
        zoom = yWidth / ( scaledHeight() );
    }

    camera.setZoom( zoom );

    QVector3D c1 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1, 1, 0 );
    QVector3D c2 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1, 1, 0 );
    QVector3D c3 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1,-1, 0 );
    QVector3D c4 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1,-1, 0 );

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    renderLater();
}

void BALEEN::clearSystem()
{
    m_dataSetManager.reset( new DataSetManager< float > );

    m_histogramGridVertices.clear();
    m_histogramGridVertices.shrink_to_fit();

    m_histogramGridColors.clear();
    m_histogramGridColors.shrink_to_fit();

    m_selectedTimeStep = 0;

    camera.reset();
    camera.setZoom( 0.007 );
    camera.dragLeftButton( Vec2< float >( -8.1f, 2.0f ) );

    m_dataInitializedFirst = false;

    updateColorLegendGeom( m_histTF );

    m_recalculateFullTimePlot = true;
}

void BALEEN::cleanDialogues()
{
    for( auto & mp : m_histogramDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_histogramDefinitionDialogues.clear();

    for( auto & mp : m_phasePlotDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_phasePlotDefinitionDialogues.clear();

    for( auto & mp : m_timePlotDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_timePlotDefinitionDialogues.clear();

    for( auto & mp : m_timeSeriesDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_timeSeriesDefinitionDialogues.clear();

    for( auto & mp : m_particleFilterDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_particleFilterDialogues.clear();

    for( auto & mp : m_roiDialogs )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_roiDialogs.clear();
}

BALEEN::~BALEEN()
{
    if( m_logLevel != LOGGING_DISABLED )
    {
        writeLogs( m_logLevel );
    }

    cleanDialogues();
    delete m_fbo;
}

void BALEEN::clearProject()
{
    m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.clear();
    m_particleCombo.clear();
    m_timePlotModeCombo.clear();
    m_phasePlotCombo.clear();
    m_filterCombo.clear();
    m_timeSeriesCombo.clear();

    cleanDialogues();

    m_currentConfigurations.distributionBasedConfiguration.clear();
    m_currentConfigurations.particleBasedConfiguration.clear();

    m_dataInitializedFirst = false;
    m_histogramDefined = false;
    m_timeSeriesDefined = false;
    m_sessionInitiated = false;

    m_datasetSelected = false;
    m_presetSelected = false;

    m_haveInitializedData = false;
    m_recalculateFullTimePlot = true;
    m_recalculatePhasePlot = true;

    m_uiLayoutEdited = false;
    m_uiLayoutInitialized = false;
    m_histDefinitionEdited = false;
    m_filterEdited = false;
    m_reloadGPUData = true;

    m_histDialogOpen = false;
    m_timePlotDialogOpen = false;
    m_phasePlotDialogOpen = false;
    m_timeSeriesDialogOpen = false;
    m_filterDialogOpen = false;

    m_dataSetManager->particleDataManager().clear();
    m_dataSetManager->distributionManager().clear();

    m_histogramLayoutLines.clear();
    m_selectedHistogramVertices.clear();
    m_selectedHistogramColors.clear();
    m_histogramGridColors.clear();
    m_histogramGridVertices.clear();

    invalidateSampling();
}

bool BALEEN::parseBasicDataInfo( const std::string & dataset, BasicDataInfo & result )
{

    const std::string path = RELATIVE_PATH_PREFIX + "Configuration//DATA//" + dataset + "//data.json";
    QFile loadFile( path.c_str() );

    if ( ! loadFile.open( QIODevice::ReadOnly ) )
    {
        ////////////////////////qDebug() << "couldn't open the file: " << path.c_str();
        return false;
    }

    ////////////////////////qDebug() << "file loaded";

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc( QJsonDocument::fromJson( saveData ) );

    ////////////////////////qDebug() << loadDoc.object();

    result.format = loadDoc.object()[ "format" ].toString().toStdString();
    result.location = loadDoc.object()[ "location" ].toString().toStdString();
    result.type = loadDoc.object()[ "type" ].toString().toStdString();
    result.description = loadDoc.object()[ "description" ].toString().toStdString();
    result.name = loadDoc.object()[ "name" ].toString().toStdString();

    ////////////////////////qDebug() << result.location.c_str();

    return true;
}

void BALEEN::synchronizeConfiguration()
{
    // gather information about all of the project settings
//    m_currentConfigurations.bkgOpacity = m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.value();
//    m_currentConfigurations.partitionModifer = m_samplerWidget.controlPanel.roiScalePanel.lodSlider.value();
//    m_currentConfigurations.histNormalizationThreshold = m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.value();
//    m_currentConfigurations.histResolutionModifier = m_samplerWidget.controlPanel.histogramPanel.resolutionSlider.value();
//    m_currentConfigurations.valueRangeModifierX = m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.value();
//    m_currentConfigurations.valueRangeModifierY = m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.value();
//    m_currentConfigurations.histOpacity = m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.value();
//    m_currentConfigurations.spatialScalingModifier = m_samplerWidget.controlPanel.panningWindowPanel.aspectRatioSlider.value();
//    m_currentConfigurations.lockValueRangeModifiers = m_samplerWidget.controlPanel.histogramPanel.linkValueWidthSlidersButton.isPressed();
//    m_currentConfigurations.histogramNormalizationMode = m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo.selectedItemText();

    m_currentConfigurations.phasePlotMode = m_phasePlotModeCombo.selectedItemText();
    m_currentConfigurations.timePlotMode = m_timePlotModeCombo.selectedItemText();
    m_currentConfigurations.timePlotRegion = m_timePlotHistogramSelector.isPressed() ? "hist" : "all";
    m_currentConfigurations.contourLevelModifier = m_isoValueSlider.value();

    m_currentConfigurations.isoRotationX = m_histogramStackRotation;
    m_currentConfigurations.isoRotationY = m_histogramStackRotationY;
    m_currentConfigurations.histHeightRotation = m_selectedHistogramRotation;

    m_currentConfigurations.scaledScreenResolutionX = scaledWidth();
    m_currentConfigurations.scaledScreenResolutionY = scaledHeight();

    m_currentConfigurations.uiOffsetA = m_resizeWidgetA.position().x() - DIVIDER_WIDTH;
    m_currentConfigurations.uiOffsetB = m_resizeWidgetB.position().x();
    m_currentConfigurations.uiOffsetC = m_resizeWidgetC.position().y();
    m_currentConfigurations.uiOffsetD = m_resizeWidgetD.position().y();

    ////////////////////////qDebug() << "getting camera toJSON";
    m_currentConfigurations.samplingCamera = camera.toJSON();
    ////////////////////////qDebug() << "got camera toJSON";

    if( m_currentConfigurations.activeType == "particle" )
    {
        if( m_histogramDefined )
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedHistogram = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedHistogram = "";
        }
        if( m_timeSeriesDefined )
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries = m_timeSeriesCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries = "";
        }

        // it's flashing if nothing is defined
        // TODO use proper state variables
        if( m_phasePlotCombo.flash() != true )
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedPhasePlot = m_phasePlotCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedPhasePlot = "";
        }

        // it's flashing if nothing is defined
        // TODO use proper state variables
        if( m_timePlotCombo.flash() != true )
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot = m_timePlotCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot = "";
        }

        m_currentConfigurations.particleBasedConfiguration.m_selectedFilter = m_filterCombo.selectedItemText();
        m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType = m_particleCombo.selectedItemText();

        // it's possible no time steps is selected, but should not be a problem
        m_currentConfigurations.particleBasedConfiguration.m_selectedTimeStep = m_selectedTimeStep;
        m_currentConfigurations.errorRemovalOption = m_errorRemovalCombo.selectedItemText();
    }
    else
    {
        if( m_histogramDefined )
        {
            m_currentConfigurations.distributionBasedConfiguration.m_selectedHistogram = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.distributionBasedConfiguration.m_selectedHistogram = "";
        }
        if( m_timeSeriesDefined )
        {
            m_currentConfigurations.distributionBasedConfiguration.m_selectedTimeSeries = m_timeSeriesCombo.selectedItemText();
        }
        else
        {
            m_currentConfigurations.distributionBasedConfiguration.m_selectedTimeSeries = "";
        }

        m_currentConfigurations.distributionBasedConfiguration.m_selectedParticleType = m_particleCombo.selectedItemText();
        m_currentConfigurations.distributionBasedConfiguration.m_selectedTimeStep = m_selectedTimeStep;
    }
}

void BALEEN::saveProject( bool asDefault )
{
    //////////qDebug() << "synchronizing configuration";

    synchronizeConfiguration();

    //////////qDebug() << "synchronized";

    auto & mp = m_savedConfigurations.at( m_datasetCombo.selectedItemText() );

    std::string name = m_populationDataTypeEnum == PopulationDataType::PARTICLES
                       ? m_currentConfigurations.particleBasedConfiguration.m_name
                       : m_currentConfigurations.distributionBasedConfiguration.m_name;

    //////////qDebug() << "grabbed name " << name.c_str();

    if( mp.count( name ) )
    {
        //////////qDebug() << "name was existing";
        mp.at( name ) = m_currentConfigurations;
        m_configurationCombo.selectByText( name );
    }
    else
    {
        //////////qDebug() << "it was new, inserting";
        mp.insert( { name, m_currentConfigurations } );
        m_configurationCombo.addItem( name );
        m_configurationCombo.selectByText( name );
    }

    //////////qDebug() << "writing configuration";
    m_currentConfigurations.write( asDefault, RELATIVE_PATH_PREFIX );
    //////////qDebug() << "done saving";

    m_saveDialogOpen = false;
    m_savedPreset = true;

    renderLater();
}

bool BALEEN::configure( const std::string & dataset, const std::string & preset )
{
    ////////////////////////qDebug() << "loading dataset" << dataset.c_str();

    clearSystem();
    cleanDialogues();
    m_particleCombo.clear();
    m_timePlotModeCombo.clear();
    m_sessionInitiated = false;


    ////////////qDebug() << "clear system";

    m_forceSpacePartitioningSelection = true;

    if( parseBasicDataInfo( dataset, m_basicDataInfo ) )
    {
        m_currentConfigurations = m_savedConfigurations.at( dataset ).at( preset );
        m_currentConfigurations.activeType = m_basicDataInfo.type;

        m_renderer->renderLoadingScreen(
            textProg,
            cartesianProg,
            simpleColorProg,
            scaledWidth(),
            scaledHeight(),
            "Initializing...",
            0 );
        swapBuffers();

        m_dataSetManager->setDataSet( m_basicDataInfo );

        m_populationDataTypeEnum = m_basicDataInfo.type == "particle" ? PopulationDataType::PARTICLES : PopulationDataType::GRID_POINTS;
        m_savePresetDialog.setConfig( & m_currentConfigurations );
        m_savePresetDialog.setSavedConfigurations( & m_savedConfigurations.at( dataset ) );

        SpacePartitioningDefinition * spacePartitioning;

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            // set particle only based ui settin
            m_timePlotModeCombo.addItem( "mean" );
            m_timePlotModeCombo.addItem( "mean-squared" );

            m_currentConfigurations.particleBasedConfiguration.m_datasetInfo = m_basicDataInfo;
            std::set< std::string > ptypes = m_dataSetManager->particleDataManager().particleTypes();
            m_currentConfigurations.particleBasedConfiguration.setParticles( ptypes );
            m_dataSetManager->particleDataManager().updateCustomConstants( m_currentConfigurations.particleBasedConfiguration.m_customConstants );

            for( auto & e : m_dataSetManager->particleDataManager().baseVariables() )
            {
                m_particleCombo.addItem( e.first );

                m_roiDialogs.insert( { e.first, std::map< std::string, ROIDialog* >() } );
                m_histogramDefinitionDialogues.insert( { e.first, std::map< std::string, HistogramDefinitionDialogue* >() } );
                m_phasePlotDefinitionDialogues.insert( { e.first, std::map< std::string, PhasePlotDefinitionDialogue* >() } );
                m_timePlotDefinitionDialogues.insert( { e.first, std::map< std::string, TimePlotDefinitionDialogue* >() } );
                m_timeSeriesDefinitionDialogues.insert( { e.first, std::map< std::string, TimeSeriesDefinitionDialogue* >() } );
                m_particleFilterDialogues.insert( { e.first, std::map< std::string, ParticleFilterDialogue* >() } );

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.at( e.first ) )
                {
                    HistogramDefinitionDialogue *dialog = new HistogramDefinitionDialogue( defs.first, this );
                    ////////////////////////qDebug() << "inserted at " << e.first.c_str() << ", " << defs.first.c_str();

                    auto inserted = m_histogramDefinitionDialogues.at( e.first ).insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelHistogramDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, HistogramDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeHistogramDefinition(
                                     const std::string &,
                                     const std::map< std::string, HistogramDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                }

                m_dataSetManager->particleDataManager().updateDerivations(  e.first, m_currentConfigurations.particleBasedConfiguration.m_derivedVariables.at(  e.first ) );

                ////////////////////////qDebug() << "updating dialogs " << selectedPartType.c_str();

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.at(  e.first ) )
                {
                    ////////////////////////qDebug() << defs.first.c_str();
                    m_histogramDefinitionDialogues.at(  e.first ).at( defs.first )->update(
                        m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find(  e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find(  e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find(  e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants() );
                    m_histogramDefinitionDialogues.at(  e.first ).at( defs.first )->setStatus( "defined" );
                }

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( e.first ) )
                {
                    auto it = m_timePlotDefinitionDialogues.find( e.first );
                    TimePlotDefinitionDialogue *dialog = new TimePlotDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimePlotDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimePlotDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                for( auto & defs : m_currentConfigurations.timeSeriesDefinitions().at( e.first ) )
                {
                    ////////////qDebug() << "time series def " << defs.first.c_str();

                    auto it = m_timeSeriesDefinitionDialogues.find( e.first );
                    TimeSeriesDefinitionDialogue *dialog = new TimeSeriesDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimeSeriesDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimeSeriesDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.timeSeriesDefinitions().find( e.first )->second,
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().fullTimeSeries()
                        :  m_dataSetManager->distributionManager().fullTimeSeries(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
                        :  m_dataSetManager->distributionManager().realTime(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
                        :  m_dataSetManager->distributionManager().simTime() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( e.first ) )
                {
                    auto it = m_particleFilterDialogues.find( e.first );
                    ParticleFilterDialogue *dialog = new ParticleFilterDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelParticleFilterDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeParticleFilterDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants(),
                        m_dataSetManager->particleDataManager().fullTimeSeries(),
                        m_dataSetManager->particleDataManager().realTime(),
                        m_dataSetManager->particleDataManager().simTime() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( e.first ) )
                {
                    auto it = m_phasePlotDefinitionDialogues.find( e.first );
                    PhasePlotDefinitionDialogue *dialog = new PhasePlotDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelPhasePlotDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, PhasePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizePhasePlotDefinition(
                                     const std::string &,
                                     const std::map< std::string, PhasePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants() );

                    inserted.first->second->fromDefinition( defs.second );
                }
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType != "" )
            {
                ////////////qDebug() << "selecting part type " << m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType.c_str();
                m_particleCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType );
            }

            updatePartType();

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedHistogram != "" )
            {
                m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedHistogram );
            }

            spacePartitioning = & m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( m_particleCombo.selectedItemText() )->second;

            if( spacePartitioning->scaling.size() != 2 )
            {
                spacePartitioning->scaling = { 1.0, 1.0 };
            }

            // check if a space partitioning was not included in the preset
            if( spacePartitioning->attributes.size() != 2 )
            {
                // check if one is defined in the data file
                if( m_basicDataInfo.defaultSpacePartitioningAttributes.count( m_particleCombo.selectedItemText() ) )
                {
                    // and make sure it is the correct size
                    if( m_basicDataInfo.defaultSpacePartitioningAttributes.find( m_particleCombo.selectedItemText() )->second.size() == 2 )
                    {
                        spacePartitioning->attributes = m_basicDataInfo.defaultSpacePartitioningAttributes.find( m_particleCombo.selectedItemText() )->second;
                        m_forceSpacePartitioningSelection = false;
                    }
                }
                // otherwise, if there is a valid selected particle type
//                else if( m_dataSetManager->particleDataManager().particleAndTimeVariantAttributes().count( m_particleCombo.selectedItemText() ) )
//                {
//                    auto & attrs = m_dataSetManager->particleDataManager().particleAndTimeVariantAttributes().find( m_particleCombo.selectedItemText() )->second;

//                    if( attrs.count( "r" ) && attrs.count( "z" ) )
//                    {
//                        spacePartitioning->attributes = { "r", "z" };
//                        m_forceSpacePartitioningSelection = false;
//                    }
//                    // else try "x", "y"
//                    else if( attrs.count( "x" ) && attrs.count( "y" ) )
//                    {
//                        spacePartitioning->attributes = { "x", "y" };
//                        m_forceSpacePartitioningSelection = false;
//                    }
//                    // "force user to select manually"
//                    else
//                    {
//                        m_forceSpacePartitioningSelection = true;
//                    }
//                }
            }
            else
            {
                m_forceSpacePartitioningSelection = false;
            }

            m_numTimeSteps = m_dataSetManager->particleDataManager().numTimeSteps();

            // set ui elements

//            m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.setSliderPos( m_currentConfigurations.valueRangeModifierX );
//            m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.setSliderPos( m_currentConfigurations.valueRangeModifierY );

            if( m_currentConfigurations.lockValueRangeModifiers )
            {
//                m_samplerWidget.controlPanel.histogramPanel.linkValueWidthSlidersButton.setPressed( true );
            }
//            m_samplerWidget.controlPanel.histogramPanel.resolutionSlider.setSliderPos( m_currentConfigurations.histResolutionModifier );

            if( m_currentConfigurations.errorRemovalOption != "" )
            {
                m_errorRemovalCombo.selectByText( m_currentConfigurations.errorRemovalOption );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedPhasePlot != "" )
            {
                m_phasePlotCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedPhasePlot );
            }

            if( m_currentConfigurations.phasePlotMode != "" )
            {
                m_phasePlotModeCombo.selectByText( m_currentConfigurations.phasePlotMode );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot != "" )
            {
                m_timePlotCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedFilter != "" )
            {
                m_filterCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedFilter );
            }

            if( m_currentConfigurations.timePlotMode != "" )
            {
                m_timePlotModeCombo.selectByText( m_currentConfigurations.timePlotMode );
            }

            if( m_currentConfigurations.timePlotRegion == "all" )
            {
                m_timePlotAllSelector.setPressed( true );
                m_timePlotBothSelector.setPressed( false );
                m_timePlotHistogramSelector.setPressed( false );
            }
            if( m_currentConfigurations.timePlotRegion == "hist" )
            {
                m_timePlotAllSelector.setPressed( false );
                m_timePlotBothSelector.setPressed( false );
                m_timePlotHistogramSelector.setPressed( true );
            }
            else if( m_currentConfigurations.timePlotRegion == "both" )
            {
                m_timePlotAllSelector.setPressed( false );
                m_timePlotBothSelector.setPressed( true );
                m_timePlotHistogramSelector.setPressed( false );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries != "" )
            {
                ////////////////////////qDebug() << "selecting time series " << m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries.c_str();

                m_timeSeriesCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries );

                ////////////////////////qDebug() << m_timeSeriesCombo.selectedItemText().c_str();
            }
        }

        else if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
        {
            m_timePlotModeCombo.clear();
            m_timePlotModeCombo.addItem( "mean" );
            m_timePlotModeCombo.addItem( "rms" );
            m_timePlotModeCombo.addItem( "variance" );
            m_timePlotModeCombo.addItem( "sum" );
            m_timePlotModeCombo.addItem( "min" );
            m_timePlotModeCombo.addItem( "max" );

            std::cout << "added w0w1" << std::endl;

            m_currentConfigurations.distributionBasedConfiguration.m_datasetInfo = m_basicDataInfo;
            m_currentConfigurations.distributionBasedConfiguration.setParticles( m_dataSetManager->distributionManager().particleTypes() );
            m_currentConfigurations.distributionBasedConfiguration.setDistributions( m_dataSetManager->distributionManager().perParticleTypePerDistributionTypeParameters() );

            for( auto & e : m_dataSetManager->distributionManager().availableData() )
            {
                m_particleCombo.addItem( e.first );
                m_timeSeriesDefinitionDialogues.insert( { e.first, std::map< std::string, TimeSeriesDefinitionDialogue* >() } );
                m_roiDialogs.insert( { e.first, std::map< std::string, ROIDialog* >() } );
            }
            m_particleCombo.selectItem( 0 );

            for( auto & e : m_dataSetManager->distributionManager().availableData().find( m_particleCombo.selectedItemText() )->second )
            {
                m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.addItem( e );
            }

            updatePartType();


//            for( auto & e : m_dataSetManager->distributionManager().perParticleTypePerDistributionTypeParameters() )
//            {
//                m_gridProjectionDialogues.insert( { e.first, std::map< std::string, EditGridProjectionDialogue* >() } );
//                for( auto & d : e.second )
//                {
//                    m_gridProjectionDialogues.find( e.first )->second.insert( { d.first, new EditGridProjectionDialogue( e.first + ", " + d.first, this  )  } );
//                    m_gridProjectionDialogues.find( e.first )->second.find( d.first )->second->update( m_dataSetManager->distributionManager().attributeKeys() );

//                    EditGridProjectionDialogue * projD = m_gridProjectionDialogues.find( e.first )->second.find( d.first )->second;

//                    connect( projD,
//                             SIGNAL(
//                                 finalizeProjection(
//                                     std::vector< std::string >,
//                                     bool
//                                 )
//                             ),
//                             this,
//                             SLOT(
//                                 finalizeProjection(
//                                     std::vector< std::string >,
//                                     bool
//                                 ) ) );
//                }
//            }

            if( m_currentConfigurations.distributionBasedConfiguration.m_selectedTimeSeries != "" )
            {
                m_timeSeriesCombo.selectByText( m_currentConfigurations.distributionBasedConfiguration.m_selectedTimeSeries );
            }

            if( m_currentConfigurations.distributionBasedConfiguration.m_selectedParticleType != "" )
            {
                m_particleCombo.selectByText( m_currentConfigurations.distributionBasedConfiguration.m_selectedParticleType );
            }

            spacePartitioning = & m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( m_particleCombo.selectedItemText() )->second;

            if( spacePartitioning->scaling.size() != 2 )
            {
                spacePartitioning->scaling = { 1.0, 1.0 };
            }

            // check if a space partitioning was not included in the preset
            if( spacePartitioning->attributes.size() != 2 )
            {
                // check if one is defined in the data file
                if( m_basicDataInfo.defaultSpacePartitioningAttributes.count( m_particleCombo.selectedItemText() ) )
                {
                    // and make sure it is the correct size
                    if( m_basicDataInfo.defaultSpacePartitioningAttributes.find( m_particleCombo.selectedItemText() )->second.size() == 2 )
                    {
                        spacePartitioning->attributes = m_basicDataInfo.defaultSpacePartitioningAttributes.find( m_particleCombo.selectedItemText() )->second;
                        m_forceSpacePartitioningSelection = false;
                    }
                }
                // otherwise, if there is a valid selected particle type
//                else
//                {
//                    auto & attrs = m_dataSetManager->distributionManager().perGridPointAttributes();

//                    if( attrs.count( "r" ) && attrs.count( "z" ) )
//                    {
//                        spacePartitioning->attributes = { "r", "z" };
//                        m_forceSpacePartitioningSelection = false;
//                    }
//                    // else try "x", "y"
//                    else if( attrs.count( "x" ) && attrs.count( "y" ) )
//                    {
//                        spacePartitioning->attributes = { "x", "y" };
//                        m_forceSpacePartitioningSelection = false;
//                    }
//                    // "force user to select manually"
//                    else
//                    {
//                        m_forceSpacePartitioningSelection = true;
//                    }
//                }
            }
            else
            {
                m_forceSpacePartitioningSelection = false;
            }

            m_timePlotCombo.clear();
//            m_timePlotCombo.addItem( "w0w1" );
        }

        // set ui according to project file
        m_isoValueSlider.setSliderPos( m_currentConfigurations.contourLevelModifier );
//        m_samplerWidget.controlPanel.roiScalePanel.lodSlider.setSliderPos( m_currentConfigurations.partitionModifer );
//        m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.setSliderPos( m_currentConfigurations.bkgOpacity );
//        m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.setSliderPos( m_currentConfigurations.histOpacity );
//        m_samplerWidget.controlPanel.panningWindowPanel.aspectRatioSlider.setSliderPos( m_currentConfigurations.spatialScalingModifier );
//        m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.setSliderPos( m_currentConfigurations.histNormalizationThreshold );
//        m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo.selectByText( m_currentConfigurations.histogramNormalizationMode );

        // camera and rotations
        m_selectedHistogramRotation = m_currentConfigurations.histHeightRotation;
        m_histogramStackRotation = m_currentConfigurations.isoRotationX;
        m_histogramStackRotationY = m_currentConfigurations.isoRotationY;

        ////////////////////////qDebug() << "setting camera from JSON";

        camera.setSize( scaledWidth(), scaledHeight() );
        if( m_configurationCombo.selectedItemText() == "base" )
        {
            resetSamplerCam();
        }
        else
        {
            camera.fromJSON( m_currentConfigurations.samplingCamera );
        }

        if( ( int( m_currentConfigurations.scaledScreenResolutionX ) == int( scaledWidth() ) )
                && ( int( m_currentConfigurations.scaledScreenResolutionY ) == int( scaledHeight() ) ) )
        {
            float offsetA = m_currentConfigurations.uiOffsetA;
            float offsetB = m_currentConfigurations.uiOffsetB;
            float offsetC = m_currentConfigurations.uiOffsetC;
            float offsetD = m_currentConfigurations.uiOffsetD;

            m_resizeWidgetA.setPosition( DIVIDER_WIDTH + offsetA, 0 );
            m_resizeWidgetA.setSize( RESIZE_HANDLE_WIDTH, scaledHeight() - MENU_HEIGHT );

            m_resizeWidgetB.setPosition( offsetB, offsetC+DIVIDER_WIDTH );
            m_resizeWidgetB.setSize( RESIZE_HANDLE_WIDTH, scaledHeight() - MENU_HEIGHT - offsetC - DIVIDER_WIDTH );

            m_resizeWidgetD.setPosition( offsetA + DIVIDER_WIDTH*2, offsetD );
            m_resizeWidgetD.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, RESIZE_HANDLE_WIDTH );

            m_resizeWidgetC.setPosition( offsetA + DIVIDER_WIDTH*2, offsetC );
            m_resizeWidgetC.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, RESIZE_HANDLE_WIDTH );

            m_uiLayoutInitialized = true;
        }

        m_sampler.setPopulationDataType( m_populationDataTypeEnum );
        recalculateUiLayout();

        ////////////////////qDebug() << "configured";

        if( m_forceSpacePartitioningSelection == false )
        {
            std::set< std::string > attrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                            m_dataSetManager->particleDataManager().attributeKeys( m_particleCombo.selectedItemText() ) :
                                            m_dataSetManager->distributionManager().attributeKeys() ;
            m_spatialPartitioningDialog->set( attrs, *spacePartitioning );
        }

        return true;
    }

    return false;
}

void BALEEN::stackHistogram2D_DistGrid(
    FrVolume & frVolume,
    int t,
    const std::vector< int > & gMap,
    const std::vector< float > & distributionValues,
    int COLS,
    int ROWS )
{

    PredefinedHistogram & pdh = m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.find(
                                    m_particleCombo.selectedItemText() )->second.find( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() )->second;
    const std::vector< float > & gridCellVolumes = m_dataSetManager->distributionManager().values( "volume" );
    const double NORM_FACTOR = pdh.normalizationFactor;

    const int SZ = gMap.size();
    const int BINS_PER_HIST = ROWS * COLS;
    const int STACK_OFFSET = t * BINS_PER_HIST;

    double volumeSum = 0;
    for ( int i = 0; i < SZ; ++i )
    {
        const int GRID_OFFSET = BINS_PER_HIST * gMap[ i ];
        #pragma omp simd
        for ( int b = 0; b < BINS_PER_HIST; ++b )
        {
            frVolume.values[ STACK_OFFSET + b ] += distributionValues[ GRID_OFFSET + b ];
        }
        volumeSum += gridCellVolumes[ gMap[ i ] ];
    }

    if( volumeSum > 0 )
    {
        #pragma omp simd
        for ( int b = 0; b < BINS_PER_HIST; ++b )
        {
            frVolume.values[ STACK_OFFSET + b ] *= NORM_FACTOR / volumeSum;
        }
    }

    // smooth merge result
    if( m_samplerWidget.controlPanel.colorScalingPanel.smoothingCombo.selectedItemText() == "3x3 average" )
    {
        const int RAD = 1;
        for ( int row = 0; row < ROWS; ++row )
        {
            for ( int col = 0; col < COLS; ++col )
            {
                int count = 0;
                double sum = 0;
                for( int cc = std::max( col - RAD, 0 ); cc <= std::min( col + RAD, COLS-1 ); ++cc )
                {
                    for( int rr = std::max( row - RAD, 0 ); rr <= std::min( row + RAD, ROWS-1 ); ++rr )
                    {
                        sum += frVolume.values[ STACK_OFFSET + rr*COLS + cc ];
                        ++count;
                    }
                }
                frVolume.values[ STACK_OFFSET + row*COLS + col ] = sum / count;
            }
        }
    }
}

void BALEEN::stackHistogram2D(
    FrVolume & frVolume,
    const HistogramDefinition & histDefinition,
    const SpacePartitioningDefinition & spacePartitioning,
    const std::array< Vec2< float >, 2 > & histThresholds,
    const I2 & histResolution,
    const Vec2< double > & spatialScaling,
    const std::string & ptype,
    int t,
    int idx,
    int cellSelection )
{
    const float X_WIDTH = histThresholds[ 0 ].b() - histThresholds[ 0 ].a();
    const float Y_WIDTH = histThresholds[ 1 ].b() - histThresholds[ 1 ].a();

    const std::vector< float > & xSpatial = *( m_dataSetManager->particleDataManager().values( idx, ptype, spacePartitioning.attributes[ 0 ] ) );
    const std::vector< float > & ySpatial = *( m_dataSetManager->particleDataManager().values( idx, ptype, spacePartitioning.attributes[ 1 ] ) );
    const std::vector< float > & xBinning = *( m_dataSetManager->particleDataManager().values( idx, ptype, histDefinition.binningSpace[ 0 ] ) );
    const std::vector< float > & yBinning = *( m_dataSetManager->particleDataManager().values( idx, ptype, histDefinition.binningSpace[ 1 ] ) );

    const int BINS_PER_HIST = histResolution.a() * histResolution.b();
    const int SZ = xSpatial.size();

    if( histDefinition.weightType != HistogramWeightType::VARIABLE )
    {
        for ( int i = 0; i < SZ; ++i )
        {
            const int gridCell = m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() );
            if ( gridCell != -1 && gridCell == cellSelection )
            {
                int r = std::floor( ( ( yBinning[ i ] - histThresholds[ 1 ].a() ) / Y_WIDTH ) * histResolution.b() );
                int c = std::floor( ( ( xBinning[ i ] - histThresholds[ 0 ].a() ) / X_WIDTH ) * histResolution.a() );

                //! clamping, probably should be an option
                r = std::max( std::min( r, histResolution.b() - 1 ), 0 );
                c = std::max( std::min( c, histResolution.a() - 1 ), 0 );

                frVolume.values[ t * BINS_PER_HIST + r * histResolution.a() + c ] += 1.f;
            }
        }
    }
    else
    {
        const std::vector< float > & weights = *( m_dataSetManager->particleDataManager().values( idx, ptype, histDefinition.weight ) );
        for ( int i = 0; i < SZ; ++i )
        {
            const int gridCell = m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() );
            if ( gridCell != -1 && gridCell == cellSelection )
            {
                int r = std::floor( ( ( yBinning[ i ] - histThresholds[ 1 ].a() ) / Y_WIDTH ) * histResolution.b() );
                int c = std::floor( ( ( xBinning[ i ] - histThresholds[ 0 ].a() ) / X_WIDTH ) * histResolution.a() );

                //! clamping, probably should be an option
                r = std::max( std::min( r, histResolution.b() - 1 ), 0 );
                c = std::max( std::min( c, histResolution.a() - 1 ), 0 );

                frVolume.values[ t * BINS_PER_HIST + r * histResolution.a() + c ] += weights[ i ];
            }
        }
    }
}

void BALEEN::renderSelectedHistContour( const std::vector< Vec2< float > > & paths, const QVector4D & color )
{
    if( m_selectedHist2DButton.isPressed() )
    {
        if( paths.size() > 2 )
        {
            glViewport(
                m_histogramViewPort.offsetX() + HIST_PAD,
                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - PANEL_HT,
                m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ),
                m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN )
            );

            QMatrix4x4 rotation;
            rotation.setToIdentity();

            if( true /*m_populationDataTypeEnum == PopulationDataType::PARTICLES */ )
            {
                rotation.scale( QVector3D( -1, 1, 1 ) );
                rotation.rotate( 90.0, QVector3D( 0, 0, 1 ) );
            }

            glLineWidth( 3.0 );
            glPointSize( 3.0 );
            m_renderer->renderPrimitivesFlat(paths, cartesianProg, QVector4D( 0.0, 0.0, 0.0, 1 ), rotation,  GL_LINES );
            m_renderer->renderPrimitivesFlat(paths, cartesianProg, QVector4D(0.0, 0.0, 0.0, 1 ), rotation, GL_POINTS );
            glLineWidth( 2 );

            glLineWidth( 1.0 );
            glPointSize( 1.0 );
            m_renderer->renderPrimitivesFlat(paths, cartesianProg, color, rotation,  GL_LINES );
            m_renderer->renderPrimitivesFlat(paths, cartesianProg, color, rotation, GL_POINTS );
        }
    }
}

inline void addNormal( const Vec3< float > & vertex, const Vec3< float > & normal, std::unordered_map< Vec3< float >, Vec3< float > > &  normals )
{
    std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
    if( ( it = normals.find( vertex ) ) != normals.end() )
    {
        it->second = it->second + normal;
    }
    else
    {
        normals.insert( std::pair< Vec3< float >, Vec3< float >  >( vertex, normal ) );
    }
}

void BALEEN::renderHistogramAsHeightField( float * hist, I2 resolution )
{
    const int MESH_DIM_A  = resolution.a()-1;
    const int MESH_DIM_B  = resolution.b()-1;

    const int NUM_VERTS = MESH_DIM_A * MESH_DIM_B * 6;

    std::vector< Vec3< float > >   verts( NUM_VERTS );
    std::vector< Vec3< float > >  colors( NUM_VERTS );
    std::vector< Vec3< float > > normals( NUM_VERTS );

    std::unordered_map< Vec3< float >, Vec3< float > > adjacentFaceNormals;

//    std::vector< Vec3< float > > negXAxis( 2 );
//    std::vector< Vec3< float > > posXAxis( 2 );
//    std::vector< Vec3< float > > negZAxis( 2 );
//    std::vector< Vec3< float > > posZAxis( 2 );
//    std::vector< Vec3< float > > posYAxis( 2 );

    double h = resolution.b() / 2.0;

//    posZAxis[ 0 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, -.2*resolution.a()  );
//    posZAxis[ 1 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, ( resolution.a()-1)/2.0  );

//    negZAxis[ 0 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, resolution.a()*1.2 );
//    negZAxis[ 1 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, ( resolution.a()-1)/2.0  );

//    negXAxis[ 0 ] = Vec3< float >( -.2*resolution.a() , 0, ( resolution.a()-1)/2.0  );
//    negXAxis[ 1 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, ( resolution.a()-1)/2.0 );

//    posXAxis[ 0 ] = Vec3< float >( resolution.a()*1.2, 0, ( resolution.a()-1)/2.0 );
//    posXAxis[ 1 ] = Vec3< float >( ( resolution.a()-1)/2.0, 0, ( resolution.a()-1)/2.0 );

//    posYAxis[ 0 ] = Vec3< float >( ( resolution.a()-1)/2.0,     0, (resolution.a()-1)/2.0  );
//    posYAxis[ 1 ] = Vec3< float >( ( resolution.a()-1)/2.0, h*1.2, (resolution.a()-1)/2.0 );

    float maxV = -std::numeric_limits<float>::max();
    float minV =  std::numeric_limits<float>::max();

    const int N_BINS = resolution.a() * resolution.b();

    for( int b = 0; b < N_BINS; ++b  )
    {
        maxV = std::max( maxV, hist[ b ] );
        minV = std::min( minV, hist[ b ] );
    }

    float deltaV = maxV - minV;

    for( int r = 0; r < MESH_DIM_B; ++r )
    {
        for( int c = 0; c < MESH_DIM_A; ++c )
        {
            const int OFFSET = 6*( r * MESH_DIM_A + c );

            const float r1 = ( ( double ) hist[         r * resolution.a() +         c ] - minV ) / ( double ) deltaV;
            const float r2 = ( ( double ) hist[ ( r + 1 ) * resolution.a() +         c ] - minV ) / ( double ) deltaV;
            const float r3 = ( ( double ) hist[         r * resolution.a() + ( c + 1 ) ] - minV ) / ( double ) deltaV;
            const float r4 = ( ( double ) hist[ ( r + 1 ) * resolution.a() + ( c + 1 ) ] - minV ) / ( double ) deltaV;

            int sign1 = hist[ r         * resolution.a() +         c ] < 0 ? -1 : 1;
            int sign2 = hist[ ( r + 1 ) * resolution.a() +         c ] < 0 ? -1 : 1;
            int sign3 = hist[         r * resolution.a() + ( c + 1 ) ] < 0 ? -1 : 1;
            int sign4 = hist[ ( r + 1 ) * resolution.a() + ( c + 1 ) ] < 0 ? -1 : 1;

            double normalization = 1;
            if( m_samplerWidget.controlPanel.colorScalingPanel.colorScalingCombo.selectedItemText() == "local" )
            {
                normalization = std::max( std::abs( minV ), std::abs( maxV ) );
            }
            else
            {
                normalization = m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition() * m_maxFrequencyInSelectedTS;
            }

            const float f1 = std::abs( hist[         r * resolution.a() +         c ] / normalization );
            const float f2 = std::abs( hist[ ( r + 1 ) * resolution.a() +         c ] / normalization );
            const float f3 = std::abs( hist[         r * resolution.a() + ( c + 1 ) ] / normalization );
            const float f4 = std::abs( hist[ ( r + 1 ) * resolution.a() + ( c + 1 ) ] / normalization );

            verts[ OFFSET     ] =                   Vec3< float >( c,      0, r );
            verts[ OFFSET + 1 ] = verts[ OFFSET ] + Vec3< float >( 0, r2 * h, 1 );
            verts[ OFFSET + 2 ] = verts[ OFFSET ] + Vec3< float >( 1, r3 * h, 0 );

            verts[ OFFSET + 3 ] = verts[ OFFSET ] + Vec3< float >( 1, r3 * h, 0 );
            verts[ OFFSET + 4 ] = verts[ OFFSET ] + Vec3< float >( 0, r2 * h, 1 );
            verts[ OFFSET + 5 ] = verts[ OFFSET ] + Vec3< float >( 1, r4 * h, 1 );

            verts[ OFFSET     ] = verts[ OFFSET ] + Vec3< float >( 0, r1 * h, 0 );

            Vec3< float > n1 = Vec3< float >::surfaceNormal(
                                   verts[ OFFSET     ],
                                   verts[ OFFSET + 1 ],
                                   verts[ OFFSET + 2 ] );

            Vec3< float > n2 = Vec3< float >::surfaceNormal(
                                   verts[ OFFSET + 3 ],
                                   verts[ OFFSET + 4 ],
                                   verts[ OFFSET + 5 ] );

            Vec3< float > v1 = verts[ OFFSET     ].rounded( 1 );
            Vec3< float > v2 = verts[ OFFSET + 1 ].rounded( 1 );
            Vec3< float > v3 = verts[ OFFSET + 2 ].rounded( 1 );

            Vec3< float > v4 = verts[ OFFSET + 3 ].rounded( 1 );
            Vec3< float > v5 = verts[ OFFSET + 4 ].rounded( 1 );
            Vec3< float > v6 = verts[ OFFSET + 5 ].rounded( 1 );

            addNormal( v1, n1, adjacentFaceNormals );
            addNormal( v2, n1, adjacentFaceNormals );
            addNormal( v3, n1, adjacentFaceNormals );

            addNormal( v4, n2, adjacentFaceNormals );
            addNormal( v5, n2, adjacentFaceNormals );
            addNormal( v6, n2, adjacentFaceNormals );

            normals[ OFFSET     ] += n1;
            normals[ OFFSET + 1 ] += n1 + n2;
            normals[ OFFSET + 2 ] += n1;

            normals[ OFFSET + 3 ] += n2 + n1;
            normals[ OFFSET + 4 ] += n2;
            normals[ OFFSET + 5 ] += n2;

            // v0
            if( r > 0 )
            {
                normals[ OFFSET     ] += normals[ OFFSET - MESH_DIM_A * 6 + 1 ] + normals[ OFFSET - MESH_DIM_A * 6 + 4 ];
            }
            if( c > 0 )
            {
                normals[ OFFSET     ] += normals[ OFFSET - 6 + 2 ] + normals[ OFFSET - 6 + 3 ];
            }
            if( r > 0 && c > 0)
            {
                normals[ OFFSET     ] += normals[ OFFSET - ( MESH_DIM_A * 6 + 6 ) + 5 ];
            }

            normals[ OFFSET     ].normalize();


            const size_t TF_SIZE = m_histTF.size();
            const size_t TF_SIZE_M1 = TF_SIZE - 1;

            double rc1 = 0.5 + f1 * 0.5;
            double rc2 = 0.5 + f1 * 0.5;
            double rc3 = 0.5 + f1 * 0.5;
            double rc4 = 0.5 + f1 * 0.5;

            colors[ OFFSET     ] = m_histTF[ std::min( ( size_t ) std::floor( rc1 * TF_SIZE ),  TF_SIZE_M1 ) ];
            colors[ OFFSET + 1 ] = m_histTF[ std::min( ( size_t ) std::floor( rc2 * TF_SIZE ),  TF_SIZE_M1 ) ];
            colors[ OFFSET + 2 ] = m_histTF[ std::min( ( size_t ) std::floor( rc3 * TF_SIZE ),  TF_SIZE_M1 ) ];
            colors[ OFFSET + 5 ] = m_histTF[ std::min( ( size_t ) std::floor( rc4 * TF_SIZE ),  TF_SIZE_M1 ) ];
            colors[ OFFSET + 3 ] = colors[ OFFSET + 2 ];
            colors[ OFFSET + 4 ] = colors[ OFFSET + 1 ];
        }
    }

    const size_t VSZ = verts.size();
    #pragma omp parallel for
    for( size_t i = 0; i < VSZ; ++i )
    {
        std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
        if( ( it = adjacentFaceNormals.find( verts[ i ].rounded( 1 ) ) ) != adjacentFaceNormals.end() )
        {
            normals[ i ] = it->second;
            normals[ i ].normalize();
        }
    }

    QMatrix4x4 M;
    M.setToIdentity();

//    M.rotate(  15, QVector3D( 1.0, 0.0, 0.2f ) );

    M.rotate(  m_selectedHistogramRotation, QVector3D( 0.0, 1.0, 0.0 ) );
    M.rotate(  m_selectedHistogramRotationY, QVector3D( 1.0, 0.0, 0.0 ) );

    M.scale( 0.3 / ( (double)resolution.a() ) );
    M.translate( -resolution.a()/2.0, -resolution.a() / 2.0, -resolution.a() /2.0  );

    detCam.setCenter( QVector3D( 0, 0, 0 ) );
    detCam.setAspectRatio( 1 );
    detCam.reset( QVector3D( -1, -1, -1 ), QVector3D( 1, 1, 1 )  );

    QMatrix4x4 P = detCam.matProj();
    QMatrix4x4 V = detCam.matView( 0.07 );

    glEnable( GL_DEPTH_TEST );
    glDisable( GL_CULL_FACE );

    Light _light = m_isoLight;
    _light.direction = QVector3D( -0.7, 1, 1 );

    Material _material = m_material1;
    _material.Ka_scale = .3;
    _material.Kd_scale = 0.8;
    _material.Ks_scale = 0.7;
    _material.shininess = 20;

    m_renderer->renderTriangleMesh(
        verts,
        normals,
        colors,
        _material,
        _light,
        M,
        V,
        P,
        prog3D,
        true );

    _material.Ka_scale = 0;
    _material.Kd_scale = 0.8;
    _material.Ks_scale = 0.7;
    _material.shininess = 20;

    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

//    glLineWidth( 2.0 );

//    m_renderer->renderPrimitivesFlat(
//        posYAxis,
//        flatProg,
//        QVector4D( 1.0, 1.0, 0.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        negXAxis,
//        flatProg,
//        QVector4D( 0.0, 0.0, 1.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        posXAxis,
//        flatProg,
//        QVector4D( 0.0, 1.0, 0.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        negZAxis,
//        flatProg,
//        QVector4D( 0.4, 0.0, 1.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        posZAxis,
//        flatProg,
//        QVector4D( 1.0, 0.0, 0.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

    glDisable( GL_DEPTH_TEST );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

void BALEEN::renderSelectedHistogram1D( )
{
    if ( m_sampler.getSelectedCellId() >= 0 )
    {
        m_renderer->renderPrimitivesColored(
            m_selectedHistogramVertices,
            m_selectedHistogramColors,
            simpleColorProg,
            camera.proj() * camera.view(),
            GL_TRIANGLES );
    }
}

void BALEEN::constructTimeHistogramStack1D(
    Waterfall & result,
    const std::string & ptype,
    const std::string & attr,
    const SpacePartitioningDefinition & spacePartitioning,
    const TimeSeriesDefinition & timeSeries,
    int selectedCell )
{
    const int N_BINS = result.bins;
    const int NUIM_STEPS = timeSeries.numSteps();
    result.values.resize( NUIM_STEPS * N_BINS );
    std::fill( result.values.begin(), result.values.end(), 0.0 );

    const Vec2< double > RANGE = m_dataSetManager->particleDataManager().variableRange( ptype, attr );
    const double binsOverWidth = N_BINS / ( RANGE.b() - RANGE.a() );
    const Vec2< float > spatialScaling = spatialScaling2D();

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    #pragma omp parallel for
    for( int t = timeSeries.firstIdx; t < timeSeries.lastIdx; t += timeSeries.idxStride )
    {
        int ti = ( t - timeSeries.firstIdx ) / timeSeries.idxStride;

        const std::vector< float > & xSpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 0 ] ) );
        const std::vector< float > & ySpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 1 ] ) );
        const std::vector< float > & w = *( m_dataSetManager->particleDataManager().values( t, ptype, attr ) );

        const int OFFS = ti*N_BINS;

        int count = 0;
        for( int i = 0; i < NP; ++i )
        {
            if( m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() ) == selectedCell )
            {
                ++count;
                double d = w[ i ] - RANGE.a();
                int b = std::min( static_cast< int >( std::floor( binsOverWidth  * d ) ), N_BINS-1 );
                result.values[ OFFS + b ] += 1;
            }
        }
        for( int b = 0; b < N_BINS; ++b )
        {
            result.values[ OFFS + b ] /= (double) count;
        }
    }
}

inline Vec2< float > getAdjustedValueRanges( const Vec2< float > & vRange, double modifier )
{
    double adjustedValueOffset = ( ( vRange.b() - vRange.a() ) / 2.0 ) * ( 1.0 - modifier );
    return Vec2< float >( vRange.a() + adjustedValueOffset, vRange.b() - adjustedValueOffset );
}

I2 BALEEN::getAdjustedHistResolution( I2 origionalResolution )
{
    const double RANGE_FACTOR = 10.0;
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        float r1 = m_samplerWidget.controlPanel.histogramPanel.resolutionSlider.sliderPosition();
        float r2 = r1;
        I2 newResolution( r1 * origionalResolution.a() * RANGE_FACTOR, r2 * origionalResolution.b() * RANGE_FACTOR );
        return I2( std::max( newResolution.a(), 1 ), std::max( newResolution.b(), 1 ) );
    }
    else
    {
        return origionalResolution;
    }
}

void BALEEN::stackHistograms2D_GPU( FrVolume & frVol )
{
    const std::string ptype = m_particleCombo.selectedItemText();
    const std::string dtype = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();

    const HistogramDefinition & hdf = m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( ptype )->second.find( dtype )->second;
    const I2 histResolution = getAdjustedHistResolution( { hdf.resolution[ 0 ], hdf.resolution[ 1 ] } );

    const SpacePartitioningDefinition spatialParitioning = m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second;

    const bool useWeight = hdf.weightType == HistogramWeightType::VARIABLE;

    const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners( );
    const int K = m_sampler.getSelectedCellId();

    const int numParticles = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    static bool dataLoaded = false;
    if( ( ! dataLoaded ) || m_reloadGPUData )
    {
        const TimeSeriesDefinition tsrs = m_dataSetManager->particleDataManager().currentTimeSeries();

        // Vx,y

        const int FIRST = tsrs.firstIdx;
        const int LAST  = tsrs.lastIdx;
        const int STRIDE = tsrs.idxStride;
        const int NT = tsrs.numSteps();
        const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );
        const int NUM_DATA_POINTS = NP*NT;

        m_x0Buffer.resize( NUM_DATA_POINTS );
        m_x1Buffer.resize( NUM_DATA_POINTS );

        m_histStackIso.resizeBuffer( NT, NP, useWeight );

        #pragma omp parallel for
        for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
        {
            int ti = ( t - FIRST ) / STRIDE;

            const std::vector< float > & vxT = *( m_dataSetManager->particleDataManager().values( t, ptype, hdf.binningSpace[ 0 ] ) );
            const std::vector< float > & vyT = *( m_dataSetManager->particleDataManager().values( t, ptype, hdf.binningSpace[ 1 ] ) );

            for ( unsigned i = 0; i < NP; ++i )
            {
                m_x0Buffer[ ti * NP + i ] = vxT[ i ];
                m_x1Buffer[ ti * NP + i ] = vyT[ i ];
            }
        }

        m_histStackIso.bufferV( m_x0Buffer, m_x1Buffer );

        #pragma omp parallel for
        for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
        {
            int ti = ( t - FIRST ) / STRIDE;

            const std::vector< float > & sxT = *( m_dataSetManager->particleDataManager().values( t, ptype, spatialParitioning.attributes[ 0 ] ) );
            const std::vector< float > & syT = *( m_dataSetManager->particleDataManager().values( t, ptype, spatialParitioning.attributes[ 1 ] ) );

            for ( unsigned i = 0; i < NP; ++i )
            {
                m_x0Buffer[ ti * NP + i ] = sxT[ i ];
                m_x1Buffer[ ti * NP + i ] = syT[ i ];
            }
        }

        m_histStackIso.bufferS( m_x0Buffer, m_x1Buffer );

        if( useWeight )
        {
            #pragma omp parallel for
            for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
            {
                int ti = ( t - FIRST ) / STRIDE;

                const std::vector< float > & wT = *( m_dataSetManager->particleDataManager().values( t, ptype, hdf.weight ) );

                for ( unsigned i = 0; i < NP; ++i )
                {
                    m_x0Buffer[ ti * NP + i ] = wT[ i ];
                }
            }
            m_histStackIso.bufferW( m_x0Buffer );
        }

        dataLoaded = true;
        m_reloadGPUData = false;
        m_updatedGPUData = true;
    }

    static int oldVolumeSize = 0;
    int newVolumeSize = m_dataSetManager->particleDataManager().numLoadedTimeSteps() * histResolution.a() * histResolution.b();
    if( newVolumeSize != oldVolumeSize )
    {
        m_histStackIso.allocateVolume( histResolution.b(), histResolution.a(), m_dataSetManager->particleDataManager().numLoadedTimeSteps() );
        oldVolumeSize = newVolumeSize;
    }

    const Vec2< float > pScale = spatialScaling2D();

    const std::vector< float > & valueRangeModifiers =
    {
        m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition(),
        m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.sliderPosition()
    };

    const std::array< Vec2< float >, 2 > & histThresholds = std::array< Vec2< float >, 2 >(
    {
        getAdjustedValueRanges( hdf.valueRange[ 0 ], valueRangeModifiers[ 0 ] ),
        getAdjustedValueRanges( hdf.valueRange[ 1 ], valueRangeModifiers[ 1 ] )
    } );

    m_histStackIso.compute(
        histThresholds[ 0 ].a(), histThresholds[ 0 ].b(),
        histThresholds[ 1 ].a(), histThresholds[ 1 ].b(),
        gridCorners[ K*4 + 2 ].x(), gridCorners[ K*4 + 0 ].x(),
        gridCorners[ K*4 + 1 ].y(), gridCorners[ K*4 + 0 ].y(),
        pScale.x(), pScale.y(),
        numParticles,
        histResolution.b(),
        histResolution.a(),
        m_dataSetManager->particleDataManager().numLoadedTimeSteps(),
        frVol.values,
        useWeight );

    if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local )
    {
        m_frVolumeCache.normalizeVolume( K );
    }
}

void BALEEN::constructTimeHistogramStack2D( FrVolume & frVolume, int dataType, BaseHistDefinition * histDefinition, const I2 & histResolution, const Vec2< double > & spatialScaling, const std::string & ptype, const std::string & dtype, int cellSelection )
{
    const int32_t NUM_TIME_STEPS = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                                   : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    //
    std::fill( frVolume.values.begin(), frVolume.values.end(), 0.f  );
    //

    if( dataType == PopulationDataType::PARTICLES )
    {
        const TimeSeriesDefinition tsrs = m_dataSetManager->particleDataManager().currentTimeSeries();
        const HistogramDefinition & hDef =  * dynamic_cast< HistogramDefinition * >( histDefinition );
        const SpacePartitioningDefinition & spatialParitioning = m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second;

        const std::vector< float > & valueRangeModifiers =
        {
            m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition(),
            m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition()
        };

        const std::array< Vec2< float >, 2 > & histThresholds = std::array< Vec2< float >, 2 >(
        {
            getAdjustedValueRanges( hDef.valueRange[ 0 ], valueRangeModifiers[ 0 ] ),
            getAdjustedValueRanges( hDef.valueRange[ 1 ], valueRangeModifiers[ 1 ] )
        } );

        #pragma omp parallel for
        for( int t = 0; t < NUM_TIME_STEPS; ++t )
        {
            const int idx = tsrs.firstIdx + t * tsrs.idxStride;
            stackHistogram2D( frVolume, hDef, spatialParitioning, histThresholds, histResolution, spatialScaling, ptype, t, idx, cellSelection );
        }
    }
    else if( dataType == PopulationDataType::GRID_POINTS )
    {
        const TimeSeriesDefinition tsrs = m_dataSetManager->distributionManager().currentTimeSeries();
        const std::vector< int > & gMap = m_sampler.gridMap()[ m_sampler.getSelectedCellId() ];

        #pragma omp parallel for
        for( int t = 0; t < NUM_TIME_STEPS; ++t )
        {
            const int idx = tsrs.firstIdx + t * tsrs.idxStride;
            stackHistogram2D_DistGrid( frVolume, t, gMap, *( m_dataSetManager->distributionManager().distributions( ptype, dtype, idx ).get() ), histResolution.a(), histResolution.b() );
        }
    }

    if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local )
    {
        m_frVolumeCache.normalizeVolume( cellSelection );
    }
}

template <typename T>
inline  std::vector<size_t> sort_indexes(const std::vector<T> &v)
{

    // initialize original index locations
    std::vector<size_t> idx(v.size());
    iota(idx.begin(), idx.end(), 0);

    // sort indexes based on comparing values in v
    std::sort(idx.begin(), idx.end(),
              [&v](size_t i1, size_t i2)
    {
        return v[i1] < v[i2];
    });

    return idx;
}

void BALEEN::renderSpatialHistStack2D()
{
    const std::vector< float > & xPos = m_dataSetManager->distributionManager().values( "r" );
    const std::vector< float > & yPos = m_dataSetManager->distributionManager().values( "z" );
    std::vector< float > rSel;
    std::vector< int > selectionIndices;

    for( auto i = 0U; i < xPos.size(); ++i )
    {
        if( std::abs( yPos[ i ] ) <= .001 )
        {
            selectionIndices.push_back( i );
            rSel.push_back( xPos[ i ] );
        }
    }

    ////////////////////////////qDebug() << "found " << rSel.size() << "grid points";

    std::vector< size_t > resortIndices = sort_indexes( rSel );
    std::vector< int > sortedIndices;
    for( auto i = 0U; i < resortIndices.size(); ++i )
    {
        sortedIndices.push_back( selectionIndices[ resortIndices[ i ] ] );
    }

    std::vector< float > stack( 30 * 33 * 17  );

    for( int i = 0; i < 30; ++i )
    {
        std::vector< float > & values = *( m_dataSetManager->distributionManager().distributions( "ions", m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText(), 0 ) );
        for( int r = 0; r < 17; ++r )
        {
            for ( int c = 0; c < 33; ++c )
            {
                stack[ i * 33*17 + r * 33 + c ] = values[ sortedIndices[ i + 30 ] * 33*17 + r * 33 + c ];
            }
        }
    }

    //////////

    std::vector<float> targetValues;

    targetValues.resize(2);

    // nomalize locally for each step
    for( int step = 0; step < 30; ++step )
    {
        float localMax = 0.0;
        for( int b = 0; b < 17*33; ++b )
        {
            localMax = std::max( localMax, std::abs( stack[ step * 33*17 + b ] ) );
        }
        for( int b = 0; b < 17*33; ++b )
        {
            stack[ step * 33*17 + b ] /= localMax;
        }
    }

    targetValues[0] =  m_isoValueSlider.sliderPosition();
    targetValues[1] = -m_isoValueSlider.sliderPosition();

    const size_t TF_SIZE = m_histTF.size();
    const size_t TF_SIZE_M1 = TF_SIZE - 1;

    double r1 = 0.5 + m_isoValueSlider.sliderPosition() * 0.5;
    double r2 = 0.5 - m_isoValueSlider.sliderPosition() * 0.5;

    Vec3< float > isoColor0 = m_histTF[ std::min( ( size_t ) std::floor( r1 * TF_SIZE ),  TF_SIZE_M1 ) ];
    Vec3< float > isoColor1 = m_histTF[ std::min( ( size_t ) std::floor( r2 * TF_SIZE ),  TF_SIZE_M1 ) ];

    m_isoMaterial.opacity   = 1.0;
    m_isoMaterial.Kd_scale  = 0.9f;
    m_isoMaterial.Ks_scale  = 0.4f;
    m_isoMaterial.Ka_scale  = 0.2f;
    m_isoMaterial.shininess = 5.0;

    const int NUM_STEPS = 30;

    ( *m_marchCubes[ 0 ] )(
        stack,
        NUM_STEPS,
        17,
        33,
        targetValues[ 0 ] );

    ( *m_marchCubes[ 1 ] )(
        stack,
        NUM_STEPS,
        17,
        33,
        targetValues[ 1 ] );

    std::vector< Vec3< float > > boundingBox( 32 );
    std::vector< Vec3< float > > timeStepIndictor( 8 );
    std::vector< Vec3< float > > negXAxis( 2 );
    std::vector< Vec3< float > > negYAxis( 2 );
    std::vector< Vec3< float > > xAxis( 2 );
    std::vector< Vec3< float > > yAxis( 2 );

    double X = 17;
    double Y = 33;

    QMatrix4x4 M;
    M.setToIdentity();
    M.rotate(  m_histogramStackRotationY, QVector3D( 0.0, 1.0, 0.0 ) );
    M.rotate(  m_histogramStackRotation, QVector3D( 1.0, 0.0, 0.0 ) );
    M.scale( 1.1 / (float)NUM_STEPS, -( 2.0 * 23 ) / ( (float)380.0 * X ), ( 2.0 * 23 ) / ( (float)380.0 * Y )  );
    M.translate( -NUM_STEPS/2.0, -X / 2.0, -Y /2.0   );

    QMatrix4x4 VolumeModel;

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        VolumeModel.setToIdentity();
        VolumeModel.rotate(  m_histogramStackRotationY, QVector3D( 0.0, 1.0, 0.0 ) );
        VolumeModel.rotate(  m_histogramStackRotation, QVector3D( 1.0, 0.0, 0.0 ) );
        VolumeModel.scale( 1.1 / (float)NUM_STEPS, -( 2.0 * 23 ) / ( (float)380.0 * X *2  ), ( 2.0 * 23 ) / ( (float)380.0 * Y )  );
        VolumeModel.translate( -NUM_STEPS/2.0, 0, -Y /2.0   );
    }
    else
    {
        VolumeModel = M;
    }

    QMatrix4x4  P (
        7.0972,         0,         0,         0,
        0,   45.4221,         0,         0,
        0,         0,  -1.00007, -0.200007,
        0,         0,        -1,         0 );
    QMatrix4x4 V (
        1,         0,         0,         0,
        0,         1,         0,         0,
        0,         0,         1,        -4,
        0,         0,         0,         1 );

    glViewport( m_timeHistIsoViewport.offsetX(), m_timeHistIsoViewport.offsetY(), m_timeHistIsoViewport.width(), m_timeHistIsoViewport.height() );
    glEnable( GL_DEPTH_TEST );
    m_renderer->renderTriangleMesh(
        m_marchCubes[ 0 ]->getVerts(),
        m_marchCubes[ 0 ]->getNormals(),
        QVector4D( isoColor0.r(), isoColor0.g(), isoColor0.b(), 1.0 ),
        VolumeModel,
        V,
        P,
        flatMeshProg );

    m_renderer->renderTriangleMesh(
        m_marchCubes[ 1 ]->getVerts(),
        m_marchCubes[ 1 ]->getNormals(),
        QVector4D( isoColor1.r(), isoColor1.g(), isoColor1.b(), 1.0 ),
        VolumeModel,
        V,
        P,
        flatMeshProg );

    glDisable( GL_DEPTH_TEST );

}

void BALEEN::renderUncertaintyPlot()
{
//    I2 histResolution;

//    std::string ptype = m_particleCombo.selectedItemText();
//    std::string dtype = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();

//    if( m_populationDataType == "particle" )
//    {
//        std::unordered_map< int, std::unordered_map< int, double > >::const_iterator it;
//        HistogramDefinition & hdf = m_userDefinedHistograms.find( ptype )->second.find( dtype )->second;
//        histResolution = getAdjustedHistResolution();
//    }
//    else if ( m_populationDataType == "distribution" )
//    {
//        PredefinedHistogram & pdh = m_predefinedHistograms.find( ptype )->second.find( dtype )->second;
//        histResolution = getAdjustedHistResolution();
//    }

//    const int PLOT_RESOLUTION = 17;
//    std::vector< float > resultAverage, resultVariance;

//    float isoMax = 0;
//    float isoMin = 0;
//    for( auto f : histogramStackFrequencies )
//    {
//        isoMax = std::max( isoMax, f );
//        isoMin = std::min( isoMin, f );
//    }

//    if( isoMax == 0 || isoMax == isoMin )
//    {
//        return;
//    }

//    ////////////////////////////qDebug() << "calculating uncertainty";

//    Vec2< float > avRange, vaRange;

//    m_gradUncertCalc.calculate(
//        histogramStackFrequencies,
//        histResolution.a(),
//        histResolution.b(),
//        numTimeSteps,
//        PLOT_RESOLUTION,
//        isoMax,
//        isoMin,
//        resultAverage,
//        resultVariance,
//        avRange,
//        vaRange );

//    if( avRange.b() == 0 || avRange.a() == avRange.b() )
//    {
//        return;
//    }

//    for( auto & v : resultAverage )
//    {
//        v = ( v - avRange.a() ) / ( avRange.b() - avRange.a() );
//    }

//    for( auto & v : resultVariance )
//    {
//        v = ( v - vaRange.a() ) / ( vaRange.b() - vaRange.a() );
//    }

//    glViewport(
//        m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT,
//        m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM,
//        m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_LEFT - m_timeLineWidget.MARGIN_RIGHT,
//        m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP );

//    //m_renderer->renderTexturedQuad( resultVariance, PLOT_RESOLUTION, numTimeSteps, ucProg  );
//    m_renderer->renderTexturedQuad( resultAverage, PLOT_RESOLUTION, numTimeSteps, ucProg  );
}

void BALEEN::mergeGridPoints()
{
    std::string ptype = m_particleCombo.selectedItemText();
    std::string dtype = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();

    const std::vector< std::vector< int > > & gridMap = m_sampler.gridMap();
    //XYK4
    PredefinedHistogram & pdh = m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.find( ptype )->second.find( dtype )->second;
    const std::vector< float > & gridCellVolumes = m_dataSetManager->distributionManager().values( "volume" );
    const double NORM_FACTOR = pdh.normalizationFactor;
    const std::vector< float >  & distributionValues = *( m_dataSetManager->distributionManager().distributions( ptype, dtype, m_selectedTimeStep ) );

    m_mergedGridPointHists.clear();
    m_localMaxFrequenciesInSelectedTS.clear();
    m_gridMergingNormalizationTerm.clear();

    I2 res = getAdjustedHistResolution( { pdh.resolution[ 0 ], pdh.resolution[ 1 ] } );
    m_maxFrequencyInSelectedTS = 0;

    const int COLS = res.a();
    const int ROWS = res.b();
    const int BINS_PER_HIST = ROWS * COLS;
    const int N_REGIONS = gridMap.size();

    for( int k = 0; k < N_REGIONS; ++k )
    {
        m_mergedGridPointHists.insert( { k, std::vector< double >( BINS_PER_HIST, 0 ) } );
        m_localMaxFrequenciesInSelectedTS.insert( { k, 0 } );
        m_gridMergingNormalizationTerm.insert( { k, 0 } );
    }

    #pragma omp parallel for
    for( int k = 0; k < N_REGIONS; ++k )
    {
        std::vector< double > & mergeResult = m_mergedGridPointHists.find( k )->second;
        const int SZ = gridMap[ k ].size();

        double volumeSum = 0;

        for ( int i = 0; i < SZ; ++i )
        {
            volumeSum += gridCellVolumes[ gridMap[ k ][ i ] ];
        }

        if( volumeSum > 0 )
        {
            for ( int i = 0; i < SZ; ++i )
            {
                const int GRID_OFFSET = gridMap[ k ][ i ];

                for ( int row = 0; row < ROWS; ++row )
                {
                    const int Cr = row * COLS;
                    const int GRID_AND_ROW_OFFSET = Cr + GRID_OFFSET * BINS_PER_HIST;

                    #pragma omp simd
                    for ( int col = 0; col < COLS; ++col )
                    {
                        mergeResult[ Cr + col ] += distributionValues[ GRID_AND_ROW_OFFSET + col ];
                    }
                }
            }
            for( auto & v : mergeResult )
            {
                v *= NORM_FACTOR / volumeSum;
            }

            // smooth merge result
            if( m_samplerWidget.controlPanel.colorScalingPanel.smoothingCombo.selectedItemText() == "3x3 average" )
            {
                const int RAD = 1;
                for ( int row = 0; row < ROWS; ++row )
                {
                    for ( int col = 0; col < COLS; ++col )
                    {
                        int count = 0;
                        double sum = 0;
                        for( int cc = std::max( col - RAD, 0 ); cc <= std::min( col + RAD, COLS-1 ); ++cc )
                        {
                            for( int rr = std::max( row - RAD, 0 ); rr <= std::min( row + RAD, ROWS-1 ); ++rr )
                            {
                                sum += mergeResult[ rr*COLS + cc ];
                                ++count;
                            }
                        }
                        mergeResult[ row*COLS + col ] = sum / count;
                    }
                }
            }
            m_localMaxFrequenciesInSelectedTS.find( k )->second =
                TN::Sequential::maxMag( mergeResult );

            m_gridMergingNormalizationTerm.find( k )->second = NORM_FACTOR / volumeSum;
        }
        else
        {
            m_localMaxFrequenciesInSelectedTS.find( k )->second = 0.f;
            m_gridMergingNormalizationTerm.find( k )->second = 0.f;
        }
    }

    m_maxFrequencyInSelectedTS = 0;
    for( const auto lmf : m_localMaxFrequenciesInSelectedTS )
    {
        m_maxFrequencyInSelectedTS = std::max( lmf.second, m_maxFrequencyInSelectedTS );
    }
}

void BALEEN::renderGridTimeSeries(
    bool showAll,
    bool showSel )
{

    return;

    const int NUM_TIME_STEPS = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                               : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    static std::vector< double > allMean     = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > allRMS      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > allSum      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > allVariance = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > allMin      = std::vector< double >( NUM_TIME_STEPS,  std::numeric_limits< float >::max() );
    static std::vector< double > allMax      = std::vector< double >( NUM_TIME_STEPS, -std::numeric_limits< float >::max() );

    static std::vector< double > selMean     = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > selRMS      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > selSum      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > selVariance = std::vector< double >( NUM_TIME_STEPS, 0.0 );
    static std::vector< double > selMin      = std::vector< double >( NUM_TIME_STEPS,  std::numeric_limits< float >::max() );
    static std::vector< double > selMax      = std::vector< double >( NUM_TIME_STEPS, -std::numeric_limits< float >::max() );

    static std::vector< double > xAxis = std::vector< double >( NUM_TIME_STEPS, 0.0 );

    static bool firstTime = true;
    static std::string aggS = m_timePlotModeCombo.selectedItemText();
    static int selectedS = m_sampler.getSelectedCellId();

    std::string agg = m_timePlotModeCombo.selectedItemText();
    int selected = m_sampler.getSelectedCellId();

    if( firstTime )
    {
        std::cout << "initializing all" << std::endl;

        allMean     = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        allRMS      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        allSum      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        allVariance = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        allMin      = std::vector< double >( NUM_TIME_STEPS,  std::numeric_limits< double >::max() );
        allMax      = std::vector< double >( NUM_TIME_STEPS, -std::numeric_limits< double >::max() );

        for( int64_t t = 0; t < NUM_TIME_STEPS; ++t )
        {
            const std::vector< float >  & meanValues  = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_mean", t );
            const std::vector< float >  & countValues = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "num_particles", t );
            const std::vector< float >  & minValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_min", t );
            const std::vector< float >  & maxValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_max", t );
            const std::vector< float >  & rmsValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_rms", t );
            const std::vector< float >  & varValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_variance", t );

            std::cout << "all, accessed timestep " << t << std::endl;

            const int64_t NG = meanValues.size();
            double totalCount = 0;

            for( int64_t g = 0; g < NG; ++g )
            {
                allMean[ t ] += meanValues[ g ] * countValues[ g ];

                allMin[  t ] = std::min( allMin[ t ], (double) minValues[ g ] );
                allMax[  t ] = std::max( allMax[ t ], (double) maxValues[ g ] );

                allVariance[ t ] += varValues[ g ] * std::max( 0.f, ( countValues[ g ] - 1.f ) );

                totalCount += countValues[ g ];
            }

            for( int g = 0; g < NG; ++g )
            {
                double v = ( rmsValues[ g ] * countValues[ g ] );
                allRMS[ t ] += v*v;
            }

            allSum = allMean; // before normalization

            if( totalCount > 0 )
            {
                allVariance[ t ] /= totalCount;
                allMean[ t ]     /= totalCount;
                allRMS[ t ] = std::sqrt( allRMS[ t ] ) / totalCount;
            }
        }

        std::cout << "done with all initialization " <<  std::endl;
    }

    if( ( firstTime || agg != aggS || selected != selectedS ) && selected >= 0 && false )
    {
        std::cout << "initializing selection" << std::endl;

        selMean     = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        selRMS      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        selSum      = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        selVariance = std::vector< double >( NUM_TIME_STEPS, 0.0 );
        selMin      = std::vector< double >( NUM_TIME_STEPS,  std::numeric_limits< double >::max() );
        selMax      = std::vector< double >( NUM_TIME_STEPS, -std::numeric_limits< double >::max() );

        const std::vector< int > & gridMap = m_sampler.gridMap()[ selected ];

        for( int64_t t = 0; t < NUM_TIME_STEPS; ++t )
        {
            const std::vector< float >  & meanValues  = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_mean", t );
            const std::vector< float >  & countValues = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "num_particles", t );
            const std::vector< float >  & minValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_min", t );
            const std::vector< float >  & maxValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_max", t );
            const std::vector< float >  & rmsValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_rms", t );
            const std::vector< float >  & varValues   = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_variance", t );

            const int64_t NG = gridMap.size();
            double totalCount = 0;

            for( int64_t i = 0; i < NG; ++i )
            {
                const int g = gridMap[ i ];

                selMean[ t ] += meanValues[ g ] * countValues[ g ];

                selMin[  t ] = std::min( selMin[ t ], (double) minValues[ g ] );
                selMax[  t ] = std::max( selMax[ t ], (double) maxValues[ g ] );

                selVariance[ t ] += varValues[ g ] * std::max( 0.f, ( countValues[ g ] - 1.f ) );

                totalCount += countValues[ g ];
            }

            for( int g = 0; g < NG; ++g )
            {
                double v = ( rmsValues[ g ] * countValues[ g ] );
                selRMS[ t ] += v*v;
            }

            selSum = selMean; // before normalization

            if( totalCount > 0 )
            {
                selVariance[ t ] /= totalCount;
                selMean[ t ] /= totalCount;
                selRMS[ t ] = std::sqrt( selRMS[ t ] ) / totalCount;
            }
        }
    }

    xAxis.resize( NUM_TIME_STEPS );
    for( int64_t t = 0; t < NUM_TIME_STEPS; ++t )
    {
        xAxis[ t ] =  t;
    }

    firstTime = false;
    aggS = agg;
    selected = selectedS;

    // now plot them ...

    const QVector4D allColorTP( .2, .2, .2, 0.8 );
    const QVector4D regionsColorTP(  0.6, 0.3, 0.5, 0.8 );

    const auto & yAll = ( agg == "mean"    ? allMean
                          : agg == "rms"      ? allRMS
                          : agg == "sum"      ? allSum
                          : agg == "variance" ? allVariance
                          : agg == "min"      ? allMin
                          : allMax );

    const auto & ySel = ( agg == "mean"    ? selMean
                          : agg == "rms"      ? selRMS
                          : agg == "sum"      ? selSum
                          : agg == "variance" ? selVariance
                          : agg == "min"      ? selMin
                          : selMax );

    double lowPlot;
    double highPlot;


    if( showAll && showSel && m_sampler.getSelectedCellId() >= 0 )
    {
        double lowValueAll  = *std::min_element( yAll.begin(), yAll.end() );
        double highValueAll = *std::max_element( yAll.begin(), yAll.end() );

        double lowValueSel  = *std::min_element( ySel.begin(), ySel.end() );
        double highValueSel = *std::max_element( ySel.begin(), ySel.end() );

        lowPlot  = std::min(  lowValueAll,  lowValueSel );
        highPlot = std::max( highValueAll, highValueSel );
    }
    else if( showAll )
    {
        double lowValueAll  = *std::min_element( yAll.begin(), yAll.end() );
        double highValueAll = *std::max_element( yAll.begin(), yAll.end() );

        lowPlot = lowValueAll;
        highPlot = highValueAll;
    }
    else if( showSel && m_sampler.getSelectedCellId() >= 0 )
    {
        double lowValueSel  = *std::min_element( ySel.begin(), ySel.end() );
        double highValueSel = *std::max_element( ySel.begin(), ySel.end() );

        lowPlot = lowValueSel;
        highPlot = highValueSel;
    }

    lowPlot  = lowPlot  - ( highPlot - lowPlot ) / 18.0;
    highPlot = highPlot + ( highPlot - lowPlot ) / 18.0;

    if( showAll )
    {
        const Vec2< double > & yRange = { lowPlot, highPlot };

        renderTemporalPlot(
            xAxis,
            yAll,
            yRange,
            *getTimeSeries( m_particleCombo.selectedItemText() ),
            m_selectedTimeStep,
            allColorTP );
    }
    if( showSel && m_sampler.getSelectedCellId() >= 0 )
    {
        const Vec2< double > & yRange = { lowPlot, highPlot };

        renderTemporalPlot(
            xAxis,
            ySel,
            yRange,
            *getTimeSeries( m_particleCombo.selectedItemText() ),
            m_selectedTimeStep,
            regionsColorTP );
    }

    //        m_renderer->renderText( false,
//                                textProg,
//                                scaledWidth(),
//                                scaledHeight(),
//                                to_string_with_precision( yRange.a(), TEXT_PREC ),
//                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
//                                1.0,
//                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                true );

//        m_renderer->renderText( false,
//                                textProg,
//                                scaledWidth(),
//                                scaledHeight(),
//                                to_string_with_precision( yRange.b(), TEXT_PREC ),
//                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
//                                1.0,
//                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                true );
}

void BALEEN::renderHistogramMultiples(
    FrVolume & frVolume, I2 histResolution )
{
    const int NUM_TIME_STEPS = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                               : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    const int BINS_PER_HIST = histResolution.a() * histResolution.b();

    std::vector< Vec2< float > > Hverts;
    std::vector< Vec3< float > > Hcolors;
    std::vector< double > values( BINS_PER_HIST );

    const int HIST_SIZE = m_timeLineWidget.size().x() / NUM_TIME_STEPS;

    bool local = m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local;

    double normalization = local ? 1.0 : m_maxFrequencyInSelectedTS *
                           m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition();

    for( int64_t t = 0; t < NUM_TIME_STEPS; ++t )
    {
        const int STACK_OFFSET = t * BINS_PER_HIST;
        for( int b = 0; b < BINS_PER_HIST; ++b )
        {
            values[ b ] = frVolume.values[ STACK_OFFSET + b ];
        }

        TN::HIST::appendGridHistModelTo2D(
            Hverts,
            Hcolors,
            Vec2< float >(  1,    1 ),
            Vec2< float >(  1, -0.5 ),
            Vec2< float >( -1,    1 ),
            Vec2< float >( -1, -0.5 ),
            histResolution,
            values,
            normalization,
            m_histTF
        );

        glViewport(
            m_timeHistIsoViewport.offsetX() + t*HIST_SIZE,
            m_timeHistIsoViewport.offsetY(),
            HIST_SIZE,
            HIST_SIZE );

        m_renderer->renderPrimitivesColored(
            Hverts,
            Hcolors,
            simpleColorProg,
            QMatrix4x4(),
            GL_TRIANGLES );
    }
}

void BALEEN::renderHistogramStackIso(
    BaseHistDefinition * def,
    FrVolume & frVolume )
{
    I2 histResolution = getAdjustedHistResolution( { (*def).resolution[ 0 ], (*def).resolution[ 1 ] } );

    float isoMax = 0;
    if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local )
    {
        isoMax = ( m_maxFrequencyInSelectedTS ) * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition();
        if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
        {
            double nf = 1;
            if( m_gridMergingNormalizationTerm.find( m_sampler.getSelectedCellId() ) != m_gridMergingNormalizationTerm.end() )
            {
                nf = m_gridMergingNormalizationTerm.find( m_sampler.getSelectedCellId() )->second;
            }
            if( nf != 0 )
            {
                isoMax /= nf;
            }
        }
    }
    else
    {
        isoMax = 1.0;
    }

    std::vector<float> targetValues;

    int nContours = 2;

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        nContours = dynamic_cast< HistogramDefinition * >( def )->weightType == HistogramWeightType::VARIABLE ? 2 : 1;
    }

    if( nContours == 2 )
    {
        targetValues.resize(2);

        targetValues[0] =  isoMax * m_isoValueSlider.sliderPosition();
        targetValues[1] = -isoMax * m_isoValueSlider.sliderPosition();

        m_currentContourLevels.resize( 2 );
        m_currentContourLevels[ 0 ] = targetValues[ 0 ];
        m_currentContourLevels[ 1 ] = targetValues[ 1 ];
    }
    else
    {
        targetValues.resize(1);

        targetValues[0] =  isoMax * m_isoValueSlider.sliderPosition();

        m_currentContourLevels.resize( 1 );
        m_currentContourLevels[ 0 ] = targetValues[ 0 ];
    }

    //

    const size_t TF_SIZE = m_histTF.size();
    const size_t TF_SIZE_M1 = TF_SIZE - 1;

    double r1 = 0.5 + m_isoValueSlider.sliderPosition() * 0.5;
    double r2 = 0.5 - m_isoValueSlider.sliderPosition() * 0.5;

    Vec3< float > isoColor0 = m_histTF[ std::min( ( size_t ) std::floor( r1 * TF_SIZE ),  TF_SIZE_M1 ) ];
    Vec3< float > isoColor1 = m_histTF[ std::min( ( size_t ) std::floor( r2 * TF_SIZE ),  TF_SIZE_M1 ) ];

    m_isoMaterial.opacity   = 1.0;
    m_isoMaterial.Kd_scale  = 0.9f;
    m_isoMaterial.Ks_scale  = 0.4f;
    m_isoMaterial.Ka_scale  = 0.2f;
    m_isoMaterial.shininess = 5.0;

    std::vector< Vec2< float > > contours[ nContours ];
    std::vector< float > columnCoords;
    std::vector< float > rowCoords;

    const int NUM_TIME_STEPS = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                               : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    const int offSt = getStepOffset();
    float * selectedHistValues = frVolume.atStep( offSt );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES  )
    {
        float dS = 2.0 / histResolution.a();
        float ofs = -1 + 1.0 / histResolution.a();

        for( int row = 0; row < histResolution.a(); ++row )
        {
            rowCoords.push_back( ofs + dS*row );
        }

        dS = 2.0 / histResolution.b();
        ofs = -1 + 1.0 / histResolution.b();
        for( int col = 0; col < histResolution.b(); ++col )
        {
            columnCoords.push_back( ofs + dS*col );
        }

        if( nContours == 2 )
        {
            #pragma omp parallel
            {
                if( omp_get_thread_num() == 0 )
                {
                    ( *m_marchCubes[ 0 ] )(
                        frVolume.values,
                        NUM_TIME_STEPS,
                        histResolution.b(),
                        histResolution.a(),
                        targetValues[ 0 ] );

                    conrec2(
                        selectedHistValues,
                        histResolution.b(),
                        histResolution.a(),
                        columnCoords,
                        rowCoords,
                    { targetValues[ 0 ] },
                    contours[ 0 ] );
                }
                else if ( 0, omp_get_thread_num() == 1 )
                {
                    ( *m_marchCubes[ 1 ] )(
                        frVolume.values,
                        NUM_TIME_STEPS,
                        histResolution.b(),
                        histResolution.a(),
                        targetValues[ 1 ] );

                    conrec2(
                        selectedHistValues,
                        histResolution.b(),
                        histResolution.a(),
                        columnCoords,
                        rowCoords,
                    { targetValues[ 1 ] },
                    contours[ 1 ] );
                }
            }
        }
        else
        {
            ( *m_marchCubes[ 0 ] )(
                frVolume.values,
                NUM_TIME_STEPS,
                histResolution.b(),
                histResolution.a(),
                targetValues[ 0 ] );

            conrec2(
                selectedHistValues,
                histResolution.b(),
                histResolution.a(),
                columnCoords,
                rowCoords,
            { targetValues[ 0 ] },
            contours[ 0 ] );
        }
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        float dS = 1.0 / ( histResolution.b() - .5 );
        float ofs = 0;

        for( int row = 0; row < histResolution.b(); ++row )
        {
            rowCoords.push_back( ofs + dS*row );
        }

        dS  =  2.0 / histResolution.a();
        ofs = -1 + 1.0 / histResolution.a();
        for( int col = 0; col < histResolution.a(); ++col )
        {
            columnCoords.push_back( ofs + dS*col );
        }

        #pragma omp parallel
        {
            if( omp_get_thread_num() == 0 )
            {
                ( *m_marchCubes[ 0 ] )(
                    frVolume.values,
                    NUM_TIME_STEPS,
                    histResolution.b(),
                    histResolution.a(),
                    targetValues[ 0 ] );

                conrec2(
                    selectedHistValues,
                    histResolution.b(),
                    histResolution.a(),
//                    columnCoords,
//                    rowCoords,
                    rowCoords,
                    columnCoords,
                { targetValues[ 0 ] },
                contours[ 0 ] );
            }
            else if ( omp_get_thread_num() == 1 )
            {
                ( *m_marchCubes[ 1 ] )(
                    frVolume.values,
                    NUM_TIME_STEPS,
                    histResolution.b(),
                    histResolution.a(),
                    targetValues[ 1 ] );

                conrec2(
                    selectedHistValues,
                    histResolution.b(),
                    histResolution.a(),
                    //columnCoords,
                    //rowCoords,
                    rowCoords,
                    columnCoords,
                { targetValues[ 1 ] },
                contours[ 1 ] );
            }
        }
    }

    renderSelectedHistContour( contours[ 0 ], QVector4D(  color3.r(),  color3.g(),  color3.b(), 1.0 ) );

    if( nContours == 2 )
    {
        renderSelectedHistContour( contours[ 1 ], QVector4D( colorn3.r(), colorn3.g(), colorn3.b(), 1.0 ) );
    }

    std::vector< Vec3< float > > boundingBox;
    std::vector< Vec3< float > > timeStepIndictor;
    std::vector< Vec3< float > > negXAxis;
    std::vector< Vec3< float > > negYAxis;
    std::vector< Vec3< float > > xAxis;
    std::vector< Vec3< float > > yAxis;

    //double X = histResolution.a();
    //double Y = histResolution.b();

    //if( m_populationDataType == "distribution" )
    //{
    double X = histResolution.b();
    double Y = histResolution.a();
    //}

    // near square
    boundingBox.push_back( Vec3< float >( 0, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, X, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, 0, Y ) );
    boundingBox.push_back( Vec3< float >( 0, X, Y ) );
    boundingBox.push_back( Vec3< float >( 0, X, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, X, Y ) );
    boundingBox.push_back( Vec3< float >( 0, 0, Y ) );

    // far square
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, Y ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, Y ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, Y ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, Y ) );

    // time depth lines
    boundingBox.push_back( Vec3< float >( 0, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, X, 0 ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, 0 ) );
    boundingBox.push_back( Vec3< float >( 0, X, Y ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, X, Y ) );
    boundingBox.push_back( Vec3< float >( 0, 0, Y ) );
    boundingBox.push_back( Vec3< float >( NUM_TIME_STEPS, 0, Y ) );


    // current time step square
    timeStepIndictor.push_back( Vec3< float >( offSt, 0, 0 ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, X, 0 ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, 0, 0 ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, 0, Y ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, X, Y ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, X, 0 ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, X, Y ) );
    timeStepIndictor.push_back( Vec3< float >( offSt, 0, Y ) );


    yAxis.push_back( Vec3< float >( offSt, X/2.0, Y/2.0 ) );
    yAxis.push_back( Vec3< float >( offSt, X, Y/2.0 ) );

    negYAxis.push_back( Vec3< float >( offSt, X/2.0, Y/2.0 ) );
    negYAxis.push_back( Vec3< float >( offSt, 0, Y/2.0 ) );

    negXAxis.push_back( Vec3< float >( offSt, X/2.0, Y/2.0 ) );
    negXAxis.push_back( Vec3< float >( offSt, X/2.0, Y ) );

    xAxis.push_back( Vec3< float >( offSt, X/2.0, Y/2.0 ) );
    xAxis.push_back( Vec3< float >( offSt, X/2.0, 0 ) );

    std::vector< Vec3< float > > orientationGlyph1( 3 );

    orientationGlyph1[ 0 ] = Vec3< float >( offSt, X*.85,   Y - Y*1.0 );
    orientationGlyph1[ 1 ] = Vec3< float >( offSt, X*1.0,   Y - Y*1.0 );
    orientationGlyph1[ 2 ] = Vec3< float >( offSt, X*1.0,   Y - Y*.85 );

    std::vector< Vec3< float > > orientationGlyph2( 3 );
    orientationGlyph2[ 0 ] = Vec3< float >( offSt - NUM_TIME_STEPS*.001, .93*X, Y - .97*Y );
    orientationGlyph2[ 1 ] = Vec3< float >( offSt - NUM_TIME_STEPS*.001, .96*X, Y - .97*Y );
    orientationGlyph2[ 2 ] = Vec3< float >( offSt - NUM_TIME_STEPS*.001, .97*X, Y - .93*Y );

    QMatrix4x4 M;
    M.setToIdentity();

    double R = std::max( histResolution.a(), histResolution.b() );

//    M.translate( ( ( 0.96 / (float)NUM_TIME_STEPS) *NUM_TIME_STEPS ) / 2.0, 0, 0 );
    M.rotate(  m_histogramStackRotationY, QVector3D( 0.0, 1.0, 0.0 ) );
    M.rotate(  m_histogramStackRotation, QVector3D( 1.0, 0.0, 0.0 ) );
    M.scale( ( 1.08 * .25 * m_timeHistIsoViewport.width() / ( double ) m_timeHistIsoViewport.height() ) / ( (float)NUM_TIME_STEPS  ),
             -( .25 * .75 ) / R,
             ( .25 * .75 ) / R );

    M.translate( -NUM_TIME_STEPS/2.0, -X / 2.0, -Y /2.0   );

    QMatrix4x4 VolumeModel;

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        VolumeModel.setToIdentity();
        VolumeModel.rotate(  m_histogramStackRotationY, QVector3D( 0.0, 1.0, 0.0 ) );
        VolumeModel.rotate(  m_histogramStackRotation, QVector3D( 1.0, 0.0, 0.0 ) );
        VolumeModel.scale( ( 1.08 * .25 * m_timeHistIsoViewport.width() / ( double ) m_timeHistIsoViewport.height() ) / ( (float)NUM_TIME_STEPS-1 ),
                           .75 * -0.25 / R,
                           .75 *  0.25 / R );

        //  VolumeModel.scale( 1.1 / (float)NUM_TIME_STEPS, -( 2.0 * 23 ) / ( (float)380.0 * X *2  ), ( 2.0 * 23 ) / ( (float)380.0 * Y )  );
        VolumeModel.translate( -(NUM_TIME_STEPS-1)/2.0, -X / 2, -Y /2.0   );
    }
    else
    {
        VolumeModel = M;
    }

    isoCam.setCenter( QVector3D( 0, 0, 0 ) );
    isoCam.setAspectRatio( m_timeHistIsoViewport.width() / ( double ) m_timeHistIsoViewport.height() );
    isoCam.reset( QVector3D( -1, -1, -1 ), QVector3D( 1, 1, 1 )  );

    QMatrix4x4 P = isoCam.matProj();
    QMatrix4x4 V = isoCam.matView( 0.07 );

    glViewport( m_timeHistIsoViewport.offsetX(), m_timeHistIsoViewport.offsetY(), m_timeHistIsoViewport.width(), m_timeHistIsoViewport.height() );
    glEnable( GL_DEPTH_TEST );

    m_renderer->renderTriangleMesh(
        m_marchCubes[ 0 ]->getVerts(),
        m_marchCubes[ 0 ]->getNormals(),
        QVector4D( isoColor0.r(), isoColor0.g(), isoColor0.b(), 1.0 ),
        VolumeModel,
        V,
        P,
        flatMeshProg );


    if( nContours == 2 )
    {
        m_renderer->renderTriangleMesh(
            m_marchCubes[ 1 ]->getVerts(),
            m_marchCubes[ 1 ]->getNormals(),
            QVector4D( isoColor1.r(), isoColor1.g(), isoColor1.b(), 1.0 ),
            VolumeModel,
            V,
            P,
            flatMeshProg );
    }

    glLineWidth( 1.0 );

    m_renderer->renderPrimitivesFlat(
        boundingBox,
        flatProg,
        QVector4D( 0, 0, 0, 0.4 ),
        P*V*M,
        GL_LINES );

    glLineWidth( 2.0 );
    m_renderer->renderPrimitivesFlat(
        timeStepIndictor,
        flatProg,
        m_boxSelectionColor,
        P*V*M,
        GL_LINES );

    //

//    glLineWidth( 2.0 );
//    m_renderer->renderPrimitivesFlat(
//        xAxis,
//        flatProg,
//        QVector4D( 0, 1.0, 0.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        negXAxis,
//        flatProg,
//        QVector4D( 0, 0, 1.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        yAxis,
//        flatProg,
//        QVector4D( 1, 0, 0, 1.0 ),
//        P*V*M,
//        GL_LINES );

//    m_renderer->renderPrimitivesFlat(
//        negYAxis,
//        flatProg,
//        QVector4D( .5, 0.0, 1.0, 1.0 ),
//        P*V*M,
//        GL_LINES );

    ////////////////////////////////////


    glEnable( GL_DEPTH_TEST );
    m_renderer->renderPrimitivesFlat(
        orientationGlyph1,
        flatProg,
        QVector4D( 0, 0, 0, 1.0 ),
        P*V*M,
        GL_TRIANGLES );

    m_renderer->renderPrimitivesFlat(
        orientationGlyph2,
        flatProg,
        QVector4D( .4, .9, .6, 1.0 ),
        P*V*M,
        GL_TRIANGLES );
    glDisable( GL_DEPTH_TEST );

    glViewport( 0.0, 0.0, scaledWidth(), scaledHeight() );
}

void BALEEN::renderHistogramStack1D()
{

//    constructTimeHistogramStack1D();

//    static std::vector< Vec3< float > >         histogramStack1DVerts;
//    static std::vector< Vec3< float > >         histogramStack1DNormals;
//    static std::vector< Vec3< float > >         histogramStackColors;

//    static int lastNumTimeSteps = 0;
//    static int lastResolution   = 0;

//    if( lastNumTimeSteps != numTimeSteps || lastResolution != resolution.a() )
//    {

//        const int NUM_VERTS = ( (resolution.a()-1)*18+12 )*numTimeSteps;

//        histogramStack1DVerts.resize(   NUM_VERTS );
//        histogramStack1DNormals.resize( NUM_VERTS );
//        histogramStackColors.resize(    NUM_VERTS );
//    }

//    lastResolution   = resolution.a();
//    lastNumTimeSteps = numTimeSteps;

//    float scale = 1.0 / m_timeHistIsoViewport.width();

//    float w   = m_timeHistIsoViewport.width() * scale * 2; // need to change later
//    float deltaX  = w / (numTimeSteps);

//    float h = m_timeHistIsoViewport.height() * scale;//m_timeHistIsoViewport.height();

//    float depth   = m_timeHistIsoViewport.height()*3 * scale; // need to change later
//    float deltaZ = depth / (resolution.a()-1);

//    Vec3< float > start( -w/2.0, 0.0, -depth/2.0 );
//    float topV = maxFrequency;
//    if( autoMaxFreq )
//    {
//        topV = 0.0;
//        for( auto bc : histogramStackFrequencies )
//        {
//            topV = std::max( topV, bc );
//        }
//    }

//    const int STRIDE = resolution.a()-1;

//    #pragma omp parallel for
//    for( int i = 0; i < numTimeSteps; ++i )
//    {

//        for( int b = 0; b < STRIDE; ++b )
//        {

//            const int OFFSET = i * ( 18*STRIDE+12 ) + b*(18);

//            float r1 = std::min( histogramStackFrequencies[ i*resolution.a() +   b     ] / ( float ) topV, 1.0 );
//            float r2 = std::min( histogramStackFrequencies[ i*resolution.a() + ( b+1 ) ] / ( float ) topV, 1.0 );

//            Vec3< float > c1 = i == timeStep ? Vec3< float >( 0.0, .3f, 0.6f ) : getColorInterp( r1, color1, color2, color3, color4, color5  );
//            Vec3< float > c2 = i == timeStep ? Vec3< float >( 0.0, .3f, 0.6f ) : getColorInterp( r2, color1, color2, color3, color4, color5  );

//            // T1 //////////////////////////////////////////////////////

//            histogramStack1DVerts[ OFFSET ] =
//                start + Vec3< float >( deltaX*i, 0.0, deltaZ*b );

//            histogramStackColors[ OFFSET ] = c1;

//            ////////////////////////////////////////////////////////////

//            histogramStack1DVerts[ OFFSET+1 ] =
//                start + Vec3< float >( deltaX*i, 0.0, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+1 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+2 ] =
//                start + Vec3< float >( deltaX*i, r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+2 ] = c1;

//            Vec3< float > nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET ], histogramStack1DVerts[ OFFSET + 1 ], histogramStack1DVerts[ OFFSET + 2 ] );
//            for( int k = 0; k < 3; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            // T2 ///////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+3 ] =
//                start + Vec3< float >( deltaX*i, r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+3 ] = c1;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+4 ] =
//                start + Vec3< float >( deltaX*i, 0.0, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+4 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+5 ] =
//                start + Vec3< float >( deltaX*i, r2*h, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+5 ] = c2;

//            nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET + 3 ], histogramStack1DVerts[ OFFSET + 4 ], histogramStack1DVerts[ OFFSET + 5 ] );
//            for( int k = 3; k < 6; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            // T3 ///////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+6 ] =
//                start + Vec3< float >( deltaX*i, r2*h, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+6 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+7 ] =
//                start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+7 ] = c1;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+8 ] =
//                start + Vec3< float >( deltaX*i, r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+8 ] = c1;

//            nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET + 6 ], histogramStack1DVerts[ OFFSET + 7 ], histogramStack1DVerts[ OFFSET + 8 ] );
//            for( int k = 6; k < 9; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            // T4 ///////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+9 ] =
//                start + Vec3< float >( deltaX*i, r2*h, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+9 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+10 ] =
//                start + Vec3< float >( deltaX*(i+1), r2*h, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+10 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+11 ] =
//                start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+11 ] = c1;

//            nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET + 9 ], histogramStack1DVerts[ OFFSET + 10 ], histogramStack1DVerts[ OFFSET + 11 ] );
//            for( int k = 9; k < 12; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            // T5 ///////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+12 ] =
//                start + Vec3< float >( deltaX*(i+1), 0.0, deltaZ*b );

//            histogramStackColors[  OFFSET+12 ] = c1;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+13 ] =
//                start + Vec3< float >( deltaX*(i+1), 0.0, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+13 ] = c2;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+14 ] =
//                start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+14 ] = c1;

//            nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET + 12 ], histogramStack1DVerts[ OFFSET + 13 ], histogramStack1DVerts[ OFFSET + 14 ] );
//            for( int k = 12; k < 15; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            // T6 ///////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+15 ] =
//                start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//            histogramStackColors[  OFFSET+15 ] = c1;

//            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+16 ] =
//                start + Vec3< float >( deltaX*(i+1), 0.0, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+16 ] = c2;

////            /////////////////////////////////////////////////////////////

//            histogramStack1DVerts[  OFFSET+17 ] =
//                start + Vec3< float >( deltaX*(i+1), r2*h, deltaZ*(b+1) );

//            histogramStackColors[  OFFSET+17 ] = c2;

//            nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[ OFFSET+15 ], histogramStack1DVerts[ OFFSET + 16 ], histogramStack1DVerts[ OFFSET + 17 ] );
//            for( int k = 15; k < 18; ++k )
//            {
//                histogramStack1DNormals[ OFFSET + k ] = nm;
//            }

//            if( b == 0 )
//            {

//                //Back Ends //////////////////////////////////////////////////////
//                ////////////////////////////////////////////////////////////
//                ///////////////////////////////////////////////////////////

//                //T7

//                histogramStack1DVerts[  OFFSET + STRIDE*18 ] =
//                    start + Vec3< float >( deltaX*i, 0.0, deltaZ*b );

//                histogramStackColors[  OFFSET + STRIDE*18 ] = c1;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[  OFFSET + STRIDE*18 + 1  ] =
//                    start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//                histogramStackColors[   OFFSET + STRIDE*18 + 1 ] = c1;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[   OFFSET + STRIDE*18 + 2 ] =
//                    start + Vec3< float >( deltaX*i, r1*h, deltaZ*b );

//                histogramStackColors[   OFFSET + STRIDE*18 + 2 ] = c1;

//                nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[OFFSET + STRIDE*18 ], histogramStack1DVerts[ OFFSET + STRIDE*18 + 1 ], histogramStack1DVerts[ OFFSET + STRIDE*18 + 2 ] );
//                for( int k = 0; k < 3; ++k )
//                {
//                    histogramStack1DNormals[ OFFSET + STRIDE*18 + k ] = nm;
//                }

//                // T8

//                histogramStack1DVerts[  OFFSET + STRIDE*18 + 3 ] =
//                    start + Vec3< float >( deltaX*i, 0.0, deltaZ*b );

//                histogramStackColors[   OFFSET + STRIDE*18 + 3 ] = c1;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[   OFFSET + STRIDE*18 + 4 ] =
//                    start + Vec3< float >( deltaX*(i+1), 0.0, deltaZ*b );

//                histogramStackColors[   OFFSET + STRIDE*18 + 4 ] = c1;


//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[   OFFSET + STRIDE*18 + 5 ] =
//                    start + Vec3< float >( deltaX*(i+1), r1*h, deltaZ*b );

//                histogramStackColors[   OFFSET + STRIDE*18 + 5 ] = c1;

//                nm = Vec3< float >::surfaceNormal( histogramStack1DVerts[OFFSET + STRIDE*18 + 3 ], histogramStack1DVerts[ OFFSET + STRIDE*18 + 4 ], histogramStack1DVerts[ OFFSET + STRIDE*18 + 5 ] );
//                for( int k = 3; k < 6; ++k )
//                {
//                    histogramStack1DNormals[ OFFSET + STRIDE*18 + k ] = nm;
//                }
//            }

//            else if( b == ( STRIDE - 1 ) )
//            {

//                // Front ends

//                // T9

//                histogramStack1DVerts[  OFFSET+24 ] =
//                    start + Vec3< float >( deltaX*i, 0.0, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+24 ] = c2;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[  OFFSET+25 ] =
//                    start + Vec3< float >( deltaX*(i+1), r2*h, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+25 ] = c2;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[  OFFSET+26 ] =
//                    start + Vec3< float >( deltaX*i, r2*h, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+26 ] = c2;

//                // T10

//                histogramStack1DVerts[  OFFSET+27 ] =
//                    start + Vec3< float >( deltaX*i, 0.0, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+27 ] = c2;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[  OFFSET+28 ] =
//                    start + Vec3< float >( deltaX*(i+1), 0.0, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+28 ] = c2;

//                /////////////////////////////////////////////////////////////

//                histogramStack1DVerts[  OFFSET+29 ] =
//                    start + Vec3< float >( deltaX*(i+1), r2*h, deltaZ*(b+1) );

//                histogramStackColors[  OFFSET+29 ] = c2;
//            }
//        }
//    }

//    glViewport( 0, 0, scaledWidth(), scaledHeight() );

//    QMatrix4x4 M;
//    M.setToIdentity();
//    // M.translate(  w/2.0, 0.0, 0.0 );
//    M.rotate( histogramStackRotationY, QVector3D( 0, 1, 0 ) );
//    M.translate( 0.0, 0.35, 0.0 );

//    QMatrix4x4 P(
//        1.03569,         0,         0,         0,
//        0,   1.98403,         0,         0,
//        0,         0,  -1.00007, -0.200007,
//        0,         0,        -1,         0
//    );

//    QMatrix4x4 V(
//        0.370952,-0.0294033,  0.928186, 0.0827702,
//        -0.0869608,  0.994007, 0.0662425, -0.824322,
//        -0.924571, -0.105289,  0.366172,  -2.13854,
//        0,         0,         0,         1
//    );

//    glEnable( GL_DEPTH_TEST );
//    glDisable( GL_CULL_FACE );

//    m_renderer->renderTriangleMesh(
//        histogramStack1DVerts,
//        histogramStack1DNormals,
//        histogramStackColors,
//        material,
//        light,
//        M,
//        V,
//        P,
//        prog3D,
//        false
//    );

//    glDisable( GL_DEPTH_TEST );
//    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}


void BALEEN::wheelEvent(QWheelEvent *e)
{
    double numDegrees = e->angleDelta().y() / -9000000.0;
    Vec2< float > pos( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() );

    if( m_timeHistIsoViewport.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )
            || m_histogramViewPort.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) ) )
    {

        isoCam.zoom( -e->angleDelta().y() );
        renderLater();
    }

    if( m_samplerWidget.visualizationWindow.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )  )
    {
        camera.wheel(numDegrees);
        invalidateSampling();
    }
    else if ( m_auxViewPort.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )  )
    {
        auxViewCamera.zoom( -e->angleDelta().y() * 10 );
    }

    if( m_allWeightHistWidget.pointInViewPort( pos ) )
    {
        m_allWeightHistWidget.mouseWheel( numDegrees );
    }
    if( m_regionWeightHistWidget.pointInViewPort( pos ) )
    {
        m_regionWeightHistWidget.mouseWheel( numDegrees );
    }
    if( m_binWeightHistWidget.pointInViewPort( pos ) )
    {
        m_binWeightHistWidget.mouseWheel( numDegrees );
    }

    if( m_allWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_allWeightHistWidget.rangeSelector.mouseWheel( numDegrees );
    }
    if( m_regionWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_regionWeightHistWidget.rangeSelector.mouseWheel( numDegrees );
    }
    if( m_binWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_binWeightHistWidget.rangeSelector.mouseWheel( numDegrees );
    }

    QVector3D c1;
    QVector3D c2;
    QVector3D c3;
    QVector3D c4;

    c1.setX( -1.0 );
    c1.setY(  1.0 );
    c1.setZ(  0.0 );

    c2.setX( 1.0 );
    c2.setY( 1.0 );
    c2.setZ( 0.0 );

    c3.setX(  1.0 );
    c3.setY( -1.0 );
    c3.setZ(  0.0 );

    c4.setX( -1.0 );
    c4.setY( -1.0 );
    c4.setZ(  0.0 );

    c1 = ( camera.proj()* camera.view() ).inverted() * c1;
    c2 = ( camera.proj()* camera.view() ).inverted() * c2;
    c3 = ( camera.proj()* camera.view() ).inverted() * c3;
    c4 = ( camera.proj()* camera.view() ).inverted() * c4;

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    renderLater();
}

void BALEEN::mouseDoubleClickEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos();

    QVector2D mouseDir =
        QVector2D((mouseCurr - m_previousMousePosition )
                  / (scaledHeight()));

    mouseDir.setY(-mouseDir.y());

    Vec2< float > pos1( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y()  );
    m_previousMousePosition = mouseCurr;

    // Weight Breakdown Widgets

    if( m_binWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_binWeightHistWidget.dblck( pos1 );
        m_regionWeightHistWidget.clearSelection();
        m_allWeightHistWidget.clearSelection();
    }
    else if ( m_regionWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_regionWeightHistWidget.dblck( pos1 );
        m_binWeightHistWidget.clearSelection();
        m_allWeightHistWidget.clearSelection();
    }
    else if ( m_allWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_allWeightHistWidget.dblck( pos1 );
        m_binWeightHistWidget.clearSelection();
        m_regionWeightHistWidget.clearSelection();
    }
    else if ( m_auxViewPort.pointInViewPort( pos1 ) )
    {
        m_allWeightHistWidget.clearSelection();
        m_binWeightHistWidget.clearSelection();
        m_regionWeightHistWidget.clearSelection();
    }

    QVector3D pos;

    float w = scaledWidth();
    float h = scaledHeight();

    pos.setX( (     mouseCurr.x() - w / 2 )   / ( w / 2 ) );
    pos.setY( ( - ( mouseCurr.y() - h / 2 ) ) / ( h / 2 ) );
    pos.setZ( 0 );

    pos = ( camera.proj() * camera.view() ).inverted() * pos;

    if( m_sampler.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) ) )
    {
        // last parameter specifies 1d or 2d
        m_sampler.dblClick( Vec3< float>( pos.x(), pos.y(), 0.0 ), 2, "cell" );

        if( e->buttons() & Qt::RightButton )
        {
            m_sampler.dblRightClick( Vec3< float>( pos.x(), pos.y(), 0.0 ) );
        }
    }

    if( m_histogramViewPort.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) ) )
    {
        // get relative position in rendered histogram,
        // convert that position into the same position in the sampling grid,
        // then call the mouse double click function with that position on the sampler

        ViewPort vp;
        vp.setSize( m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ), m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ) );
        vp.setPosition(
            m_histogramViewPort.offsetX() + HIST_PAD,
            m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - PANEL_HT );

        if( vp.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) ) )
        {
            int cellId = m_sampler.getSelectedCellId();

            if( cellId >= 0 )
            {
                float px = ( mouseCurr.x() - vp.offsetX() ) / ( ( vp.offsetX() + vp.width() ) - vp.offsetX() );
                float py = ( ( scaledHeight() - mouseCurr.y() ) - vp.offsetY() ) / ( ( vp.offsetY() + vp.height() ) - vp.offsetY() );

                const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();

                Vec2< float > a = gridCorners[ cellId*4 + 0 ];
                Vec2< float > b = gridCorners[ cellId*4 + 1 ];
                Vec2< float > c = gridCorners[ cellId*4 + 2 ];
//                Vec2< float > d = gridCorners[ cellId*4 + 3 ];

                float newX = c.x() + ( a.x() - c.x() ) * px;
                float newY = b.y() + ( a.y() - b.y() ) * py;

                m_sampler.dblClick( Vec3< float>( newX, newY, 0.f ), 2, "bin" );
            }
        }
        else
        {
            m_sampler.deselectBin();
        }
    }
}

void BALEEN::writeVelocity()
{
    const std::vector< float > & r     = *m_dataSetManager->particleDataManager().values( 101, "ions", "r"     );
    const std::vector< float > & z     = *m_dataSetManager->particleDataManager().values( 101, "ions", "z"     );

    std::ofstream outFile( "r1.txt" );
    for( int i = 0; i < 200000; ++i )
    {
        outFile << r[ i ] << " ";
    }
    outFile.close();

    outFile.open( "z1.txt" );
    for( int i = 0; i < 200000; ++i )
    {
        outFile << z[ i ] << " ";
    }
    outFile.close();
}

void BALEEN::mousePressEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos() * devicePixelRatio();
    Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );


    m_mouseIsPressed = true;

    /****************************** Combos ********************************/

    bool comboPressed = false;

    // These will be refactored out later as all ui elements will be collected into ui panels
    for( auto comboPtr : m_comboRefs )
    {
        if( (*comboPtr).pointInViewPort( pos ) )
        {
            if( ! (*comboPtr).isPressed() )
            {
                (*comboPtr).setPressed( true );
            }
            else
            {
                (*comboPtr).pressed( pos );
                (*comboPtr).setPressed( false );
            }
            comboPressed = true;
        }
        else
        {
            (*comboPtr).setPressed( false );
        }
    }

    if( comboPressed )
    {
        renderLater();
        return;
    }

    // the sampler panel combos

    for( auto & panel : m_samplerWidget.controlPanel.visiblePanels() )
    {
        for( TN::ComboWidget * combo : panel->combos() )
        {
            if( (*combo).pointInViewPort( pos ) )
            {
                if( ! (*combo).isPressed() )
                {
                    (*combo).setPressed( true );
                }
                else
                {
                    (*combo).pressed( pos );
                    (*combo).setPressed( false );
                }
                comboPressed = true;
            }
            else
            {
                (*combo).setPressed( false );
            }

            if( comboPressed )
            {
                renderLater();
                return;
            }
        }
    }

    /****************************************************************************/

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if( m_timeLineWidget.pointInViewPort( pos + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
            && m_timeLineWidget.pointInViewPort( pos + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) )
            && m_sessionInitiated )
    {
        int offX = pos.x() - ( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT );

        TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                    : m_dataSetManager->distributionManager().currentTimeSeries();
        double relative = ( offX / ( double ) m_timeLineWidget.plotSize().x() );
        int intTStep = ( std::max( std::min( static_cast< int32_t >( std::round( tsrs.firstIdx + relative * ( tsrs.lastIdx - tsrs.firstIdx ) ) ), tsrs.lastIdx ), tsrs.firstIdx ) );
        int diffStride = ( intTStep - tsrs.firstIdx ) % tsrs.idxStride;
        if( diffStride != 0 )
        {
            if( diffStride > tsrs.idxStride / 2.0 )
            {
                intTStep += tsrs.idxStride - diffStride;
            }
            else
            {
                intTStep -= diffStride;
            }
        }

        m_selectedTimeStep = intTStep;
        m_timeLineWidget.setTimeStep( m_selectedTimeStep );

        ////////////////////////////qDebug() << "set time step to " << m_selectedTimeStep;

        renderLater();
    }

    if( m_allWeightHistWidget.pointInViewPort( pos ) )
    {
        m_allWeightHistWidget.setDragInit( pos );
    }
    if( m_regionWeightHistWidget.pointInViewPort( pos ) )
    {
        m_regionWeightHistWidget.setDragInit( pos );
    }
    if( m_binWeightHistWidget.pointInViewPort( pos ) )
    {
        m_binWeightHistWidget.setDragInit( pos );
    }

    if( m_allWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_allWeightHistWidget.rangeSelector.mouseDrag( pos );
    }
    if( m_regionWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_regionWeightHistWidget.rangeSelector.mouseDrag( pos );
    }
    if( m_binWeightHistWidget.rangeSelector.pointInViewPort( pos ) )
    {
        m_binWeightHistWidget.rangeSelector.mouseDrag( pos );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // resize widgets

    if( m_resizeWidgetA.pointInViewPort( pos ) )
    {
        m_resizeWidgetA.setPressed( true );
    }
    else if( m_resizeWidgetB.pointInViewPort( pos ) )
    {
        m_resizeWidgetB.setPressed( true );
    }
    else if( m_resizeWidgetC.pointInViewPort( pos ) )
    {
        m_resizeWidgetC.setPressed( true );
    }
    else if( m_resizeWidgetD.pointInViewPort( pos ) )
    {
        m_resizeWidgetD.setPressed( true );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Buttons

    if( m_timePlotBothSelector.pointInViewPort( pos ) )
    {
        m_timePlotBothSelector.setPressed( true );
        m_timePlotHistogramSelector.setPressed( false );
        m_timePlotAllSelector.setPressed( false );
    }
    else if( m_timePlotHistogramSelector.pointInViewPort( pos ) )
    {
        m_timePlotHistogramSelector.setPressed( true );
        m_timePlotBothSelector.setPressed( false );
        m_timePlotAllSelector.setPressed( false );
    }
    else if( m_timePlotAllSelector.pointInViewPort( pos ) )
    {
        m_timePlotAllSelector.setPressed( true );
        m_timePlotBothSelector.setPressed( false );
        m_timePlotHistogramSelector.setPressed( false );
    }

    if( m_saveButton.pointInViewPort( pos ) )
    {
        m_saveButton.setPressed( true );
    }

    if( m_loadProjectFileButton.pointInViewPort( pos ) )
    {
        m_loadProjectFileButton.setPressed( true );
    }

    if( m_inspectWarningsButton.pointInViewPort( pos ) )
    {
        m_inspectWarningsButton.setPressed( true );
    }
    if( m_saveProjectButton.pointInViewPort( pos ) )
    {
        m_saveProjectButton.setPressed( true );
    }

    if( m_initiateSessionButton.pointInViewPort( pos ) )
    {
        m_initiateSessionButton.setPressed( true );
    }

    /****************/

    for( auto & button : m_samplerWidget.visibleButtons() )
    {
        if( button->pointInViewPort( pos ) )
        {
            if( button->mode() == TN::PressButton::Mode::Toggle )
            {
                button->setPressed( ! button->isPressed() );
            }
            else
            {
                button->setPressed( true );
            }
        }
    }

    /***********************/

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        if( m_negPosScalarButton.pointInViewPort( pos ) )
        {
            m_negPosScalarButton.setPressed( ! m_negPosScalarButton.isPressed() );
        }
    }

    if( m_definePlotButton.pointInViewPort( pos ) )
    {
        m_definePlotButton.setPressed( true );
    }

    if( m_defineTimeSeriesButton.pointInViewPort( pos ) )
    {
        m_defineTimeSeriesButton.setPressed( true );
    }

    if( m_definePhasePlotButton.pointInViewPort( pos ) )
    {
        m_definePhasePlotButton.setPressed( true );
    }
    if( m_phasePlotAllSelector.pointInViewPort( pos ) )
    {
        m_phasePlotSelectionSelector.setPressed( false );
        m_phasePlotAllSelector.setPressed( true );
    }
    if( m_phasePlotSelectionSelector.pointInViewPort( pos ) )
    {
        m_phasePlotSelectionSelector.setPressed( true );
        m_phasePlotAllSelector.setPressed( false );
    }

    if( m_editPhasePlotButton.pointInViewPort( pos ) )
    {
        m_editPhasePlotButton.setPressed( true );
    }

    if( m_editTimePlotButton.pointInViewPort( pos ) )
    {
        m_editTimePlotButton.setPressed( true );
    }

    if( m_editTimeSeriesButton.pointInViewPort( pos ) )
    {
        m_editTimeSeriesButton.setPressed( true );
    }

    if( m_defineFilterButton.pointInViewPort( pos ) )
    {
        m_defineFilterButton.setPressed( true );
    }

    if( m_editFilterButton.pointInViewPort( pos ) )
    {
        m_editFilterButton.setPressed( true );
    }
    if( m_selectedHist2DButton.pointInViewPort( pos ) )
    {
        m_selectedHist2DButton.setPressed( true );
        m_selectedHist3DButton.setPressed( false );
    }
    if( m_selectedHist3DButton.pointInViewPort( pos ) )
    {
        m_selectedHist3DButton.setPressed( true );
        m_selectedHist2DButton.setPressed( false );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Sliders

    for( auto & slider : m_samplerWidget.visibleSliders() )
    {
        if( slider->pointInViewPort( pos ) )
        {
            slider->setPressed( true );
        }
    }

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        if( m_smoothRadiusSlider.pointInViewPort( pos ) )
        {
            m_smoothRadiusSlider.setPressed( true );
        }
        if( m_gridColorScaleSlider.pointInViewPort( pos  ) )
        {
            m_gridColorScaleSlider.setPressed( true );
        }
    }

    if( m_isoValueSlider.pointInViewPort( pos ) )
    {
        m_isoValueSlider.setPressed( true );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // help label buttons

    for( HelpLabelWidget * hl : m_helpLabelWidgetRefs )
    {
        if( hl->pointInViewPort( pos ) )
        {
//            hl->setPressed( true );
        }
    }

    ////////////////////////////////////???????/////////////////////////////////////////////////////////////////////////

    renderLater();
}

void BALEEN::cancelTimeSeriesDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_timeSeriesDefinitionDialogues.find( ptype ) == m_timeSeriesDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_timeSeriesDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_timeSeriesDialogOpen = false;
}

void BALEEN::cancelROIDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_roiDialogs.find( ptype ) == m_roiDialogs.end() )
    {
        return;
    }

    auto & currentDialogsForCurrentParticle = m_roiDialogs.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_roiDialogOpen = false;

    qDebug() << "cancelled roi dialog";
}

void BALEEN::cancelTimePlotDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_timePlotDefinitionDialogues.find( ptype ) == m_timePlotDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_timePlotDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_timePlotDialogOpen = false;
}


void BALEEN::cancelHistogramDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_histogramDefinitionDialogues.find( ptype ) == m_histogramDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_histogramDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_histDialogOpen = false;
}

void BALEEN::cancelParticleFilterDefinition()
{
    ////////////////////////////qDebug() << "canceling particle filter";

    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_particleFilterDialogues.find( ptype ) == m_particleFilterDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_particleFilterDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }
    m_filterDialogOpen = false;
}

void BALEEN::updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_derivedVariables.at( ptype ) = derived;
        m_dataSetManager->particleDataManager().updateDerivations( ptype, derived );
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_currentConfigurations.distributionBasedConfiguration.m_derivedVariables.at( ptype ) = derived;
        m_dataSetManager->distributionManager().updateDerivations( ptype, derived );
    }

    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::updateCustomConstants( const std::map< std::string, CustomConstant > & cc )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_customConstants = cc;
        m_dataSetManager->particleDataManager().updateCustomConstants( cc );
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_customConstants = cc;
        m_dataSetManager->distributionManager().updateCustomConstants( cc );
    }

    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::updateBaseConstants( const std::map< std::string, BaseConstant > & cc )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_baseConstants = cc;
        m_dataSetManager->particleDataManager().updateBaseConstants( cc );
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_baseConstants = cc;
        m_dataSetManager->distributionManager().updateBaseConstants( cc );
    }

    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & variables )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_baseVariables.find( ptype )->second = variables;
        m_dataSetManager->particleDataManager().updateBaseVariables( ptype, variables );
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_baseVariables.find( ptype )->second = variables;
        m_dataSetManager->distributionManager().updateBaseVariables( ptype, variables );
    }

    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeROIDefinition(
    const std::string & roiKey,
    const std::map< std::string, ROIDefinition > & roiDefinitions,
    const std::map< std::string, BaseVariable >        & baseVars,
    const std::map< std::string, DerivedVariable >  & derivedVars,
    const std::map< std::string, BaseConstant  >    & baseConsts,
    const std::map< std::string, CustomConstant   > & custConsts )
{
    bool storeState = m_trackState;
    m_trackState = false;

    qDebug() << "finalizing ROI definition";

    const std::string & ptype = m_particleCombo.selectedItemText();

    if( m_roiDialogs.find( ptype ) == m_roiDialogs.end() )
    {
        qDebug() << "roi dialig, pype not mapped";
    }

    auto & currentDialogsForCurrentParticle = ( m_roiDialogs.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( roiKey ) == currentDialogsForCurrentParticle.end() )
    {
        qDebug() << "roi key is not found, erasing temp, and inserting new key";

        if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
        {
            qDebug() << "error couldn't find temp";
        }

        ROIDialog *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { roiKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    qDebug() << "getting definitions from configuration";

    if( m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.find( ptype ) ==
            m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.end() )
    {
        qDebug() << "configuration doesn't have ptype mapped";
    }

    m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.at( ptype ) = roiDefinitions;

//    updateBaseVariables( ptype, baseVars );
//    updateDerivations( ptype, derivedVars );
//    updateCustomConstants( custConsts );
//    updateBaseConstants( baseConsts );

    m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.clear();

    int comboIdx = 0;
    for( auto & r : m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.find( ptype )->second )
    {
        int idx = m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.addItem( r.second.name );
        qDebug() << "adding roi item";
        if( r.second.name == roiKey )
        {
            comboIdx = idx;
        }
    }
    if( m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.numItems() )
    {
        m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectItem( comboIdx );
    }

    //////////////////////////qDebug() << "updated combo selection";
    ///
    m_roiDialogOpen = false;

    finalizeProjection( roiDefinitions.at( roiKey ).space, true );

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeHistogramDefinition(
    const std::string & histKey,
    const std::map< std::string, HistogramDefinition > & histDefinitions,
    const std::map< std::string, BaseVariable >        & baseVars,
    const std::map< std::string, DerivedVariable >  & derivedVars,
    const std::map< std::string, BaseConstant  >    & baseConsts,
    const std::map< std::string, CustomConstant   > & custConsts )
{
    bool storeState = m_trackState;
    m_trackState = false;

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_histogramDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( histKey ) == currentDialogsForCurrentParticle.end() )
    {
        HistogramDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { histKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    ////////////////////////qDebug() << "before set config hists";
    m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( ptype )->second = histDefinitions;
    ////////////////////////qDebug() << "after set config hists";

    updateBaseVariables( ptype, baseVars );

    ////////////////////////qDebug() << "updated baseVars";

    updateDerivations( ptype, derivedVars );

    ////////////////////////qDebug() << "updated derivations";

    updateCustomConstants( custConsts );

    ////////////////////////qDebug() << "updated custConst";

    updateBaseConstants( baseConsts );

    ////////////////////////qDebug() << "updated baseConstants";

    m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.addItem( h.second.name );
        if( h.second.name == histKey )
        {
            comboIdx = idx;
        }
    }
    if( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.numItems() )
    {
        m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectItem( comboIdx );
    }

    //////////////////////////qDebug() << "updated combo selection";

    m_histDefinitionEdited = true;
    m_histDialogOpen = false;

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeParticleFilterDefinition
(
    const std::string & key,
    const std::map< std::string, ParticleFilter >   & particleFilters,
    const std::map< std::string, BaseVariable >     & baseVars,
    const std::map< std::string, DerivedVariable >  & derivedVars,
    const std::map< std::string, BaseConstant  >    & baseConsts,
    const std::map< std::string, CustomConstant   > & custConsts )
{
    bool storeState = m_trackState;
    m_trackState = false;

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_particleFilterDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( key ) == currentDialogsForCurrentParticle.end() )
    {
        ParticleFilterDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { key, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( ptype )->second = particleFilters;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_filterCombo.clear();
    m_filterCombo.addItem( "None" );

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_filterCombo.addItem( h.second.name );
        if( h.second.name == key )
        {
            comboIdx = idx;
        }
    }
    if( m_filterCombo.numItems() )
    {
        m_filterCombo.selectItem( comboIdx );
    }

    m_filterDialogOpen = false;
    invalidateSampling();
    m_filterEdited = true;

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::updateUserDataDescription( const std::string & desc )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_userDataDescription = desc;
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_userDataDescription = desc;
    }
}

void BALEEN::updateAttributes(
    const std::map<std::string, BaseVariable> & bv,
    const std::map<std::string, DerivedVariable> & dv,
    const std::map<std::string, BaseConstant> & bc,
    const std::map<std::string, CustomConstant> & cc
)
{
    bool storeState = m_trackState;
    m_trackState = false;

    updateBaseConstants( bc );
    updateCustomConstants( cc );
    updateBaseVariables( m_particleCombo.selectedItemText(), bv );
    updateDerivations( m_particleCombo.selectedItemText(), dv );

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::mouseReleaseEvent( QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos();
    Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );

    m_mouseIsPressed = false;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Buttons

    /****************/

    for( auto & button : m_samplerWidget.visibleButtons() )
    {
        if( button->isPressed() )
        {
            if( button->mode() != TN::PressButton::Mode::Toggle )
            {
                button->setPressed( false );
            }
            if( button == & m_samplerWidget.controlPanel.histSelectionPanel.spatialButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.pointInViewPort( pos ) )
                {
                    if( m_presetSelected )
                    {
                        if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
                        {
                            m_spatialPartitioningDialog->update( m_dataSetManager->distributionManager().attributeKeys() );
                        }
                        else if ( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
                        {
                            m_spatialPartitioningDialog->update( m_dataSetManager->particleDataManager().attributeKeys( m_particleCombo.selectedItemText() ) );
                        }

                        m_spatialPartitioningDialog->show();
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // ROI Dialog

            if( button == & m_samplerWidget.controlPanel.histSelectionPanel.addROIButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.addROIButton.pointInViewPort( pos ) )
                {
                    qDebug() << "released roi button";

                    if( m_datasetCombo.selectedItemText() == "" )
                    {
                        return;
                    }

                    if( m_roiDialogOpen )
                    {
                        return;
                    }

                    m_roiDialogOpen = false;

                    if( m_particleCombo.numItems() > 0 )
                    {
                        std::string currentParticleType = m_particleCombo.selectedItemText();

//                        qDebug() << "released roi button";

                        auto it = m_roiDialogs.find( currentParticleType );
                        if( it == m_roiDialogs.end() )
                        {
                            ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define hist button";
                        }
                        else
                        {
                            ROIDialog *dialog = new ROIDialog( "temp", this );
                            auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                            connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT( cancelROIDefinition() ) );

                            connect( inserted.first->second,
                                     SIGNAL(
                                         finalizeDefinition(
                                             const std::string &,
                                             const std::map< std::string, ROIDefinition > &,
                                             const std::map< std::string, BaseVariable > &,
                                             const std::map< std::string, DerivedVariable > &,
                                             const std::map< std::string, BaseConstant > &,
                                             const std::map< std::string, CustomConstant > &
                                         )
                                     ),
                                     this,
                                     SLOT(
                                         finalizeROIDefinition(
                                             const std::string &,
                                             const std::map< std::string, ROIDefinition > &,
                                             const std::map< std::string, BaseVariable > &,
                                             const std::map< std::string, DerivedVariable > &,
                                             const std::map< std::string, BaseConstant > &,
                                             const std::map< std::string, CustomConstant > &
                                         ) ) );

                            connect( inserted.first->second,
                                     SIGNAL( updateAttributes(
                                                 const std::map<std::string, BaseVariable> &,
                                                 const std::map<std::string, DerivedVariable> &,
                                                 const std::map<std::string, BaseConstant> &,
                                                 const std::map<std::string, CustomConstant> & ) ),
                                     this,
                                     SLOT( updateAttributes(
                                               const std::map<std::string, BaseVariable> &,
                                               const std::map<std::string, DerivedVariable> &,
                                               const std::map<std::string, BaseConstant> &,
                                               const std::map<std::string, CustomConstant> & ) ) );

                            connect( inserted.first->second,
                                     SIGNAL( updateUserDataDescription(
                                                 const std::string & ) ),
                                     this,
                                     SLOT( updateUserDataDescription(
                                               const std::string & ) ) );

                            inserted.first->second->update(
                                m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.find( currentParticleType )->second,
                                m_dataSetManager->distributionManager().baseVariables().at( currentParticleType ),
                                m_dataSetManager->distributionManager().derivedVariables().at( currentParticleType ),
                                m_dataSetManager->distributionManager().customConstants(),
                                m_dataSetManager->distributionManager().baseConstants() );

                            inserted.first->second->show();
                        }
                    }
                }
            }

            if( button == &  m_samplerWidget.controlPanel.histSelectionPanel.editROIButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.editROIButton.pointInViewPort( pos ) )
                {

                    if( m_datasetCombo.selectedItemText() == "" )
                    {
                        return;
                    }

                    auto it = m_roiDialogs.find( m_particleCombo.selectedItemText() );
                    if( it == m_roiDialogs.end() )
                    {
                        //////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                    }
                    else
                    {
                        auto it2 = it->second.find( m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectedItemText() );
                        if( it2 == it->second.end() )
                        {
                            //////////////////////qDebug() << "error pdf is not mapped";
                        }
                        else
                        {
//                            std::string currentParticleType = m_particleCombo.selectedItemText();
//                            const HistogramDefinition & def = m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( m_particleCombo.selectedItemText() )->second.find( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() )->second;
//                            it2->second->update(
//                                m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( currentParticleType )->second,
//                                m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
//                                m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
//                                m_dataSetManager->particleDataManager().customConstants(),
//                                m_dataSetManager->particleDataManager().baseConstants() );
//                            it2->second->fromDefinition( def );
                            it2->second->show();
                        }
                    }
                }
            }


            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if( button == & m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.pointInViewPort( pos ) )
                {
                    if( m_datasetCombo.selectedItemText() == "" )
                    {
                        return;
                    }

                    if( m_histDialogOpen )
                    {
                        return;
                    }
                    m_histDialogOpen = true;

                    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
                    {
                        if( m_particleCombo.numItems() > 0 )
                        {
                            std::string currentParticleType = m_particleCombo.selectedItemText();

                            auto it = m_histogramDefinitionDialogues.find( currentParticleType );
                            if( it == m_histogramDefinitionDialogues.end() )
                            {
                                ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define hist button";
                            }
                            else
                            {
                                HistogramDefinitionDialogue *dialog = new HistogramDefinitionDialogue( "temp", this );
                                auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                                connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelHistogramDefinition() ) );
                                connect( inserted.first->second,
                                         SIGNAL(
                                             finalizeDefinition(
                                                 const std::string &,
                                                 const std::map< std::string, HistogramDefinition > &,
                                                 const std::map< std::string, BaseVariable > &,
                                                 const std::map< std::string, DerivedVariable > &,
                                                 const std::map< std::string, BaseConstant > &,
                                                 const std::map< std::string, CustomConstant > &
                                             )
                                         ),
                                         this,
                                         SLOT(
                                             finalizeHistogramDefinition(
                                                 const std::string &,
                                                 const std::map< std::string, HistogramDefinition > &,
                                                 const std::map< std::string, BaseVariable > &,
                                                 const std::map< std::string, DerivedVariable > &,
                                                 const std::map< std::string, BaseConstant > &,
                                                 const std::map< std::string, CustomConstant > &
                                             ) ) );
                                connect( inserted.first->second,
                                         SIGNAL( updateAttributes(
                                                     const std::map<std::string, BaseVariable> &,
                                                     const std::map<std::string, DerivedVariable> &,
                                                     const std::map<std::string, BaseConstant> &,
                                                     const std::map<std::string, CustomConstant> & ) ),
                                         this,
                                         SLOT( updateAttributes(
                                                   const std::map<std::string, BaseVariable> &,
                                                   const std::map<std::string, DerivedVariable> &,
                                                   const std::map<std::string, BaseConstant> &,
                                                   const std::map<std::string, CustomConstant> & ) ) );
                                connect( inserted.first->second,
                                         SIGNAL( updateUserDataDescription(
                                                     const std::string & ) ),
                                         this,
                                         SLOT( updateUserDataDescription(
                                                   const std::string & ) ) );

                                //                        //////////////////////qDebug() << "updating";
                                inserted.first->second->update(
                                    m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().customConstants(),
                                    m_dataSetManager->particleDataManager().baseConstants() );
                                inserted.first->second->show();
                            }
                        }
                    }
                }
            }

            if( button == &  m_samplerWidget.controlPanel.histSelectionPanel.editDistributionButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.editDistributionButton.pointInViewPort( pos ) )
                {

                    if( m_datasetCombo.selectedItemText() == "" )
                    {
                        return;
                    }

                    if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
                    {
                        //                auto it = m_gridProjectionDialogues.find( m_particleCombo.selectedItemText() );
                        //                if( it == m_gridProjectionDialogues.end() )
                        //                {
                        //                    ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                        //                }
                        //                else
                        //                {
                        //                    auto it2 = it->second.find( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() );
                        //                    if( it2 == it->second.end() )
                        //                    {
                        //                        ////////////////////////////qDebug() << "error pdf is not mapped";
                        //                    }
                        //                    else
                        //                    {
                        //                        it2->second->show();
                        //                    }
                        //                }
                    }

                    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
                    {
                        auto it = m_histogramDefinitionDialogues.find( m_particleCombo.selectedItemText() );
                        if( it == m_histogramDefinitionDialogues.end() )
                        {
                            //////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                        }
                        else
                        {
                            auto it2 = it->second.find( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() );
                            if( it2 == it->second.end() )
                            {
                                //////////////////////qDebug() << "error pdf is not mapped";
                            }
                            else
                            {
                                std::string currentParticleType = m_particleCombo.selectedItemText();
                                const HistogramDefinition & def = m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( m_particleCombo.selectedItemText() )->second.find( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() )->second;
                                it2->second->update(
                                    m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                                    m_dataSetManager->particleDataManager().customConstants(),
                                    m_dataSetManager->particleDataManager().baseConstants() );
                                it2->second->fromDefinition( def );
                                it2->second->show();
                            }
                        }
                    }
                }
            }

            if( button == &  m_samplerWidget.controlPanel.histSelectionPanel.centerButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.centerButton.pointInViewPort( pos ) )
                {
                    invalidateSampling();
                    resetSamplerCam();
                }
            }
            if( button == &  m_samplerWidget.controlPanel.histSelectionPanel.resetButton )
            {
                if( m_samplerWidget.controlPanel.histSelectionPanel.resetButton.pointInViewPort( pos ) )
                {
                    invalidateSampling();
                    m_sampler.resetToDefault();
                    setPtclOpacity( m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider.value() );
                }
            }
        }
    }

    /*******************/

    if( m_saveProjectButton.isPressed() )
    {
        m_saveProjectButton.setPressed( false );
        if( m_saveProjectButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_loadProjectFileButton.isPressed() )
    {
        m_loadProjectFileButton.setPressed( false );
        if( m_loadProjectFileButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_saveButton.isPressed() )
    {
        m_saveButton.setPressed( false );
        if( m_saveButton.pointInViewPort( pos ) )
        {
            if( m_datasetSelected && m_presetSelected )
            {
                if( m_configurationCombo.selectedItemText() != "base" )
                {
                    std::string name = m_configurationCombo.selectedItemText();
                    std::string author = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                         m_currentConfigurations.particleBasedConfiguration.m_author
                                         : m_currentConfigurations.distributionBasedConfiguration.m_author;

                    std::string desc = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                       m_currentConfigurations.particleBasedConfiguration.m_description
                                       : m_currentConfigurations.distributionBasedConfiguration.m_description;

                    //////////qDebug() << "updating save dialog " << name.c_str() << " " << author.c_str() << " " << desc.c_str();
                    m_savePresetDialog.setSavedConfigurations( & m_savedConfigurations.at( m_datasetCombo.selectedItemText() ) );
                    m_savePresetDialog.setConfig( & m_currentConfigurations );
                    m_savePresetDialog.update( name, author, desc );
                    //////////qDebug() << "updated";
                }
                //////////qDebug() << "showing dialog";
                m_saveDialogOpen = true;
                m_savePresetDialog.show();
            }
        }
    }
    if( m_initiateSessionButton.isPressed() )
    {
        m_initiateSessionButton.setPressed( false );
        if( m_initiateSessionButton.pointInViewPort( pos ) )
        {
            if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined && m_histogramDefined && ! m_forceSpacePartitioningSelection )
            {
                m_sessionInitiated = true;
            }
        }
    }
    if( m_closeButton.isPressed() )
    {
        m_closeButton.setPressed( false );
        if( m_closeButton.pointInViewPort( pos ) )
        {
            QApplication::quit();
        }
    }

    if( m_defineFilterButton.isPressed() )
    {
        m_defineFilterButton.setPressed( false );
        if( m_defineFilterButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_filterDialogOpen )
            {
                return;
            }
            m_filterDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_particleFilterDialogues.find( currentParticleType );
                    if( it == m_particleFilterDialogues.end() )
                    {
                        ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define hist button";
                    }
                    else
                    {
                        ParticleFilterDialogue *dialog = new ParticleFilterDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelParticleFilterDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, ParticleFilter > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizeParticleFilterDefinition(
                                         const std::string &,
                                         const std::map< std::string, ParticleFilter > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants(),
                            m_dataSetManager->particleDataManager().fullTimeSeries(),
                            m_dataSetManager->particleDataManager().realTime(),
                            m_dataSetManager->particleDataManager().simTime() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }
    if( m_editFilterButton.isPressed() )
    {
        m_editFilterButton.setPressed( false );

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editFilterButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_particleFilterDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_particleFilterDialogues.end() )
                {
                    ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_filterCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        ////////////////////////////qDebug() << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_definePlotButton.isPressed() )
    {
        m_definePlotButton.setPressed( false );
        if( m_definePlotButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_timePlotDialogOpen )
            {
                return;
            }
            m_timePlotDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_timePlotDefinitionDialogues.find( currentParticleType );
                    if( it == m_timePlotDefinitionDialogues.end() )
                    {
                        ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define phase plot button";
                    }
                    else
                    {
                        TimePlotDefinitionDialogue *dialog = new TimePlotDefinitionDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimePlotDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, TimePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizeTimePlotDefinition(
                                         const std::string &,
                                         const std::map< std::string, TimePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );

                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );

                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }

    if( m_defineTimeSeriesButton.isPressed() )
    {
        m_defineTimeSeriesButton.setPressed( false );
        if( m_defineTimeSeriesButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_timeSeriesDialogOpen )
            {
                return;
            }
            m_timeSeriesDialogOpen = true;

            if( m_particleCombo.numItems() > 0 )
            {
                std::string currentParticleType = m_particleCombo.selectedItemText();

                auto it = m_timeSeriesDefinitionDialogues.find( currentParticleType );
                if( it == m_timeSeriesDefinitionDialogues.end() )
                {
                    ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define time series button";
                }
                else
                {
                    TimeSeriesDefinitionDialogue *dialog = new TimeSeriesDefinitionDialogue( "temp", this );
                    auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimeSeriesDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimeSeriesDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 ) ) );
                    //
                    // need full time series, real time and sim time

                    inserted.first->second->update(
                        m_currentConfigurations.timeSeriesDefinitions().find( currentParticleType )->second,
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().fullTimeSeries()
                        :  m_dataSetManager->distributionManager().fullTimeSeries(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
                        :  m_dataSetManager->distributionManager().realTime(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
                        :  m_dataSetManager->distributionManager().simTime() );
                    inserted.first->second->show();
                }
            }
        }
    }
    if( m_definePhasePlotButton.isPressed() )
    {
        m_definePhasePlotButton.setPressed( false );
        if( m_definePhasePlotButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_phasePlotDialogOpen )
            {
                return;
            }
            m_phasePlotDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_phasePlotDefinitionDialogues.find( currentParticleType );
                    if( it == m_phasePlotDefinitionDialogues.end() )
                    {
                        ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped define phase plot button";
                    }
                    else
                    {
                        PhasePlotDefinitionDialogue *dialog = new PhasePlotDefinitionDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelPhasePlotDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, PhasePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizePhasePlotDefinition(
                                         const std::string &,
                                         const std::map< std::string, PhasePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }
    if( m_inspectWarningsButton.isPressed() )
    {
        m_inspectWarningsButton.setPressed( false );
        if( m_inspectWarningsButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_editPhasePlotButton.isPressed() )
    {
        m_editPhasePlotButton.setPressed( false );
        m_editPhasePlotButton.setPressed( false );
        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editPhasePlotButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_phasePlotDefinitionDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_phasePlotDefinitionDialogues.end() )
                {
                    ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_phasePlotCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        ////////////////////////////qDebug() << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_editTimePlotButton.isPressed() )
    {
        m_editTimePlotButton.setPressed( false );
        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editTimePlotButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_timePlotDefinitionDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_timePlotDefinitionDialogues.end() )
                {
                    ////////////////////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_timePlotCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        ////////////////////////////qDebug() << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_editTimeSeriesButton.isPressed() )
    {
        m_editTimeSeriesButton.setPressed( false );
        if( m_editTimeSeriesButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            auto it = m_timeSeriesDefinitionDialogues.find( m_particleCombo.selectedItemText() );
            if( it == m_timeSeriesDefinitionDialogues.end() )
            {
                //////////////qDebug() << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
            }
            else
            {
                auto it2 = it->second.find( m_timeSeriesCombo.selectedItemText() );
                if( it2 == it->second.end() )
                {
                    //////////////qDebug() << "error pdf is not mapped";
                }
                else
                {
                    it2->second->show();
                }
            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Sliders

    for( auto & slider : m_samplerWidget.visibleSliders() )
    {
        slider->setPressed( false );
    }

    m_isoValueSlider.setPressed( false );
    m_gridColorScaleSlider.setPressed( false );

    if( m_smoothRadiusSlider.isPressed() )
    {
        renderLater();
    }
    m_smoothRadiusSlider.setPressed( false );

    // resize widgets

    m_resizeWidgetA.setPressed( false );
    m_resizeWidgetB.setPressed( false );
    m_resizeWidgetC.setPressed( false );
    m_resizeWidgetD.setPressed( false );

    /////////////////////////////////////////////

    // help button widgets

    for( HelpLabelWidget * hl : m_helpLabelWidgetRefs )
    {
        if( hl->pointInViewPort( pos ) )
        {
//            if( hl->isPressed() )
//            {
                if( hl == &m_helpButtonBinning )
                {
                    m_samplerWidget.controlPanel.activate( ! m_samplerWidget.controlPanel.isActive() );
                }

//                QDir dir( QCoreApplication::applicationDirPath() );
//                QString link = dir.absoluteFilePath( ( RELATIVE_PATH_PREFIX + "UserGuide/index.html" ).c_str() );// + hl->htmlReference();
//                link.replace( " ", "\ " );
//              QUrl url = QUrl( "file:///" + link + hl->htmlReference().c_str(), QUrl::TolerantMode );
////                url.setFragment( hl->htmlReference().c_str(), QUrl::TolerantMode );
//                              QDesktopServices::openUrl( url );
            }
//        }
        //hl->setPressed( false );
    }

    ////////////////////////////////////////////
    renderLater();
}

void BALEEN::cancelPhasePlotDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_phasePlotDefinitionDialogues.find( ptype ) == m_phasePlotDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_phasePlotDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_phasePlotDialogOpen = false;
}

void BALEEN::finalizePhasePlotDefinition(
    const std::string & plotKey,
    const std::map<std::string, PhasePlotDefinition > & phasePlotDefinitions,
    const std::map<std::string, BaseVariable> & baseVars,
    const std::map<std::string, DerivedVariable> & derivedVars,
    const std::map<std::string, BaseConstant> & baseConsts,
    const std::map<std::string, CustomConstant> & custConsts )
{
    bool storeState = m_trackState;
    m_trackState = false;

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_phasePlotDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( plotKey ) == currentDialogsForCurrentParticle.end() )
    {
        PhasePlotDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { plotKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second = phasePlotDefinitions;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_phasePlotCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second )
    {
        int idx = m_phasePlotCombo.addItem( h.second.name );
        if( h.second.name == plotKey )
        {
            comboIdx = idx;
        }
    }
    if( m_phasePlotCombo.numItems() )
    {
        m_phasePlotCombo.selectItem( comboIdx );
    }

//    PhasePlotDefinition & def = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second.find( plotKey )->second;

    m_phasePlotDialogOpen = false;

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeTimeSeriesDefinition(
    const std::string & seriesKey,
    const std::map<std::string, TimeSeriesDefinition > & timeSeriesDefinitions )
{
    bool storeState = m_trackState;
    m_trackState = false;

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_timeSeriesDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( seriesKey ) == currentDialogsForCurrentParticle.end() )
    {
        TimeSeriesDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { seriesKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.timeSeriesDefinitions().find( ptype )->second = timeSeriesDefinitions;

    //need a combo for the plot separate from the 'mode'
    m_timeSeriesCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.timeSeriesDefinitions().find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_timeSeriesCombo.addItem( h.second.name );
        if( h.second.name == seriesKey )
        {
            comboIdx = idx;
        }
    }
    if( m_timeSeriesCombo.numItems() )
    {
        m_timeSeriesCombo.selectItem( comboIdx );
    }

    m_recalculateFullTimePlot = true;
    m_timeSeriesDialogOpen = false;
    renderLater();

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeTimePlotDefinition(
    const std::string & plotKey,
    const std::map<std::string, TimePlotDefinition > & timePlotDefinitions,
    const std::map<std::string, BaseVariable> & baseVars,
    const std::map<std::string, DerivedVariable> & derivedVars,
    const std::map<std::string, BaseConstant> & baseConsts,
    const std::map<std::string, CustomConstant> & custConsts )
{
    bool storeState = m_trackState;
    m_trackState = false;

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_timePlotDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( plotKey ) == currentDialogsForCurrentParticle.end() )
    {
        TimePlotDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { plotKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( ptype )->second =  timePlotDefinitions;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_timePlotCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_timePlotCombo.addItem( h.second.name );
        if( h.second.name == plotKey )
        {
            comboIdx = idx;
        }
    }
    if( m_timePlotCombo.numItems() )
    {
        m_timePlotCombo.selectItem( comboIdx );
    }

    m_recalculateFullTimePlot = true;
    m_timePlotDialogOpen = false;

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::finalizeProjection( std::vector< std::string > projection, bool preserveAspect )
{
    bool storeState = m_trackState;
    m_trackState = false;

    SpacePartitioningDefinition * sp;
    const std::string & ptype = m_particleCombo.selectedItemText();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        sp = & ( m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second );
    }
    else
    {
        sp = & ( m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second );
    }

    sp->attributes.clear();
    sp->scaling.clear();

    sp->preserveAspectRatio = preserveAspect;

    for( const auto & vKey : projection )
    {
        sp->attributes.push_back( vKey );
        sp->scaling.push_back( 1.0 );
    }

    m_histDefinitionEdited = true;

    ////////////////////////qDebug() << "set projection";
    m_forceSpacePartitioningSelection = false;

    m_trackState = storeState;
    if( m_trackState )
    {
        pushState();
    }
}

void BALEEN::mouseMoveEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos() * devicePixelRatio();
    Vec2< float > pos1( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y()  );

    QVector2D mouseDir =
        QVector2D((mouseCurr - m_previousMousePosition )
                  / ( height()));

    mouseDir.setY(-mouseDir.y());

    m_previousMousePosition = mouseCurr;

    // Weight Breakdown Widgets

    if( m_binWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_binWeightHistWidget.suppressed( false );
        m_regionWeightHistWidget.suppressed( true );
        m_allWeightHistWidget.suppressed( true );

        m_binWeightHistWidget.hover( pos1 );
        m_regionWeightHistWidget.mouseOut();
        m_allWeightHistWidget.mouseOut();
    }
    else if ( m_regionWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_binWeightHistWidget.suppressed( true );
        m_regionWeightHistWidget.suppressed( false );
        m_allWeightHistWidget.suppressed( true );

        m_regionWeightHistWidget.hover( pos1 );
        m_binWeightHistWidget.mouseOut();
        m_allWeightHistWidget.mouseOut();
    }
    else if ( m_allWeightHistWidget.pointInViewPort( pos1 ) )
    {
        m_binWeightHistWidget.suppressed( true );
        m_regionWeightHistWidget.suppressed( true );
        m_allWeightHistWidget.suppressed( false );

        m_allWeightHistWidget.hover( pos1 );
        m_binWeightHistWidget.mouseOut();
        m_regionWeightHistWidget.mouseOut();
    }
    else
    {
        m_binWeightHistWidget.suppressed( false );
        m_regionWeightHistWidget.suppressed( false );
        m_allWeightHistWidget.suppressed( false );

        m_binWeightHistWidget.mouseOut();
        m_regionWeightHistWidget.mouseOut();
        m_allWeightHistWidget.mouseOut();
    }

    // sliders

    bool sliderDown = false;

    for( auto & slider : m_samplerWidget.visibleSliders() )
    {
        if( slider->isPressed() )
        {
            sliderDown = true;

            if( slider->invalidates() )
            {
                invalidateSampling();
            }

            if( ( slider == & m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX ||
                    slider == & m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY )
                    && m_samplerWidget.controlPanel.histogramPanel.linkValueWidthSlidersButton.isPressed() )
            {
                translateLinkedSliders(
                    m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX,
                    m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY,
                    pos1
                );
            }
            else if( ( slider == & m_samplerWidget.controlPanel.roiScalePanel.xScaleSlider
                       || slider == & m_samplerWidget.controlPanel.roiScalePanel.yScaleSlider )
                     && m_samplerWidget.controlPanel.roiScalePanel.linkScaleSlidersButton.isPressed() )
            {
                translateLinkedSliders(
                    m_samplerWidget.controlPanel.roiScalePanel.xScaleSlider,
                    m_samplerWidget.controlPanel.roiScalePanel.yScaleSlider,
                    pos1
                );
            }
            else if( ( slider == & m_samplerWidget.controlPanel.roiPanel.xSplitSlider
                       || slider == & m_samplerWidget.controlPanel.roiPanel.ySplitSlider )
                     && m_samplerWidget.controlPanel.roiPanel.linkSplitSlidersButton.isPressed() )
            {
                translateLinkedSliders(
                    m_samplerWidget.controlPanel.roiPanel.xSplitSlider,
                    m_samplerWidget.controlPanel.roiPanel.ySplitSlider,
                    pos1
                );
            }
            else if ( slider == & m_samplerWidget.controlPanel.contextPanel.bkgOpacitySlider )
            {
                slider->translate( pos1 );
                setPtclOpacity( slider->sliderPosition() );
            }
            else
            {
                slider->translate( pos1 );
            }
        }
    }

    if( m_isoValueSlider.isPressed() )
    {
        sliderDown = true;
        m_isoValueSlider.translate( pos1 );
    }
    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        if( m_smoothRadiusSlider.isPressed() )
        {
            sliderDown = true;
            m_smoothRadiusSlider.translate( pos1 );
        }
        if( m_gridColorScaleSlider.isPressed() )
        {
            sliderDown = true;
            m_gridColorScaleSlider.translate( pos1 );
        }
    }

    ///////////

    const float viewSpaceWidth = scaledWidth();
    const float viewSpaceHeight = ( scaledHeight() - MENU_HEIGHT );

    // resize widgets

    if(  m_resizeWidgetA.pointInViewPort( pos1 )
            || m_resizeWidgetB.pointInViewPort( pos1 )
      )
    {
        QApplication::setOverrideCursor( Qt::SizeHorCursor );
    }
    else if ( m_resizeWidgetC.pointInViewPort( pos1 )
              || m_resizeWidgetD.pointInViewPort( pos1 ) )
    {
        QApplication::setOverrideCursor( Qt::SizeVerCursor );
    }
    else
    {
        QApplication::restoreOverrideCursor();
    }

    if( m_resizeWidgetA.isPressed() )
    {
        double detailViewMax = std::min( DETAIVIEW_MAX_WIDTH, scaledWidth()
                                         - SAMPLER_MIN_WIDTH - PHASEVIEW_MIN_WIDTH - DIVIDER_WIDTH * 2 - 2 );
        float rzOffst = std::min( std::max( (double) pos1.x(), DETAILVIEW_MIN_WIDTH ), detailViewMax );

        if( ! ( m_miniMapViewPort.height() <= MINIMAP_MIN_HEIGHT && rzOffst > m_resizeWidgetA.position().x() ) )
        {
            m_resizeWidgetA.setPosition( rzOffst, m_resizeWidgetA.position().y() );
            m_resizeWidgetC.setPosition( rzOffst + DIVIDER_WIDTH, m_resizeWidgetC.position().y() );
            m_resizeWidgetD.setPosition( rzOffst + DIVIDER_WIDTH, m_resizeWidgetD.position().y() );

            m_resizeWidgetC.setSize( scaledWidth() - m_resizeWidgetA.position().x() - DIVIDER_WIDTH, RESIZE_HANDLE_WIDTH );
            m_resizeWidgetD.setSize( m_resizeWidgetC.size().x(), RESIZE_HANDLE_WIDTH );

            if( m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x() < SAMPLER_MIN_WIDTH )
            {
                ////////////////////qDebug() << "reset part B to " << m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH;
                m_resizeWidgetB.setPosition( m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH, m_resizeWidgetB.position().y() );
            }

            m_uiLayoutEdited = true;
            m_uiLayoutInitialized = true;
        }
    }
    else if ( m_resizeWidgetB.isPressed() )
    {
        float rzOffst = pos1.x();

        if( viewSpaceWidth - rzOffst < PHASEVIEW_MIN_WIDTH)
        {
            rzOffst = viewSpaceWidth - PHASEVIEW_MIN_WIDTH;
        }

        ////////////////////qDebug() << "reset B to " << rzOffst;
        m_resizeWidgetB.setPosition( rzOffst, m_resizeWidgetB.position().y() );

        if( m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x()  < SAMPLER_MIN_WIDTH )
        {
            ////////////////////qDebug() << "set B to  " << m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH;
            m_resizeWidgetB.setPosition( m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH, m_resizeWidgetB.position().y() );
        }

        m_uiLayoutEdited = true;
        m_uiLayoutInitialized = true;
    }
    else if ( m_resizeWidgetC.isPressed() )
    {
        float rzOffst = pos1.y();
        if( viewSpaceHeight - rzOffst - MENU_HEIGHT < SAMPLER_MIN_HEIGHT )
        {
            rzOffst = viewSpaceHeight - SAMPLER_MIN_HEIGHT - MENU_HEIGHT + 1;
        }

        if( rzOffst < ISOVIEW_MIN_HEIGHT + TIMELINE_MIN_HEIGHT + DIVIDER_WIDTH )
        {
            rzOffst = ISOVIEW_MIN_HEIGHT + TIMELINE_MIN_HEIGHT + DIVIDER_WIDTH;
        }

        m_resizeWidgetC.setPosition( m_resizeWidgetC.position().x(), rzOffst );

        if( m_resizeWidgetC.position().y() - m_resizeWidgetD.position().y() < ISOVIEW_MIN_HEIGHT )
        {
            m_resizeWidgetD.setPosition( m_resizeWidgetD.position().x(), rzOffst - ISOVIEW_MIN_HEIGHT );
        }

        ////////////////////qDebug() << "B is set to " <<  m_resizeWidgetB.position().x();
        m_resizeWidgetB.setPosition( m_resizeWidgetB.position().x(), rzOffst + DIVIDER_WIDTH );
        m_resizeWidgetB.setSize( m_resizeWidgetB.size().x(), scaledHeight()
                                 - m_resizeWidgetC.position().y() - MENU_HEIGHT - DIVIDER_WIDTH );

        m_uiLayoutEdited = true;
        m_uiLayoutInitialized = true;
    }
    else if ( m_resizeWidgetD.isPressed() )
    {
        float rzOffst = pos1.y();

        if( scaledHeight() - rzOffst - m_histogramViewPort.height() - MENU_HEIGHT
                - DIVIDER_WIDTH * 2 - m_samplerWidget.controlPanel.size().y() < MINIMAP_MIN_HEIGHT )
        {
            rzOffst = scaledHeight() - m_histogramViewPort.height()
                      - MENU_HEIGHT - DIVIDER_WIDTH * 2 - m_samplerWidget.controlPanel.size().y() - MINIMAP_MIN_HEIGHT - 2;
        }
        if( rzOffst < TIMELINE_MIN_HEIGHT )
        {
            rzOffst = TIMELINE_MIN_HEIGHT;
        }
        if( viewSpaceHeight - rzOffst < SAMPLER_MIN_HEIGHT + DIVIDER_WIDTH + ISOVIEW_MIN_HEIGHT )
        {
            rzOffst = viewSpaceHeight - ( SAMPLER_MIN_HEIGHT + DIVIDER_WIDTH + ISOVIEW_MIN_HEIGHT );
        }
        m_resizeWidgetC.setPosition( m_resizeWidgetC.position().x(), rzOffst + ( m_resizeWidgetC.position().y() - m_resizeWidgetD.position().y() ) );

        if( viewSpaceHeight - m_resizeWidgetC.position().y() < SAMPLER_MIN_HEIGHT )
        {
            m_resizeWidgetC.setPosition( m_resizeWidgetC.position().x(), viewSpaceHeight - SAMPLER_MIN_HEIGHT );
        }

        m_resizeWidgetD.setPosition( m_resizeWidgetD.position().x(), rzOffst );

        m_resizeWidgetB.setPosition( m_resizeWidgetB.position().x(), m_resizeWidgetC.position().y() + DIVIDER_WIDTH );
        m_resizeWidgetB.setSize( m_resizeWidgetB.size().x(), scaledHeight() - m_resizeWidgetC.position().y()
                                 - MENU_HEIGHT - DIVIDER_WIDTH );

        m_uiLayoutEdited = true;
        m_uiLayoutInitialized = true;
    }

    ///////////

    if (e->buttons() & Qt::RightButton)
    {
        if( m_samplerWidget.visualizationWindow.pointInViewPort( pos1 ) )
        {
        }
        else if ( m_auxViewPort.pointInViewPort( pos1 ) )
        {
            auxViewCamera.track(mouseDir);
        }
        else if ( m_miniMapViewPort.pointInViewPort( pos1 ) )
        {

        }
        else if ( m_histogramViewPort.pointInViewPort( pos1 ) )
        {

        }
        else if ( m_timeHistIsoViewport.pointInViewPort( pos1 ) )
        {
            isoCam.track(mouseDir);
        }
    }
    if (e->buttons() & Qt::LeftButton && sliderDown == false
            && ! ( m_resizeWidgetA.isPressed()
                   || m_resizeWidgetB.isPressed()
                   || m_resizeWidgetC.isPressed()
                   || m_resizeWidgetD.isPressed() ) )
    {
        if( m_sampler.pointInViewPort( pos1 ) )
        {
//            if( m_samplerWidget.controlPanel.mode == TN::SamplerPanel::Mode::PanningWindows )
//            {
            camera.dragLeftButton( ( Vec2< float >( mouseDir.x(), mouseDir.y() ) * 5000 ) * camera.getZoom() );
            invalidateSampling();
//            }
        }
        else if ( m_auxViewPort.pointInViewPort( pos1 ) )
        {
            auxViewCamera.orbit(mouseDir);
        }
        else if ( m_miniMapViewPort.pointInViewPort( pos1 ) )
        {

        }
        else if ( m_histogramViewPort.pointInViewPort( pos1 ) )
        {
            m_selectedHistogramRotation += mouseDir.x() * 400;
            m_selectedHistogramRotationY += mouseDir.y() * 400;
        }
        else if ( m_timeHistIsoViewport.pointInViewPort( pos1 ) )
        {
            m_histogramStackRotation += mouseDir.y() * 400;
            m_histogramStackRotationY += mouseDir.x() * 400;
        }

        if(  e->buttons() & Qt::LeftButton )
        {
            if( m_timeLineWidget.pointInViewPort( pos1 + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
                    && m_timeLineWidget.pointInViewPort( pos1 + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) )
                    && m_sessionInitiated )

            {
                int offX = pos1.x() - ( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT );

                TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                            : m_dataSetManager->distributionManager().currentTimeSeries();
                double relative = ( offX / ( double ) m_timeLineWidget.plotSize().x() );
                int intTStep = ( std::max( std::min( static_cast< int32_t >( std::round( tsrs.firstIdx + relative * ( tsrs.lastIdx - tsrs.firstIdx ) ) ), tsrs.lastIdx ), tsrs.firstIdx ) );
                int diffStride = ( intTStep - tsrs.firstIdx ) % tsrs.idxStride;
                if( diffStride != 0 )
                {
                    if( diffStride > tsrs.idxStride / 2.0 )
                    {
                        intTStep += tsrs.idxStride - diffStride;
                    }
                    else
                    {
                        intTStep -= diffStride;
                    }
                }

                m_selectedTimeStep = intTStep;
                m_timeLineWidget.setTimeStep( m_selectedTimeStep );

                ////////////////////////////qDebug() << "set time step to " << m_selectedTimeStep;

                renderLater();
            }

            if( m_allWeightHistWidget.pointInViewPort( pos1 ) )
            {
                m_allWeightHistWidget.mouseDrag( pos1 );
            }
            if( m_regionWeightHistWidget.pointInViewPort( pos1 ) )
            {
                m_regionWeightHistWidget.mouseDrag( pos1 );
            }
            if( m_binWeightHistWidget.pointInViewPort( pos1 ) )
            {
                m_binWeightHistWidget.mouseDrag( pos1 );
            }

            if( m_allWeightHistWidget.rangeSelector.pointInViewPort( pos1 ) )
            {
                m_allWeightHistWidget.rangeSelector.mouseDrag( pos1 );
            }
            if( m_regionWeightHistWidget.rangeSelector.pointInViewPort( pos1 ) )
            {
                m_regionWeightHistWidget.rangeSelector.mouseDrag( pos1 );
            }
            if( m_binWeightHistWidget.rangeSelector.pointInViewPort( pos1 ) )
            {
                m_binWeightHistWidget.rangeSelector.mouseDrag( pos1 );
            }
        }
    }

    QVector3D c1 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1, 1, 0 );
    QVector3D c2 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1, 1, 0 );
    QVector3D c3 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1,-1, 0 );
    QVector3D c4 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1,-1, 0 );

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    QVector3D pos;

    float w = scaledWidth();
    float h = scaledHeight();

    pos.setX( (     mouseCurr.x() - w / 2 )   / ( w / 2 ) );
    pos.setY( ( - ( mouseCurr.y() - h / 2 ) ) / ( h / 2 ) );
    pos.setZ( 0 );
    pos = ( camera.proj() * camera.view() ).inverted() * pos;

    //////////////////////////////////////////////////////////////////////////////////////////////

    if( !  m_samplerWidget.controlPanel.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) )
        && m_samplerWidget.visualizationWindow.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) )
        && m_uiLayoutEdited == false )
    {
        if (e->buttons() & Qt::LeftButton && m_sampler.hasPointSelection() )
        {
            m_sampler.mouseDrag( Vec3< float>( pos.x(), pos.y(), 0.0 ) );
        }
        if( e->buttons() & Qt::RightButton )
        {
            m_sampler.rightMouseDrag( Vec3< float>( pos.x(), pos.y(), 0.0 ), Vec2< float >( mouseDir.x(), mouseDir.y() )*2 );
        }
        else
        {
            m_sampler.mouseMove( Vec3< float>( pos.x(), pos.y(), 0.0 ), 2 );
        }
    }
    else
    {
        m_sampler.mouseOut();
    }

    if( m_histogramViewPort.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) )
            && m_uiLayoutEdited == false )
    {
        // get relative position in rendered histogram,
        // convert that position into the same position in the sampling grid,
        // then call the mouse move function with that position on the sampler

        ViewPort vp;
        vp.setSize( m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ), m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ) );
        vp.setPosition(
            m_histogramViewPort.offsetX() + HIST_PAD,
            m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - PANEL_HT );

        if( vp.pointInViewPort( Vec2< float >( mouseCurr.x(), scaledHeight() - mouseCurr.y() ) ) )
        {
            int cellId = m_sampler.getSelectedCellId();

            if( cellId >= 0 )
            {
                float px = ( mouseCurr.x() - vp.offsetX() ) / ( ( vp.offsetX() + vp.width() ) - vp.offsetX() );
                float py = ( ( scaledHeight() - mouseCurr.y() ) - vp.offsetY() ) / ( ( vp.offsetY() + vp.height() ) - vp.offsetY() );

                const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();

                Vec2< float > a = gridCorners[ cellId*4 + 0 ];
                Vec2< float > b = gridCorners[ cellId*4 + 1 ];
                Vec2< float > c = gridCorners[ cellId*4 + 2 ];
//                Vec2< float > d = gridCorners[ cellId*4 + 3 ];

                float newX = c.x() + ( a.x() - c.x() ) * px;
                float newY = b.y() + ( a.y() - b.y() ) * py;

                m_sampler.mouseMove( Vec3< float>( newX, newY, 0.f ), 2, "bin" );
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////


    /*********************** Combos ****************************/

    // these will be refactored out into panels
    for( auto comboPtr : m_comboRefs )
    {
        if( (*comboPtr).isPressed() )
        {
            int id = (*comboPtr).mousedOver( pos1 );
            if( id == -1 )
            {
                (*comboPtr).setPressed( false );
            }
            renderLater();
            return;
        }
    }

    // UI panel combos
    for( auto & panel : m_samplerWidget.controlPanel.visiblePanels() )
    {
        for( TN::ComboWidget * combo : panel->combos() )
        {
            if( (*combo).isPressed() )
            {
                int id = (*combo).mousedOver( pos1 );
                if( id == -1 )
                {
                    (*combo).setPressed( false );
                }
                renderLater();
                return;
            }
        }
    }

    /************************************************************/

    renderLater();
}

void BALEEN::compileShaders()
{

    std::cout << "recompiled shaders" << std::endl;

    m_recalculatePhasePlot = true;

    const std::string SHADER_PATH =
        std::string( "../Platform/" ) +
        ( VIDI_GL_MAJOR_VERSION == 3 ? "GL3" : "GL4" ) +
        std::string( "/shaders" );

    timeStackProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/timeStack.vert" ).c_str() );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/timeStack.frag" ).c_str() );

    if(!timeStackProg->link())
    {
        ////////////////////////////qDebug() << "timeStack Program linkage failed";
    }

    ///////////////////////////////////////////////////////////

    cartesianProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D.vert" ).c_str() );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg->link())
    {
        ////////////////////////////qDebug() << "flat2D Program linkage failed";
    }

    cartesianProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D3.vert" ).c_str() );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg3->link())
    {
        ////////////////////////////qDebug() << "flat2D3 Program linkage failed";
    }

    colorMapProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/vz.vert" ).c_str() );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/vz.frag" ).c_str() );

    if(!colorMapProg3->link())
    {
        ////////////////////////////qDebug() << "vz Program linkage failed";
    }

    points3DProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/points3D.vert" ).c_str() );


    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/points3D.frag" ).c_str() );

    if(!points3DProg->link())
    {
        ////////////////////////////qDebug() << "points3D Program linkage failed";
    }


    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        tubePathProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Vertex,
            ( SHADER_PATH + "/pathtube.vert" ).c_str() );


        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Fragment,
            ( SHADER_PATH + "/pathtube.frag" ).c_str() );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Geometry,
            ( SHADER_PATH + "/pathtube.geom" ).c_str() );

        if(!tubePathProgram->link())
        {
            ////////////////////////////qDebug() << "pathtube Program linkage failed";
        }
    }

    //

    simpleProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simple.vert" ).c_str() );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!simpleProg->link())
    {
        ////////////////////////////qDebug() << "simple Program linkage failed";
    }

    //

    simpleColorProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simpleColor.vert" ).c_str() );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/simpleColor.frag" ).c_str() );

    if(!simpleColorProg->link())
    {
        ////////////////////////////qDebug() << "simpleColor Program linkage failed";
    }

    //

    prog3D = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/mesh.vert" ).c_str() );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/mesh.frag" ).c_str() );

    if(!simpleProg->link())
    {
        ////////////////////////////qDebug() << "mesh Program linkage failed";
    }

    //

    flatProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat3D.vert" ).c_str() );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat3D.frag" ).c_str() );

    if(!flatProg->link())
    {
        ////////////////////////////qDebug() << "flat3D Program linkage failed";
    }

    //

    flatProgSerial = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2DSerial.vert" ).c_str() );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2DSerial.frag" ).c_str() );

    if(!flatProgSerial->link())
    {
        ////////////////////////////qDebug() << "flat2DSerial Program linkage failed";
    }

    //

    textProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/text.vert" ).c_str() );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/text.frag" ).c_str() );

    if(!textProg->link())
    {
        ////////////////////////////qDebug() << "text Program linkage failed";
    }

    //

    ucProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/quadTex.frag" ).c_str() );

    if(!ucProg->link())
    {
        ////////////////////////////qDebug() << "quadTex Program linkage failed";
    }

    //

    colorLegendProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    colorLegendProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    colorLegendProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/ColorLegend.frag" ).c_str() );

    if(! colorLegendProg->link())
    {
        ////////////////////qDebug() << "legend Program linkage failed";
    }

    //

    flatMeshProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flatMesh.vert" ).c_str() );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flatMesh.frag" ).c_str() );

    if(!flatMeshProg->link())
    {
        ////////////////////////////qDebug() << "flatMesh Program linkage failed";
    }

    //

    heatMapGenProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapGen.vert" ).c_str() );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapGen.frag" ).c_str() );

    if(!heatMapGenProg->link())
    {
        ////////////////////////////qDebug() << "heatmapgen Program linkage failed";
    }

    //

    pointHeatMapGenProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    pointHeatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/pointHeatMapGen.vert" ).c_str() );

    pointHeatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/pointHeatMapGen.frag" ).c_str() );

    if(!pointHeatMapGenProg->link())
    {
        ////////////////////////////qDebug() << "heatmapgen Program linkage failed";
    }

    ////

    heatMapRenderProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapRender.vert" ).c_str() );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapRender.frag" ).c_str() );

    if(!heatMapRenderProg->link())
    {
        ////////////////////////////qDebug() << "flatMesh Program linkage failed";
    }

    ////////////////////////////qDebug() << "returning";

    binDiagnosticProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/binDiagnostic.vert" ).c_str() );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/binDiagnostic.frag" ).c_str() );

    if(!binDiagnosticProgram->link())
    {
        ////////////////////////////qDebug() << "bin diagnostic Program linkage failed";
    }

    /////////

    triangulationMapProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    triangulationMapProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/triangulationmap.vert" ).c_str() );

    triangulationMapProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/triangulationmap.frag" ).c_str() );

    if(!triangulationMapProgram->link())
    {
        qDebug() << "triangulationmap Program linkage failed";
    }

    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        lineMapProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

        lineMapProgram->addShaderFromSourceFile(
            QOpenGLShader::Vertex,
            ( SHADER_PATH + "/LineMap.vert" ).c_str() );


        lineMapProgram->addShaderFromSourceFile(
            QOpenGLShader::Fragment,
            ( SHADER_PATH + "/LineMap.frag" ).c_str() );

        lineMapProgram->addShaderFromSourceFile(
            QOpenGLShader::Geometry,
            ( SHADER_PATH + "/LineMap.geom" ).c_str() );

        if( ! lineMapProgram->link())
        {
            ////////////////////////////qDebug() << "lineMap Program linkage failed";
        }
    }
}

void BALEEN::initGL()
{

    ////////////////////////////qDebug() << "initializing OpenGL";

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor( m_dividerColor.x(), m_dividerColor.y(), m_dividerColor.z(), 1.0 );
    glEnable( GL_BLEND );

    ////////////////////////////qDebug() << "calling compiling the shaders";

    compileShaders();

    ////////////////////////////qDebug() << "initializing renderer";

    m_renderer->init();

    ////////////////////////////qDebug() << "computing pixel angle densities";

    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        m_renderer->computePixelDensityByAngle( binDiagnosticProgram, m_pixelDensityMap );
    }

    //////////////////////////////////////
    // render params

    m_light1.Ia = QVector3D( 0.5f, 0.5f, 0.5f );
    m_light1.Id = QVector3D( 0.5f, 0.5f, 0.5f );
    m_light1.Is = QVector3D( 0.5f, 0.5f, 0.5f );

    m_light1.Ia_scale = 1.0;
    m_light1.Id_scale = 1.0;
    m_light1.Is_scale = 1.0;

    m_light1.direction = QVector3D(.1f, 0.5f, 0.3f );
    m_light1.type = Light::SUN;
    m_light1.position = QVector3D( 0, 10, 100 );

    //

    m_material1.Ka = QVector3D(1.0f, 0.2f, 0.0f );
    m_material1.Kd = QVector3D(1.0f, 0.2f, 0.0f );
    m_material1.Ks = QVector3D(1.0f, 1.0f, 1.0f );
    m_material1.Ka_scale = 0.2;
    m_material1.Kd_scale = 1.2;
    m_material1.Ks_scale = 1.2;
    m_material1.opacity = 1.0;
    m_material1.shininess = 10.0;
    m_material1.overrideAmbient = true;
//

    m_isoLight.Ia = QVector3D( 0.5f, 0.5f, 0.5f );
    m_isoLight.Id = QVector3D( 0.5f, 0.5f, 0.5f );
    m_isoLight.Is = QVector3D( 0.5f, 0.5f, 0.5f );

    m_isoLight.Ia_scale = 1.0;
    m_isoLight.Id_scale = 1.0;
    m_isoLight.Is_scale = 1.0;

    m_isoLight.direction = QVector3D( 0, -0.5f, -1.0f );
    m_isoLight.type = Light::SUN;
    m_isoLight.position = QVector3D( 0, 10, 100 );

    m_isoMaterial.Ka = QVector3D(1.0f, 1.0f, 1.0f );
    m_isoMaterial.Kd = QVector3D(1.0f, 0.2f, 0.0f );
    m_isoMaterial.Ks = QVector3D(1.0f, 1.0f, 1.0f );
    m_isoMaterial.Ka_scale  = 0.2;
    m_isoMaterial.Kd_scale  = 0.9;
    m_isoMaterial.Ks_scale  = 0.2;
    m_isoMaterial.opacity   = 1.0;
    m_isoMaterial.shininess = 10.0;
    m_isoMaterial.overrideAmbient = true;

    ///////////////////////////////////////////

    resizeEvent( NULL );
    const float FACTOR = 255.0;

//    color1 = QVector3D(245,245,245 ) / FACTOR;
//    color2 = QVector3D( 241,110,19 ) / FACTOR;
//    color3 = QVector3D( 217,80,10 )  / FACTOR;
//    color4 = QVector3D( 176,54,3 )    / FACTOR;
//    color5 = QVector3D( 137,30,4 ) / FACTOR;

//    colorn2 = QVector3D( 158,202,225 ) / FACTOR;
//    colorn3 = QVector3D( 66,146,198 )  / FACTOR;
//    colorn4 = QVector3D( 16,87,160 )    / FACTOR;
//    colorn5 = QVector3D( 8,52,110 ) / FACTOR;

    color1 = Vec3< float >(247,247,247 )  / FACTOR;
    color2 = Vec3< float >( 244,165,130 ) / FACTOR;
    color3 = Vec3< float >( 214,96,77 )   / FACTOR;
    color4 = Vec3< float >( 178,24,43 )   / FACTOR;
    color5 = Vec3< float >( 110,10,44 )    / FACTOR;

    colorn2 = Vec3< float >( 146,197,222 ) / FACTOR;
    colorn3 = Vec3< float >( 67,147,195 ) / FACTOR;
    colorn4 = Vec3< float >( 33,102,172 )  / FACTOR;
    colorn5 = Vec3< float >( 10,58,120 )  / FACTOR;

//    m_selectedHist2DButton.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures//buttonIcon2.png", RELATIVE_PATH_PREFIX + "/textures//buttonIcon2Pressed.png" );
//    m_selectedHist3DButton.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures//buttonIcon1.png", RELATIVE_PATH_PREFIX + "/textures//buttonIcon1Pressed.png" );

    /////////////////////////////////////////////////////////////////

    m_isoAORenderScene.init( RELATIVE_PATH_PREFIX, this );

    m_isoRenderObject.init();
    m_isoBoundBox.init();
    m_isoTimeStepIndicator.init();
    m_isoDirectionIndicator.init();

    m_isoAORenderScene.addRenderObject( & m_isoRenderObject );
    m_isoAORenderScene.addRenderObject( & m_isoBoundBox );
    m_isoAORenderScene.addRenderObject( & m_isoTimeStepIndicator );
    m_isoAORenderScene.addRenderObject( & m_isoDirectionIndicator );

    //////

    m_hist2DAORenderScene.init( RELATIVE_PATH_PREFIX, this );

    m_selectedHist3DMesh.init();
    m_hist2DAORenderScene.addRenderObject( & m_selectedHist3DMesh );

    ////////////////////////////////////////////////////////////////

    m_timeAggregateEquationDefinitions.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures/latex/description.png", RELATIVE_PATH_PREFIX + "/textures/latex/description.png" );
    m_timeAggregateEquationDefinitions.resizeByHeight( EQUATION_HEIGHT );


    m_timeAggregateEquationDefinitionsAll.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures/latex/descriptionAll.png", RELATIVE_PATH_PREFIX + "/textures/latex/descriptionAll.png" );
    m_timeAggregateEquationDefinitionsAll.resizeByHeight( EQUATION_HEIGHT );

    m_phasePlotCache.initGL();
    m_phasePlotCache.setAngleToPixelDensityTexture( m_pixelDensityMap );

    m_phaseVAO = std::unique_ptr<QOpenGLVertexArrayObject>(new QOpenGLVertexArrayObject());
    m_phaseVBO = std::unique_ptr<QOpenGLBuffer>(new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer ) );
    m_phaseVBO->setUsagePattern( QOpenGLBuffer::DynamicCopy);
    m_phaseVAO->create();
    m_phaseVBO->create();

    m_triangulationTF.create();
    m_phasePlotTFAll.create();
    m_phasePlotTFRegion.create();
    m_phasePlotTFRegionBin.create();
    m_phasePlotTFWeightBin.create();

    loadTF( RELATIVE_PATH_PREFIX + "/TF//diverging//bwr.txt", m_triangulationTF );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFAll );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFRegion );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFRegionBin );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFWeightBin );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//diverging//bwr.txt", m_histTF );
}

void BALEEN::renderParticleTrace3D(
    const std::vector< std::string > & attrs,
    const std::vector< unsigned int > & highlightIndices,
    const TimeSeriesDefinition & tsrs,
    bool preserveAspect
)
{
    if( ! m_sampler.hasPointSelection() )
    {
        return;
    }

    const unsigned int first = tsrs.firstIdx;
    const unsigned int last = tsrs.lastIdx;
    const unsigned int stride = tsrs.idxStride;
    int windowSize = ( last - first ) / stride + 1;

    static std::vector< Vec3< float > > spathLines;
    static std::vector< unsigned int > indices;
    static std::vector< int > breaks;

    spathLines.clear();
    breaks.clear();
    indices.clear();

    const int SZ = highlightIndices.size();
    const std::string & ptype = m_particleCombo.selectedItemText();

    spathLines.resize( highlightIndices.size() * windowSize );
    breaks.resize( spathLines.size() );
    indices.resize( spathLines.size() );

    Vec2< double > xRange = m_dataSetManager->particleDataManager().variableRange( ptype, attrs[ 0 ] );
    Vec2< double > yRange = m_dataSetManager->particleDataManager().variableRange( ptype, attrs[ 1 ] );
    Vec2< double > zRange = m_dataSetManager->particleDataManager().variableRange( ptype, attrs[ 2 ] );

    double xWidthd2 = ( xRange.b() - xRange.a() ) / 2.0;
    double yWidthd2 = ( yRange.b() - yRange.a() ) / 2.0;
    double zWidthd2 = ( zRange.b() - zRange.a() ) / 2.0;

    double xN = ( xRange.b() - xRange.a() );
    double yN = ( yRange.b() - yRange.a() );
    double zN = ( zRange.b() - zRange.a() );

    if( preserveAspect )
    {
        double mx = std::max( xWidthd2, std::max( yWidthd2, zWidthd2 ) );
        xN = mx;
        yN = mx;
        zN = mx;
    }

    #pragma omp parallel for
    for ( unsigned int t = first; t <= last; t += stride )
    {
        const std::vector< float > & x = *( m_dataSetManager->particleDataManager().values( t, ptype, attrs[ 0 ] ) );
        const std::vector< float > & y = *( m_dataSetManager->particleDataManager().values( t, ptype, attrs[ 1 ] ) );
        const std::vector< float > & z = *( m_dataSetManager->particleDataManager().values( t, ptype, attrs[ 2 ] ) );

        int ti = first - t / tsrs.idxStride;

        for ( int i = 0; i < SZ; ++i )
        {
            const unsigned int index = highlightIndices[ i ];

            spathLines[ windowSize*i + ti ] =
                Vec3< float >(
                    ( x[ index ] - ( xRange.a() + xWidthd2 ) ) / xN,
                    ( y[ index ] - ( yRange.a() + yWidthd2 ) ) / yN,
                    ( z[ index ] - ( zRange.a() + zWidthd2 ) ) / zN );

            indices[ windowSize*i + ti ] = windowSize*i + ti;
            breaks[  windowSize*i + ti ] = t == first;
        }
    }

    QMatrix4x4 M;
    M.setToIdentity();

    Material mtl = m_material1;
    mtl.Ka = QVector3D( .9, 0.8, 1 );
    mtl.Kd = QVector3D( .9, 0.8, 1 );
    mtl.Ks = QVector3D( .9, 0.8, 1 );
    mtl.Ka_scale = 0.1;
    mtl.Kd_scale = 1.0;
    mtl.Ks_scale = 1.0;

    glViewport( m_auxViewPort.offsetX(), m_auxViewPort.offsetY(), m_auxViewPort.width(), m_auxViewPort.height() - m_samplerWidget.controlPanel.size().y() );
    auxViewCamera.setAspectRatio( m_auxViewPort.width() / ( ( double ) ( m_auxViewPort.height() -  m_samplerWidget.controlPanel.size().y() ) ) );
    auxViewCamera.setCenter( QVector3D( 0, 0, 0 ) );

    glClear( GL_DEPTH_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );

    std::vector< Vec3< float > > boundingBox;
    std::vector< Vec3< float > > negXAxis;
    std::vector< Vec3< float > > negYAxis;
    std::vector< Vec3< float > > xAxis;
    std::vector< Vec3< float > > yAxis;
    std::vector< Vec3< float > > zAxis;
    std::vector< Vec3< float > > negZAxis;

    // near square
    boundingBox.push_back( Vec3< float >( -1, -1, -1 ) );
    boundingBox.push_back( Vec3< float >( -1,  1, -1 ) );

    boundingBox.push_back( Vec3< float >( -1, 1, -1 ) );
    boundingBox.push_back( Vec3< float >( -1, 1,  1 ) );

    boundingBox.push_back( Vec3< float >( -1, 1, 1 ) );
    boundingBox.push_back( Vec3< float >( -1, -1, 1 ) );

    boundingBox.push_back( Vec3< float >( -1, -1, 1 ) );
    boundingBox.push_back( Vec3< float >( -1, -1, -1 ) );

    // far square
    boundingBox.push_back( Vec3< float >( 1, -1, -1 ) );
    boundingBox.push_back( Vec3< float >( 1,  1, -1 ) );

    boundingBox.push_back( Vec3< float >( 1, 1, -1 ) );
    boundingBox.push_back( Vec3< float >( 1, 1,  1 ) );

    boundingBox.push_back( Vec3< float >( 1, 1, 1 ) );
    boundingBox.push_back( Vec3< float >( 1, -1, 1 ) );

    boundingBox.push_back( Vec3< float >( 1, -1, 1 ) );
    boundingBox.push_back( Vec3< float >( 1, -1, -1 ) );

    // time depth lines
    boundingBox.push_back( Vec3< float >( -1, 1, 1 ) );
    boundingBox.push_back( Vec3< float >(  1, 1, 1 ) );

    boundingBox.push_back( Vec3< float >( -1, -1, 1 ) );
    boundingBox.push_back( Vec3< float >(  1, -1, 1 ) );

    boundingBox.push_back( Vec3< float >( -1, -1, -1 ) );
    boundingBox.push_back( Vec3< float >(  1, -1, -1 ) );

    boundingBox.push_back( Vec3< float >( -1, 1, -1 ) );
    boundingBox.push_back( Vec3< float >(  1, 1, -1 ) );

    QMatrix4x4 MVP = auxViewCamera.matProj() * auxViewCamera.matView( 1.0 );

    glLineWidth( 2.0 );

    m_renderer->renderPrimitivesFlat(
        boundingBox,
        flatProg,
        QVector4D( 0, 0, 0, 0.4 ),
        MVP,
        GL_LINES );

    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        m_renderer->renderPathTubes( spathLines, indices, breaks, mtl, m_light1, auxViewCamera, 0.01f, tubePathProgram );
    }

    glDisable( GL_CULL_FACE );
    glDisable( GL_DEPTH_TEST );
}

void BALEEN::renderParticleTrace2D(
    const std::vector< std::unique_ptr< std::vector< float > > > & x,
    const std::vector< std::unique_ptr< std::vector< float > > > & y,
    const QMatrix4x4 & M )
{
    const TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                      : m_dataSetManager->distributionManager().currentTimeSeries();

    unsigned int first = tsrs.firstIdx;
    unsigned int last  = tsrs.lastIdx;

    const int windowSize =  m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                            : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    static std::vector< Vec2< float > > spathLines;

    if( spathLines.size() != ( windowSize * 2 * ( *x[ 0 ] ).size() - 2 ) )
    {
        spathLines.resize( windowSize * 2 * ( *x[ 0 ] ).size() - 2 );

        #pragma omp parallel for
        for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
        {
            int ti = ( t - tsrs.firstIdx ) / tsrs.idxStride;

            const std::vector< float > & xPos = *( x[ t ] );
            const std::vector< float > & yPos = *( y[ t ] );

            for ( unsigned i = 0, end = xPos.size(); i < end; ++i )
            {

                const Vec2< float > pos2( xPos[ i ], yPos[ i ] );

                if ( t == first )
                {
                    spathLines[ i*2*windowSize + ti*2 ] = pos2;
                }
                else if ( t == last )
                {
                    spathLines[ i*2*windowSize + ti*2-1 ] = pos2;
                }
                else
                {
                    spathLines[ i*2*windowSize + ti*2-1 ] = pos2;
                    spathLines[ i*2*windowSize + ti*2   ] = pos2;
                }
            }
        }
    }

    glLineWidth(3.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 0.2, 0.2, 0.2, 1 ),
        M,
        GL_LINES
    );


    glLineWidth(1.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 1, 1, 1, 1.0 ),
        M,
        GL_LINES
    );
}

void BALEEN::renderParticleTrace2D(
    const std::vector< std::unique_ptr< std::vector< float > > > & x,
    const std::vector< std::unique_ptr< std::vector< float > > > & y,
    const std::vector< unsigned int > & highlightIndices,
    const QMatrix4x4 & M )
{
    const TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                      : m_dataSetManager->distributionManager().currentTimeSeries();

    unsigned int first = tsrs.firstIdx;
    unsigned int last  = tsrs.lastIdx;

    const int windowSize =  m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                            : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    static std::vector< Vec2< float > > spathLines;
    spathLines.resize( windowSize * 2 * highlightIndices.size() - 2 );

    #pragma omp parallel for
    for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
    {
        int ti = ( t - tsrs.firstIdx ) / tsrs.idxStride;

        const std::vector< float > & xPos = *( x[ t ] );
        const std::vector< float > & yPos = *( y[ t ] );

        const int FPSZ = static_cast<int> ( highlightIndices.size() );
        for ( int i = 0; i < FPSZ; ++i )
        {
            unsigned int index = highlightIndices[ i ];

            const Vec2< float > pos2( xPos[ index ], yPos[ index ] );

            if ( t == first )
            {
                spathLines[ i*2*windowSize + ti*2 ] = pos2;
            }
            else if ( t == last )
            {
                spathLines[ i*2*windowSize + ti*2-1 ] = pos2;
            }
            else
            {
                spathLines[ i*2*windowSize + ti*2-1 ] = pos2;
                spathLines[ i*2*windowSize + ti*2   ] = pos2;
            }
        }
    }

    glLineWidth(4.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 0.18 * 1.5, 0.22 * 1.5, 0.02 * 1.5, 1 ),
        M,
        GL_LINES
    );

    glLineWidth(2.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( .88, 1.0, .7, 0.07 ),
        M,
        GL_LINES
    );
}

void BALEEN::renderParticleTrace2D(
    const std::vector< std::unique_ptr< std::vector< float > > > & x,
    const std::vector< std::unique_ptr< std::vector< float > > > & y,
    const std::vector< std::unique_ptr< std::vector< float > > > & _real,
    const std::vector< unsigned int > & highlightIndices,
    const QMatrix4x4 & M )
{
    const TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                      : m_dataSetManager->distributionManager().currentTimeSeries();

    unsigned int first = tsrs.firstIdx;
    unsigned int last  = tsrs.lastIdx;

    const int windowSize =  m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().numLoadedTimeSteps()
                            : m_dataSetManager->distributionManager().numLoadedTimeSteps();

    static std::vector< Vec2< float > > spathLines;
    spathLines.clear();
    spathLines.reserve( windowSize * 2 * highlightIndices.size() - 2 );

    for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
    {
        const std::vector< float > & xPos = *( x[ t ] );
        const std::vector< float > & yPos = *( y[ t ] );
        const std::vector< float > & real = *( _real[ t ] );

        const std::vector< float > & xPos2 = *( x[ t + 1 ] );
        const std::vector< float > & yPos2 = *( y[ t + 1] );
        const std::vector< float > & real2 = *( _real[ t + 1 ] );

        const int FPSZ = static_cast<int> ( highlightIndices.size() );
        for ( int i = 0; i < FPSZ; ++i )
        {
            unsigned int index = highlightIndices[ i ];

            if( real[ index ] < .5 || real2[ index ] < .5 )
            {
                continue;
            }

            const Vec2< float > pos1( xPos[ index ], yPos[ index ] );
            const Vec2< float > pos2( xPos2[ index ], yPos2[ index ] );

            spathLines.push_back( pos1 );
            spathLines.push_back( pos2 );
        }
    }


    glLineWidth(4.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 0, 0.2, 0.05, 1 ),
        M,
        GL_LINES
    );

    glLineWidth(2.0);
    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 0, 1.0, .35, 1 ),
        M,
        GL_LINES
    );

    m_renderer->renderPrimitivesFlat(
        spathLines,
        cartesianProg,
        QVector4D( 0, 0, 0, 0.013 ),
        M,
        GL_LINES
    );
}

BaseHistDefinition * BALEEN::validateCurrentDefinition( int dataTypeEnum, const std::string & ptype, const std::string & pdfKey )
{
    if( m_datasetCombo.numItems() <= 0 )
    {
        return nullptr;
    }

    if( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.numItems() <= 0 )
    {
        return nullptr;
    }

    if( m_particleCombo.numItems() <= 0 )
    {
        return nullptr;
    }

    if( dataTypeEnum == PopulationDataType::PARTICLES )
    {
        if( m_dataSetManager->particleDataManager().numLoadedTimeSteps() <= m_selectedTimeStep )
        {
            return nullptr;
        }

        const auto it = m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( ptype );
        if( it == m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.end() )
        {
            return nullptr;
        }

        const auto & d_it = it->second.find( pdfKey );
        if( d_it == it->second.end() )
        {
            return nullptr;
        }

//        if( d_it->second.partitionSpace.size() == 2 )
//        {
//            const std::unique_ptr< std::vector< float > > & xAttr = m_dataSetManager->particleDataManager().values( timeStep, ptype, d_it->second.partitionSpace[ 0 ] );
//            const std::unique_ptr< std::vector< float > > & yAttr = m_dataSetManager->particleDataManager().values( timeStep, ptype, d_it->second.partitionSpace[ 1 ] );
//            if( xAttr == nullptr || yAttr == nullptr )
//            {
//                return nullptr;
//            }
//        }
//        else
//        {
//            return nullptr;
//        }

        return & d_it->second;
    }
    else if ( dataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        if( m_dataSetManager->distributionManager().numLoadedTimeSteps() <= m_selectedTimeStep )
        {
            return nullptr;
        }

        const auto it = m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.find( ptype );
        if( it == m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.end() )
        {
            return nullptr;
        }

        const auto & d_it = it->second.find( pdfKey );
        if( d_it == it->second.end() )
        {
            return nullptr;
        }

//        if( d_it->second.partitionSpace.size() != 2 )
//        {
//            return nullptr;
//        }

//        const std::unique_ptr< std::vector< float > >  & distributionValues = m_dataSetManager->distributionManager().distributions( ptype, pdfKey, timeStep );
//        if( distributionValues == nullptr )
//        {
//            return nullptr;
//        }

        return & d_it->second;
    }

    return nullptr;
}

TimeSeriesDefinition * BALEEN::getTimeSeries( const std::string & ptype )
{
    std::string seriesKey = m_timeSeriesCombo.selectedItemText();

    const auto it = m_currentConfigurations.timeSeriesDefinitions().find( ptype );
    if( it == m_currentConfigurations.timeSeriesDefinitions().end() )
    {
        return nullptr;
    }

    const auto & d_it = it->second.find( seriesKey );
    if( d_it == it->second.end() )
    {
        return nullptr;
    }

    return & d_it->second;
}

LinkedViewDefinition * BALEEN::getLinkedViewDefinition( int dataTypeEnum, const std::string & ptype, const std::string & pdfKey, int viewType )
{
    if( dataTypeEnum == PopulationDataType::PARTICLES )
    {
        const std::string key = m_phasePlotCombo.selectedItemText();
        auto it = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype );
        if( it == m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.end() )
        {
            return nullptr;
        }
        auto it2 = it->second.find( key );
        if( it2 == it->second.end() )
        {
            return nullptr;
        }
        return & it2->second;
    }
    return nullptr;
}

TimePlotDefinition * BALEEN::getTimePlotDefinition( int dataTypeEnum, const std::string & ptype, const std::string & pdfKey )
{
    const std::string & key = m_timePlotCombo.selectedItemText();
    auto it = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( ptype );
    if( it == m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.end() )
    {
        return nullptr;
    }
    auto it2 = it->second.find( key );
    if( it2 == it->second.end() )
    {
        return nullptr;
    }
    return & it2->second;
}

std::set< std::string > BALEEN::getVariablesInUse(
    BaseHistDefinition * histDef,
    SpacePartitioningDefinition * spacePartitioning,
    LinkedViewDefinition * linkedViewDef,
    TimePlotDefinition * timePlotDef,
    int linkedViewType,
    int dataTypeEnum )
{
    std::set< std::string > variablesInUse = {};

    if( dataTypeEnum == PopulationDataType::PARTICLES )
    {
        if( histDef != nullptr )
        {
            for( const std::string & vKey : histDef->binningSpace )
            {
                variablesInUse.insert( vKey );
            }
            if( dynamic_cast< HistogramDefinition * >( histDef )->weight != "1" )
            {
                variablesInUse.insert( dynamic_cast< HistogramDefinition * >( histDef )->weight );
            }
        }
        if( spacePartitioning != nullptr )
        {
            for( const std::string & vKey : spacePartitioning->attributes )
            {
                variablesInUse.insert( vKey );
            }
        }
        if( timePlotDef != nullptr )
        {
            variablesInUse.insert( timePlotDef->attr );
        }
        if( linkedViewDef != nullptr )
        {
            for( const std::string & vKey : dynamic_cast< PhasePlotDefinition * >( linkedViewDef )->attrs )
            {
                variablesInUse.insert( vKey );
            }
        }
    }
    else if ( dataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        variablesInUse.insert( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() );
    }
    return variablesInUse;
}

Vec2< double > BALEEN::getSelectedCellColorValueRange()
{
    int selectedGridCell = m_sampler.getSelectedCellId();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        HistogramDefinition * def = dynamic_cast< HistogramDefinition * >(
                                        validateCurrentDefinition( m_populationDataTypeEnum, m_particleCombo.selectedItemText(), m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText() ) );
        double scale = 1.0;

        if( def != nullptr )
        {
            scale = def->constWeightValue;
        }

        if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global )
        {
            if( def->weightType == HistogramWeightType::VARIABLE )
            {
                return Vec2< double >( -m_maxFrequencyInSelectedTS, m_maxFrequencyInSelectedTS ) * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition() * scale;
            }
            else if ( def->constWeightValue > 0 )
            {
                return Vec2< double >( 0, m_maxFrequencyInSelectedTS ) * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition() * scale;
            }
            else
            {
                return Vec2< double >( -m_maxFrequencyInSelectedTS, 0 ) * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition() * scale;
            }
        }
        else
        {
            double localMax = m_localMaxFrequenciesInSelectedTS.find( selectedGridCell )->second;

            if( def->weightType == HistogramWeightType::VARIABLE )
            {
                return Vec2< double > ( -localMax, localMax ) * scale;
            }
            else if ( def->constWeightValue > 0 )
            {
                return Vec2< double > ( 0, localMax ) * scale;
            }
            else
            {
                return Vec2< double > ( -localMax, 0 ) * scale;
            }
        }
    }
    else
    {
        Vec2< double > result;

        if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global )
        {
            result = Vec2< double >( -m_maxFrequencyInSelectedTS, m_maxFrequencyInSelectedTS ) * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition();
        }

        else
        {
            double localMax = m_localMaxFrequenciesInSelectedTS.find( selectedGridCell )->second;
            result = Vec2< double > ( -localMax, localMax );
        }

        return result * m_gridMergingNormalizationTerm.find( selectedGridCell )->second;
    }
}

int BALEEN::getStepOffset()
{
    std::int32_t firstTStep = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries().firstIdx
                              : m_dataSetManager->distributionManager().currentTimeSeries().firstIdx;

    std::int32_t tsStride = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries().idxStride
                            : m_dataSetManager->distributionManager().currentTimeSeries().idxStride;

    return ( m_selectedTimeStep - firstTStep ) / tsStride;
}

double BALEEN::getSelectedBinFrequency()
{
    float val = 0;
    bool isValid;
    if( m_sampler.getSelectedCellId() >= 0 && m_sampler.getSelectedBinId() >= 0 )
    {
        //qDebug() << "getting value from volume cache at step " << getStepOffset() << " and bin id " << m_sampler.getSelectedBinId();
        val = m_frVolumeCache.get( m_sampler.getSelectedCellId(), isValid ).binAtStep( getStepOffset(), m_sampler.getSelectedBinId() );
        //qDebug() << "return value is  " << val;
    }
    if( ! isValid )
    {
        //qDebug() << "volume was not flagged as valid";
        val = 0;
    }

    return val;
}

void BALEEN::renderDetailView( FrVolume & frVol, BaseHistDefinition * def, int cellSelection, int binSelection )
{
    I2 resolution = getAdjustedHistResolution( { def->resolution[ 0 ], def->resolution[ 1 ] } );

    glViewport(
        m_histogramViewPort.offsetX() + HIST_PAD,
        m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - PANEL_HT,
        m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN ),
        m_histogramViewPort.width() - ( HIST_PAD + HIST_MARGIN )
    );

    if( m_selectedHist2DButton.isPressed() )
    {
        glLineWidth( 1 );
        m_renderer->renderPrimitivesFlat(
            m_selectedHistGridLines,
            cartesianProg,
            QVector4D( 1.0, 1.0, 1.0, .5 ),
            QMatrix4x4(),
            GL_LINES );
    }

    if( cellSelection >= 0 )
    {
        if( m_selectedHist2DButton.isPressed() )
        {
            m_renderer->renderPrimitivesColored(
                m_selectedHistogramVertices,
                m_selectedHistogramColors,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
        }
        else
        {
            glViewport(
                m_histogramViewPort.offsetX(),
                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) - PANEL_HT,
                m_histogramViewPort.width(),
                m_histogramViewPort.width()
            );

            if( std::abs( int( m_histogramViewPort.width() ) - m_hist2DAORenderScene.xSize ) > 2
                    || std::abs( int( m_histogramViewPort.width() ) - m_hist2DAORenderScene.ySize ) > 2 )
            {
                m_hist2DAORenderScene.setViewPortSize( m_histogramViewPort.width(), m_histogramViewPort.width() );
            }

            renderHistogramAsHeightField(
                frVol.atStep( getStepOffset() ), resolution );

            glViewport(
                m_histogramViewPort.offsetX(),
                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) - PANEL_HT,
                m_histogramViewPort.width(),
                m_histogramViewPort.width()
            );
        }
    }

    if( binSelection >= 0 )
    {
        if( m_selectedHist2DButton.isPressed() )
        {
            int cellId = m_sampler.getSelectedCellId();
            int binId = m_sampler.getSelectedBinId();
            if( binId >= 0 && cellId >= 0 )
            {
                //const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();
                std::vector< Vec2< float > > binVerts( 8 );
                binVerts.clear();
                TN::HIST::appendCellOutline2D(
                    binId,
                    //                gridCorners[ cellId * 4 + 0 ],
                    //                gridCorners[ cellId * 4 + 1 ],
                    //                gridCorners[ cellId * 4 + 2 ],
                    //                gridCorners[ cellId * 4 + 3 ],
                    Vec2< float >(  1,  1 ),
                    Vec2< float >(  1, -1 ),
                    Vec2< float >( -1,  1 ),
                    Vec2< float >( -1, -1 ),
                    resolution,
                    binVerts );

                glLineWidth( 4 );
                glPointSize( 4 );
                m_renderer->renderPrimitivesFlat( binVerts, cartesianProg, QVector4D( 0, 0, 0, 1.0 ), QMatrix4x4(), GL_LINES );
                m_renderer->renderPrimitivesFlat( binVerts, cartesianProg, QVector4D( 0, 0, 0, 1.0 ), QMatrix4x4(), GL_POINTS );

                glLineWidth( 1 );
                glPointSize( 1 );
                m_renderer->renderPrimitivesFlat( binVerts, cartesianProg, QVector4D( 1, 1, .1, 1.0 ), QMatrix4x4(), GL_LINES );
                m_renderer->renderPrimitivesFlat( binVerts, cartesianProg, QVector4D( 1, 1, .1, 1.0 ), QMatrix4x4(), GL_POINTS );
            }
        }
    }

    if( m_selectedHist2DButton.isPressed() )
    {
        std::vector< Vec2< float > > negXAxis( 2 );
        std::vector< Vec2< float > > posXAxis( 2 );
        std::vector< Vec2< float > > negZAxis( 2 );
        std::vector< Vec2< float > > posZAxis( 2 );

        negZAxis[ 0 ] = Vec2< float >( 0,  0  );
        negZAxis[ 1 ] = Vec2< float >( 0, -1  );

        posZAxis[ 0 ] = Vec2< float >( 0, 0 );
        posZAxis[ 1 ] = Vec2< float >( 0, 1 );

        negXAxis[ 0 ] = Vec2< float >( -1, 0 );
        negXAxis[ 1 ] = Vec2< float >(  0, 0 );

        posXAxis[ 0 ] = Vec2< float >( 0, 0 );
        posXAxis[ 1 ] = Vec2< float >( 1, 0 );

        std::vector< Vec2< float > > orientationGlyph1( 3 );

        orientationGlyph1[ 0 ] = Vec2< float >( .8, 1.0 );
        orientationGlyph1[ 1 ] = Vec2< float >( 1.0, 1.0 );
        orientationGlyph1[ 2 ] = Vec2< float >( 1.0, .8 );

        std::vector< Vec2< float > > orientationGlyph2( 3 );
        orientationGlyph2[ 0 ] = Vec2< float >(.92, .96 );
        orientationGlyph2[ 1 ] = Vec2< float >(.96, .96 );
        orientationGlyph2[ 2 ] = Vec2< float >(.96, .92 );

        m_renderer->renderPrimitivesFlat(orientationGlyph1, cartesianProg, QVector4D( 0.0,  0.0, 0.0, 1.0 ), QMatrix4x4(), GL_TRIANGLES );
        m_renderer->renderPrimitivesFlat(orientationGlyph2, cartesianProg, QVector4D( .4, .9, .6, 1.0 ), QMatrix4x4(), GL_TRIANGLES );

        glLineWidth(5);
        m_renderer->renderPopSquare( simpleColorProg );

        if( cellSelection >= 0 )
        {
            glLineWidth( 3 );
            m_renderer->renderPrimitivesFlat(
                std::vector< Vec2< float > > (
            {
                Vec2< float >( -1,  1 ), Vec2< float >(  1,  1 ),
                Vec2< float >(  1,  1 ), Vec2< float >(  1, -1 ),
                Vec2< float >(  1, -1 ), Vec2< float >( -1, -1 ),
                Vec2< float >( -1, -1 ), Vec2< float >( -1,  1 )
            } ),
            cartesianProg,
            QVector4D( .1, .1, 0, 1.0 ),
            QMatrix4x4(),
            GL_LINES );

            glLineWidth( 2.5 );
            m_renderer->renderPrimitivesFlat(
                std::vector< Vec2< float > > (
            {
                Vec2< float >( -1,  1 ), Vec2< float >(  1,  1 ),
                Vec2< float >(  1,  1 ), Vec2< float >(  1, -1 ),
                Vec2< float >(  1, -1 ), Vec2< float >( -1, -1 ),
                Vec2< float >( -1, -1 ), Vec2< float >( -1,  1 )
            } ),
            cartesianProg,
            QVector4D( m_boxSelectionColor.x(), m_boxSelectionColor.y(), m_boxSelectionColor.z(), 1.0 ),
            QMatrix4x4(),
            GL_LINES );

            m_renderer->renderPopSquare( simpleColorProg );
        }
    }

    const int LEGEND_HEIGHT = 30;
    const int LEGEND_PAD = 20;

    glViewport(
        m_histogramViewPort.offsetX() + LEGEND_PAD,
        m_histogramViewPort.offsetY() + LEGEND_PAD * 4 + LEGEND_HEIGHT - PANEL_HT,
        m_histogramViewPort.width() - LEGEND_PAD * 2,
        LEGEND_HEIGHT
    );

    m_renderer->renderPrimitivesColored(
        m_colorLegendVerts,
        m_colorLegendColors,
        simpleColorProg,
        QMatrix4x4(),
        GL_TRIANGLES
    );

    glLineWidth(5);
    m_renderer->renderPopSquare( simpleColorProg );

    glViewport(
        0,
        0,
        scaledWidth(),
        scaledHeight()
    );

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            m_colorLegendLabel,
                            m_histogramViewPort.offsetX() + LEGEND_PAD,
                            m_histogramViewPort.offsetY() + LEGEND_PAD*4 + LEGEND_HEIGHT * 2 + 15 - PANEL_HT,
                            1.0,
                            m_textColor );

    ///////

    if( m_selectedHist2DButton.isPressed() )
    {
        std::string aX, aY;
        std::string vMinX, vMaxX, vMinY, vMaxY, vMidX, vMidY;

        std::string ptype = m_particleCombo.selectedItemText();
        std::string dtype = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();

        float YOFF1, XOFF1;

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            const auto & def = m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( ptype )->second.find( dtype ) ->second;

            aX = def.binningSpace[ 0 ];
            aY = def.binningSpace[ 1 ];

            Vec2< float > valueRangeX = m_sampler.getAdjustedValueRanges( def.valueRange[ 0 ], m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition() );
            Vec2< float > valueRangeY = m_sampler.getAdjustedValueRanges( def.valueRange[ 1 ], m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.sliderPosition() );

            vMinX = to_string_with_precision( valueRangeX.a() );
            vMaxX = to_string_with_precision( valueRangeX.b() );
            vMidX = to_string_with_precision( ( valueRangeX.a() + valueRangeX.b() ) / 2.0 );

            vMinY = to_string_with_precision( valueRangeY.a() );
            vMaxY = to_string_with_precision( valueRangeY.b() );
            vMidY = to_string_with_precision( ( valueRangeY.a() + valueRangeY.b() ) / 2.0 );

            XOFF1 = 0;
            YOFF1 = 0;
        }
        else
        {
            const auto & def = m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.find( ptype )->second.find( dtype )->second;

            aX = def.binningSpace[ 0 ];
            aY = def.binningSpace[ 1 ];

            vMinX = to_string_with_precision( def.valueRange[ 0 ].a() );
            vMaxX = to_string_with_precision( def.valueRange[ 0 ].b() );
            vMidX = to_string_with_precision( ( def.valueRange[ 0 ].a() + def.valueRange[ 0 ].b() ) / 2.0 );

            vMinY = to_string_with_precision( def.valueRange[ 1 ].a() );
            vMaxY = to_string_with_precision( def.valueRange[ 1 ].b() );
            vMidY = to_string_with_precision( ( def.valueRange[ 1 ].a() + def.valueRange[ 1 ].b() ) / 2.0 );

            XOFF1 = 40;
            YOFF1 = 40;
        }

        if( aX == "vperp" )
        {
            aX = "|vperp|";
        }
        if( aY == "vperp" )
        {
            aY = "|vperp|";
        }

        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                aX,
                                m_histogramViewPort.offsetX() + m_histogramViewPort.width() / 2.0,
                                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - 44 - PANEL_HT,
                                1.0,
                                m_textColor );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                vMinX,
                                m_histogramViewPort.offsetX() + HIST_PAD,
                                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - 20 - PANEL_HT,
                                1.0,
                                m_textColor );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                vMidX,
                                m_histogramViewPort.offsetX() + m_histogramViewPort.width() / 2.0 - 10 + XOFF1 * .7,
                                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - 20 - PANEL_HT,
                                1.0,
                                m_textColor );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                vMaxX,
                                m_histogramViewPort.offsetX() + m_histogramViewPort.width()
                                - vMaxX.size()*7.5,
                                m_histogramViewPort.offsetY() + ( m_histogramViewPort.height() - m_histogramViewPort.width() ) + HIST_PAD - 20 - PANEL_HT,
                                1.0,
                                m_textColor );

        ////////

        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                aY,
                                m_histogramViewPort.offsetX() + HIST_PAD - 20,
                                m_histogramViewPort.offsetY() + m_histogramViewPort.height() - m_histogramViewPort.width() / 2.0 - PANEL_HT,
                                1.0,
                                m_textColor,
                                true );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                vMaxY,
                                m_histogramViewPort.offsetX() + HIST_PAD - 5,
                                m_histogramViewPort.offsetY() + m_histogramViewPort.height()
                                - PANEL_HT - vMaxY.size()*7.5,
                                1.0,
                                m_textColor,
                                true );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                ( m_populationDataTypeEnum == PopulationDataType::PARTICLES ? vMidY : vMinY ),
                                m_histogramViewPort.offsetX() + HIST_PAD - 5,
                                m_histogramViewPort.offsetY() + m_histogramViewPort.height() - m_histogramViewPort.width() / 2.0 + YOFF1 / 2.0 - PANEL_HT,
                                1.0,
                                m_textColor,
                                true );

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    vMinY,
                                    m_histogramViewPort.offsetX() + HIST_PAD - 15,
                                    m_histogramViewPort.offsetY() + m_histogramViewPort.height() - m_histogramViewPort.width() + 60 - PANEL_HT,
                                    1.0,
                                    m_textColor,
                                    true );
        }
    }

    if( cellSelection >= 0 || ( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global ) )
    {
        Vec2< double > rng = getSelectedCellColorValueRange();

        std::string rngA = to_string_with_precision( rng.a() );
        std::string midStr = "0";
        std::string rngB = to_string_with_precision( rng.b() );

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( dynamic_cast< HistogramDefinition * >( def )->weightType != HistogramWeightType::VARIABLE )
            {
                if( dynamic_cast< HistogramDefinition * >( def )->constWeightValue < 0 )
                {
                    rngA = to_string_with_precision( -rng.a() );
                    midStr = to_string_with_precision( -rng.a() / 2.0 );
                    rngB = "0";
                }
                else
                {
                    rngA = "0";
                    midStr = to_string_with_precision( rng.b() / 2.0 );
                }
            }
        }

        if( m_sampler.getSelectedBinId() >= 0 )
        {
            std::string sCValStr = to_string_with_precision( getSelectedBinFrequency() );
            //qDebug() << " value is " <<  getSelectedBinFrequency() << " string is " << sCValStr.c_str();
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    sCValStr,
                                    m_histogramViewPort.offsetX() + m_histogramViewPort.width() - 8 - sCValStr.size()*8,
                                    m_histogramViewPort.offsetY() + m_histogramViewPort.height() - PANEL_HT - 6,
                                    1.0,
                                    m_textColor );
        }

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                rngA,
                                m_histogramViewPort.offsetX() + LEGEND_PAD,
                                m_histogramViewPort.offsetY() + LEGEND_PAD * 4 - PANEL_HT + 10,
                                1.0,
                                m_textColor );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                midStr,
                                m_histogramViewPort.offsetX() + LEGEND_PAD + ( m_histogramViewPort.width() - LEGEND_PAD * 2 ) / 2.0 - 3.25,
                                m_histogramViewPort.offsetY() + LEGEND_PAD * 4 - PANEL_HT + 10,
                                1.0,
                                m_textColor );

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                rngB,
                                m_histogramViewPort.offsetX() + LEGEND_PAD + ( m_histogramViewPort.width() - LEGEND_PAD * 2 ) - rngB.size() * 7.5,
                                m_histogramViewPort.offsetY() + LEGEND_PAD * 4 - PANEL_HT + 10,
                                1.0,
                                m_textColor );

        //Contours value location over color legend

        glViewport(
            m_histogramViewPort.offsetX() + LEGEND_PAD,
            m_histogramViewPort.offsetY() + LEGEND_PAD * 4 + LEGEND_HEIGHT - PANEL_HT - 1,
            m_histogramViewPort.width() - LEGEND_PAD * 2,
            LEGEND_HEIGHT
        );

        float transLate = 0;
        float scale = 1.0;

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( dynamic_cast< HistogramDefinition * >( def )->weightType != HistogramWeightType::VARIABLE )
            {
                if( dynamic_cast< HistogramDefinition * >( def )->weightType == HistogramWeightType::CONSTANT &&
                        dynamic_cast< HistogramDefinition * >( def )->constWeightValue < 0 )
                {
                    transLate = 3;
                    scale = 2.0;
                }
                else
                {
                    transLate = -1;
                    scale = 2.0;
                }
            }
        }

        std::vector< Vec2< float > > contourMarkers( m_currentContourLevels.size() * 2 );
        for( unsigned i = 0, end = m_currentContourLevels.size(); i < end; ++i )
        {
            float r = m_currentContourLevels[ i ] * scale  + transLate;
            if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global && m_sampler.getSelectedCellId() >= 0 )
            {
                r /= m_maxFrequencyInSelectedTS * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition();
                if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
                {
                    if( m_gridMergingNormalizationTerm.find( m_sampler.getSelectedCellId() ) != m_gridMergingNormalizationTerm.end() )
                    {
                        r *= m_gridMergingNormalizationTerm.find( m_sampler.getSelectedCellId() )->second;
                    }
                }
            }
            contourMarkers[ i*2     ] = Vec2< float >( r,  1 );
            contourMarkers[ i*2 + 1 ] = Vec2< float >( r, -1 );
        }

        glLineWidth( 2.0 );
        m_renderer->renderPrimitivesFlat(
            contourMarkers,
            cartesianProg,
            QVector4D( 0,0,0, m_isoValueSlider.isPressed() ? 1.0 : .5 ),
            QMatrix4x4(),
            GL_LINES
        );
    }
}

void BALEEN::renderMiniMap( int cellSelection, int binSelection, bool userIsPanning )
{
    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    std::string ptype = m_particleCombo.selectedItemText();
    std::string pdfKey = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();

    const float MARGIN_LEFT   = 30.0;
    const float MARGIN_BOTTOM = 30.0;

    ////////////////////////qDebug() << ( m_populationDataTypeEnum != PopulationDataType::PARTICLES );

    const BaseHistDefinition & def = * ( m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                         dynamic_cast< BaseHistDefinition * >( & m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.find( ptype )->second.find( pdfKey )->second )
                                         : dynamic_cast< BaseHistDefinition * >( &  m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.find( ptype )->second.find( pdfKey )->second ) );

    const SpacePartitioningDefinition & spatialPartitioning = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
            m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second
            : m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second;

    ////////////////////////qDebug() << "got the definition";

    if( spatialPartitioning.attributes.size() == 2 )
    {
        ////////////////////////qDebug() << "partition space is size 2";

        double conversionRatio = ( m_upperLeftWindowCornerWorldSpace.y() - m_lowerLeftWindowCornerWorldSpace.y() ) / ( ( double )scaledHeight() );

        const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();
        int cell = m_sampler.getSelectedCellId();

        Vec2< float > a( -std::numeric_limits< float >::max(), std::numeric_limits< float >::max() );
        Vec2< float > d( a );

        Vec2< float > b( a );
        Vec2< float > c( a );

        Vec2< float > mins(   m_contextBoundingBox.first  );
        Vec2< float > maxes ( m_contextBoundingBox.second );

        QMatrix4x4 M;
        QMatrix4x4 GridTR;
        QMatrix4x4 PartTR;

        float dx = ( m_miniMapViewPort.width()  -   MARGIN_LEFT ) * conversionRatio;
        float dy = ( m_miniMapViewPort.height() - MARGIN_BOTTOM ) * conversionRatio;

        float scaleFactor = 1.0;

        float dataAspect = m_miniMapSizeWorldSpace.x() / m_miniMapSizeWorldSpace.y();
        float viewAspect = dx / dy;

        ////////////////////////qDebug() << "view aspect = " << viewAspect;

        // then scale just enough to fit the width of the data in the view
        if( dataAspect >= viewAspect )
        {
            scaleFactor = dx / ( m_miniMapSizeWorldSpace.x() );
        }
        // scale just enough to fit the height of the data in the view
        else
        {
            scaleFactor = dy / ( m_miniMapSizeWorldSpace.y() );
        }

        scaleFactor *= .93;


        ////////////////////////qDebug() << "calling spatial scaling 2D";
        Vec2< float > scaling = spatialScaling2D();

        QMatrix4x4 S;
        S.scale( scaling.x(), scaling.y() );

        // translation after scaling
        float tx = m_lowerLeftWindowCornerWorldSpace.x() + ( m_miniMapViewPort.offsetX() + MARGIN_LEFT   ) * conversionRatio + dx/2.0;
        float ty = m_lowerLeftWindowCornerWorldSpace.y() + ( m_miniMapViewPort.offsetY() + MARGIN_BOTTOM ) * conversionRatio + dy/2.0;

        // center of model
        float cX = ( m_contextBoundingBox.second.a() + m_contextBoundingBox.first.a() ) / 2.0;
        float cY = ( m_contextBoundingBox.second.b() + m_contextBoundingBox.first.b() ) / 2.0;

        M.setToIdentity();

        M.translate( tx, ty, 0.0 );
        M.scale( scaleFactor );
        M.translate( -cX, -cY, 0.0 );

        QMatrix4x4 M2 = M;
        M = M * S;

        PartTR = camera.proj() * camera.view() * M;
        GridTR = camera.proj() * camera.view() * M2;

        ////////////////////////qDebug() << "transforms complete";

        if( cell >= 0 )
        {
            ////////////////////////qDebug() << "getting selected grid corners";
            a = gridCorners[ cell*4 + 0 ];
            b = gridCorners[ cell*4 + 1 ];
            c = gridCorners[ cell*4 + 2 ];
            d = gridCorners[ cell*4 + 3 ];
        }

        glViewport( 0, 0, scaledWidth(), scaledHeight() );

        ////////////////////////qDebug() << "rendering grid";

        //////////////////////////qDebug() << mins.a() << "," << mins.b() << " "
        //<< maxes.a() << "," << maxes.b() << " "
        //<< a.a() << "," << a.b() << " "
        //<< d.a() << "," << d.b() << " "
        //<< cell;

        m_renderer->renderGrid(
            scaledWidth(),
            scaledHeight(),
            mins,
            maxes,
            30,
            30,
            GridTR,
            a,
            d,
            QVector4D( 1, 1, 1, 1 ),
            m_boxSelectionColor,
            cartesianProg,
            simpleColorProg,
            cell >= 0 );

        ////////////////////////qDebug() << "rendered grid";
        glViewport( 0, 0, scaledWidth(), scaledHeight() );

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            ////////////////////////qDebug() << "population is particles";

            const std::vector< float > & xAttr = *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spatialPartitioning.attributes[ 0 ] ) );
            const std::vector< float > & yAttr = *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spatialPartitioning.attributes[ 1 ] ) );

            glPointSize( 7.0 );
            m_renderer->renderPointsIndexed(
                xAttr,
                yAttr,
                m_sampler.getHighlightIndices(),
                flatProgSerial,
                QVector4D( 0, 0, 0, 1.0 ),
                PartTR );

            glPointSize( 4.0 );
            m_renderer->renderPointsIndexed(
                xAttr,
                yAttr,
                m_sampler.getHighlightIndices(),
                flatProgSerial,
                QVector4D( 0.8, 0.8, 0.8, 1.0 ),
                PartTR
            );
        }
        else
        {
            const std::vector< float > & xAttr = m_dataSetManager->distributionManager().values( spatialPartitioning.attributes[ 0 ] );
            const std::vector< float > & yAttr = m_dataSetManager->distributionManager().values( spatialPartitioning.attributes[ 1 ] );

            m_renderer->renderPrimitivesFlatSerial(
                xAttr,
                yAttr,
                flatProgSerial,
                QVector4D( m_contextColor.x(), m_contextColor.y(), m_contextColor.z(), 0.05 ),
                PartTR,
                GL_POINTS
            );

            ////////////////////////qDebug() << "rendered grid points";
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        renderVisualContextModels( m_populationDataTypeEnum, spatialPartitioning.attributes, { 1, 1 }, M, 3.0, {0,0}, { scaledWidth(), scaledHeight() } );
        glViewport( 0, 0, scaledWidth(), scaledHeight() );

        ////////////////////////qDebug() << "rendered visual context";

        ////////////////////////////

        float xLayoutMin =  std::numeric_limits< float >::max();
        float xLayoutMax = -std::numeric_limits< float >::max();
        float yLayoutMin =  std::numeric_limits< float >::max();
        float yLayoutMax = -std::numeric_limits< float >::max();

        for( auto & v : gridCorners )
        {
            xLayoutMin = std::min( xLayoutMin, v.x() );
            xLayoutMax = std::max( xLayoutMax, v.x() );
            yLayoutMin = std::min( yLayoutMin, v.y() );
            yLayoutMax = std::max( yLayoutMax, v.y() );
        }

        Vec2< float > lr( xLayoutMax, yLayoutMin );
        Vec2< float > ur( xLayoutMax, yLayoutMax );

        Vec2< float > ll( xLayoutMin, yLayoutMin );
        Vec2< float > ul( xLayoutMin, yLayoutMax );

        // get the bounds of the minimap in worldspace
        float minX = ( m_lowerLeftWindowCornerWorldSpace.x() +   ( m_miniMapViewPort.offsetX() +  1 ) * conversionRatio );
        float maxX = ( m_lowerLeftWindowCornerWorldSpace.x() + ( m_miniMapViewPort.offsetX() + m_miniMapViewPort.width() - 2 ) * conversionRatio );

        float minY = ( m_lowerLeftWindowCornerWorldSpace.y() + (  m_miniMapViewPort.offsetY() + 1 ) * conversionRatio );
        float maxY = ( m_lowerLeftWindowCornerWorldSpace.y() + ( m_miniMapViewPort.offsetY() + m_miniMapViewPort.height() - 2 ) * conversionRatio );

        // apply reverse of the transformation done to put the particles in the minimap from worldspace
        minX = ( minX - tx ) / scaleFactor + cX;
        maxX = ( maxX - tx ) / scaleFactor + cX;
        minY = ( minY - ty ) / scaleFactor + cY;
        maxY = ( maxY - ty ) / scaleFactor + cY;

        ////////////////////////////////////////

        QVector4D ulg( mins.a(), maxes.b(), 0, 1 );
        QVector4D lrg( maxes.a(), mins.b(), 0, 1 );

        ulg = GridTR * ulg;
        lrg = GridTR * lrg;

        float xOffset = ( ( ulg.x() ) * .5 + .5 ) *scaledWidth();
        float yOffset = ( ( lrg.y() ) * .5 + .5 ) *scaledHeight();

        float xR = ( lrg.x() * .5 + .5 ) *scaledWidth();
        float yR = ( ulg.y() * .5 + .5 ) *scaledHeight();

        float xWidth  = xR - xOffset;
        float yHeight = yR - yOffset;

        ////////////////////////qDebug() << "rendering text in minimap";

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                spatialPartitioning.attributes[ 0 ],
                                m_miniMapViewPort.offsetX() + m_miniMapViewPort.width() / 2.0 - spatialPartitioning.attributes[ 0 ].size() * 7.5 / 2.0,
                                yOffset - MARGIN_BOTTOM,
                                1.0f,
                                m_textColor );

        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                spatialPartitioning.attributes[ 1 ],
                                xOffset - MARGIN_LEFT,
                                m_miniMapViewPort.offsetY() + m_miniMapViewPort.height() / 2.0 - spatialPartitioning.attributes[ 1 ].size() * 7.5 / 2.0,
                                1.0,
                                m_textColor,
                                true );

        ////////////////////////////////////////

        std::vector< Vec2< float > > histWindow( 8 );
        histWindow[ 0 ] = ul;
        histWindow[ 1 ] = ur;
        histWindow[ 2 ] = ur;
        histWindow[ 3 ] = lr;
        histWindow[ 4 ] = lr;
        histWindow[ 5 ] = ll;
        histWindow[ 6 ] = ll;
        histWindow[ 7 ] = ul;

        std::vector< Vec2< float > > histWindowCropped( 8 );
        histWindowCropped.clear();

        ////////////////////////qDebug() << "rendering lines in minimap";

        // horizontel lines
        if( histWindow[ 0 ].y() < maxY && histWindow[ 0 ].y() > minY && histWindow[ 0 ].x() < maxX && histWindow[ 1 ].x() > minX )
        {
            histWindowCropped.push_back( Vec2< float>( std::max( histWindow[ 0 ].x(), minX ), histWindow[ 0 ].y() ) );
            histWindowCropped.push_back( Vec2< float>( std::min( histWindow[ 1 ].x(), maxX ), histWindow[ 1 ].y() ) );
        }
        if( histWindow[ 5 ].y() < maxY && histWindow[ 5 ].y() > minY && histWindow[ 5 ].x() < maxX && histWindow[ 4 ].x() > minX )
        {
            histWindowCropped.push_back( Vec2< float>( std::max( histWindow[ 5 ].x(), minX ), histWindow[ 5 ].y() ) );
            histWindowCropped.push_back( Vec2< float>( std::min( histWindow[ 4 ].x(), maxX ), histWindow[ 4 ].y() ) );
        }

        // vertical lines
        if( histWindow[ 6 ].x() < maxX && histWindow[ 6 ].x() > minX && histWindow[ 6 ].y() < maxY && histWindow[ 7 ].y() > minY )
        {
            histWindowCropped.push_back( Vec2< float>( histWindow[ 6 ].x(), std::max( histWindow[ 6 ].y(), minY ) ) );
            histWindowCropped.push_back( Vec2< float>( histWindow[ 7 ].x(), std::min( histWindow[ 7 ].y(), maxY ) ) );
        }
        if( histWindow[ 3 ].x() < maxX && histWindow[ 3 ].x() > minX && histWindow[ 3 ].y() < maxY && histWindow[ 2 ].y() > minY )
        {
            histWindowCropped.push_back( Vec2< float>( histWindow[ 3 ].x(), std::max( histWindow[ 3 ].y(), minY ) ) );
            histWindowCropped.push_back( Vec2< float>( histWindow[ 2 ].x(), std::min( histWindow[ 2 ].y(), maxY ) ) );
        }

        glLineWidth( 2.0 );
        glPointSize( 2.0 );

        m_renderer->renderPrimitivesFlat( histWindowCropped, cartesianProg, m_boxSelectionColor, GridTR, GL_POINTS );
        m_renderer->renderPrimitivesFlat( histWindowCropped, cartesianProg, m_boxSelectionColor, GridTR, GL_LINES );

        bool mousedOver = m_sampler.hasCellSelection();
        if ( mousedOver )
        {
            ////////////////////////qDebug() << "moused over was true";

            lr = gridCorners[ m_sampler.getSelectedCellId()*4 + 1 ];
            ur = gridCorners[ m_sampler.getSelectedCellId()*4     ];
            ll = gridCorners[ m_sampler.getSelectedCellId()*4 + 3 ];
            ul = gridCorners[ m_sampler.getSelectedCellId()*4 + 2 ];

            std::vector< Vec2< float > > orientationGlyph1( 3 );
            orientationGlyph1[ 0 ] = Vec2< float >( ul.x() + .85 * ( ur.x() - ul.x() ), ll.y() + 1.0 * ( ur.y() - ll.y() ) );
            orientationGlyph1[ 1 ] = Vec2< float >( ul.x() + 1.0 * ( ur.x() - ul.x() ), ll.y() + 1.0 * ( ur.y() - ll.y() ) );
            orientationGlyph1[ 2 ] = Vec2< float >( ul.x() + 1.0 * ( ur.x() - ul.x() ), ll.y() + .85 * ( ur.y() - ll.y() ) );

            std::vector< Vec2< float > > orientationGlyph2( 3 );
            orientationGlyph2[ 0 ] = Vec2< float >( ul.x() + .92 * ( ur.x() - ul.x() ), ll.y() + .98 * ( ur.y() - ll.y() ) );
            orientationGlyph2[ 1 ] = Vec2< float >( ul.x() + .98 * ( ur.x() - ul.x() ), ll.y() + .98 * ( ur.y() - ll.y() ) );
            orientationGlyph2[ 2 ] = Vec2< float >( ul.x() + .98 * ( ur.x() - ul.x() ), ll.y() + .92 * ( ur.y() - ll.y() ) );

            std::vector< Vec2< float > > histViewBox(8);
            histViewBox[ 0 ] = ul;
            histViewBox[ 1 ] = ur;
            histViewBox[ 2 ] = ur;
            histViewBox[ 3 ] = lr;
            histViewBox[ 4 ] = lr;
            histViewBox[ 5 ] = ll;
            histViewBox[ 6 ] = ll;
            histViewBox[ 7 ] = ul;

            std::vector< Vec2< float > > mmapBox(8);
            mmapBox.clear();

            // horizontel lines
            if( histViewBox[ 0 ].y() < maxY && histViewBox[ 0 ].y() > minY )
            {
                mmapBox.push_back( Vec2< float>( std::max( histViewBox[ 0 ].x(), minX ), histViewBox[ 0 ].y() ) );
                mmapBox.push_back( Vec2< float>( std::min( histViewBox[ 1 ].x(), maxX ), histViewBox[ 1 ].y() ) );
            }
            if( histViewBox[ 5 ].y() < maxY && histViewBox[ 5 ].y() > minY )
            {
                mmapBox.push_back( Vec2< float>( std::max( histViewBox[ 5 ].x(), minX ), histViewBox[ 5 ].y() ) );
                mmapBox.push_back( Vec2< float>( std::min( histViewBox[ 4 ].x(), maxX ), histViewBox[ 4 ].y() ) );
            }

            // vertical lines
            if( histViewBox[ 6 ].x() < maxX && histViewBox[ 6 ].x() > minX )
            {
                mmapBox.push_back( Vec2< float>( histViewBox[ 6 ].x(), std::max( histViewBox[ 6 ].y(), minY ) ) );
                mmapBox.push_back( Vec2< float>( histViewBox[ 7 ].x(), std::min( histViewBox[ 7 ].y(), maxY ) ) );
            }
            if( histViewBox[ 3 ].x() < maxX && histViewBox[ 3 ].x() > minX )
            {
                mmapBox.push_back( Vec2< float>( histViewBox[ 3 ].x(), std::max( histViewBox[ 3 ].y(), minY ) ) );
                mmapBox.push_back( Vec2< float>( histViewBox[ 2 ].x(), std::min( histViewBox[ 2 ].y(), maxY ) ) );
            }

            glLineWidth( 2.0 );
            glPointSize( 2.0 );

            m_renderer->renderPrimitivesFlat( mmapBox, cartesianProg, m_boxSelectionColor, GridTR, GL_LINES );
            m_renderer->renderPrimitivesFlat( mmapBox, cartesianProg, m_boxSelectionColor, GridTR, GL_POINTS );

            /////// Selection in Sampler View

            glLineWidth( 3.0 );
            glPointSize( 3.0 );

            if( ! userIsPanning )
            {
                m_renderer->renderPrimitivesFlat( histViewBox, cartesianProg, m_boxSelectionColor, ( camera.proj()*camera.view() ), GL_LINES );
                m_renderer->renderPrimitivesFlat( histViewBox, cartesianProg, m_boxSelectionColor, ( camera.proj()*camera.view() ), GL_POINTS );

                m_renderer->renderPrimitivesFlat( orientationGlyph1, cartesianProg, QVector4D( .1, .1, .1, 1 ), ( camera.proj()*camera.view() ), GL_TRIANGLES );
                m_renderer->renderPrimitivesFlat( orientationGlyph1, cartesianProg, QVector4D( .1, .1, .1, 1 ), ( camera.proj()*camera.view() ), GL_TRIANGLES );

                m_renderer->renderPrimitivesFlat( orientationGlyph2, cartesianProg, QVector4D( .4, .9, .6, 1.0 ), ( camera.proj()*camera.view() ), GL_TRIANGLES );
                m_renderer->renderPrimitivesFlat( orientationGlyph2, cartesianProg, QVector4D( .4, .9, .6, 1.0 ), ( camera.proj()*camera.view() ), GL_TRIANGLES );

                if( m_sampler.hasCellSelection()
                        && m_sampler.clickSelectedCellId() >= 0
                        && m_sampler.clickSelectedCellId() != m_sampler.getSelectedCellId() )
                {
                    QVector4D storedSelectionColor( .2, .8, .3, 1 );

                    int selCelId = m_sampler.clickSelectedCellId();

                    auto lr = gridCorners[ selCelId*4 + 1 ];
                    auto ur = gridCorners[ selCelId*4     ];
                    auto ll = gridCorners[ selCelId*4 + 3 ];
                    auto ul = gridCorners[ selCelId*4 + 2 ];

                    std::vector< Vec2< float > > histViewBoxStored = { ul, ur, ur, lr, lr, ll, ll, ul };

                    glLineWidth( 3.0 );
                    glPointSize( 3.0 );
                    m_renderer->renderPrimitivesFlat( histViewBoxStored, cartesianProg, m_boxSelectionColor, ( camera.proj()*camera.view() ), GL_LINES );
                    m_renderer->renderPrimitivesFlat( histViewBoxStored, cartesianProg, m_boxSelectionColor, ( camera.proj()*camera.view() ), GL_POINTS );

                    glLineWidth( 1.0 );
                    glPointSize( 1.0 );
                    m_renderer->renderPrimitivesFlat( histViewBoxStored, cartesianProg, storedSelectionColor, ( camera.proj()*camera.view() ), GL_LINES );
                    m_renderer->renderPrimitivesFlat( histViewBoxStored, cartesianProg, storedSelectionColor, ( camera.proj()*camera.view() ), GL_POINTS );
                }
            }

            /////////////////////////////////////

            float xA = ul.x();
            float xB = lr.x();

            float yA = lr.y();
            float yB = ul.y();

            std::string xAStr = to_string_with_precision( xA / scaling.x() );
            std::string xBStr = to_string_with_precision( xB / scaling.x() );
            std::string yAStr = to_string_with_precision( yA / scaling.y() );
            std::string yBStr = to_string_with_precision( yB / scaling.y() );

            glViewport( 0, 0, scaledWidth(), scaledHeight() );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    xAStr,
                                    xOffset + xWidth * std::min( ( ( xA - mins.a() ) / ( maxes.a() - mins.a() ) ), 1.f ) - xAStr.size() * 7.5,
                                    yOffset - 16,
                                    1.0f,
                                    m_textColor );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    xBStr,
                                    xOffset + xWidth * std::min( ( xB - mins.a() ) / ( maxes.a() - mins.a() ), 1.f ),
                                    yOffset - 16,
                                    1.0f,
                                    m_textColor );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    yAStr,
                                    xOffset - 8,
                                    yOffset + yHeight * std::min( ( ( yA - mins.b() ) / ( maxes.b() - mins.b() ) ), 1.f ) - yAStr.size() * 7.5,
                                    1.0f,
                                    m_textColor,
                                    true );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    yBStr,
                                    xOffset - 8,
                                    yOffset + yHeight * std::min( ( ( yB - mins.b() ) / ( maxes.b() - mins.b() ) ), 1.f ),
                                    1.0f,
                                    m_textColor,
                                    true );
        }
    }
}

void BALEEN::renderGridSelectionContext(
    const std::vector< std::pair< std::string, double > >  & space,
    const ROIDefinition & roiDefinition,
    bool selectionValid )
{
    float conversionRatio = ( m_upperLeftWindowCornerWorldSpace.y() - m_lowerLeftWindowCornerWorldSpace.y() ) / ( ( float ) scaledHeight() );

    qDebug() << "got conversion ratio";

    float dx =  ( m_auxViewPort.width() ) * conversionRatio;
    float dy =  ( m_auxViewPort.height() - m_samplerWidget.controlPanel.size().y() - MENU_HEIGHT ) * conversionRatio;

    float margins = DIVIDER_WIDTH * 4 * conversionRatio;

    qDebug() << "space dimensions";

    Vec2< double > rX = m_dataSetManager->distributionManager().range( space[ 0 ].first ) * space[ 0 ].second;
    Vec2< double > rY = m_dataSetManager->distributionManager().range( space[ 1 ].first ) * space[ 0 ].second;

    qDebug() << "got the ranges";

    std::pair< Vec2< float >, Vec2< float > > mBB;

    mBB.first.x(  rX.a() );
    mBB.first.y(  rY.a() );
    mBB.second.x( rX.b() );
    mBB.second.y( rY.b() );

    Vec2< float > mmS = Vec2< float >( ( rX.b() - rX.a() ), ( rY.b() - rY.a() ) );

    const float marginalScale = 1.0;
    float scaleFactor = std::min( ( dy - margins ) / ( mmS.y()*marginalScale ), ( dx - margins ) / mmS.x()*marginalScale );

    // center of model
    float cX = ( mBB.second.x() + mBB.first.x() ) / 2.0;
    float cY = ( mBB.second.y() + mBB.first.y() ) / 2.0;

    qDebug() << "model coordinates";

    QMatrix4x4 TR;

    TR.setToIdentity();
    TR.translate( m_upperRightWindowCornerWorldSpace.x() - dx/2.0,
                  m_upperRightWindowCornerWorldSpace.y() - dy/2.0 - ( m_samplerWidget.controlPanel.size().y() + MENU_HEIGHT ) * conversionRatio, 0.0 );
    TR.scale( scaleFactor );
    TR.translate( -cX, -cY, 0.0 );

    int GRID_POINT_MODE = 0;
    int AGGREGATION_MODE = 1;
    int NO_MODE = 2;

    int MODE = GRID_POINT_MODE; //selectionValid ? NO_MODE : 1;

    // finally get the attributes, and make sure they are loaded in memory
    const std::vector< float > & xAttr = m_dataSetManager->distributionManager().values( space[ 0 ].first );
    const std::vector< float > & yAttr = m_dataSetManager->distributionManager().values( space[ 1 ].first );

    qDebug() << "got the grid values";

    QMatrix4x4 S;
    S.scale( space[ 0 ].second, space[ 1 ].second );

    // now render
    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    if( MODE == GRID_POINT_MODE )
    {
        qDebug() << "rendering grid points";

        glPointSize( 3.0 );
        m_renderer->renderPrimitivesFlatSerial(
            xAttr,
            yAttr,
            flatProgSerial,
            QVector4D( m_contextColor.x(), m_contextColor.y(), m_contextColor.z(), 0.8 ),
            camera.proj()*camera.view() * S * TR,
            GL_POINTS
        );

        qDebug() << "rendered grid points";

        if( m_sampler.hasCellSelection() )
        {
            qDebug() << "cell selection exists";

            const std::vector< int > & gridSelection = m_sampler.gridMap()[ m_sampler.getSelectedCellId() ];
            const int SZ = gridSelection.size();

            std::vector< float > selectedX( SZ );
            std::vector< float > selectedY( SZ );

            #pragma omp parallel for
            for( int i = 0; i < SZ; ++i )
            {
                selectedX[ i ] = xAttr[ gridSelection[ i ] ];
                selectedY[ i ] = yAttr[ gridSelection[ i ] ];
            }

            glPointSize( 7 );
            m_renderer->renderPrimitivesFlatSerial(
                selectedX,
                selectedY,
                flatProgSerial,
                QVector4D( 0.0, 0.0, 0.0 , 1 ),
                camera.proj()*camera.view() * S * TR,
                GL_POINTS
            );

            glPointSize( 4 );
            m_renderer->renderPrimitivesFlatSerial(
                selectedX,
                selectedY,
                flatProgSerial,
                QVector4D( 0.0, 1.0, .4 , 1 ),
                camera.proj()*camera.view() * S * TR,
                GL_POINTS
            );
        }

        if( roiDefinition.mode == (int) ROIDialog::Mode::FixedRegion )
        {
            int roiCellIndex = m_roiCell / 4;

            qDebug() << "rendering roi cell " << roiCellIndex ;
            qDebug() << "GMap size = " << m_sampler.gridMap().size();

            if( roiCellIndex < m_sampler.gridMap().size() )
            {
                const std::vector< int > & gridSelection = m_sampler.gridMap()[ roiCellIndex ];
                const int SZ = gridSelection.size();

                qDebug() << "n cell points = " << SZ;

                std::vector< float > selectedX( SZ );
                std::vector< float > selectedY( SZ );

                qDebug() << "copying over points";

                #pragma omp parallel for
                for( int i = 0; i < SZ; ++i )
                {
                    selectedX[ i ] = xAttr[ gridSelection[ i ] ];
                    selectedY[ i ] = yAttr[ gridSelection[ i ] ];
                }

                qDebug() << "copying roi points";

                glPointSize( 7 );
                m_renderer->renderPrimitivesFlatSerial(
                    selectedX,
                    selectedY,
                    flatProgSerial,
                    QVector4D( 0.0, 1.0, 0.0 , 1 ),
                    camera.proj()*camera.view() * S * TR,
                    GL_POINTS
                );

                glPointSize( 4 );
                m_renderer->renderPrimitivesFlatSerial(
                    selectedX,
                    selectedY,
                    flatProgSerial,
                    QVector4D( 1.0, 1.0, .4 , 1 ),
                    camera.proj()*camera.view() * S * TR,
                    GL_POINTS
                );
            }
            else
            {
                qDebug() << "ERROR: ROI Cell is invalid";
            }

            qDebug() << "done";
        }
    }

    else if ( MODE == AGGREGATION_MODE )
    {
        qDebug() << "aggregation mode enabled";

        static std::string aggS = m_aggregationCombo.selectedItemText();
        static bool firstTime = true;

        const std::string agg = m_aggregationCombo.selectedItemText();

        const std::vector< float > & values =
            agg == "mean"     ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_mean",      m_selectedTimeStep ) :
            agg == "sum"      ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_mean",      m_selectedTimeStep ) :
            agg == "rms"      ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_rms",       m_selectedTimeStep ) :
            agg == "variance" ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_variance",  m_selectedTimeStep ) :
            agg == "count"    ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "num_particles",  m_selectedTimeStep ) :
            agg == "min"      ? m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_min",       m_selectedTimeStep ) :
            m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "w0w1_max",       m_selectedTimeStep );

        const std::vector< float > & counts = m_dataSetManager->distributionManager().statistic( m_particleCombo.selectedItemText(), "num_particles",  m_selectedTimeStep );
        static std::vector< float > smoothedValues = values;

        const int MAX_RADIUS = 7;
        static int radiusS = std::round( m_smoothRadiusSlider.sliderPosition() * MAX_RADIUS );
        const int radius = std::round( m_smoothRadiusSlider.sliderPosition() * MAX_RADIUS );
        static int timeStep = m_selectedTimeStep;

        if( ( agg != aggS || radius != radiusS || firstTime || timeStep != m_selectedTimeStep ) && ! m_smoothRadiusSlider.isPressed() )
        {
            smoothedValues = values;

            if( agg == "sum" )
            {
                const size_t SZ = values.size();
                #pragma omp parallel for simd
                for( size_t i = 0; i < SZ; ++i )
                {
                    smoothedValues[ i ] = smoothedValues[ i ] * counts[ i ];
                }
            }

            auto & neighborhoods    = m_dataSetManager->distributionManager().neighborhood();
            auto & neighborhoodSums = m_dataSetManager->distributionManager().neighborhoodSums();

            if( radius > 0 )
            {
                TN::smooth(
                    values,
                    counts,
                    neighborhoods,
                    neighborhoodSums,
                    smoothedValues,
                    radius,
                    agg == "mean"     ? TN::aggregateMean
                    : agg == "rms"      ? TN::aggregateRMS
                    : agg == "variance" ? TN::aggregateVariance
                    : agg == "min"      ? TN::aggregateMin
                    : agg == "max"      ? TN::aggregateMax
                    : agg == "sum"      ? TN::aggregateSum
                    : TN::aggregateCount );
            }
        }

        timeStep = m_selectedTimeStep;
        radiusS = radius;

        if( m_smoothRadiusSlider.isPressed() )
        {
            firstTime = true;
        }
        else
        {
            firstTime = false;
        }

        const std::vector< unsigned int > & triangulation = m_dataSetManager->distributionManager().triangulation();

        glDisable( GL_CULL_FACE );
        glDisable( GL_DEPTH_TEST );
        glEnable( GL_BLEND );

        m_renderer->renderTriangulationMap(
            xAttr,
            yAttr,
            smoothedValues,
            triangulation,
            triangulationMapProgram,
            m_triangulationTF,
            *std::min_element( smoothedValues.begin(), smoothedValues.end() ),
            *std::max_element( smoothedValues.begin(), smoothedValues.end() ),
            m_gridColorScaleSlider.sliderPosition() * 10.f + 1.f,
            m_negPosScalarButton.isPressed(),
            true,
            camera.proj()*camera.view() * S * TR,
            GL_TRIANGLES
        );

        std::vector< size_t > completHullIndices;
        TN::convexHull( xAttr, yAttr, completHullIndices );

        const size_t N_CI = completHullIndices.size();
        std::vector< float > pcx( N_CI );
        std::vector< float > pcy( N_CI );

        for( int i = 0; i < N_CI; ++i )
        {
            pcx[ i ] = xAttr[ completHullIndices[ i ] ];
            pcy[ i ] = yAttr[ completHullIndices[ i ] ];
        }

        glPointSize( 4 );
        m_renderer->renderPrimitivesFlatSerial(
            pcx,
            pcy,
            flatProgSerial,
            QVector4D( 0.2, 0.2, 0.2, 1.0 ),
            camera.proj() * camera.view() * S * TR,
            GL_LINE_STRIP
        );

        auto & gMap = m_sampler.gridMap();

        std::vector< float > windowGPX;
        std::vector< float > windowGPY;

        for( int i = 0; i < gMap.size(); ++i )
        {
            const std::vector< int > & grp = gMap[ i ];
            for( int j = 0; j < grp.size(); ++j )
            {
                windowGPX.push_back( xAttr[ grp[ j ] ] );
                windowGPY.push_back( yAttr[ grp[ j ] ] );
            }
        }

        std::vector< size_t > windowHullIndices;
        TN::convexHull( windowGPX, windowGPY, windowHullIndices );

        const size_t N_CW = windowHullIndices.size();
        std::vector< float > wcx( N_CW );
        std::vector< float > wcy( N_CW );
        for( int i = 0; i < N_CW; ++i )
        {
            wcx[ i ] = windowGPX[ windowHullIndices[ i ] ];
            wcy[ i ] = windowGPY[ windowHullIndices[ i ] ];
        }

//        glPointSize( 2 );
//        m_renderer->renderPrimitivesFlatSerial(
//            windowGPX,
//            windowGPY,
//            flatProgSerial,
//            QVector4D( 0.0, 1.0, 0, 0.5 ),
//            camera.proj()*camera.view() * S * TR,
//            GL_POINTS
//        );

        glPointSize( 4 );
//        m_renderer->renderPrimitivesFlatSerial(
//            wcx,
//            wcy,
//            flatProgSerial,
//            QVector4D( 0.2, 0.2, 0.2, 1.0 ),
//            camera.proj() * camera.view() * S * TR,
//            GL_LINE_STRIP
//        );

        bool showVisualMap = selectionValid;

        if( m_sampler.hasCellSelection() )
        {
            const std::vector< int > & gridSelection = gMap[ m_sampler.getSelectedCellId() ];
            const int SZ = gridSelection.size();

            if( SZ > 0 )
            {
                std::vector< float > selectedX( SZ );
                std::vector< float > selectedY( SZ );

                float maxX = -std::numeric_limits<float>::max();
                float maxY = maxX;
                float minX = -maxX;
                float minY = minX;

                for( int i = 0; i < SZ; ++i )
                {
                    selectedX[ i ] = xAttr[ gridSelection[ i ] ];
                    selectedY[ i ] = yAttr[ gridSelection[ i ] ];

                    minX = std::min( minX, selectedX[ i ] );
                    maxX = std::max( maxX, selectedX[ i ] );
                    minY = std::min( minY, selectedY[ i ] );
                    maxY = std::max( maxY, selectedY[ i ] );
                }

                enum
                {
                    SGP_BOUNDING_BOX,
                    SGP_CONVEX_HULL,
                    SGP_CONVAVE_HULL,
                    SGP_POINTS
                };

                const int SGP_REN_MODE = SGP_CONVEX_HULL;

                if( SGP_REN_MODE == SGP_BOUNDING_BOX && SZ >= 4 )
                {
                    m_renderer->renderPrimitivesFlatSerial(
                        std::vector< float >( { minX, minX, maxX, maxX, minX } ),
                        std::vector< float >( { minY, maxY, maxY, minY, minY } ),
                        flatProgSerial,
                        QVector4D( 0.0, 0.0, 0, 0.5 ),
                        camera.proj()*camera.view() * S * TR,
                        GL_LINE_STRIP
                    );
                }
                else if( SGP_REN_MODE == SGP_CONVEX_HULL && SZ  >= 4 )
                {
                    glPointSize( 2 );
                    m_renderer->renderPrimitivesFlatSerial(
                        selectedX,
                        selectedY,
                        flatProgSerial,
                        QVector4D( 1.0, 0.0, 0, 0.5 ),
                        camera.proj() * camera.view() * S * TR,
                        GL_POINTS
                    );

                    std::vector< size_t > indices;
                    TN::convexHull( selectedX, selectedY, indices );

                    std::vector< float > px( indices.size() );
                    std::vector< float > py( indices.size() );

                    const size_t N_I = indices.size();
                    for( int i = 0; i < N_I; ++i )
                    {
                        px[ i ] = selectedX[ indices[ i ] ];
                        py[ i ] = selectedY[ indices[ i ] ];
                    }

                    px.push_back( px.back() );
                    py.push_back( py.back() );

                    glPointSize( 2 );
//                    m_renderer->renderPrimitivesFlatSerial(
//                        px,
//                        py,
//                        flatProgSerial,
//                        QVector4D( 0.0, 0.0, 0, 0.5 ),
//                        camera.proj() * camera.view() * S * TR,
//                        GL_LINE_STRIP
//                    );

                }
                else if( SGP_REN_MODE == SGP_POINTS )
                {
                    glPointSize( 2 );
                    m_renderer->renderPrimitivesFlatSerial(
                        selectedX,
                        selectedY,
                        flatProgSerial,
                        QVector4D( 0.0, 0.0, 0, 0.5 ),
                        camera.proj()*camera.view() * S * TR,
                        GL_POINTS
                    );
                }
            }
        }
        if( showVisualMap && selectionValid )
        {
            for( unsigned int i = 0; i < gMap.size(); ++i )
            {
                const std::vector< int > & cellGP = gMap[ i ];
                const int SZ = cellGP.size();
                if( SZ > 0 )
                {
                    std::vector< float > cellX( SZ );
                    std::vector< float > cellY( SZ );

                    for( int i = 0; i < SZ; ++i )
                    {
                        cellX[ i ] = xAttr[ cellGP[ i ] ];
                        cellY[ i ] = yAttr[ cellGP[ i ] ];
                    }

                    double shade = i % 2 == 0 ? 0.5 : 0.9;

                    glPointSize( 2 );
                    m_renderer->renderPrimitivesFlatSerial(
                        cellX,
                        cellY,
                        flatProgSerial,
                        QVector4D( shade, shade, shade, 0.5 ),
                        camera.proj()*camera.view() * S * TR,
                        GL_POINTS
                    );
                }
            }

            qDebug() << "checking roi mode";

            if( roiDefinition.mode == (int) ROIDialog::Mode::FixedRegion )
            {
                qDebug() << "roi mode is fixed region";

                int roiCellIndex = m_roiCell / 4;

                qDebug() << "roi cell is " << roiCellIndex;
                qDebug() << "region grid map is size " << gMap.size();

                const std::vector< int > & gridSelection = gMap[ roiCellIndex ];
                const int SZ = gridSelection.size();

                if( SZ > 0 )
                {
                    std::vector< float > roiPointsX( SZ );
                    std::vector< float > roiPointsY( SZ );

                    float maxX = -std::numeric_limits<float>::max();
                    float maxY = maxX;
                    float minX = -maxX;
                    float minY = minX;

                    for( int i = 0; i < SZ; ++i )
                    {
                        roiPointsX[ i ] = xAttr[ gridSelection[ i ] ];
                        roiPointsY[ i ] = yAttr[ gridSelection[ i ] ];

                        minX = std::min( minX, roiPointsX[ i ] );
                        maxX = std::max( maxX, roiPointsX[ i ] );
                        minY = std::min( minY, roiPointsY[ i ] );
                        maxY = std::max( maxY, roiPointsY[ i ] );
                    }

                    glPointSize( 10 );
                    m_renderer->renderPrimitivesFlatSerial(
                        std::vector< float >( { minX, minX, maxX, maxX, minX } ),
                        std::vector< float >( { minY, maxY, maxY, minY, minY } ),
                        flatProgSerial,
                        QVector4D( 0.0, 0.0, 0, 1.0 ),
                        camera.proj()*camera.view() * S * TR,
                        GL_LINE_STRIP
                    );
                }
            }

            qDebug() << "roi mode is not fixed region";

            // render window corners ?


            // render roi corners



            // render selected regionion corners

        }
    }
}

//void BALEEN::renderLinkedView( int dataType, int viewMode, const std::string & ptype, int weightType, const std::string & weightKey, bool selectionValid )
//{
//    if( dataType == PopulationDataType::GRID_POINTS )
//    {
//        const auto it = m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype );
//        if( it != m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.end() )
//        {
//            auto sp = m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second;
//            if( sp.attributes.size() >= 2 )
//            {
//                renderGridSelectionContext( { { "r", 1.0 }, { "z", 1.0 } }, sp.attributes[ 0 ],  sp.attributes[ 1 ], selectionValid && ! m_samplerWidget.controlPanel.valid() );
//            }
//        }
//    }
//    else if ( dataType == PopulationDataType::PARTICLES )
//    {
//        std::string phasePlotName = m_phasePlotCombo.selectedItemText();

//        auto phaseDefMapIter = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype );
//        if( phaseDefMapIter == m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.end() )
//        {
//            return;
//        }

//        auto phaseDefIter = phaseDefMapIter->second.find( phasePlotName );
//        if ( phaseDefIter == phaseDefMapIter->second.end() )
//        {
//            return;
//        }

//        PhasePlotDefinition plotDef = phaseDefIter->second;
//        renderPhasePlot( dataType, viewMode, ptype, plotDef, weightType, weightKey  );
//    }
//}

void BALEEN::updateHistLayoutLines()
{
    const std::vector< Vec2< float > > & g = m_sampler.getGridCorners();
    const std::vector< Vec2< float > > & ng = m_sampler.getNonEmptyGridCorners();

    if( g.size() < 4 )
    {
        return;
    }

    int end = g.size() - 3;

    m_histogramLayoutLines.clear();
    m_nonEmptyHistLayoutHighlights.clear();

    for ( int i = 0; i < end; i+=4 )
    {
        HIST::appendCartesianOutlineTo(
            m_histogramLayoutLines,
            g[i+0],
            g[i+1],
            g[i+2],
            g[i+3]
        );
    }

    end = ng.size() - 3;
    for ( int i = 0; i < end; i+=4 )
    {
        HIST::appendCartesianOutlineTo(
            m_nonEmptyHistLayoutHighlights,
            ng[i+0],
            ng[i+1],
            ng[i+2],
            ng[i+3]
        );
    }
}

void BALEEN::recalculateSamplingLayout()
{
    SamplerParams params(
        m_lowerLeftWindowCornerWorldSpace,
        ( m_upperLeftWindowCornerWorldSpace.y() - m_lowerLeftWindowCornerWorldSpace.y() ) / ( ( double ) scaledHeight() ) );

    params.detailFactor = m_samplerWidget.controlPanel.roiScalePanel.lodSlider.sliderPosition();
    m_sampler.recalculate( params );

    if( m_samplerWidget.controlPanel.mode == SamplerPanel::Mode::RegionOfInterest )
    {
        // snap region of interest to nearest histogram or not at all if off screen
        // note when panning this should happen as well
        // this case affects zoom level, which should be disabled.
    }
}

void BALEEN::renderSamplingLayout( bool userIsPanning )
{
    updateHistLayoutLines();
    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();

    if( gridCorners.size() < 4 )
    {
        return;
    }

    ROIDefinition roiDef = m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.at( m_particleCombo.selectedItemText() ).at(
        m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectedItemText() );

    for( int i = 0, end = static_cast< int >( gridCorners.size() ) - 3; i < end; i += 4 )
    {
        Vec2< float > a = gridCorners[ i + 0 ];
        Vec2< float > b = gridCorners[ i + 1 ];
        Vec2< float > c = gridCorners[ i + 2 ];

        QVector4D aP = ( camera.proj()*camera.view() ) * QVector4D( a.x(), a.y(), 0, 1.0 );
        QVector4D bP = ( camera.proj()*camera.view() ) * QVector4D( b.x(), b.y(), 0, 1.0 );
        QVector4D cP = ( camera.proj()*camera.view() ) * QVector4D( c.x(), c.y(), 0, 1.0 );

        float x1 = ( aP.x() / 2.0 + .5 ) *scaledWidth();
        float x3 = ( cP.x() / 2.0 + .5 ) *scaledWidth();
        float y1 = ( aP.y() / 2.0 + .5 ) *scaledHeight();
        float y2 = ( bP.y() / 2.0 + .5 ) *scaledHeight();

        glViewport( x3, y2, x1 - x3, y1 - y2 );
        m_renderer->renderPopSquare( simpleColorProg, 1.0 );
    }

    QMatrix4x4 S;
    auto scaling = spatialScaling2D();
    S.scale( scaling.x(), scaling.y() );

    if( m_samplerWidget.controlPanel.mode == SamplerPanel::Mode::RegionOfInterest && m_roiSnapped  )
    {
        glLineWidth( 2 );
        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderPrimitivesFlatSerial(
            std::vector< float >( { roiDef.xROI.a(), roiDef.xROI.b(), roiDef.xROI.b(), roiDef.xROI.a(), roiDef.xROI.a() } ),
            std::vector< float >( { roiDef.yROI.b(), roiDef.yROI.b(), roiDef.yROI.a(), roiDef.yROI.a(), roiDef.yROI.b() }  ),
            flatProgSerial,
            QVector4D( 0.3, 0.3, 0.3, 1.0 ),
            camera.proj() * camera.view() * S,
            GL_LINE_STRIP
        );

        auto ptsx = std::vector< float >( { roiDef.xROI.a(), roiDef.xROI.b(), roiDef.xROI.b(), roiDef.xROI.a(), roiDef.xROI.a() } );
        auto ptsy = std::vector< float >( { roiDef.yROI.b(), roiDef.yROI.b(), roiDef.yROI.a(), roiDef.yROI.a(), roiDef.yROI.b() } );

        if( userIsPanning )
        {
            ptsx.push_back( roiDef.xROI.a() + ( roiDef.xROI.b() -  roiDef.xROI.a() ) / 2.0 );
            ptsy.push_back( roiDef.yROI.a() + ( roiDef.yROI.b() -  roiDef.yROI.a() ) / 2.0 );
            glPointSize( 5 );
        }
        else
        {
            glPointSize( 2 );
        }

        m_renderer->renderPrimitivesFlatSerial(
            ptsx,
            ptsy,
            flatProgSerial,
            QVector4D( 0.3, 0.3, 0.3, 1.0 ),
            camera.proj() * camera.view() * S,
            GL_POINTS
        );
    }

    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    glLineWidth( 1.0 );
    m_renderer->renderPrimitivesFlat(
        m_nonEmptyHistLayoutHighlights,
        cartesianProg,
        QVector4D( 0.3, 0.3, 0.3, 1.0f ),
        camera.proj()*camera.view(),
        GL_LINES );
}

void BALEEN::invalidateSampling()
{
    m_sampler.clear();
    m_frVolumeCache.invalidate();
    m_timePlotCache.invalidate();
    m_phasePlotCache.invalidate();
}

void BALEEN::samplePopulation( BaseHistDefinition * def, const SpacePartitioningDefinition & spacePartitioning, int dataType, const std::string & ptype, const Vec2< double > & scaling )
{
    if( dataType == PopulationDataType::PARTICLES )
    {
        if( spacePartitioning.attributes.size() == 2 )
        {
            if( dynamic_cast< HistogramDefinition * >( def )->weightType != HistogramWeightType::VARIABLE )
            {
                m_sampler.mapParticlesToHistograms(
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spacePartitioning.attributes[ 0 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spacePartitioning.attributes[ 1 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, def->binningSpace[ 0 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, def->binningSpace[ 1 ] ) ),
                    def->valueRange,
                { m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition(), m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.sliderPosition() },
                std::vector< double >( { scaling.x(), scaling.y() } ) );
            }
            else
            {
                m_sampler.mapParticlesToHistograms(
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spacePartitioning.attributes[ 0 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, spacePartitioning.attributes[ 1 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, def->binningSpace[ 0 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, def->binningSpace[ 1 ] ) ),
                    *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, dynamic_cast< HistogramDefinition * >( def )->weight ) ),
                    def->valueRange,
                { m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderX.sliderPosition(), m_samplerWidget.controlPanel.histogramPanel.valueWidthSliderY.sliderPosition() },
                std::vector< double >( { scaling.x(), scaling.y() } ) );
            }
        }
    }
    else if ( dataType == PopulationDataType::GRID_POINTS )
    {
        if( spacePartitioning.attributes.size() == 2 )
        {
            const std::vector< float > & xPos = m_dataSetManager->distributionManager().values( spacePartitioning.attributes[ 0 ] );
            const std::vector< float > & yPos = m_dataSetManager->distributionManager().values( spacePartitioning.attributes[ 1 ] );
            m_sampler.clusterGridPoints( xPos, yPos, scaling.x(), scaling.y() );
            mergeGridPoints();
        }
    }
}

void BALEEN::renderPopulation( int dataType, const std::vector< std::string > & partitionSpace, const Vec2< double > & scaling, const QMatrix4x4 & M, double pointSize )
{
    std::string ptype = m_particleCombo.selectedItemText();

    if( dataType == PopulationDataType::PARTICLES )
    {
        if( partitionSpace.size() == 2 )
        {
            // MODIFY
            // finally get the attributes, and make sure they are loaded in memory
            const std::unique_ptr< std::vector< float > > & xAttr = m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, partitionSpace[ 0 ] );
            const std::unique_ptr< std::vector< float > > & yAttr = m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, partitionSpace[ 1 ] );

            QMatrix4x4 S;
            S.scale( scaling.x(), scaling.y() );

            // now render
            glViewport( 0, 0, scaledWidth(), scaledHeight() );
            glPointSize( pointSize );
            m_renderer->renderPrimitivesFlatSerial(
                *xAttr,
                *yAttr,
                flatProgSerial,
                QVector4D( m_contextColor.x(), m_contextColor.y(), m_contextColor.z(), m_contextOpacity ),
                camera.proj()*camera.view() * M * S,
                GL_POINTS
            );

//            if( m_sampler.hasPointSelection() )
//            {
//                glPointSize( 5.0 );
//                m_renderer->renderPointsIndexed(
//                    *xAttr,
//                    *yAttr,
//                    m_sampler.getHighlightIndices(),
//                    flatProgSerial,
//                    QVector4D( 0, 0, 0, 1 ),
//                    camera.proj()*camera.view() * M * S
//                );
//                glPointSize( 3.0 );
//                m_renderer->renderPointsIndexed(
//                    *xAttr,
//                    *yAttr,
//                    m_sampler.getHighlightIndices(),
//                    flatProgSerial,
//                    QVector4D( 0, 1.0, .35, 1 ),
//                    camera.proj()*camera.view() * M * S
//                );
//            }
        }
    }
    else if ( dataType == PopulationDataType::GRID_POINTS )
    {
        if( partitionSpace.size() == 2 )
        {

            // finally get the attributes, and make sure they are loaded in memory
            const std::vector< float > & xAttr = m_dataSetManager->distributionManager().values( partitionSpace[ 0 ] );
            const std::vector< float > & yAttr = m_dataSetManager->distributionManager().values( partitionSpace[ 1 ] );

            QMatrix4x4 S;
            S.scale( scaling.x(), scaling.y() );

            // now render
            glViewport( 0, 0, scaledWidth(), scaledHeight() );
            glPointSize( pointSize );

            m_renderer->renderPrimitivesFlatSerial(
                xAttr,
                yAttr,
                flatProgSerial,
                QVector4D( m_contextColor.x(), m_contextColor.y(), m_contextColor.z(), m_contextOpacity ),
                camera.proj()*camera.view() * M * S,
                GL_POINTS
            );

            ROIDefinition roiDef = m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.at( m_particleCombo.selectedItemText() ).at(
                m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectedItemText() );

            if( m_samplerWidget.controlPanel.mode == SamplerPanel::Mode::RegionOfInterest )
            {
                glLineWidth( 2 );
                glViewport( 0, 0, scaledWidth(), scaledHeight() );
                m_renderer->renderPrimitivesFlatSerial(
                    std::vector< float >( { roiDef.xROI.a(), roiDef.xROI.b(), roiDef.xROI.b(), roiDef.xROI.a(), roiDef.xROI.a() } ),
                    std::vector< float >( { roiDef.yROI.b(), roiDef.yROI.b(), roiDef.yROI.a(), roiDef.yROI.a(), roiDef.yROI.b() }  ),
                    flatProgSerial,
                    QVector4D( 0.5, 0.5, 0.5, 0.7 ),
                    camera.proj() * camera.view() * M * S,
                    GL_LINE_STRIP
                );

                glPointSize( 2 );
                glViewport( 0, 0, scaledWidth(), scaledHeight() );
                m_renderer->renderPrimitivesFlatSerial(
                    std::vector< float >( { roiDef.xROI.a(), roiDef.xROI.b(), roiDef.xROI.b(), roiDef.xROI.a(), roiDef.xROI.a() } ),
                    std::vector< float >( { roiDef.yROI.b(), roiDef.yROI.b(), roiDef.yROI.a(), roiDef.yROI.a(), roiDef.yROI.b() }  ),
                    flatProgSerial,
                    QVector4D( 0.5, 0.5, 0.5, 0.7 ),
                    camera.proj() * camera.view() * M * S,
                    GL_POINTS
                );
            }

            /****************************************************************************************************/

//            float xmn = TN::Parallel::min( xAttr );
//            float xmx = TN::Parallel::max( xAttr );
//            float ymn = TN::Parallel::min( yAttr );
//            float ymx = TN::Parallel::max( yAttr );

//            float xmn = 2.0;
//            float xmx = 2.07;
//            float ymn = -0.07;
//            float ymx = 0.07;

//            std::vector< float > xtst = { xmn, xmx, xmx, xmn, xmn + ( xmx - xmn ) / 2.f };
//            std::vector< float > ytst = { ymn, ymn, ymx, ymx, ymn + ( ymx - ymn ) / 2.f };

//            glPointSize( 10 );
//            m_renderer->renderPrimitivesFlatSerial(
//                xtst,
//                ytst,
//                flatProgSerial,
//                QVector4D( 0.0, 0.0, 0.0, 1.0 ),
//                camera.proj()*camera.view() * M * S,
//                GL_POINTS
//            );

            /*****************************************************************************************************/
        }
    }
}

void BALEEN::renderVisualContextModels(
    int dataType,
    const std::vector< std::string > & space,
    const std::vector< double > & scaling,
    const QMatrix4x4 & M,
    float lineWidth,
    const Vec2< float > & pos,
    const Vec2< float > & size,
    bool useCamera )
{
    const std::map< std::string, VisualContextModel< float > >  * const backgroundObjects =
        dataType == PopulationDataType::PARTICLES   ? &(     m_dataSetManager->particleDataManager().visualContextModels() ) :
        dataType == PopulationDataType::GRID_POINTS ? &( m_dataSetManager->distributionManager().visualContextModels() )     : nullptr;

    if( backgroundObjects == nullptr )
    {
        return;
    }

    glViewport( pos.x(), pos.y(), size.x(), size.y() );

    QMatrix4x4 S;
    S.scale( scaling[ 0 ],
             scaling[ 1 ] );

    for ( auto & obj : ( * backgroundObjects ) )
    {
        bool visualContextValidInThisSpace = true;
        for( const auto & s : space )
        {
            if( obj.second.coordinates.find( s ) == obj.second.coordinates.end() )
            {
                visualContextValidInThisSpace = false;
                break;
            }
        }
        if( visualContextValidInThisSpace )
        {
            if( space.size() == 2 )
            {
                QMatrix4x4 MVP;
                MVP.setToIdentity();

                if( useCamera )
                {
                    MVP = camera.proj()*camera.view() * M * S;
                }
                else
                {
                    MVP = M * S;
                }

                glLineWidth( lineWidth  );
                glPointSize( lineWidth );

                m_renderer->renderPrimitivesFlatSerial(
                    obj.second.coordinates.find( space[ 0 ] )->second,
                    obj.second.coordinates.find( space[ 1 ] )->second,
                    flatProgSerial,
                    QVector4D( obj.second.color.r*.3, obj.second.color.g*.3, obj.second.color.b*.3, obj.second.color.a ),
                    MVP,
                    obj.second.primitiveType
                );

                if( obj.second.primitiveType == GL_LINES )
                {
                    m_renderer->renderPrimitivesFlatSerial(
                        obj.second.coordinates.find( space[ 0 ] )->second,
                        obj.second.coordinates.find( space[ 1 ] )->second,
                        flatProgSerial,
                        QVector4D( obj.second.color.r*.3, obj.second.color.g*.3, obj.second.color.b*.3, obj.second.color.a ),
                        MVP,
                        GL_POINTS
                    );
                }

                glLineWidth( lineWidth / 3.0 );
                glPointSize( lineWidth / 3.0 );

                if( obj.second.primitiveType == GL_LINES || obj.second.primitiveType == GL_POINTS )
                {
                    m_renderer->renderPrimitivesFlatSerial(
                        obj.second.coordinates.find( space[ 0 ] )->second,
                        obj.second.coordinates.find( space[ 1 ] )->second,
                        flatProgSerial,
                        QVector4D( obj.second.color.r, obj.second.color.g, obj.second.color.b, obj.second.color.a ),
                        MVP,
                        obj.second.primitiveType
                    );
                    if( obj.second.primitiveType == GL_LINES )
                    {
                        m_renderer->renderPrimitivesFlatSerial(
                            obj.second.coordinates.find( space[ 0 ] )->second,
                            obj.second.coordinates.find( space[ 1 ] )->second,
                            flatProgSerial,
                            QVector4D( obj.second.color.r, obj.second.color.g, obj.second.color.b, obj.second.color.a ),
                            MVP,
                            GL_POINTS
                        );
                    }
                }
            }
        }
    }
}

void BALEEN::prepareRegionPhaseTrajectories(
    const TimeSeriesDefinition & trsr,
    const int selectedCell,
    const std::string & ptype )
{
    // already have mapped particles to cells and bins in sampling step
    const std::vector< std::pair< int, unsigned int > > ptclMap = m_sampler.particleMap();

    const int NT = trsr.numSteps();
    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    m_phaseBufferCountsRegion.resize( NP );

    #pragma omp parallel
    for (int i = 0; i < NP; ++i)
    {
        m_phaseBufferCountsRegion[ i ] = ( ptclMap[ i ].first == selectedCell ) * NT;
    }
}

void BALEEN::prepareBinPhaseTrajectories(
    const TimeSeriesDefinition & trsr,
    const std::string & ptype )
{
    // already have mapped particles to cells and bins in sampling step
    const auto & indices = m_sampler.getHighlightIndices();

    const int NT = trsr.numSteps();
    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    m_phaseBufferCountsBin.resize( NP );
    std::fill( m_phaseBufferCountsBin.begin(), m_phaseBufferCountsBin.end(), 0 );

    const int NB = indices.size();

    #pragma omp parallel
    for (int i = 0; i < NB; ++i)
    {
        m_phaseBufferCountsBin[ indices[ i ] ] = NT;
    }
}

// just copying from data manager into a different format, wouldn't be necessary if
// memory was already in this format, but need to weight the tradeoffs first
void BALEEN::preparePhaseTrajectories(
    const std::string & ptype,
    const TimeSeriesDefinition & trsr,
    const PhasePlotDefinition & plotDef )
{
    const int FIRST = trsr.firstIdx;
    const int LAST  = trsr.lastIdx;
    const int STRIDE = trsr.idxStride;
    const int NT = trsr.numSteps();
    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    m_x0Buffer.resize( NP*NT );
    m_x1Buffer.resize( NP*NT );

    const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
    const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );

    #pragma omp parallel for
    for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
    {
        int ti = ( t - FIRST ) / STRIDE;

        const std::vector< float > & xPos = *( xAttr[ t ] );
        const std::vector< float > & yPos = *( yAttr[ t ] );

        for ( unsigned i = 0; i < NP; ++i )
        {
            m_x0Buffer[ i*NT + ti ] = xPos[ i ];
            m_x1Buffer[ i*NT + ti ] = yPos[ i ];
        }
    }

    m_phaseBufferFirsts.resize( NP );
    m_phaseBufferCounts.resize( NP );

    #pragma omp parallel
    for ( int i = 0; i < NP; ++i )
    {
        m_phaseBufferFirsts[ i ] = NT * i;
        m_phaseBufferCounts[ i ] = NT;
    }

    size_t N_BYTES = NP * NT * sizeof( float );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    if ( static_cast< size_t >( m_phaseVBO->size() ) < N_BYTES*2 )
    {
        m_phaseVBO->allocate( N_BYTES*2 );
    }
    checkError( "allocate phase vbo" );

//    //////////////////////qDebug() << m_phaseVBO->size();

    m_phaseVBO->write( 0, m_x0Buffer.data(), N_BYTES );
    m_phaseVBO->write( N_BYTES, m_x1Buffer.data(), N_BYTES );

    m_phaseVBO->release();
    m_phaseVAO->release();
}

void BALEEN::renderPhasePrimitives(
    int viewMode,
    const std::string & interpolation,
    const std::string & ptype,
    const PhasePlotDefinition & plotDef,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    float w,
    float h,
    float x,
    float y,
    const QVector4D & color )
{
    glLineWidth( 4.0 );
    glPointSize( 4.0 );

    glViewport( x, y, w, h );

    QMatrix4x4 M;
    M.setToIdentity();

    Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 0 ] )->second;
    Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 1 ]  )->second;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const float xW = ( rX.b() - rX.a() );
    const float yW = ( rY.b() - rY.a() );

    // scale to (-1,1)
    M.scale( 2.0 / xW, 2.0 / yW );

    // center at (0,0)
    M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

    if( viewMode == PlotViewMode::TRAJECTORIES  )
    {
        flatProgSerial->bind();
        checkError( "bind line map program" );

        GLuint xAttr = flatProgSerial->attributeLocation( "xAttr" );
        GLuint yAttr = flatProgSerial->attributeLocation( "yAttr" );
        checkError( "get attr locations" );

        m_phaseVAO->bind();
        m_phaseVBO->bind();
        checkError( "bind phase vao,vbo" );

        flatProgSerial->setUniformValue( "color", color );
        checkError( "set uniforms" );

        flatProgSerial->setUniformValue( "MVP", M );
        checkError( "set uniforms" );

        flatProgSerial->enableAttributeArray( xAttr );
        flatProgSerial->enableAttributeArray( yAttr );
        checkError( "enable attributes" );

        flatProgSerial->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        flatProgSerial->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );
        checkError( "set attrs" );

        if( interpolation != "None" )
        {
            glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
        }
        glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), NP );

        checkError( "draw multi arrays" );

        flatProgSerial->disableAttributeArray( xAttr );
        flatProgSerial->disableAttributeArray( yAttr );
        checkError( "disable attr" );

        m_phaseVBO->release();
        m_phaseVAO->release();
        checkError( "release vao, vbo" );

        flatProgSerial->release();
        checkError( "release program" );
    }
    else if( viewMode == PlotViewMode::SCATTER_PLOT )
    {
//        const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
//        const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );
//        glPointSize(3.0);
//        m_renderer->renderPointsAsHeatMap(
//           *( xAttr[ m_selectedTimeStep ] ),
//           *( yAttr[ m_selectedTimeStep ] ),
//           M,
//           pointHeatMapGenProg );
    }
}

// can now generalize this since I can pass the texture layer, as well as the counts array to use to switch between all, region, bin
QMatrix4x4 BALEEN::computePhaseTexture(
    PhasePlotCache & cache,
    TextureLayer & result,
    int viewMode,
    const std::string & ptype,
    const PhasePlotDefinition & plotDef,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const std::string & interpolation,
    const std::string & blending,
    const std::string & what,
    int cellSelection )
{
    cache.setTexture( result );
    checkError( "after set texture" );

    cache.bindFrameBuffer();
    checkError( "after bind frame buffer" );

    glEnable( GL_BLEND );
    checkError( "after enable blend" );

    //glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glBlendFunc(GL_ONE, GL_ONE);

    checkError( "after set blend" );

    glLineWidth( 2.0 );
    glPointSize( 2.0 );

    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    checkError( "after set clear color" );

    glViewport( 0, 0, result.width, result.height );
    checkError( ( "after viewport " + std::to_string( result.width ) + " " + std::to_string( result.height ) ).c_str() );

    glClear( GL_COLOR_BUFFER_BIT );
    checkError( "clear color bit" );

    QMatrix4x4 M;
    M.setToIdentity();

    Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 0 ] )->second;
    Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 1 ]  )->second;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const float xW = ( rX.b() - rX.a() );
    const float yW = ( rY.b() - rY.a() );

    // scale to (-1,1)
    M.scale( 2.0 / xW, 2.0 / yW );

    // center at (0,0)
    M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

    if( viewMode == PlotViewMode::TRAJECTORIES && interpolation != "None" )
    {
        lineMapProgram->bind();
        checkError( "bind line map program" );

        GLuint xAttr = lineMapProgram->attributeLocation( "xAttr" );
        GLuint yAttr = lineMapProgram->attributeLocation( "yAttr" );
        checkError( "get attr locations" );

        m_phaseVAO->bind();
        m_phaseVBO->bind();
        checkError( "bind phase vao,vbo" );

        glActiveTexture( GL_TEXTURE0 );
        cache.angleToPixelDensityTexture.bind();
        checkError( "bind angle to density texture" );

        lineMapProgram->setUniformValue( "angleToNormalization", 0 );
        lineMapProgram->setUniformValue( "M", M );
        lineMapProgram->setUniformValue( "width", static_cast< float >( result.width ) );
        lineMapProgram->setUniformValue( "height", static_cast< float >( result.height ) );
        lineMapProgram->setUniformValue( "distanceWeighted", blending == "Number Density" );

        checkError( "set uniforms" );

        lineMapProgram->enableAttributeArray( xAttr );
        lineMapProgram->enableAttributeArray( yAttr );
        checkError( "enable attributes" );

        lineMapProgram->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        lineMapProgram->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );
        checkError( "set attrs" );

        glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
        checkError( "draw multi arrays" );

        lineMapProgram->disableAttributeArray( xAttr );
        lineMapProgram->disableAttributeArray( yAttr );
        checkError( "disable attr" );

        m_phaseVBO->release();
        m_phaseVAO->release();
        checkError( "release vao, vbo" );

        lineMapProgram->release();
        checkError( "release program" );
    }
    else if( viewMode == PlotViewMode::TRAJECTORIES )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear( GL_COLOR_BUFFER_BIT );

        pointHeatMapGenProg->bind();

        GLuint xAttr = pointHeatMapGenProg->attributeLocation( "xAttr" );
        GLuint yAttr = pointHeatMapGenProg->attributeLocation( "yAttr" );

        GLuint loc = pointHeatMapGenProg->uniformLocation( "MVP" );
        pointHeatMapGenProg->setUniformValue( loc, M );

        m_phaseVBO->bind();
        m_phaseVBO->bind();

        pointHeatMapGenProg->enableAttributeArray( xAttr );
        pointHeatMapGenProg->enableAttributeArray( yAttr );

        pointHeatMapGenProg->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        pointHeatMapGenProg->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );

        glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), counts.size() );

        m_phaseVBO->release();
        m_phaseVBO->release();

        pointHeatMapGenProg->release();
    }
    else if( viewMode == PlotViewMode::SCATTER_PLOT )
    {
        const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
        const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );
        glPointSize(3.0);

        if( what == "all" )
        {
            m_renderer->renderPointsAsHeatMap(
                *( xAttr[ m_selectedTimeStep ] ),
                *( yAttr[ m_selectedTimeStep ] ),
                M,
                pointHeatMapGenProg );
        }
        else if( what == "hist" )
        {
            const std::vector< std::pair< int, unsigned int > > & ptclMap = m_sampler.particleMap();

            static std::vector< float > sHistSelectX;
            static std::vector< float > sHistSelectY;

            sHistSelectX.clear();
            sHistSelectY.clear();

            for ( unsigned i = 0, end = ( *( xAttr[ 0 ] ) ).size(); i < end; ++i )
            {
                if( ptclMap[ i ].first != cellSelection )
                {
                    continue;
                }

                sHistSelectX.push_back( ( *( xAttr[ m_selectedTimeStep ] ) )[ i ] );
                sHistSelectY.push_back( ( *( yAttr[ m_selectedTimeStep ] ) )[ i ] );
            }

            m_renderer->renderPointsAsHeatMap(
                sHistSelectX,
                sHistSelectY,
                M,
                pointHeatMapGenProg );
        }
        else if ( what == "bin" )
        {
            m_renderer->renderPointsAsHeatMap(
                *( xAttr[ m_selectedTimeStep ] ),
                *( yAttr[ m_selectedTimeStep ] ),
                m_sampler.getHighlightIndices(),
                M,
                pointHeatMapGenProg );
        }
    }

    result.computeMax();

    cache.releaseFrameBuffer();
    checkError( "after release frame buffer" );

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    return M;
}

void BALEEN::getPhasePlotSizeAndLocation( const PhasePlotDefinition & def, const std::string & ptype, bool includeWeightBreakdown, Vec2< float > & size, Vec2< float > & location )
{
    const int LEGEND_TOP = 60;
    const int LEGEND_HEIGHT = 20;
    const int LEGEND_PAD = 6;

    const float PAD_BOTTOM = 90; //LEGEND_TOP + LEGEND_HEIGHT * 3 + LEGEND_PAD * 5;
    const int PAD_TOP = 30;
    const float PAD_LEFT = includeWeightBreakdown ? m_auxViewPort.width() * .4 : 40;
    float PAD_RIGHT = 40;

    Vec2< float > ulScreen(
        m_auxViewPort.offsetX() + PAD_LEFT,
        m_auxViewPort.offsetY() + m_auxViewPort.height() - PAD_TOP - m_samplerWidget.controlPanel.size().y() );

    Vec2< float > lrScreen(
        m_auxViewPort.offsetX() + m_auxViewPort.width() - PAD_RIGHT,
        m_auxViewPort.offsetY() + PAD_BOTTOM );

    if( def.phasePlotSizeMode == PhasePlotSizeMode::FILL_SPACE )
    {
        if( ( ulScreen.y() - lrScreen.y() ) > ( lrScreen.x() - ulScreen.x() ) )
        {
            lrScreen.y( ulScreen.y() - ( lrScreen.x() - ulScreen.x() ) );
        }
        else
        {
            ulScreen.x( lrScreen.x() - ( ulScreen.y() - lrScreen.y() ) );
        }
    }
    else if ( def.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT )
    {
        Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 0 ] )->second;
        Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 1 ] )->second;

        double dataAspect = ( rX.b() - rX.a() ) / ( rY.b() - rY.a() );
        double viewAspect = ( lrScreen.x() - ulScreen.x() ) / ( ulScreen.y() - lrScreen.y() );

        ////////////////////qDebug() << dataAspect << " " << viewAspect << " " << ulScreen.y() << " " << lrScreen.y() <<  " " << lrScreen.x() << " " << ulScreen.x() << " " << m_auxViewPort.width();

        // if datas aspect is wider than available space
        if( dataAspect > viewAspect )
        {
            // keep origional view width and shrink height
            ulScreen.y( lrScreen.y() + ( lrScreen.x() - ulScreen.x() ) * ( 1.0 / dataAspect ) );
        }
        // datas aspect is narrower
        else
        {
            // keep origional view height and shrink width
            ulScreen.x( lrScreen.x() -( ulScreen.y() - lrScreen.y() ) * dataAspect );
        }
    }

    size = Vec2< float >( lrScreen.x() - ulScreen.x(), ulScreen.y() - lrScreen.y() );
    location = Vec2< float >( ulScreen.x(), m_auxViewPort.offsetY() + m_auxViewPort.height() - PAD_TOP - m_samplerWidget.controlPanel.size().y() - size.y() );
}

void BALEEN::constructLineIndicators(
    const Vec2< float > & linePos,
    const WeightHistogramWidget & weightHistWidget,
    std::vector< TN::Vec2< float > > & lines,
    std::vector< TN::Vec2< float > > & triangles )
{
    const int OFFSET = 12;

    lines =
        std::vector< TN::Vec2< float > > (
    {
        {
            linePos.a(),
            ( weightHistWidget.rangeSelector.position().y() )
        },
        {
            linePos.a(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },

        {
            linePos.b(),
            ( weightHistWidget.rangeSelector.position().y() )
        },
        {
            linePos.b(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },

        {
            linePos.a(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },
        {
            weightHistWidget.rangeSelector.position().x(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },

        {
            linePos.b(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },
        {
            ( weightHistWidget.rangeSelector.position().x() + weightHistWidget.rangeSelector.size().x() ),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },

        {
            weightHistWidget.rangeSelector.position().x(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },
        {
            weightHistWidget.rangeSelector.position().x(),
            ( weightHistWidget.position().y() )
        },

        {
            ( weightHistWidget.rangeSelector.position().x() + weightHistWidget.rangeSelector.size().x() ),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + OFFSET )
        },
        {
            ( weightHistWidget.rangeSelector.position().x() + weightHistWidget.rangeSelector.size().x() ) ,
            ( weightHistWidget.position().y() )
        }
    } );

    const float T_SIZE = 8;

    triangles =
        std::vector< TN::Vec2< float > > (
    {
        {
            linePos.a(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() - 2 )
        },

        {
            linePos.a() - T_SIZE / 2,
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + T_SIZE - 2 )
        },

        {
            linePos.a() + T_SIZE / 2,
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + T_SIZE - 2 )
        },

        {
            linePos.b(),
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() - 2 )
        },

        {
            linePos.b() - T_SIZE / 2,
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + T_SIZE - 2 )
        },

        {
            linePos.b() + T_SIZE / 2,
            ( weightHistWidget.rangeSelector.position().y() + weightHistWidget.rangeSelector.size().y() + T_SIZE - 2 )
        },
    } );

    for( auto & p : lines )
    {
        p.x( p.x() * 2.f /  scaledWidth() - 1 );
        p.y( p.y() * 2.f / scaledHeight() - 1 );
    }

    for( auto & p : triangles )
    {
        p.x( p.x() * 2.f /  scaledWidth() - 1 );
        p.y( p.y() * 2.f / scaledHeight() - 1 );
    }
}

void BALEEN::renderParticleWeightBreakdown(
    bool updateRange,
    bool updateAll,
    const Vec2< float > & plotSize,
    const Vec2< float > & plotPosition,
    const std::string & ptype,
    const std::string & weightKey,
    std::vector< int > & binWeightIds,
    std::vector< int > & histWeightIds )
{
    QVector4D rangeOcclusionColor( 0, 0, 0, .15 );

    static std::vector< float > histWeights;
    static std::vector< float > binWeights;

    const std::vector< float > & weights = *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, weightKey ) );

    const std::vector< std::pair< int, unsigned int > > & ptclMap = m_sampler.particleMap();

    const int CELL_ID = m_sampler.getSelectedCellId();
    const unsigned BIN_ID = m_sampler.getSelectedBinId();

    const int CONNECTOR_H = 10;
    const int RANGE_H = TEXT_ROW_HEIGHT + CONNECTOR_H + 20;

    const int WEIGHT_HIST_HEIGHT = ( m_auxViewPort.height() - m_samplerWidget.controlPanel.size().y() - TEXT_ROW_HEIGHT * 7  - RANGE_H * 3 ) / ( 3 );
    const int WEIGHT_HIST_WIDTH  = plotPosition.x() - m_auxViewPort.offsetX() - 4*TEXT_ROW_HEIGHT;
    const int WEIGHT_HIST_OFFX   = m_auxViewPort.offsetX() + TEXT_ROW_HEIGHT * 2;
    const int WEIGHT_HIST_OFFY   = m_auxViewPort.offsetY() + RANGE_H;

    float selectedBinValB = 0;
    float selectedBinValR = 0;
    float selectedBinValA = 0;

    m_binWeightHistWidget.setSize( WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_regionWeightHistWidget.setSize( WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_allWeightHistWidget.setSize( WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );

    m_binWeightHistWidget.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2 );
    m_regionWeightHistWidget.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) );
    m_allWeightHistWidget.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2 );

    Vec2< double > rW = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( weightKey )->second;

    if( updateRange )
    {
        m_allWeightHistWidget.rangeSelector.setRange( rW );
        m_allWeightHistWidget.rangeSelector.resetSelectedRange();

        m_regionWeightHistWidget.rangeSelector.setRange( rW );
        m_regionWeightHistWidget.rangeSelector.resetSelectedRange();

        m_binWeightHistWidget.rangeSelector.setRange( rW );
        m_binWeightHistWidget.rangeSelector.resetSelectedRange();
    }

    if( updateAll )
    {
        m_allWeightHistWidget.rangeSelector.update( weights );
//        m_allWeightHistWidget.update( weights );
    }

    if( 1 )
    {
        m_allWeightHistWidget.update( weights );
    }

    if( ! updateAll )
    {
        // it would be done in update stage otherwise, and I need to make sure the highlighted bin
        // has the correct color
        m_allWeightHistWidget.remakeGeometry();
    }

    if( m_sampler.hasPointSelection() )
    {
        binWeights.clear();
        binWeightIds.clear();
        histWeights.clear();
        histWeightIds.clear();

        for( unsigned i = 0, end = ptclMap.size(); i < end; ++i )
        {
            if( ptclMap[ i ].first == CELL_ID )
            {
                histWeights.push_back( weights[ i ] );
                histWeightIds.push_back( i );
            }
            if( ptclMap[ i ].second == BIN_ID && ptclMap[ i ].first == CELL_ID )
            {
                binWeights.push_back( weights[ i ] );
                binWeightIds.push_back( i );
            }
        }
        m_regionWeightHistWidget.rangeSelector.update( histWeights );
        m_binWeightHistWidget.rangeSelector.update( binWeights );

        m_regionWeightHistWidget.update( histWeights );
        m_binWeightHistWidget.update( binWeights );
    }
    else if ( m_sampler.hasCellSelection() )
    {
        binWeights.clear();
        binWeightIds.clear();

        histWeights.clear();
        histWeightIds.clear();
        for( unsigned i = 0, end = ptclMap.size(); i < end; ++i )
        {
            if( ptclMap[ i ].first == CELL_ID )
            {
                histWeights.push_back( weights[ i ] );
                histWeightIds.push_back( i );
            }
        }
        m_regionWeightHistWidget.rangeSelector.update( histWeights );
        m_regionWeightHistWidget.update( histWeights );
    }

    /////////////////////////////////////////////////////////////////

    QVector4D weightHistBkgcPos( 201/255.0, 181/255.0, 181/255.0, 1 );
    QVector4D weightHistBkgcNeg( 197/255.0, 203/255.0, 214/255.0, 1 );
    QVector4D weightHistBkgc( .85, .85, .85, 1 );

    // bin weight hist RENDERING ///////////////////////////////////////////////////////////////////////

    glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );

    m_renderer->clear( weightHistBkgc, cartesianProg );
    if( m_sampler.hasPointSelection() && binWeights.size() > 2 )
    {
        if( m_binWeightHistWidget.range().a() < 0 && m_binWeightHistWidget.range().b() > 0 )
        {
            float p = ( 0 - m_binWeightHistWidget.range().a() ) / ( m_binWeightHistWidget.range().b() - m_binWeightHistWidget.range().a()  );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcNeg, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH*(1-p), WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcPos, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->renderPrimitivesColored(
                m_binWeightHistWidget.geometry(),
                m_binWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
            //

            glViewport( 0, 0, scaledWidth(), scaledHeight() );
            m_renderer->renderText( true,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    "0",
                                    WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p - 4,
                                    WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT * 2 - 4,
                                    1.0f,
                                    Vec3< float >( .0, .0, .0 ) );
        }
        else if ( m_binWeightHistWidget.range().a() >= 0 )
        {
            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcPos, cartesianProg );

            m_renderer->renderPrimitivesColored(
                m_binWeightHistWidget.geometry(),
                m_binWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );

        }
        else
        {
            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcNeg, cartesianProg );

            m_renderer->renderPrimitivesColored(
                m_binWeightHistWidget.geometry(),
                m_binWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
        }

        // Bin Pop ...

        const std::vector< float > binVals = m_binWeightHistWidget.bins();
        const int NBINS = binVals.size();

        for( int i = 0; i < NBINS; ++i )
        {
            float x1 = (float) ( i     ) / (float)( NBINS );
            float x2 = (float) ( i + 1 ) / (float)( NBINS );
            float y = binVals[ i ] / m_binWeightHistWidget.maxF();

            if( y > 0 )
            {
                glViewport(
                    WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                    WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT * 2,
                    std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                    std::ceil( std::max( y * WEIGHT_HIST_HEIGHT, 3.f ) ) );
                m_renderer->renderPopSquare( simpleColorProg );
            }

            if( m_binWeightHistWidget.hovered() )
            {
                if( m_binWeightHistWidget.hoveredBin() == i )
                {
                    glViewport(
                        WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                        WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT * 2,
                        std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                        std::ceil( WEIGHT_HIST_HEIGHT ) );
                    m_renderer->renderPopSquare( simpleColorProg );

                    selectedBinValB = m_binWeightHistWidget.binValue( i );
                }
            }
            else if( m_binWeightHistWidget.selected() )
            {
                if( m_binWeightHistWidget.selectedBin() == i )
                {
                    glViewport(
                        WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                        WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT * 2,
                        std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                        std::ceil( WEIGHT_HIST_HEIGHT ) );
                    m_renderer->renderPopSquare( simpleColorProg );

                    selectedBinValB = m_binWeightHistWidget.binValue( i );
                }
            }
        }
    }

    glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_renderer->renderPopSquare( simpleColorProg );

    m_binWeightHistWidget.rangeSelector.setPosition(WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY - CONNECTOR_H );
    m_binWeightHistWidget.rangeSelector.setSize( WEIGHT_HIST_WIDTH, RANGE_H - TEXT_ROW_HEIGHT - CONNECTOR_H  );
    glViewport(
        m_binWeightHistWidget.rangeSelector.position().x(),
        m_binWeightHistWidget.rangeSelector.position().y(),
        m_binWeightHistWidget.rangeSelector.size().x(),
        m_binWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( QVector4D( 1, 1, 1, 1 ), cartesianProg );

    if( m_sampler.hasPointSelection() && binWeights.size() > 2 )
    {
        m_renderer->renderPrimitivesColored(
            m_binWeightHistWidget.rangeSelector.geometry(),
            m_binWeightHistWidget.rangeSelector.colors(),
            1.0,
            simpleColorProg,
            QMatrix4x4(),
            GL_TRIANGLES );
    }

    m_renderer->renderPopSquare( simpleColorProg );
    m_binWeightHistWidget.rangeSelector.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY - CONNECTOR_H );

    Vec2< float > linePosBin = m_binWeightHistWidget.rangeSelector.getRangeSelectionPositions();
    std::vector< Vec2< float > > lineIndicatorsBin;
    std::vector< Vec2< float > > lineIndicatorsArrowBin;
    constructLineIndicators( linePosBin, m_binWeightHistWidget, lineIndicatorsBin, lineIndicatorsArrowBin );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    glLineWidth( 1 );
    glPointSize( 1 );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsBin,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsBin,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_LINES
    );

    m_renderer->renderPrimitivesFlat(
        lineIndicatorsArrowBin,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_TRIANGLES
    );

    glViewport(
        m_binWeightHistWidget.rangeSelector.position().x(),
        m_binWeightHistWidget.rangeSelector.position().y(),
        linePosBin.a() - m_binWeightHistWidget.rangeSelector.position().x(),
        m_binWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    glViewport(
        linePosBin.b(),
        m_binWeightHistWidget.rangeSelector.position().y(),
        ( m_binWeightHistWidget.rangeSelector.position().x() + m_binWeightHistWidget.rangeSelector.size().x() ) - linePosBin.b(),
        m_binWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    // region weight hist ///////////////////////////////////////////////////////////
    glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_renderer->clear( weightHistBkgc, cartesianProg );
    if( m_sampler.hasCellSelection() && histWeights.size() > 2 )
    {
        if( m_regionWeightHistWidget.range().a() < 0 && m_regionWeightHistWidget.range().b() > 0 || 1 )
        {
            float p = 0;

            if( m_regionWeightHistWidget.range().a() < 0 && m_regionWeightHistWidget.range().b() > 0 )
            {
                p = ( 0 - m_regionWeightHistWidget.range().a() ) / ( m_regionWeightHistWidget.range().b() - m_regionWeightHistWidget.range().a()  );

                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ), WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_HEIGHT );
                m_renderer->clear( weightHistBkgcNeg, cartesianProg );

                glViewport( WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ), WEIGHT_HIST_WIDTH*(1-p), WEIGHT_HIST_HEIGHT );
                m_renderer->clear( weightHistBkgcPos, cartesianProg );

                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
                m_renderer->renderPrimitivesColored(
                    m_regionWeightHistWidget.geometry(),
                    m_regionWeightHistWidget.colors(),
                    1.0,
                    simpleColorProg,
                    QMatrix4x4(),
                    GL_TRIANGLES );
            }
            else if ( m_regionWeightHistWidget.range().a() > 0 )
            {
                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
                m_renderer->clear( weightHistBkgcPos, cartesianProg );
                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
                m_renderer->renderPrimitivesColored(
                    m_regionWeightHistWidget.geometry(),
                    m_regionWeightHistWidget.colors(),
                    1.0,
                    simpleColorProg,
                    QMatrix4x4(),
                    GL_TRIANGLES );
            }
            else
            {
                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
                m_renderer->clear( weightHistBkgcNeg, cartesianProg );
                glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
                m_renderer->renderPrimitivesColored(
                    m_regionWeightHistWidget.geometry(),
                    m_regionWeightHistWidget.colors(),
                    1.0,
                    simpleColorProg,
                    QMatrix4x4(),
                    GL_TRIANGLES );
            }

            const std::vector< float > binVals = m_regionWeightHistWidget.bins();
            const int NBINS = binVals.size();

            for( int i = 0; i < NBINS; ++i )
            {
                float x1 = (float) ( i     ) / (float)( NBINS );
                float x2 = (float) ( i + 1 ) / (float)( NBINS );
                float y = binVals[ i ] / m_regionWeightHistWidget.maxF();

                if( y > 0 )
                {
                    glViewport(
                        WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                        WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2+ RANGE_H ),
                        std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                        std::ceil( std::max( y * WEIGHT_HIST_HEIGHT, 3.f ) ) );
                    m_renderer->renderPopSquare( simpleColorProg );
                }

                if( m_regionWeightHistWidget.hovered() )
                {
                    if( m_regionWeightHistWidget.hoveredBin() == i )
                    {
                        glViewport(
                            WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                            WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H),
                            std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                            std::ceil( WEIGHT_HIST_HEIGHT ) );
                        m_renderer->renderPopSquare( simpleColorProg );

                        selectedBinValR = m_regionWeightHistWidget.binValue( i );
                    }
                }
                else if( m_regionWeightHistWidget.selected() )
                {
                    if( m_regionWeightHistWidget.selectedBin() == i )
                    {
                        glViewport(
                            WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                            WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ),
                            std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                            std::ceil( WEIGHT_HIST_HEIGHT ) );
                        m_renderer->renderPopSquare( simpleColorProg );

                        selectedBinValR = m_regionWeightHistWidget.binValue( i );
                    }
                }
            }
            glViewport( 0, 0, scaledWidth(), scaledHeight() );

            if( m_regionWeightHistWidget.range().a() < 0 && m_regionWeightHistWidget.range().b() > 0 )
            {
                m_renderer->renderText( true,
                                        textProg,
                                        scaledWidth(),
                                        scaledHeight(),
                                        "0",
                                        WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p - 4,
                                        WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 - 4 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H),
                                        1.0f,
                                        Vec3< float >( .0, .0, .0 ) );
            }
        }
    }
    glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H), WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_renderer->renderPopSquare( simpleColorProg );

    m_regionWeightHistWidget.rangeSelector.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) - CONNECTOR_H );
    m_regionWeightHistWidget.rangeSelector.setSize( WEIGHT_HIST_WIDTH, RANGE_H - TEXT_ROW_HEIGHT - CONNECTOR_H );

    glViewport( m_regionWeightHistWidget.rangeSelector.position().x(), m_regionWeightHistWidget.rangeSelector.position().y(), m_regionWeightHistWidget.rangeSelector.size().x(), m_regionWeightHistWidget.rangeSelector.size().y() );
    m_renderer->renderPrimitivesColored(
        m_regionWeightHistWidget.rangeSelector.geometry(),
        m_regionWeightHistWidget.rangeSelector.colors(),
        1.0,
        simpleColorProg,
        QMatrix4x4(),
        GL_TRIANGLES );
    m_renderer->renderPopSquare( simpleColorProg );
    m_regionWeightHistWidget.rangeSelector.setPosition( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) - CONNECTOR_H );

    Vec2< float > linePosRegion = m_regionWeightHistWidget.rangeSelector.getRangeSelectionPositions();
    std::vector< Vec2< float > > lineIndicatorsRegion;
    std::vector< Vec2< float > > lineIndicatorsArrowRegion;
    constructLineIndicators( linePosRegion, m_regionWeightHistWidget, lineIndicatorsRegion, lineIndicatorsArrowRegion );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    glLineWidth( 1 );
    glPointSize( 1 );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsRegion,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsRegion,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_LINES
    );

    m_renderer->renderPrimitivesFlat(
        lineIndicatorsArrowRegion,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_TRIANGLES
    );

    glViewport(
        m_regionWeightHistWidget.rangeSelector.position().x(),
        m_regionWeightHistWidget.rangeSelector.position().y(),
        linePosRegion.a() - m_regionWeightHistWidget.rangeSelector.position().x(),
        m_regionWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    glViewport(
        linePosRegion.b(),
        m_regionWeightHistWidget.rangeSelector.position().y(),
        ( m_regionWeightHistWidget.rangeSelector.position().x() + m_regionWeightHistWidget.rangeSelector.size().x() ) - linePosRegion.b(),
        m_regionWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    // all /////////////////////////////////////////////////

    if( m_allWeightHistWidget.range().a() < 0 && m_allWeightHistWidget.range().b() > 0  || 1 )
    {
        float p = 0;

        if( m_allWeightHistWidget.range().a() < 0 && m_allWeightHistWidget.range().b() > 0 )
        {
            p = ( 0 - m_allWeightHistWidget.range().a() ) / ( m_allWeightHistWidget.range().b() - m_allWeightHistWidget.range().a()  );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcNeg, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH*(1-p), WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcPos, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->renderPrimitivesColored(
                m_allWeightHistWidget.geometry(),
                m_allWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
        }
        else if ( m_allWeightHistWidget.range().a() > 0 )
        {
            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcPos, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->renderPrimitivesColored(
                m_allWeightHistWidget.geometry(),
                m_allWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
        }
        else
        {
            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->clear( weightHistBkgcNeg, cartesianProg );

            glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
            m_renderer->renderPrimitivesColored(
                m_allWeightHistWidget.geometry(),
                m_allWeightHistWidget.colors(),
                1.0,
                simpleColorProg,
                QMatrix4x4(),
                GL_TRIANGLES );
        }

        const std::vector< float > binVals = m_allWeightHistWidget.bins();
        const int NBINS = binVals.size();

        for( int i = 0; i < NBINS; ++i )
        {
            float x1 = (float) ( i     ) / (float)( NBINS );
            float x2 = (float) ( i + 1 ) / (float)( NBINS );
            float y = binVals[ i ] / m_allWeightHistWidget.maxF();

            if( y > 0 )
            {
                glViewport(
                    WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                    WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2,
                    std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                    std::ceil( std::max( y * WEIGHT_HIST_HEIGHT, 3.f ) ) );
                m_renderer->renderPopSquare( simpleColorProg );
            }

            if( m_allWeightHistWidget.hovered() )
            {
                if( m_allWeightHistWidget.hoveredBin() == i )
                {
                    glViewport(
                        WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                        WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ) * 2,
                        std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                        std::ceil( WEIGHT_HIST_HEIGHT ) );
                    m_renderer->renderPopSquare( simpleColorProg );

                    selectedBinValA = m_allWeightHistWidget.binValue( i );
                }
            }
            else if(m_allWeightHistWidget.selected())
            {
                if( m_allWeightHistWidget.selectedBin() == i )
                {
                    glViewport(
                        WEIGHT_HIST_OFFX + x1 * WEIGHT_HIST_WIDTH,
                        WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H ) * 2,
                        std::ceil( x2 * WEIGHT_HIST_WIDTH - x1 * WEIGHT_HIST_WIDTH ),
                        std::ceil( WEIGHT_HIST_HEIGHT ) );
                    m_renderer->renderPopSquare( simpleColorProg );

                    selectedBinValA = m_allWeightHistWidget.binValue( i );
                }
            }
        }

        glViewport( 0, 0, scaledWidth(), scaledHeight() );

        if( m_allWeightHistWidget.range().a() < 0 && m_allWeightHistWidget.range().b() > 0 )
        {
            m_renderer->renderText( true,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    "0",
                                    WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH*p - 4,
                                    WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 - 4 + 2 * ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H),
                                    1.0f,
                                    Vec3< float >( .0, .0, .0 ) );
        }
    }


    glViewport( WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY  + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2, WEIGHT_HIST_WIDTH, WEIGHT_HIST_HEIGHT );
    m_renderer->renderPopSquare( simpleColorProg );

    m_allWeightHistWidget.rangeSelector.setPosition(  WEIGHT_HIST_OFFX, WEIGHT_HIST_OFFY + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) * 2 - CONNECTOR_H);
    m_allWeightHistWidget.rangeSelector.setSize( WEIGHT_HIST_WIDTH, RANGE_H - TEXT_ROW_HEIGHT - CONNECTOR_H );

    glViewport( m_allWeightHistWidget.rangeSelector.position().x(), m_allWeightHistWidget.rangeSelector.position().y(), m_allWeightHistWidget.rangeSelector.size().x(), m_allWeightHistWidget.rangeSelector.size().y() );
    m_renderer->renderPrimitivesColored(
        m_allWeightHistWidget.rangeSelector.geometry(),
        m_allWeightHistWidget.rangeSelector.colors(),
        1.0,
        simpleColorProg,
        QMatrix4x4(),
        GL_TRIANGLES );
    m_renderer->renderPopSquare( simpleColorProg );

    Vec2< float > linePosAll = m_allWeightHistWidget.rangeSelector.getRangeSelectionPositions();

    std::vector< Vec2< float > > lineIndicatorsAll;
    std::vector< Vec2< float > > lineIndicatorsAllArrow;
    constructLineIndicators( linePosAll, m_allWeightHistWidget, lineIndicatorsAll, lineIndicatorsAllArrow );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    glLineWidth( 1 );
    glPointSize( 1 );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsAll,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlat(
        lineIndicatorsAll,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_LINES
    );

    m_renderer->renderPrimitivesFlat(
        lineIndicatorsAllArrow,
        cartesianProg,
        QVector4D( .2, .2, .2, 1 ),
        QMatrix4x4(),
        GL_TRIANGLES
    );

    glViewport(
        m_allWeightHistWidget.rangeSelector.position().x(),
        m_allWeightHistWidget.rangeSelector.position().y(),
        linePosAll.a() - m_allWeightHistWidget.rangeSelector.position().x(),
        m_allWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    glViewport(
        linePosAll.b(),
        m_allWeightHistWidget.rangeSelector.position().y(),
        ( m_allWeightHistWidget.rangeSelector.position().x() + m_allWeightHistWidget.rangeSelector.size().x() ) - linePosAll.b(),
        m_allWeightHistWidget.rangeSelector.size().y() );
    m_renderer->clear( rangeOcclusionColor, cartesianProg );

    //////////////

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Bin Weights",
                            WEIGHT_HIST_OFFX,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + WEIGHT_HIST_HEIGHT + 6,
                            1.0f,
                            m_textColor );

    if( m_sampler.hasPointSelection() )
    {
        std::string binMn = to_string_with_precision( std::max( rW.a(), (double)m_binWeightHistWidget.range().a() ) );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                binMn,
                                WEIGHT_HIST_OFFX + 4,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT,
                                1.0f,
                                m_textColor );

        std::string binMx = to_string_with_precision( std::min( rW.b(), (double)m_binWeightHistWidget.range().b() ) );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                binMx,
                                WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - binMx.size()*8,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT,
                                1.0f,
                                m_textColor );

        std::string binMaxF = std::to_string( (int ) m_binWeightHistWidget.maxF() );
        std::string binSelectedF =
            std::to_string( (int ) selectedBinValB ) + "/" + std::to_string( (int) m_binWeightHistWidget.nSamples() )
            + "=" + std::to_string( selectedBinValB / m_binWeightHistWidget.nSamples() );

        if( selectedBinValB > 0 )
        {
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    binSelectedF,
                                    WEIGHT_HIST_OFFX + m_binWeightHistWidget.size().x() - binSelectedF.size()*8,
                                    WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + WEIGHT_HIST_HEIGHT + 6 - 20,
                                    1.0f,
                                    m_textColor );
        }

        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                binMaxF,
                                WEIGHT_HIST_OFFX - 6,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + WEIGHT_HIST_HEIGHT - binMaxF.size() * 7,
                                1.0f,
                                m_textColor,
                                true );

        // Selector / full range ////////////////////////////////////////////////////////////////////////////

        std::string selectorMin = to_string_with_precision( rW.a() );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                selectorMin,
                                WEIGHT_HIST_OFFX + 4,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT - RANGE_H - CONNECTOR_H + 6,
                                1.0f,
                                m_textColor );

        std::string selectorMax = to_string_with_precision( rW.b() );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                selectorMax,
                                WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - selectorMax.size()*8,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT - RANGE_H - CONNECTOR_H + 6,
                                1.0f,
                                m_textColor );
    }

    ///

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Histogram Weights",
                            WEIGHT_HIST_OFFX,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + WEIGHT_HIST_HEIGHT + 6,
                            1.0f,
                            m_textColor );

    if( m_sampler.hasCellSelection() )
    {
        std::string histMn = to_string_with_precision( std::max( rW.a(), (double)m_regionWeightHistWidget.range().a() ) );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                histMn,
                                WEIGHT_HIST_OFFX + 4,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + 6 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H),
                                1.0f,
                                m_textColor );

        std::string histMx = to_string_with_precision( std::min( rW.b(), (double)m_regionWeightHistWidget.range().b() ) );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                histMx,
                                WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - histMx.size()*8,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + 6 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H),
                                1.0f,
                                m_textColor );

        // Selector / full range ////////////////////////////////////////////////////////////////////////////

        std::string selectorMin = to_string_with_precision( rW.a() );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                selectorMin,
                                WEIGHT_HIST_OFFX + 4,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + WEIGHT_HIST_HEIGHT + RANGE_H,
                                1.0f,
                                m_textColor );

        std::string selectorMax = to_string_with_precision( rW.b() );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                selectorMax,
                                WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - selectorMax.size()*8,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + WEIGHT_HIST_HEIGHT + RANGE_H,
                                1.0f,
                                m_textColor );

        ////////////////////////////

        std::string regionMaxF = std::to_string( (int ) m_regionWeightHistWidget.maxF() );
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                regionMaxF,
                                WEIGHT_HIST_OFFX - 6,
                                WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*4 + WEIGHT_HIST_HEIGHT*2 + RANGE_H - regionMaxF.size() * 7,
                                1.0f,
                                m_textColor,
                                true );

        std::string regionSelectedF =
            std::to_string( (int ) selectedBinValR ) + "/" + std::to_string( (int) m_regionWeightHistWidget.nSamples() )
            + "=" + std::to_string( selectedBinValR / m_regionWeightHistWidget.nSamples() );

        if( selectedBinValR > 0 )
        {
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    regionSelectedF,
                                    WEIGHT_HIST_OFFX + m_regionWeightHistWidget.size().x() - regionSelectedF.size()*8,
                                    -20 + WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + WEIGHT_HIST_HEIGHT + 6,
                                    1.0f,
                                    m_textColor );
        }

    }

    ///

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "All Weights",
                            WEIGHT_HIST_OFFX,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + 2 * ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + WEIGHT_HIST_HEIGHT + 6,
                            1.0f,
                            m_textColor );

    std::string allMn = to_string_with_precision( std::max( rW.a(), (double)m_allWeightHistWidget.range().a() ) );
    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            allMn,
                            WEIGHT_HIST_OFFX + 4,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + 2 * ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + 6,
                            1.0f,
                            m_textColor );

    std::string allMx = to_string_with_precision( std::min( rW.b(), (double)m_allWeightHistWidget.range().b() ) );
    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            allMx,
                            WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - allMx.size()*8,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT + 2 * ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + 6,
                            1.0f,
                            m_textColor );

    std::string allMaxF = std::to_string( (int ) m_allWeightHistWidget.maxF() );
    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            allMaxF,
                            WEIGHT_HIST_OFFX - 6,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*6 + WEIGHT_HIST_HEIGHT*3 + RANGE_H*2 - allMaxF.size() * 7,
                            1.0f,
                            m_textColor,
                            true );

    std::string allSelectedF =
        std::to_string( (int ) selectedBinValA ) + "/" + std::to_string( (int) m_allWeightHistWidget.nSamples() )
        + "=" + std::to_string( selectedBinValA / m_allWeightHistWidget.nSamples() );

    if( selectedBinValA > 0 )
    {
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                allSelectedF,
                                WEIGHT_HIST_OFFX + m_allWeightHistWidget.size().x() - allSelectedF.size()*8,
                                -20 + WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*2 + 2 * ( WEIGHT_HIST_HEIGHT + TEXT_ROW_HEIGHT*2 + RANGE_H) + WEIGHT_HIST_HEIGHT + 6,
                                1.0f,
                                m_textColor );
    }

    // Selector / full range ////////////////////////////////////////////////////////////////////////////


    std::string selectorMin = to_string_with_precision( rW.a() );
    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            selectorMin,
                            WEIGHT_HIST_OFFX + 4,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*3 + WEIGHT_HIST_HEIGHT*2 + RANGE_H*2,
                            1.0f,
                            m_textColor );

    std::string selectorMax = to_string_with_precision( rW.b() );
    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            selectorMax,
                            WEIGHT_HIST_OFFX + WEIGHT_HIST_WIDTH - selectorMax.size()*8,
                            WEIGHT_HIST_OFFY + TEXT_ROW_HEIGHT*3 + WEIGHT_HIST_HEIGHT*2 + RANGE_H*2,
                            1.0f,
                            m_textColor );

    ////////
}

void BALEEN::renderPhaseTexture( TextureLayer & layer, Texture1D3 & tf, bool fade, Vec2< float > & position, Vec2< float > & size, bool outline, bool onlyOutline )
{

    glViewport( position.x(), position.y(), size.x(), size.y() );

    /*    glBindFramebuffer( GL_READ_FRAMEBUFFER, multisampledFBO );
        glBindFramebuffer( GL_DRAW_FRAMEBUFFER, normalFBO );
        glBlitFramebuffer( 0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST )*/;

    m_renderer->renderHeatMapTexture( layer.tex, tf, heatMapRenderProg, layer.maxValue, fade, layer.width, layer.height, outline, onlyOutline );
}

void BALEEN::renderPhasePlotGrid(
    const Vec2< float > & plotSize,
    const Vec2< float > & plotPosition,
    const std::string & xLabel,
    const std::string & yLabel,
    const Vec2< double > & rX,
    const Vec2< double > & rY )
{
    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            xLabel,
                            plotPosition.x() + plotSize.x() / 2.0 - 25,
                            plotPosition.y() - 40,
                            1.0f,
                            m_textColor );

    m_renderer->renderText( true,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            yLabel,
                            plotPosition.x() - TEXT_ROW_HEIGHT - 6,
                            plotPosition.y() + plotSize.y() / 2.0 - 20,
                            1.0,
                            m_textColor ,
                            true );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    const int CHARWIDTH = 7.5;

    for( int c = 0; c <= 2; ++c )
    {
        float p = c / 2.f;
        float value = rX.a() + ( rX.b() - rX.a() ) * p;
        std::string text = to_string_with_precision( value, 3 );
        float offset = c == 2 ? text.length() * CHARWIDTH : c == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            text,
            plotPosition.x() + plotSize.x() * p - offset,
            plotPosition.y() - 26,
            1.0f,
            m_textColor );
    }

    for( int r = 0; r <= 2; ++r )
    {
        float p = r / 2.f;
        float value = rY.a() + ( rY.b() - rY.a() ) * p;
        std::string text = to_string_with_precision( value, 3 );
        float offset = r == 2.f ? text.length() * CHARWIDTH : r == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            text,
            plotPosition.x() - 16,
            plotPosition.y() + plotSize.y() * p - offset,
            1.0,
            m_textColor ,
            true );
    }

    int gridSpacing = 20;

    m_renderer->renderGrid(
        plotSize,
        plotPosition,
        gridSpacing,
        QVector4D( 1, 1, 1, 1 ),
        QVector4D( 0.9, 0.9, 0.9, 1 ),
        cartesianProg,
        simpleColorProg );
}

void BALEEN::renderWeightBasedParticleSelection(
    const std::vector< int > & binWeightIds,
    const std::vector< int > & histWeightIds,
    int weightType,
    int viewMode,
    const TimeSeriesDefinition & tsrs,
    const std::string & xAttrS,
    const std::string & yAttrS,
    const std::string & ptype )
{
    if( weightType == HistogramWeightType::VARIABLE )
    {
        const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, xAttrS );
        const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, yAttrS );

        QMatrix4x4 M;
        M.setToIdentity();

        Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( xAttrS )->second;
        Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( yAttrS  )->second;

        const float xW = ( rX.b() - rX.a() );
        const float yW = ( rY.b() - rY.a() );

        // scale to (-1,1)
        M.scale( 2.0 / xW, 2.0 / yW );

        // center at (0,0)
        M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

        if( ( m_sampler.hasCellSelection() || m_sampler.hasPointSelection() ) && viewMode != PlotViewMode::SCATTER_PLOT )
        {
            static std::vector< Vec2< float > > spathLinesSelected;
            spathLinesSelected.clear();

            if( ( m_binWeightHistWidget.hovered() || m_binWeightHistWidget.selected() ) &&  m_sampler.hasPointSelection() )
            {
                int binId = m_binWeightHistWidget.hovered() ? m_binWeightHistWidget.hoveredBin() : m_binWeightHistWidget.selectedBin();

                const std::unordered_set< int > &  idMap = m_binWeightHistWidget.binMap()[ binId ];

                unsigned int first = tsrs.firstIdx;
                unsigned int last  = tsrs.lastIdx;

                for ( unsigned wId = 0; wId < binWeightIds.size(); ++wId )
                {
                    if( ! idMap.count( wId ) )
                    {
                        continue;
                    }
                    for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
                    {
                        const std::vector< float > & xPos = *( xAttr[ t ] );
                        const std::vector< float > & yPos = *( yAttr[ t ] );

                        const Vec2< float > pos2( xPos[ binWeightIds[ wId ] ], yPos[ binWeightIds[ wId ] ] );

                        if ( t == first )
                        {
                            spathLinesSelected.push_back( pos2 );
                        }
                        else if ( t == last )
                        {
                            spathLinesSelected.push_back( pos2 );
                        }
                        else
                        {
                            spathLinesSelected.push_back( pos2 );
                            spathLinesSelected.push_back( pos2 );
                        }
                    }
                }

                glLineWidth(5.0);
                m_renderer->renderPrimitivesFlat(
                    spathLinesSelected,
                    cartesianProg,
                    QVector4D( 0, 0, 0, 1 ),
                    M,
                    GL_LINES
                );

                glLineWidth(2.0);
                m_renderer->renderPrimitivesFlat(
                    spathLinesSelected,
                    cartesianProg,
                    QVector4D( 1, 1, 1, 1.0 ),
                    M,
                    GL_LINES
                );

            }
            else if( ( m_regionWeightHistWidget.hovered() || m_regionWeightHistWidget.selected() ) && m_sampler.hasCellSelection() )
            {
                int binId = m_regionWeightHistWidget.hovered() ? m_regionWeightHistWidget.hoveredBin() : m_regionWeightHistWidget.selectedBin();
                const std::unordered_set< int > &  idMap = m_regionWeightHistWidget.binMap()[ binId ];

                unsigned int first = tsrs.firstIdx;
                unsigned int last  = tsrs.lastIdx;

                for ( unsigned wId = 0; wId < histWeightIds.size(); ++wId )
                {
                    if( ! idMap.count( wId ) )
                    {
                        continue;
                    }

                    for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
                    {
                        const std::vector< float > & xPos = *( xAttr[ t ] );
                        const std::vector< float > & yPos = *( yAttr[ t ] );

                        const Vec2< float > pos2( xPos[ histWeightIds[ wId ] ], yPos[ histWeightIds[ wId ] ] );

                        if ( t == first )
                        {
                            spathLinesSelected.push_back( pos2 );
                        }
                        else if ( t == last )
                        {
                            spathLinesSelected.push_back( pos2 );
                        }
                        else
                        {
                            spathLinesSelected.push_back( pos2 );
                            spathLinesSelected.push_back( pos2 );
                        }
                    }
                }

                glLineWidth(5.0);
                m_renderer->renderPrimitivesFlat(
                    spathLinesSelected,
                    cartesianProg,
                    QVector4D( 0, 0, 0, 1 ),
                    M,
                    GL_LINES
                );

                glLineWidth(2.0);
                m_renderer->renderPrimitivesFlat(
                    spathLinesSelected,
                    cartesianProg,
                    QVector4D( 1, 1, 1, 1.0 ),
                    M,
                    GL_LINES
                );
            }
        }
    }
}

void BALEEN::renderPhasePlot( int dataType, int viewMode, const std::string & ptype, const PhasePlotDefinition & def, int weightType, const std::string & weightKey )
{
//    if( dataType == PopulationDataType::PARTICLES )
//    {
//        if( m_resizeWidgetA.isPressed() || m_resizeWidgetB.isPressed() || m_resizeWidgetC.isPressed() || m_resizeWidgetD.isPressed() )
//        {
//            return;
//        }

//        const TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
//                                          : m_dataSetManager->distributionManager().currentTimeSeries();

//        if( def.attrs.size() == 2 )
//        {
//            float conversionRatio = ( m_upperLeftWindowCornerWorldSpace.y() - m_lowerLeftWindowCornerWorldSpace.y() ) / ( ( float )scaledHeight() );

//            const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, def.attrs[ 0 ] );
//            const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, def.attrs[ 1 ] );

//            //////////////////////////qDebug() << "found attributes";

//            QMatrix4x4 S;
//            int cell = m_sampler.getSelectedCellId();

//            Vec2< float > a( -std::numeric_limits< float >::max(), std::numeric_limits< float >::max() );
//            Vec2< float > d( a );

//            Vec2< float > b( a );
//            Vec2< float > c( a );

//            Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 0 ] )->second;
//            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 1 ]  )->second;

//            Vec2< float > mins(   rX.x(), rY.x() );
//            Vec2< float > maxes ( rX.y(), rY.y() );

//            QMatrix4x4 GridTR;
//            QMatrix4x4 PartTR;

//            Vec2< float > ulScreen, lrScreen;

//            const float PAD_BOTTOM = 50; //m_populationDataTypeEnum == PopulationDataType::PARTICLES ? ( LEGEND_TOP + LEGEND_HEIGHT * 3 + LEGEND_PAD * 5 ) : 40;
//            const int PAD_TOP = 30;//m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  3 * ( TEXT_ROW_HEIGHT*2 + WEIGHT_HIST_HEIGHT ) + TEXT_ROW_HEIGHT : PAD_BOTTOM;;
//            const float PAD_LEFT = ( ( m_populationDataTypeEnum == PopulationDataType::PARTICLES ) && ( weightType == HistogramWeightType::VARIABLE ) ) ? m_auxViewPort.width() * .4 : 40;
//            float PAD_RIGHT = 40;

//            ulScreen = Vec2< float >(
//                           m_auxViewPort.offsetX() + PAD_LEFT,
//                           m_auxViewPort.offsetY() + m_auxViewPort.height() - PAD_TOP - m_samplerWidget.controlPanel.size().y() );

//            lrScreen = Vec2< float >(
//                           m_auxViewPort.offsetX() + m_auxViewPort.width() - PAD_RIGHT,
//                           m_auxViewPort.offsetY() + PAD_BOTTOM );

//            //1: get the desired box coordinates in world space
//            Vec2< float > ul = ulScreen * conversionRatio + m_lowerLeftWindowCornerWorldSpace;
//            Vec2< float > lr = lrScreen * conversionRatio + m_lowerLeftWindowCornerWorldSpace;

//            if( def.phasePlotSizeMode == PhasePlotSizeMode::FILL_SPACE )
//            {
//                if( ( ul.y() - lr.y() ) > ( lr.x() - ul.x() ) )
//                {
//                    lr.y( ul.y() - ( lr.x() - ul.x() ) );
//                    lrScreen.y( ulScreen.y() - ( lrScreen.x() - ulScreen.x() ) );
//                }
//                else
//                {
//                    ul.x( lr.x() - ( ul.y() - lr.y() ) );
//                    ulScreen.x( lrScreen.x() - ( ulScreen.y() - lrScreen.y() ) );
//                }
//            }
//            else if ( def.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT )
//            {
//                double dataAspect = ( rX.b() - rX.a() ) / ( rY.b() - rY.a() );
//                double viewAspect = ( lrScreen.x() - ulScreen.x() ) / ( ulScreen.y() - lrScreen.y() );

//                // if datas aspect is wider than available space
//                if( dataAspect > viewAspect )
//                {
//                    // keep origional view width and shrink height
//                    ul.y( lr.y() + ( lr.x() - ul.x() ) * ( 1.0 / dataAspect ) );
//                    ulScreen.y( lrScreen.y() + ( lrScreen.x() - ulScreen.x() ) * ( 1.0 / dataAspect ) );
//                }

//                // datas aspect is narrower
//                else
//                {
//                    // keep origional view height and shrink width
//                    ul.x( lr.x() - ( ul.y() - lr.y() ) * dataAspect );
//                    ulScreen.x( lrScreen.x() -( ulScreen.y() - lrScreen.y() ) * dataAspect );
//                }
//            }

//            //////////////////////////qDebug() << "computed screen space";

//            glViewport( 0, 0, scaledWidth(), scaledHeight() );

////            m_renderer->renderText( false,
////                                    textProg,
////                                    scaledWidth(),
////                                    scaledHeight(),
////                                    def.attrs[ 0 ],
////                                    ulScreen.x() + ( lrScreen.x() - ulScreen.x() ) / 2.0 - 25,
////                                    lrScreen.y() - 40,
////                                    1.0f,
////                                    m_textColor );

////            m_renderer->renderText( false,
////                                    textProg,
////                                    scaledWidth(),
////                                    scaledHeight(),
////                                    def.attrs[ 1 ],
////                                    ulScreen.x() - TEXT_ROW_HEIGHT - 6,
////                                    lrScreen.y() + ( ulScreen.y() - lrScreen.y() ) / 2.0 - 20,
////                                    1.0,
////                                    m_textColor ,
////                                    true );

//            //3 : translate to  the center of the box
//            Vec2< float > transC (
//                ( lr.x() + ul.x() ) / 2.0,
//                ( lr.y() + ul.y() ) / 2.0 );

//            //2:  Scale, to match
//            Vec2< float > scaleC(
//                ( lr.x() - ul.x() ) / ( rX.b() - rX.a() ),
//                ( ul.y() - lr.y() ) / ( rY.b() - rY.a() ) );

//            //3 : translate to 0
//            Vec2< float > trans0 (
//                -( rX.b() + rX.a() ) / 2.0,
//                -( rY.b() + rY.a() ) / 2.0 );

//            S.translate( transC.x(), transC.y() );
//            S.scale( scaleC.x(), scaleC.y() );
//            S.translate( trans0.x(), trans0.y() );

//            mins  = Vec2< float >( ul.x(), lr.y() );
//            maxes = Vec2< float >( lr.x(), ul.y() );

//            PartTR = camera.proj() * camera.view() * S;
//            GridTR = camera.proj() * camera.view();

//            int COLS = 20;
//            int ROWS = 20;

//            if(  def.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT )
//            {
//                double aspect = ( lrScreen.x() - ulScreen.x() ) / ( ulScreen.y() - lrScreen.y() );
//                COLS *= aspect;
//            }

//            ///////////////////////////////////////////////////////////////////////////////////////////////////

//            const std::vector< std::pair< int, unsigned int > > ptclMap = m_sampler.particleMap();
//            const int CELL_ID = m_sampler.getSelectedCellId();
//            const unsigned BIN_ID = m_sampler.getSelectedBinId();

//            ////////////////////////qDebug() << "got selected bin and cell";

//            ////////////////////////////////////////////////////////////////////////////////////////////////////

//            // BEGIN WEIGHT BREAKDOWN

//            ////////////////////////////////////////////////////////////////////////////////////////////////////

//            static std::vector< int > histWeightIds;
//            static std::vector< float > histWeights;
//            static std::vector< int > binWeightIds;
//            static std::vector< float > binWeights;

//            if( weightType == HistogramWeightType::VARIABLE )
//            {
//                const std::vector< float > & weights = *( m_dataSetManager->particleDataManager().values( m_selectedTimeStep, ptype, weightKey ) );
//                if( m_sampler.hasPointSelection() )
//                {
//                    binWeights.clear();
//                    binWeightIds.clear();
//                    histWeights.clear();
//                    histWeightIds.clear();
//                    for( unsigned i = 0, end = ptclMap.size(); i < end; ++i )
//                    {
//                        if( ptclMap[ i ].first == CELL_ID )
//                        {
//                            histWeights.push_back( weights[ i ] );
//                            histWeightIds.push_back( i );
//                        }
//                        if( ptclMap[ i ].second == BIN_ID && ptclMap[ i ].first == CELL_ID )
//                        {
//                            binWeights.push_back( weights[ i ] );
//                            binWeightIds.push_back( i );
//                        }
//                    }
//                    m_regionWeightHistWidget.update( histWeights, rW );
//                    m_binWeightHistWidget.update( binWeights, rW );
//                }
//            }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    // END WEIGHT BREAKDOWN

    ////////////////////////////////////////////////////////////////////////////////////////////////////


//            glViewport( 0, 0, scaledWidth(), scaledHeight() );

//            const int CHARWIDTH = 7.5;
//            for( int c = 0; c <= COLS; c += COLS / 2 )
//            {
//                float p = c / ( double ) COLS;
//                float value = rX.a() + ( rX.b() - rX.a() ) * p;
//                std::string text = to_string_with_precision( value, 3 );
//                float offset = c == COLS ? text.length() * CHARWIDTH : c == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

//                m_renderer->renderText( false,
//                                        textProg,
//                                        scaledWidth(),
//                                        scaledHeight(),
//                                        text,
//                                        ulScreen.x() + ( lrScreen.x() - ulScreen.x() ) * p - offset,
//                                        lrScreen.y() - 26,
//                                        1.0f,
//                                        m_textColor );
//            }
//            for( int r = 0; r <= ROWS; r += ROWS / 2.0 )
//            {
//                float p = r / ( double ) ROWS;
//                float value = rY.a() + ( rY.b() - rY.a() ) * p;
//                std::string text = to_string_with_precision( value, 3 );
//                float offset = r == ROWS ? text.length() * CHARWIDTH : r == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

//                m_renderer->renderText( false,
//                                        textProg,
//                                        scaledWidth(),
//                                        scaledHeight(),
//                                        text,
//                                        ulScreen.x() - 16,
//                                        lrScreen.y() + ( ulScreen.y() - lrScreen.y() ) * p - offset,
//                                        1.0,
//                                        m_textColor ,
//                                        true );
//            }

//            return;

//            ////////////////////////////////////////////////////

//            // BEGIN SELECTED WEIGHT BIN TRAJECTORIES

//            ///////////////////////////////////////////////////

//            if( weightType == HistogramWeightType::VARIABLE )
//            {
//                if( ( m_sampler.hasCellSelection() || m_sampler.hasPointSelection() ) && viewMode != PlotViewMode::SCATTER_PLOT )
//                {
//                    static std::vector< Vec2< float > > spathLinesSelected;
//                    spathLinesSelected.clear();

//                    if( ( m_binWeightHistWidget.hovered() || m_binWeightHistWidget.selected() ) &&  m_sampler.hasPointSelection() )
//                    {
//                        int binId = m_binWeightHistWidget.hovered() ? m_binWeightHistWidget.hoveredBin() : m_binWeightHistWidget.selectedBin();

//                        const std::unordered_set< int > &  idMap = m_binWeightHistWidget.binMap()[ binId ];

//                        unsigned int first = tsrs.firstIdx;
//                        unsigned int last  = tsrs.lastIdx;

//                        for ( unsigned wId = 0; wId < binWeightIds.size(); ++wId )
//                        {
//                            if( ! idMap.count( wId ) )
//                            {
//                                continue;
//                            }
//                            for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
//                            {
//                                const std::vector< float > & xPos = *( xAttr[ t ] );
//                                const std::vector< float > & yPos = *( yAttr[ t ] );

//                                const Vec2< float > pos2( xPos[ binWeightIds[ wId ] ], yPos[ binWeightIds[ wId ] ] );

//                                if ( t == first )
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                                else if ( t == last )
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                                else
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                            }
//                        }

//                        glLineWidth(5.0);
//                        m_renderer->renderPrimitivesFlat(
//                            spathLinesSelected,
//                            cartesianProg,
//                            QVector4D( 0, 0, 0, 1 ),
//                            PartTR,
//                            GL_LINES
//                        );

//                        glLineWidth(2.0);
//                        m_renderer->renderPrimitivesFlat(
//                            spathLinesSelected,
//                            cartesianProg,
//                            QVector4D( 1, 1, 1, 1.0 ),
//                            PartTR,
//                            GL_LINES
//                        );

//                    }
//                    else if( ( m_regionWeightHistWidget.hovered() || m_regionWeightHistWidget.selected() ) && m_sampler.hasCellSelection() )
//                    {
//                        int binId = m_regionWeightHistWidget.hovered() ? m_regionWeightHistWidget.hoveredBin() : m_regionWeightHistWidget.selectedBin();
//                        const std::unordered_set< int > &  idMap = m_regionWeightHistWidget.binMap()[ binId ];

//                        unsigned int first = tsrs.firstIdx;
//                        unsigned int last  = tsrs.lastIdx;

//                        for ( unsigned wId = 0; wId < histWeightIds.size(); ++wId )
//                        {
//                            if( ! idMap.count( wId ) )
//                            {
//                                continue;
//                            }

//                            for ( unsigned int t = first; t <= last; t += tsrs.idxStride )
//                            {
//                                const std::vector< float > & xPos = *( xAttr[ t ] );
//                                const std::vector< float > & yPos = *( yAttr[ t ] );

//                                const Vec2< float > pos2( xPos[ histWeightIds[ wId ] ], yPos[ histWeightIds[ wId ] ] );

//                                if ( t == first )
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                                else if ( t == last )
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                                else
//                                {
//                                    spathLinesSelected.push_back( pos2 );
//                                    spathLinesSelected.push_back( pos2 );
//                                }
//                            }
//                        }

//                        glLineWidth(5.0);
//                        m_renderer->renderPrimitivesFlat(
//                            spathLinesSelected,
//                            cartesianProg,
//                            QVector4D( 0, 0, 0, 1 ),
//                            PartTR,
//                            GL_LINES
//                        );

//                        glLineWidth(2.0);
//                        m_renderer->renderPrimitivesFlat(
//                            spathLinesSelected,
//                            cartesianProg,
//                            QVector4D( 1, 1, 1, 1.0 ),
//                            PartTR,
//                            GL_LINES
//                        );
//                    }
//                }
//            }
//            ////////////////////////////////////////////////////

//            // END SELECTED WEIGHT BIN TRAJECTORIES

//            ///////////////////////////////////////////////////

//            glViewport( 0, 0, scaledWidth(), scaledHeight() );

//            if( cell >= 0 )
//            {
//                glLineWidth( 3 );
//                m_renderer->renderPrimitivesFlat(
//                    std::vector< Vec2< float > > (
//                {
//                    a, b,
//                    a, c,
//                    d, c,
//                    d, b
//                } ),
//                cartesianProg,
//                QVector4D( .1, .1, 0, 1 ),
//                GridTR,
//                GL_LINES );
//                glLineWidth( 2 );
//                m_renderer->renderPrimitivesFlat(
//                    std::vector< Vec2< float > > (
//                {
//                    a, b,
//                    a, c,
//                    d, c,
//                    d, b
//                } ),
//                cartesianProg,
//                m_boxSelectionColor,
//                GridTR,
//                GL_LINES );
//            }
//        }

//        else if ( def.attrs.size() == 3 )
//        {
//            renderParticleTrace3D(
//                def.attrs,
//                m_sampler.getHighlightIndices(),
//                tsrs,
//                def.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT  );
//        }
//    }
}

void BALEEN::renderSamplingResult( I2 histResolution )
{
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    glPointSize( 2.0 );

    const std::vector< Vec2< float > > & gridCorners = m_sampler.getGridCorners();

    m_histogramGridVertices.clear();
    m_histogramGridColors.clear();

    m_selectedHistogramVertices.clear();
    m_selectedHistogramColors.clear();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        const std::unordered_map< int, std::unordered_map< int, double > > & histogramMap = m_sampler.getHistogramMap();
        std::unordered_map< int, std::unordered_map< int, double > >::const_iterator it;

        double maxF = 0.0;
        if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global )
        {
            for( auto & cell : histogramMap )
            {
                for( auto & bin : cell.second  )
                {
                    maxF = std::max( maxF,  std::abs( bin.second ) );
                }
            }

            m_maxFrequencyInSelectedTS = maxF;
        }
        else if (m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local )
        {
            m_localMaxFrequenciesInSelectedTS.clear();
            for( auto & cell : histogramMap )
            {
                maxF = 0;
                for( auto & bin : cell.second  )
                {
                    maxF = std::max( maxF,  std::abs( bin.second ) );
                }
                m_localMaxFrequenciesInSelectedTS.insert( { cell.first, maxF } );
            }
        }

        for ( int i = 0, k = 0; i < (int)gridCorners.size()-3; i += 4, ++k )
        {
            if ( ( it = histogramMap.find( k ) ) != histogramMap.end() )
            {
                double normalization = 1;
                if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local )
                {
                    normalization = m_localMaxFrequenciesInSelectedTS.find( k )->second;
                }
                else
                {
                    normalization = m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition() * m_maxFrequencyInSelectedTS;
                }

                TN::HIST::appendModelTo2D(
                    color1, color2, color3, color4, color5,
                    colorn2, colorn3, colorn4, colorn5,
                    m_histogramGridVertices,
                    m_histogramGridColors,
                    gridCorners[i  ],
                    gridCorners[i+1],
                    gridCorners[i+2],
                    gridCorners[i+3],
                    histResolution,
                    it->second,
                    normalization,
                    true,
                    false
                );
                if ( k == m_sampler.getSelectedCellId() )
                {
                    TN::HIST::appendModelTo2D(
                        color1, color2, color3, color4, color5,
                        colorn2, colorn3, colorn4, colorn5,
                        m_selectedHistogramVertices,
                        m_selectedHistogramColors,
                        Vec2< float >(  1,  1 ),
                        Vec2< float >(  1, -1 ),
                        Vec2< float >( -1,  1 ),
                        Vec2< float >( -1, -1 ),
                        histResolution,
                        it->second,
                        normalization,
                        true,
                        false
                    );
                }
            }
        }
    }
    else if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        const std::vector< std::vector< int > > & gridMap = m_sampler.gridMap();

        for ( int i = 0, k = 0; i < (int)gridCorners.size()-3; i += 4, ++k )
        {
            if ( gridMap[k].size() )
            {
                double normalization = m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local ?
                                       m_localMaxFrequenciesInSelectedTS.find( k )->second :
                                       m_maxFrequencyInSelectedTS * m_samplerWidget.controlPanel.colorScalingPanel.colorScaleSlider.sliderPosition();

                TN::HIST::appendGridHistModelTo2D(
                    m_histogramGridVertices,
                    m_histogramGridColors,
                    gridCorners[i  ],
                    gridCorners[i+1],
                    gridCorners[i+2],
                    gridCorners[i+3],
                    histResolution,
                    m_mergedGridPointHists.find( k )->second,
                    normalization,
                    m_histTF
                );
                if ( k == m_sampler.getSelectedCellId() )
                {
                    TN::HIST::appendGridHistModelTo2D(
                        m_selectedHistogramVertices,
                        m_selectedHistogramColors,
                        Vec2< float >(  1,  1 ),
                        Vec2< float >(  1, -1 ),
                        Vec2< float >( -1,  1 ),
                        Vec2< float >( -1, -1 ),
                        histResolution,
                        m_mergedGridPointHists.find( k )->second,
                        normalization,
                        m_histTF
                    );
                }
            }
        }
    }

    m_renderer->renderPrimitivesColored(
        m_histogramGridVertices,
        m_histogramGridColors,
        m_samplerWidget.controlPanel.contextPanel.histOpacitySlider.sliderPosition(),
        simpleColorProg,
        camera.proj() * camera.view(),
        GL_TRIANGLES );

//    glPointSize( 4.0 );
//    m_renderer->renderPrimitivesFlat( nonEmptyGridCenters, cartesianProg, QVector4D( 0.0f, 0.0f, 0.0f, 1.0 ), ( camera.proj()*camera.view() ), GL_POINTS );
//    glPointSize( 2.0 );
//    m_renderer->renderPrimitivesFlat( nonEmptyGridCenters, cartesianProg, QVector4D( 1.0f, 1.0f, 1.0f, 1.0 ), ( camera.proj()*camera.view() ), GL_POINTS );
}

void BALEEN::renderComboItems( const ComboWidget & combo )
{
    const int PADY = 8;

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderText(
        false, textProg, scaledWidth(), scaledHeight(),
        combo.text(),
        combo.textX(), combo.textY(),
        1.0,
        m_textColor );

    if( ! combo.isPressed() && combo.numItems() > 0 )
    {
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                combo.items()[ combo.selectedItem() ],
                                combo.position().x() + 8,
                                combo.position().y() + PADY,
                                1.0,
                                m_textColor );
    }
    else if ( combo.numItems() > 0 )
    {
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                combo.items()[ combo.selectedItem() ],
                                combo.position().x() + 8,
                                combo.position().y() + PADY,
                                1.0,
                                m_textColor );

        int offset = 1;

        for( int i = 0; i < combo.numItems(); ++i )
        {
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    combo.items()[ i ],
                                    combo.position().x() + 8,
                                    combo.position().y() - combo.size().y() * offset + PADY,
                                    1.0,
                                    m_textColor );

            offset += 1;
        }
    }
}

void BALEEN::renderTimeSeriesInfo( const std::string & ptype )
{
    std::vector< Vec2< float > > lines;

    // MODIFY

    int allStart, allEnd, allStride, currentStart, currentEnd, currentStride;
    allStart = allEnd = allStride = currentStart = currentEnd = currentStride = 0;
    allStart = 0;
    allEnd = m_numTimeSteps;
    allStride = 1;

    TimeSeriesDefinition def;

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        def = m_dataSetManager->particleDataManager().currentTimeSeries();
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        def = m_dataSetManager->distributionManager().currentTimeSeries();
    }
    else
    {
        return;
    }

    currentStart  = def.firstIdx;
    currentEnd    = def.lastIdx;
    currentStride = def.idxStride;

    double allStartReal, allEndReal, allStrideReal, currentStartReal, currentEndReal, currentStrideReal;
    allStartReal = allEndReal = allStrideReal = currentStartReal = currentEndReal = currentStrideReal = 0;

    int allStartSim, allEndSim, allStrideSim, currentStartSim, currentEndSim, currentStrideSim;
    allStartSim = allEndSim = allStrideSim = currentStartSim = currentEndSim = currentStrideSim = 0;

    const std::vector< double > & realTime = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
            :  m_dataSetManager->distributionManager().realTime();

    const std::vector< std::int32_t > & simTime = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
            :  m_dataSetManager->distributionManager().simTime();
    bool rT=false, sT = false;
    if( realTime.size() >= 2 )
    {
        rT = true;
        allStartReal = realTime.front();
        allEndReal = realTime.back();

        // TODO: check if this is const
        allStrideReal = realTime[ 1 ] - realTime[ 0 ];

        currentStartReal  = realTime[ def.firstIdx ];
        currentEndReal    = realTime[ def.lastIdx ];

        // This too...
        currentStrideReal = realTime[ def.lastIdx ] - realTime[ def.lastIdx - def.idxStride ];
    }
    if( simTime.size() >= 2 )
    {
        sT = true;
        allStartSim = simTime.front();
        allEndSim = simTime.back();

        // TODO: check if this is const
        allStrideSim = simTime[ 1 ] - simTime[ 0 ];

        currentStartSim  = simTime[ def.firstIdx ];
        currentEndSim    = simTime[ def.lastIdx ];

        // This too ...
        currentStrideSim = simTime[ def.lastIdx ] - simTime[ def.lastIdx - def.idxStride ];
    }

    const int OFF_TLD = 40;

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Dataset",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - 16,
                            1.0f,
                            Vec3< float >( .2, .2, .1 ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "                " + m_dataSetManager->name() + ( m_populationDataTypeEnum == PopulationDataType::PARTICLES && m_particleCombo.selectedItemText() != "" ?
                                    ( ", " + std::to_string( m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() ) ) + "/"
                                      + std::to_string(m_dataSetManager->particleDataManager().numParticles( ptype ) ) + " particles" )
                                    : "" ),
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - 16,
                            1.0f,
                            Vec3< float >( 0, 0, 0 ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Time Series",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - 38,
                            1.0f,
                            Vec3< float >( .2, .2, .1 ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "                      (no post-hoc t.s.-integration)",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - 38,
                            1.0f,
                            Vec3< float >( 0, 0, 0 ) );

    float dX = ( m_dataInfoPanelViewPort.width() - 80 ) / 3;
    float dY = 20;
    float offX = m_dataInfoPanelViewPort.offsetX() + 94;
    float offY = m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - OFF_TLD - 14 - dY;

//    lines.push_back( Vec2< float >( offX - 9,   offY - 10 + dY ) );
//    lines.push_back( Vec2< float >( offX - 9, ( offY - 9 - dY*3 ) ) );

//    lines.push_back( Vec2< float >( offX - 116, offY - 9 ) );
//    lines.push_back( Vec2< float >( offX - 9 + dX * 3 - 3, offY - 9 ) );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "first",
//        offX,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allStartReal ) : "NA",
//        offX,
//        offY-dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allStartSim ) : "NA",
//        offX,
//        offY-dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allStart ),
//        offX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "last",
//        offX + dX,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allEndReal ) : "NA",
//        offX + dX,
//        offY-dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allEndSim ) : "NA",
//        offX + dX,
//        offY-dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allEnd ),
//        offX + dX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "stride",
//        offX + dX*2,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allStrideReal ) : "NA",
//        offX + dX*2,
//        offY - dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allStrideSim ) : "NA",
//        offX + dX*2,
//        offY - dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allStride ),
//        offX + dX*2,
//        offY - dY*3,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Real Time",
//        m_timeLineDetailViewPort.offsetX() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Simulation TS",
//        m_timeLineDetailViewPort.offsetX() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Output TS",
//        m_timeLineDetailViewPort.offsetX() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    //

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Time",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - OFF_TLD - dY,
                            1.0f,
                            Vec3< float >( 0, 0, 0 ) );

    offY = m_dataInfoPanelViewPort.offsetY() + m_dataInfoPanelViewPort.height() - OFF_TLD - dY;

    lines.push_back( Vec2< float >( offX - 9,   offY - 10 + dY ) );
    lines.push_back( Vec2< float >( offX - 9, ( offY - 5 - dY*2 ) ) );

    lines.push_back( Vec2< float >( offX - 130, offY - 6 ) );
    lines.push_back( Vec2< float >( offX - 9 + dX * 3 - 6, offY - 6 ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "first",
                            offX,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentStartReal ) : "NA",
                            offX,
                            offY-dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentStartSim ) + "/" + std::to_string( allStartSim ): "NA",
                            offX,
                            offY-dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentStart ),
//        offX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "last",
                            offX + dX,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentEndReal ) : "NA",
                            offX + dX,
                            offY - dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentEndSim ) + "/" + std::to_string( allEndSim ) : "NA",
                            offX + dX,
                            offY - dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentEnd ),
//        offX + dX,
//        offY - dY*3,
//        1.0,
//        m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "stride",
                            offX + dX*2,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentStrideReal ) : "NA",
                            offX + dX*2,
                            offY - dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentStrideSim ) + "/" + std::to_string( allStrideSim ): "NA",
                            offX + dX*2,
                            offY - dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentStride ),
//        offX + dX*2,
//        offY - dY*3,
//        1.0,
//        m_textColor );

    offY -= dY;

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Real",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            offY,
                            1.0,
                            m_textColor );

    offY -= dY;

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Simulation",
                            m_dataInfoPanelViewPort.offsetX() + 10,
                            offY,
                            1.0,
                            m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Output TS",
//        m_timeLineDetailViewPort.offsetX() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Selected : Real,Sim,Out",
//        m_timeLineDetailViewPort.offsetX() + 10,
//        m_timeLineDetailViewPort.offsetY() + 10,
//        1.0,
//        Vec3< float >( 0, 0, 0 ) );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "( "
//        + ( realTime.size() ? to_string_with_precision( realTime[ timeStep ] ) : "NA" )
//        + ", " + ( simTime.size() ? std::to_string( simTime[ timeStep ] ) : "NA" )
//        + ", " + std::to_string( timeStep )
//        + " )",
//        m_timeLineDetailViewPort.offsetX() + 160,
//        m_timeLineDetailViewPort.offsetY() + 10,
//        1.0f,
//        Vec3< float >( m_boxSelectionColor.x(), m_boxSelectionColor.y(), m_boxSelectionColor.z() ) );

    for( auto & p : lines )
    {
        p.x( 2 * p.x() / ( ( double )scaledWidth() ) - 1.0 );
        p.y( 2 * p.y() / ( ( double )scaledHeight() ) - 1.0 );
    }

    glLineWidth( 1 );
    m_renderer->renderPrimitivesFlat( lines, cartesianProg, QVector4D( 0.4, 0.4, 0.4, 1.0 ), QMatrix4x4(),  GL_LINES );
}

void BALEEN::renderTimeLineAnnotation()
{
    TimeSeriesDefinition def;

    const std::vector< std::int32_t > simulationTime =  m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().simTime()
            : m_dataSetManager->distributionManager().simTime();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        def = m_dataSetManager->particleDataManager().currentTimeSeries();
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        def = m_dataSetManager->distributionManager().currentTimeSeries();
    }
    else
    {
        return;
    }

    if( ( def.lastIdx - def.firstIdx ) < 2 )
    {
        return;
    }

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    int NC = 10;

    float xS = m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft();
    float dX = ( float( 1 ) / ( NC - 1 ) ) * m_timeLineWidget.plotSize().x();
    float y =  m_timeLineWidget.position().y() + 16;

    for( int c = 0; c < NC; ++c )
    {
        float x = xS + c * dX;
        std::string valStr;

        int idx = def.firstIdx + ( c / ( double ) ( NC - 1 ) ) * ( def.lastIdx - def.firstIdx );

        valStr = std::to_string( simulationTime[ idx ] );

        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            valStr,
            x - valStr.size() * 7.0 / 2.0,
            y,
            1.0,
            m_textColor );
    }

    std::string tsString( std::to_string( simulationTime[ m_selectedTimeStep ] ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            tsString,
                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() + ( ( ( m_selectedTimeStep - def.firstIdx ) / ( double ) def.idxStride ) / ( double ) ( def.numSteps() - 1  ) )
                            * ( m_timeLineWidget.size().x() - m_timeLineWidget.plotLeft() - m_timeLineWidget.plotRight() )
                            - 7 * tsString.size() / 2.0,
                            m_timeLineWidget.position().y() + m_timeLineWidget.plotBottom() - 14,
                            1.0,
                            m_textColor );
}

void BALEEN::updateSampler( BaseHistDefinition * def )
{
    m_sampler.setHistResolution( getAdjustedHistResolution( { def->resolution[ 0 ], def->resolution[ 1 ] } ) );
}

void BALEEN::snapDataPointsToSamplingWindow( const TN::RegionOfInterest & roi, SpacePartitioningDefinition * spacePartitioning )
{
    const std::vector< Vec2< float > > gridCorners = m_sampler.getGridCorners();
    const int SZ = gridCorners.size() - 3;

    float xWidth = roi.xRange.b() - roi.xRange.a();
    float yWidth = roi.yRange.b() - roi.yRange.a();

    spacePartitioning->preserveAspectRatio = false;

    if( yWidth > 0 )
    {
        spacePartitioning->scaling = { 1.0, xWidth / yWidth  };
    }

    Vec2< double > roiCenter = roi.center();

    roiCenter.x( roiCenter.x() * spacePartitioning->scaling[ 0 ] );
    roiCenter.y( roiCenter.y() * spacePartitioning->scaling[ 1 ] );

    double minDist = std::numeric_limits< double >::max();
    int minCellOffset = 0;
    TN::Vec2< double > minCenter;

    for ( int k = 0; k < SZ; k += 4 )
    {
        const auto & c1 = gridCorners[k];
        const auto & c2 = gridCorners[k+1];
        const auto & c3 = gridCorners[k+2];

        TN::Vec2< double > cellCenter =
        {
            c3.x() + ( c1.x() - c3.x() ) / 2.0,
            c2.y() + ( c1.y() - c2.y() ) / 2.0
        };

        double dist = cellCenter.distance( roiCenter );
        if( dist < minDist )
        {
            minDist = dist;
            minCellOffset = k;
            minCenter = cellCenter;
        }
    }

    m_roiCell = minCellOffset;

    TN::Vec2< float > translation =
    {
        ( minCenter.x() - roiCenter.x() ),
        ( minCenter.y() - roiCenter.y() )
    };

    /*************************************************************************************/

    // sets manual scaling adjustment to a scale factor of 1
    m_samplerWidget.controlPanel.panningWindowPanel.aspectRatioSlider.setSliderPos( 0.5 );

    qDebug() << "roi cell is " << m_roiCell << " out of " << gridCorners.size();

    Vec2< float > a = gridCorners[ m_roiCell + 0 ];
    Vec2< float > b = gridCorners[ m_roiCell + 1 ];
    Vec2< float > c = gridCorners[ m_roiCell + 2 ];

    QVector4D aP = ( camera.proj()*camera.view() ) * QVector4D( a.x(), a.y(), 0, 1.0 );
    QVector4D bP = ( camera.proj()*camera.view() ) * QVector4D( b.x(), b.y(), 0, 1.0 );
    QVector4D cP = ( camera.proj()*camera.view() ) * QVector4D( c.x(), c.y(), 0, 1.0 );

    float x1 = ( aP.x() / 2.0 + .5 ) * scaledWidth();
    float x3 = ( cP.x() / 2.0 + .5 ) * scaledWidth();
    float y1 = ( aP.y() / 2.0 + .5 ) * scaledHeight();
    float y2 = ( bP.y() / 2.0 + .5 ) * scaledHeight();

    float zoom = xWidth / ( x1 - x3 );

    camera.setZoom(    zoom / 2.0 );
    camera.translate( translation );

    QVector3D c1 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1, 1, 0 );
    QVector3D c2 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1, 1, 0 );
    QVector3D c3 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1,-1, 0 );
    QVector3D c4 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1,-1, 0 );

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    m_roiSnapped = true;
}

void BALEEN::snapLayoutToSamplingGrid()
{
    const std::vector< Vec2< float > > gridCorners = m_sampler.getGridCorners();
    I2 dims = m_sampler.dims();
    Vec2< double > gridURSCREEN = worldSpaceToScreenSpace( gridCorners[  ( ( dims.b() - 1 ) * dims.a() + 0 ) * 4 + 0 ] );

    //////////////////qDebug() << "grid right in snap" << gridURSCREEN.x() << " " << gridCorners[  ( ( dims.b() - 1 ) * dims.a() + 0 ) * 4 + 0 ].x();
    ////////////////////qDebug() << "screen width " << scaledWidth();
    ////////////////////qDebug() << "world space to screen " << worldSpaceToScreenSpace( m_upperRightWindowCornerWorldSpace ).x();

    float xFree = std::ceil( ( m_sampler.position().x() + m_sampler.size().x() ) - gridURSCREEN.x() );
    float yFree = std::ceil( ( m_sampler.position().y()
                               + m_sampler.BIG_EDGE_MARGIN )
                             + ( m_sampler.size().y() - m_sampler.BIG_EDGE_MARGIN ) - gridURSCREEN.y() );

    if(  std::abs( m_sampler.size().x() - ( m_sampler.size().y()) ) > m_sampler.SMALL_EDGE_MARGIN )
    {
        if( m_sampler.size().x() > ( m_sampler.size().y()  ) )
        {
            if( ! ( m_resizeWidgetC.isPressed() || m_resizeWidgetD.isPressed() ) )
            {
                if( std::abs( xFree - m_sampler.SMALL_EDGE_MARGIN ) > 1 )
                {
                    ////////////////////qDebug() << "in snap to layout, " << m_resizeWidgetB.position().x() - xFree + m_sampler.SMALL_EDGE_MARGIN << " " << m_resizeWidgetB.position().x() << " " << xFree << " " << m_sampler.SMALL_EDGE_MARGIN;
                    m_resizeWidgetB.setPosition( m_resizeWidgetB.position().x() - xFree + m_sampler.SMALL_EDGE_MARGIN, m_resizeWidgetB.position().y() );
                    m_uiLayoutEdited = true;
                    m_uiLayoutInitialized = true;
                }
            }
        }
        else if ( ! ( m_resizeWidgetA.isPressed() || m_resizeWidgetB.isPressed() ) )
        {
            if( std::abs( yFree - m_sampler.SMALL_EDGE_MARGIN ) > 1 )
            {
                m_resizeWidgetC.setPosition( m_resizeWidgetC.position().x(), m_resizeWidgetC.position().y() + yFree - m_sampler.SMALL_EDGE_MARGIN );
                m_resizeWidgetD.setPosition( m_resizeWidgetD.position().x(), m_resizeWidgetD.position().y() + yFree - m_sampler.SMALL_EDGE_MARGIN );
                m_resizeWidgetB.setPosition( m_resizeWidgetB.position().x(), m_resizeWidgetC.position().y() + DIVIDER_WIDTH );
                m_resizeWidgetB.setSize( m_resizeWidgetB.size().x(), scaledHeight() - m_resizeWidgetC.position().y()
                                         - MENU_HEIGHT - DIVIDER_WIDTH );
                m_uiLayoutEdited = true;
                m_uiLayoutInitialized = true;
            }
        }
    }
}

void BALEEN::renderSamplingLayoutAnnotation( const std::string & spaceX, const std::string & spaceY )
{
    const std::vector< Vec2< float > > gridCorners = m_sampler.getGridCorners();
    I2 dims = m_sampler.dims();

    // background
    glViewport(
        m_sampler.position().x(),
        m_sampler.position().y(),
        m_sampler.BIG_EDGE_MARGIN,
        m_sampler.size().y() );

    qDebug() << m_sampler.position().x() << " "
             << m_sampler.position().y() << " "
             << m_sampler.BIG_EDGE_MARGIN << " "
             << m_sampler.size().y() << " is the viewport or the background";

    m_renderer->clear( QVector4D( .99, .99, .99, .8 ), cartesianProg );

    glViewport(
        m_sampler.position().x() + m_sampler.BIG_EDGE_MARGIN,
        m_sampler.position().y(),
        m_sampler.size().x() - m_sampler.BIG_EDGE_MARGIN,
        m_sampler.BIG_EDGE_MARGIN );
    m_renderer->clear( QVector4D( .99, .99, .99, .8 ), cartesianProg );

    Vec2< double > gridURSCREEN = worldSpaceToScreenSpace( gridCorners[  ( ( dims.b() - 1 ) * dims.a() + 0 ) * 4 + 0 ] );

    glViewport(
        gridURSCREEN.x(),
        std::ceil( m_sampler.position().y() + m_sampler.BIG_EDGE_MARGIN ),
        std::round( ( m_sampler.position().x() + m_sampler.size().x() ) - gridURSCREEN.x() ),
        std::ceil( m_sampler.size().y() - m_sampler.BIG_EDGE_MARGIN ) );
    m_renderer->clear( QVector4D( .99, .99, .99, .8 ), cartesianProg );

    glViewport(
        m_sampler.position().x() + m_sampler.BIG_EDGE_MARGIN,
        std::ceil( gridURSCREEN.y() ),
        std::round( gridURSCREEN.x() - ( m_sampler.position().x() + m_sampler.BIG_EDGE_MARGIN ) ),
        std::ceil( ( m_sampler.position().y() + m_sampler.BIG_EDGE_MARGIN ) + ( m_sampler.size().y() - m_sampler.BIG_EDGE_MARGIN ) - gridURSCREEN.y() ) );
    m_renderer->clear( QVector4D( .99, .99, .99, .8 ), cartesianProg );

    //

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    const int PRINT_PREC = 4;
    float LINE_W = 12;
    float CHAR_W = 7;
    float TEXT_HT = 11;
    float TEXT_PAD = 8;
    float xPos, yPos;
    std::string valStr;

    Vec3< float > cl( .3, .3, .3 );

    Vec2< float > spatialScaling = spatialScaling2D();

    {
        int r = 0;

        // c3
        Vec2< float > val =  gridCorners[ r*4 + 2 ];
        Vec2< double > pos = worldSpaceToScreenSpace( val );

        xPos = pos.x();

        valStr = to_string_with_precision( val.y() / spatialScaling.y(), PRINT_PREC );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            valStr,
            pos.x() - TEXT_PAD - LINE_W,
            pos.y() - valStr.size() * CHAR_W,
            1.0,
            cl,
            true );

        r = dims.a() - 1;

        // c4
        val = gridCorners[ r*4 + 3 ];
        pos = worldSpaceToScreenSpace( val );

        valStr = to_string_with_precision( val.y() / spatialScaling.y(), PRINT_PREC );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            valStr,
            pos.x() - TEXT_PAD - LINE_W,
            pos.y(),
            1.0,
            cl,
            true );

    }
    {
        int c = 0;

        // c4
        Vec2< float > val = gridCorners[ ( c * dims.a() + ( dims.a() - 1 ) ) * 4 + 3 ];
        Vec2< double > pos = worldSpaceToScreenSpace( val );

        yPos = pos.y();

        valStr = to_string_with_precision( val.x() / spatialScaling.x(), PRINT_PREC );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            valStr,
            pos.x(),
            pos.y() - TEXT_HT - TEXT_PAD - LINE_W,
            1.0,
            cl,
            false );
        {
            c = dims.b() - 1;

            // c2
            val = gridCorners[ ( c * dims.a() + ( dims.a() - 1 ) ) * 4 + 1 ];
            pos = worldSpaceToScreenSpace( val );

            valStr = to_string_with_precision( val.x() / spatialScaling.x(), PRINT_PREC );
            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                valStr,
                pos.x() - valStr.size() * CHAR_W,
                pos.y() - TEXT_HT - TEXT_PAD - LINE_W,
                1.0,
                cl,
                false );
        }
    }

    if( m_sampler.hasCellSelection() )
    {
        int cellId = m_sampler.getSelectedCellId();

        Vec2< float > val3 = gridCorners[ cellId * 4 + 2 ];
        Vec2< double > pos3 = worldSpaceToScreenSpace( val3 );

        Vec2< float > val2 = gridCorners[ cellId * 4 + 1 ];
        Vec2< double > pos2 = worldSpaceToScreenSpace( val2 );

        int r = cellId % dims.a();
        int c = cellId / dims.a();

        // y-axis //
        if( r != 0 )
        {
            valStr = to_string_with_precision( val3.y() / spatialScaling.y(), PRINT_PREC );
            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                valStr,
                xPos - TEXT_PAD,
                pos3.y(),
                1.0,
                cl,
                true );
        }
        if ( r != dims.a() - 1 )
        {
            valStr = to_string_with_precision( val2.y() / spatialScaling.y(), PRINT_PREC );
            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                valStr,
                xPos - TEXT_PAD,
                pos2.y() - valStr.size() * CHAR_W,
                1.0,
                cl,
                true );
        }

        // x-axis //
        if( c != 0 )
        {
            valStr = to_string_with_precision( val3.x() / spatialScaling.x(), PRINT_PREC );
            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                valStr,
                pos3.x() - valStr.size() * CHAR_W,
                yPos - TEXT_HT - TEXT_PAD,
                1.0,
                cl,
                false );
        }
        if ( c != dims.b() - 1 )
        {
            valStr = to_string_with_precision( val2.x() / spatialScaling.x(), PRINT_PREC );
            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                valStr,
                pos2.x(),
                yPos - TEXT_HT - TEXT_PAD,
                1.0,
                cl,
                false );
        }
    }

    m_renderer->renderText(
        true,
        textProg,
        scaledWidth(),
        scaledHeight(),
        spaceX,
        m_sampler.position().x() + m_sampler.size().x() / 2.0 - ( spaceX.size() / 2.0 ) * CHAR_W,
        yPos - TEXT_HT - TEXT_PAD - LINE_W,
        1.0,
        m_textColor,
        false );

    m_renderer->renderText(
        true,
        textProg,
        scaledWidth(),
        scaledHeight(),
        spaceY,
        xPos - TEXT_PAD - LINE_W,
        m_sampler.position().y() + m_sampler.size().y() / 2.0 - ( spaceY.size() / 2.0 ) * CHAR_W,
        1.0,
        m_textColor,
        true );
}

void BALEEN::renderTemporalPlot(
    const std::vector< double > & x,
    const std::vector< double > & y,
    const Vec2< double > & yRange,
    const TimeSeriesDefinition & tsrs,
    int selectedTimeStep,
    const QVector4D & color,
    bool offsetCurrentValueText )
{
    int currIdx = ( selectedTimeStep - tsrs.firstIdx ) / tsrs.idxStride;

    double currentY = y[ currIdx ];
    double currentX = x[ currIdx ];

    static std::vector< Vec2< float > > mPath;

    const int NUM_TIME_STEPS = tsrs.numSteps();
    mPath.resize( NUM_TIME_STEPS * 2 - 2 );

    mPath[ 0 ] = Vec2< float>( 0, y[ 0 ] );
    const int LTI = ( NUM_TIME_STEPS - 1 );
    const int LTI2 = LTI*2 - 1;
    mPath[ LTI2 ] = Vec2< float>( LTI, y[ LTI ] );

    #pragma omp parallel for
    for ( int t = 1; t < LTI; ++t )
    {
        const int I2 = t*2;
        const int I1 = I2 - 1;

        mPath[ I1 ] = Vec2< float>( t, y[ t ] );
        mPath[ I2 ] = Vec2< float>( t, y[ t ] );
    }

    float plHT = m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP;
    float pltOffY = m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM;

    glViewport(
        m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT,
        pltOffY,
        m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_LEFT - m_timeLineWidget.MARGIN_RIGHT,
        plHT );

    const int DRAW_SIZE = 4.0;

    glLineWidth( DRAW_SIZE );
    glPointSize( DRAW_SIZE );

    double yWidth = yRange.b() - yRange.a();
    QMatrix4x4 M;
    M.scale( 2.0 / ( NUM_TIME_STEPS-1), 2.0 / yWidth );
    M.translate( -( NUM_TIME_STEPS-1 ) / 2.0,  -( yRange.a() + ( yWidth / 2.0 ) ) );
    glLineWidth( 1 );
    std::vector< Vec2< float > > currentValue = { Vec2< float >( 0, currentY ), Vec2< float >( currentX, currentY ) };

    m_renderer->renderPrimitivesFlat(
        currentValue,
        cartesianProg,
        QVector4D( 0.0, 0.0, 0.0, 1 ),
        M,
        GL_LINES
    );

    glLineWidth( DRAW_SIZE );
    m_renderer->renderPrimitivesFlat(
        mPath,
        cartesianProg,
        color,
        M,
        GL_LINES
    );

    m_renderer->renderPrimitivesFlat(
        mPath,
        cartesianProg,
        color,
        M,
        GL_POINTS
    );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    int offSet = 12;

    if( offsetCurrentValueText )
    {
        offSet += 18;
    }

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            to_string_with_precision( currentY, 4 ),
                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - offSet,
                            m_timeLineWidget.position().y() + pltOffY + plHT * ( currentY - yRange.a() ) / ( yRange.b() - yRange.a() )- 24,
                            1.0,
                            Vec3< float >( color.x(), color.y(), color.z() )*.9,
                            true );
}

void BALEEN::computeTemporalPlots(
    LineData & data,
    const std::string & ptype,
    const TimePlotDefinition & timePlotDef,
    const SpacePartitioningDefinition & spacePartitioning )
{
    std::vector< double > & meanWeights = data.values[ LineData::MEAN ];
    std::vector< double > & meanSquaredWeights = data.values[ LineData::MEAN_SQUARED ];
    Vec2< double > & meanRange = data.ranges[ LineData::MEAN ];
    Vec2< double > & meanSquaredRange = data.ranges[ LineData::MEAN_SQUARED ];

    TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                : m_dataSetManager->distributionManager().currentTimeSeries();

    const int FIRST = tsrs.firstIdx;
    const int LAST  = tsrs.lastIdx;
    const int STRIDE = tsrs.idxStride;

    static std::vector< Vec2< double > > perThreadMeanRange;
    static std::vector< Vec2< double > > perThreadMeanSquaredRange;

    int nThreads;
    #pragma omp parallel
    {
        nThreads = omp_get_num_threads();
    }

    perThreadMeanRange.resize( nThreads );
    perThreadMeanSquaredRange.resize( nThreads );

    #pragma omp parallel
    {
        const int idx = omp_get_thread_num();
        perThreadMeanRange[ idx ] = Vec2< double >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
        perThreadMeanSquaredRange[ idx ] = perThreadMeanRange[ idx ];
    }

    const std::int32_t cellIdx = m_sampler.getSelectedCellId();
    const Vec2< float >  spatialScaling = spatialScaling2D();

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    #pragma omp parallel for
    for( int t = FIRST; t <= LAST; t += STRIDE )
    {
        const int T_ID = omp_get_thread_num();
        const int ti = ( t - FIRST ) / STRIDE;

        const std::vector< float > & xSpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 0 ] ) );
        const std::vector< float > & ySpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 1 ] ) );
        const std::vector< float > & w = *( m_dataSetManager->particleDataManager().values( t, ptype, timePlotDef.attr ) );

        meanWeights[ ti ] = 0.L;
        meanSquaredWeights[ ti ] = 0.L;

        int count = 0;
        for( int i = 0; i < NP; ++i )
        {
            if( m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() ) == cellIdx )
            {
                ++count;
                meanWeights[ ti ] += w[ i ];
                meanSquaredWeights[ ti ] += std::pow( static_cast< double >( w[ i ] ), 2.L );
            }
        }

        meanWeights[ ti ] /= static_cast< double >( count );
        perThreadMeanRange[ T_ID ].a( std::min( perThreadMeanRange[ T_ID ].a(), meanWeights[ ti ] ) );
        perThreadMeanRange[ T_ID ].b( std::max( perThreadMeanRange[ T_ID ].b(), meanWeights[ ti ] ) );

        meanSquaredWeights[ ti ] /= static_cast< double >( count );
        perThreadMeanSquaredRange[ T_ID ].a( std::min( perThreadMeanSquaredRange[ T_ID ].a(), meanSquaredWeights[ ti ] ) );
        perThreadMeanSquaredRange[ T_ID ].b( std::max( perThreadMeanSquaredRange[ T_ID ].b(), meanSquaredWeights[ ti ] ) );
    }

    for( int th = 0; th < nThreads; ++th )
    {
        meanRange.a( std::min( meanRange.a(), perThreadMeanRange[ th ].a() ) );
        meanRange.b( std::max( meanRange.b(), perThreadMeanRange[ th ].b() ) );
        meanSquaredRange.a( std::min( meanSquaredRange.a(), perThreadMeanSquaredRange[ th ].a() ) );
        meanSquaredRange.b( std::max( meanSquaredRange.b(), perThreadMeanSquaredRange[ th ].b() ) );
    }
}

void BALEEN::computeTemporalPlotsAll(
    LineData & data,
    const std::string & ptype,
    const TimePlotDefinition & timePlotDef )
{
    std::vector< double > & meanWeights = data.values[ LineData::MEAN ];
    std::vector< double > & meanSquaredWeights = data.values[ LineData::MEAN_SQUARED ];
    Vec2< double > & meanRange = data.ranges[ LineData::MEAN ];
    Vec2< double > & meanSquaredRange = data.ranges[ LineData::MEAN_SQUARED ];

    TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                : m_dataSetManager->distributionManager().currentTimeSeries();

    const int FIRST = tsrs.firstIdx;
    const int LAST  = tsrs.lastIdx;
    const int STRIDE = tsrs.idxStride;

    static std::vector< Vec2< double > > perThreadMeanRange;
    static std::vector< Vec2< double > > perThreadMeanSquaredRange;

    int nThreads;
    #pragma omp parallel
    {
        nThreads = omp_get_num_threads();
    }

    perThreadMeanRange.resize( nThreads );
    perThreadMeanSquaredRange.resize( nThreads );

    #pragma omp parallel
    {
        const int idx = omp_get_thread_num();
        perThreadMeanRange[ idx ] = Vec2< double >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
        perThreadMeanSquaredRange[ idx ] = perThreadMeanRange[ idx ];
    }

    #pragma omp parallel for
    for( int t = FIRST; t <= LAST; t += STRIDE )
    {
        const int T_ID = omp_get_thread_num();
        const int ti = ( t - FIRST ) / STRIDE;

        const std::vector< float > & w = *( m_dataSetManager->particleDataManager().values( t, ptype, timePlotDef.attr ) );

        meanWeights[ ti ] = 0.L;
        meanSquaredWeights[ ti ] = 0.L;

        const int NP = w.size();
        for( int i = 0; i < NP; ++i )
        {
            meanWeights[ ti ] += w[ i ];
            meanSquaredWeights[ ti ] += std::pow( static_cast< double >( w[ i ] ), 2.L );
        }

        meanWeights[ ti ] /= static_cast< double >( w.size() );
        perThreadMeanRange[ T_ID ].a( std::min( perThreadMeanRange[ T_ID ].a(), meanWeights[ ti ] ) );
        perThreadMeanRange[ T_ID ].b( std::max( perThreadMeanRange[ T_ID ].b(), meanWeights[ ti ] ) );

        meanSquaredWeights[ ti ] /= static_cast< double >( w.size() );
        perThreadMeanSquaredRange[ T_ID ].a( std::min( perThreadMeanSquaredRange[ T_ID ].a(), meanSquaredWeights[ ti ] ) );
        perThreadMeanSquaredRange[ T_ID ].b( std::max( perThreadMeanSquaredRange[ T_ID ].b(), meanSquaredWeights[ ti ] ) );
    }

    for( int th = 0; th < nThreads; ++th )
    {
        meanRange.a( std::min( meanRange.a(), perThreadMeanRange[ th ].a() ) );
        meanRange.b( std::max( meanRange.b(), perThreadMeanRange[ th ].b() ) );
        meanSquaredRange.a( std::min( meanSquaredRange.a(), perThreadMeanSquaredRange[ th ].a() ) );
        meanSquaredRange.b( std::max( meanSquaredRange.b(), perThreadMeanSquaredRange[ th ].b() ) );
    }
}


// updates all of the combo boxes and other ui elements to reflect the
// definitions and parameters currently available for the current ptype
// if a selected parameter or definition for the previous ptype is
// avaiable for the new ptype (according to it's name), then it will
// be selected for the new ptype
void BALEEN::updatePartType()
{
    const std::string ptype = m_particleCombo.selectedItemText();
    const std::string pdf = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();
    const std::string phasePlot = m_phasePlotCombo.selectedItemText();
    const std::string timePlot = m_timePlotCombo.selectedItemText();
    const std::string filter = m_filterCombo.selectedItemText();
    const std::string timeSeries = m_timeSeriesCombo.selectedItemText();

    m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.clear();
    m_phasePlotCombo.clear();
    m_timePlotCombo.clear();
    m_timeSeriesCombo.clear();
    m_filterCombo.clear();

    m_filterCombo.addItem( "None" );

    int selectedIdx = 0;
    int idx = 0;

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        selectedIdx = 0;
        idx = 0;
        for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_userDefinedHistograms.at( ptype ) )
        {
            if( h.second.name == pdf )
            {
                selectedIdx = idx;
            }
            ++idx;

            m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.addItem( h.second.name );
        }
        m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectItem( selectedIdx );

        selectedIdx = 0;
        idx = 0;
        for( auto & tp : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( ptype ) )
        {
            if( tp.second.name == timePlot )
            {
                selectedIdx = idx;
            }
            ++idx;
            m_timePlotCombo.addItem( tp.second.name );
        }
        m_timePlotCombo.selectItem( selectedIdx );

        selectedIdx = 0;
        idx = 0;
        for( auto & pp : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ) )
        {
            if( pp.second.name == phasePlot )
            {
                selectedIdx = idx;
            }
            ++idx;
            m_phasePlotCombo.addItem( pp.second.name );
        }
        m_phasePlotCombo.selectItem( selectedIdx );

        for( auto & f : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( ptype ) )
        {
            m_filterCombo.addItem( f.second.name );
        }
        m_filterCombo.selectByText( filter );
    }
    else if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        selectedIdx = 0;
        idx = 0;
        for( auto & ph : m_currentConfigurations.distributionBasedConfiguration.m_predefinedHistograms.at( ptype ) )
        {
            if( ph.second.name == pdf )
            {
                selectedIdx = idx;
            }
            ++idx;

            m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.addItem( ph.second.name );
        }
        m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectItem( selectedIdx );
    }

    selectedIdx = 0;
    idx = 0;
    for( auto & ts : m_currentConfigurations.timeSeriesDefinitions().at( ptype ) )
    {
        if( ts.second.name == timeSeries )
        {
            selectedIdx = idx;
        }
        ++idx;
        m_timeSeriesCombo.addItem( ts.second.name );
    }
    m_timeSeriesCombo.selectItem( selectedIdx );
}

std::uint8_t BALEEN::getErrorRemovalOption()
{
    std::uint8_t errorRemovalState = 0;
    if( m_errorRemovalCombo.selectedItemText() == "NaN,Inf" )
    {
        errorRemovalState |= ParticleDataManager<float>::REMOVE_INF | ParticleDataManager<float>::REMOVE_NAN;
    }
    else if ( m_errorRemovalCombo.selectedItemText() == "NaN,Inf,Unexp." )
    {
        errorRemovalState |= ParticleDataManager<float>::REMOVE_INF | ParticleDataManager<float>::REMOVE_NAN |
                             ParticleDataManager<float>::REMOVE_UNEXPECTED_VALUES;
    }

    return errorRemovalState;
}

std::string BALEEN::getWarningSummary( int populationDataTypeEnum, const std::string & ptype )
{
    std::string summary;
    std::uint8_t storedGlobalWarningState = 0;

    if( populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        storedGlobalWarningState = m_dataSetManager->particleDataManager().getStoredGlobalWarningState( ptype );
    }
    else
    {

    }

    if( storedGlobalWarningState == 0 )
    {
        return "No Errors Detected";
    }

    summary += "Detected: ";

    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_INF )
    {
        summary += "Inf, ";
    }
    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_NAN )
    {
        summary += "NaN, ";
    }
    if( m_dataSetManager->particleDataManager().getHasZeros( ptype ) )
    {
        summary += "Zero Points, ";
    }
    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_UNEXPECTED )
    {
        summary += "Unexpected Values, ";
    }

    summary.pop_back();
    summary.pop_back();

    return summary;
}

void BALEEN::render()
{
    static std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::system_clock> current = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = ( current - start );
    double elapsed = elapsed_seconds.count() * 3;

    for( auto & combo : m_comboRefs )
    {
        combo->flashPhase( elapsed );
    }

    for( auto & panel : m_samplerWidget.controlPanel.visiblePanels() )
    {
        for( TN::ComboWidget * combo : panel->combos() )
        {
            combo->flashPhase( elapsed );
        }
    }

    m_initiateSessionButton.flashPhase( elapsed );
    m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.flashPhase( elapsed );
    m_definePhasePlotButton.flashPhase( elapsed );
    m_defineTimeSeriesButton.flashPhase( elapsed );
    m_definePlotButton.flashPhase( elapsed );
    m_defineFilterButton.flashPhase( elapsed );
    m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.flashPhase( elapsed );

    if( m_saveDialogOpen == true )
    {
        return;
    }

    const int TEXT_PAD = 8;

    if ( ! m_haveInitializedOpenGL )
    {
        initGL();
        m_haveInitializedOpenGL = true;
        ////////////////////////////qDebug() << "openGL initialized";
    }

    if( m_datasetSelected == false && m_datasetCombo.selectedItemText() != "select dataset" )
    {
        afterSelectedDataset();
    }

    static std::string SpsetName = "select preset";
    std::string presetName = m_configurationCombo.selectedItemText();

    bool presetChange = false;

    if( SpsetName != presetName )
    {
        if( ! m_savedPreset )
        {
            m_loadNewPreset = true;
            presetChange = true;
        }
        else
        {
            m_savedPreset = false;
        }
    }
    SpsetName = presetName;

    if( m_datasetSelected == true && ( m_presetSelected == false || presetChange == true ) && m_configurationCombo.selectedItemText() != "select preset" )
    {
        afterSelectedPreset();
    }

    if( ! m_sessionInitiated )
    {
        renderLater();
    }

    if( scaledHeight() < 700 )
    {
        return;
    }

    if( m_uiLayoutEdited )
    {
        invalidateSampling();
        recalculateUiLayout();
        m_recalculatePhasePlot = true;

        m_binWeightHistWidget.clearSelection();
        m_regionWeightHistWidget.clearSelection();
        m_allWeightHistWidget.clearSelection();
    }

    recalculateSamplingLayout();

    /////////////////////////////////////////////////////////////////////////////

    static bool userIsPanning = false;
    static QPointF lastMousePos = m_previousMousePosition;

    if( ( m_previousMousePosition != lastMousePos )
       && m_sampler.pointInViewPort( Vec2< float >( lastMousePos.x(), scaledHeight() - lastMousePos.y() ) )
       && m_mouseIsPressed )
    {
        userIsPanning = true;
    }
    else if( userIsPanning && ! m_mouseIsPressed )
    {
        userIsPanning = false;
    }

    lastMousePos = m_previousMousePosition;

    static std::string previousPtype = m_particleCombo.selectedItemText();
    const std::string ptype = m_particleCombo.selectedItemText();

    bool activePTypeChanged = previousPtype != ptype;
    if( activePTypeChanged && m_presetSelected )
    {
        updatePartType();
        previousPtype = ptype;
    }

    if( m_presetSelected )
    {
        if( ! m_timeSeriesDefined )
        {
            if( m_timeSeriesCombo.numItems() <= 0 )
            {
                m_defineTimeSeriesButton.flash( true );
            }
            else
            {
                m_timeSeriesDefined = true;
                m_defineTimeSeriesButton.flash( false );
            }
        }

        if( ! m_histogramDefined )
        {
            if( m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.numItems() <= 0 )
            {
                m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.flash( true );
            }
            else
            {
                m_histogramDefined = true;
                m_samplerWidget.controlPanel.histSelectionPanel.addDistributionButton.flash( false );
            }
        }

        if( m_timePlotCombo.numItems() <= 0 )
        {
            m_definePlotButton.flash( true );
        }
        else
        {
            m_definePlotButton.flash( false );
        }

        if( m_phasePlotCombo.numItems() <= 0 )
        {
            m_definePhasePlotButton.flash( true );
        }

        else
        {
            m_definePhasePlotButton.flash( false );
        }

        if( m_forceSpacePartitioningSelection )
        {
            m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.flash( true );
        }

        else
        {
            m_samplerWidget.controlPanel.histSelectionPanel.spatialButton.flash( false );
        }
    }

    static std::string dataS = m_datasetCombo.selectedItemText();
    std::string _ds = m_datasetCombo.selectedItemText();

    if( _ds != dataS && m_presetSelected )
    {
        datasetSwitched();
    }
    else if( _ds != dataS )
    {
        afterSelectedDataset();
    }

    dataS = _ds;

    const std::string dtype = m_samplerWidget.controlPanel.histSelectionPanel.distributionCombo.selectedItemText();
    const std::string linkedViewModeStr = m_phasePlotModeCombo.selectedItemText();

    if( dtype != "" )
    {
        m_populationDataTypeEnum = m_dataSetManager->dataType() == "particle" ?
                                   PopulationDataType::PARTICLES :
                                   PopulationDataType::GRID_POINTS;
    }

    const int linkedViewModeEnum =
        linkedViewModeStr == "Trajectories" ? PlotViewMode::TRAJECTORIES :
        linkedViewModeStr == "Points"       ? PlotViewMode::SCATTER_PLOT : -1;

    static int lastLinkedViewMode =
        linkedViewModeStr == "Trajectories" ? PlotViewMode::TRAJECTORIES :
        linkedViewModeStr == "Points"       ? PlotViewMode::SCATTER_PLOT : -1;

    static int SCurrentTimeStep = m_selectedTimeStep;
    bool timeStepChanged = m_selectedTimeStep != SCurrentTimeStep;
    SCurrentTimeStep = m_selectedTimeStep;

    SpacePartitioningDefinition * spacePartitioning = nullptr;

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        auto it = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype );
        if( it != m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.end() )
        {
            auto it2 = it->second.find( m_phasePlotCombo.selectedItemText() );
            if( it2 != it->second.end() )
            {
                it2->second.phasePlotTypeId = linkedViewModeEnum;
            }
        }
        if( m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype ) != m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.end() )
        {
            spacePartitioning =  & m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second;
        }
    }
    else
    {
        if( m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype ) != m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.end() )
        {
            spacePartitioning = & m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second;
        }
    }

    if( ( timeStepChanged && linkedViewModeEnum == PlotViewMode::SCATTER_PLOT ) || lastLinkedViewMode != linkedViewModeEnum )
    {
        m_recalculatePhasePlot = true;
    }

    BaseHistDefinition *  histDefinition = validateCurrentDefinition( m_populationDataTypeEnum, ptype, dtype );
    LinkedViewDefinition * linkedViewDef = getLinkedViewDefinition( m_populationDataTypeEnum, ptype, dtype, linkedViewModeEnum );
    TimePlotDefinition   * timePlotDef   = getTimePlotDefinition( m_populationDataTypeEnum, ptype, dtype );

    static TimeSeriesDefinition timeSeriesS;
    TimeSeriesDefinition * timeSeries = getTimeSeries( ptype );

    static std::set< std::string > variablesInUseS = {};
    std::set< std::string > variablesOrPrecomputedPDFSInUse
        = getVariablesInUse(
              histDefinition,
              spacePartitioning,
              linkedViewDef,
              timePlotDef,
              linkedViewModeEnum,
              m_populationDataTypeEnum );

    qDebug() << "checking memory state";

    // conditions to test to determine if in core memory needs to be altered
    if( timeSeries != nullptr && histDefinition != nullptr && m_sessionInitiated && spacePartitioning != nullptr )
    {
        qDebug() << "pulling from disk";

        const std::string filter = m_filterCombo.selectedItemText();

        bool activeVariablesChanged = variablesOrPrecomputedPDFSInUse != variablesInUseS;

        bool activeTimeSeriesChanged = ! ( timeSeriesS == (*timeSeries) );

        if( activeTimeSeriesChanged )
        {
            ////////qDebug() << "Time Series Changed";
        }

        bool activeFilterChanged = filter != m_previousFilter;

        static std::uint8_t lastErrorRemovalOption = getErrorRemovalOption();

        ////////qDebug() << "getting error removal option";
        std::uint8_t errorRemovalOption = getErrorRemovalOption();

        bool errorRemovalOptionChanged = false;
        if( lastErrorRemovalOption != errorRemovalOption )
        {
            errorRemovalOptionChanged = true;
        }

        ////////qDebug() << "checking error state";
        bool hasErrors = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().getStoredGlobalWarningState( ptype ) : false;
        bool needToUpdateErrorRemove = hasErrors && ( errorRemovalOptionChanged != 0 );

        if( m_loadNewPreset || activeVariablesChanged || activeTimeSeriesChanged || activeFilterChanged || activePTypeChanged || needToUpdateErrorRemove || m_filterEdited )
        {
            qDebug() << "updating memory";

            m_loadNewPreset = false;

            std::pair< std::string, std::string > loadTimeLog;
            loadTimeLog.first = getTimeStamp();

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {

                qDebug() << "it's particle data";

                m_dataSetManager->particleDataManager().setErrorRemovalOption( errorRemovalOption );

                if( activeFilterChanged || m_filterEdited )
                {
                    if( filter != "None" )
                    {
                        m_dataSetManager->particleDataManager().setFilter( ptype, m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( ptype ).at( filter ) );
                    }
                    else
                    {
                        // filters are default constructed as null filters
                        m_dataSetManager->particleDataManager().setFilter( ptype, ParticleFilter() );

                    }
                }

                bool recalculateFilter = m_filterEdited || activeFilterChanged;
                m_filterEdited = false;

                qDebug() << "recalculating filter ? " << recalculateFilter;

                // in this case, we will need to reload from disk, because what's in memory has been effected by filtering
                bool invalidateCurrentlyHeldData = recalculateFilter;

                std::atomic< int > filterProgress( 0 );
                std::atomic< int > derivePrepareProgress( 0 );
                std::atomic< int > deriveCalculateProgress( 0 );
                std::atomic< int > baseProgress( 0 );
                std::atomic< int > NaNCheckProgress( 0 );
                std::atomic< int > InfCheckProgress( 0 );
                std::atomic< int > ZeroCheckProgress( 0 );
                std::atomic< int > ErrorRemovalProgress( 0 );
                std::atomic< int > rangeProgress( 0 );

                std::atomic< bool > importStatus( false );
                qDebug() << "before launching loading thread";

                std::thread td( static_cast<void (ParticleDataManager<float>::*)(
                                    const std::string &,
                                    const std::set< std::string > &,
                                    const TimeSeriesDefinition & timeSeries,
                                    bool,
                                    bool,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< bool > & )>(
                                    &ParticleDataManager<float>::load ),
                                &( m_dataSetManager->particleDataManager() ),
                                std::ref( ptype ),
                                std::ref( variablesOrPrecomputedPDFSInUse ),
                                std::ref( *timeSeries ),
                                invalidateCurrentlyHeldData,
                                recalculateFilter,
                                std::ref( filterProgress ),
                                std::ref( derivePrepareProgress ),
                                std::ref( deriveCalculateProgress ),
                                std::ref( baseProgress ),
                                std::ref( NaNCheckProgress ),
                                std::ref( InfCheckProgress ),
                                std::ref( ZeroCheckProgress ),
                                std::ref( ErrorRemovalProgress ),
                                std::ref( rangeProgress ),
                                std::ref( importStatus ) );

                std::atomic< int > currentProgress( 0 );

                qDebug() << "filtering";
                while( filterProgress < 100 )
                {
                    if( currentProgress < filterProgress )
                    {
                        currentProgress.exchange( filterProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Filtering: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                qDebug() << "deriving";
                while( derivePrepareProgress < 100 )
                {
                    if( currentProgress < derivePrepareProgress )
                    {
                        currentProgress.exchange( derivePrepareProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Loading to Derive: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                qDebug() << "done loading for derivation";

                m_renderer->renderLoadingScreen(
                    textProg,
                    cartesianProg,
                    simpleColorProg,
                    scaledWidth(),
                    scaledHeight(),
                    "Calculating Derivations: ",
                    0 );
                swapBuffers();

                while( deriveCalculateProgress < 100 )
                {
                    if( currentProgress < deriveCalculateProgress )
                    {
                        currentProgress.exchange( deriveCalculateProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Calculating Derivations: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                m_renderer->renderLoadingScreen(
                    textProg,
                    cartesianProg,
                    simpleColorProg,
                    scaledWidth(),
                    scaledHeight(),
                    "Loading: ",
                    0 );
                swapBuffers();

                while( baseProgress < 100 )
                {
                    if( currentProgress < baseProgress )
                    {
                        currentProgress.exchange( baseProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Loading: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_NAN )
                {
                    while( NaNCheckProgress < 100 )
                    {
                        if( currentProgress < NaNCheckProgress )
                        {
                            currentProgress.exchange( NaNCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for NaN: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_INF )
                {
                    while( InfCheckProgress < 100 )
                    {
                        if( currentProgress < InfCheckProgress )
                        {
                            currentProgress.exchange( InfCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for inf: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_ZEROS )
                {
                    while( ZeroCheckProgress < 100 )
                    {
                        if( currentProgress < ZeroCheckProgress )
                        {
                            currentProgress.exchange( ZeroCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for Zeros: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }
                currentProgress = 0;

                if( errorRemovalOption != 0 )
                {
                    while( ErrorRemovalProgress < 100 )
                    {
                        if( currentProgress < ErrorRemovalProgress )
                        {
                            currentProgress.exchange( ErrorRemovalProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Removing Bad Data: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }
                currentProgress = 0;

                while( rangeProgress < 100 || importStatus == false )
                {
                    if( currentProgress < rangeProgress )
                    {
                        currentProgress.exchange( rangeProgress );

                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Recalculating Ranges: ",
                            currentProgress );

                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }

                td.join();

                qDebug() << "done loading everyuting";

                renderLater();
            }

            else if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
            {
                qDebug() << "going to disk again for grid points";

                std::atomic< bool > importStatus( false );
                std::atomic< int > importProgress( 0 );

                std::thread td( static_cast<void (DistributionGridManager<float>::*)(
                                    const std::string &,
                                    const std::set< std::string > &,
                                    const TimeSeriesDefinition & timeSeries,
                                    std::atomic< int > &,
                                    std::atomic< bool > & )>(
                                    &DistributionGridManager<float>::load ),
                                &( m_dataSetManager->distributionManager() ),
                                std::ref( ptype ),
                                std::ref( variablesOrPrecomputedPDFSInUse ),
                                std::ref( *timeSeries ),
                                std::ref( importProgress ),
                                std::ref( importStatus ) );

                int currentProgress = 0;
                while( importStatus == false )
                {
                    if( importProgress > currentProgress )
                    {
                        currentProgress = importProgress;
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Loading: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
                }

                td.join();

                renderLater();
            }

            //////////////////////////////////////////

            loadTimeLog.second = getTimeStamp();
            m_loadingLogs.push_back( loadTimeLog );
            if( m_trackState )
            {
                pushState();
            }

            /////////////////////////////////////////

            variablesInUseS = variablesOrPrecomputedPDFSInUse;
            timeSeriesS = *timeSeries;
            m_selectedTimeStep = timeSeries->firstIdx;

            qDebug() << "before update time steps";

            std::vector< float > tSteps( timeSeries->numSteps() );
            std::vector< float > tCodomain( timeSeries->numSteps(), .5 );
            for( int i = timeSeries->firstIdx, k = 0; i <= timeSeries->lastIdx; i += timeSeries->idxStride )
            {
                tSteps[ k++ ] = i;
            }
            m_timeLineWidget.update( tSteps, tCodomain, Vec2< float >( 0, 1 ) );

            m_recalculatePhasePlot = true;
            m_recalculateFullTimePlot = true;
            m_binWeightHistWidget.clearSelection();
            m_regionWeightHistWidget.clearSelection();
            m_allWeightHistWidget.clearSelection();

            m_previousFilter = m_filterCombo.selectedItemText();
            previousPtype = ptype;
            m_reloadGPUData = true;

            invalidateSampling();
            timeStepChanged = true;

            qDebug() << "done loading memory";
        }
        else
        {
            qDebug() << "not going to update, need more definitions";
        }

        // histDefEditied is true if a new definition was created, or an existing one had been edited
        // the below code needs to happen after the data has been loaded, because prior to that the
        // variables ranges have not been calculated

        // likewise, if the memory has been edited, then maybe a new range of time steps has been
        // chosen, for example, and the variables ranges have potentially changed, so the definitions
        // ranges should be updated

        // int the future, it might be better to simply not store the ranges in the hist definition,
        // but for now it's easier because several other changes would need to be made otherwise

        if( m_histDefinitionEdited || activeVariablesChanged || activeTimeSeriesChanged || activeFilterChanged || activePTypeChanged )
        {
            qDebug() << "updating value ranges for hist definitions";

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                qDebug() << histDefinition->binningSpace.size();
                qDebug() << histDefinition->valueRange.size();

                for( unsigned i = 0, end = histDefinition->binningSpace.size(); i < end; ++i )
                {
                    qDebug() << histDefinition->binningSpace[ i ].c_str();
                    histDefinition->valueRange[ i ] = m_dataSetManager->particleDataManager().attributeRanges().at( ptype ).at( histDefinition->binningSpace[ i ] );
                }

                qDebug() << ( (HistogramDefinition*)histDefinition)->weight.c_str();
            }

            qDebug() << "updating partition space scaling";

            for( unsigned i = 0, end = spacePartitioning->attributes.size(); i < end; ++i )
            {
                if( spacePartitioning->preserveAspectRatio )
                {
                    spacePartitioning->scaling[ i ] = 1.0;
                }
                else
                {
                    Vec2< double > spatialRange = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                                  m_dataSetManager->particleDataManager().attributeRanges().at( ptype ).at( spacePartitioning->attributes[ i ] )
                                                  : m_dataSetManager->distributionManager().range( spacePartitioning->attributes[ i ] );

                    spacePartitioning->scaling[ i ] = 1.0 / ( spatialRange.b() - spatialRange.a() );
                }
            }

            m_histDefinitionEdited = false;
            invalidateSampling();

            if( m_dataInitializedFirst == false )
            {
                qDebug() << "updating things";
                updateBoundingBoxes( *spacePartitioning, m_populationDataTypeEnum, ptype );
                resetSamplerCam();
                m_uiLayoutEdited = true;
                m_dataInitializedFirst = true;
                renderLater();
                return;
            }
        }
    }
    else
    {
        qDebug() << "not ready";
    }

    //////////////////////////////////////////////////////////////////

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    m_renderer->clearViewPort( m_samplerWidget.visualizationWindow.viewPort(), cartesianProg );

    qDebug() << "cleared viewort";

    ///////////////////////////////////////////////////////////////////////

    static Vec2< double > spatialScaling( 1.0, 1.0 );

    I2 histResolution;

    if( histDefinition != nullptr && timeSeries != nullptr && m_sessionInitiated && spacePartitioning != nullptr )
    {
        updateBoundingBoxes( *spacePartitioning, m_populationDataTypeEnum, ptype );
        updateSampler( histDefinition );

        if( ! m_samplerWidget.controlPanel.roiScalePanel.lodSlider.isPressed() )
        {
            snapLayoutToSamplingGrid();
        }

        if( m_samplerWidget.controlPanel.mode == SamplerPanel::Mode::RegionOfInterest
                && ! m_mouseIsPressed
                && true ) // m_samplerWidget.controlPanel.roiPanel.roiCombo.numItems() > 0 )
        {
            ROIDefinition roiDef = m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.at( ptype ).at(
                m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectedItemText() );

            if( roiDef.mode == 1 )
            {
                snapDataPointsToSamplingWindow( { roiDef.xROI, roiDef.yROI }, spacePartitioning );
                qDebug() << "snapped";
                updateBoundingBoxes( *spacePartitioning, m_populationDataTypeEnum, ptype );
                qDebug() << "updated bounding boxes";
                recalculateSamplingLayout();
                qDebug() << "calculated sampling layout";
            }
        }

        histResolution = getAdjustedHistResolution( { histDefinition->resolution[ 0 ], histDefinition->resolution[ 1 ] } );
        spatialScaling = spatialScaling2D();
        updateSelectedHistGridLines( std::vector< int >( histResolution.a(), histResolution.b() ) );

        qDebug() << "updated grid lines";
        qDebug() << "updated sampler";

        //////////////qDebug() << spatialScaling.x() << " " << spatialScaling.y();

        renderPopulation(
            m_populationDataTypeEnum,
            spacePartitioning->attributes,
        { spatialScaling.x(), spatialScaling.y() },
        QMatrix4x4(), 3.0 );

        renderVisualContextModels(
            m_populationDataTypeEnum,
            spacePartitioning->attributes,
        { spatialScaling.x(), spatialScaling.y() },
        QMatrix4x4(),
        4.0,
        {0,0}, { scaledWidth(), scaledHeight() } );

        qDebug() << "rendered visual context";

        samplePopulation( histDefinition, *spacePartitioning, m_populationDataTypeEnum, ptype, spatialScaling );

        qDebug() << "sampling population";

        if( timeStepChanged && m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            m_sampler.updateSelection();
        }

        qDebug() << "update sampler complete";
    }

    m_renderer->clearViewPortInverse( scaledHeight(), scaledWidth(), m_samplerWidget.visualizationWindow.viewPort(), m_dividerColor, cartesianProg );

    qDebug() << "cleared viewport inverse";

    // The control panel on the sampling viewport
//    glViewport(
//        m_samplerWidget.visualizationWindow.viewPort().offsetX(),
//        m_samplerWidget.visualizationWindow.viewPort().offsetY() + m_samplerWidget.visualizationWindow.viewPort().height(),
//        m_samplerWidget.visualizationWindow.viewPort().width(),
//        m_samplerWidget.controlPanel.size().y() );
//    m_renderer->clear( m_dividerColor, cartesianProg );

//    qDebug() << "";
//    qDebug() << m_samplerWidget.controlPanel.position().x();
//    qDebug() << m_samplerWidget.controlPanel.position().y();
//    qDebug() << m_samplerWidget.controlPanel.size().x();
//    qDebug() << m_samplerWidget.controlPanel.size().y();
//    qDebug() << "";

//    glViewport(
//        m_samplerWidget.controlPanel.position().x(),
//        m_samplerWidget.controlPanel.position().y(),
//        m_samplerWidget.controlPanel.size().x(),
//        m_samplerWidget.controlPanel.size().y() );
//    m_renderer->clear( m_dividerColor, cartesianProg );

    m_renderer->clearViewPort( m_timeHistIsoViewport, cartesianProg );
    m_renderer->clearViewPort( m_histogramViewPort, cartesianProg );
    m_renderer->clearViewPort( m_timeLineWidget.viewPort(), cartesianProg );
    m_renderer->clearViewPort( m_auxViewPort, cartesianProg );
    m_renderer->clearViewPort( m_miniMapViewPort, cartesianProg );

    // spatial view control panel
    glViewport(
        m_auxViewPort.offsetX(),
        m_auxViewPort.offsetY() + m_auxViewPort.height() - m_samplerWidget.controlPanel.size().y() + 24,
        m_auxViewPort.width(),
        m_samplerWidget.controlPanel.size().y() - 24 );
    m_renderer->clear( m_dividerColor, cartesianProg );

    glViewport(
        m_histogramViewPort.offsetX(),
        m_histogramViewPort.offsetY() + m_histogramViewPort.height() - 70,
        m_histogramViewPort.width(),
        70 );
    m_renderer->clear( m_dividerColor, cartesianProg );

    // Menu Bar
    glViewport( 0, scaledHeight() - MENU_HEIGHT, scaledWidth(), MENU_HEIGHT );
    m_renderer->clear( m_dividerColor, cartesianProg );

    // Timeline Control Panel

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        glViewport(
            m_dataInfoPanelViewPort.offsetX(),
            m_dataInfoPanelViewPort.offsetY() + 10,
            m_dataInfoPanelViewPort.width(),
            m_dataInfoPanelViewPort.height() - 10 );
        m_renderer->clear( QVector4D( .99, .99, .99, 1 ), cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );
    }
    const QVector4D allColorTP( .2, .2, .2, 0.8 );
    const QVector4D regionsColorTP(  0.6, 0.3, 0.5, 0.8 );

    qDebug() << "UI rendered background";

    if( histDefinition != nullptr && timeSeries != nullptr && m_sessionInitiated && spacePartitioning != nullptr )
    {
        qDebug() << "Data ready, going into workflow";

        static int ScellSelection = m_sampler.getSelectedCellId();
        static int SbinSelection  = m_sampler.getSelectedBinId();

        int cellSelection = m_sampler.getSelectedCellId();
        int binSelection  = m_sampler.getSelectedBinId();

        bool cellChanged = ScellSelection != cellSelection;
        bool binChanged = SbinSelection != binSelection;

        int weightType = HistogramWeightType::NONE;
        std::string weightKey = "1";
        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            weightType = dynamic_cast< HistogramDefinition * >( histDefinition )->weightType;
            weightKey = dynamic_cast< HistogramDefinition * >( histDefinition )->weight;
        }

        Vec2< float > plotPosition;
        Vec2< float > plotSize;

        qDebug() << "Getting ROI";

        const auto & roiDef = m_currentConfigurations.distributionBasedConfiguration.m_roiDefinitions.at( ptype ).at(
            m_samplerWidget.controlPanel.histSelectionPanel.roiCombo.selectedItemText() );

        qDebug() << "got ROI";

        if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
        {
            qDebug() << "rendering grid selection context";
            renderGridSelectionContext( { { "r", 1.0 }, { "z", 1.0 } }, roiDef, userIsPanning );
            qDebug() << "done rendering grid selection context";
        }

        /// Check if phase trajectories need to be updated, this can be refactored later
        else if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            static bool linkedViewDefinedPreviously = linkedViewDef != nullptr;
            bool linkedViewDefined = linkedViewDef != nullptr;

            if( ! linkedViewDefined )
            {
                linkedViewDefinedPreviously = false;
            }
            else
            {
                bool useWeightBreakdown = weightType == HistogramWeightType::VARIABLE;

                getPhasePlotSizeAndLocation( *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ), ptype, useWeightBreakdown, plotSize, plotPosition );

                static std::string SpX = dynamic_cast< PhasePlotDefinition * >( linkedViewDef )->attrs[ 0 ];
                static std::string SpY = dynamic_cast< PhasePlotDefinition * >( linkedViewDef )->attrs[ 1 ];

                std::string pX = dynamic_cast< PhasePlotDefinition * >( linkedViewDef )->attrs[ 0 ];
                std::string pY = dynamic_cast< PhasePlotDefinition * >( linkedViewDef )->attrs[ 1 ];

                auto t_start = std::chrono::high_resolution_clock::now();

                Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( pX )->second;
                Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( pY )->second;

                renderPhasePlotGrid( plotSize, plotPosition, pX, pY, rX, rY );

                //renderLinkedView( m_populationDataTypeEnum, linkedViewModeEnum, ptype, weightType, weightKey );

                auto t_end = std::chrono::high_resolution_clock::now();
                double timeTook = std::chrono::duration<double, std::milli>(t_end-t_start).count();
                ////////////////////////qDebug() << "phase plot took " << timeTook << " ms, recalculated=" << m_recalculatePhasePlot;


                bool updateAllWeightHistWidget = false;

                static bool firstTime = true;

                /// Check if phase trajectories need to be updated, this can be refactored later
                if( SpX != pX || SpY != pY  || ! linkedViewDefinedPreviously || firstTime || m_updatedGPUData )
                {
                    //qDebug() << "preparing trajectories " << bool( SpX != pX ) << " " << bool( SpY != pY ) << " " << bool( ! linkedViewDefinedPreviously ) << " " << firstTime << " " << m_reloadGPUData;
                    preparePhaseTrajectories( ptype, *timeSeries, *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ) );

                    SpX = pX;
                    SpY = pY;

                    updateAllWeightHistWidget = true;
                    m_recalculatePhasePlot = true;
                }

                bool updateRange = false;

                if( timeStepChanged )
                {
                    updateAllWeightHistWidget = true;
                }
                if( linkedViewDefinedPreviously == false || m_updatedGPUData || firstTime )
                {
                    updateRange = true;
                }

                static std::vector< int > binWeightIds;
                static std::vector< int > histWeightIds;

                renderParticleWeightBreakdown( updateRange, updateAllWeightHistWidget, plotSize, plotPosition, ptype, weightKey, binWeightIds, histWeightIds );

                linkedViewDefinedPreviously = true;

                /// Check if need to recalcuate texture ( if it hasn't been computed or if the viewspace has changed dimensions )
                bool recalculateNow = ! ( m_resizeWidgetA.isPressed() || m_resizeWidgetB.isPressed() || m_resizeWidgetC.isPressed() || m_resizeWidgetD.isPressed() )
                                      && m_recalculatePhasePlot;

                bool settingsChanged = false;
                static std::string interpolationOption = m_phasePlotInterpolationCombo.selectedItemText();
                static std::string blendingMode = m_phasePlotBlendingCombo.selectedItemText();
                static std::string plotMode = m_phasePlotModeCombo.selectedItemText();
                if( linkedViewModeEnum == PlotViewMode::TRAJECTORIES )
                {
                    if( interpolationOption != m_phasePlotInterpolationCombo.selectedItemText()
                            ||        blendingMode != m_phasePlotBlendingCombo.selectedItemText()
                            ||        plotMode != m_phasePlotModeCombo.selectedItemText() )
                    {
                        settingsChanged = true;
                    }
                }
                interpolationOption = m_phasePlotInterpolationCombo.selectedItemText();
                blendingMode = m_phasePlotBlendingCombo.selectedItemText();
                plotMode = m_phasePlotModeCombo.selectedItemText();

                if( settingsChanged || firstTime )
                {
                    const std::string & dummyPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanAll.png";

                    m_phaseEquationAll.setTexFromPNG( dummyPath, dummyPath );
                    m_phaseEquationRegion.setTexFromPNG( dummyPath, dummyPath );
                    m_phaseEquationBin.setTexFromPNG( dummyPath, dummyPath );

                    m_phaseEquationAll.resizeByHeight( EQUATION_HEIGHT );
                    m_phaseEquationRegion.resizeByHeight( EQUATION_HEIGHT );
                    m_phaseEquationBin.resizeByHeight( EQUATION_HEIGHT );

                    if( m_trackState )
                    {
                        pushState();
                    }
                }

                firstTime = false;

                static QMatrix4x4 M;

                ////////////////////qDebug() << "before rendering all trajectories";

                if( recalculateNow || m_updatedGPUData || settingsChanged )
                {
                    m_phasePlotCache.setParameters( plotSize.x(), plotSize.y() );

                    // need to set the texture sizes potentially
                    M = computePhaseTexture(
                            m_phasePlotCache,
                            m_phasePlotCache.fullPlotTexture,
                            linkedViewModeEnum,
                            ptype,
                            *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ),
                            m_phaseBufferCounts,
                            m_phaseBufferFirsts,
                            interpolationOption,
                            blendingMode,
                            "all",
                            -1 );
                }

                bool renderCellTrajectories = cellSelection >= 0 && ! userIsPanning && m_phasePlotSelectionSelector.isPressed();
                bool binCellIsInHoveredCell = ( ( m_sampler.clickSelectedCellId() == m_sampler.getSelectedCellId() )
                                                || ( m_sampler.getFocusBinId() == m_sampler.getSelectedBinId() ) );

                bool renderBinTrajectories =
                    m_sampler.hasPointSelection()
                    &&( ! userIsPanning )
                    && binCellIsInHoveredCell
                    && m_phasePlotSelectionSelector.isPressed();

                bool hasSelectedBin = m_sampler.getSelectedBinId() != -1;

                if( renderCellTrajectories  && ! renderBinTrajectories || ! renderCellTrajectories  )
                {
                    renderPhaseTexture( m_phasePlotCache.fullPlotTexture, m_phasePlotTFAll, renderCellTrajectories || m_phasePlotSelectionSelector.isPressed(), plotPosition, plotSize, true, renderBinTrajectories );
                }

                ////////////////////qDebug() << "before render cell trajectories";

                if( renderCellTrajectories )
                {
                    bool alreadyCached;
                    TextureLayer & regionLayer = m_phasePlotCache.get( cellSelection, alreadyCached );

                    if( ! alreadyCached || recalculateNow || timeStepChanged || m_updatedGPUData || settingsChanged )
                    {
                        prepareRegionPhaseTrajectories( *timeSeries, cellSelection, ptype );

                        M = computePhaseTexture(
                                m_phasePlotCache,
                                regionLayer,
                                linkedViewModeEnum,
                                ptype,
                                *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ),
                                m_phaseBufferCountsRegion,
                                m_phaseBufferFirsts,
                                interpolationOption,
                                blendingMode,
                                "hist",
                                cellSelection );
                    }

                    if( ! renderBinTrajectories && ! hasSelectedBin )
                    {
                        prepareRegionPhaseTrajectories( *timeSeries, cellSelection, ptype );
                        renderPhasePrimitives(
                            linkedViewModeEnum,
                            interpolationOption,
                            ptype,
                            *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ),
                            m_phaseBufferCountsRegion,
                            m_phaseBufferFirsts,
                            plotSize.x(),
                            plotSize.y(),
                            plotPosition.x(),
                            plotPosition.y(),
                            QVector4D( 0, 0, 0, 1.0 ) );
                    }

                    renderPhaseTexture( regionLayer, m_phasePlotTFRegion, renderBinTrajectories || hasSelectedBin, plotPosition, plotSize, renderBinTrajectories || hasSelectedBin, false );
                }

                qDebug() << "before rendering bin trajectories";

                if(  renderBinTrajectories )
                {
                    // too many bins for cacheing to be worthwhile, but there is a chance that the bin has not changed
                    if( timeStepChanged || cellChanged || binChanged || m_recalculatePhasePlot || m_updatedGPUData )
                    {
                        //qDebug() << "preparing bin trajectories";
                        prepareBinPhaseTrajectories( *timeSeries, ptype );
                    }

                    M = computePhaseTexture(
                            m_phasePlotCache,
                            m_phasePlotCache.binPlotTexture,
                            linkedViewModeEnum,
                            ptype,
                            *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ),
                            m_phaseBufferCountsBin,
                            m_phaseBufferFirsts,
                            interpolationOption,
                            blendingMode,
                            "bin",
                            cellSelection );

                    renderPhasePrimitives(
                        linkedViewModeEnum,
                        interpolationOption,
                        ptype,
                        *( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) ),
                        m_phaseBufferCountsBin,
                        m_phaseBufferFirsts,
                        plotSize.x(),
                        plotSize.y(),
                        plotPosition.x(),
                        plotPosition.y(),
                        QVector4D( 0, 0, 0, 1.0 ) );

                    renderPhaseTexture( m_phasePlotCache.binPlotTexture, m_phasePlotTFRegionBin, false, plotPosition, plotSize, false, false );
                }

                renderWeightBasedParticleSelection( binWeightIds, histWeightIds, weightType, linkedViewModeEnum, *timeSeries, pX, pY, ptype );

                if( recalculateNow )
                {
                    m_recalculatePhasePlot = false;
                }

                renderVisualContextModels( m_populationDataTypeEnum, ( dynamic_cast< PhasePlotDefinition * >( linkedViewDef ) )->attrs, { 1.0, 1.0 }, M, 3.0, plotPosition, plotSize, false );

                const float legendWidth = 200;
                const float equationWidth = 140;
                Vec2< float > lPS( plotPosition.x() + plotSize.x() - legendWidth, plotPosition.y() + plotSize.y() + 24 );
                Vec2< float > lSZ( legendWidth, 26 );

                m_phaseEquationAll.setPosition( lPS.x() - lSZ.x() - 10, lPS.y() - ( m_phaseEquationAll.size().y() - lSZ.y()  ) / 2.0 );
                //m_renderer->renderTexturedPressButton( m_phaseEquationAll, ucProg, cartesianProg, simpleColorProg );

                m_renderer->renderColorLegend( lPS, lSZ, m_phasePlotTFAll, colorLegendProg );
                m_renderer->renderPopSquare( simpleColorProg );

//                void Renderer::renderColorLegend(
//                    const Vec2< float > & pos,
//                    const Vec2< float > & size,
//                    Texture1D3 & tf,
//                    const std::unique_ptr<QOpenGLShaderProgram> & program )
            }
            m_updatedGPUData = false;
        }

//        std::string phasePlotName = m_phasePlotCombo.selectedItemText();

//        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
//        {
//            auto phaseDefMapIter = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype );
//            if( phaseDefMapIter == m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.end() )
//            {
//                return;
//            }

//            auto phaseDefIter = phaseDefMapIter->second.find( phasePlotName );
//            if ( phaseDefIter == phaseDefMapIter->second.end() )
//            {
//                return;
//            }
//        }

        ///////////////////////////////////////////////////////

        int currentNumSteps = timeSeries->numSteps();
        qDebug() << "before frVolume stuff";

        {
            if( cellSelection >= 0 && ( ! userIsPanning ) )
            {
                //////////////////////////////////////////////////////
                // update current volume settings if necessary

                if( m_frVolumeCache.rows != histResolution.a()
                        || m_frVolumeCache.cols != histResolution.b()
                        || m_frVolumeCache.steps != currentNumSteps )
                {

                    auto t_start = std::chrono::high_resolution_clock::now();

                    // will resize each volume to correct size and invalidate cache items
                    m_frVolumeCache.setParameters( histResolution.a(), histResolution.b(), currentNumSteps );

                    auto t_end = std::chrono::high_resolution_clock::now();
                    double timeTook = std::chrono::duration<double, std::milli>(t_end-t_start).count();
                    ////////////////////////qDebug() << "rebuild cache took " << timeTook << " ms";
                }

                if( m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Local && m_frVolumeCache.normalize == false )
                {
                    m_frVolumeCache.setNormalization( true );
                }
                else if(  m_samplerWidget.colorScalingMode() == SamplerParameters::ColorScalingMode::Global && m_frVolumeCache.normalize == true  )
                {
                    m_frVolumeCache.setNormalization( false );
                }

                bool frVolValid;
                FrVolume & frVolume = m_frVolumeCache.get( cellSelection, frVolValid );

                if( ! frVolValid )
                {
                    auto t_start = std::chrono::high_resolution_clock::now();

                    qDebug() << "stacking temporally";

                    if( ( m_populationDataTypeEnum == PopulationDataType::PARTICLES ) && ( VIDI_CUDA_SUPPORT == 1 ) )
                    {
                        stackHistograms2D_GPU( frVolume );
                    }
                    else
                    {
                        constructTimeHistogramStack2D( frVolume, m_populationDataTypeEnum, histDefinition, histResolution, spatialScaling, ptype, dtype, cellSelection );
                    }
                    auto t_end = std::chrono::high_resolution_clock::now();
                    double timeTook = std::chrono::duration<double, std::milli>(t_end-t_start).count();
                    ////////////////////////qDebug() << "recalcuate volume took " << timeTook << " ms, function=" << ( ( m_populationDataTypeEnum == PopulationDataType::PARTICLES ) && ( VIDI_CUDA_SUPPORT == 1 ) ? "cuda" : "cpu" );
                }

                renderSamplingLayoutAnnotation( spacePartitioning->attributes[ 0 ], spacePartitioning->attributes[ 1 ] );
                qDebug() << "rendered sampling layout annotation";

                renderSamplingResult( histResolution );
                qDebug() << "rendered sampling result";

                renderSamplingLayout( userIsPanning );
                qDebug() << "rendered sampling layout";

                renderDetailView( frVolume, histDefinition, cellSelection, binSelection );
                qDebug() << "rendered iso stack";

                renderMiniMap( cellSelection, binSelection, userIsPanning );
                qDebug() << "rendered minimap";

                auto t_start = std::chrono::high_resolution_clock::now();

                if( m_populationDataTypeEnum == PopulationDataType::PARTICLES
                        || ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS && m_distSeriesModeCombo.selectedItemText() == "2D-Stacked-Iso"  ) )
                {
                    renderHistogramStackIso(
                        histDefinition,
                        frVolume
                    );
                }
                else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS && m_distSeriesModeCombo.selectedItemText() == "2D-Multiples" )
                {
                    renderHistogramMultiples(
                        frVolume,
                        histResolution
                    );
                }

                auto t_end = std::chrono::high_resolution_clock::now();
                double timeTook2 = std::chrono::duration<double, std::milli>(t_end-t_start).count();
                qDebug() << "render isosurface took " << timeTook2;
            }
            else
            {
                renderSamplingResult( histResolution );
                qDebug() << "rendered sampling result";

                renderSamplingLayoutAnnotation( spacePartitioning->attributes[ 0 ], spacePartitioning->attributes[ 1 ] );
                qDebug() << "rendered sampling layout annotation";

                renderSamplingLayout( userIsPanning );
                qDebug() << "rendered sampling layout";

                renderMiniMap( cellSelection, binSelection, userIsPanning );
                qDebug() << "rendered minimap";
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////

            qDebug() << "before timeline";

            // Timeline

//            m_renderer->renderTimeLinePlot( m_timeLineWidget, cartesianProg, flatProgSerial, m_boxSelectionColor  );
//            renderTimeLineAnnotation();

            if ( ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS && m_distSeriesModeCombo.selectedItemText() == "1D-Aggregation" )
                    || m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                m_renderer->renderTimeLinePlot( m_timeLineWidget, cartesianProg, flatProgSerial, m_boxSelectionColor  );
                renderTimeLineAnnotation();
            }

            if ( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS && m_distSeriesModeCombo.selectedItemText() == "1D-Aggregation" )
            {
                renderGridTimeSeries(
                    m_timePlotAllSelector.isPressed()       || m_timePlotBothSelector.isPressed(),
                    m_timePlotHistogramSelector.isPressed() || m_timePlotBothSelector.isPressed() );
            }

            qDebug() << "before time plot stuff";

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES && timePlotDef != nullptr )
            {
                static int S_plotKey = -1;

                const int TEXT_PREC = 4;
                const int MIN_MAX_TEX_OFF_A = 30;
                const int MIN_MAX_TEX_OFF_B = 20;
                const int PLOT_MM_TEX_X = 48;

                int plotKey = m_timePlotModeCombo.selectedItemText() == "mean" ? LineData::MEAN : LineData::MEAN_SQUARED;

                // update textures, in the future will dynamically generate symbols and equations from user specified functions
                if( S_plotKey != plotKey )
                {
                    std::string allTexPath = "";
                    std::string regionTexPath = "";

                    if( plotKey == LineData::MEAN )
                    {
                        allTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanAll.png";
                        regionTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanRegion.png";
                    }
                    else if ( plotKey == LineData::MEAN_SQUARED )
                    {
                        allTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanSquaredAll.png";
                        regionTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanSquaredRegion.png";
                    }

                    m_timeAggregateAllEquation.setTexFromPNG( allTexPath, allTexPath );
                    m_timeAggregateRegionEquation.setTexFromPNG( regionTexPath, regionTexPath );
                    m_timeAggregateAllEquation.resizeByHeight( EQUATION_HEIGHT );
                    m_timeAggregateRegionEquation.resizeByHeight( EQUATION_HEIGHT );

                    S_plotKey = plotKey;

                    if( m_trackState )
                    {
                        pushState();
                    }
                }

                m_timeAggregateEquationDefinitionsAll.setPosition(
                    m_timeLineWidget.position().x() + m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_RIGHT - m_timeAggregateEquationDefinitionsAll.size().x(),
                    m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - EQUATION_HEIGHT - 15 );

                m_timeAggregateEquationDefinitions.setPosition(
                    m_timeLineWidget.position().x() + m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_RIGHT - m_timeAggregateEquationDefinitions.size().x(),
                    m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - EQUATION_HEIGHT - 15 );

                if( /*m_timePlotAllSelector.isPressed() || m_timePlotBothSelector.isPressed()*/ true )
                {
                    m_timeAggregateAllEquation.setPosition( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT + 40, m_timeAggregateEquationDefinitions.position().y() );
                    m_timeAggregateRegionEquation.setPosition( m_timeAggregateAllEquation.position().x() + m_timeAggregateAllEquation.size().x() + 80, m_timeAggregateEquationDefinitions.position().y() );
                    m_renderer->renderTexturedPressButton( m_timeAggregateAllEquation, ucProg, cartesianProg, simpleColorProg );

                    glViewport( m_timeAggregateAllEquation.position().x() - 40, m_timeAggregateAllEquation.position().y() + 10, EQUATION_HEIGHT-20, EQUATION_HEIGHT-20 );
                    m_renderer->clear( allColorTP, cartesianProg );
                }
                else if( m_timePlotHistogramSelector.isPressed() )
                {
                    m_timeAggregateRegionEquation.setPosition( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT + 40, m_timeAggregateEquationDefinitions.position().y() );
                }

                if( /*m_timePlotHistogramSelector.isPressed() || m_timePlotBothSelector.isPressed()*/ true )
                {
                    m_renderer->renderTexturedPressButton( m_timeAggregateRegionEquation, ucProg, cartesianProg, simpleColorProg );
                    glViewport( m_timeAggregateRegionEquation.position().x() - 40, m_timeAggregateRegionEquation.position().y() + 10, EQUATION_HEIGHT-20, EQUATION_HEIGHT-20 );
                    m_renderer->clear( regionsColorTP, cartesianProg );
                }

                if( m_timeAggregateRegionEquation.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )
                        /*&& ( m_timePlotHistogramSelector.isPressed() || m_timePlotBothSelector.isPressed() )*/ )
                {
                    m_renderer->renderTexturedPressButton( m_timeAggregateEquationDefinitions, ucProg, cartesianProg, simpleColorProg );
                }
                if( m_timeAggregateAllEquation.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )
                        /*&& ( m_timePlotAllSelector.isPressed() || m_timePlotBothSelector.isPressed() )*/ )
                {
                    m_renderer->renderTexturedPressButton( m_timeAggregateEquationDefinitionsAll, ucProg, cartesianProg, simpleColorProg );
                }

                if( ! ( m_timePlotCache.timeSeries == *timeSeries ) )
                {
                    m_recalculateFullTimePlot = true;
                    m_timePlotCache.setParameters( *timeSeries );
                }

                if( m_recalculateFullTimePlot )
                {
                    m_timePlotCache.resetFullAggregateRanges();
                    computeTemporalPlotsAll( m_timePlotCache.fullAggregate, ptype, *timePlotDef );
                }

                float pltOffY = m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM;
                float plHT = m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP;

                if( m_timePlotAllSelector.isPressed() )
                {
                    const std::vector< double > & x = m_timePlotCache.timePoints;
                    const std::vector< double > & y = m_timePlotCache.fullAggregate.values[ plotKey ];
                    const Vec2< double > & yRange = m_timePlotCache.fullAggregate.ranges[ plotKey ];
                    renderTemporalPlot( x, y, yRange, *timeSeries, m_selectedTimeStep, allColorTP );

                    m_renderer->renderText( false,
                                            textProg,
                                            scaledWidth(),
                                            scaledHeight(),
                                            to_string_with_precision( yRange.a(), TEXT_PREC ),
                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                            m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
                                            1.0,
                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                            true );

                    m_renderer->renderText( false,
                                            textProg,
                                            scaledWidth(),
                                            scaledHeight(),
                                            to_string_with_precision( yRange.b(), TEXT_PREC ),
                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                            m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
                                            1.0,
                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                            true );
                }
                else if( m_timePlotHistogramSelector.isPressed() )
                {
                    if( cellSelection >= 0 && ( ! userIsPanning ) )
                    {
                        bool tscValid;
                        LineData & lineData = m_timePlotCache.get( cellSelection, tscValid );
                        if( ! tscValid )
                        {
                            computeTemporalPlots( lineData, ptype, *timePlotDef, *spacePartitioning );
                        }

                        const std::vector< double > & x = m_timePlotCache.timePoints;
                        const std::vector< double > & y = lineData.values[ plotKey ];
                        const Vec2< double > & yRange = lineData.ranges[ plotKey ];
                        renderTemporalPlot( x, y, yRange, *timeSeries, m_selectedTimeStep, regionsColorTP );

                        m_renderer->renderText( false,
                                                textProg,
                                                scaledWidth(),
                                                scaledHeight(),
                                                to_string_with_precision( yRange.a(), TEXT_PREC ),
                                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                                m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
                                                1.0,
                                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                                true );

                        m_renderer->renderText( false,
                                                textProg,
                                                scaledWidth(),
                                                scaledHeight(),
                                                to_string_with_precision( yRange.b(), TEXT_PREC ),
                                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                                m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
                                                1.0,
                                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                                true );

                    }
                }
                else if( m_timePlotBothSelector.isPressed() )
                {
                    const std::vector< double > & xAll = m_timePlotCache.timePoints;
                    const std::vector< double > & yAll = m_timePlotCache.fullAggregate.values[ plotKey ];
                    const Vec2< double > & yRangeAll = m_timePlotCache.fullAggregate.ranges[ plotKey ];

                    Vec2< double > fullRange = yRangeAll;

                    if( cellSelection >= 0 && ( ! userIsPanning ) )
                    {
                        bool tscValid;
                        LineData & lineData = m_timePlotCache.get( cellSelection, tscValid );
                        if( ! tscValid )
                        {
                            computeTemporalPlots( lineData, ptype, *timePlotDef, *spacePartitioning );
                        }

                        const std::vector< double > & y = lineData.values[ plotKey ];
                        const Vec2< double > & yRange = lineData.ranges[ plotKey ];

                        fullRange.a( std::min( fullRange.a(), yRange.a() ) );
                        fullRange.b( std::max( fullRange.b(), yRange.b() ) );

                        renderTemporalPlot( xAll, yAll, fullRange, *timeSeries, m_selectedTimeStep, allColorTP );
                        renderTemporalPlot( xAll, y, fullRange, *timeSeries, m_selectedTimeStep, regionsColorTP, true );
                    }
                    else
                    {
                        renderTemporalPlot( xAll, yAll, fullRange, *timeSeries, m_selectedTimeStep, allColorTP );
                    }

                    m_renderer->renderText( false,
                                            textProg,
                                            scaledWidth(),
                                            scaledHeight(),
                                            to_string_with_precision( fullRange.a(), TEXT_PREC ),
                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                            m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
                                            1.0,
                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                            true );

                    m_renderer->renderText( false,
                                            textProg,
                                            scaledWidth(),
                                            scaledHeight(),
                                            to_string_with_precision( fullRange.b(), TEXT_PREC ),
                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                            m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
                                            1.0,
                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                            true );
                }
            }
        }
        qDebug() << "after all of that stuff";
    }

    /////////////////////////////////////////////////////////////////////////////////

    // Box Shadows

    m_renderer->renderViewPortBoxShadow( m_samplerWidget.viewPort(), simpleColorProg );
    m_renderer->renderViewPortBoxShadow( m_miniMapViewPort,  simpleColorProg );
    m_renderer->renderViewPortBoxShadow( m_auxViewPort,      simpleColorProg );
    m_renderer->renderViewPortBoxShadow( m_histogramViewPort, simpleColorProg );
    m_renderer->renderViewPortBoxShadow( m_timeHistIsoViewport, simpleColorProg );

    ViewPort vprt;
    vprt.setSize(scaledWidth(), m_timeLineWidget.size().y() );
    vprt.offsetX( 0 );
    vprt.offsetY( m_timeLineWidget.position().y() );
    m_renderer->renderViewPortBoxShadow( vprt, simpleColorProg );

    //m_renderer->renderViewPortBoxShadow( m_timeLineWidget.viewPort(), simpleColorProg );

//    const int INSET = 2;
//    glViewport(
//        m_samplerWidget.visualizationWindow.viewPort().offsetX() + INSET,
//        m_samplerWidget.visualizationWindow.viewPort().offsetY() + m_samplerWidget.visualizationWindow.viewPort().height(),
//        m_samplerWidget.visualizationWindow.viewPort().width() - INSET * 2,
//        m_samplerWidget.controlPanel.size().y() - INSET );
//    m_renderer->renderPopSquare( simpleColorProg );

//    glViewport(
//        m_auxViewPort.offsetX() + INSET,
//        m_auxViewPort.offsetY() + m_auxViewPort.height() - MENU_HEIGHT,
//        m_auxViewPort.width() - INSET * 2,
//        MENU_HEIGHT - INSET );
//    m_renderer->renderPopSquare( simpleColorProg );

//    glViewport(
//        m_timeLineDetailViewPort.offsetX() + INSET,
//        m_timeLineDetailViewPort.offsetY() + m_timeLineDetailViewPort.height() - OFF_TLD,
//        m_timeLineDetailViewPort.width() - INSET * 2,
//        OFF_TLD - INSET );
//    m_renderer->renderPopSquare( simpleColorProg );

    //////////

    m_renderer->renderResizeWidget( m_resizeWidgetA, cartesianProg );
    m_renderer->renderResizeWidget( m_resizeWidgetB, cartesianProg );
    m_renderer->renderResizeWidget( m_resizeWidgetC, cartesianProg );
    m_renderer->renderResizeWidget( m_resizeWidgetD, cartesianProg );

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const int LABEL_OFF = 110;

    /////////////////////////////////////////////////////////////////////////

    // Sampler

    if( m_samplerWidget.controlPanel.isActive() )
    {
        m_renderer->clearViewPort( m_samplerWidget.controlPanel.viewPort(), cartesianProg );

        // sort the panels by position to prevent combos from occluding eachother
        std::vector< TN::PanelWidget* > ControlPanelPointers = m_samplerWidget.controlPanel.visiblePanels();\
        std::sort(
            ControlPanelPointers.begin(),
            ControlPanelPointers.end(),
            [ ]( TN::PanelWidget * c1, TN::PanelWidget * c2 )
            {
               return c1->position().y() < c2->position().y();
            } );

        for( auto & panel : ControlPanelPointers )
        {
            m_renderer->renderControlPanel(
                *panel,
                cartesianProg,
                simpleColorProg,
                textProg,
                m_textColor,
                scaledWidth(),
                scaledHeight(),
                true );
        }
    }

    qDebug() << "rendered control panel";

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    m_renderer->renderSlider( m_isoValueSlider, cartesianProg, simpleColorProg );

    // Buttons
    m_renderer->renderPressButton(
        m_defineTimeSeriesButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    m_renderer->renderPressButton(
        m_saveButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    m_renderer->renderPressButton(
        m_definePlotButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_renderer->renderPressButton(
            m_editPhasePlotButton,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );
        m_renderer->renderPressButton(
            m_definePhasePlotButton,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );
    }

    m_renderer->renderPressButton(
        m_editTimePlotButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_renderer->renderPressButton(
            m_phasePlotAllSelector,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );

        m_renderer->renderPressButton(
            m_phasePlotSelectionSelector,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );
    }

    m_renderer->renderPressButton(
        m_editTimeSeriesButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined && m_histogramDefined && m_forceSpacePartitioningSelection == false )
    {
        m_renderer->renderPressButton(
            m_initiateSessionButton,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );
    }

    m_renderer->renderPressButton(
        m_saveProjectButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    m_renderer->renderPressButton(
        m_loadProjectFileButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_renderer->renderPressButton( m_defineFilterButton, cartesianProg, simpleColorProg );
        m_renderer->renderPressButton( m_editFilterButton, cartesianProg, simpleColorProg );
    }

    if( /*m_populationDataTypeEnum != PopulationDataType::GRID_POINTS*/ true )
    {
        m_renderer->renderPressButton( m_timePlotBothSelector, cartesianProg, simpleColorProg );
        m_renderer->renderPressButton( m_timePlotHistogramSelector, cartesianProg, simpleColorProg );
        m_renderer->renderPressButton( m_timePlotAllSelector, cartesianProg, simpleColorProg );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Both", m_timePlotBothSelector.position().x() +  m_timePlotBothSelector.size().x() / 2.0 - 18, m_timePlotBothSelector.position().y() + TEXT_PAD, 1.0, m_textColor );

        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Sel.", m_timePlotHistogramSelector.position().x() + TEXT_PAD-4, m_timePlotHistogramSelector.position().y() + TEXT_PAD, 1.0, m_textColor );

        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "All", m_timePlotAllSelector.position().x() + TEXT_PAD-4, m_timePlotAllSelector.position().y() + TEXT_PAD, 1.0, m_textColor );

        const int cIPAD = 6;
        const int cISZ = m_timePlotAllSelector.size().y() - cIPAD*2;

        glViewport(
            m_timePlotAllSelector.position().x() + m_timePlotAllSelector.size().x() - cIPAD - cISZ,
            m_timePlotHistogramSelector.position().y() + cIPAD,
            cISZ,
            cISZ );
        m_renderer->clear( allColorTP, cartesianProg );

        glViewport(
            m_timePlotHistogramSelector.position().x() + m_timePlotHistogramSelector.size().x() - cIPAD - cISZ,
            m_timePlotHistogramSelector.position().y() + cIPAD,
            cISZ,
            cISZ );
        m_renderer->clear( regionsColorTP, cartesianProg );
    }

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderPressButton(
        m_selectedHist2DButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    m_renderer->renderPressButton(
        m_selectedHist3DButton,
        cartesianProg,
        simpleColorProg,
        textProg,
        m_textColor,
        scaledWidth(),
        scaledHeight() );

    ///////////////

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_renderer->renderSlider( m_smoothRadiusSlider, cartesianProg, simpleColorProg );
        m_renderer->renderSlider( m_gridColorScaleSlider, cartesianProg, simpleColorProg );

        m_renderer->renderPressButton(
            m_negPosScalarButton,
            cartesianProg,
            simpleColorProg,
            textProg,
            m_textColor,
            scaledWidth(),
            scaledHeight() );
    }

    //////////////// Text

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                " + ", m_definePlotButton.position().x() + TEXT_PAD-4, m_definePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );
    }
    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " + ", m_defineTimeSeriesButton.position().x() + TEXT_PAD-4, m_defineTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Var.", m_sampler.CONTROL_PANEL_SPACING, m_definePlotButton.position().y() + TEXT_PAD, 1.0f, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Mode", m_sampler.CONTROL_PANEL_SPACING, m_timePlotModeCombo.position().y() + TEXT_PAD, 1.0f, m_textColor );

    if( m_populationDataTypeEnum != PopulationDataType::GRID_POINTS )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Plot", m_auxViewPort.offsetX() + 10, m_editPhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Type", m_auxViewPort.offsetX() + 130, m_phasePlotModeCombo.position().y() + TEXT_PAD, 1.0, m_textColor );
        //

        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                " + ", m_definePhasePlotButton.position().x() + TEXT_PAD-4, m_definePhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

        if( m_phasePlotModeCombo.selectedItemText() == "Trajectories" && m_phasePlotInterpolationCombo.selectedItemText() != "None" )
        {
            m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                    "Heat Map ", m_phasePlotBlendingCombo.position().x() - 75, m_phasePlotBlendingCombo.position().y() + TEXT_PAD, 1.0, m_textColor );
        }

        if( m_phasePlotModeCombo.selectedItemText() == "Trajectories"  )
        {
            m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                    "Interp.", m_phasePlotInterpolationCombo.position().x() - 75, m_phasePlotInterpolationCombo.position().y() + TEXT_PAD, 1.0, m_textColor );
        }
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Contour Levels", m_sampler.CONTROL_PANEL_SPACING, m_isoValueSlider.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "smooth radius", m_smoothRadiusSlider.position().x() - LABEL_OFF + 16, m_smoothRadiusSlider.position().y() + TEXT_PAD, 1.0, m_textColor );

        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "color scale", m_gridColorScaleSlider.position().x() - LABEL_OFF + 30, m_gridColorScaleSlider.position().y() + TEXT_PAD, 1.0, m_textColor );
    }

    if( m_populationDataTypeEnum != PopulationDataType::GRID_POINTS )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Filter ", m_defineFilterButton.position().x() - 50, m_defineFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Edit", m_editFilterButton.position().x() + TEXT_PAD - 1, m_editFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                " + ", m_defineFilterButton.position().x() + TEXT_PAD-4, m_defineFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "2D", m_selectedHist2DButton.position().x() + 6, m_selectedHist2DButton.position().y() + TEXT_PAD, 1.0, Vec3< float >( .3, .3, .3 ) );
    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "3D", m_selectedHist3DButton.position().x() + 6, m_selectedHist3DButton.position().y() + TEXT_PAD, 1.0, Vec3< float >( .3, .3, .3 ) );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Edit", m_editTimePlotButton.position().x() + TEXT_PAD - 1, m_editTimePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Edit", m_editPhasePlotButton.position().x() + TEXT_PAD - 1, m_editPhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );


        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "    All Elements", m_phasePlotAllSelector.position().x() + TEXT_PAD - 1, m_phasePlotAllSelector.position().y() + TEXT_PAD, 1.0, m_textColor );
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                " Selected Element", m_phasePlotSelectionSelector.position().x() + TEXT_PAD - 1, m_phasePlotSelectionSelector.position().y() + TEXT_PAD, 1.0, m_textColor );
    }
    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                " {-,0,+}", m_negPosScalarButton.position().x() + TEXT_PAD - 3, m_negPosScalarButton.position().y() + TEXT_PAD, 1.0, m_textColor );
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Save", m_saveButton.position().x() + TEXT_PAD - 1, m_saveButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Edit", m_editTimeSeriesButton.position().x() + TEXT_PAD - 1, m_editTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                           "Save Project ", m_saveProjectButton.position().x() + TEXT_PAD - 1, m_saveProjectButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined && m_histogramDefined && m_forceSpacePartitioningSelection == false )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Begin", m_initiateSessionButton.position().x() + TEXT_PAD - 3, m_initiateSessionButton.position().y() + TEXT_PAD, 1.0, m_textColor );
    }

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Dataset ", m_datasetCombo.position().x() - 60, m_particleCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Preset", m_configurationCombo.position().x() - 52, m_configurationCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Data Object ", m_particleCombo.position().x() - 90, m_particleCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Time Series ", m_defineTimeSeriesButton.position().x() - 90, m_defineTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Warnings ", m_warningWidget.position().x() - 74, m_warningWidget.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( histDefinition != nullptr && timeSeries != nullptr && spacePartitioning != nullptr )
    {
//        std::string wMssg = getWarningSummary( m_populationDataTypeEnum, ptype );
//        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                                wMssg, m_warningWidget.position().x() + TEXT_PAD - 1, m_warningWidget.position().y() + TEXT_PAD, 1.0,
//                                ( wMssg[ 0 ] == 'D' ? Vec3< float >( .9, .2, 0 ) : Vec3< float >( .5, .5, .5 ) ) );
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Remove ", m_errorRemovalCombo.position().x() + TEXT_PAD - 1 - 68,
                            m_errorRemovalCombo.position().y() + TEXT_PAD, 1.0,
                            m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Advanced", m_inspectWarningsButton.position().x() + TEXT_PAD - 1,
//                            m_inspectWarningsButton.position().y() + TEXT_PAD, 1.0,
//                            m_textColor );

    // Combos
    for( auto & combo : m_comboRefs )
    {
        if( combo == &m_phasePlotCombo
                || combo == &m_phasePlotModeCombo
                || combo == & m_filterCombo
                /*         || combo == & m_timePlotCombo
                         || combo == & m_timePlotModeCombo*/ )
        {
            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                m_renderer->renderComboButton( *combo, cartesianProg, simpleColorProg );
                renderComboItems( *combo );
            }
        }
        else if ( combo == & m_phasePlotInterpolationCombo )
        {
            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_phasePlotModeCombo.selectedItemText() == "Trajectories" )
                {
                    m_renderer->renderComboButton( *combo, cartesianProg, simpleColorProg );
                    renderComboItems( *combo );
                }
            }
        }
        else if ( combo == & m_phasePlotBlendingCombo )
        {
            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_phasePlotModeCombo.selectedItemText() == "Trajectories" && m_phasePlotInterpolationCombo.selectedItemText() != "None" )
                {
                    m_renderer->renderComboButton( *combo, cartesianProg, simpleColorProg );
                    renderComboItems( *combo );
                }
            }
        }
        else
        {
            m_renderer->renderComboButton( *combo, cartesianProg, simpleColorProg );
            renderComboItems( *combo );
        }
    }

    if( int( std::floor( elapsed ) ) % 5 == 0 )
    {
        qDebug() << "requerying";
        qDebug() << m_configurationCombo.selectedItemText().c_str();
        reQueryPresets();
        qDebug() << "done query";
        qDebug() << m_configurationCombo.selectedItemText().c_str();
        renderLater();
    }
    ////////////////////////////qDebug() << "rendered frame";


    /////////////////////////////////////////////////////////////////

    // Widget Labels

    QVector4D labelColor( 0.94, 0.96, 1.0, 0.8 );
    Vec3< float > labelTextColor( 0, 0, 0 );
    float labelPad = TEXT_PAD - 3;
    float txtOff = 4;

    for( HelpLabelWidget * hlRef : m_helpLabelWidgetRefs )
    {
        glViewport(
            hlRef->position().x(),
            hlRef->position().y(),
            hlRef->size().x(),
            hlRef->size().y() );
        m_renderer->clear( labelColor, cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );
        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            true,
            textProg,
            scaledWidth(),
            scaledHeight(),
            hlRef->text(),
            hlRef->position().x() + txtOff,
            hlRef->position().y() + labelPad, 1.0f, labelTextColor );

        // question mark background

//        glViewport(
//            hlRef->position().x() + hlRef->size().x() - BUTTON_HEIGHT - 3,
//            hlRef->position().y(),
//            BUTTON_HEIGHT,
//            BUTTON_HEIGHT );

//        QVector4D hColor = hlRef->pointInViewPort(
//                               Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) ) ? QVector4D( 0, .7, .3, .7 )
//                           : QVector4D( 0, .5, .2, .2 );

//        Vec3< float > lColor = Vec3< float >( 1, 1, 1 );

//        if( hlRef->isPressed() )
//        {
//            glPointSize( BUTTON_HEIGHT - 6 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0, 0 ) } ),
//                cartesianProg,
//                QVector4D( .1, .1, .1, 0.7 ),
//                QMatrix4x4(),
//                GL_POINTS );

//            glPointSize( BUTTON_HEIGHT - 9 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0.08, -0.1 ) } ),
//                cartesianProg,
//                hColor,
//                QMatrix4x4(),
//                GL_POINTS );

//        }
//        else
//        {
//            glPointSize( BUTTON_HEIGHT - 6 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0, 0 ) } ),
//                cartesianProg,
//                hColor,
//                QMatrix4x4(),
//                GL_POINTS );
//        }

//        // question mark

//        glViewport( 0, 0, scaledWidth(), scaledHeight() );
//        m_renderer->renderText(
//            true,
//            textProg,
//            scaledWidth(),
//            scaledHeight(),
//            "?",
//            hlRef->position().x() + hlRef->size().x() - BUTTON_HEIGHT + 4.5,
//            hlRef->position().y() + labelPad, 1.0f,
//            lColor );
    }

    // warning widget

    Vec3< float > lColor = Vec3< float >( 0, 0, 0 );
//    m_renderer->clearViewPort( m_warningWidget.viewPort(), cartesianProg );
//    m_renderer->renderPopSquare( simpleColorProg );
    //m_renderer->renderPressButton(
        // m_inspectWarningsButton,
        // cartesianProg,
        // simpleColorProg,
        // textProg,
        // m_textColor,
        // scaledWidth(),
        // scaledHeight() );

    /************************************************************************************/
}

void BALEEN::keyPressEvent(QKeyEvent *e)
{
    if ( e->key() == Qt::Key_C )
    {
        compileShaders();
    }
//    if( e->key() == Qt::Key_S )
//    {
//        sparsifyTemporalDistributions();
//    }
    renderLater();
}

void BALEEN::resizeEvent(QResizeEvent *e)
{
    float w = scaledWidth();
    float h = scaledHeight();

    QVector3D c1;
    QVector3D c2;
    QVector3D c3;
    QVector3D c4;

    c1.setX( -1.0 );
    c1.setY(  1.0 );
    c1.setZ(  0.0 );

    c2.setX( 1.0 );
    c2.setY( 1.0 );
    c2.setZ( 0.0 );

    c3.setX(  1.0 );
    c3.setY( -1.0 );
    c3.setZ(  0.0 );

    c4.setX( -1.0 );
    c4.setY( -1.0 );
    c4.setZ(  0.0 );

    camera.setSize( w, h );

    c1 = ( camera.proj()* camera.view() ).inverted() * c1;
    c2 = ( camera.proj()* camera.view() ).inverted() * c2;
    c3 = ( camera.proj()* camera.view() ).inverted() * c3;
    c4 = ( camera.proj()* camera.view() ).inverted() * c4;

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    m_uiLayoutInitialized = false;
    recalculateUiLayout();

    renderLater();
}

void BALEEN::updateColorLegendGeom( const std::vector< TN::Vec3< float > > & tf )
{
    m_colorLegendColors.clear();
    m_colorLegendVerts.clear();

    const size_t TF_SIZE    = tf.size();
    const size_t TF_SIZE_M1 = TF_SIZE - 1;

    float df = 1.0 / 100.0;

    for( float f = -1.0; f <= 1.0 - df; f += df )
    {
        float r1 = ( f + 1.0 ) / 2.0;
        float r2 = ( f + 1.0 + df ) / 2.0;

        const TN::Vec3< float > & c1 = tf[ std::min( ( size_t ) std::floor( r1 * TF_SIZE ),  TF_SIZE_M1 ) ];
        const TN::Vec3< float > & c2 = tf[ std::min( ( size_t ) std::floor( r2 * TF_SIZE ),  TF_SIZE_M1 ) ];

        // 1
        // 2 3
        m_colorLegendColors.push_back( c1 );
        m_colorLegendColors.push_back( c1 );
        m_colorLegendColors.push_back( c2 );

        // 1 2
        //   3
        m_colorLegendColors.push_back( c1 );
        m_colorLegendColors.push_back( c2 );
        m_colorLegendColors.push_back( c2 );

        // 1
        // 2 3
        m_colorLegendVerts.push_back( Vec2< float >( ( f ),  1 ) );
        m_colorLegendVerts.push_back( Vec2< float >( ( f ), -1 ) );
        m_colorLegendVerts.push_back( Vec2< float >( ( f + df ), -1 ) );

        // 1 2
        //   3
        m_colorLegendVerts.push_back( Vec2< float >( f,  1 ) );
        m_colorLegendVerts.push_back( Vec2< float >( f + df,  1 ) );
        m_colorLegendVerts.push_back( Vec2< float >( f + df, -1 ) );
    }
}

void BALEEN::updateSelectedHistGridLines( const std::vector< std::int32_t > & resolution )
{
    if( resolution.size() < 2 )
    {
        return;
    }

    const int NC = m_populationDataTypeEnum == PopulationDataType::GRID_POINTS ? 33 : resolution[ 0 ];
    const int NR = m_populationDataTypeEnum == PopulationDataType::GRID_POINTS ? 33 : resolution[ 1 ];

    m_selectedHistGridLines.resize( ( NR + NC ) * 2 );

    float dc = 2.0 / NC;
    for( int c = 0; c < NC; ++c )
    {
        m_selectedHistGridLines[ c * 2 + 0 ] = Vec2< float >( -1 + (c+1)*dc, -1 );
        m_selectedHistGridLines[ c * 2 + 1 ] = Vec2< float >( -1 + (c+1)*dc,  1 );
    }
    float dr = 2.0 / NR;
    for( int r = NC; r < NR+NC; ++r )
    {
        m_selectedHistGridLines[ r * 2 + 0 ] = Vec2< float >( -1, -1 + (r-NC+1)*dr );
        m_selectedHistGridLines[ r * 2 + 1 ] = Vec2< float >(  1, -1 + (r-NC+1)*dr );
    }
}

void BALEEN::setPtclOpacity( double v )
{
    m_contextOpacity = v*v*v;

    renderLater();
}

//void BALEEN::setPtype( const std::string & ptype )
//{
//    // ...
//    renderLater();
//}

Vec2< float > BALEEN::spatialScaling2D()
{
    const double ADJUSTABILITY = 100.0;

    float mx = m_samplerWidget.controlPanel.panningWindowPanel.aspectRatioSlider.sliderPosition();
    float my = mx;

    mx = std::min( mx * 2.f - 1.f, 0.f );
    my = std::max( my * 2.f - 1.f, 0.f );
    my = -my;

    Vec2< float > modifier(
        std::pow( ADJUSTABILITY, mx ),
        std::pow( ADJUSTABILITY, my ) );

    std::string ptype    = m_particleCombo.selectedItemText();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        const auto & sp = m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second;
        return Vec2< float >( sp.scaling[ 0 ] * modifier.x(), sp.scaling[ 1 ] * modifier.y() );
    }
    else
    {
        const auto & sp = m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second;
        return Vec2< float >(  sp.scaling[ 0 ] * modifier.x(),  sp.scaling[ 1 ] * modifier.y() );
    }
}

void BALEEN::updateBoundingBoxes( const SpacePartitioningDefinition & spacePartitioning, int dataType, const std::string & ptype )
{
    Vec2< float > rX;
    Vec2< float > rY;

    Vec2< float > scaling = spatialScaling2D();

    if( dataType == PopulationDataType::PARTICLES )
    {
        rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( spacePartitioning.attributes[ 0 ] )->second * scaling.x();
        rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( spacePartitioning.attributes[ 1 ] )->second * scaling.y();

//        if( spacePartitioning.attributes[ 0 ] == "r" )
//        {
//            rX.a( 4.0 );
//        }
    }

    else if(  dataType == PopulationDataType::GRID_POINTS )
    {
        rX = m_dataSetManager->distributionManager().range( spacePartitioning.attributes[ 0 ] ) * scaling.x();
        rY = m_dataSetManager->distributionManager().range( spacePartitioning.attributes[ 1 ] ) * scaling.y();
    }

    m_contextBoundingBox.first.x( rX.a() );
    m_contextBoundingBox.first.y( rY.a() );
    m_contextBoundingBox.second.x( rX.b() );
    m_contextBoundingBox.second.y( rY.b() );

    m_miniMapSizeWorldSpace = Vec2< float >( ( rX.b() - rX.a() ) * 1.15, ( rY.b() - rY.a() ) * 1.15 );
}

void BALEEN::recalculateUiLayout()
{
    // set default layout parameters at initialization
    if( ! m_uiLayoutInitialized )
    {
        ////////////////////qDebug() << "setting default UI settings";

//        float offsetA = std::max( scaledWidth() / 8.0 , DETAILVIEW_MIN_WIDTH + 1 );
//        float offsetB = std::max( scaledWidth() - 2*offsetA - 2*DIVIDER_WIDTH, offsetA + SAMPLER_MIN_WIDTH + 1 );

//        float offsetD = scaledHeight() / 8.0 + DIVIDER_WIDTH;
//        float offsetC = 2*offsetD;

//        m_resizeWidgetA.setPosition( DIVIDER_WIDTH + offsetA, 0 );
//        m_resizeWidgetA.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT );

//        m_resizeWidgetB.setPosition( offsetB, offsetC+DIVIDER_WIDTH );
//        m_resizeWidgetB.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT - offsetC - DIVIDER_WIDTH );

//        ////////////////////qDebug() << "set part B to " << offsetB;

//        m_resizeWidgetD.setPosition( offsetA + DIVIDER_WIDTH*2, offsetD );
//        m_resizeWidgetD.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, DIVIDER_WIDTH );

//        m_resizeWidgetC.setPosition( offsetA + DIVIDER_WIDTH*2, offsetC );
//        m_resizeWidgetC.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, DIVIDER_WIDTH );


        float offsetA = std::max( scaledWidth() / 7.0 , DETAILVIEW_MIN_WIDTH + 1 );
        float offsetB = std::max( scaledWidth() - 2*offsetA - 2*DIVIDER_WIDTH, offsetA + SAMPLER_MIN_WIDTH + 1 );

        float offsetD = scaledHeight() / 8.0 + DIVIDER_WIDTH;
        float offsetC = 2*offsetD;

        m_resizeWidgetA.setPosition( DIVIDER_WIDTH + offsetA, 0 );
        m_resizeWidgetA.setSize( RESIZE_HANDLE_WIDTH, scaledHeight() - MENU_HEIGHT );

        m_resizeWidgetB.setPosition( offsetB, offsetC+DIVIDER_WIDTH );
        m_resizeWidgetB.setSize( RESIZE_HANDLE_WIDTH, scaledHeight() - MENU_HEIGHT - offsetC - DIVIDER_WIDTH );

        ////////////////////qDebug()() << "set part B to " << offsetB;

        m_resizeWidgetD.setPosition( offsetA + DIVIDER_WIDTH*2, offsetD );
        m_resizeWidgetD.setSize( scaledWidth() - offsetA - DIVIDER_WIDTH * 2, RESIZE_HANDLE_WIDTH );

        m_resizeWidgetC.setPosition( offsetA + DIVIDER_WIDTH*2, offsetC );
        m_resizeWidgetC.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, RESIZE_HANDLE_WIDTH );
    }
    else
    {
        qDebug() << "part b was already at " << m_resizeWidgetB.position().x();
    }

    //

    m_uiLayoutEdited = false;

    m_dataInfoPanelViewPort.width( m_resizeWidgetA.position().x() );
    m_dataInfoPanelViewPort.height( m_samplerWidget.controlPanel.size().y() );
    m_dataInfoPanelViewPort.offsetX( 0 );
    m_dataInfoPanelViewPort.offsetY( scaledHeight() - MENU_HEIGHT - m_samplerWidget.controlPanel.size().y() );

    m_timeLineWidget.setPosition( m_resizeWidgetA.position().x() + DIVIDER_WIDTH, 0 );
    m_timeLineWidget.setSize( scaledWidth() - m_timeLineWidget.position().x(), m_resizeWidgetD.position().y() );

    qDebug() << "timeline widget size is " << m_timeLineWidget.size().y();

    m_timeHistIsoViewport.width( m_timeLineWidget.size().x() );
    m_timeHistIsoViewport.height( m_resizeWidgetC.position().y() - m_resizeWidgetD.position().y() - DIVIDER_WIDTH );
    m_timeHistIsoViewport.offsetX( m_timeLineWidget.position().x() );
    m_timeHistIsoViewport.offsetY( m_resizeWidgetD.position().y() + DIVIDER_WIDTH );

//    m_timeHistIsoViewport.width(   m_timeLineWidget.size().x() );
//    m_timeHistIsoViewport.height(  m_timeLineWidget.size().y() );
//    m_timeHistIsoViewport.offsetX( m_timeLineWidget.position().x() );
//    m_timeHistIsoViewport.offsetY( m_timeLineWidget.position().y() );

    m_histogramViewPort.width( m_dataInfoPanelViewPort.width() );

    float Hw = m_histogramViewPort.width() - TEXT_ROW_HEIGHT*3;
    float Hh = Hw;// Hw * ( histRows / ( float ) histCols )
    float Dh = Hh + 9 * TEXT_ROW_HEIGHT + LABEL_ROW_HEIGHT;

    m_histogramViewPort.height( Dh );
    m_histogramViewPort.offsetY( m_timeLineWidget.position().y() + m_timeLineWidget.size().y() + DIVIDER_WIDTH );
    m_histogramViewPort.offsetX( 0 );

    m_dataSetInfoWidget.setSize( m_histogramViewPort.width(), m_samplerWidget.controlPanel.size().y() );
    m_dataSetInfoWidget.setPosition( 0, scaledHeight() - MENU_HEIGHT - m_dataSetInfoWidget.size().y() );

    m_miniMapViewPort.width( m_histogramViewPort.width() );
    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_miniMapViewPort.height( ( scaledHeight() - MENU_HEIGHT )
                                  - m_histogramViewPort.height() - m_timeLineWidget.size().y() - DIVIDER_WIDTH * 2 );
    }
    else
    {
        m_miniMapViewPort.height( ( scaledHeight() - MENU_HEIGHT - m_samplerWidget.controlPanel.size().y() )
                                  - m_histogramViewPort.height() - m_timeLineWidget.size().y() - DIVIDER_WIDTH * 2 );
    }

    m_miniMapViewPort.offsetY( m_histogramViewPort.offsetY() + m_histogramViewPort.height() + DIVIDER_WIDTH );
    m_miniMapViewPort.offsetX( 0 );

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    m_samplerWidget.visualizationWindow.viewPort().width(   m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x() - DIVIDER_WIDTH );
//    m_samplerWidget.visualizationWindow.viewPort().height(  scaledHeight() - m_resizeWidgetD.position().y() - DIVIDER_WIDTH - MENU_HEIGHT );
//    m_samplerWidget.visualizationWindow.viewPort().offsetX( m_resizeWidgetA.position().x() + DIVIDER_WIDTH );
//    m_samplerWidget.visualizationWindow.viewPort().offsetY( m_resizeWidgetD.position().y() + DIVIDER_WIDTH );

    /**********************************************************************************/

    m_samplerWidget.setPosition(
        m_resizeWidgetA.position().x() + DIVIDER_WIDTH,
        m_resizeWidgetC.position().y() + DIVIDER_WIDTH );

   qDebug() << "calling set sampleWidget size";
    m_samplerWidget.setSize(
        m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x() - DIVIDER_WIDTH,
        scaledHeight() - m_resizeWidgetC.position().y() - DIVIDER_WIDTH - MENU_HEIGHT  );

//    m_samplingViewPort.width( m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x() - DIVIDER_WIDTH );
//    m_samplingViewPort.height( scaledHeight() - m_resizeWidgetC.position().y() - DIVIDER_WIDTH - MENU_HEIGHT );
//    m_samplingViewPort.offsetX( m_resizeWidgetA.position().x() + DIVIDER_WIDTH );
//    m_samplingViewPort.offsetY( m_resizeWidgetC.position().y() + DIVIDER_WIDTH );

    m_sampler.setViewPort( m_samplerWidget.visualizationWindow.viewPort() );

/****************************Combo Logic*****************************************

    auto sComboPtrs = m_samplerWidget.visibleCombos();

    // update the global set of combo pointers, removing the invisible
    // sampler combos and adding the visible ones

    qDebug() << "grabbing combos";

    std::vector< TN::ComboWidget * >  allSamplerCombos = m_samplerWidget.controlPanel.combos();

    qDebug() << "got combos";

    std::set< TN::ComboWidget * > samplerCombos(
        allSamplerCombos.begin(),
        allSamplerCombos.end() );

    qDebug() << "copied combos";

    m_comboRefs.erase( std::remove_if(
                           m_comboRefs.begin(),
                           m_comboRefs.end(),
                           [&]( TN::ComboWidget * x )
    {
        return samplerCombos.count( x );
    }),
    m_comboRefs.end() );

    qDebug() << "erased combos";

    m_comboRefs.insert( m_comboRefs.end(), sComboPtrs.begin(), sComboPtrs.end() );

    qDebug() << "done inserting combos";

    /*********************************************************************************/

    m_auxViewPort.width( scaledWidth() - m_resizeWidgetB.position().x() - DIVIDER_WIDTH );
    m_auxViewPort.height( m_samplerWidget.size().y() );
    m_auxViewPort.offsetX( m_resizeWidgetB.position().x() + DIVIDER_WIDTH );
    m_auxViewPort.offsetY( m_samplerWidget.position().y() );

    m_negPosScalarButton.setSize( 54, BUTTON_HEIGHT );
    m_negPosScalarButton.setPosition( m_auxViewPort.offsetX() + m_helpButtonLinkedView.size().x() + 4, m_auxViewPort.offsetY() + m_auxViewPort.height() - BUTTON_HEIGHT - 4 );

    m_smoothRadiusSlider.setSize( ( m_auxViewPort.width() - 8 ) / 2 - 8 - 100, BUTTON_HEIGHT );
    m_smoothRadiusSlider.setPosition( m_auxViewPort.offsetX() + 100 + 4, m_auxViewPort.offsetY() + m_auxViewPort.height() - ( BUTTON_HEIGHT + 4 ) * 2 );
    m_gridColorScaleSlider.setSize( m_smoothRadiusSlider.size().x(), BUTTON_HEIGHT );
    m_gridColorScaleSlider.setPosition( m_smoothRadiusSlider.position().x() + m_smoothRadiusSlider.size().x() + 100 + 4,
                                        m_smoothRadiusSlider.position().y() );

    m_aggregationCombo.setSize( 120, BUTTON_HEIGHT );
    m_aggregationCombo.setPosition( m_negPosScalarButton.position().x() + m_negPosScalarButton.size().x() + 4,
                                    m_negPosScalarButton.position().y() );

    const int EDIT_WDTH = 40;

    int BTN_WDTH = ( m_dataInfoPanelViewPort.width() - MENU_SPACING * 3.0 ) / 2.0;
    const int LABEL_OFF1 = 120;
    const int BTN_WDTH3 = m_dataInfoPanelViewPort.width() - LABEL_OFF1 - MENU_SPACING * 4 - EDIT_WDTH + DIVIDER_WIDTH*2;
    int offS = LABEL_OFF1;

    //

    m_definePlotButton.setPosition( m_sampler.CONTROL_PANEL_SPACING + 40, m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - BUTTON_HEIGHT * 2 - DIVIDER_WIDTH  );
    m_definePlotButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_timePlotCombo.setPosition(  m_definePlotButton.position().x() + BUTTON_HEIGHT, m_definePlotButton.position().y() );
    m_timePlotCombo.setSize( m_dataInfoPanelViewPort.width() - EDIT_WDTH - BUTTON_HEIGHT - m_sampler.CONTROL_PANEL_SPACING - MENU_SPACING - 40, BUTTON_HEIGHT );

    m_editTimePlotButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editTimePlotButton.setPosition( m_timePlotCombo.position().x() + m_timePlotCombo.size().x(), m_timePlotCombo.position().y() );

    m_timePlotModeCombo.setPosition( m_sampler.CONTROL_PANEL_SPACING + 40, m_timePlotCombo.position().y() - BUTTON_HEIGHT - MENU_SPACING );
    m_timePlotModeCombo.setSize(  m_timePlotCombo.size().x() + EDIT_WDTH + BUTTON_HEIGHT, BUTTON_HEIGHT );

    //

    float SW_WDT = std::floor( ( ( m_histogramViewPort.width() - m_definePlotButton.position().x() )
                                 - ( m_histogramViewPort.width() -  m_editTimePlotButton.position().x() - m_editTimePlotButton.size().x() ) ) / 3.0 );

    m_timePlotHistogramSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timePlotHistogramSelector.setPosition( std::floor( m_definePlotButton.position().x() ), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    m_timePlotAllSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timePlotAllSelector.setPosition( std::floor( m_definePlotButton.position().x() ) + m_timePlotHistogramSelector.size().x(), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    m_timePlotBothSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timePlotBothSelector.setPosition( m_timePlotAllSelector.position().x() + m_timePlotAllSelector.size().x(), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    //

    m_isoValueSlider.setPosition( LABEL_OFF1 + MENU_SPACING, m_histogramViewPort.offsetY() + m_histogramViewPort.height()
                                  - BUTTON_HEIGHT * 2 - MENU_SPACING * 2 );
    m_isoValueSlider.setSize( BTN_WDTH3 + EDIT_WDTH + MENU_SPACING - 30, BUTTON_HEIGHT );

    ////////////////////////////////////

    const int BTN_WDTH2 = 94;//( m_miniMapViewPort.width() - MENU_SPACING*4 ) / 3.0;

    BTN_WDTH = scaledWidth() < 1901 ? 112 : 230;
    offS = 90;

//    m_saveProjectButton.setPosition( MENU_SPACING, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
//    m_saveProjectButton.setSize( BTN_WDTH2, BUTTON_HEIGHT );

//    offS += BTN_WDTH2 + MENU_SPACING;

//    m_loadProjectFileButton.setPosition( offS, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
//    m_loadProjectFileButton.setSize( BTN_WDTH2, BUTTON_HEIGHT );

//    offS += BTN_WDTH2 + MENU_SPACING;

//    m_openButton.setPosition( offS, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
//    m_openButton.setSize( BTN_WDTH2 + 6, BUTTON_HEIGHT );

    //offS = m_sampler.position().x() + MENU_SPACING * 3 + 75 + 180;
    //offS += 94 + MENU_SPACING * 3 + 90;

    m_datasetCombo.setPosition( offS, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
    m_datasetCombo.setSize( BTN_WDTH+4, BUTTON_HEIGHT );

    offS += BTN_WDTH + MENU_SPACING + 48;
    offS += MENU_SPACING*2;

    m_configurationCombo.setPosition( offS, m_datasetCombo.position().y() );
    m_configurationCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    offS += BTN_WDTH + BUTTON_HEIGHT*2 + MENU_SPACING * 2 + 90;

    m_saveButton.setPosition( m_configurationCombo.position().x()+ m_configurationCombo.size().x(), m_datasetCombo.position().y() );
    m_saveButton.setSize( BUTTON_HEIGHT*2, BUTTON_HEIGHT );

    m_particleCombo.setPosition( offS, m_datasetCombo.position().y() );
    m_particleCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    offS += BTN_WDTH + MENU_SPACING * 2 + 90;

    m_defineTimeSeriesButton.setPosition( offS, m_datasetCombo.position().y());
    m_defineTimeSeriesButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_timeSeriesCombo.setPosition( offS + BUTTON_HEIGHT, m_defineTimeSeriesButton.position().y() );
    m_timeSeriesCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    m_editTimeSeriesButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editTimeSeriesButton.setPosition( m_timeSeriesCombo.position().x() + m_timeSeriesCombo.size().x(), m_defineTimeSeriesButton.position().y() );

    m_defineFilterButton.setPosition( m_editTimeSeriesButton.position().x() + m_editTimeSeriesButton.size().x() + 74, m_datasetCombo.position().y());
    m_defineFilterButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_filterCombo.setPosition( m_defineFilterButton.position().x() + m_defineFilterButton.size().x(), m_defineFilterButton.position().y() );
    m_filterCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    m_editFilterButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editFilterButton.setPosition( m_filterCombo.position().x() + m_filterCombo.size().x(), m_defineFilterButton.position().y() );

    m_initiateSessionButton.setPosition( m_editFilterButton.size().x() + m_editFilterButton.position().x() + MENU_SPACING*2, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
    m_initiateSessionButton.setSize( 50, BUTTON_HEIGHT );

    offS = m_sampler.position().x() + MENU_SPACING * 3 + 75 + 180;
    offS += 94 + MENU_SPACING * 3 + 90;

    m_closeButton.setPosition( scaledWidth() - BUTTON_HEIGHT - MENU_SPACING, m_datasetCombo.position().y() );
    m_closeButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_definePhasePlotButton.setPosition(
        m_auxViewPort.offsetX() + MENU_SPACING + 50,
        m_auxViewPort.offsetY() + m_auxViewPort.height() - BUTTON_HEIGHT * 2 - MENU_SPACING * 3 );

    m_definePhasePlotButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_phasePlotCombo.setPosition(
        m_definePhasePlotButton.position().x() + BUTTON_HEIGHT,
        m_definePhasePlotButton.position().y() );

    m_phasePlotCombo.setSize( 180, BUTTON_HEIGHT );

    m_editPhasePlotButton.setPosition( m_phasePlotCombo.position().x() + m_phasePlotCombo.size().x(),
                                       m_phasePlotCombo.position().y() );
    m_editPhasePlotButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );

    m_phasePlotModeCombo.setPosition( m_auxViewPort.offsetX() + 170, scaledHeight() - MENU_HEIGHT * 2 + MENU_SPACING );
    m_phasePlotModeCombo.setSize(
        m_editPhasePlotButton.position().x() + m_editPhasePlotButton.size().x() + MENU_SPACING,
        BUTTON_HEIGHT );

    m_phasePlotInterpolationCombo.setPosition(  m_phasePlotModeCombo.position().x() + m_phasePlotModeCombo.size().x() + LABEL_OFF1 -EDIT_WDTH, m_phasePlotModeCombo.position().y() );
    m_phasePlotInterpolationCombo.setSize( m_phasePlotModeCombo.size().x(), BUTTON_HEIGHT );

    m_phasePlotBlendingCombo.setPosition( m_phasePlotCombo.position().x() + m_phasePlotCombo.size().x() + LABEL_OFF1 , m_phasePlotCombo.position().y() );
    m_phasePlotBlendingCombo.setSize( m_phasePlotModeCombo.size().x(), BUTTON_HEIGHT );

    isoCam.setAspectRatio( m_timeHistIsoViewport.width() / ( float ) m_timeHistIsoViewport.height() );

    //////////////////

    // Widget Labels / help buttons

    float labelMargin = 1;
    float hbH = m_definePlotButton.size().y();

    m_helpButtonTimeline.setPosition(
        labelMargin,
        m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonTimeline.setSize(
//        100 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonLinkedView.setPosition(
        m_auxViewPort.offsetX() + labelMargin,
        m_auxViewPort.offsetY() + m_auxViewPort.height() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonLinkedView.setSize(
//        130 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonDetailView.setPosition(
        m_histogramViewPort.offsetX() + labelMargin,
        m_histogramViewPort.offsetY() + m_histogramViewPort.height() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonDetailView.setSize(
//        190 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonMiniMap.setPosition(
        m_miniMapViewPort.offsetX() + labelMargin,
        m_miniMapViewPort.offsetY() + m_miniMapViewPort.height() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonMiniMap.setSize(
//        92 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonIsoView.setPosition(
        m_timeHistIsoViewport.offsetX() + labelMargin,
        m_timeHistIsoViewport.offsetY() + m_timeHistIsoViewport.height() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonIsoView.setSize(
//        170 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonBinning.setPosition(
        m_samplerWidget.controlPanel.position().x() + labelMargin,
        m_samplerWidget.controlPanel.position().y() + m_samplerWidget.controlPanel.size().y() - BUTTON_HEIGHT - labelMargin );
//    m_helpButtonBinning.setSize(
//        40 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonData.setPosition(
        labelMargin,
        m_datasetCombo.position().y() );
//    m_helpButtonData.setSize(
//        54 + BUTTON_HEIGHT,
//        hbH );

    for( HelpLabelWidget * hlRef : m_helpLabelWidgetRefs )
    {
        hlRef->setSize(
            20 + hlRef->text().size()*8,
            hbH );

        qDebug() << "set size " << hlRef->text().c_str();
    }

    // Warning Widget

    m_errorRemovalCombo.setSize( 120, m_datasetCombo.size().y() );
    m_inspectWarningsButton.setSize( 80, m_datasetCombo.size().y() );

//    m_warningWidget.setSize( 354,
//                             m_datasetCombo.size().y() );

//    m_warningWidget.setPosition( scaledWidth()
//                                 - m_warningWidget.size().x() /*- m_inspectWarningsButton.size().x()*/ - m_errorRemovalCombo.size().x() - 74,
//                                 m_datasetCombo.position().y() );

//    m_inspectWarningsButton.setPosition( m_warningWidget.position().x() + m_warningWidget.size().x(), m_datasetCombo.position().y() );

//    m_errorRemovalCombo.setPosition( m_inspectWarningsButton.position().x() /*+ m_inspectWarningsButton.size().x() */ + 74, m_datasetCombo.position().y() );

    ///

    const float TEX_B_S = BUTTON_HEIGHT;

    m_selectedHist2DButton.setSize( TEX_B_S + 8, TEX_B_S );
    m_selectedHist2DButton.setPosition(
        m_histogramViewPort.offsetX() + m_histogramViewPort.width() - TEX_B_S*2-MENU_SPACING-16,
        m_histogramViewPort.offsetY() + m_histogramViewPort.height() - TEX_B_S - DIVIDER_WIDTH );

    m_selectedHist3DButton.setSize( TEX_B_S + 8, TEX_B_S );
    m_selectedHist3DButton.setPosition(
        m_histogramViewPort.offsetX() + m_histogramViewPort.width()  - TEX_B_S-MENU_SPACING-8,
        m_histogramViewPort.offsetY() + m_histogramViewPort.height() - TEX_B_S -DIVIDER_WIDTH );

    m_phasePlotAllSelector.setSize( 140, BUTTON_HEIGHT );
    m_phasePlotSelectionSelector.setSize( 140, BUTTON_HEIGHT );

    m_phasePlotAllSelector.setPosition(       m_auxViewPort.offsetX() + m_auxViewPort.width() - 150, m_auxViewPort.offsetY() + 10 );
    m_phasePlotSelectionSelector.setPosition( m_phasePlotAllSelector.position().x() - 150, m_phasePlotAllSelector.position().y() );

    //////////////////

    if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        m_distSeriesModeCombo.setSize( m_histogramViewPort.width() - m_helpButtonTimeline.size().x() - DIVIDER_WIDTH*2, BUTTON_HEIGHT );
        m_distSeriesModeCombo.setPosition( m_helpButtonTimeline.position().x() + m_helpButtonTimeline.size().x() + DIVIDER_WIDTH, m_helpButtonTimeline.position().y() );
    }
    qDebug() << "done calculating UI layout ";
}

}
