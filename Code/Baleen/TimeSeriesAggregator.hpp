#ifndef TIMESERIESAGGREGATOR_HPP
#define TIMESERIESAGGREGATOR_HPP

namespace TN
{

namespace Aggregator
{

void aggregate(
    LineData & data,
    const std::string & ptype,
    const std::string & attribute,
    const SpacePartitioningDefinition & spacePartitioning,
    const Vec2< float > spatialScaling,
    const ParticleDataManeger & particleData,
    const TimeSeriesDefinition & timeSeries,
    const int selectedCell )
{
    const int FIRST = timeSeries.firstIdx;
    const int LAST  = timeSeries.lastIdx;
    const int STRIDE = timeSeries.idxStride;

    static std::vector< Vec2< double > > perThreadMeanRange;
    static std::vector< Vec2< double > > perThreadMeanSquaredRange;

    int nThreads;
    #pragma omp parallel
    {
        nThreads = omp_get_num_threads();
    }

    perThreadMeanRange.resize( nThreads );
    perThreadMeanSquaredRange.resize( nThreads );

    #pragma omp parallel
    {
        const int idx = omp_get_thread_num();
        perThreadMeanRange[ idx ] = Vec2< double >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
        perThreadMeanSquaredRange[ idx ] = perThreadMeanRange[ idx ];
    }

    const int NP = particleData.numLoadedParticles( ptype );

    #pragma omp parallel for
    for( int t = FIRST; t <= LAST; t += STRIDE )
    {
        const int T_ID = omp_get_thread_num();
        const int ti = ( t - FIRST ) / STRIDE;

        const std::vector< float > & xSpatial = *( particleData.values( t, ptype, spacePartitioning.attributes[ 0 ] ) );
        const std::vector< float > & ySpatial = *( particleData.values( t, ptype, spacePartitioning.attributes[ 1 ] ) );
        const std::vector< float > & w = *( particleData.values( t, ptype, attribute ) );

        meanWeights[ ti ] = 0.L;
        meanSquaredWeights[ ti ] = 0.L;

        int count = 0;
        for( int i = 0; i < NP; ++i )
        {
            if( m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() ) == selectedCell )
            {
                ++count;
                meanWeights[ ti ] += w[ i ];
                meanSquaredWeights[ ti ] += std::pow( static_cast< double >( w[ i ] ), 2.L );
            }
        }

        meanWeights[ ti ] /= static_cast< double >( count );
        perThreadMeanRange[ T_ID ].a( std::min( perThreadMeanRange[ T_ID ].a(), meanWeights[ ti ] ) );
        perThreadMeanRange[ T_ID ].b( std::max( perThreadMeanRange[ T_ID ].b(), meanWeights[ ti ] ) );

        meanSquaredWeights[ ti ] /= static_cast< double >( count );
        perThreadMeanSquaredRange[ T_ID ].a( std::min( perThreadMeanSquaredRange[ T_ID ].a(), meanSquaredWeights[ ti ] ) );
        perThreadMeanSquaredRange[ T_ID ].b( std::max( perThreadMeanSquaredRange[ T_ID ].b(), meanSquaredWeights[ ti ] ) );
    }

    for( int th = 0; th < nThreads; ++th )
    {
        meanRange.a( std::min( meanRange.a(), perThreadMeanRange[ th ].a() ) );
        meanRange.b( std::max( meanRange.b(), perThreadMeanRange[ th ].b() ) );
        meanSquaredRange.a( std::min( meanSquaredRange.a(), perThreadMeanSquaredRange[ th ].a() ) );
        meanSquaredRange.b( std::max( meanSquaredRange.b(), perThreadMeanSquaredRange[ th ].b() ) );
    }

}

}

}

#endif // TIMESERIESAGGREGATOR_HPP
