

#include "HistogramStackIso.hpp"

#include <iostream>
#include <string>

#include <chrono>
#include <ctime>

#include <cuda.h>
#include <cuda_runtime.h>

// ** Kernels ** //

__global__
void computeVolumeWeighted(
    const float * _vx,
    const float * _vy,
    const float * _sx,
    const float * _sy,
    const float * _w,
    float * _volume,
    const float FX,
    const float NX,
    const float FY,
    const float NY,
    const float sxMin,
    const float sxMax,
    const float syMin,
    const float syMax,
    const int histRows,
    const int histCols,
    const int numTimeSteps,
    const int numParticles )
{
    const int p = blockIdx.x * blockDim.x + threadIdx.x;

    // buffers may be padded
    if( p >= numParticles )
    {
        return;
    }

    const int BINS_PER_HIST = histCols * histRows;

    const float histRowsM1 = histRows - 1;
    const float histColsM1 = histCols - 1;

    for( int t = 0; t < numTimeSteps;  ++t )
    {
        const int idx  = t * numParticles + p;

        const float sx = _sx[ idx ];
        const float sy = _sy[ idx ];

        if( sx >= sxMin && sx <= sxMax
         && sy >= syMin && sy <= syMax )
        {
             const int r = max( min( floor( FY*_vy[ idx ] - NY ), histRowsM1 ), 0.f );
             const int c = max( min( floor( FX*_vx[ idx ] - NX ), histColsM1 ), 0.f );

             atomicAdd( &_volume[ t * BINS_PER_HIST + r * histCols + c ], _w[ idx ] );
        }
    }
}

__global__
void computeVolume(
    const float * _vx,
    const float * _vy,
    const float * _sx,
    const float * _sy,
    float * _volume,
    const float FX,
    const float NX,
    const float FY,
    const float NY,
    const float sxMin,
    const float sxMax,
    const float syMin,
    const float syMax,
    const int histRows, 
    const int histCols,
    const int numTimeSteps,
    const int numParticles )
{
    const int p = blockIdx.x * blockDim.x + threadIdx.x;

    // buffers may be padded
    if( p >= numParticles )
    {
        return;
    }

    const int BINS_PER_HIST = histCols * histRows;

    const float histRowsM1 = histRows - 1;
    const float histColsM1 = histCols - 1;

    for( int t = 0; t < numTimeSteps;  ++t )
    {
        const int idx  = t * numParticles + p;

        const float sx = _sx[ idx ];
        const float sy = _sy[ idx ];

        if( sx >= sxMin && sx <= sxMax
         && sy >= syMin && sy <= syMax )
        {
             const int r = max( min( floor( FY*_vy[ idx ] - NY ), histRowsM1 ), 0.f );
             const int c = max( min( floor( FX*_vx[ idx ] - NX ), histColsM1 ), 0.f );

             atomicAdd( &_volume[ t * BINS_PER_HIST + r * histCols + c ], 1.f );
        }
    }
}

// ** Instantiations ** //


/////////////////////////////////////////////////////////////////////////////////////////////

void HistogramStackIso::checkError( const std::string & mssg )
{
    cudaError_t error = cudaGetLastError();
    if( error != cudaSuccess )
    {
        // print the CUDA error message and exit
        printf("%s CUDA error: %s\n", mssg.c_str(), cudaGetErrorString( error ) );
        exit( -1 );
    }
}


HistogramStackIso::HistogramStackIso()
{
     vx_d = 0;
     vy_d = 0;
     sx_d = 0;
     sy_d = 0;
      w_d = 0;
 volume_d = 0;

     vx_h = 0;
     vy_h = 0;
     sx_h = 0;
     sy_h = 0;
      w_h = 0;
}


HistogramStackIso::~HistogramStackIso()
{
    cleanBuffers();
    cleanVolume();
}


void HistogramStackIso::cleanBuffers()
{
    cudaFree( vx_d );
    cudaFree( vy_d );
    cudaFree( sx_d );
    cudaFree( sy_d );
    cudaFree(  w_d );

    cudaFreeHost( vx_h );
    cudaFreeHost( vy_h );
    cudaFreeHost( sx_h );
    cudaFreeHost( sy_h );
    cudaFreeHost(  w_h );

    checkError( "cleaning buffers" );

    vx_d = 0;
    vy_d = 0;
    sx_d = 0;
    sy_d = 0;
     w_d = 0;

     vx_h = 0;
     vy_h = 0;
     sx_h = 0;
     sy_h = 0;
      w_h = 0;
}


void HistogramStackIso::cleanVolume()
{
    cudaFree( volume_d );
    volume_d = 0;
}


void HistogramStackIso::allocateVolume(
    int histRows,
    int histCols,
    int numSteps )
{
    // allocate the volume memory on the device
    cleanVolume();
    cudaMalloc( (void **) &volume_d, histRows * histCols * numSteps * sizeof ( float ) );

    checkError( "allocating volume" );
}


void HistogramStackIso::resizeBuffer(
    int numSteps,
    int numParticles,
    bool useWeight )
{
    cleanBuffers();
    cudaMalloc( (void **) & vx_d, numParticles * numSteps * sizeof ( float ) );
    cudaMalloc( (void **) & vy_d, numParticles * numSteps * sizeof ( float ) );
    cudaMalloc( (void **) & sx_d, numParticles * numSteps * sizeof ( float ) );
    cudaMalloc( (void **) & sy_d, numParticles * numSteps * sizeof ( float ) );

    if( useWeight )
    {
        cudaMalloc( (void **) &  w_d, numParticles * numSteps * sizeof ( float ) );
    }

    checkError( "resizing buffer" );
}

void HistogramStackIso::bufferS(
   const std::vector< float > & sx,
   const std::vector< float > & sy )
{
    const int N = sx.size();
    cudaMemcpy( sx_d, sx.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( sy_d, sy.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    checkError( "buffering data" );
}

void HistogramStackIso::bufferV(
    const std::vector< float > & vx,
    const std::vector< float > & vy )
{
    const int N = vx.size();
    cudaMemcpy( vx_d, vx.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( vy_d, vy.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    checkError( "buffering data" );
}

void HistogramStackIso::bufferW(
    const std::vector< float > & w )
{
    const int N = w.size();
    cudaMemcpy(  w_d,  w.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    checkError( "buffering data" );
}

//////////////////////////////////////////////////////////////////////////////////////////////
// With chunking


void HistogramStackIso::compute(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & sx,
        const std::vector< float > & sy,
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        int timeStepOffset,
        int timeStepChunk )
{
    int volumeOffset = ( timeStepOffset * histRows * histCols );
    cudaMemset( volume_d + volumeOffset, 0, histRows * histCols * timeStepChunk * sizeof( float ) );

    const int N = timeStepChunk * numParticles;
    int dataOffset = numParticles * timeStepOffset;
    cudaMemcpy( vx_d, vx.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( vy_d, vy.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( sx_d, sx.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( sy_d, sy.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );

    dim3 block_size( 512, 1, 1 );
    dim3 grid_size(  512 * std::ceil( ( float ) numParticles / float( 512 ) ), 1 );

    const float FX = histCols / ( vxMax - vxMin );
    const float FY = histRows / ( vyMax - vyMin );

    const float NX = FX * vxMin;
    const float NY = FY * vyMin;

    computeVolume<<< grid_size, block_size >>>(
            vx_d, vy_d, sx_d, sy_d, volume_d + volumeOffset,
            FX, NX,
            FY, NY,
            sxMin / sxScale, sxMax / sxScale,
            syMin / syScale, syMax / syScale,
            histRows,
            histCols,
            timeStepChunk,
            numParticles );

    cudaThreadSynchronize();
    cudaMemcpy( result.data() + volumeOffset, volume_d + volumeOffset, timeStepChunk * histRows * histCols * sizeof( float ), cudaMemcpyDeviceToHost );
}


void HistogramStackIso::compute(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & sx,
        const std::vector< float > & sy,
        const std::vector< float > & w,
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        int timeStepOffset,
        int timeStepChunk )
{
    int volumeOffset = ( timeStepOffset * histRows * histCols );
    cudaMemset( volume_d + volumeOffset, 0, histRows * histCols * timeStepChunk * sizeof( float ) );

    const int N = timeStepChunk * numParticles;
    int dataOffset = numParticles * timeStepOffset;
    cudaMemcpy( vx_d, vx.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( vy_d, vy.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( sx_d, sx.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( sy_d, sy.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy(  w_d,  w.data() + dataOffset, N * sizeof( float ), cudaMemcpyHostToDevice );

    dim3 block_size( 512, 1, 1 );
    dim3 grid_size(  512 * std::ceil( ( float ) numParticles / float( 512 ) ), 1 );

    const float FX = histCols / ( vxMax - vxMin );
    const float FY = histRows / ( vyMax - vyMin );

    const float NX = FX * vxMin;
    const float NY = FY * vyMin;

    computeVolumeWeighted<<< grid_size, block_size >>>(
            vx_d, vy_d, sx_d, sy_d, w_d, volume_d + volumeOffset,
            FX, NX,
            FY, NY,
            sxMin / sxScale, sxMax / sxScale,
            syMin / syScale, syMax / syScale,
            histRows,
            histCols,
            timeStepChunk,
            numParticles );

    cudaThreadSynchronize();
    cudaMemcpy( result.data() + volumeOffset, volume_d + volumeOffset, timeStepChunk * histRows * histCols * sizeof( float ), cudaMemcpyDeviceToHost );
}

/////////////////////////////////////////////////////////////////
// Without chunking


void HistogramStackIso::compute(
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        bool useWeight )
{
    cudaMemset( volume_d, 0, histRows * histCols * numTimeSteps * sizeof( float ) );

    dim3 block_size( 512, 1, 1 );
    dim3 grid_size(  512 * std::ceil( ( float ) numParticles / float( 512 ) ), 1 );

    const float FX = histCols / ( vxMax - vxMin );
    const float FY = histRows / ( vyMax - vyMin );

    const float NX = FX * vxMin;
    const float NY = FY * vyMin;

    if( useWeight )
    {
        computeVolumeWeighted<<< grid_size, block_size >>>(
                vx_d, vy_d, sx_d, sy_d, w_d, volume_d,
                FX, NX,
                FY, NY,
                sxMin / sxScale, sxMax / sxScale,
                syMin / syScale, syMax / syScale,
                histRows,
                histCols,
                numTimeSteps,
                numParticles );
    }
    else
    {
        computeVolume<<< grid_size, block_size >>>(
                vx_d, vy_d, sx_d, sy_d, volume_d,
                FX, NX,
                FY, NY,
                sxMin / sxScale, sxMax / sxScale,
                syMin / syScale, syMax / syScale,
                histRows,
                histCols,
                numTimeSteps,
                numParticles );
    }

    cudaThreadSynchronize();
    cudaMemcpy( result.data(), volume_d, numTimeSteps * histRows * histCols * sizeof( float ), cudaMemcpyDeviceToHost );
}
