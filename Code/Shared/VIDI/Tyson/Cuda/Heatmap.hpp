

#ifndef TN_CUDA_HEATMAP_HPP
#define TN_CUDA_HEATMAP_HPP

#include <cuda.h>
#include <cuda_runtime.h>

#include <vector>
#include <string>

class HeatMapper2D
{
    float * vx_d;
    float * vy_d;
    float * w_d;
    unsigned char * flags_d;
    float * result_d;

    int m_resolutionX;
    int m_resolutionY;
    int m_numParticles;

    void checkError( const std::string & mssg );
    void cleanBuffers();

public:

    void buffer(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & w,
        const std::vector< unsigned char > & f );

    void allocateResultDevice( int resolutionX, int resolutionY );
    void resizeBuffer( int numParticles );

    HeatMapper2D();
    ~HeatMapper2D();

    void compute(
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        std::vector< float > & result,
        std::pair< float, float > & range,
        bool maintainAspectRatio );
};

#endif
