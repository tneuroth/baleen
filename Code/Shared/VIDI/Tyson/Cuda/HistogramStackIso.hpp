

#ifndef HISTOGRAM_STACK_ISO_HPP
#define HISTOGRAM_STACK_ISO_HPP

#include <vector>
#include <string>
#include <cuda.h>
#include <cuda_runtime.h>


class HistogramStackIso
{
    float * vx_h;
    float * vy_h;
    float * sx_h;
    float * sy_h;
    float *  w_h;

    float * vx_d;
    float * vy_d;
    float * sx_d;
    float * sy_d;
    float *  w_d;

    float * volume_d;

    void clean();

public :

    HistogramStackIso();

//    void pinMemory(
//            std::vector< float > & vx,
//            std::vector< float > & vy,
//            std::vector< float > & sx,
//            std::vector< float > & sy,
//            std::vector< float > & w );

    void bufferS(
        const std::vector< float > & sx,
        const std::vector< float > & sy );

    void bufferV(
        const std::vector< float > & vx,
        const std::vector< float > & vy );

    void bufferW(
        const std::vector< float > & w );

    void allocateVolume(
        int histRows,
        int histCols,
        int numSteps );

    void cleanVolume();
    void cleanBuffers();

    std::pair< dim3, dim3 > computeWorkDims( int numSteps );

    void compute(
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRols,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        bool useWeight );

    void compute(
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        int timeStepOffset,
        int timeStepChunk,
        bool useWeight );

    void compute(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & sx,
        const std::vector< float > & sy,
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        int timeStepOffset,
        int timeStepChunk );

    void compute(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & sx,
        const std::vector< float > & sy,
        const std::vector< float > & w,
        float vxMin,
        float vxMax,
        float vyMin,
        float vyMax,
        float sxMin,
        float sxMax,
        float syMin,
        float syMax,
        float sxScale,
        float syScale,
        int numParticles,
        int histRows,
        int histCols,
        int numTimeSteps,
        std::vector< float > & result,
        int timeStepOffset,
        int timeStepChunk );

    void resizeBuffer(
        int numSteps,
        int numParticles,
        bool useWeight );

    void checkError( const std::string & mssg );

    ~HistogramStackIso();
};

#endif
