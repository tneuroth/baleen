

#ifndef TN_GRADIENT_UNCERTAINTY_CALCULATOR_HPP
#define TN_GRADIENT_UNCERTAINTY_CALCULATOR_HPP

#include "Vec.hpp"

#include <QDebug>

#include <vector>
#include <algorithm>
#include <cmath>

namespace TN
{

// NOTE: should use a Sobel Filter?, cite: http://jwcn.eurasipjournals.springeropen.com/articles/10.1186/s13638-015-0477-0
// For now assuming that values are equally likely, the variance is computed as 1/n sum( ( f_i - mean( f ) )^2, 1, n )

template< typename ResultType >
class GradientUncertaintyCalculator
{
    int m_cols, m_rows, m_resultRes, m_steps;
    double m_minF, m_maxF;
    std::vector< ResultType > m_tempFrameCounts;
    std::vector< std::vector< ResultType > > m_frameObservations;
    // For each observed frequency, calculate it's gradient
    // Then bin and compute the average of each bin
    // And the uncertainty of each bin...

    template < typename OrigionalType >
    void calculateStep(
        const std::vector< OrigionalType > & stack,
        std::vector< ResultType > & resultAverage,
        std::vector< ResultType > & resultVariance,
        Vec2< float > & avRange,
        Vec2< float > & vaRange,
        int step )
    {
        double fWidth = m_maxF - m_minF;

        std::fill( m_tempFrameCounts.begin(), m_tempFrameCounts.end(), 0.f );
        for( int i = 0; i < m_frameObservations.size(); ++i )
        {
            m_frameObservations[ i ].clear();
        }

        const int RESULT_OFFSET = step * m_resultRes;

        //! iterate over each pixel in the stack frame
        const int OFFSET = step * m_rows * m_cols;
        for( int row = 0; row < m_rows; ++row )
        {
            for( int col = 0; col < m_cols; ++col )
            {
                //! get the frequency of this bin of the stack frame
                double f = stack[ OFFSET + row * m_cols + col ];

                double dC;
                double dR;

                // compute the gradientMagnitude
                if( col < m_cols - 1 )
                {
                    dC = stack[ OFFSET + row * m_cols + ( col + 1 ) ] - f;
                }
                else
                {
                    dC = f - stack[ OFFSET + row * m_cols + ( col - 1 ) ];
                }

                if( row < m_rows - 1 )
                {
                    dR = stack[ OFFSET + ( row + 1 ) * m_cols + col ] - f;
                }
                else
                {
                    dR = f - stack[ OFFSET + ( row - 1 ) * m_cols + col ];
                }

                double gM = std::sqrt( dR*dR + dC*dC );
                int binIdx =  std::max( std::min( int( std::floor( ( ( f - m_minF ) / fWidth ) * m_resultRes ) ), m_resultRes - 1 ), 0 );

                resultAverage[ RESULT_OFFSET  + binIdx ] += gM;
                m_tempFrameCounts[   binIdx ] += 1.0;
                m_frameObservations[ binIdx ].push_back( gM );
            }
        }
        for( int i = 0; i < m_resultRes; ++i )
        {
            if( ! m_tempFrameCounts[ i ] )
            {
                resultAverage[  RESULT_OFFSET + i ] = 0;
                resultVariance[ RESULT_OFFSET + i ] = 0;
                continue;
            }

            resultAverage[  RESULT_OFFSET + i ] /= m_tempFrameCounts[ i ];
            for( int obs = 0; obs < m_frameObservations[ i ].size(); ++obs )
            {
                resultVariance[ RESULT_OFFSET + i ] +=
                    ( m_frameObservations[ i ][ obs ] - resultAverage[ RESULT_OFFSET +i ] )
                    * ( m_frameObservations[ i ][ obs ] - resultAverage[ RESULT_OFFSET +i ] );
            }

            resultVariance[  RESULT_OFFSET + i ] /= m_tempFrameCounts[ i ];

            avRange.a( std::min( avRange.a(), resultAverage[ RESULT_OFFSET + i ] ) );
            avRange.b( std::max( avRange.b(), resultAverage[ RESULT_OFFSET + i ] ) );

            vaRange.a( std::min( vaRange.a(), resultVariance[ RESULT_OFFSET + i ] ) );
            vaRange.b( std::max( vaRange.b(), resultVariance[ RESULT_OFFSET + i ] ) );
        }
    }

public:

    template < typename OrigionalType >
    void calculate(
        const std::vector< OrigionalType > & stack,
        const int COLS,
        const int ROWS,
        const int STEPS,
        const int RESULT_RESOLUTION,
        const double MAX_F,
        const double MIN_F,
        std::vector< ResultType > & resultAverage,
        std::vector< ResultType > & resultVariance,
        Vec2< float > & avRange,
        Vec2< float > & vaRange )
    {
        m_cols = COLS;
        m_rows = ROWS;
        m_steps = STEPS;
        m_resultRes = RESULT_RESOLUTION;
        m_minF = MIN_F;
        m_maxF = MAX_F;

        resultAverage  = std::vector< ResultType >( STEPS * RESULT_RESOLUTION, 0.f );
        resultVariance = std::vector< ResultType >( STEPS * RESULT_RESOLUTION, 0.f );

        m_tempFrameCounts = std::vector< ResultType >(  RESULT_RESOLUTION );
        m_frameObservations.resize(  RESULT_RESOLUTION );

        //qDebug() << "Before iterating over all " << STEPS << " steps " << ROWS << " " << COLS << " " << MIN_F << " " << MAX_F << " " << stack.size();

        for ( int t = 0; t < STEPS; ++t )
        {
            calculateStep(
                stack,
                resultAverage,
                resultVariance,
                avRange,
                vaRange,
                t );
        }
    }
};

}

#endif // TN_GRADIENT_UNCERTAINTY_CALCULATOR_HPP




