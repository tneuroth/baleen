#ifndef CONTOUR_HPP
#define CONTOUR_HPP

// This is a C++ implementation of Paul D. Bourk's origional CONREC countouring routine
// http://paulbourke.net/papers/conrec/

// This file is licensed under GPLv3 https://www.gnu.org/licenses/gpl-3.0.en.html

#include "Types/Vec.hpp"
#include <vector>

namespace TN
{

#define xsect(p1,p2) (h[p2]*xh[p1]-h[p1]*xh[p2])/(h[p2]-h[p1])
#define ysect(p1,p2) (h[p2]*yh[p1]-h[p1]*yh[p2])/(h[p2]-h[p1])

inline int atFlat( const int i, const int j, const int COLS )
{
    return i*COLS + j;
}

inline void conrec2( float * d,
                     const int ROWS,
                     const int COLS,
                     const std::vector< float > & xCoordinates,
                     const std::vector< float > & yCoordinates,
                     const std::vector< double > & levels,
                     std::vector< Vec2< float > > & results )
{
    const int im[4] = {0,1,1,0},jm[4]= {0,0,1,1};
    const int castab[3][3][3] =
    {
        {
            {0,0,8},{0,2,5},{7,6,9}
        },
        {
            {0,3,4},{1,3,1},{4,3,0}
        },
        {
            {9,6,7},{5,2,0},{8,0,0}
        }
    };

    for ( int j = COLS - 2; j >= 0; --j )
    {
        for ( int i = 0; i <= ROWS - 2; ++i )
        {
            const double   V = d[ atFlat(   i,   j, COLS ) ];
            const double V01 = d[ atFlat(   i, j+1, COLS ) ];
            const double V10 = d[ atFlat( i+1,   j, COLS ) ];
            const double V11 = d[ atFlat( i+1, j+1, COLS ) ];

            double dmin = std::min( std::min( V, V01 ), std::min( V10, V11 ) );
            double dmax = std::max( std::max( V, V01 ), std::max( V10, V11 ) );

            if ( dmax >= levels.front() || dmin <= levels.back() )
            {
                for ( int k = 0; k < levels.size(); ++k )
                {
                    if ( levels[ k ] >= dmin && levels[ k ] <= dmax )
                    {
                        int sh[ 5 ];
                        double h[ 5 ];
                        double xh[ 5 ];
                        double yh[ 5 ];

                        for ( int m = 4; m >= 0; --m )
                        {
                            if ( m > 0 )
                            {
                                h[ m ] = d[ atFlat( ( i + im[ m - 1 ] ), ( j + jm[ m - 1 ] ), COLS ) ] - levels[ k ];

                                xh[ m ] = xCoordinates[ i + im[ m - 1 ] ];
                                yh[ m ] = yCoordinates[ j + jm[ m - 1 ] ];
                            }
                            else
                            {
                                h[ 0 ] = 0.25 * ( h[ 1 ] + h[ 2 ] + h[ 3 ] + h[ 4 ] );

                                xh[ 0 ] = 0.5 * ( xCoordinates[ i ] + xCoordinates[ i + 1 ] );
                                yh[ 0 ] = 0.5 * ( yCoordinates[ j ] + yCoordinates[ j + 1 ] );
                            }

                            if (h[ m ] > 0.0 )
                            {
                                sh[ m ] = 1;
                            }
                            else if ( h[ m ] < 0.0 )
                            {
                                sh[ m ] = -1;
                            }
                            else
                            {
                                sh[ m ] = 0;
                            }
                        }

                        int m3 = 0;
                        for ( int m = 1; m <= 4; ++m )
                        {
                            if( m != 4 )
                            {
                                m3 = m + 1;
                            }
                            else
                            {
                                m3 = 1;
                            }

                            double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

                            int case_value = castab[ sh[ m ] +1 ][ sh[ 0 ] + 1 ][ sh[ m3 ] + 1 ];

                            if ( case_value != 0 )
                            {
                                switch (case_value)
                                {

                                case 1:

                                    x1=xh[m];
                                    y1=yh[m];
                                    x2=xh[0];
                                    y2=yh[0];

                                    break;

                                case 2:

                                    x1=xh[0];
                                    y1=yh[0];
                                    x2=xh[m3];
                                    y2=yh[m3];

                                    break;

                                case 3:

                                    x1=xh[m3];
                                    y1=yh[m3];
                                    x2=xh[m];
                                    y2=yh[m];

                                    break;

                                case 4:

                                    x1=xh[m];
                                    y1=yh[m];
                                    x2=xsect(0,m3);
                                    y2=ysect(0,m3);

                                    break;

                                case 5:

                                    x1=xh[0];
                                    y1=yh[0];
                                    x2=xsect(m3,m);
                                    y2=ysect(m3,m);

                                    break;

                                case 6:

                                    x1=xh[m3];
                                    y1=yh[m3];
                                    x2=xsect(m,0);
                                    y2=ysect(m,0);

                                    break;

                                case 7:

                                    x1=xsect(m,0);
                                    y1=ysect(m,0);
                                    x2=xsect(0,m3);
                                    y2=ysect(0,m3);

                                    break;

                                case 8:

                                    x1=xsect(0,m3);
                                    y1=ysect(0,m3);
                                    x2=xsect(m3,m);
                                    y2=ysect(m3,m);

                                    break;

                                case 9:

                                    x1=xsect(m3,m);
                                    y1=ysect(m3,m);
                                    x2=xsect(m,0);
                                    y2=ysect(m,0);

                                    break;

                                default:
                                    break;
                                }

                                results.push_back( Vec2< float >( x1, y1 ) );
                                results.push_back( Vec2< float >( x2, y2 ) );
                            }
                        }
                    }
                }
            }
        }
    }
}

}


#endif // CONTOUR_HPP

