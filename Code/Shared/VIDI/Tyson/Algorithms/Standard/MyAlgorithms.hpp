

#ifndef PARALLELALGORITHM_CPP
#define PARALLELALGORITHM_CPP

//#include <QDebug>

#include "Types/Vec.hpp"

#include <vector>
#include <algorithm>

namespace TN
{

namespace Parallel
{

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );

template < typename T1, typename T2 >
void sortTogether( T1 * v1,
                   T2 * v2,
                   const size_t SZ )
{
    std::vector< std::size_t > indices( SZ );

    std::vector< T1 > copy1( SZ );
    std::vector< T2 > copy2( SZ );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
        copy1[ i ] = v1[ i ];
        copy2[ i ] = v2[ i ];
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( v1[ a ] < v1[ b ] );
    }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}


template < typename T1, typename T2 >
void sortTogether( std::vector< T1 > & v1,
                   std::vector< T2 > & v2 )
{
    const int SZ = v1.size();

    std::vector< std::size_t > indices( SZ );
    std::vector< T1 > copy1 = v1;
    std::vector< T2 > copy2 = v2;

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( v1[ a ] < v1[ b ] );
    }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}

template < typename POINT_TYPE, typename ID_TYPE >
inline void sortedIndicesXY(
    const std::vector< POINT_TYPE > & x,
    const std::vector< POINT_TYPE > & y,
    std::vector< ID_TYPE > & indices )
{
    const int SZ = x.size();
    indices.resize( SZ );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( ( x[ a ] < x[ b ] ) || ( x[ a ] == x[ b ] && y[ a ] < y[ b ] ) );
    }
    );
}

}

namespace Sequential
{

template< typename T >
TN::Vec2< double > getRange( const T * values, const int64_t N )
{
    Vec2< double > result( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max()  );
    for( size_t i = 0; i < N; ++i )
    {
        result.a( std::min( result.a(), ( double ) values[ i ] ) );
        result.b( std::max( result.b(), ( double ) values[ i ] ) );
    }
    return result;
}

template< typename T >
TN::Vec2< double > getRange( const std::vector< T > & values )
{
    return getRange(  values.data(), values.size() );
}

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );

}

}

#endif // PARALLELALGORITHM_CPP
