

#ifndef TN_UTIL_H
#define TN_UTIL_H

#include "Types/Vec.hpp"
#include "MyAlgorithms.hpp"

#include <QMatrix4x4>
//#include <QOpenGLFunctions>
#include <GL/glew.h>

#include <QOpenGLDebugLogger>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <fstream>
#include <cmath>
#include <chrono>

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 4 )
{
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}

namespace TN
{

inline void blur( std::vector< float > & d, int width, int height )
{

    //qDebug() << "bluring";
    const float kernel[3][3] =
    {
        0.077847,	0.123317,	0.077847,
        0.123317,	0.195346,	0.123317,
        0.077847,	0.123317,	0.077847,
    };

    for( int i = 0; i < width; ++i )
    {
        for( int j = 0; j < height; ++j )
        {
            float sum = 0;
            for( int x = i-1, xi=0; x < i + 1 && x < width && x >= 0; ++x, ++xi )
            {
                for( int y = j - 1, yi = 0; y < j + 1  && y < height && y >= 0; ++y, ++yi )
                {
                    sum += kernel[xi][yi] * d[ y*width+x ];
                }
            }
            d[ j*width+i ] = sum;
        }
    }

    for( int i = 0; i < width-1; ++i )
    {
        for( int j = 0; j < height; ++j )
        {
            d[ j*width+i ] = d[ j*width+i+1 ];
        }
    }

    for( int i = 0; i < width; ++i )
    {
        for( int j = 0; j < height-1; ++j )
        {
            d[ j*width+i ] = d[ (j+1)*width+i ];
        }
    }

    for( int i = 0; i < width-1; ++i )
    {
        for( int j = 0; j < height; ++j )
        {
            d[ j*width+i ] = d[ j*width+i+1 ];
        }
    }

    for( int i = 0; i < width; ++i )
    {
        for( int j = 0; j < height-1; ++j )
        {
            d[ j*width+i ] = d[ (j+1)*width+i ];
        }
    }
}

inline std::string getTimeStamp()
{
    auto timeNow = std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() );
    return std::ctime( & timeNow );
}

inline Vec3< float > getPosFromView( const QMatrix4x4 & V )
{
    QVector4D pos = V.inverted().column( 4 );
    //qDebug() << V;
    return Vec3< float >( pos.x(), pos.y(), pos.z() );
}

inline Vec3< float > distBasedInterpolation(
    Vec2< float > & p,
    Vec2< float > & v1,
    Vec2< float > & v2,
    Vec2< float > & v3,
    Vec3< float > & f1,
    Vec3< float > & f2,
    Vec3< float > & f3
)
{

    double a1 = p.distance( v1 );
    double a2 = p.distance( v2 );
    double a3 = p.distance( v3 );

    double a = a1 + a2 + a3;

    a1 = ( 1.0 - a1/a ) / 2.0;
    a2 = ( 1.0 - a2/a ) / 2.0;
    a3 = ( 1.0 - a3/a ) / 2.0;

    return f1*a1 + f2*a2 + f3*a3;
}

inline Vec3< float > barycentricInterpolation(
    const TN::Vec2< float >  & p,
    const TN::Vec2< float >  & v1,
    const TN::Vec2< float >  & v2,
    const TN::Vec2< float >  & v3,
    const TN::Vec3< float > & f1,
    const TN::Vec3< float > & f2,
    const TN::Vec3< float > & f3
)
{

    double a1 = std::abs( p.x() * (v2.y() - v3.y() )
                          + v2.x() * (v3.y() -  p.y() )
                          + v3.x() * ( p.y() - v2.y() ) ) / 2.0;

    double a2 = std::abs( v1.x() * ( p.y() - v3.y() )
                          +   p.x() * (v3.y() - v1.y() )
                          +  v3.x() * (v1.y() -  p.y() ) ) / 2.0;

    double a3 = std::abs( v1.x() * (v2.y() -  p.y() )
                          + v2.x() * ( p.y() - v1.y() )
                          +  p.x() * (v1.y() - v2.y() ) ) / 2.0;

    double a  = std::abs( v1.x() * (v2.y() - v3.y() )
                          + v2.x() * (v3.y() - v1.y() )
                          + v3.x() * (v1.y() - v2.y() ) ) / 2.0;

    return

        TN::Vec3< float >( a1*f1.x() + a2*f2.x() + a3*f3.x(),
                           a1*f1.y() + a2*f2.y() + a3*f3.y(),
                           a1*f1.z() + a3*f2.z() + a3*f3.z() ) / a;
}


inline int intersect( Vec2< float > A1, Vec2< float > B1, Vec2< float > A2, Vec2< float > B2, Vec2< float > &intersection )
{
    double denom = ((B2.y() - A2.y() )*( B1.x() - A1.x())) -
                   ((B2.x() -  A2.x() ) *( B1.y() - A1.y()));

    double nume_a = ((B2.x() - A2.x())*(A1.y() - A2.y() ) ) -
                    ((B2.y() - A2.y())*(A1.x() - A2.x() ) );

    double nume_b = ((B1.x() - A1.x())*(A1.y() - A2.y() ) ) -
                    ((B1.y() - A1.y())*(A1.x() - A2.x() ) );

    if( std::abs( denom ) < 1.f )
    {
        if(nume_a == 0.0f && nume_b == 0.0f)
        {
            return -1;
        }
        return -1;
    }

    double ua = nume_a / denom;
    double ub = nume_b / denom;

    if(ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f)
    {
        // Get the intersection point.
        intersection.x( A1.x() + ua*(B1.x() - A1.x() ) );
        intersection.y( A1.y() + ua*(B1.y() - A1.y() ) );

        return 1;
    }

    return -1;
}

inline Vec3< float > interpolate3(
    TN::Vec2< float >  & p,
    TN::Vec2< float >  & v1,
    TN::Vec2< float >  & v2,
    TN::Vec2< float >  & v3,
    Vec3< float > & f1,
    Vec3< float > & f2,
    Vec3< float > & f3
)
{

    TN::Vec2< float >  pi;

    intersect( v1, v2, v3, v3 + ( p-v3 )*10, pi );

    double d1 = v1.distance( v2 );
    double d2 = v1.distance( pi );
    double r = d2 / d1;

    Vec3< float > fi = f2 * r + f1 * ( 1 - r );

    double d3 = v3.distance( pi );
    double d4 = v3.distance(  p );
    double r2 = d4 / d3;

    return fi*r2 + f3*( 1 - r2 );
}

inline void computeSquareToQuad(
    double x0,
    double y0,
    double x1,
    double y1,
    double x2,
    double y2,
    double x3,
    double y3,
    QMatrix4x4 & mat
)
{
    double dx1 = x1 - x2,    dy1 = y1 - y2;
    double dx2 = x3 - x2,    dy2 = y3 - y2;
    double sx = x0 - x1 + x2 - x3;
    double sy = y0 - y1 + y2 - y3;
    double g = (sx * dy2 - dx2 * sy) / (dx1 * dy2 - dx2 * dy1);
    double h = (dx1 * sy - sx * dy1) / (dx1 * dy2 - dx2 * dy1);
    double a = x1 - x0 + g * x1;
    double b = x3 - x0 + h * x3;
    double c = x0;
    double d = y1 - y0 + g * y1;
    double e = y3 - y0 + h * y3;
    double f = y0;

    mat.data()[ 0] = a;
    mat.data()[ 1] = d;
    mat.data()[ 2] = 0;
    mat.data()[ 3] = g;
    mat.data()[ 4] = b;
    mat.data()[ 5] = e;
    mat.data()[ 6] = 0;
    mat.data()[ 7] = h;
    mat.data()[ 8] = 0;
    mat.data()[ 9] = 0;
    mat.data()[10] = 1;
    mat.data()[11] = 0;
    mat.data()[12] = c;
    mat.data()[13] = f;
    mat.data()[14] = 0;
    mat.data()[15] = 1;
}

inline void computeQuadToSquare(
    double x0,
    double y0,
    double x1,
    double y1,
    double x2,
    double y2,
    double x3,
    double y3,
    QMatrix4x4 & mat
)

{
    computeSquareToQuad(x0,y0,x1,y1,x2,y2,x3,y3, mat);

    // invert through adjoint

    double a = mat.data()[ 0],      d = mat.data()[ 1],    /* ignore */            g = mat.data()[ 3];
    double b = mat.data()[ 4],      e = mat.data()[ 5],    /* 3rd col*/            h = mat.data()[ 7];
    /* ignore 3rd row */
    double c = mat.data()[12],      f = mat.data()[13];

    double A =     e - f * h;
    double B = c * h - b;
    double C = b * f - c * e;
    double D = f * g - d;
    double E =     a - c * g;
    double F = c * d - a * f;
    double G = d * h - e * g;
    double H = b * g - a * h;
    double I = a * e - b * d;

    // Probably unnecessary since 'I' is also scaled by the determinant,
    //   and 'I' scales the homogeneous coordinate, which, in turn,
    //   scales the X,Y coordinates.
    // Determinant  =   a * (e - f * h) + b * (f * g - d) + c * (d * h - e * g);
    double idet = 1.0f / (a * A           + b * D           + c * G);

    mat.data()[ 0] = A * idet;
    mat.data()[ 1] = D * idet;
    mat.data()[ 2] = 0;
    mat.data()[ 3] = G * idet;
    mat.data()[ 4] = B * idet;
    mat.data()[ 5] = E * idet;
    mat.data()[ 6] = 0;
    mat.data()[ 7] = H * idet;
    mat.data()[ 8] = 0       ;
    mat.data()[ 9] = 0       ;
    mat.data()[10] = 1;
    mat.data()[11] = 0       ;
    mat.data()[12] = C * idet;
    mat.data()[13] = F * idet;
    mat.data()[14] = 0;
    mat.data()[15] = I * idet;
}

double inline sign( double x, double y,  TN::Vec2< float > v1, TN::Vec2< float > v2 )
{

    return ( x - v2.x() ) * ( v1.y() - v2.y() ) - ( v1.x() - v2.x() ) * ( y - v2.y() );
}

bool inline insideTri( TN::Vec2< float > v1, TN::Vec2< float > v2, TN::Vec2< float > v3, TN::Vec2< float > p )
{

    bool b1, b2, b3;

    b1 = sign( p.x(), p.y(), v1, v2 ) <= 0.f;
    b2 = sign( p.x(), p.y(), v2, v3 ) <= 0.f;
    b3 = sign( p.x(), p.y(), v3, v1 ) <= 0.f;

    return ( ( b1 == b2 ) && (b2 == b3 ) );
}

inline double slcDist( double lon1, double lat1, double lon2, double lat2  )
{

    const double PI = std::acos( -1.0 );

    double dLon = ( lon2 - lon1 );

    double y = std::sin( dLon ) * std::cos( lat2 );
    double x = std::cos(lat1) * std::sin(lat2) - std::sin(lat1) * std::cos(lat2)*std::cos(dLon);
    double brng = std::atan2(y, x) * 180.0 / PI;

    while( brng > 360.0 )
    {
        brng -= 360.0;
    }

    return 360.0 - brng;
}

inline void fix( std::vector<Vec2< float >> verts, Vec2< float > center )
{

    std::ofstream out( "outline_fixed.txt" );
    std::ofstream outb( "fblend.obj" );

    outb << "o f\n";

    Vec2< float > start( verts[0].r(), verts[0].theta() );
    start = start.toCartesian() + center;

    int vtsz = verts.size();
    for ( int j = 0; j < vtsz; )
    {

        double h = 0.001;

        Vec2< float > current = verts[j].toCartesian() + center;

        if ( start.distance( current ) > h )
        {
            double l = start.distance( current );
            double x = ( current.x() - start.x() ) * ( h / l ) + start.x();
            double y = ( current.y() - start.y() ) * ( h / l ) + start.y();
            Vec2< float > np( x, y );
            start = np;
            out << x << " " << y << "\n";
            outb << "v " << x << " " << y << " " << 0.f << "\n";
        }
        else
        {
            ++j;
        }
    }

    out.close();
    outb.close();
}

template < typename T >
inline int pnpoly( const std::vector< TN::Vec2< T > > & vert, TN::Vec2< T > test )
{
    int i, j, c = 0, nvert = vert.size();
    for (i = 0, j = nvert-1; i < nvert; j = i++)
    {
        if ( ( ( vert[i].y() > test.y() ) != (vert[j].y() > test.y() ) ) &&
                ( test.x() < ( vert[j].x() - vert[i].x() ) * ( test.y() - vert[i].y() ) / ( vert[j].y() - vert[i].y() ) + vert[i].x() ) )
            c = !c;
    }
    return c;
}

// Check if the points are clockwise ordered
template< typename T >
inline bool CCW(
    const TN::Vec2< T > & a,
    const TN::Vec2< T > & b,
    const TN::Vec2< T > & c )
{
    return ( ( b.x() - a.x() ) * ( c.y() - a.y() ) ) > ( ( b.y() - a.y() ) * ( c.x() - a.x() ) );
}

template < typename POINT_TYPE, typename ID_TYPE >
inline void convexHull(
    const std::vector< POINT_TYPE > & px,
    const std::vector< POINT_TYPE > & py,
    std::vector< ID_TYPE > & b )
{
    ID_TYPE SZ = px.size();
    b.clear();

    if ( SZ <= 3 )
    {
        b.resize( SZ );
        for( size_t i = 0; i < SZ; ++i )
        {
            b[ i ] = i;
        }
    }

    std::vector< ID_TYPE > sortedIndices( SZ );
    TN::Parallel::sortedIndicesXY( px, py, sortedIndices );

    for( int i = 0; i < SZ; ++i )
    {
        const POINT_TYPE x = px[ sortedIndices[ i ] ];
        const POINT_TYPE y = py[ sortedIndices[ i ] ];

        while ( b.size() >= 2
                && ! CCW( Vec2< POINT_TYPE >( px[ b[ b.size() - 2 ] ], py[ b[ b.size() - 2 ] ] ),
                          Vec2< POINT_TYPE >( px[ b[ b.size() - 1 ] ], py[ b[ b.size() - 1  ]] ),
                          Vec2< POINT_TYPE >( x, y ) ) )
        {
            b.pop_back();
        }
        b.push_back( sortedIndices[ i ] );
    }

    const ID_TYPE BMX = b.size() + 1;
    for( int i = px.size() - 2; i >= 0; --i )
    {
        const POINT_TYPE x = px[ sortedIndices[ i ] ];
        const POINT_TYPE y = py[ sortedIndices[ i ] ];

        while ( b.size() >= BMX
                && ! CCW( Vec2< POINT_TYPE >( px[ b[ b.size() - 2 ] ], py[ b[ b.size() - 2 ] ] ),
                          Vec2< POINT_TYPE >( px[ b[ b.size() - 1 ] ], py[ b[ b.size() - 1  ]] ),
                          Vec2< POINT_TYPE >( x, y ) ) )
        {
            b.pop_back();
        }
        b.push_back( sortedIndices[ i ] );
    }
}

inline void checkError(const QString &prefix)
{
    GLenum glErr = glGetError();

    while(glErr != GL_NO_ERROR)
    {
        QString error;
        switch (glErr)
        {
        case GL_NO_ERROR:
            error="NO_ERROR";
            break;
        case GL_INVALID_OPERATION:
            error="INVALID_OPERATION";
            break;
        case GL_INVALID_ENUM:
            error="INVALID_ENUM";
            break;
        case GL_INVALID_VALUE:
            error="INVALID_VALUE";
            break;
        case GL_OUT_OF_MEMORY:
            error="OUT_OF_MEMORY";
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            error="INVALID_FRAMEBUFFER_OPERATION";
            break;
        }

        if(error != "NO_ERROR")
        {
            //qDebug() << prefix << ":" << error;
        }

        glErr = glGetError();
    }
}

}

#endif // TN_UTIL_H
