

#include "Algorithms/Standard/MyAlgorithms.hpp"

#include <limits>
#include <cmath>

namespace TN
{

template< typename T >
T Parallel::max( const std::vector< T > & v )
{
    const int END = v.size();

    T mx( 0 );
    if( END )
    {
        mx = v[ 0 ];
    }

    #pragma omp parallel for reduction(max:mx)
    for( int i = 1; i < END; ++i )
    {
        if( v[ i ] > mx )
        {
            mx = v[ i ];
        }
    }

    return mx;
}

template< typename T >
T Parallel::min( const std::vector< T > & v )
{
    const int END = v.size();

    T mn( 0 );

    if( END )
    {
        mn = v[ 0 ];
    }

    #pragma omp parallel for reduction(min:mn)
    for( int i = 1; i < END; ++i )
    {
        if( v[ i ] < mn )
        {
            mn = v[ i ];
        }
    }

    return mn;
}

template< typename T >
T Parallel::maxMag( const std::vector< T > & v )
{
    return std::max( std::abs( Parallel::max< T >( v ) ), std::abs( Parallel::min< T >( v ) ) );
}

template                  float Parallel::min<                  float >( const std::vector<                  float > & v );
template                 double Parallel::min<                 double >( const std::vector<                 double > & v );
template            long double Parallel::min<            long double >( const std::vector<            long double > & v );
//template          unsigned char Parallel::min<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Parallel::min<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Parallel::min<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Parallel::min<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Parallel::min< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Parallel::min<                   char >( const std::vector<                   char > & v );
template                  short Parallel::min<                  short >( const std::vector<                  short > & v );
template                    int Parallel::min<                    int >( const std::vector<                    int > & v );
template               long int Parallel::min<               long int >( const std::vector<               long int > & v );
template          long long int Parallel::min<          long long int >( const std::vector<          long long int > & v );

template                  float Parallel::max<                  float >( const std::vector<                  float > & v );
template                 double Parallel::max<                 double >( const std::vector<                 double > & v );
template            long double Parallel::max<            long double >( const std::vector<            long double > & v );
//template          unsigned char Parallel::max<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Parallel::max<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Parallel::max<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Parallel::max<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Parallel::max< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Parallel::max<                   char >( const std::vector<                   char > & v );
template                  short Parallel::max<                  short >( const std::vector<                  short > & v );
template                    int Parallel::max<                    int >( const std::vector<                    int > & v );
template               long int Parallel::max<               long int >( const std::vector<               long int > & v );
template          long long int Parallel::max<          long long int >( const std::vector<          long long int > & v );

template                  float Parallel::maxMag<                  float >( const std::vector<                  float > & v );
template                 double Parallel::maxMag<                 double >( const std::vector<                 double > & v );
template            long double Parallel::maxMag<            long double >( const std::vector<            long double > & v );
//template          unsigned char Parallel::maxMag<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Parallel::maxMag<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Parallel::maxMag<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Parallel::maxMag<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Parallel::maxMag< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Parallel::maxMag<                   char >( const std::vector<                   char > & v );
template                  short Parallel::maxMag<                  short >( const std::vector<                  short > & v );
template                    int Parallel::maxMag<                    int >( const std::vector<                    int > & v );
template               long int Parallel::maxMag<               long int >( const std::vector<               long int > & v );
template          long long int Parallel::maxMag<          long long int >( const std::vector<          long long int > & v );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sequential

template< typename T >
T Sequential::max( const std::vector< T > & v )
{
    const int END = v.size();

    T mx( 0 );
    if( END )
    {
        mx = v[ 0 ];
    }

    for( int i = 1; i < END; ++i )
    {
        if( v[ i ] > mx )
        {
            mx = v[ i ];
        }
    }

    return mx;
}

template< typename T >
T Sequential::min( const std::vector< T > & v )
{
    const int END = v.size();

    T mn( 0 );

    if( END )
    {
        mn = v[ 0 ];
    }

    for( int i = 1; i < END; ++i )
    {
        if( v[ i ] < mn )
        {
            mn = v[ i ];
        }
    }

    return mn;
}

template< typename T >
T Sequential::maxMag( const std::vector< T > & v )
{
    return std::max( std::abs( Sequential::max< T >( v ) ), std::abs( Sequential::min< T >( v ) ) );
}

template                  float Sequential::min<                  float >( const std::vector<                  float > & v );
template                 double Sequential::min<                 double >( const std::vector<                 double > & v );
template            long double Sequential::min<            long double >( const std::vector<            long double > & v );
//template          unsigned char Sequential::min<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Sequential::min<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Sequential::min<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Sequential::min<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Sequential::min< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Sequential::min<                   char >( const std::vector<                   char > & v );
template                  short Sequential::min<                  short >( const std::vector<                  short > & v );
template                    int Sequential::min<                    int >( const std::vector<                    int > & v );
template               long int Sequential::min<               long int >( const std::vector<               long int > & v );
template          long long int Sequential::min<          long long int >( const std::vector<          long long int > & v );

template                  float Sequential::max<                  float >( const std::vector<                  float > & v );
template                 double Sequential::max<                 double >( const std::vector<                 double > & v );
template            long double Sequential::max<            long double >( const std::vector<            long double > & v );
//template          unsigned char Sequential::max<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Sequential::max<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Sequential::max<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Sequential::max<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Sequential::max< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Sequential::max<                   char >( const std::vector<                   char > & v );
template                  short Sequential::max<                  short >( const std::vector<                  short > & v );
template                    int Sequential::max<                    int >( const std::vector<                    int > & v );
template               long int Sequential::max<               long int >( const std::vector<               long int > & v );
template          long long int Sequential::max<          long long int >( const std::vector<          long long int > & v );

template                  float Sequential::maxMag<                  float >( const std::vector<                  float > & v );
template                 double Sequential::maxMag<                 double >( const std::vector<                 double > & v );
template            long double Sequential::maxMag<            long double >( const std::vector<            long double > & v );
//template          unsigned char Sequential::maxMag<          unsigned char >( const std::vector<          unsigned char > & v );
//template         unsigned short Sequential::maxMag<         unsigned short >( const std::vector<         unsigned short > & v );
//template           unsigned int Sequential::maxMag<           unsigned int >( const std::vector<           unsigned int > & v );
//template      unsigned long int Sequential::maxMag<      unsigned long int >( const std::vector<      unsigned long int > & v );
//template unsigned long long int Sequential::maxMag< unsigned long long int >( const std::vector< unsigned long long int > & v );
template                   char Sequential::maxMag<                   char >( const std::vector<                   char > & v );
template                  short Sequential::maxMag<                  short >( const std::vector<                  short > & v );
template                    int Sequential::maxMag<                    int >( const std::vector<                    int > & v );
template               long int Sequential::maxMag<               long int >( const std::vector<               long int > & v );
template          long long int Sequential::maxMag<          long long int >( const std::vector<          long long int > & v );

}
