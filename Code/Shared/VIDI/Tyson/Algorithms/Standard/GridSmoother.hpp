#ifndef GRIDSMOOTHER_HPP
#define GRIDSMOOTHER_HPP

#include <cstdint>
#include <vector>
#include <unordered_set>

namespace TN
{

inline float aggregateSum(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ];
        totalCount += 1.0;
    }
    return sum / totalCount;
}

inline float aggregateMin(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    float mn = std::numeric_limits<float>::max();
    if( indices.size() > 0 )
    {
        for( const auto & i : indices )
        {
            mn = std::min( mn, values[ i ] );
        }
    }
    return mn;
}

inline float aggregateMax(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    float mx = -std::numeric_limits<float>::max();
    if( indices.size() > 0 )
    {
        for( const auto & i : indices )
        {
            mx = std::max( mx, values[ i ] );
        }
    }
    return mx;
}
inline float aggregateVariance(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * ( counts[ i ] - 1 );
        totalCount += counts[ i ];
    }
    return sum / totalCount;
}

inline float aggregateRMS(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * values[ i ] * counts[ i ];
        totalCount += counts[ i ];
    }
    return std::sqrt( sum ) / totalCount;
}

inline float aggregateMean(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * counts[ i ];
        totalCount += counts[ i ];
    }
    return sum / totalCount;
}

inline float aggregateCount(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        totalCount += counts[ i ];
    }
    return totalCount / indices.size();
}

template < typename F >
inline void smooth(
    const std::vector< float > & values,
    const std::vector< float > & counts,
    const std::vector< int64_t > & neighborhoods,
    const std::vector< int64_t > & neighborhoodSums,
    std::vector< float > & smoothed,
    int radius,
    F f )
{
    const int64_t SZ = values.size();
    smoothed = values;

    #pragma omp parallel for
    for( int64_t i = 0; i < SZ; ++i )
    {
        std::unordered_set< int64_t > accounted   = {i};
        std::unordered_set< int64_t > toVisit     = {i};
        std::unordered_set< int64_t > visited;

        for( int k = 0; k < radius; ++k )
        {
            for( auto & node : toVisit )
            {
                accounted.insert( node );
                visited.insert(   node );

                for( int j = ( node == 0 ? 0 : neighborhoodSums[ node - 1 ] );
                        j < neighborhoodSums[ node ];
                        ++j )
                {
                    accounted.insert( neighborhoods[ j ] );
                }
            }

            toVisit.clear();
            for( auto & node : accounted )
            {
                if( visited.count( node ) == 0 )
                {
                    toVisit.insert( node );
                }
            }
        }

        std::unordered_set< int64_t > toAggregate;
        for( auto & ac : accounted )
        {
            if( counts[ ac ] > 0 )
            {
                toAggregate.insert( ac );
            }
        }

        if( toAggregate.size() > 0 )
        {
            smoothed[ i ] = f( toAggregate, values, counts );
        }
    }
}
}

#endif // GRIDSMOOTHER_HPP
