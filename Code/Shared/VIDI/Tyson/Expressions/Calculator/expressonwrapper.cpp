

#include "Expressions/Calculator/Expression.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"

namespace TN
{

ExpressionWrapper::ExpressionWrapper()
{
    m_parser.reset( new ExpressionParser< double > );
}

const ExpressionParser< double > & ExpressionWrapper::parser() const
{
    return *m_parser;
}

}
