

#ifndef TN_EXPRESSION
#define TN_EXPRESSION

#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Algorithms/Standard/Util.hpp"

// #include "mpreal.h"
// #include "exprtk_mpfr_adaptor.hpp"
#include "exprtk.hpp"

#include <QDebug>

#include <fstream>
#include <string>

#include <string>
#include <unordered_map>

namespace TN
{

template < typename ProcessingFloatType >
struct ExpressionParser
{
    std::string expandExpression(
        const std::string & expression_str,
        const std::map< std::string, BaseConstant    > & baseConstants,
        const std::map< std::string, CustomConstant  > & customConstants,
        const std::map< std::string, BaseVariable    > & baseVariables,
        const std::map< std::string, DerivedVariable > & derivedVariables ) const
    {
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        std::string result;

        // for (std::size_t i = 0; i < generator.size(); ++i)
        // {
        //     if( customConstants.find( generator[i].value ) != customConstants.end() )
        //     {
        //         result += "(" + to_string_with_precision( evaluateConstExpression( customConstants.find( generator[ i ].value )->second.expression(), baseConstants, customConstants ), 20 ) + ")";
        //     }
        //     else if( baseConstants.find( generator[i].value ) != baseConstants.end() )
        //     {
        //         result += "(" + to_string_with_precision( baseConstants.find( generator[i].value )->second.value(), 20 ) + ")";
        //     }
        //     else if( baseVariables.find( generator[i].value ) != baseVariables.end() )
        //     {
        //         result += "(" + generator[i].value + ")";
        //     }
        //     else if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
        //     {
        //         const std::string dvExpr = derivedVariables.find( generator[i].value )->second.expression();
        //         result += "(" + expandExpression( dvExpr, baseConstants, customConstants, baseVariables, derivedVariables ) + ")";
        //     }
        //     else if( validateNumber( generator[ i ].value ) )
        //     {
        //         result += "(" + generator[ i ].value + ")";
        //     }
        //     else
        //     {
        //         result += generator[ i ].value;
        //     }
        // }
        return result;
    }

    bool validate(
        const std::string & expression_str,
        const std::map< std::string, BaseVariable    > & baseVariables,
        const std::map< std::string, DerivedVariable > & derivedVariables,
        const std::map< std::string, CustomConstant  > & customConstants,
        const std::map< std::string, BaseConstant    > & baseConstants ) const
    {
        // if( expression_str == "" )
        // {
        //     return false;
        // }

        // typedef exprtk::symbol_table<double> symbol_table_t;
        // typedef exprtk::expression<double> expression_t;
        // typedef exprtk::parser<double> parser_t;
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // std::vector< std::string > variables;
        // std::vector< double > variableValues;
        // std::vector< std::string > constants;

        // std::vector< std::string > expressions;
        // std::vector< double > expressionValues;

        // for (std::size_t i = 0; i < generator.size(); ++i)
        // {
        //     if( customConstants.find( generator[i].value ) != customConstants.end() )
        //     {
        //         constants.push_back( generator[i].value );
        //     }
        //     if( baseConstants.find( generator[i].value ) != baseConstants.end() )
        //     {
        //         constants.push_back( generator[i].value );
        //     }
        //     if( baseVariables.find( generator[i].value ) != baseVariables.end() )
        //     {
        //         variables.push_back( generator[i].value );
        //         variableValues.push_back( 1.0 );
        //     }
        //     if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
        //     {
        //         expressions.push_back( generator[i].value );
        //         expressionValues.push_back( 1.0 );
        //     }
        // }

        // symbol_table_t symbol_table;
        // symbol_table.add_constants();

        // for( size_t i = 0, end = variables.size(); i < end; ++i )
        // {
        //     symbol_table.add_variable( variables[ i ], variableValues[ i ] );
        // }

        // for( size_t i = 0, end = expressions.size(); i < end; ++i )
        // {
        //     symbol_table.add_variable( expressions[ i ], expressionValues[ i ] );
        // }

        // for( const auto & str : constants )
        // {
        //     symbol_table.add_constant( str, 1.0 );
        // }

        // expression_t expression;
        // expression.register_symbol_table(symbol_table);

        // parser_t parser;

        // if (!parser.compile( expression_str, expression))
        // {
        //     //           printf("Error: %s\tExpression: %s\n",
        //     //                  parser.error().c_str(),
        //     //                  expression_str.c_str());

        //     //           for (std::size_t i = 0; i < parser.error_count(); ++i)
        //     //           {
        //     //              error_t error = parser.get_error(i);
        //     //              printf("Error: %02d Position: %02d Type: [%s] Msg: %s Expr: %s\n",
        //     //                     static_cast<int>(i),
        //     //                     static_cast<int>(error.token.position),
        //     //                     exprtk::parser_error::to_str(error.mode).c_str(),
        //     //                     error.diagnostic.c_str(),
        //     //                     expression_str.c_str());
        //     //           }

        //     return false;
        // }

        return true;
    }

    bool isVariant(
        const std::string & expression_str,
        const std::map< std::string, BaseVariable    > & baseVariables,
        const std::map< std::string, DerivedVariable > & derivedVariables ) const
    {
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // for (std::size_t i = 0; i < generator.size(); ++i)
        // {
        //     if( baseVariables.find( generator[i].value ) != baseVariables.end() )
        //     {
        //         return true;
        //     }
        //     if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
        //     {
        //         return true;
        //     }
        // }
        return false;
    }

    bool validateNumber( const std::string & number ) const
    {
        bool isNumber;
        double value = QString( number.c_str() ).toDouble( &isNumber );
        if( ! isNumber )
        {
            return false;
        }
        if( std::isinf( value ) || std::isnan( value ) )
        {
            return false;
        }
        return true;
    }

    bool validateConstExpression(
        const std::string & expression_str,
        const std::map< std::string, BaseConstant    > & baseConstants,
        const std::map< std::string, CustomConstant  > & customConstants ) const
    {
        // typedef exprtk::symbol_table<double> symbol_table_t;
        // typedef exprtk::expression<double> expression_t;
        // typedef exprtk::parser<double> parser_t;
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // symbol_table_t symbol_table;
        // symbol_table.add_constants();

        // std::vector< std::string > identifiers = getExpressionIdentifiers( expression_str );

        // for( auto & id : identifiers )
        // {
        //     bool identifierValid = false;

        //     if( baseConstants.find( id ) != baseConstants.end() )
        //     {
        //         const BaseConstant & bc = baseConstants.find( id )->second;
        //         symbol_table.add_constant( bc.name(), bc.value() );
        //         identifierValid = true;
        //     }
        //     else if ( customConstants.find( id ) != customConstants.end() )
        //     {
        //         const CustomConstant & cc = customConstants.find( id )->second;
        //         symbol_table.add_constant( cc.name(), evaluateConstExpression( cc.expression(), baseConstants, customConstants ) );
        //         identifierValid = true;
        //     }

        //     if( identifierValid == false )
        //     {
        //         return false;
        //     }
        // }

        // expression_t expression;
        // expression.register_symbol_table(symbol_table);

        // parser_t parser;
        // if( ! parser.compile( expression_str, expression ) )
        // {
        //     return false;
        // }

        return true;
    }


    double evaluateConstExpression(
        const std::string & expression_str,
        const std::map< std::string, BaseConstant    > & baseConstants,
        const std::map< std::string, CustomConstant  > & customConstants ) const
    {
        // typedef exprtk::symbol_table< long double > symbol_table_t;
        // typedef exprtk::expression< long double > expression_t;
        // typedef exprtk::parser< long double > parser_t;
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // symbol_table_t symbol_table;
        // symbol_table.add_constants();

        // std::vector< std::string > identifiers = getExpressionIdentifiers( expression_str );

        // for( auto & id : identifiers )
        // {
        //     if( baseConstants.find( id ) != baseConstants.end() )
        //     {
        //         const BaseConstant & bc = baseConstants.find( id )->second;
        //         symbol_table.add_constant( bc.name(), bc.value() );
        //     }
        //     else if ( customConstants.find( id ) != customConstants.end() )
        //     {
        //         const CustomConstant & cc = customConstants.find( id )->second;
        //         symbol_table.add_constant( cc.name(), evaluateConstExpression( cc.expression(), baseConstants, customConstants ) );
        //     }
        // }

        // expression_t expression;
        // expression.register_symbol_table( symbol_table );

        // parser_t parser;
        // parser.compile( expression_str, expression );

        return 1; //expression.value();
    }

    template < typename FloatType >
    bool updateStaticFilter(
        const std::string & expression_str,
        const std::map< std::string, FloatType * > & variables,
        std::vector< std::uint8_t > & result ) const
    {
        // typedef exprtk::symbol_table< ProcessingFloatType > symbol_table_t;
        // typedef exprtk::expression< ProcessingFloatType > expression_t;
        // typedef exprtk::parser< ProcessingFloatType > parser_t;
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // symbol_table_t symbol_table;

        // std::unordered_map< std::string, ProcessingFloatType > variableValueHolders;

        // for( auto & v : variables )
        // {
        //     variableValueHolders.insert( { v.first, 0.0 } );
        // }
        // for( auto & v : variableValueHolders )
        // {
        //     symbol_table.add_variable( v.first, v.second );
        // }
        // expression_t expression;
        // expression.register_symbol_table( symbol_table );
        // parser_t parser;

        // if ( ! parser.compile( expression_str, expression ) )
        // {
        //     return false;
        // }
        // for( unsigned int idx = 0, end = result.size(); idx < end; ++idx )
        // {
        //     for( auto & v : variables )
        //     {
        //         variableValueHolders.find( v.first )->second
        //             = static_cast< ProcessingFloatType >( v.second[ idx ] );
        //     }

        //     result[ idx ] = result[ idx ] || static_cast< std::uint8_t >( expression.value() );
        // }

        return true;
    }

    std::vector< std::string > getExpressionIdentifiers( const std::string & expression_str ) const
    {
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        std::vector< std::string > tokens;

        // for (std::size_t i = 0; i < generator.size(); ++i)
        // {
        //     if(  ( operators.find( generator[ i ].value ) == operators.end() )
        //             && (  keyWords.find( generator[ i ].value ) ==  keyWords.end() )
        //             && ( illegalFirst.find( generator[ i ].value[ 0 ] ) == illegalFirst.end() ) )
        //         tokens.push_back( generator[ i ].value );
        // }

        return tokens;
    }

    template< typename TargetFloatType >
    bool evaluateExpression(
        const std::string & expression_str,
        const std::map< std::string, TargetFloatType * > & variables,
        std::vector< TargetFloatType > & result ) const
    {
        // typedef exprtk::symbol_table< ProcessingFloatType > symbol_table_t;
        // typedef exprtk::expression< ProcessingFloatType > expression_t;
        // typedef exprtk::parser< ProcessingFloatType > parser_t;
        // typedef exprtk::lexer::generator generator_t;

        // generator_t generator;
        // generator.process( expression_str );

        // symbol_table_t symbol_table;

        // std::unordered_map< std::string, ProcessingFloatType > variableValueHolders;

        // for( auto & v : variables )
        // {
        //     variableValueHolders.insert( { v.first, 0.0 } );
        // }
        // for( auto & v : variableValueHolders )
        // {
        //     symbol_table.add_variable( v.first, v.second );
        // }

        // expression_t expression;
        // expression.register_symbol_table( symbol_table );

        // parser_t parser;

        // if ( ! parser.compile( expression_str, expression ) )
        // {
        //     return false;
        // }

        // for( unsigned idx = 0, end = result.size(); idx < end; ++idx )
        // {
        //     for( auto & v : variables )
        //     {
        //         variableValueHolders.find( v.first )->second
        //             = static_cast< ProcessingFloatType >( v.second[ idx ] );
        //     }
        //     result[ idx ] = static_cast< TargetFloatType >( expression.value() );
        // }

        return true;
    }
};

}

#endif // TN_EXPRESSION

