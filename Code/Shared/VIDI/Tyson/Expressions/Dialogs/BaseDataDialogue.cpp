

#include "Types/Vec.hpp"
#include "Algorithms/Standard/Util.hpp"

#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Data/Definitions/Attributes.hpp"

#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN
{

BaseDataDialogue::~BaseDataDialogue() {}

void BaseDataDialogue::selectedItemDescriptionChanged()
{
    QString var = selectedItemDescriptionLB->text().split(" ").at(0);

    {
        auto it = baseVariables.find( var.toStdString() );
        if( it != baseVariables.end() )
        {
            it->second.description( selectedItemDescription->toPlainText().toStdString() );
            return;
        }
    }
    {
        auto it = derivedVariables.find( var.toStdString() );
        if( it != derivedVariables.end() )
        {
            it->second.description( selectedItemDescription->toPlainText().toStdString() );
            return;
        }
    }
    {
        auto it = baseConstants.find( var.toStdString() );
        if( it != baseConstants.end() )
        {
            it->second.description( selectedItemDescription->toPlainText().toStdString() );
            return;
        }
    }
    {
        auto it = customConstants.find( var.toStdString() );
        if( it != customConstants.end() )
        {
            it->second.description( selectedItemDescription->toPlainText().toStdString() );
            return;
        }
    }
}

void BaseDataDialogue::attributeFocused(QListWidgetItem *lwi )
{
    QString text = lwi->text();
    auto it = baseVariables.find( text.toStdString() );
    if( it != baseVariables.end() )
    {
        std::string desc = it->second.description();
        selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
        selectedItemDescription->setText( desc.c_str() );
    }
}
void BaseDataDialogue::constantFocused(QListWidgetItem *lwi)
{
    QString text = lwi->text();
    QString first = text.split(" ").at(0);
    auto it = baseConstants.find( first.toStdString() );
    if( it != baseConstants.end() )
    {
        std::string desc = it->second.description();
        selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
        selectedItemDescription->setText( desc.c_str() );
    }
}
void BaseDataDialogue::userConstantFocused(QListWidgetItem *lwi)
{
    QString text = lwi->text();
    QString first = text.split(" ").at(0);
    auto it = customConstants.find( first.toStdString() );
    if( it != customConstants.end() )
    {
        std::string desc = it->second.description();
        selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
        selectedItemDescription->setText( desc.c_str() );
    }
}
void BaseDataDialogue::derivedVariableFocused(QListWidgetItem *lwi)
{
    QString text = lwi->text();
    QString first = text.split(" ").at(0);
    auto it = derivedVariables.find( first.toStdString() );
    if( it != derivedVariables.end() )
    {
        std::string desc = it->second.description();
        selectedItemDescriptionLB->setText( ( it->first + " description" ).c_str() );
        selectedItemDescription->setText( desc.c_str() );
    }
}

bool BaseDataDialogue::validateNumber( QString number )
{
    bool isNumber;
    double value = number.toDouble( &isNumber );
    if( ! isNumber )
    {
        return false;
    }
    if( std::isinf( value ) || std::isnan( value ) )
    {
        return false;
    }
    return true;
}

bool BaseDataDialogue::validateInteger( QString number )
{
    std::string nm = number.toStdString();

    if( ! validateNumber( number ) )
    {
        return false;
    }
    if( nm.find_first_of( "123456789" ) != 0  )
    {
        return false;
    }
    if( nm.find_first_not_of( "1234567890Ee+" ) != nm.npos  )
    {
        return false;
    }
    return true;
}

bool BaseDataDialogue::validateName( QString name )
{
    // make sure it's not in use, and no spaces, tabs, or newlines please!

    std::string _name = name.toStdString();

    if( name == "" )
    {
        return false;
    }

    if( TN::keyWords.find( _name ) != TN::keyWords.end() )
    {
        return false;
    }

    if( _name.find_first_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_" ) != 0  )
    {
        return false;
    }

    if( _name.find_first_not_of( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890" ) != name.toStdString().npos  )
    {
        return false;
    }

    if( baseVariables.find( _name ) != baseVariables.end() )
    {
        return false;
    }

    if( derivedVariables.find( _name ) != derivedVariables.end() )
    {
        return false;
    }

    if( baseConstants.find( _name ) != baseConstants.end() )
    {
        return false;
    }

    if( customConstants.find( _name ) != customConstants.end() )
    {
        return false;
    }

    return true;
}

bool BaseDataDialogue::validateExpression( QString expression_qstr )
{
    return m_expressionCalculator->parser().validate(
               expression_qstr.toStdString(),
               baseVariables,
               derivedVariables,
               customConstants,
               baseConstants );
}

bool BaseDataDialogue::validateConstExpression( QString expression_qstr )
{
    return m_expressionCalculator->parser().validateConstExpression( expression_qstr.toStdString(), baseConstants, customConstants );
}

bool BaseDataDialogue::validateIsVariant( QString expression_qstr )
{
    return m_expressionCalculator->parser().isVariant( expression_qstr.toStdString(), baseVariables, derivedVariables );
}

void BaseDataDialogue::nameLineEditChanged( QString text )
{
    QLineEdit * edit = (QLineEdit*) QObject::sender();
    std::string cl( edit == deriveNameEdit ? "purple" : edit == defineNameEdit ? "rgb(200,90,0)" : "black" );

    if( validateName( text ) )
    {
        edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
    }
    else
    {
        edit->setStyleSheet( "QLineEdit { color : gray; }" );
    }
}

void BaseDataDialogue::constExpressionLineEditChanged( QString text )
{
    QLineEdit * edit = (QLineEdit*) QObject::sender();

    std::string cl = "purple";

    if( validateConstExpression( text ) )
    {
        edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
    }
    else
    {
        edit->setStyleSheet( "QLineEdit { color : gray; }" );
    }
}

void BaseDataDialogue::expressionLineEditChanged( QString text )
{
    QLineEdit * edit = (QLineEdit*) QObject::sender();

    std::string cl( "purple" );

    if( validateExpression( text ) )
    {
        edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
    }
    else
    {
        edit->setStyleSheet( "QLineEdit { color : gray; }" );
    }
}

void BaseDataDialogue::valueLineEditChanged( QString text )
{
    QLineEdit * edit = (QLineEdit*) QObject::sender();

    std::string cl( edit == defineEdit ? "rgb(20,20,20)" : "black" );

    if( validateNumber( text ) )
    {
        edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
    }
    else
    {
        edit->setStyleSheet( "QLineEdit { color : gray; }" );
    }
}

void BaseDataDialogue::valueEditChanged( QString text )
{
    QLineEdit * edit = (QLineEdit*) QObject::sender();

    std::string cl( "rgb(20,20,20)" );

    if( validateNumber( text ) )
    {
        edit->setStyleSheet( ("QLineEdit { color :" + cl + "; }" ).c_str() );
    }
    else
    {
        edit->setStyleSheet( "QLineEdit { color : gray; }" );
    }
}

void BaseDataDialogue::attemptDefineConstant( )
{
    if( ! validateName( defineNameEdit->text() ) )
    {
        return;
    }

    if( ! validateConstExpression( defineEdit->text() ) )
    {
        return;
    }

    CustomConstant cc;
    cc.name( defineNameEdit->text().toStdString() );
    cc.expression( defineEdit->text().toStdString() );

    customConstants.insert( { cc.name(), cc } );

    std::string _nm = cc.name() + " = " + cc.expression();
    if( ! m_expressionCalculator->parser().validateNumber( cc.expression() ) )
    {
        _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( cc.expression(), baseConstants, customConstants ), 10 );
    }

    userConstantList->addItem( _nm.c_str() );
    defineNameEdit->setStyleSheet( "QLineEdit { color : gray; }" );

    emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants  );
}

void BaseDataDialogue::attemptDeriveVariable( )
{
    if( ! validateName( deriveNameEdit->text() ) )
    {
        return;
    }

    if( ! validateExpression( deriveEdit->text() ) )
    {
        return;
    }

    if( ! validateIsVariant( deriveEdit->text() ) )
    {
        return;
    }

    DerivedVariable dv;
    dv.name( deriveNameEdit->text().toStdString() );
    dv.expression( deriveEdit->text().toStdString() );
    derivedVariables.insert( { dv.name(), dv } );
    derivedAttributeList->addItem( ( dv.name() + " = " + dv.expression() ).c_str() );
    deriveNameEdit->setStyleSheet( "QLineEdit { color : gray; }" );

    emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants );
}

void BaseDataDialogue::saveDataDescription( )
{
    const QString & text = dataSetDescription->toPlainText();
    emit updateUserDataDescription( text.toStdString() );
}

BaseDataDialogue::BaseDataDialogue( std::string _key, QWindow *parentWindow ) : QDialog( 0 ), m_parentWindow( parentWindow ), m_key( _key ), m_status( "undefined" )
{
    m_expressionCalculator.reset( new ExpressionWrapper );

    setStyleSheet("  QWidget { background-color: rgb( 240, 240, 240 ); } QLineEdit { background-color: white; } QComboBox { background-color: white; } QListWidget { background-color: white; }");

    ///////////////////////////////////////////////////////////////////////////

    // Type / Calculation / Expressions / Validation

    errorMessageLabel = new QLabel;
    errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

    //////////////////////////////////////////////////////////////////////////////

    // Info Widget // note should make this one a module, as it can be used in a variety of contexts

    attributeList        = new QListWidget;
    derivedAttributeList = new QListWidget;
    constantList         = new QListWidget;
    userConstantList     = new QListWidget;

    userConstantList->setStyleSheet( "QListWidget { color : purple; }" );
    attributeList->setStyleSheet( "QListWidget { background-color : white; }" );
    derivedAttributeList->setStyleSheet( "QListWidget { background-color : white; color : purple; }" );
    constantList->setStyleSheet( "QListWidget { background-color : white; }" );

    connect( attributeList,        SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( attributeFocused(QListWidgetItem*) ) );
    connect( constantList,         SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( constantFocused(QListWidgetItem*) ) );
    connect( userConstantList,     SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( userConstantFocused(QListWidgetItem*) ) );
    connect( derivedAttributeList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT( derivedVariableFocused(QListWidgetItem*) ) );

    //dataSetInfo             = new QTextEdit;
    dataSetDescription      = new QTextEdit;
    dataSetDescription->setText( "An XGC simulation of the ITER device. The data has been provided by CS Chang's group at Princeton Plasma Physics Laboratory." );

    //selectedItemInfo        = new QTextEdit;
    selectedItemDescription = new QTextEdit;

    connect( selectedItemDescription, SIGNAL(textChanged()), this, SLOT( selectedItemDescriptionChanged()) );

    //dataSetInfo->setMinimumWidth( 400 );
    dataSetDescription->setMinimumWidth( 500 );
    //selectedItemInfo->setMinimumWidth( 400 );
    selectedItemDescription->setMinimumWidth( 500 );

    //dataSetInfo->setStyleSheet( "QTextEdit { background-color : white; }" );
    dataSetDescription->setStyleSheet( "QTextEdit { background-color : white; }" );
    //selectedItemInfo->setStyleSheet( "QTextEdit { background-color : white; }" );
    selectedItemDescription->setStyleSheet( "QTextEdit { background-color : white; }" );

    infoWidget = new QWidget;

    mainInfoLayout      = new QVBoxLayout;
    dataSetLayout       = new QHBoxLayout;
    attributeInfoLayout = new QHBoxLayout;
    descriptionLayuout  = new QHBoxLayout;

    attributeListLT           = new QVBoxLayout;
    derivedAttributeListLT    = new QVBoxLayout;
    constantListLT            = new QVBoxLayout;
    dataSetInfoLT             = new QVBoxLayout;
    dataSetDescriptionLT      = new QVBoxLayout;
    selectedItemInfoLT        = new QVBoxLayout;
    selectedItemDescriptionLT = new QVBoxLayout;

    attributeListLB           = new QLabel( "Base Variables"    );
    derivedAttributeListLB    = new QLabel( "Derived Variables" );
    constantListLB            = new QLabel( "Stock Constants"   );
    QLabel * userConstantsLB  = new QLabel( "Custom Constants"  );

    //dataSetInfoLB             = new QLabel( "Dataset Info" );
    dataSetDescriptionLB      = new QLabel( "Dataset Description" );
    //selectedItemInfoLB        = new QLabel( "Selected Variable Name Here" );
    selectedItemDescriptionLB = new QLabel( "Selected Item Description" );

    QVBoxLayout * addDerivedButtonLayout  = new QVBoxLayout;
    QVBoxLayout * addConstantButtonLayout = new QVBoxLayout;

    addDerivedButton  = new QPushButton( "Derive" );
    addConstantButton = new QPushButton( "Define" );

    connect( addConstantButton, SIGNAL( clicked(bool) ), this, SLOT( attemptDefineConstant() ) );
    connect( addDerivedButton,  SIGNAL( clicked(bool) ), this, SLOT( attemptDeriveVariable() ) );

    deriveEdit  = new QLineEdit;
    defineEdit  = new QLineEdit;

    deriveLabel = new QLabel( "=" );
    defineLabel = new QLabel( "=" );

    deriveNameEdit = new QLineEdit;
    defineNameEdit = new QLineEdit;

    connect( deriveNameEdit, SIGNAL(textChanged(QString)), this, SLOT(nameLineEditChanged(QString)) );
    connect( deriveEdit, SIGNAL(textChanged(QString)), this, SLOT(expressionLineEditChanged(QString)) );
    connect( defineNameEdit, SIGNAL(textChanged(QString)), this, SLOT(nameLineEditChanged(QString)) );
    connect( defineEdit, SIGNAL(textChanged(QString)), this, SLOT(constExpressionLineEditChanged(QString)) );

    QHBoxLayout * derivedLayout1 = new QHBoxLayout;
    QHBoxLayout * derivedLayout2 = new QHBoxLayout;

    derivedLayout1->addWidget( addDerivedButton );
    derivedLayout1->addWidget( deriveNameEdit );
    derivedLayout2->addWidget( deriveLabel );
    derivedLayout2->addWidget( deriveEdit );

    addDerivedButtonLayout->addLayout( derivedLayout1 );
    addDerivedButtonLayout->addLayout( derivedLayout2 );

    QHBoxLayout * defineLayout1 = new QHBoxLayout;
    QHBoxLayout * defineLayout2 = new QHBoxLayout;

    defineLayout1->addWidget( addConstantButton );
    defineLayout1->addWidget( defineNameEdit );
    defineLayout2->addWidget( defineLabel );
    defineLayout2->addWidget( defineEdit );

    addConstantButtonLayout->addLayout( defineLayout1 );
    addConstantButtonLayout->addLayout( defineLayout2 );

    attributeListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    derivedAttributeListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    //dataSetInfoLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    constantListLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    dataSetDescriptionLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    selectedItemDescriptionLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    //selectedItemInfoLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );
    userConstantsLB->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt; }" );

    attributeListLT->addWidget( attributeListLB );
    attributeListLT->addWidget( attributeList );
    attributeListLT->addWidget( derivedAttributeListLB );
    attributeListLT->addWidget( derivedAttributeList );
    attributeListLT->addLayout( addDerivedButtonLayout );

    constantListLT->addWidget( constantListLB );
    constantListLT->addWidget( constantList );
    constantListLT->addWidget( userConstantsLB );
    constantListLT->addWidget( userConstantList );
    constantListLT->addLayout( addConstantButtonLayout );

    //dataSetInfoLT->addWidget( dataSetInfoLB );
    dataSetDescriptionLT->addWidget( dataSetDescriptionLB );
    //selectedItemInfoLT->addWidget( selectedItemInfoLB );
    selectedItemDescriptionLT->addWidget( selectedItemDescriptionLB );

    //dataSetInfoLT->addWidget( dataSetInfo );
    dataSetDescriptionLT->addWidget( dataSetDescription );
    //selectedItemInfoLT->addWidget( selectedItemInfo );
    selectedItemDescriptionLT->addWidget( selectedItemDescription );

    dataSetLayout->addLayout( dataSetInfoLT );
    dataSetLayout->addLayout( dataSetDescriptionLT );
    attributeInfoLayout->addLayout( attributeListLT );

    QFrame *l1kjudfdfhsdkd = new QFrame;
    l1kjudfdfhsdkd->setFrameShape( QFrame::VLine );
    l1kjudfdfhsdkd->setFrameShadow( QFrame::Sunken );
    l1kjudfdfhsdkd->setStyleSheet( "QFrame { background-color : white; }" );
    attributeInfoLayout->addWidget( l1kjudfdfhsdkd );
    attributeInfoLayout->addLayout( constantListLT );

    descriptionLayuout->addLayout( selectedItemInfoLT );
    descriptionLayuout->addLayout( selectedItemDescriptionLT );

    QFrame *l1kjuhsdkd = new QFrame;
    l1kjuhsdkd->setFrameShape( QFrame::HLine );
    l1kjuhsdkd->setFrameShadow( QFrame::Sunken );
    l1kjuhsdkd->setStyleSheet( "QFrame { background-color : white; }" );

    QFrame *l1kjuhssddkd = new QFrame;
    l1kjuhssddkd->setFrameShape( QFrame::HLine );
    l1kjuhssddkd->setFrameShadow( QFrame::Sunken );
    l1kjuhssddkd->setStyleSheet( "QFrame { background-color : white; }" );

    mainInfoLayout->addLayout( dataSetLayout );
    mainInfoLayout->addWidget( l1kjuhsdkd );
    mainInfoLayout->addLayout( attributeInfoLayout );
    mainInfoLayout->addWidget( l1kjuhssddkd );
    mainInfoLayout->addLayout( descriptionLayuout );

    infoWidget->setLayout( mainInfoLayout );

    connect( dataSetDescription, SIGNAL( textChanged() ), this, SLOT(saveDataDescription()));

    //////////////////////////////////////////////////////////////////////////////

    // main layout

    QHBoxLayout * higherLevelLayout = new QHBoxLayout;
    m_mainLayout        = new QVBoxLayout;

    higherLevelLayout->addWidget( infoWidget );
    higherLevelLayout->addLayout( m_mainLayout );

    setLayout( higherLevelLayout );
}
}

