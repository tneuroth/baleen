


#ifndef BASEDATADIALOGUE_HPP
#define BASEDATADIALOGUE_HPP

#include "Data/Definitions/Attributes.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"

#include <QDialog>
#include <string>
#include <vector>
#include <cmath>

class QMessageBox;
class QFormLayout;
class QLabel;
class QLineEdit;
class QDialogButtonBox;
class QCheckBox;
class QPushButton;
class QComboBox;
class QListWidget;
class QTextEdit;
class QListWidgetItem;
class QVBoxLayout;
class QHBoxLayout;

namespace TN
{

class ExpressionWrapper;

class BaseDataDialogue : public QDialog
{
    Q_OBJECT

protected :

    std::unique_ptr< ExpressionWrapper > m_expressionCalculator;

    std::map< std::string, BaseVariable > baseVariables;
    std::map< std::string, DerivedVariable > derivedVariables;
    std::map< std::string, CustomConstant  > customConstants;
    std::map< std::string, BaseConstant   > baseConstants;

    QLabel *errorMessageLabel;

    QWindow * m_parentWindow;
    std::string m_key;
    std::string m_status;

    QVBoxLayout * m_mainLayout;

    // Data info Section

    QListWidget * attributeList;
    QListWidget * derivedAttributeList;
    QListWidget * constantList;
    QListWidget * userConstantList;

    //QTextEdit * dataSetInfo;
    QTextEdit * dataSetDescription;
    //QTextEdit * selectedItemInfo;
    QTextEdit * selectedItemDescription;

    QWidget * infoWidget;

    QVBoxLayout * mainInfoLayout;

    QHBoxLayout * dataSetLayout;
    QHBoxLayout * attributeInfoLayout;
    QHBoxLayout * descriptionLayuout;


    QVBoxLayout * attributeListLT;
    QVBoxLayout * derivedAttributeListLT;
    QVBoxLayout * constantListLT;

    QVBoxLayout * dataSetInfoLT;
    QVBoxLayout * dataSetDescriptionLT;
    QVBoxLayout * selectedItemInfoLT;
    QVBoxLayout * selectedItemDescriptionLT;

    QLabel * attributeListLB;
    QLabel * derivedAttributeListLB;
    QLabel * constantListLB;

    QLineEdit * deriveEdit;
    QLineEdit * defineEdit;
    QLabel * deriveLabel;
    QLabel * defineLabel;
    QLineEdit * deriveNameEdit;
    QLineEdit * defineNameEdit;

//    QLabel * dataSetInfoLB;
    QLabel * dataSetDescriptionLB;
//    QLabel * selectedItemInfoLB;
    QLabel * selectedItemDescriptionLB;

    QPushButton * addDerivedButton;
    QPushButton * addConstantButton;

    ////////////////////////////////////////////////////////////////////

public:

    virtual ~BaseDataDialogue();

signals:

    void updateUserDataDescription( const std::string & );

    void updateAttributes(
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> & );

protected slots:

    void saveDataDescription();
    void selectedItemDescriptionChanged();
    void attributeFocused(QListWidgetItem *lwi );
    void constantFocused(QListWidgetItem *lwi);
    void userConstantFocused(QListWidgetItem *lwi);
    void derivedVariableFocused(QListWidgetItem *lwi);
    bool validateNumber( QString number );
    bool validateInteger( QString number );
    bool validateName( QString name );
    bool validateExpression( QString expression_qstr );
    bool validateConstExpression( QString expression_qstr );
    bool validateIsVariant( QString expression_qstr );
    void nameLineEditChanged( QString text );
    void constExpressionLineEditChanged( QString text );
    void expressionLineEditChanged( QString text );
    void valueLineEditChanged( QString text );
    void valueEditChanged( QString text );

    virtual void attemptDefineConstant();
    virtual void attemptDeriveVariable();

public:

    BaseDataDialogue( std::string _key, QWindow *parentWindow );
};

}


#endif // BASEDATADIALOGUE_HPP
