

#ifndef GTSSINGLEIMPORTER_HPP
#define GTSSINGLEIMPORTER_HPP

#include "Data/Importers/DistributionDataImporter.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"

#include <fstream>
#include <memory>
#include <cstdint>
#include <vector>
#include <set>
#include <string>
#include <map>
#include <QFile>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The GTSImporter class
 */
class GTSSingle : public DistributionDataImporter< TargetFloatType >
{
    /** @brief The floating point type that the distribution data is represented by on disk */
    typedef double OrigionalFloatType;

    /** @brief The base path where the data files are located */
    std::string m_basePath;

    /** @brief Available Distribution Types */
    std::vector< std::string > m_distributionTypes;

    /** @brief Available Particle Types */
    std::vector< std::string > m_particleTypes;

    /** @brief Relative path to the Ion distribution Data */
    std::string m_ionPath;

    template< typename T >
    Vec2< double > getRange( const std::vector< T > & values )
    {
        Vec2< double > result( std::numeric_limits< TargetFloatType >::max(), -std::numeric_limits< TargetFloatType >::max()  );
        for( auto val : values )
        {
            result.a( std::min( result.a(), ( double ) val ) );
            result.b( std::max( result.b(), ( double ) val ) );
        }
        return result;
    }

public:

    /**
     * @brief init
     * @param dataSetMetaData
     * @return
     */
    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        m_distributionTypes =
        {
            "Particle Distribution",
            "Energy Distribution"
        };

        m_particleTypes = { "ions" };
        m_ionPath = "/PDF_data.dat";

        this->m_basicDataInfo = basicDataInfo;
        m_basePath = basicDataInfo.location;

        return true;
    }

    /**
     * @brief loadTemporallyStaticAttributes
     * @param perGridPointAttributes
     * @param gridPointSpatialAttributes
     * @param perParticleTypePerDistributionTypeParameters
     * @param perParticleTypeConstants
     * @param particleTypes
     * @param perParticleTypeDistributionTypes
     * @param simulationTimeSteps
     * @param realTimeValues
     */
    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics,
        std::vector< unsigned int > & triangulation,
        std::vector< int64_t > & neighborhoods,
        std::vector< int64_t > & neighborhoodOffsets,
        std::map< std::string, std::vector< TargetFloatType > > & perGridPointAttributes,
        std::map< std::string, Vec2< double > > & perGridPointAttributeRanges,
        std::vector< std::string > & gridPointSpatialAttributes,
        std::map< std::string, std::map< std::string, PredefinedHistogram > > & perParticleTypePerDistributionTypeParameters,
        std::map< std::string, std::map< std::string, double > > & perParticleTypeConstants,
        std::set< std::string > & particleTypes,
        std::map< std::string, std::set< std::string > > & perParticleTypeDistributionTypes,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTimeValues,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::int32_t & numTimeSteps )
    {
        numTimeSteps = 1;

        particleTypes.insert( m_particleTypes.begin(), m_particleTypes.end() );

        for( auto & ptype : m_particleTypes )
        {
            perParticleTypeDistributionTypes.insert( std::pair< std::string, std::set< std::string > >(
                        ptype, std::set< std::string >( m_distributionTypes.begin(), m_distributionTypes.end() ) ) );
            distributions.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                                      ptype,
                                      std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >() ) );

            perParticleTypePerDistributionTypeParameters.insert( { ptype, std::map< std::string, PredefinedHistogram >()  } );

            for( auto & dtype : m_distributionTypes )
            {
                perParticleTypePerDistributionTypeParameters.find( ptype )->second.insert( { dtype, PredefinedHistogram() } );

                distributions.find( ptype )->second.insert( std::pair<  std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            dtype,
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > >(1) ) );
            }
        }

        gridPointSpatialAttributes.push_back( "r" );
        gridPointSpatialAttributes.push_back( "z" );

        std::string dir = "../datasets/GTS/Single/";

        int    npts = 3840;
        double deltaV;                // offset: 28
        double normalization_factor;  // offset: 32

        std::vector< double > swap( npts );

        ////////////////////////////////////////////////////

        std::ifstream in1( (dir + "PDF_grid_parameters.dat").c_str() );

        if( ! in1.is_open() )
        {
            //qDebug() << (dir + "PDF_grid_parameters.dat").c_str() << " not open";
        }

        in1.seekg( 28 );
        in1.read((char*)&deltaV, sizeof(double) );
        in1.read((char*)&normalization_factor, sizeof(double) );
        in1.seekg( 52 );
        in1.read((char*)swap.data(), sizeof(double)*npts );

        perGridPointAttributeRanges.insert( { "volume", getRange( swap ) } );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "volume", std::vector< TargetFloatType >( swap.begin(), swap.end() ) ) );

        //qDebug() << "added volume";

        in1.seekg( 68 + npts*8*3 );
        in1.read((char*)swap.data(), sizeof(double)*npts );

        perGridPointAttributeRanges.insert( { "r", getRange( swap ) } );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "r", std::vector< TargetFloatType >( swap.begin(), swap.end() ) ) );

        //qDebug() << "added r";

        in1.read((char*)swap.data(), sizeof(double)*npts );

        perGridPointAttributeRanges.insert( { "z", getRange( swap ) } );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "z", std::vector< TargetFloatType >( swap.begin(), swap.end() ) ) );

        //qDebug() << "aded z";

        in1.close();
        ///////////////////////////////////////////////////////////
        std::ifstream in2( ( dir + "PDF_data.dat" ).c_str() );

        if ( ! in2.is_open() )
        {
            //qDebug() <<  ( dir + "PDF_data.dat" ).c_str() << " not open";
        }

        swap.resize( npts*17*33 );

        in2.seekg( 20 );
        in2.read((char*)swap.data(), npts*17*33*sizeof(double) );

        //qDebug() << "read temp";

        distributions.find( "ions" )->second.find( "Particle Distribution" )->second[ 0 ].reset( new std::vector< TargetFloatType >( swap.begin(), swap.end() ) );

        //qDebug() << "read Particle Dist";

        in2.read((char*)swap.data(), npts*17*33*sizeof(double) );
        distributions.find( "ions" )->second.find( "Energy Distribution" )->second[ 0 ].reset( new std::vector< TargetFloatType >( swap.begin(), swap.end() ) );

        //qDebug() << "read energy dist";

        in2.close();

        for( const auto & ptype : m_particleTypes )
        {
            for( auto & dtype : m_distributionTypes )
            {
                PredefinedHistogram & pdh = perParticleTypePerDistributionTypeParameters.find( ptype )->second.find( dtype )->second;
                pdh.normalizationFactor = normalization_factor;
                pdh.valueRange = std::vector< Vec2< float > >( { Vec2< float > ( -3.2, 3.2 ), Vec2< float >( 0, 3.2 ) } );
                pdh.resolution = std::vector< std::int32_t >( { 2 * 16 + 1, 16 + 1 } );
//                   pdh.partitionSpace = { "r", "z" };
                pdh.binningSpace = { "vpara", "|vperp|" };
//                   pdh.partitionSpaceScaling = { 1.0, 1.0 };
                pdh.name = dtype;
            }
        }

        //qDebug() << "done loading data";
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param distributions
     */
    virtual void loadTimeStep(
        const std::string & ptype,
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions
    )
    {}

    virtual void loadStatistics(
        const std::string & ptype,
        std::int64_t tstep,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics  ) {}

    /**
     * @brief ~GTSImporter
     */
    virtual ~GTSSingle()
    {
    }
};

}

#endif // GTSSINGLEIMPORTER_HPP

