#ifndef FROMVOLUME_HPP
#define FROMVOLUME_HPP

#include "Types/Vec.hpp"
#include "../ParticleDataImporter.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"

#include <QDebug>

#include <fstream>
#include <map>
#include <string>

#include <vector>
#include <memory>
#include <limits>

#include <boost/filesystem.hpp>

namespace TN
{

template< typename TargetFloatType >

class VolumeDataImporter : public ParticleDataImporter< TargetFloatType >
{
    typedef float OrigionalFloatType;

    std::vector< double > m_swap;
    std::vector< std::string > m_simulationTimeSteps;

    static const std::int32_t DIM_X = 672;
    static const std::int32_t DIM_Y = 672;
    static const std::int32_t DIM_Z = 1;

    static const std::uint64_t N_VOX = DIM_X*DIM_Y*DIM_Z;

    std::map< std::string, int > m_variableOrder;
    std::map< std::string, double > m_variableScaleFactors;

public :

    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        this->m_initialized = true;

        qDebug() << "initialized importer";

        return true;
    }

    void loadTimeStepInfo(
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps )
    {
        using namespace boost;

        const std::string BASE_PATH = this->m_basicDataInfo.location + "/data";
        filesystem::path p( BASE_PATH );

        simulationTimeSteps.clear();
        realTime.clear();
        m_simulationTimeSteps.clear();

        if( ! filesystem::is_directory( p ) )
        {
            std::cerr << p << " is not a directory";
        }
        else
        {
            std::cerr << "going through directory " << p.c_str();

            filesystem::directory_iterator end_itr;
            for ( filesystem::directory_iterator itr( p ); itr != end_itr; ++itr )
            {
                if ( is_regular_file( itr->path() ) )
                {
                    std::string current_file = itr->path().filename().string();
                    std::stringstream sstr( current_file );
                    std::string a,b,c;

                    std::getline( sstr, a, '.' );
                    std::getline( sstr, b, '.' );
                    std::getline( sstr, c, '.' );

                    m_simulationTimeSteps.push_back( b + "." + c );
                    realTime.push_back( std::stod( b + "." + c ) );
                }
            }
        }

        TN::Parallel::sortTogether( realTime, m_simulationTimeSteps );

        for( int i = 0; i < m_simulationTimeSteps.size(); ++i )
        {
            simulationTimeSteps.push_back( i );
            qDebug() << realTime[ i ] << ", " << m_simulationTimeSteps[ i ].c_str();
        }

        numTimeSteps = simulationTimeSteps.size();

        qDebug() << "loaded time step info " << numTimeSteps;
    }

    virtual void loadTemporallyStaticAttributes(
            std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
            std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
            std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
            std::map< std::string, BaseConstant > & constants,
            std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
            std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
            std::map< std::string, std::map< std::string, LookupTexture< TargetFloatType > > > & lookupTextures,
            std::vector< std::int32_t > & simulationTimeSteps,
            std::vector< double > & realTime,
            std::int32_t & numTimeSteps,
            std::map< std::string, std::size_t > & numParticles )
    {
        qDebug() << "loading static info";

        std::string ptype = "voxel";
        numParticles.insert( { ptype, ( std::size_t ) DIM_X*DIM_Y*DIM_Z } );

        loadTimeStepInfo(
            simulationTimeSteps,
            realTime,
            numTimeSteps );

        particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                                                   ptype,
                                                   std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

        variantAttributeRanges.insert( std::pair< std::string, std::map< std::string, Vec2< double > > > (
                                           ptype,
                                           std::map< std::string, Vec2< double > >( ) ) );

        std::ifstream varFile( this->m_basicDataInfo.location + "/variables.txt" );

        std::vector< std::string > variables;
        std::string varString;

        int vIdx = 0;
        while( std::getline( varFile, varString ) )
        {
            variables.push_back( varString );
            m_variableOrder.insert( { varString, vIdx } );
            ++vIdx;
        }
        varFile.close();

        varFile.open( this->m_basicDataInfo.location + "/matlab/ref.txt" );
        vIdx = 0;
        while( std::getline( varFile, varString ) )
        {
            std::stringstream sstr( varString );
            std::string a, b, c, d;
            sstr >> a >> b >> c >> d;
            double scaleFactor = std::stod( d );
            m_variableScaleFactors.insert( { variables[ vIdx ], scaleFactor } );
            ++vIdx;
        }

        variables.push_back( "x" );
        variables.push_back( "y" );
        variables.push_back( "z" );

        varFile.close();

        for( auto & attr : variables )
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                                                                             attr,
                                                                             std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
            for( int t = 0; t < numTimeSteps; ++t )
            {
                 particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
            }

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                                               attr,
                                               Vec2< double >() ) );
        }

        m_swap.resize( N_VOX );
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param particleAndTimeVariantAttributes
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes )
    {
        std::string t = m_simulationTimeSteps[ timeStep ];

        for( const auto & partType : needToLoad )
        {
            for( const auto & attr : partType.second  )
            {
                qDebug() << "loading " << attr.c_str();

                std::unique_ptr< std::vector< TargetFloatType > > & dat
                    = particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ];

                if( dat == nullptr )
                {
                    dat.reset( new std::vector< TargetFloatType >( N_VOX) );
                }
                else
                {
                    ( *dat ).resize( N_VOX );
                }

                if( attr == "x" || attr == "y" || attr == "z" )
                {
                    for( int x = 0; x < DIM_X; ++x )
                    {
                        for( int y = 0; y < DIM_Y; ++y )
                        {
                            for( int z = 0; z < DIM_Z; ++z )
                            {
                                int flatIDX = x + DIM_X * ( y + z * DIM_Y );
                                (*dat)[ flatIDX ] = attr == "x" ? x :
                                                    attr == "y" ? y : z;
                            }
                        }
                    }
                }
                else
                {
                    const double SCALE_FACTOR = m_variableScaleFactors.at( attr );

                    std::string fileName = this->m_basicDataInfo.location + "/data/hcci." + t + ".field.mpi";
                    std::ifstream inFile( fileName );

                    if( ! inFile.is_open() )
                    {
                           qDebug() << "failed to load file: " << fileName.c_str();
                    }

                    m_swap.resize(   N_VOX );

                    inFile.seekg( N_VOX * m_variableOrder.at( attr ) * sizeof( double ) );
                    inFile.read( (char *) m_swap.data(), N_VOX * sizeof( double ) );

                    for( int i = 0; i < N_VOX; ++i )
                    {
                        (*dat)[ i ] = m_swap[ i ] * SCALE_FACTOR;
                    }

                    inFile.close();
                }
            }
        }
    }

    /**
     * @brief ~VolumeDataImporter
     */
    virtual ~VolumeDataImporter() {}
};

}

#endif // FROMVOLUME_HPP
