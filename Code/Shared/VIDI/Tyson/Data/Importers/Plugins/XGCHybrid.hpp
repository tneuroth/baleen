

#ifndef TN_XGC_HYBRID_IMPORTER
#define TN_XGC_HYBRID_IMPORTER

#include "Data/Importers/DistributionDataImporter.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Summary/Summary.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"

#include <adios2.h>
#include <fstream>
#include <memory>
#include <cstdint>
#include <vector>
#include <set>
#include <string>
#include <map>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The XGCHybrid class
 */
class XGCHybridImporter : public DistributionDataImporter< TargetFloatType >
{
    /** @brief The floating point type that the distribution data is represented by on disk */
    typedef float OrigionalFloatType;

    /************************************************************************/

    std::unique_ptr< adios2::ADIOS > m_adiosObject;
    std::unique_ptr< std::string, adios2::IO > m_ioObject;
    std::unique_ptr< adios2::Engine > m_reader;
    std::vector< OrigionalFloatType > m_swap;

    std::vector< std::string > m_particleTypes;
    std::map< std::string, TN::ScalarVariableStatistics< TargetFloatType > > m_availableStatistics;
    std::map< std::string, std::map< std::string, TN::HistogramDefinition< TargetFloatType > > m_availableHistograms;

    std::int32_t m_numTimeSteps;
    std::int32_t m_numGridPoints;

    /** @brief The base path where the data files are located */
    std::string m_basePath;

    /*************************************************************************/

public:

    /**
     * @brief init
     * @param dataSetMetaData
     * @return
     */

    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        m_basePath = basicDataInfo.location;

        m_adiosObject = std::unique_ptr< adios2::ADIOS >(
            new adios2::ADIOS( adios2::DebugOFF ) );

        m_ioObject = std::unique_ptr< adios2::IO >(
            new adios2::IO( m_adiosObject->DeclareIO( "Summary-IO" ) ) );

        m_reader = std::unique_ptr< adios2::Engine >(
            new adios2::Engine( m_ioObject->Open(
                m_basePath + "/summary.bp", adios2::Mode::Write ) ) );

        adios2::Attribute< std::string > particles = m_ioObject->InquireAttribute< std::string >( "particles" );
        for( auto & s : particles.Data() )
        {
            m_particleTypes.push_back( s );
        }

        return m_particleTypes.size();
    }

    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics,
        std::vector< unsigned int > & triangulation,
        std::vector< int64_t > & neighborhoods,
        std::vector< int64_t > & neighborhoodOffsets,
        std::map< std::string, std::vector< TargetFloatType > > & perGridPointAttributes,
        std::map< std::string, Vec2< double > > & perGridPointAttributeRanges,
        std::vector< std::string > & gridPointSpatialAttributes,
        std::map< std::string, std::map< std::string, PredefinedHistogram > > & perParticleTypePerDistributionTypeParameters,
        std::map< std::string, std::map< std::string, double > > & perParticleTypeConstants,
        std::set< std::string > & particleTypes,
        std::map< std::string, std::set< std::string > > & perParticleTypeDistributionTypes,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTimeValues,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::int32_t & numTimeSteps )
    {
        particleTypes.insert( m_particleTypes.begin(), m_particleTypes.end() );

        for( auto & ptype : m_particleTypes )
        {
            perParticleTypeConstants.insert( {  ptype, {} } );
            perParticleTypeDistributionTypes.insert( std::pair< std::string, std::set< std::string > >(
                        ptype, std::set< std::string > ) );

            for( auto & h : m_availableHistograms.at( ptype ) )
            {
                perParticleTypeDistributionTypes.at( ptype ).insert( h.first );
            }

            distributions.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                                      ptype,
                                      std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >() ) );

            statistics.insert( std::pair< std::string, std::map< std::string, std::vector< std::vector< TargetFloatType > > > >(
                                   ptype,
                                   std::map< std::string, std::vector< std::vector< TargetFloatType > > >() ) );

            perParticleTypePerDistributionTypeParameters.insert( { ptype, std::map< std::string, PredefinedHistogram >()  } );

            for( auto & dtype : m_distributionTypes )
            {
                perParticleTypePerDistributionTypeParameters.find( ptype )->second.insert( { dtype, PredefinedHistogram() } );

                distributions.find( ptype )->second.insert( std::pair<  std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            dtype,
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > >() ) );
            }
            for( std::string s : m_statistics  )
            {
                statistics.find( ptype )->second.insert( { s, std::vector< std::vector< float > >() } );
            }
        }

        gridPointSpatialAttributes.push_back( "r" );
        gridPointSpatialAttributes.push_back( "z" );

        const std::string ISTEP_PATH    = m_basePath + "/tsteps.dat";
        const std::string REALTIME_PATH = m_basePath + "/realtime.dat";

        ////////////////////////////////////////////////////////////////////

        // Meta file

        OrigionalFloatType partDv;
        int vperp_bins;
        int vpara_bins;

        std::ifstream metaFile( m_basePath + "/summary.meta.txt" );
        std::string metaLine;
        {
            std::getline( metaFile, metaLine );
            std::stringstream sst( metaLine );
            std::string nm;
            sst >> nm >> partDv;
        }
        {
            std::getline( metaFile, metaLine );
            std::stringstream sst( metaLine );
            std::string nm;
            sst >> nm >> vpara_bins;
        }
        {
            std::getline( metaFile, metaLine );
            std::stringstream sst( metaLine );
            std::string nm;
            sst >> nm >> vperp_bins;
        }
        {
            std::getline( metaFile, metaLine );
            std::stringstream sst( metaLine );
            std::string nm;
            sst >> nm >> m_numGridPoints;
        }
        metaFile.close();

        m_numDiscreteVelocities = vperp_bins - 1;
        OrigionalFloatType partNormFactor = 1.0;
        m_DistributionNumBins = ( 2 * m_numDiscreteVelocities + 1 ) * ( m_numDiscreteVelocities + 1 );

        ////////////////////////////////////////////////////////////////////////////////////////////////

        std::vector< std::string > possibleTypes = {  "ions", "electrons" };
        for( const auto & ptype : possibleTypes )
        {
            if( perParticleTypeDistributionTypes.find( ptype )!= perParticleTypeDistributionTypes.end() )
            {
                for( auto & dtype : m_distributionTypes )
                {
                    PredefinedHistogram & pdh = perParticleTypePerDistributionTypeParameters.find( ptype )->second.find( dtype )->second;
                    pdh.normalizationFactor = partNormFactor;
                    pdh.valueRange = std::vector< Vec2< float > >( { Vec2< float > ( -partDv * m_numDiscreteVelocities, partDv * m_numDiscreteVelocities ), Vec2< float >( 0, partDv * m_numDiscreteVelocities ) } );
                    pdh.resolution = std::vector< std::int32_t >( { 2 * m_numDiscreteVelocities + 1, m_numDiscreteVelocities + 1 } );
                    pdh.binningSpace = { "vpara", "|vperp|" };
                    pdh.name = dtype;
                }
            }
        }

        /**************************************************************************************/

        m_swap.resize( m_numGridPoints );

        std::ifstream fIn( m_basePath + "/summary.grid.dat" );

        if( ! fIn.is_open() )
        {
            std::cerr << "Failed to open file " << m_basePath + "/summary.grid.dat\n";
        }

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "r", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "r", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "r" ).x() << " " << perGridPointAttributeRanges.at( "r" ).y() << "\n";

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "z", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "z", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "z" ).x() << " " << perGridPointAttributeRanges.at( "z" ).y() << "\n";

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "psin", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "psin", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "psin" ).x() << " " << perGridPointAttributeRanges.at( "psin" ).y() << "\n";

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "poloidal_angle", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "poloidal_angle", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "poloidal_angle" ).x() << " " << perGridPointAttributeRanges.at( "poloidal_angle" ).y() << "\n";

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "bfield_strength", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "bfield_strength", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "bfield_strength" ).x() << " " << perGridPointAttributeRanges.at( "bfield_strength" ).y() << "\n";

        fIn.read( (char *) m_swap.data(), sizeof( float ) * m_numGridPoints );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "volume", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "volume", TN::Sequential::getRange( m_swap ) } );
        std::cerr << perGridPointAttributeRanges.at( "volume" ).x() << " " << perGridPointAttributeRanges.at( "volume" ).y() << "\n";

        fIn.close();

        /****************************************************************************************/

        std::ifstream tstepsFile( ISTEP_PATH );

        if( ! tstepsFile.is_open() )
        {
            std::cerr << "Failed to open file " << ISTEP_PATH << "\n";
        }

        tstepsFile.seekg( 0, std::ios::end );
        size_t fileSize = tstepsFile.tellg();
        m_numTimeSteps = fileSize / sizeof( std::int64_t );
        numTimeSteps = m_numTimeSteps;
        simulationTimeSteps.resize( m_numTimeSteps );
        realTimeValues.resize( m_numTimeSteps );
        tstepsFile.close();

        std::vector< int64_t > tstepsTemp( m_numTimeSteps );
        tstepsFile.open( ISTEP_PATH );
        if( ! tstepsFile.is_open() )
        {
            std::cerr << "Failed to open file " << ISTEP_PATH << "\n";
        }
        tstepsFile.read(  (char*) tstepsTemp.data(), m_numTimeSteps*sizeof( std::int64_t ) );
        tstepsFile.close();

        std::vector< double > rtTemp( m_numTimeSteps );
        tstepsFile.open( REALTIME_PATH );
        if( ! tstepsFile.is_open() )
        {
            std::cerr << "Failed to open file " << REALTIME_PATH << "\n";
        }
        tstepsFile.read(  (char*) rtTemp.data(), m_numTimeSteps*sizeof( double ) );
        tstepsFile.close();

        for( int i = 0; i < m_numTimeSteps; ++i )
        {
            simulationTimeSteps[ i ] = tstepsTemp[ i ];
            realTimeValues[ i ] = rtTemp[ i ];
        }

        for( auto & ptype : m_particleTypes )
        {
            for( auto & dtype : m_distributionTypes )
            {
                for( int t = 0; t < m_numTimeSteps; ++t )
                {
                    distributions.find( ptype )->second.find( dtype )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
                }
            }
            for( auto & s : m_statistics )
            {
                for( int t = 0; t < m_numTimeSteps; ++t )
                {
                    statistics.find( ptype )->second.find( s )->second.push_back( std::vector< TargetFloatType >( ) );
                }
            }
        }

        m_swap.resize( m_DistributionNumBins * m_numGridPoints );

        ///////////////////////////////////////////////////////////

        // triangulation

        std::ifstream meshFile( m_basePath + "/summary.grid.delaunay.obj" );
        std::string line;
        while( std::getline( meshFile, line ) )
        {
            if( line.size() > 3 )
            {
                std::stringstream sstr( line );
                std::string v;
                sstr >> v;
                if( v == "f" )
                {
                    unsigned int i1, i2, i3;
                    sstr >> i1 >> i2 >> i3;
                    triangulation.push_back( i1 - 1 );
                    triangulation.push_back( i2 - 1 );
                    triangulation.push_back( i3 - 1 );
                }
            }
        }

        std::cout << "triangulation size: " << triangulation.size() / 3 << std::endl;

        // Neigborhoods
        neighborhoodOffsets.resize( m_numGridPoints );
        std::ifstream nbFile( m_basePath + "/summary.grid.neighbor.counts.dat" );
        nbFile.read( (char*) neighborhoodOffsets.data(), m_numGridPoints * sizeof( int64_t ) );
        nbFile.close();

        size_t totalNeighbors = neighborhoodOffsets.back();
        neighborhoods.resize( totalNeighbors );
        nbFile.open( m_basePath + "/summary.grid.neighbors.dat" );
        if( ! nbFile.is_open() )
        {
            std::cout << "couldn't open file " << m_basePath + "/summary.grid.neighbors.dat" << std::endl;
        }
        nbFile.read( (char*) neighborhoods.data(), totalNeighbors * sizeof( int64_t ) );
        nbFile.close();
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param distributions
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions
    )
    {
        std::cout << "loading time step " << timeStep << std::endl;

        size_t TIME_CHUNK_OFFSET_SIZE = ( 6 + m_DistributionNumBins ) * m_numGridPoints * sizeof( OrigionalFloatType );

        const size_t NUM_BYTES_DIST = m_numGridPoints * m_DistributionNumBins * sizeof( OrigionalFloatType );

        std::cout << NUM_BYTES_DIST << " " << TIME_CHUNK_OFFSET_SIZE << "\n";
        std::cout << NUM_BYTES_DIST << " " << TIME_CHUNK_OFFSET_SIZE << "\n";
        std::cout << m_fileStreams.find( "ions" )->second->is_open() << "\n";

        for( const auto & partType : needToLoad )
        {
            std::cout << partType.first << "\n";
            m_fileStreams.find( partType.first )->second->seekg( TIME_CHUNK_OFFSET_SIZE * timeStep );

            for( const auto & distType : m_distributionTypes  )
            {
                if( ! std::is_same< OrigionalFloatType, TargetFloatType >::value )
                {
                    m_fileStreams.find( partType.first )->second->read( ( char* ) m_swap.data(), NUM_BYTES_DIST );
                    distributions.find( partType.first )->second.find( distType )->second[ timeStep ].reset(
                        new std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) );
                }
                else
                {
                    distributions.find( partType.first )->second.find( distType )->second[ timeStep ].reset(
                        new std::vector< TargetFloatType >( m_numGridPoints * m_DistributionNumBins ) );

                    m_fileStreams.find( partType.first )->second->read(
                        ( char * ) distributions.find( partType.first )->second.find( distType )->second[ timeStep ]->data(),
                        NUM_BYTES_DIST );
                }
            }
        }
    }

    virtual void loadStatistics(
        const std::string & ptype,
        std::int64_t tstep,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics )
    {
        std::cout << "loading step " << tstep << "stats" << std::endl;

        size_t TIME_CHUNK_OFFSET_SIZE = ( 6 + m_DistributionNumBins ) * m_numGridPoints * sizeof( OrigionalFloatType );

        for( int i = 0; i < m_statistics.size(); ++i )
        {
            size_t LOCAL_OFFSET = m_numGridPoints * m_DistributionNumBins * sizeof( OrigionalFloatType )
                                  + i * m_numGridPoints * sizeof( OrigionalFloatType );

            m_fileStreams.find( ptype )->second->seekg( TIME_CHUNK_OFFSET_SIZE * tstep + LOCAL_OFFSET );

            std::cout << "loading " << m_statistics[ i ] << std::endl;

            if( ! std::is_same< OrigionalFloatType, TargetFloatType >::value )
            {
                std::cout << "not same type " << m_statistics[ i ] << std::endl;
                std::vector< float > tmp( m_numGridPoints );
                m_fileStreams.find( ptype )->second->read( ( char* ) tmp.data(), m_numGridPoints * sizeof( OrigionalFloatType ) );
                statistics.find( ptype )->second.find( m_statistics[ i ] )->second[ tstep ]=
                    std::vector< TargetFloatType >( tmp.begin(), tmp.end() );
            }
            else
            {
                std::cout << "is same type " << m_statistics[ i ] << std::endl;
                std::cout << "vector tsteps size " << statistics.find( ptype )->second.find( m_statistics[ i ] )->second.size() << std::endl;
                statistics.find( ptype )->second.find( m_statistics[ i ] )->second[ tstep ].resize( m_numGridPoints );
                m_fileStreams.find( ptype )->second->read(
                    ( char * ) statistics.find( ptype )->second.find( m_statistics[ i ] )->second[ tstep ].data(),
                    m_numGridPoints * sizeof( OrigionalFloatType ) );
            }
        }
    }

    /**
     * @brief ~XGCHybridImporter
     */
    virtual ~XGCHybridImporter()
    {
        if( m_reader )
        {
            m_reader->Close();
        }
    }
};

}

#endif // TN_XGC_HYBRID_IMPORTER

