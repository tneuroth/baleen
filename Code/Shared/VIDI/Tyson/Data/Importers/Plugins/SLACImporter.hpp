

#ifndef TN_SLACIMPORTER
#define TN_SLACIMPORTER

#include "Types/Vec.hpp"
//#include "Data/Interpolators/FieldInterpolator.h"
#include "../ParticleDataImporter.hpp"
#include "Data/Definitions/VisualContextModel.hpp"

#include <QDebug>
#include <fstream>
#include <map>
#include <vector>
#include <memory>
#include <limits>
#include <cfloat>
#include <cstdint>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The SLACImporter class
 */
class SLACImporter : public ParticleDataImporter< TargetFloatType >
{

    typedef float OrigionalFloatType;

    std::vector< OrigionalFloatType > m_swap;
    std::vector< std::int32_t > m_simulationTimeSteps;

public :

    /**
     * @brief init
     * @param dataSetMetaData
     * @return
     */
    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        this->m_initialized = true;
        return this->m_initialized;
    }

    /**
     * @brief loadTemporallyStaticAttributes
     * @param particleVariantAttributes
     * @param constants
     * @param simulationTimeSteps
     * @param realTime
     */
    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
        std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
        std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
        std::map< std::string, BaseConstant > & constants,
        std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps,
        std::int32_t & numParticles )
    {
        numTimeSteps = 3771;
        numParticles = 14946;

        const std::string BASE_PATH = "/media/user/Samsung 1TB/datasets/SLAC_SUBSELECTION/";
        const std::string ptype = "ptcl";

        ////////////////////


        particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                    ptype,
                    std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

        variantAttributeRanges.insert( std::pair< std::string, std::map< std::string, Vec2< double > > > (
                                           ptype,
                                           std::map< std::string, Vec2< double > >( ) ) );


        // x
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "x",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "x",
                        Vec2< double > ( -0.103362, 0.10331 ) ) );
        }


        // y
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "y",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );


            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "y",
                        Vec2< double > ( -0.10336, 0.103299 ) ) );
        }


        // z
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "z",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "z",
                        Vec2< double > ( -0.149999, 10.8718 ) ) );
        }


        // mx
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "mx",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "mx",
                        Vec2< double > ( -5.30735e-22, 5.46387e-22 ) ) );
        }


        // my
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "my",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "my",
                        Vec2< double > ( -5.18241e-22, 5.30594e-22 ) ) );
        }

        // mz
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "mz",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "mz",
                        Vec2< double > ( -6.0e-20, 6.0e-20 ) ) );
        }

        // maxMZ
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "maxMZ",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "maxMZ",
                        Vec2< double > ( -.5e-20, .5e-20 ) ) );
        }

        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "startZ",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "startZ",
                        Vec2< double > ( -0.149999, 1.6 ) ) );
        }

        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "midZ",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "midZ",
                        Vec2< double > ( -0.149999, 10.0 ) ) );
        }
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "mr",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "mr",
                        Vec2< double > ( 0, 4.30594e-22 ) ) );
        }
        {
            particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                        "r",
                        std::vector< std::unique_ptr< std::vector< TargetFloatType > > > ( numTimeSteps ) ) );

            variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                        "r",
                        Vec2< double > ( 0, 0.05 ) ) );
        }

        std::ifstream inFile;

        std::vector< float > _maxMZ( numParticles, 0.f  );

        //qDebug() <<particleAndTimeVariantAttributes.find( ptype )->second.find( "x" )->second.size();

        for( int t = 0; t < numTimeSteps; ++t )
        {
            simulationTimeSteps.push_back( t );

            std::unique_ptr< std::vector< TargetFloatType > > & x  = particleAndTimeVariantAttributes.find( ptype )->second.find( "x" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & y  = particleAndTimeVariantAttributes.find( ptype )->second.find( "y" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & z  = particleAndTimeVariantAttributes.find( ptype )->second.find( "z" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & mx = particleAndTimeVariantAttributes.find( ptype )->second.find( "mx" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & my = particleAndTimeVariantAttributes.find( ptype )->second.find( "my" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & mz = particleAndTimeVariantAttributes.find( ptype )->second.find( "mz" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & maxMZ = particleAndTimeVariantAttributes.find( ptype )->second.find( "maxMZ" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & mr = particleAndTimeVariantAttributes.find( ptype )->second.find( "mr" )->second[ t ];
            std::unique_ptr< std::vector< TargetFloatType > > & r = particleAndTimeVariantAttributes.find( ptype )->second.find( "r" )->second[ t ];

            x.reset(  new std::vector< TargetFloatType >( numParticles ) );
            y.reset(  new std::vector< TargetFloatType >( numParticles ) );
            z.reset( new std::vector< TargetFloatType >( numParticles ) );
            mx.reset( new std::vector< TargetFloatType >( numParticles ) );
            my.reset(  new std::vector< TargetFloatType >( numParticles ) );
            mz.reset(  new std::vector< TargetFloatType >( numParticles ) );
            mr.reset(  new std::vector< TargetFloatType >( numParticles ) );
            maxMZ.reset(  new std::vector< TargetFloatType >( numParticles ) );
            r.reset(  new std::vector< TargetFloatType >( numParticles ) );

            inFile.open( BASE_PATH + std::to_string(t) + ".x.bin" );

            if( ! inFile.is_open() )
            {
                //qDebug() << "failed to open file ";;
            }

            inFile.read( (char*) ( *x ).data(), numParticles*sizeof( float ) );
            inFile.close();

            inFile.open( BASE_PATH + std::to_string(t) + ".y.bin" );
            inFile.read( (char*) ( *y ).data(), numParticles*sizeof( float ) );
            inFile.close();

            inFile.open( BASE_PATH + std::to_string(t) + ".z.bin" );
            inFile.read( (char*) ( *z ).data(), numParticles*sizeof( float ) );
            inFile.close();

            inFile.open( BASE_PATH + std::to_string(t) + ".mx.bin" );
            inFile.read( (char*) ( *mx ).data(), numParticles*sizeof( float ) );
            inFile.close();

            inFile.open( BASE_PATH + std::to_string(t) + ".my.bin" );
            inFile.read( (char*) ( *my ).data(), numParticles*sizeof( float ) );
            inFile.close();

            inFile.open( BASE_PATH + std::to_string(t) + ".mz.bin" );
            inFile.read( (char*) ( *mz ).data(), numParticles*sizeof( float ) );
            inFile.close();

            for( int p = 0; p < numParticles; ++p )
            {
                (_maxMZ)[ p ] = std::max( (_maxMZ)[ p ], (*mz)[ p ] );
                (*mr)[ p ] = ( float ) std::sqrt( (long double ) (*mx)[ p ] * (long double ) (*mx)[ p ] + (long double ) (*my)[ p ] * (long double ) (*my)[ p ] );
                (*r)[ p ] = std::sqrt( (long double )(*x)[ p ] * (long double ) (*x)[ p ] + (long double ) (*y)[ p ] * (long double ) (*y)[ p ] );
            }
        }

        std::vector< TargetFloatType >  & z0 = (*particleAndTimeVariantAttributes.find( ptype )->second.find( "z" )->second[ 0 ]);
        std::vector< TargetFloatType >  & zend = (*particleAndTimeVariantAttributes.find( ptype )->second.find( "z" )->second[ numTimeSteps * .5 ]);

        for( int t = 0; t < numTimeSteps; ++t )
        {
            std::unique_ptr< std::vector< TargetFloatType > > & maxMZ = particleAndTimeVariantAttributes.find( ptype )->second.find( "maxMZ" )->second[ t ];
            maxMZ.reset(  new std::vector< TargetFloatType >( (_maxMZ).begin(), (_maxMZ).end() ) );

            std::unique_ptr< std::vector< TargetFloatType > > & startZ = particleAndTimeVariantAttributes.find( ptype )->second.find( "startZ" )->second[ t ];
            startZ.reset( new std::vector< TargetFloatType >( z0.begin(), z0.end() ) );

            std::unique_ptr< std::vector< TargetFloatType > > & midZ = particleAndTimeVariantAttributes.find( ptype )->second.find( "midZ" )->second[ t ];
            midZ.reset( new std::vector< TargetFloatType >( zend.begin(), zend.end() ) );
        }

        //qDebug() << "data loaded";
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param particleAndTimeVariantAttributes
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes )
    {}

    /**
     * @brief ~SLACImporter
     */
    virtual ~SLACImporter() {}
};

}

#endif // SLACIMPORT

