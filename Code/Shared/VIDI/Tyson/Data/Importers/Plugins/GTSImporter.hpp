

#ifndef TN_GTS_IMPORTER
#define TN_GTS_IMPORTER

#include "Data/Importers/DistributionDataImporter.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"

#include <fstream>
#include <memory>
#include <cstdint>
#include <vector>
#include <set>
#include <string>
#include <map>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The GTSImporter class
 */
class GTSImporter : public DistributionDataImporter< TargetFloatType >
{
    /** @brief The floating point type that the distribution data is represented by on disk */
    typedef double OrigionalFloatType;

    /** @brief file streams containing the Distribution data, one per particle type */
    std::map< std::string, std::unique_ptr< std::ifstream > > m_fileStreams;

    /** @brief The base path where the data files are located */
    std::string m_basePath;

    /** @brief Available Distribution Types */
    std::vector< std::string > m_distributionTypes;

    /** @brief Available Particle Types */
    std::vector< std::string > m_particleTypes;

    /** @brief Relative path to the Ion distribution Data */
    std::string m_ionPath;

    /** @brief Relative path to the Electrob distribution Data */
    std::string m_electronPath;

    /** @brief a temporary storage vector to be used for reading and swapping,
       used when the origional type (in the binary file) differs from the target type. */
    std::vector< OrigionalFloatType > m_swap;

    /** @brief fileSize / m_timeChunkReadSize = numTimeSteps */
    std::uint64_t m_fileSize;

    /** @brief fileSize / m_timeChunkReadSize = numTimeSteps */
    std::uint64_t m_timeChunkReadSize;

    /** @brief fileSize / m_timeChunkReadSize = numTimeSteps */
    std::int32_t m_numTimeSteps;

    /** @brief The number of grid points in the simulation where the distribution functions are evaluated, npts in simulation */
    std::int32_t m_numGridPoints;

    /** @brief The number of discrete velocities used to produce the Distributions,
       corresponding to nv, as named in simulation, where the Distribution
       resolution is ( 2 * nv + 1 ) X ( nv + 1 ). The second dimension is
       magnitude only, while the first dimension is signed.  */
    std::int32_t m_numDiscreteVelocities;

    /** @brief The total number of Distribution bins is ( 2 * m_numDiscreteVelocities + 1 ) X ( m_numDiscreteVelocities + 1 ) */
    std::int32_t m_DistributionNumBins;

    template< typename T >
    Vec2< double > getRange( const std::vector< T > & values )
    {
        Vec2< double > result( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max()  );
        for( auto val : values )
        {
            result.a( std::min( result.a(), ( double ) val ) );
            result.b( std::max( result.b(), ( double ) val ) );
        }
        return result;
    }

public:

    /**
     * @brief init
     * @param dataSetMetaData
     * @return
     */
    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        m_distributionTypes =
        {
            "Particle Distribution",
            "Momentum Distribution",
            "Momentum Flux",
            "Energy Distribution",
            "Energy Flux",
            "Particle Flux",
            "Bootstrap Current"
        };

        m_ionPath = "/PDF_ion_data.dat";
        m_electronPath ="/PDF_electron_data.dat";

        this->m_basicDataInfo = basicDataInfo;
        m_basePath = basicDataInfo.location;

        //! attempt to instantiate input file streams for each data file
        m_fileStreams.insert( std::pair< std::string, std::unique_ptr< std::ifstream > >(       "ions", std::unique_ptr< std::ifstream >( new std::ifstream( m_basePath + m_ionPath      ) ) ) );
        m_fileStreams.insert( std::pair< std::string, std::unique_ptr< std::ifstream > >(  "electrons", std::unique_ptr< std::ifstream >( new std::ifstream( m_basePath + m_electronPath ) ) ) );

        if( m_fileStreams.find( "ions" )->second->is_open() )
        {
            m_particleTypes.push_back( "ions" );
        }
        else
        {
            m_fileStreams.erase( "ions" );
        }


        if( m_fileStreams.find( "electrons" )->second->is_open() )
        {
            m_particleTypes.push_back( "electrons" );
        }
        else
        {
            m_fileStreams.erase( "electrons" );
        }

        // true if at least one particle type file is open
        return m_particleTypes.size();
    }

    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics,
        std::vector< unsigned int > & triangulation,
        std::vector< int64_t > & neighborhoods,
        std::vector< int64_t > & neighborhoodOffsets,
        std::map< std::string, std::vector< TargetFloatType > > & perGridPointAttributes,
        std::map< std::string, Vec2< double > > & perGridPointAttributeRanges,
        std::vector< std::string > & gridPointSpatialAttributes,
        std::map< std::string, std::map< std::string, PredefinedHistogram > > & perParticleTypePerDistributionTypeParameters,
        std::map< std::string, std::map< std::string, double > > & perParticleTypeConstants,
        std::set< std::string > & particleTypes,
        std::map< std::string, std::set< std::string > > & perParticleTypeDistributionTypes,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTimeValues,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::int32_t & numTimeSteps )
    {
        particleTypes.insert( m_particleTypes.begin(), m_particleTypes.end() );

        for( auto & pt : particleTypes )
        {
            //qDebug() << "m_particle types already included " << pt.c_str();
        }

        for( auto & ptype : m_particleTypes )
        {
            perParticleTypeConstants.insert( {  ptype, {} } );

            perParticleTypeDistributionTypes.insert( std::pair< std::string, std::set< std::string > >(
                        ptype, std::set< std::string >( m_distributionTypes.begin(), m_distributionTypes.end() ) ) );
            distributions.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                                      ptype,
                                      std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >() ) );

            perParticleTypePerDistributionTypeParameters.insert( { ptype, std::map< std::string, PredefinedHistogram >()  } );

            for( auto & dtype : m_distributionTypes )
            {
                perParticleTypePerDistributionTypeParameters.find( ptype )->second.insert( { dtype, PredefinedHistogram() } );

                distributions.find( ptype )->second.insert( std::pair<  std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            dtype,
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > >() ) );
            }
        }

        gridPointSpatialAttributes.push_back( "r" );
        gridPointSpatialAttributes.push_back( "z" );

        const std::string ISTEP_PATH    = "/iSteps.dat";
        const std::string REALTIME_PATH = "/realTime.dat";

        std::int32_t nbytes, iStep;
        double realTime;

        std::ifstream fIn( m_basePath + "/PDF_grid_parameters.dat" );

        fIn.read(                  (char *)&nbytes, sizeof(                  nbytes ) );
        fIn.read(         (char *)&m_numGridPoints, sizeof(         m_numGridPoints ) );
        fIn.read( (char *)&m_numDiscreteVelocities, sizeof( m_numDiscreteVelocities ) );
        fIn.read(                 (char *)&nbytes,  sizeof(                  nbytes ) );

        m_DistributionNumBins = ( 2 * m_numDiscreteVelocities + 1 ) * ( m_numDiscreteVelocities + 1 );

        std::vector< std::string > possibleTypes = {  "ions", "electrons" };
        for( const auto & ptype : possibleTypes )
        {
            OrigionalFloatType partRatio, partDv, partNormFactor;

            fIn.read( (char *)&nbytes, sizeof( nbytes ) );
            fIn.read(    (char *)&partRatio,        sizeof( OrigionalFloatType ) );
            fIn.read(    (char *)&partDv,           sizeof( OrigionalFloatType ) );
            fIn.read(    (char *)&partNormFactor, sizeof( OrigionalFloatType ) );
            fIn.read( (char *)&nbytes, sizeof( nbytes ) );

            if( perParticleTypeDistributionTypes.find( ptype )!= perParticleTypeDistributionTypes.end() )
            {
                perParticleTypeConstants.find( ptype )->second.insert( std::pair< std::string, double >( "particle_ratio", partRatio ) );
                for( auto & dtype : m_distributionTypes )
                {
                    PredefinedHistogram & pdh = perParticleTypePerDistributionTypeParameters.find( ptype )->second.find( dtype )->second;
                    pdh.normalizationFactor = partNormFactor;
                    pdh.valueRange = std::vector< Vec2< float > >( { Vec2< float > ( -partDv * m_numDiscreteVelocities, partDv * m_numDiscreteVelocities ), Vec2< float >( 0, partDv * m_numDiscreteVelocities ) } );
                    pdh.resolution = std::vector< std::int32_t >( { 2 * m_numDiscreteVelocities + 1, m_numDiscreteVelocities + 1 } );
//                    pdh.partitionSpace = { "r", "z" };
                    pdh.binningSpace = { "vpara", "|vperp|" };
//                    pdh.partitionSpaceScaling = { 1.0, 1.0 };
                    pdh.name = dtype;
                }
            }
        }

        const std::int32_t NUM_BYTES_GRID =  m_numGridPoints * sizeof( OrigionalFloatType );
        m_swap.resize( m_numGridPoints );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*) m_swap.data(), NUM_BYTES_GRID );
        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "volume", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "volume", getRange( m_swap ) } );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*) m_swap.data(), NUM_BYTES_GRID );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "radial_position", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "radial_position", getRange( m_swap ) } );

        fIn.read( (char*)   m_swap.data(), NUM_BYTES_GRID );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "poloidal_angle", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "poloidal_angle", getRange( m_swap ) } );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*)   m_swap.data(), NUM_BYTES_GRID );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "r", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "r", getRange( m_swap ) } );

        fIn.read( (char*)   m_swap.data(), NUM_BYTES_GRID );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "z", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "z", getRange( m_swap ) } );

        fIn.read( (char*) m_swap.data(), NUM_BYTES_GRID );
        perGridPointAttributes.insert( std::pair< std::string, std::vector< TargetFloatType > >( "bfield_strength", std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) ) );
        perGridPointAttributeRanges.insert( { "bfield_strength", getRange( m_swap ) } );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );

        fIn.close();

        // Determine the size of a time steps worth of pdf data
        m_timeChunkReadSize = m_distributionTypes.size() * m_numGridPoints * m_DistributionNumBins * sizeof( OrigionalFloatType )
                              + sizeof( OrigionalFloatType ) + sizeof( nbytes ) * 10;

        const std::string _ptype = *( particleTypes.begin() );

        m_fileStreams.find( _ptype )->second->seekg( 0, std::ios::end );
        m_fileSize = m_fileStreams.find( _ptype )->second->tellg();
        m_numTimeSteps = m_fileSize / m_timeChunkReadSize;
        numTimeSteps = m_numTimeSteps;

        // Reserve the number of time steps based on the file size
        // and the data size per time step

        // extract the real simulation time and time step information
        // first check if this information has been written to file
        fIn.open( m_basePath + ISTEP_PATH );
        if( fIn.is_open() )
        {
            simulationTimeSteps.resize( m_numTimeSteps );
            realTimeValues.resize( m_numTimeSteps );

            nbytes = m_numTimeSteps * sizeof( std::int32_t );
            fIn.read( (char *)simulationTimeSteps.data(), nbytes );
            fIn.close();

            fIn.open( m_basePath + REALTIME_PATH );
            nbytes = m_numTimeSteps*sizeof( OrigionalFloatType );
            if( std::is_same< OrigionalFloatType, TargetFloatType >::value )
            {
                fIn.read( (char *) realTimeValues.data(), nbytes );
            }
            else
            {
                std::vector< OrigionalFloatType > temp( m_numTimeSteps );
                fIn.read( (char *) temp.data(), nbytes );
                realTimeValues = std::vector< double >( temp.begin(), temp.end() );
            }
            fIn.close();
        }
        // if not, then extract it from the other very large files and write the file
        else
        {
            simulationTimeSteps.clear();
            realTimeValues.clear();

            simulationTimeSteps.reserve( m_numTimeSteps );
            realTimeValues.reserve( m_numTimeSteps );

            for( std::int32_t t = 0; t < m_numTimeSteps; ++t )
            {
                m_fileStreams.find( _ptype )->second->seekg( m_timeChunkReadSize * t );

                m_fileStreams.find( _ptype )->second->read( (char *)&nbytes,   sizeof(   nbytes ) );
                m_fileStreams.find( _ptype )->second->read( (char *)&realTime, sizeof( realTime ) );
                m_fileStreams.find( _ptype )->second->read( (char *)&nbytes,   sizeof(   nbytes ) );

                m_fileStreams.find( _ptype )->second->read( (char *)&nbytes,   sizeof(   nbytes ) );
                m_fileStreams.find( _ptype )->second->read( (char *)&iStep,    sizeof(    iStep ) );

                realTimeValues.push_back(  realTime );
                simulationTimeSteps.push_back( iStep );
            }

            // in case the Target type is different from the original type
            std::vector< OrigionalFloatType > realTimeTemp( realTimeValues.begin(), realTimeValues.end() );

            // write to file
            std::ofstream outFile;

            outFile.open( m_basePath + ISTEP_PATH, std::ios::binary | std::ios::out );
            outFile.write( reinterpret_cast<const char*>( simulationTimeSteps.data() ), simulationTimeSteps.size() * sizeof( std::uint32_t ) );
            outFile.close();

            outFile.open( m_basePath + REALTIME_PATH, std::ios::binary | std::ios::out );
            outFile.write( reinterpret_cast<const char*>( realTimeTemp.data() ), realTimeTemp.size() * sizeof( OrigionalFloatType ) );
            outFile.close();
        }

        m_swap.resize( m_numGridPoints * m_DistributionNumBins );

        for( auto & ptype : m_particleTypes )
        {
            for( auto & dtype : m_distributionTypes )
            {
                for( int t = 0; t < m_numTimeSteps; ++t )
                {
                    distributions.find( ptype )->second.find( dtype )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
                }
            }
        }
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param distributions
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & distributions
    )
    {
        const std::int32_t NUM_BYTES_GRID = m_numGridPoints * m_DistributionNumBins * sizeof( OrigionalFloatType );
        for( const auto & partType : needToLoad )
        {
            m_fileStreams.find( partType.first )->second->seekg( m_timeChunkReadSize * timeStep +  9 * sizeof( std::int32_t ) + sizeof( OrigionalFloatType ) );

            for( const auto & distType : m_distributionTypes  )
            {
                if( partType.second.find( distType ) != partType.second.end() )
                {
                    if( ! std::is_same< OrigionalFloatType, TargetFloatType >::value )
                    {
                        m_fileStreams.find( partType.first )->second->read( ( char* ) m_swap.data(), NUM_BYTES_GRID );
                        distributions.find(partType.first )->second.find( distType )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) );
                    }
                    else
                    {
                        distributions.find(partType.first )->second.find( distType )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_numGridPoints * m_DistributionNumBins ) );
                        m_fileStreams.find( partType.first )->second->read(
                            ( char * ) distributions.find( partType.first )->second.find( distType )->second[ timeStep ]->data(),
                            NUM_BYTES_GRID );
                    }
                }
                else
                {
                    m_fileStreams.find( partType.first )->second->seekg( static_cast< std::int64_t >( m_fileStreams.find( partType.first )->second->tellg() ) + NUM_BYTES_GRID );
                }
            }
        }
    }

    virtual void loadStatistics(
        const std::string & ptype,
        std::int64_t tstep,
        std::map< std::string, std::map< std::string, std::vector< std::vector< float > > > > & statistics  ) {}

    /**
     * @brief ~GTSImporter
     */
    virtual ~GTSImporter()
    {
        for( auto & e : m_fileStreams )
        {
            e.second->close();
        }
    }
};

}

#endif // GTSIMPORTER

