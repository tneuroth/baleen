

#ifndef TN_DATA_IMPORTER
#define TN_DATA_IMPORTER

#include "Data/Configuration/BasicDataInfo.hpp"

namespace TN
{

template < typename TargetValueType >
/**
 * @brief The DataImporter class
 */
class DataImporter
{

protected:

    bool m_initialized;
    BasicDataInfo m_basicDataInfo;

public:

    /**
     * @brief initialized
     * @return
     */
    bool initialized()
    {
        return m_initialized;
    }

    /**
     * @brief init
     * @return
     */
    virtual bool init( const BasicDataInfo & ) = 0;

    /**
     * @brief ~DataImporter
     */
    virtual ~DataImporter() {}
};

}

#endif // DATAIMPORTER

