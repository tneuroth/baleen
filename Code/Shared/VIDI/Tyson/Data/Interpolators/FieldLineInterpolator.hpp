

#ifndef FIELDLINE_INTERPOLATOR_H
#define FIELDLINE_INTERPOLATOR_H

#include "Types/Vec.hpp"
#include "FieldInterpolator.h"

#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <functional>

using namespace std;
using namespace boost::numeric::odeint;
namespace pl = std::placeholders;

struct FieldLineInterpolator
{
    typedef boost::array< double , 3 > state_type;

    FieldInterpolator23 BFieldInterpolate;
    std::vector<state_type> int1;
    std::vector<state_type> int2;

    FieldLineInterpolator( const std::vector< TN::Vec2< double > > & mesh, const std::vector< TN::Vec3< double > > & field ) :
        BFieldInterpolate( mesh, field ) {}

    void writePoints1( const state_type & L , const double t )
    {
        int1.push_back( L );
    }

    void writePoints2( const state_type & L , const double t )
    {
        int2.push_back( L );
    }

    void f( const state_type & L, state_type & dLdPhi, double phi )
    {

        double r = L[0];
        double z = L[1];

        TN::Vec3 B = BFieldInterpolate( TN::Vec2< double >( r, z ) );

        double bR   = B.x();
        double bZ   = B.y();
        double bPhi = B.z();

        dLdPhi[0] =  r * bR / bPhi;
        dLdPhi[1] =  r * bZ / bPhi;
        dLdPhi[2] =  1.0;
    }

    void operator() ( state_type & lStart, state_type & lEnd, const int NPoints, std::vector< TN::Vec3< double > > & result )
    {

        int1.clear();
        int2.clear();

        double dPhi = ( lEnd[2] - lStart[2] ) / NPoints;

        integrate(
            std::bind( &FieldLineInterpolator::f, this, pl::_1, pl::_2, pl::_3 ),
            lStart,
            lStart[2],
            lEnd[2],
            dPhi,
            std::bind( &FieldLineInterpolator::writePoints1, this, pl::_1, pl::_2 )
        );

        integrate(
            std::bind( &FieldLineInterpolator::f, this, pl::_1, pl::_2, pl::_3 ),
            lEnd,
            lEnd[2],
            lStart[2],
            -dPhi,
            std::bind( &FieldLineInterpolator::writePoints2, this, pl::_1, pl::_2 )
        );

        for ( int i = 0; i < NPoints; ++i )
        {

            double c2 = ( ( double ) i ) / NPoints;
            double c1 = 1.0 - c2;

            double r   = c1*int1[i][0] + c2*int2[i][0];
            double z   = c1*int1[i][0] + c2*int2[i][0];
            double phi = c1*int1[i][0] + c2*int2[i][0];

            result.push_back( TN::Vec3( r, z, phi ) );
        }
    }
};

#endif

