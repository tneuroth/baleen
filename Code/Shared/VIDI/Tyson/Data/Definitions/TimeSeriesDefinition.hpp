

#ifndef TIMESERIESDEFINITION
#define TIMESERIESDEFINITION

#include <cstdint>
#include <string>
#include <cmath>
#include <QJsonObject>
#include <QDebug>

namespace TN
{

struct TimeSeriesDefinition
{
    std::string name;
    std::string description;

    std::int32_t firstIdx;
    std::int32_t lastIdx;
    std::int32_t idxStride;

    bool integrate;

    std::int32_t numSteps() const
    {
        return ( lastIdx - firstIdx ) / idxStride + 1;
    }
    bool operator == ( const TimeSeriesDefinition & rhs ) const
    {
        return ( firstIdx  == rhs.firstIdx  )
               && ( lastIdx  == rhs.lastIdx  )
               && ( idxStride == rhs.idxStride )
               && ( integrate == rhs.integrate );
    }

    TimeSeriesDefinition() : name( "" ), integrate( false ) {}

    std::string toString() const
    {
        return
            std::to_string( firstIdx ) + ", " +
            std::to_string( lastIdx ) + ", " +
            std::to_string( idxStride ) + ", " + std::to_string( integrate );
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );
        obj.insert( "firstIdx", firstIdx );
        obj.insert( "lastIdx", lastIdx );
        obj.insert( "idxStride", idxStride );
        obj.insert( "integrate", integrate );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
        firstIdx = obj[ "firstIdx" ].toInt();
        lastIdx = obj[ "lastIdx" ].toInt();
        idxStride = obj[ "idxStride" ].toInt();
        integrate = obj[ "integrate" ].toBool();
    }
};

}

#endif // TIMESERIESDEFINITION

