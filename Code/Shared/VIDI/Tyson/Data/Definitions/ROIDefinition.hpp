#ifndef ROIDEFINITION_HPP
#define ROIDEFINITION_HPP

#include "Types/Vec.hpp"

#include <QJsonObject>
#include <string>
#include <vector>

namespace TN
{

struct ROIDefinition
{
    std::string name;
    std::string description;
    int mode;
    std::vector< std::string > space;

    std::string spatialPath;
    std::string spatiotemporalPath;

    TN::Vec2< double > xROI;
    TN::Vec2< double > yROI;

    TN::Vec2< double > widths;

    ROIDefinition()
    {
        name = "None";
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
    }
};

}

#endif // ROIDEFINITION_HPP
