

#ifndef TN_PARTICLE_FILTER_HPP
#define TN_PARTICLE_FILTER_HPP

#include "Types/Vec.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"

#include <string>

namespace TN
{

struct ParticleFilter
{
    std::string name;
    std::string description;
    std::string expression;
    TimeSeriesDefinition window;
    ParticleFilter()
    {
        name = "None";
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );
        obj.insert( "expression", expression.c_str() );
        obj.insert( "window", window.toJSON() );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
        expression = obj[ "expression" ].toString().toStdString();

        ////qDebug() << "before window from json";

        window.fromJSON( obj[ "window" ].toObject() );
    }
};

}
#endif

