

#ifndef TN_VISUAL_CONTEXT_MODEL
#define TN_VISUAL_CONTEXT_MODEL

#include "Types/Vec.hpp"

#include <vector>
#include <map>

namespace TN
{

template< typename TargetFloatType >
struct VisualContextModel
{
    Vec4 color;
    int primitiveType;
    std::map< std::string, std::vector< TargetFloatType > > coordinates;
    std::map< std::string, Vec2< TargetFloatType > > boundingBox;
};

}

#endif // VISUALCONTEXTMODEL

