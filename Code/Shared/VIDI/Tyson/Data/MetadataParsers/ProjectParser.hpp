

#ifndef PROJECTCONFIG_HPP
#define PROJECTCONFIG_HPP

#include "Types/Vec.hpp"
#include "Data/Importers/MetaData.hpp"

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

#include <set>
#include <vector>
#include <cstdint>
#include <string>

namespace TN
{

inline bool parseProjectFile( const std::string & filePath, ProjectMetaData & result )
{
    QFile loadFile( filePath.c_str() );

    if ( ! loadFile.open( QIODevice::ReadOnly ) )
    {
        //qDebug() << "couldn't open the file";
        return false;
    }

    //qDebug() << "file loaded";

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc( QJsonDocument::fromJson( saveData ) );

    //qDebug() << loadDoc.object();

    std::string dataFormat = loadDoc.object()[ "dataset" ].toObject()[ "format" ].toString().toStdString();
    result.dataset.format = dataFormat;
    result.dataset.filePath = loadDoc.object()[ "dataset" ].toObject()[ "filePath" ].toString().toStdString();

    //qDebug() << dataFormat.c_str();

    return true;
}

}

#endif // PROJECTCONFIG_HPP

