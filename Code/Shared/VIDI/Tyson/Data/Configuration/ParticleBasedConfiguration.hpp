#ifndef XGCCONFIGURATION_HPP
#define XGCCONFIGURATION_HPP

#include "BaseConfiguration.hpp"

#include <QFile>
#include <QJsonDocument>

namespace TN
{

struct ParticleBasedConfiguration : public BaseConfiguration
{
    std::map< std::string, std::map< std::string, HistogramDefinition > >
    m_userDefinedHistograms;

    std::map< std::string, std::map< std::string, PhasePlotDefinition > >
    m_phasePlotDefinitions;

    std::map< std::string, std::map< std::string, TimePlotDefinition > >
    m_timePlotDefinitions;

    std::map< std::string, std::map< std::string, ParticleFilter > >
    m_particleFilters;

    std::map< std::string, std::map< std::string, DerivedVariable > > m_derivedVariables;
    std::map< std::string, std::map< std::string, BaseVariable > > m_baseVariables;

    std::string m_selectedFilter;
    std::string m_selectedTimePlot;
    std::string m_selectedPhasePlot;


    void setParticles( const std::set< std::string > & ptypes )
    {
        m_particleTypes = ptypes;

        for( auto & p : ptypes )
        {
            m_userDefinedHistograms.insert( { p, std::map< std::string, HistogramDefinition >() } );
            m_phasePlotDefinitions.insert( { p, std::map< std::string, PhasePlotDefinition >() } );
            m_timePlotDefinitions.insert( { p, std::map< std::string, TimePlotDefinition >() } );
            m_timeSeriesDefinitions.insert( { p, std::map< std::string, TimeSeriesDefinition >() } );
            m_particleFilters.insert( { p, std::map< std::string, ParticleFilter >() } );
            m_derivedVariables.insert( { p, std::map< std::string, DerivedVariable >() } );
            m_baseVariables.insert( { p, std::map< std::string, BaseVariable >() } );
            m_spacePartitioning.insert( { p, SpacePartitioningDefinition() } );
        }
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj = BaseConfiguration::toJSON();

        ////qDebug() << "called BaseConfig::toJSON";

        QJsonObject histObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_userDefinedHistograms.find( p );
            if( it != m_userDefinedHistograms.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            histObj.insert( p.c_str(), ar );
        }
        obj.insert( "histograms", histObj );

        /////

        ////qDebug() << "added hist definitions";

        QJsonObject phasPlotObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_phasePlotDefinitions.find( p );
            if( it != m_phasePlotDefinitions.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            phasPlotObj.insert( p.c_str(), ar );
        }
        obj.insert( "phasePlots", phasPlotObj );


        ////qDebug() << "added phase plot definitions";

        /////

        QJsonObject timePlotObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_timePlotDefinitions.find( p );
            if( it != m_timePlotDefinitions.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            timePlotObj.insert( p.c_str(), ar );
        }
        obj.insert( "timePlots", timePlotObj );

        ////qDebug() << "added time plot visualizations";

        /////

        QJsonObject filterObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_particleFilters.find( p );
            if( it != m_particleFilters.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            filterObj.insert( p.c_str(), ar );
        }
        obj.insert( "filters", filterObj );

        ////qDebug() << "added filter definitions";

        //////

        QJsonObject derivedVarObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_derivedVariables.find( p );
            if( it != m_derivedVariables.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            derivedVarObj.insert( p.c_str(), ar );
        }
        obj.insert( "derivedVariables", derivedVarObj );

        ////qDebug() << "added derived variables";

        QJsonObject baseVarObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_baseVariables.find( p );
            if( it != m_baseVariables.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            baseVarObj.insert( p.c_str(), ar );
        }
        obj.insert( "baseVariables", baseVarObj );

        ////qDebug() << "added base variables";

        obj.insert( "selectedTimePlot", m_selectedTimePlot.c_str() );
        obj.insert( "selectedPhasePlot", m_selectedPhasePlot.c_str() );
        obj.insert( "selectedFilter", m_selectedFilter.c_str() );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        BaseConfiguration::fromJSON( obj );

        ////qDebug() << "called Base from json";

        m_userDefinedHistograms.clear();
        m_phasePlotDefinitions.clear();
        m_timePlotDefinitions.clear();
        m_particleFilters.clear();
        m_derivedVariables.clear();

        for( auto p : m_particleTypes )
        {
            m_userDefinedHistograms.insert( { p, std::map< std::string, HistogramDefinition >() } );
            foreach ( QJsonValue tsv, obj[ "histograms" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                HistogramDefinition tsd;
                tsd.fromJSON( tsObj );
                m_userDefinedHistograms.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }

            ////qDebug() << "loaded histograms for " << p.c_str();

            m_phasePlotDefinitions.insert( { p, std::map< std::string, PhasePlotDefinition >() } );
            foreach ( QJsonValue tsv, obj[ "phasePlots" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                PhasePlotDefinition tsd;
                tsd.fromJSON( tsObj );
                m_phasePlotDefinitions.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }

            ////qDebug() << "loaded phasePlots for " << p.c_str();

            m_timePlotDefinitions.insert( { p, std::map< std::string, TimePlotDefinition >() } );
            foreach ( QJsonValue tsv, obj[ "timePlots" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                TimePlotDefinition tsd;
                tsd.fromJSON( tsObj );
                m_timePlotDefinitions.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }

            ////qDebug() << "loaded timePlots for " << p.c_str();

            m_particleFilters.insert( { p, std::map< std::string, ParticleFilter >() } );
            foreach ( QJsonValue tsv, obj[ "filters" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                ParticleFilter tsd;
                ////qDebug() << "before filter from JSON " << p.c_str() << "\n" << tsObj;
                tsd.fromJSON( tsObj );
                ////qDebug() << "after filter from JSON, is p mapped?? " <<  m_particleFilters.count( p );
                m_particleFilters.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
                ////qDebug() << "after filter insert";
            }

            ////qDebug() << "loaded filters for " << p.c_str();

            m_baseVariables.insert( { p, std::map< std::string, BaseVariable >() } );
            foreach ( QJsonValue tsv, obj[ "baseVariables" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                BaseVariable tsd;
                tsd.fromJSON( tsObj );
                m_baseVariables.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }

            ////qDebug() << "loaded variables for " << p.c_str();

            m_derivedVariables.insert( { p, std::map< std::string, DerivedVariable >() } );
            foreach ( QJsonValue tsv, obj[ "derivedVariables" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                DerivedVariable tsd;
                tsd.fromJSON( tsObj );
                m_derivedVariables.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }

            ////qDebug() << "loaded derived variables for " << p.c_str();
        }

        m_selectedPhasePlot = obj[ "selectedPhasePlot" ].toString().toStdString();
        m_selectedTimePlot = obj[ "selectedTimePlot" ].toString().toStdString();
        m_selectedFilter = obj[ "selectedFilter" ].toString().toStdString();

        ////qDebug() << "loaded selections";
    }

    void clear()
    {
        BaseConfiguration::clear();

        m_userDefinedHistograms.clear();
        m_phasePlotDefinitions.clear();
        m_timePlotDefinitions.clear();
        m_particleFilters.clear();
        m_derivedVariables.clear();
    }

    ~ParticleBasedConfiguration() {}
};

}

#endif // XGCCONFIGURATION_HPP
