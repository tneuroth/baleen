#ifndef DATAINFO_HPP
#define DATAINFO_HPP

#include <string>
#include <vector>
#include <map>

struct BasicDataInfo
{
    std::string name;
    std::string description;
    std::string location;
    std::string type;
    std::string format;

    // particle type -> default spatial layout attributes ( at least 2 )
    std::map< std::string, std::vector< std::string > > defaultSpacePartitioningAttributes;

};

#endif // DATAINFO_HPP
