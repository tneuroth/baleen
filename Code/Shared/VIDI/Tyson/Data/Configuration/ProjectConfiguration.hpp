#ifndef PROJECTCONFIGURATION_HPP
#define PROJECTCONFIGURATION_HPP

#include "Data/Configuration/ParameterKeys.hpp"
#include "Data/Configuration/ParticleBasedConfiguration.hpp"
#include "Data/Configuration/DistributionBasedConfiguration.hpp"

#include "Algorithms/Standard/Util.hpp"

#include <boost/filesystem.hpp>
#include <QJsonObject>

#include <boost/filesystem.hpp>

namespace TN
{

struct ProjectConfiguration
{
    std::vector< QJsonObject > stateChangeStack;
    QJsonObject lastState;

    ParticleBasedConfiguration particleBasedConfiguration;
    DistributionBasedConfiguration distributionBasedConfiguration;

    std::string activeType;

    double scaledScreenResolutionX;
    double scaledScreenResolutionY;

    double uiOffsetA;
    double uiOffsetB;
    double uiOffsetC;
    double uiOffsetD;

    std::string histogramNormalizationMode;
    std::string phasePlotMode;
    std::string timePlotMode;
    std::string timePlotRegion;

    double histNormalizationThreshold;
    double histResolutionModifier;
    double spatialScalingModifier;
    double histOpacity;
    double bkgOpacity;
    double valueRangeModifierX;
    double valueRangeModifierY;
    bool lockValueRangeModifiers;
    double partitionModifer;
    double contourLevelModifier;
    std::string errorRemovalOption;
    double isoRotationY;
    double isoRotationX;
    double histHeightRotation;

    QJsonObject samplingCamera;

    QJsonObject toJSON() const
    {
        QJsonObject obj = activeType == "particle" ? particleBasedConfiguration.toJSON() : distributionBasedConfiguration.toJSON();

        ////qDebug() << "called  " << ( activeType == "particle" ? "particle" : "distribution" ) << " toJSON";

        obj.insert( "scaledScreenResolutionX", scaledScreenResolutionX );
        obj.insert( "scaledScreenResolutionY", scaledScreenResolutionY );

        obj.insert( "uiOffsetA", uiOffsetA );
        obj.insert( "uiOffsetB", uiOffsetB );
        obj.insert( "uiOffsetC", uiOffsetC );
        obj.insert( "uiOffsetD", uiOffsetD );

        obj.insert( "normalizationMode", histogramNormalizationMode.c_str() );
        obj.insert( "normalizationModifier", histNormalizationThreshold );
        obj.insert( "partitionModifer", partitionModifer );
        obj.insert( "spatialScalingModifier", spatialScalingModifier );
        obj.insert( "histOpacity", histOpacity );
        obj.insert( "bkgOpacity", bkgOpacity );
        obj.insert( "contourLevelModifer", contourLevelModifier );
        obj.insert( "isoRotationX", isoRotationX );
        obj.insert( "isoRotationY", isoRotationY );
        obj.insert( "samplingCamera", samplingCamera );
        obj.insert( "histHeightRotation", histHeightRotation );

        if( activeType == "particle" )
        {
            obj.insert( "phasePlotMode", phasePlotMode.c_str() );
            obj.insert( "timePlotMode", timePlotMode.c_str() );
            obj.insert( "timePlotRegion", timePlotRegion.c_str() );
            obj.insert( "resolutionModifier", histResolutionModifier );
            obj.insert( "valueRangeModifierX", valueRangeModifierX );
            obj.insert( "valueRangeModifierY", valueRangeModifierY );
            obj.insert( "lockValueRangeModifiers", lockValueRangeModifiers );
            obj.insert( "errorRemovalOption", errorRemovalOption.c_str() );
            obj.insert( "type", "particle" );
        }
        else
        {
            obj.insert( "type", "distribution" );
        }

        return obj;
    }

    std::map< std::string, std::map< std::string, TimeSeriesDefinition > > & timeSeriesDefinitions()
    {
        if( activeType == "particle" )
        {
            return particleBasedConfiguration.m_timeSeriesDefinitions;
        }
        else
        {
            return distributionBasedConfiguration.m_timeSeriesDefinitions;
        }
    }

    QJsonObject stateDiff( QJsonObject & obj1, QJsonObject & obj2 )
    {
        // for now just push the whole state, until I decide the implementation for computing and encoding the differnces
        return obj2;
    }

    void pushStateChange()
    {
        QJsonObject stateChangeObject;
        stateChangeObject.insert( "time", getTimeStamp().c_str() );

        QJsonObject currentState = toJSON();

        if( stateChangeStack.empty() )
        {
            stateChangeObject.insert( "state_change", toJSON() );
        }
        else
        {
            stateChangeObject.insert( "state_change", stateDiff( currentState, lastState ) );
        }

        stateChangeStack.push_back( stateChangeObject );
        lastState = currentState;
    }

    void writeLogs( const std::string & path )
    {
        QJsonObject log;
        log.insert( "logout_time", getTimeStamp().c_str() );

        QJsonArray stateChangesArr;
        for( auto & obj : stateChangeStack )
        {
            stateChangesArr.append( obj );
        }
        log.insert( "state_changes", stateChangesArr );

        QFile outFile( ( path ).c_str() );
        outFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QJsonDocument document( log );
        outFile.write( document.toJson() );
    }

    void write( bool asDefault, const std::string & basePath )
    {
        ////qDebug() << "writing project configuration";
        ////qDebug() << "activeType=" << activeType.c_str();

        std::string name = activeType == "particle" ? particleBasedConfiguration.m_name : distributionBasedConfiguration.m_name;
        std::string data = activeType == "particle" ? particleBasedConfiguration.m_datasetInfo.name : distributionBasedConfiguration.m_datasetInfo.name;

        std::string configDir = basePath + "/Configuration//DATA//" + data + "//config//";
        boost::filesystem::path dir( configDir );
        if( ! boost::filesystem::exists( dir ) )
        {
            if (! boost::filesystem::create_directory( dir ) )
            {
                qDebug() << "config directory couldn't be created" << "\n";
            }
        }

        QString path = ( basePath + "/Configuration//DATA//" + data + "//config//" + name + ".json" ).c_str();

//        ////qDebug() << activeType.c_str() << " " << name.c_str();
//        ////qDebug() << particleBasedConfiguration.m_name.c_str() << " " << distributionBasedConfiguration.m_name.c_str();

        QFile outFile( path );

        ////qDebug() << "calling ProjectConfiguration::toJSON()";
        ////qDebug() << toJSON();
        ////qDebug() << "writing to path " << path;

        outFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QJsonDocument document( toJSON() );
        outFile.write( document.toJson() );

        if( asDefault )
        {
            ////qDebug() << "writing as default as well";

            QFile defFile( ( basePath + "//Configuration//DATA//" + data + "//default.json" ).c_str() );

            QString val;
            defFile.open( QIODevice::ReadOnly | QIODevice::Text );
            val = defFile.readAll();
            defFile.close();

            //////qDebug() << val;
            QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject obj = d.object();
            obj[ "default" ] = name.c_str();

            QFile defFileOut( ( basePath + "/Configuration//DATA//" + data + "//default.json" ).c_str() );
            defFileOut.open( QIODevice::WriteOnly | QIODevice::Text );

            defFileOut.write( QJsonDocument( obj ).toJson() );
        }
    }

    void fromJSON( const QJsonObject & obj )
    {
        activeType = obj[ "type" ].toString().toStdString();

//        ////qDebug() << "type is " << obj[ "type" ].toString().toStdString().c_str();

        if( activeType == "particle" )
        {
            particleBasedConfiguration.fromJSON( obj );
            ////qDebug() << "after Particle based from json";
        }
        else
        {
            distributionBasedConfiguration.fromJSON( obj );
            ////qDebug() << "after distribution based from json";
        }

        ////qDebug() << "after type dependent stuff";

        histogramNormalizationMode = obj[ "normalizationMode" ].toString().toStdString();
        histNormalizationThreshold = obj[ "normalizationModifier" ].toDouble();
        partitionModifer = obj[ "partitionModifer" ].toDouble();
        spatialScalingModifier = obj[ "spatialScalingModifier" ].toDouble();
        histOpacity = obj[ "histOpacity" ].toDouble();
        bkgOpacity = obj[ "bkgOpacity" ].toDouble();
        contourLevelModifier = obj[ "contourLevelModifer" ].toDouble();
        isoRotationX = obj[ "isoRotationX" ].toDouble();
        isoRotationY = obj[ "isoRotationY" ].toDouble();
        samplingCamera = obj[ "samplingCamera" ].toObject();
        histHeightRotation = obj[ "histHeightRotation" ].toDouble();

        scaledScreenResolutionX = obj[ "scaledScreenResolutionX" ].toDouble();
        scaledScreenResolutionY = obj[ "scaledScreenResolutionY" ].toDouble();

        uiOffsetA = obj[ "uiOffsetA" ].toDouble();
        uiOffsetB = obj[ "uiOffsetB" ].toDouble();
        uiOffsetC = obj[ "uiOffsetC" ].toDouble();
        uiOffsetD = obj[ "uiOffsetD" ].toDouble();

        if( activeType == "particle" )
        {
            phasePlotMode = obj[ "phasePlotMode" ].toString().toStdString();
            timePlotMode = obj[ "timePlotMode" ].toString().toStdString();
            timePlotRegion = obj[ "timePlotRegion" ].toString().toStdString();
            histResolutionModifier = obj[ "resolutionModifier" ].toDouble();
            valueRangeModifierX = obj[ "valueRangeModifierX" ].toDouble();
            valueRangeModifierY = obj[ "valueRangeModifierY" ].toDouble();
            lockValueRangeModifiers = obj[ "lockValueRangeModifiers" ].toBool();
            phasePlotMode = obj[ "phasePlotMode" ].toString().toStdString();
            errorRemovalOption = obj[ "errorRemovalOption" ].toString().toStdString();
        }
    }

    void fromFile( const std::string & path )
    {
        QFile loadFile( path.c_str() );

        if ( ! loadFile.open( QIODevice::ReadOnly ) )
        {
            ////qDebug() << "couldn't open the file: " << path.c_str();
        }

        QByteArray saveData = loadFile.readAll();
        QJsonDocument loadDoc( QJsonDocument::fromJson( saveData ) );
        fromJSON( loadDoc.object() );
    }
};

}

#endif // PROJECTCONFIGURATION_HPP
