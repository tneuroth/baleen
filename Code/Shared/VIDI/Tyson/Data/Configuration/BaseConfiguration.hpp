

#ifndef TN_CONFIGURATION
#define TN_CONFIGURATION

#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Definitions/ParticleFilter.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"

#include <QJsonObject>
#include <QJsonArray>

#include <set>
#include <map>
#include <string>

namespace TN
{
struct BaseConfiguration
{
    std::string m_name;
    std::string m_author;
    std::string m_description;
    std::string m_userDataDescription;

    BasicDataInfo m_datasetInfo;

    std::set< std::string > m_particleTypes;

    std::map< std::string, std::map< std::string, TimeSeriesDefinition > >
    m_timeSeriesDefinitions;

    std::string m_selectedTimeSeries;
    std::string m_selectedParticleType;
    int m_selectedTimeStep;
    std::string m_selectedHistogram;

    std::map< std::string, CustomConstant > m_customConstants;
    std::map< std::string, BaseConstant > m_baseConstants;

    // map by particle type, since it cannot be assumed each particle type has the same
    // set of attributes
    std::map< std::string, SpacePartitioningDefinition > m_spacePartitioning;

    QJsonObject toJSON() const
    {
        QJsonObject obj;

        obj.insert( "name", m_name.c_str() );
        obj.insert( "author", m_author.c_str() );
        obj.insert( "description", m_description.c_str() );
        obj.insert( "selectedTimeSeries", m_selectedTimeSeries.c_str() );
        obj.insert( "selectedParticleType", m_selectedParticleType.c_str() );
        obj.insert( "selectedDistribution", m_selectedHistogram.c_str() );
        obj.insert( "selectedTimeStep", m_selectedTimeStep );
        obj.insert( "userDataDescription", m_userDataDescription.c_str() );

        QJsonArray baseConstArray;
        QJsonArray custConstArray;
        QJsonArray partTypeArray;

        for( auto c : m_baseConstants )
        {
            baseConstArray.append( c.second.toJSON() );
        }
        obj.insert( "baseConstants", baseConstArray );

        ////qDebug() << "added base constants";

        for( auto c : m_customConstants )
        {
            custConstArray.append( c.second.toJSON() );
        }

        obj.insert( "customConstants", custConstArray );

        ////qDebug() << "added custom constants";

        for( auto p : m_particleTypes )
        {
            ////qDebug() << "inserted " << p.c_str() << " as a particle type";
            partTypeArray.append( p.c_str() );
        }

        obj.insert( "particles", partTypeArray );

        ////qDebug() << "added particle types";

        QJsonObject tsObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_timeSeriesDefinitions.find( p );
            if( it != m_timeSeriesDefinitions.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            tsObj.insert( p.c_str(), ar );
        }

        obj.insert( "timeSeries", tsObj );

        ////qDebug() << "added time series";

        QJsonObject partitioningObj;
        for( auto p : m_spacePartitioning )
        {
            partitioningObj.insert( p.first.c_str(), p.second.toJSON() );
        }
        obj.insert( "spatialPartitioning", partitioningObj );

        ////qDebug() << "added spatial partitioning";

        return obj;
    }

    void clear()
    {
        m_particleTypes.clear();
        m_timeSeriesDefinitions.clear();

        m_name = "";
        m_author = "";
        m_description = "";

        m_selectedTimeSeries = "";
        m_selectedParticleType = "";

        m_spacePartitioning.clear();
        m_customConstants.clear();
    }

    void fromJSON( const QJsonObject & obj )
    {
        m_name = obj[ "name" ].toString().toStdString();
        m_author = obj[ "author" ].toString().toStdString();
        m_description = obj[ "description" ].toString().toStdString();
        m_userDataDescription = obj[ "userDataDescription" ].toString().toStdString();
        m_selectedTimeSeries = obj[ "selectedTimeSeries" ].toString().toStdString();
        m_selectedParticleType = obj[ "selectedParticleType" ].toString().toStdString();
        m_selectedHistogram = obj[ "selectedDistribution" ].toString().toStdString();
        m_selectedTimeStep = obj[ "selectedTimeStep" ].toInt();

        m_baseConstants.clear();
        foreach ( QJsonValue bcv, obj[ "baseConstants" ].toArray() )
        {
            QJsonObject bcObj = bcv.toObject();
            BaseConstant baseC;
            baseC.fromJSON( bcObj );
            m_baseConstants.insert( { bcObj[ "name" ].toString().toStdString(), baseC } );
        }

        m_customConstants.clear();
        foreach ( QJsonValue ccv, obj[ "customConstants" ].toArray() )
        {
            QJsonObject ccObj = ccv.toObject();
            CustomConstant custC;
            custC.fromJSON( ccObj );
            m_customConstants.insert( { ccObj[ "name" ].toString().toStdString(), custC } );
        }

        m_particleTypes.clear();
        foreach ( QJsonValue ccv, obj[ "particles" ].toArray() )
        {
            m_particleTypes.insert( ccv.toString().toStdString() );
        }

        m_timeSeriesDefinitions.clear();
        for( auto p : m_particleTypes )
        {
            m_timeSeriesDefinitions.insert( { p, std::map< std::string, TimeSeriesDefinition >() } );
            foreach ( QJsonValue tsv, obj[ "timeSeries" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                TimeSeriesDefinition tsd;
                tsd.fromJSON( tsObj );
                m_timeSeriesDefinitions.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }
        }

        m_spacePartitioning.clear();
        for( auto p : m_particleTypes )
        {
            SpacePartitioningDefinition pt;
            pt.fromJSON( obj[ "spatialPartitioning" ].toObject()[ p.c_str() ].toObject() );
            m_spacePartitioning.insert( { p, pt } );
        }
    }

    virtual ~BaseConfiguration() {}
};
}

#endif // TN_CONFIGURATION

