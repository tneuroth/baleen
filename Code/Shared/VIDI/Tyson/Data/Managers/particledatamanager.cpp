

#include "Types/Vec.hpp"
#include "Data/Importers/ParticleDataImporter.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/Expression.hpp"
#include "Data/Managers/ParticleDataManager.hpp"

#include <QDebug>
#include <omp.h>

#include <chrono>
#include <vector>
#include <map>
#include <cstdint>
#include <memory>

namespace TN
{

template < typename TargetFloatType >
ParticleDataManager< TargetFloatType >::ParticleDataManager() {}

template < typename TargetFloatType >
ParticleDataManager< TargetFloatType >::~ParticleDataManager() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// memory release
template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::release(
    const std::vector< std::int32_t > & iSteps, const std::map< std::string, std::set< std::string > > & toRelease )
{
    for( auto i : iSteps )
    {
        for( const auto & partType : toRelease )
        {
            for( const auto & attr : partType.second )
            {
                if( m_particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ i ] != nullptr )
                {
                    m_particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ i ].reset( nullptr );
                }
            }
        }
    }
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::release( const std::vector< std::int32_t > & iSteps )
{
    release( iSteps, m_storedVariables );
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::release( const std::string & ptype )
{
    std::map< std::string, std::set< std::string > > toRelease;
    for( auto & e : m_particleAndTimeVariantAttributes )
    {
        if( e.first == ptype )
        {
            toRelease.insert( { e.first, {} } );
            for( auto & ve : e.second )
            {
                toRelease.find( e.first )->second.insert( ve.first );
            }
        }
    }

    std::vector< std::int32_t > iSteps( m_numTimeSteps );
    for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
    {
        iSteps[ i ] = i;
    }
    release( iSteps, toRelease );
}

template < typename TargetFloatType >
std::set< std::string > ParticleDataManager< TargetFloatType >::attributeKeys( const std::string & ptype ) const
{
    std::set< std::string > keys;
    if( m_particleAndTimeVariantAttributes.count( ptype )  )
    {
        for( auto & p : m_particleAndTimeVariantAttributes.at( ptype ) )
        {
            keys.insert( p.first );
        }
    }
    return keys;
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::release()
{
    std::vector< std::int32_t > iSteps( m_numTimeSteps );
    for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
    {
        iSteps[ i ] = i;
    }
    release( iSteps );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// initialization

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::reset( const BasicDataInfo & basicDataInfo, ParticleDataImporter< TargetFloatType > * dataImporter )
{
    //////////qDebug() << "data particle data manager was reset";

    m_particleAndTimeVariantAttributes.clear();
    m_variantAttributeRanges.clear();
    m_particleVariantAttributes.clear();

    m_baseConstants.clear();
    m_customConstants.clear();
    m_derivedVariables.clear();
    m_baseVariables.clear();

    m_storedVariables.clear();
    m_storedVariableErrorState.clear();

    m_visualContextModels.clear();
    m_simulationTimeSteps.clear();
    m_realTimeValues.clear();
    m_filtered.clear();
    m_filterIndices.clear();
    m_filters.clear();

    m_dataImporter = dataImporter;

    //////////qDebug() << "calling data importer init";
    m_dataImporter->init( basicDataInfo );

    // TODO: load metadata, such as derivations, descriptions, custom constants, etc.
    m_dataImporter->loadTemporallyStaticAttributes(
        m_particleAndTimeVariantAttributes,
        m_variantAttributeRanges,
        m_particleVariantAttributes,
        m_baseConstants,
        m_derivedVariables,
        m_visualContextModels,
        m_lookUpTextures,
        m_simulationTimeSteps,
        m_realTimeValues,
        m_numTimeSteps,
        m_numParticles
    );

    for( auto & ptype : m_particleAndTimeVariantAttributes )
    {
        m_particleTypes.insert( ptype.first );

        m_derivedVariables.insert( { ptype.first, std::map< std::string, DerivedVariable >() } );
        m_baseVariables.insert( { ptype.first, std::map< std::string, BaseVariable >() } );

        m_filtered.insert( { ptype.first, std::vector< std::uint8_t >( m_numParticles.at( ptype.first ), 0 ) } );
        m_filterIndices.insert( { ptype.first, std::vector< std::uint64_t >() } );

        m_filters.insert( { ptype.first, ParticleFilter() } );
        m_storedVariables.insert( { ptype.first, {} } );
        m_storedVariableErrorState.insert( { ptype.first, {} } );

        for( auto & attr : ptype.second )
        {
            BaseVariable bv;
            bv.name( attr.first );
            m_baseVariables.find( ptype.first )->second.insert( { attr.first, bv } );
            m_baseVariables.find( ptype.first )->second.find( attr.first )->second.range( m_variantAttributeRanges.find( ptype.first )->second.find( attr.first )->second );
        }
    }

    m_fullTimeSeries.description = "All";
    m_fullTimeSeries.firstIdx = 0;
    m_fullTimeSeries.lastIdx = m_numTimeSteps - 1;
    m_fullTimeSeries.idxStride = 1;
    m_fullTimeSeries.integrate = false;

    m_currentTimeSeries = m_fullTimeSeries;
    m_errorRemovalOption = 0;
    m_errorIndicesAreRemoved = false;
    m_errorFilterIndices.clear();
    m_hasZeros = false;
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::applyErrorFilter( int step, const std::string & ptype, const std::string & var )
{
    // full data at this location -> filtered data
    std::unique_ptr< std::vector< TargetFloatType > > & data = m_particleAndTimeVariantAttributes.at( ptype ).at( var )[ step ];

    // shouldn't happen, maybe should be exception?
    if( data == nullptr )
    {
        //////////qDebug() << "error, data is null";
        return;
    }

    for( size_t i = 0, end = m_errorFilterIndices.size(); i < end; ++i )
    {
        (*data)[ i ] = (*data)[ m_errorFilterIndices[ i ] ];
    }

    (*data ).resize( m_errorFilterIndices.size() );
    (*data ).shrink_to_fit();
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::applyFilter( int step, const std::string & ptype, const std::string & var )
{
    // full data at this location -> filtered data

    //////////qDebug() << m_particleAndTimeVariantAttributes.at( ptype ).count( var );
    std::unique_ptr< std::vector< TargetFloatType > > & data = m_particleAndTimeVariantAttributes.at( ptype ).at( var )[ step ];

    // shouldn't happen, maybe should be exception?
    if( data == nullptr )
    {
        //////////qDebug() << "error, data is null";
        return;
    }

    std::vector< uint64_t > & indices = m_filterIndices.at( ptype );

    for( size_t i = 0, end = indices.size(); i < end; ++i )
    {
        (*data)[ i ] = (*data)[ indices[ i ] ];
    }
    (*data ).resize( indices.size() );
    (*data ).shrink_to_fit();
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::computeFilter(
    const std::string & ptype,
    const std::set< std::string > & toRetain,
    std::atomic< int > & filterProgress )
{
    const ParticleFilter & filter = m_filters.at( ptype );
    const std::string expr = m_expressionCalculator.parser().expandExpression(
                                 filter.expression,
                                 m_baseConstants,
                                 m_customConstants,
                                 m_baseVariables.find( ptype )->second,
                                 m_derivedVariables.find( ptype )->second );

    if( expr == "" )
    {
        m_filtered.find( ptype )->second = std::vector< std::uint8_t >( m_numParticles.at( ptype ), 0 );
        std::vector< uint64_t > & indices = m_filterIndices.at( ptype );
        indices.clear();
        indices.reserve( m_numParticles.at( ptype ) );
        for( std::int32_t i = 0; i < m_numParticles.at( ptype ); ++i )
        {
            indices.push_back( i );
        }

        return;
    }

    //////////qDebug() << "computing filter " << expr.c_str();

    //////////////////////////////////////////////////////////
    //
    // Load variables needed for computing the expression
    //
    //////////////////////////////////////////////////////////

    std::vector< std::string > identifiers = m_expressionCalculator.parser().getExpressionIdentifiers( expr );

    const int WINDOW_START  = filter.window.firstIdx;
    const int WINDOW_LAST   = filter.window.lastIdx;
    const int WINDOW_STRIDE = filter.window.idxStride;

    //////////qDebug() << "loading filter data " << WINDOW_START << " " << WINDOW_LAST << " " << WINDOW_STRIDE;

    int NSTEPS = filter.window.numSteps();
    int TOTAL = NSTEPS * 3;

    for( std::int32_t i = WINDOW_START, c = 0; i <= WINDOW_LAST; i += WINDOW_STRIDE )
    {
        std::map< std::string, std::set< std::string > > needToLoad;
        bool everythingAleadyInMemory = true;

        // for each variable needed
        for( const auto & attr :identifiers )
        {
            // and if this variable is not currently in memory for this time step
            if( m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second[ i ] == nullptr )
            {
                // if not done yet, add the particle->variable set element to the map
                if( needToLoad.find( ptype ) == needToLoad.end() )
                {
                    needToLoad.insert( std::pair< std::string, std::set< std::string > >( ptype, std::set< std::string >( ) ) );
                }
                // add the variable
                needToLoad.find( ptype )->second.insert( attr );
                everythingAleadyInMemory = false;
            }
        }

        // if things need to be loaded at this time step, then load them
        if( ! everythingAleadyInMemory )
        {
            m_dataImporter->loadTimeStep(
                i,
                needToLoad,
                m_particleAndTimeVariantAttributes
            );
        }
        ++c;
        filterProgress = ( c * 100 ) / TOTAL;
    }

    //////////qDebug() << "calculating flags";

    //////////////////////////////////////////////////////
    //
    // now calculate the filter flags
    //
    //////////////////////////////////////////////////////

    m_filtered.find( ptype )->second = std::vector< std::uint8_t >( m_numParticles.at( ptype ), 0 );

    for( std::int32_t i = WINDOW_START, c = 0; i <= WINDOW_LAST; i += WINDOW_STRIDE )
    {
        std::map< std::string, TargetFloatType* > variablePointers;

        // the expanded expression should consist only of base variables, numbers, operators, keywords, ...
        // constants have been replaced with numbers and derived variables have been replaced with expression of base variables
        for( auto & identifier : identifiers )
        {
            variablePointers.insert( { identifier, nullptr } );
        }

        // set variables pointer to point to this time steps worth of data
        for( auto & v : variablePointers )
        {
            v.second = ( m_particleAndTimeVariantAttributes.find( ptype )->second.find( v.first )->second[ i ] )->data();
        }

        m_expressionCalculator.parser().updateStaticFilter(
            expr,
            variablePointers,
            m_filtered.find( ptype )->second );

        filterProgress = ( c * 100 + NSTEPS ) / TOTAL;
    }

    //////////qDebug() << "building indices base variables ";

    std::vector< uint8_t > & flags = m_filtered.at( ptype );
    std::vector< uint64_t > & indices = m_filterIndices.at( ptype );

    //////////qDebug() << "flags : " << flags.size();

    indices.clear();
    for( uint64_t i = 0, end = flags.size(); i < end; ++i )
    {
        if( ! flags[ i ] )
        {
            indices.push_back( i );
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    //                   now release memory as appropriate
    //
    ///////////////////////////////////////////////////////////////////////////

    //////////qDebug() << "releasing memory";

    const int START = m_currentTimeSeries.firstIdx;
    const int LAST = m_currentTimeSeries.lastIdx;
    const int STRIDE = m_currentTimeSeries.idxStride;

    std::vector< std::int32_t > toRelease;
    toRelease.reserve( m_numTimeSteps );
    for( std::int32_t t = WINDOW_START; t <= WINDOW_LAST; t += WINDOW_STRIDE )
    {
        // if it's not something we will want to load
        if( ( t < START ) // out of range
                || ( t > LAST  )  // out of range
                || ( ( ( t - START ) % STRIDE ) != 0 ) ) // not on desired interval
        {
            toRelease.push_back( t );
        }
    }
    release( toRelease );

    for( std::int32_t i = START, c = 0; i <= LAST; i += STRIDE )
    {
        for( auto & id : identifiers )
        {
            // if we are not wanting to retain it (either we will not need it anymore, or we want to limit what's in memory at a single time)
            if( toRetain.find( id ) == toRetain.end() )
            {
                // and it's in memory
                if( m_particleAndTimeVariantAttributes.find( ptype )->second.find( id )->second[ i ] != nullptr )
                {
                    // then delete it
                    m_particleAndTimeVariantAttributes.find( ptype )->second.find( id )->second[ i ].reset( nullptr );
                }
            }
            else
            {
                applyFilter( i, ptype, id );
            }
        }
        filterProgress = ( c * 100 + NSTEPS * 2 ) / TOTAL;
    }

    //////////qDebug() << "done";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::load(
    const std::string & ptype,
    const std::set< std::string > & variables,
    const TimeSeriesDefinition & timeSeries,
    bool invalidateMemory,
    bool updateFilter,
    std::atomic< int > & filterProgress,
    std::atomic< int > & derivationPrepareProgress,
    std::atomic< int > & derivationCalculateProgress,
    std::atomic< int > & baseProgress,
    std::atomic< int > & NaNCheckProgress,
    std::atomic< int > & InfCheckProgress,
    std::atomic< int > & ZeroCheckProgress,
    std::atomic< int > & ErrorRemoveProgress,
    std::atomic< int > & rangeProgress,
    std::atomic< bool > & status )
{

    //////////qDebug() << "loading";

    const int NSTEPS = timeSeries.numSteps();

    // update the timeSeries we are keeping in memory
    m_currentTimeSeries = timeSeries;
    const int START = m_currentTimeSeries.firstIdx;
    const int LAST = m_currentTimeSeries.lastIdx;
    const int STRIDE = m_currentTimeSeries.idxStride;

    //////////qDebug() << "there are " << timeSeries.numSteps() << " steps to load";

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    //  Release everything we know we don't need                            //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    updateDerivations( ptype, m_derivedVariables.at( ptype ) );

    //////////qDebug() << m_filters.at( ptype ).name.c_str();

    if( invalidateMemory || m_errorIndicesAreRemoved )
    {
        //////////qDebug() << "invalidating memory";
        release();
        m_errorIndicesAreRemoved = false;
    }
    else
    {
        // release all data for time steps not in the current time series
        std::vector< std::int32_t > toRelease;
        toRelease.reserve( m_numTimeSteps );
        for( std::int32_t t = 0; t < m_numTimeSteps; ++t )
        {
            // if it's not something we will want to load
            if( ( t < START )  // out of range
                    || ( t > LAST  )  // out of range
                    || ( ( ( t - START ) % STRIDE ) != 0 ) ) // not on desired interval
            {
                toRelease.push_back( t );
            }
        }
        release( toRelease );

        // Release all data from other particle types
        for( auto & e : m_particleAndTimeVariantAttributes )
        {
            if( e.first != ptype )
            {
                release( e.first );
                m_storedVariables.find( ptype )->second.clear();
                m_storedVariableErrorState.find( ptype )->second.clear();
            }
        }
    }

    ////////qDebug() << "initial memory release";

    /////////////////////////////////////////////////////////////////////////////
    //                                                                         //
    //  Get Variables Needed to Compute Derivations                            //
    //                                                                         //
    /////////////////////////////////////////////////////////////////////////////

    // first get based variables needed for derivations
    std::set< std::string > varsNeededForDerivation;
    for( const auto & attr : variables )
    {
        // if it's a derived variable
        const auto & dvIt = m_derivedVariables.find( ptype )->second.find( attr );
        if( dvIt != m_derivedVariables.find( ptype )->second.end() )
        {
            const DerivedVariable & dv = dvIt->second;

            std::string expandedExpression = m_expressionCalculator.parser().expandExpression(
                                                 dv.expression(),
                                                 m_baseConstants,
                                                 m_customConstants,
                                                 m_baseVariables.find( ptype )->second,
                                                 m_derivedVariables.find( ptype )->second );

            // TODO : test this function

            std::vector< std::string > identifiers = m_expressionCalculator.parser().getExpressionIdentifiers( expandedExpression );

            // the expanded expression should consist only of base variables, numbers, operators, keywords, ...
            // constants have been replaced with numbers and derived variables have been replaced with expression of base variables
            for( auto & identifier : identifiers )
            {
                varsNeededForDerivation.insert( identifier );
            }
        }
    }

    ////////qDebug() << "variables needed for derivation";

    ////////////////////////////////////////////////////////////////////////////////
    //
    //  Compute the filtering results, retaining what we will need for later
    //                                 if they happen to also be needed to filter
    //                                 May want to reconsider what is retained and
    //                                 limit what is in memory at a time for the
    //                                 processing phase of memory management
    //                                 For example, to derive variables or to apply
    //                                 the filter, we may need temporary access to
    //                                 several variables that are not 'active'
    //                                 To maximize memory utualization we should
    //                                 applying such processing one time step at
    //                                 a time and clearing temporary data at each
    //                                 step, which would also however be much slower
    //
    //                                 For now, just try to make it fast by
    //                                 retaining what we will need for all
    //                                 subsequent steps if it happens to be
    //                                 needed for a prior step. Later there
    //                                 could be options with tradeoffs.
    //
    //                                 NOTE: what has been retained after filtParticles
    //                                 will be in filtered form...
    //
    ////////////////////////////////////////////////////////////////////////////////

    if( ! m_filters.count( ptype ) )
    {
        ////////qDebug() << "didn't find filter map";
    }

    if( updateFilter && m_filters.at( ptype ).name != "None" )
    {
        ////////qDebug() << "updating filter " << m_filters.at( ptype ).expression.c_str();

        std::set< std::string > toRetain = variables;
        for( auto & v : varsNeededForDerivation )
        {
            toRetain.insert( v );
        }
        computeFilter( ptype, toRetain, filterProgress );
    }

    filterProgress = 100;

    ////////qDebug() << "updated filter";
    ////////qDebug() << m_filterIndices.at( ptype ).size();

    // NOTE: anything in memory at this point is now in filtered form

    /////////////////////////////////////////////////////////////////////////////
    //
    // now load those base variables needed to derive others
    //
    /////////////////////////////////////////////////////////////////////////////

    for( std::int32_t i = START, c = 0; i <= LAST; i += STRIDE )
    {
        std::map< std::string, std::set< std::string > > needToLoad;
        bool everythingAleadyInMemory = true;

        // for each variable needed for a derivation
        for( const auto & attr : varsNeededForDerivation )
        {
            ////////qDebug() << "checking attr " << attr.c_str() << " it is mapped?? " << m_particleAndTimeVariantAttributes.find( ptype )->second.count( attr );

            if( m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second[ i ] == nullptr )
            {
                if( needToLoad.find( ptype ) == needToLoad.end() )
                {
                    needToLoad.insert( std::pair< std::string, std::set< std::string > >( ptype, std::set< std::string >( ) ) );
                }
                // add the variable
                needToLoad.find( ptype )->second.insert( attr );
                everythingAleadyInMemory = false;
            }
            // if it's already in memory, we still need to find out if it has already been filtered due to being retained after being loaded for processing the filter
            // and the filter having then been already applied to it
            else if ( m_filters.at( ptype ).name != "None" && m_filterIndices.at( ptype ).size() != m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second[ i ]->size() )
            {
                applyFilter( i, ptype, attr );
            }
        }

        ////////qDebug() << "extracted variables needed, now loading " << i;

        // if things need to be loaded at this time step, then load them, and then filter if necessary
        if( ! everythingAleadyInMemory )
        {
            m_dataImporter->loadTimeStep(
                i,
                needToLoad,
                m_particleAndTimeVariantAttributes
            );

            if( m_filters.at( ptype ).name != "None" )
            {
                ////////qDebug() << "now applying filter";
                for( auto & var : needToLoad.at( ptype ) )
                {
                    applyFilter( i, ptype, var );
                }
            }
        }
        ++c;
        derivationPrepareProgress = ( c * 100 ) / NSTEPS;
    }
    derivationPrepareProgress = 100;

    ////////qDebug() << "loaded base variables needed for derivation";

    // now process and store derived variables
    // Note that for now, the derived variable is recomputed regardless of whether
    // it has previously been computed and is still in memory, however, in that case
    // it will not be reallocated
    // recomputing is necessary as the expression associated with the derived variable
    // name may be changed, although in the future, we can track whether the expression
    // has changed and only recompute as necessary

    // no filtering is necessary, because the filters have already been applied to the base
    // variables the derived variables will be calculated from
    for( const auto & attr : variables )
    {
        // if it's a derived variable
        const auto & dvIt = m_derivedVariables.find( ptype )->second.find( attr );
        if( dvIt != m_derivedVariables.find( ptype )->second.end() )
        {
            ////////////qDebug() << "deriving " << dvIt->first.c_str();

            const DerivedVariable & dv = dvIt->second;

            std::vector< std::unique_ptr< std::vector< TargetFloatType > > > & data
                = m_particleAndTimeVariantAttributes.find( ptype )->second.find( dv.name() )->second;

            std::string expandedExpression = m_expressionCalculator.parser().expandExpression(
                                                 dv.expression(),
                                                 m_baseConstants,
                                                 m_customConstants,
                                                 m_baseVariables.find( ptype )->second,
                                                 m_derivedVariables.find( ptype )->second );

            ////////qDebug() << expandedExpression.c_str();
            //////////qDebug() << numLoadedParticles( ptype );

            std::vector< std::string > identifiers = m_expressionCalculator.parser().getExpressionIdentifiers( expandedExpression );
            ////////qDebug() << "filter is " << m_filters.at( ptype ).name.c_str();

            ////////qDebug() << "now processing and storing " << m_filterIndices.at( ptype ).size();

            std::atomic< int > progress( 0 );

            #pragma omp parallel for
            for( int t = START; t <= LAST; t += STRIDE )
            {
                if( data[ t ] == nullptr || updateFilter )
                {
                    //numParticles( ptype ) gives number of particles after filtering
                    data[ t ].reset( new std::vector< TargetFloatType >( numLoadedParticles( ptype ) ) );
                }
                else if ( m_filters.at( ptype ).name != "None" && m_filterIndices.at( ptype ).size() != data[ t ]->size() )
                {
                    applyFilter( t, ptype, dv.name() );
                    continue;
                }

                std::map< std::string, TargetFloatType* > variablePointers;

                // the expanded expression should consist only of base variables, numbers, operators, keywords, ...
                // constants have been replaced with numbers and derived variables have been replaced with expression of base variables
                for( auto & identifier : identifiers )
                {
                    variablePointers.insert( { identifier, nullptr } );
                }

                // set variables pointer to point to this time steps worth of data
                for( auto & v : variablePointers )
                {
                    ////////////qDebug() << "adding pointer to " << v.first.c_str();
                    v.second = ( m_particleAndTimeVariantAttributes.find( ptype )->second.find( v.first )->second[ t ] )->data();
                }

                ////////////qDebug() << "computing derived variables";
                m_expressionCalculator.parser().evaluateExpression( expandedExpression, variablePointers, *( data[ t ] ) );

                progress += 1;
                derivationCalculateProgress = std::floor( 100 * ( progress / (float)  NSTEPS ) );
            }
        }
    }

    derivationCalculateProgress = 100;
    ////////qDebug() << "processed and stored derived variables";

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Free un-needed variables

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // note that all memory outside of the selected time sequence has already been freed
    for( std::int32_t i = START; i <= LAST; i += STRIDE )
    {
        // for each variable
        for( const auto & attr : m_particleAndTimeVariantAttributes.find( ptype )->second )
        {
            // if we don't need it
            if( ! variables.count( attr.first ) )
            {
                // and it's in memory
                if( m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr.first )->second[ i ] != nullptr )
                {
                    // then release it
                    m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr.first )->second[ i ].reset( nullptr );
                }
            }
        }
    }

    ////////qDebug() << "released now un-needed memory that was used for derivation";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Load remaining variabes needed

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // now load the base variables needed alone, and also free memory not needed
    for( std::int32_t i = START, c = 0; i <= LAST; i += STRIDE )
    {
        std::map< std::string, std::set< std::string > > needToLoad;
        bool everythingAleadyInMemory = true;

        // for each variable
        for( const auto & attr : variables )
        {
            // if its a base variable
            if( m_baseVariables.find( ptype )->second.find( attr ) != m_baseVariables.find( ptype )->second.end() )
            {
                // and if this variable is not currently in memory for this time step
                if( m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second[ i ] == nullptr )
                {
                    // if not done yet, add the particle->variable set element to the map
                    if( needToLoad.find( ptype ) == needToLoad.end() )
                    {
                        needToLoad.insert( std::pair< std::string, std::set< std::string > >( ptype, std::set< std::string >( ) ) );
                    }
                    // add the variable
                    needToLoad.find( ptype )->second.insert( attr );
                    everythingAleadyInMemory = false;
                }
                // if it's already in memory, we still need to find out if it has already been filtered due to being retained after being loaded for processing the filter
                // and the filter having then been already applied to it
                else if ( m_filters.at( ptype ).name != "None" && m_filterIndices.at( ptype ).size() != m_particleAndTimeVariantAttributes.find( ptype )->second.find( attr )->second[ i ]->size() )
                {
                    applyFilter( i, ptype, attr );
                }
            }
        }

        // if things need to be loaded at this time step, then load them
        if( ! everythingAleadyInMemory )
        {
            m_dataImporter->loadTimeStep(
                i,
                needToLoad,
                m_particleAndTimeVariantAttributes
            );

            if( m_filters.at( ptype ).name != "None" )
            {
                for( auto & var : needToLoad.at( ptype ) )
                {
                    applyFilter( i, ptype, var );
                }
            }
        }

        ++c;
        baseProgress = ( c * 100 ) / NSTEPS;
    }
    baseProgress = 100;

    ////////qDebug() << "loaded active base variables";

    //////////////////////////////////////////////////////////////////////

    // go through the data that is loaded and calculate the ranges

    ////////////////////////////////////////////////////////////////////

    m_storedVariables.find( ptype )->second = variables;
    for( auto & v : variables )
    {
        m_storedVariableErrorState.at( ptype ).insert( { v, 0 } );
    }

    ////////qDebug() << "getting error state";

    std::map< std::string, std::uint8_t > & errorStates = m_storedVariableErrorState.at( ptype );

    std::set< size_t > errorIndices;
    const size_t NP = numLoadedParticles( ptype );

    //////////qDebug() << NP;

    int c = 0;
    if( m_errorRemovalOption & REMOVE_INF )
    {
        //////////qDebug() << "detecting INF";
        for( auto & var : m_particleAndTimeVariantAttributes.at( ptype ) )
        {
            for( auto t = START; t <= LAST; t += STRIDE )
            {
                if( var.second[ t ] != nullptr )
                {
                    for( size_t i = 0; i < NP; ++i )
                    {
                        TargetFloatType val = ( *( var.second[ t ] ) )[ i ];

                        if( std::isinf( val ) )
                        {
                            errorStates.at( var.first ) |= HAS_INF;
                            errorIndices.insert( i );
                        }
                    }
                }
                ++c;
                NaNCheckProgress = ( c * 100 ) / NSTEPS;
            }
        }
        //////////qDebug() << "errors " << errorIndices.size();
    }
    NaNCheckProgress = 100;
    c = 0;
    if( m_errorRemovalOption & REMOVE_NAN )
    {
        //////////qDebug() << "detecting NAN";
        for( auto & var : m_particleAndTimeVariantAttributes.at( ptype ) )
        {
            for( auto t = START; t <= LAST; t += STRIDE )
            {
                if( var.second[ t ] != nullptr )
                {
                    for( size_t i = 0; i < NP; ++i )
                    {
                        TargetFloatType val = ( *( var.second[ t ] ) )[ i ];

                        if( ! std::isfinite( val ) )
                        {
                            if( ! std::isinf( val ) )
                            {
                                errorStates.at( var.first ) |= HAS_NAN;
                                errorIndices.insert( i );
                            }
                        }
                    }
                }
                ++c;
                InfCheckProgress = ( c * 100 ) / NSTEPS;
            }
        }
        //////////qDebug() << "errors " << errorIndices.size();
    }
    InfCheckProgress = 100;
    c = 0;
    if( m_errorRemovalOption & REMOVE_UNEXPECTED_VALUES )
    {
        //////////qDebug() << "detecting zeros";

        for( auto t = START; t <= LAST; t += STRIDE )
        {
            for( size_t i = 0; i < NP; ++i )
            {
                bool allZeros = true;
                for( auto & var : m_particleAndTimeVariantAttributes.at( ptype ) )
                {
                    if( var.second[ t ] != nullptr )
                    {

                        TargetFloatType val = ( *( var.second[ t ] ) )[ i ];

                        if( val != 0 )
                        {
                            allZeros = false;
                        }
                    }
                }
                if( allZeros )
                {
                    m_hasZeros = true;
                    errorIndices.insert( i );
                }
            }
            ++c;
            ZeroCheckProgress = ( c * 100 ) / NSTEPS;
        }
        //////////qDebug() << "errors " << errorIndices.size();
    }
    c = 0;
    ZeroCheckProgress = 100;

    // Update data to remove errors

    m_errorIndicesAreRemoved = false;

    if( m_errorRemovalOption != 0 && errorIndices.size() > 0 )
    {
        m_errorFilterIndices.clear();

        for( size_t i = 0; i < NP; ++i )
        {
            if( errorIndices.count( i ) == 0 )
            {
                m_errorFilterIndices.push_back( i );
            }
        }

        //////////qDebug() << m_errorFilterIndices.size() << " " << NP;

        if( m_errorFilterIndices.size() < NP )
        {
            m_errorIndicesAreRemoved = true;
            for( auto & var : m_storedVariables.at( ptype ) )
            {
                //////////qDebug() << "now filtering " << var.c_str() << " " << ptype.c_str();


                std::atomic< int > progress( 0 );

                #pragma omp parallel for
                for( auto t = START; t <= LAST; t += STRIDE )
                {
                    applyErrorFilter( t, ptype, var );
                    ++progress;
                    ErrorRemoveProgress = ( 100 * progress / NSTEPS );
                }
            }
        }
    }
    ErrorRemoveProgress = 100;

    ////////qDebug() << "removed data points";

    const int NATTR = m_particleAndTimeVariantAttributes.size();
    int attrRc = 0;
    for( const auto & p : m_particleAndTimeVariantAttributes )
    {
        // parallelism is over the variables, so the range update code is
        // thread safe

        #pragma omp parallel
        {
            int tId = omp_get_thread_num();
            int num_t = omp_get_num_threads();

            int k = 0;

            // each thread gets a unique chunk of varables according to the order they are iterated over
            for( const auto & var : p.second )
            {
                // if this variable belings to this thread
                if( k % num_t == tId )
                {
                    // calculate the variables range over the current time sequence
                    Vec2< double > range( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max() );
                    bool inMemory = false;

                    for( auto t = START; t <= LAST; t += STRIDE )
                    {
                        if( var.second[ t ] != nullptr )
                        {
                            inMemory = true;
                            for( const auto & val : *( var.second[ t ] ) )
                            {
                                range.a( std::min( range.a(), static_cast< double >( val ) ) );
                                range.b( std::max( range.b(), static_cast< double >( val ) ) );
                            }
                        }
                    }

                    // TODO ... This is temporary hack to deal with bad data points
                    if( var.first == "r" )
                    {
                        range.a( 4 );
                    }

                    // update the ranges only for those variables that are loaded
                    if( inMemory )
                    {
                        m_variantAttributeRanges.find( ptype )->second.find( var.first )->second = range;

                        if( m_derivedVariables.find( ptype )->second.count( var.first ) )
                        {
                            m_derivedVariables.find( ptype )->second.find( var.first )->second.range( range );
                        }

                        else if ( m_baseVariables.find( ptype )->second.count( var.first ) )
                        {
                            m_baseVariables.find( ptype )->second.find( var.first )->second.range( range );
                        }
                    }
                }
                ++k;
            }
        }
        ++attrRc;
        rangeProgress = ( attrRc * 100 ) / NATTR;
    }
    rangeProgress = 100;

    for( auto & p : m_storedVariables )
    {
        for( auto & attr : p.second )
        {
            //////////qDebug() << p.first.c_str() << " " << attr.c_str() << " " <<  m_variantAttributeRanges.at( p.first ).at( attr ).a() << " " << m_variantAttributeRanges.at( p.first ).at( attr ).b();
        }
    }

    ////////qDebug() << "updated variable ranges";

    status = true;

    //////////qDebug() << m_errorFilterIndices.size();
    //////////qDebug() << numLoadedParticles( ptype );
    //////////qDebug() << m_filterIndices.at( ptype ).size();
    //////////qDebug() << m_numParticles.at( ptype );
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::clear()
{
    return release();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Getters //

template < typename TargetFloatType >
const std::unique_ptr< std::vector< TargetFloatType > > & ParticleDataManager< TargetFloatType >::values( std::int32_t timeStep, const std::string & particleType, const std::string & attribute ) const
{
    return m_particleAndTimeVariantAttributes.find( particleType )->second.find( attribute )->second[ timeStep ];
}

template < typename TargetFloatType >
const std::vector< std::unique_ptr< std::vector< TargetFloatType > > > & ParticleDataManager< TargetFloatType >::values( const std::string & particleType, const std::string & attribute ) const
{
    return m_particleAndTimeVariantAttributes.find( particleType )->second.find( attribute )->second;
}

template < typename TargetFloatType >
const std::map< std::string, std::map< std::string, Vec2< double > > > & ParticleDataManager< TargetFloatType >::attributeRanges() const
{
    return m_variantAttributeRanges;
}

template < typename TargetFloatType >
Vec2< double > ParticleDataManager< TargetFloatType >::variableRange( const std::string & ptype, const std::string & key ) const
{
    return m_variantAttributeRanges.find( ptype )->second.find( key )->second;
}

template < typename TargetFloatType >
const std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & ParticleDataManager< TargetFloatType >::particleAndTimeVariantAttributes() const
{
    return m_particleAndTimeVariantAttributes;
}

template < typename TargetFloatType >
const std::map< std::string, VisualContextModel< TargetFloatType > > & ParticleDataManager< TargetFloatType >::visualContextModels() const
{
    return m_visualContextModels;
}

template < typename TargetFloatType >
const std::map< std::string, std::map< std::string, BaseVariable > > & ParticleDataManager< TargetFloatType >::baseVariables() const
{
    return m_baseVariables;
}
template < typename TargetFloatType >
const std::map< std::string, CustomConstant > & ParticleDataManager< TargetFloatType >::customConstants() const
{
    return m_customConstants;
}
template < typename TargetFloatType >
const std::map< std::string, BaseConstant > & ParticleDataManager< TargetFloatType >::baseConstants() const
{
    return m_baseConstants;
}
template < typename TargetFloatType >
const std::map< std::string, std::map< std::string, DerivedVariable > > & ParticleDataManager< TargetFloatType >::derivedVariables() const
{
    return m_derivedVariables;
}

template < typename TargetFloatType >
std::size_t ParticleDataManager< TargetFloatType >::numTimeSteps() const
{
    return m_numTimeSteps;
}
template < typename TargetFloatType >
std::size_t ParticleDataManager< TargetFloatType >::numLoadedTimeSteps() const
{
    return m_currentTimeSeries.numSteps();
}

template < typename TargetFloatType >
TimeSeriesDefinition ParticleDataManager< TargetFloatType >::currentTimeSeries() const
{
    return m_currentTimeSeries;
}

template < typename TargetFloatType >
TimeSeriesDefinition ParticleDataManager< TargetFloatType >::fullTimeSeries() const
{
    return m_fullTimeSeries;
}

template < typename TargetFloatType >
const std::vector< double > & ParticleDataManager< TargetFloatType >:: realTime() const
{
    return m_realTimeValues;
}

template < typename TargetFloatType >
const std::vector< std::int32_t > & ParticleDataManager< TargetFloatType >::simTime() const
{
    return m_simulationTimeSteps;
}

template < typename TargetFloatType >
std::size_t ParticleDataManager< TargetFloatType >::numParticles( const std::string & ptype ) const
{
    return m_numParticles.at( ptype );
}

template < typename TargetFloatType >
std::size_t ParticleDataManager< TargetFloatType >::numLoadedParticles( const std::string & ptype ) const
{
    if( m_errorRemovalOption == 0 )
    {
        if( m_filters.at( ptype ).name == "None" )
        {
            return m_numParticles.at( ptype );
        }
        else
        {
            return m_filterIndices.at( ptype ).size();
        }
    }
    else if( m_errorIndicesAreRemoved )
    {
        return m_errorFilterIndices.size();
    }
    else if( m_filters.at( ptype ).name != "None" )
    {
        return m_filterIndices.at( ptype ).size();
    }
    else
    {
        return m_numParticles.at( ptype );
    }
}

template < typename TargetFloatType >
std::uint8_t ParticleDataManager< TargetFloatType >::getStoredGlobalWarningState( const std::string & ptype ) const
{
    std::uint8_t ret = 0;
    for( auto & ws : m_storedVariableErrorState.at( ptype ) )
    {
        ret |= ws.second;
    }
    return ret;
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                               Setters                                     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived )
{
    if( ! m_derivedVariables.count( ptype ) )
    {
        m_derivedVariables.insert( { ptype, std::map< std::string, DerivedVariable>() } );
    }
    m_derivedVariables.find( ptype )->second = derived;

    // this shouldn't ever be the case ( unless a custom importer is flawed )
    if( ! m_particleAndTimeVariantAttributes.count( ptype ) )
    {
        m_particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                    ptype,
                    std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );
    }

    // this shouldn't ever be the case ( unless a custom importer is flawed )
    if( ! m_variantAttributeRanges.count( ptype ) )
    {
        m_variantAttributeRanges.insert( { ptype, std::map< std::string, Vec2< double > >() } );
    }

    for( const auto & dv : m_derivedVariables.at( ptype ) )
    {
        if( ! m_variantAttributeRanges.find( ptype )->second.count( dv.second.name() ) )
        {
            m_variantAttributeRanges.find( ptype )->second.insert( { dv.second.name(), Vec2< double >() } );
        }

        if( ! m_particleAndTimeVariantAttributes.at( ptype ).count( dv.second.name() ) )
        {
            for( int t = 0; t < m_numTimeSteps; ++t )
            {
                m_particleAndTimeVariantAttributes.at( ptype ).insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            dv.second.name(),
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
                m_particleAndTimeVariantAttributes.at( ptype).find( dv.second.name() )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
            }
        }
    }
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::updateCustomConstants( const std::map< std::string, CustomConstant > & cc )
{
    m_customConstants = cc;
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::updateBaseConstants( const std::map< std::string, BaseConstant > & bc )
{
    m_baseConstants = bc;
}

// the only thing that can have changed is the user specified descriptions
template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & bv )
{
    m_baseVariables.find( ptype )->second = bv;
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::setFilter( const std::string & ptype, const ParticleFilter & filter )
{
    m_filters.at( ptype ) = filter;
}

template < typename TargetFloatType >
void ParticleDataManager< TargetFloatType >::setErrorRemovalOption( std::uint8_t errorRemovalOption )
{
    m_errorRemovalOption = errorRemovalOption;
}

template < typename TargetFloatType >
bool ParticleDataManager< TargetFloatType >::getHasZeros( const std::string & ptype ) const
{
    return m_hasZeros;
}

template < typename TargetFloatType >
std::set< std::string > ParticleDataManager< TargetFloatType >::particleTypes() const
{
    return m_particleTypes;
}

/////////////////////////////////////////////////////////////////////////////////

// Explicit Instantiations

template class ParticleDataManager< float >;
template class ParticleDataManager< double >;
template class ParticleDataManager< long double >;

}

