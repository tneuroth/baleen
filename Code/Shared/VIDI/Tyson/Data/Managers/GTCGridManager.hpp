

#ifndef GTC_Distrobution_GRID_MANAGER
#define GTC_Distrobution_GRID_MANAGER

#include <fstream>
#include <memory>
#include <cstdint>
#include <vector>
#include <iostream>

//TODO: Use memory mapping, get estimate how much memory the application should use,
// pre-allocate this amount, and optimize read performance
// currently, there may be a risk of memory fragmentation...

/** A reader and memory manager for the Distrobution grids produced by GTC

   The current primary use case is for handling the loading and unloading of sequences of
   time chunks into/from memory
*/
template< typename OrigionalValueType, typename TargetValueType >
class GTCDistrobutionGridManager
{
    /** This construct should be in 1-1 correspondence with the 'Distrobution types' in file */
    struct DistrobutionType_
    {
        std::int32_t NUM_TYPES;

        /** the ids of the distrobutions, must correspond to the order they are written in file */
        std::vector< std::int32_t > DistrobutionIndex;

        /** The names of ths Distrobution types */
        std::vector< std::string > DistrobutionName;

        /** The descriptions of ths Distrobution types */
        std::vector< std::string > DistrobutionDescription;

        /** In the future this information may be loaded from file */
        DistrobutionType_()
        {
            NUM_TYPES = 7;

            DistrobutionIndex =
            {
                0, 1, 2, 3, 4, 5, 6
            };

            DistrobutionName =
            {
                "Particle Distrobution",
                "Momentum Distrobution",
                "Momentum Flux",
                "Energy Distrobution",
                "Energy Flux",
                "Particle Flux",
                "Bootstrap Current"
            };

            DistrobutionDescription =
            {
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            };
        }
    } DistrobutionType;

    /** This construct should be in 1-1 correspondence with the separate data files */
    struct ParticleType_
    {
        int NUM_TYPES;

        /** the ids of the particles */
        std::vector< std::int32_t > ParticleIndex;

        /** The names of the files for each particle type */
        std::vector< std::string > FileName;

        /** The names of ths Particle types */
        std::vector< std::string > ParticleName;

        /** The descriptions of ths Particle types */
        std::vector< std::string > ParticleDescription;

        /** In the future this information may be loaded from file */
        ParticleType_()
        {
            NUM_TYPES = 2;

            ParticleIndex =
            {
                0, 1
            };

            FileName =
            {
                "/PDF_ion_data.dat",
                "/PDF_electron_data.dat"
            };

            ParticleName =
            {
                "Ions",
                "Electrons"
            };

            ParticleDescription =
            {
                "",
                ""
            };
        }
    } ParticleType;

    /** An 'wrapper' of the per time slice and type, Distrobution grid */
    class DistrobutionGrid
    {
        /** The index of the distrobutions */
        std::int32_t m_distrobutionIndex;

        /** The bin values for all Distrobutions of a type, in a given time chunk, stored flat */
        std::vector< TargetValueType > m_DistrobutionValues;

    public:

        /** get a reference to the bin values */
        std::vector< TargetValueType > & values()
        {
            return m_DistrobutionValues;
        }
    };

    /** Indicates if the Manager has been initialized successfully */
    bool m_initialized;

    /** The base path where the data files are located */
    std::string m_basePath;

    /** file streams containing the Distrobution data, one per particle type */
    std::vector< std::ifstream > m_fileStreams;

    /** Number of "real" particles represented by each simulation particle, by type */
    std::vector< OrigionalValueType > m_particleRatio;

    /** velocity interval of uniform velocity grid (=v_max/nv) */
    std::vector< OrigionalValueType > m_particleDeltaV;

    /** Factor used for normalizing the distribution function.
       It is equal to the inverse of the phase space volume of
       each box where the PDF is evaluated.  */
    std::vector< OrigionalValueType > m_particleNormFactor;

    /** a temporary storage vector to be used for reading and swapping,
       used when the origional type (in the binary file) differs from the target type. */
    std::vector< OrigionalValueType > m_swap;

    /** fileSize / m_timeChunkReadSize = numTimeSteps */
    std::uint64_t m_fileSize;

    /** fileSize / m_timeChunkReadSize = numTimeSteps */
    std::uint64_t m_timeChunkReadSize;

    /** fileSize / m_timeChunkReadSize = numTimeSteps */
    std::int32_t m_numTimeSteps;

    /** The number of grid points in the simulation where the distrobution functions are evaluated, npts in simulation */
    std::int32_t m_numGridPoints;

    /** The number of discrete velocities used to produce the Distrobutions,
       corresponding to nv, as named in simulation, where the Distrobution
       resolution is ( 2 * nv + 1 ) X ( nv + 1 ). The second dimension is
       magnitude only, while the first dimension is signed.  */
    std::int32_t m_numDiscreteVelocities;

    /** The total number of Distrobution bins is ( 2 * m_numDiscreteVelocities + 1 ) X ( m_numDiscreteVelocities + 1 ) */
    std::int32_t m_DistrobutionNumBins;

    /** The 'real' simulation time of the time steps */
    std::vector< TargetValueType > m_simulationTime;

    /** the simulation time steps */
    std::vector< std::int32_t > m_simulationTimeStep;

    /** The main data structure through which the Distrobution data is stored/accessed.
       The first dimension corresponds to the different types of particles available.
       The second dimension corresoponds to the different types of Distrobutions which are available.
       The third dimensions corresponds to the available time steps.
       The unique_ptr< DistrobutionGrid > will be equal to nullptr unless loaded in memory */
    std::vector< std::vector< std::vector< std::unique_ptr< DistrobutionGrid > > > > m_gridData;

    /** radial position (sqrt(norm. toroidal flux)) of each box in magnetic coordinates */
    std::vector< TargetValueType > m_radialPositions;

    /** poloidal angle ("theta") of each box in magnetic coordinates */
    std::vector< TargetValueType > m_poloidalAngle;

    /** Laboratory coordinates of each box/cell/grid point, in (R,Z) cylindrical coordinates */
    std::vector< TargetValueType > m_posR;
    std::vector< TargetValueType > m_posZ;

    /** The magnetic field strength at (R,Z) ( for each grid point )*/
    std::vector< TargetValueType > m_bField;

    /** volume of each box where the PDF is evaluated */
    std::vector< TargetValueType > m_gridCellVolumes;

    /** The first time step in memory */
    std::int32_t m_currentTimeChunkStart;

    /** The last time step in memory */
    std::int32_t m_currentTimeChunkEnd;

    /** The unit stride of the current time series chunk held in memory*/
    std::int32_t m_currentTimeChunkStride;

    void readParameters()
    {
        const std::string ISTEP_PATH    = "/app_data/iSteps.dat";
        const std::string REALTIME_PATH = "/app_data/realTime.dat";

        std::int32_t nbytes, iStep;
        double realTime;
        std::ifstream fIn( m_basePath + "/PDF_grid_parameters.dat" );

        fIn.read(                  (char *)&nbytes, sizeof(                  nbytes ) );
        fIn.read(         (char *)&m_numGridPoints, sizeof(         m_numGridPoints ) );
        fIn.read( (char *)&m_numDiscreteVelocities, sizeof( m_numDiscreteVelocities ) );
        fIn.read(                 (char *)&nbytes,  sizeof(                  nbytes ) );

        m_DistrobutionNumBins = ( 2 * m_numDiscreteVelocities + 1 ) * ( m_numDiscreteVelocities + 1 );

        for( int pType = 0; pType < ParticleType.NUM_TYPES; ++pType )
        {
            fIn.read( (char *)&nbytes, sizeof( nbytes ) );
            fIn.read(         (char *)&m_particleRatio[ pType ], sizeof( OrigionalValueType ) );
            fIn.read(        (char *)&m_particleDeltaV[ pType ], sizeof( OrigionalValueType ) );
            fIn.read(    (char *)&m_particleNormFactor[ pType ], sizeof( OrigionalValueType ) );
            fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        }

        m_radialPositions.resize( m_numGridPoints );
        m_poloidalAngle.resize(   m_numGridPoints );
        m_posR.resize(            m_numGridPoints );
        m_posZ.resize(            m_numGridPoints );
        m_bField.resize(          m_numGridPoints );
        m_gridCellVolumes.resize( m_numGridPoints );

        const std::int32_t NUM_BYTES_GRID =  m_numGridPoints * sizeof( TargetValueType );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*) m_gridCellVolumes.data(), NUM_BYTES_GRID );
        fIn.read( (char *)&nbytes, sizeof( nbytes ) );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*) m_radialPositions.data(), NUM_BYTES_GRID );
        fIn.read( (char*)   m_poloidalAngle.data(), NUM_BYTES_GRID );
        fIn.read( (char *)&nbytes, sizeof( nbytes ) );

        fIn.read( (char *)&nbytes, sizeof( nbytes ) );
        fIn.read( (char*)   m_posR.data(), NUM_BYTES_GRID );
        fIn.read( (char*)   m_posZ.data(), NUM_BYTES_GRID );
        fIn.read( (char*) m_bField.data(), NUM_BYTES_GRID );
        fIn.read( (char *)&nbytes, sizeof( nbytes ) );

        fIn.close();

        for( std::int32_t pType = 0; pType < ParticleType.NUM_TYPES; ++pType )
        {
            m_fileStreams[ pType ].open( m_basePath + ParticleType.FileName[ pType ] );
        }

        // Determine the size of a time steps worth of pdf data
        m_timeChunkReadSize = DistrobutionType.NUM_TYPES * m_numGridPoints * m_DistrobutionNumBins * sizeof( OrigionalValueType )
                              + sizeof( OrigionalValueType ) + sizeof( nbytes ) * 10;

        m_fileStreams[ 0 ].seekg( 0, std::ios::end );
        m_fileSize = m_fileStreams[ 0 ].tellg();
        m_numTimeSteps = m_fileSize / m_timeChunkReadSize;

        // Reserve the number of time steps based on the file size
        // and the data size per time step
        m_simulationTime.reserve(     m_numTimeSteps );
        m_simulationTimeStep.reserve( m_numTimeSteps );

        // extract the real simulation time and time step information
        // first check if this information has been written to file
        fIn.open( m_basePath + ISTEP_PATH );
        if( fIn.is_open() )
        {
            nbytes = m_numTimeSteps * sizeof( std::int32_t );
            m_simulationTimeStep.resize( m_numTimeSteps );
            fIn.read( (char *)m_simulationTimeStep.data(), nbytes );
            fIn.close();

            fIn.open( m_basePath + REALTIME_PATH );
            m_simulationTime.resize( m_numTimeSteps );
            nbytes = m_numTimeSteps*sizeof( OrigionalValueType );
            if( std::is_same< OrigionalValueType, TargetValueType >::value )
            {
                fIn.read( (char *) m_simulationTime.data(), nbytes );
            }
            else
            {
                std::vector< OrigionalValueType > temp( m_numTimeSteps );
                fIn.read( (char *) temp.data(), nbytes );
                m_simulationTime = std::vector< TargetValueType >( temp.begin(), temp.end() );
            }
            fIn.close();
        }
        // if not, then extract it from the other very large files and write the file
        else
        {
            for( std::int32_t t = 0; t < m_numTimeSteps; ++t )
            {
                m_fileStreams[ 0 ].seekg( m_timeChunkReadSize * t );

                m_fileStreams[ 0 ].read( (char *)&nbytes,   sizeof(   nbytes ) );
                m_fileStreams[ 0 ].read( (char *)&realTime, sizeof( realTime ) );
                m_fileStreams[ 0 ].read( (char *)&nbytes,   sizeof(   nbytes ) );

                m_fileStreams[ 0 ].read( (char *)&nbytes,   sizeof(   nbytes ) );
                m_fileStreams[ 0 ].read( (char *)&iStep,    sizeof(    iStep ) );

                m_simulationTime.push_back(  realTime );
                m_simulationTimeStep.push_back( iStep );
            }

            // in case the Target type is different from the original type
            std::vector< OrigionalValueType > realTimeTemp( m_simulationTime.begin(), m_simulationTime.end() );

            // write to file
            std::ofstream outFile;

            outFile.open( m_basePath + ISTEP_PATH, std::ios::binary | std::ios::out );
            outFile.write( reinterpret_cast<const char*>( m_simulationTimeStep.data() ), m_simulationTimeStep.size() * sizeof( m_simulationTimeStep[ 0 ] ) );
            outFile.close();

            outFile.open( m_basePath + REALTIME_PATH, std::ios::binary | std::ios::out );
            outFile.write( reinterpret_cast<const char*>( realTimeTemp.data() ), realTimeTemp.size() * sizeof( realTimeTemp[ 0 ] ) );
            outFile.close();
        }
    }

    void readPdf(
        std::int32_t timeStep,
        const std::vector< std::int32_t > & partTypeFlags,
        const std::vector< std::vector< std::int32_t > > & loadFlagTable )
    {
        std::int32_t nbytes, istep, npts_2, nv_2, nval;
        OrigionalValueType real_time;

        const std::int32_t NUM_BYTES_GRID = m_numGridPoints * ( 2 * m_numDiscreteVelocities + 1 ) * ( m_numDiscreteVelocities + 1) * sizeof( OrigionalValueType );

        for( std::int32_t partType = 0; partType < ParticleType.NUM_TYPES; ++partType )
        {
            if( partTypeFlags[ partType ] )
            {
                m_fileStreams[ partType ].seekg( m_timeChunkReadSize * timeStep +  9 * sizeof( std::int32_t ) + sizeof( OrigionalValueType ) );
            }

            for( std::int32_t distType = 0; distType < DistrobutionType.NUM_TYPES; ++distType )
            {
                if( loadFlagTable[ partType ][ distType ] )
                {
                    m_gridData[ partType ][ distType ][ timeStep ].reset( new DistrobutionGrid );

                    if( ! std::is_same< OrigionalValueType, TargetValueType >::value )
                    {
                        m_fileStreams[ partType ].read( ( char* ) m_swap.data(), NUM_BYTES_GRID );
                        m_gridData[ partType ][ distType ][ timeStep ]->values() = std::vector< float >( m_swap.begin(), m_swap.end() );
                    }
                    else
                    {
                        m_gridData[ partType ][ distType ][ timeStep ]->values().resize( m_numGridPoints );
                        m_fileStreams[ partType ].read( ( char* ) m_gridData[ partType ][ distType ][ timeStep ]->values().data(), NUM_BYTES_GRID );
                        m_fileStreams[ partType ].read( ( char* ) m_gridData[ partType ][ distType ][ timeStep ]->values().data(), NUM_BYTES_GRID );
                    }
                }
                else
                {
                    if( partTypeFlags[ partType ] )
                    {
                        m_fileStreams[ partType ].seekg( static_cast< std::int64_t >( m_fileStreams[ partType ].tellg() ) + NUM_BYTES_GRID );
                    }
                }
            }
        }
    }

    void release(
        const std::vector< std::int32_t > & iSteps,
        const std::vector< std::int32_t > & partTypes = { 0, 1 },
        const std::vector< std::int32_t > & distTypes = { 0, 1, 2, 3, 4, 5, 6 } )
    {
        for( auto i : iSteps )
        {
            for( auto distType : distTypes )
            {
                for( auto partType : partTypes )
                {
                    m_gridData[ partType ][ distType ][ i ].reset( nullptr );
                }
            }
        }
    }

public:

    bool init( const std::string filePath )
    {
        m_basePath = filePath;
        m_initialized = true;

        // TODO: make sure it's ok

        m_fileStreams = std::vector< std::ifstream >( ParticleType.NUM_TYPES );

        m_particleRatio      = std::vector< OrigionalValueType >( ParticleType.NUM_TYPES );
        m_particleDeltaV     = std::vector< OrigionalValueType >( ParticleType.NUM_TYPES );
        m_particleNormFactor = std::vector< OrigionalValueType >( ParticleType.NUM_TYPES );

        if( m_initialized )
        {
            readParameters();
        }

        m_swap.resize( m_numGridPoints * m_DistrobutionNumBins );

        for ( int pType = 0; pType < ParticleType.NUM_TYPES; ++pType )
        {
            std::vector< std::vector< std::unique_ptr< DistrobutionGrid > > > dimD;
            for( std::int32_t dType = 0; dType < DistrobutionType.NUM_TYPES; ++dType )
            {
                std::vector< std::unique_ptr< DistrobutionGrid > > dimT;
                for ( std::int32_t t = 0; t < m_numTimeSteps; ++t )
                {
                    dimT.push_back( std::unique_ptr< DistrobutionGrid >( nullptr ) );
                }
                dimD.push_back( std::move( dimT ) );
            }
            m_gridData.push_back( std::move( dimD ) );
        }

        return m_initialized;
    }

    bool ok()
    {
        return m_initialized;
    }

    GTCDistrobutionGridManager()
    {
        m_initialized = false;
    }

    GTCDistrobutionGridManager( const std::string filePath )
    {
        init( filePath );
    }

    ~GTCDistrobutionGridManager()
    {
        for( std::int32_t partType = 0; partType < ParticleType.NUM_TYPES; ++partType )
        {
            m_fileStreams[ partType ].close();
        }
    }

    void release()
    {
        std::vector< std::int32_t > iSteps( m_numTimeSteps );
        for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
        {
            iSteps[ i ] = i;
        }
        release( iSteps );
    }

    void load(
        std::int32_t first,
        std::int32_t last,
        std::int32_t stride,
        const std::vector< std::int32_t > & partTypes = { 0, 1 },
        const std::vector< std::int32_t > & distTypes = { 0, 1, 2, 3, 4, 5, 6 },
        bool releaseLastChunk = true )
    {

        if( releaseLastChunk )
        {
            std::vector< std::int32_t > toRelease;
            toRelease.reserve( m_numTimeSteps );
            for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
            {
                // if it's not something we will want to load
                if( ! ( i >= first && i <= last && ( i % stride == 0 ) ) )
                {
                    toRelease.push_back( i );
                }
            }
            release( toRelease );
        }

        m_currentTimeChunkStart  = first;
        m_currentTimeChunkEnd    = last;
        m_currentTimeChunkStride = stride;

        for( std::int32_t i = first; i <= last; i += stride )
        {
            std::vector< std::int32_t > partTypesRequested( ParticleType.NUM_TYPES, 0 );
            std::vector< std::vector< std::int32_t > > notLoadedTable( ParticleType.NUM_TYPES, std::vector< std::int32_t >( DistrobutionType.NUM_TYPES, 0 ) );
            bool needToLoad = false;

            for( auto distType : distTypes )
            {
                for( auto partType : partTypes )
                {
                    if( m_gridData[ partType ][ distType ][ i ].get() == nullptr )
                    {
                        partTypesRequested[ partType ] = 1;
                        notLoadedTable[ partType][ distType ] = 1;
                        needToLoad = true;
                    }
                }
            }
            if( needToLoad )
            {
                readPdf( i, partTypesRequested, notLoadedTable );
            }
        }
    }
};

#endif
