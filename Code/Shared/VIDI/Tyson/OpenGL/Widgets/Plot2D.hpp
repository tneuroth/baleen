


#ifndef PLOT2D_HPP
#define PLOT2D_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Plot.hpp"
#include "Types/Vec.hpp"

#include <unordered_map>
#include <limits>

namespace TN
{

class Plot2D : public Plot
{

    int m_index;

    // object map into data map, e.g. "Ions"
    std::string m_object;

    Vec2< float > m_pivotValue;

    // variable keys in data map, e.g. "v"
    std::string m_xAttr;
    std::string m_yAttr;

    // background object keys, e.g. "separatrix"
    std::vector< std::string > m_backgroundObjects;

    std::vector< Vec2< float > > m_background;
    std::vector< Vec2< float > > m_axis;
    std::vector< Vec2< float > > m_tics;
    std::vector< Vec2< float > > m_grid;

    Vec3< float > m_axisColor;
    Vec3< float > m_ticColor;
    Vec3< float > m_gridColor;
    Vec3< float > m_backgroundColor;
    Vec3< float > m_plotBackgroundColor;

    Vec2< float > m_xRange;
    Vec2< float > m_yRange;

    std::vector< float > m_xTicValues;
    std::vector< float > m_yTicValues;

    Vec2< float > m_minTicDist; // pixels, override users preference if tics would be too close
    Vec2< float > m_ticSpacing; //used when user doesn't give predefined tic value

    int m_gridRows;
    int m_gridCols;

    std::string m_xLabel;
    std::string m_yLabel;

    std::string m_title;
    PlotScale  m_xScale;
    PlotScale  m_yScale;
    float m_topMargin;         // pixels
    float m_bottomMargin;
    float m_leftMargin;
    float m_rightMargin;

    bool m_maintainAspectRatio;

    Vec2< float > m_selectedValue;
    bool m_valueIsSelected;

    bool m_boxIsSelected;
    bool m_boxC1IsSelected;
    bool m_boxC2IsSelected;

    std::pair< Vec2< float >, Vec2< float > > m_boxSelectionCorners;
    std::vector< Vec2< float > >m_boxSelection;

public:

    void make()
    {

        // background
        m_background.resize( 6 );
        m_background[0] = Vec2< float > ( -1, -1 );
        m_background[1] = Vec2< float > ( -1,  1 );
        m_background[2] = Vec2< float > (  1, -1 );
        m_background[3] = m_background[1];
        m_background[4] = Vec2< float > ( 1, 1 );
        m_background[5] = m_background[2];
        m_backgroundColor = Vec3< float >( .8, .8, .8 );

        // axis
        m_axis.resize( 4 );
        m_axis[0] = Vec2< float >( -1,  1 );
        m_axis[1] = Vec2< float >( -1, -1 );
        m_axis[2] = Vec2< float >( -1, -1 );
        m_axis[3] = Vec2< float >(  1, -1 );

        // tiks

        if( plotSize().x() <= 0 || plotSize().x() > 9999 || plotSize().y() <= 0 || plotSize().y() > 9999 )
        {
            return;
        }

        //grid
        int NC = plotSize().x() / 60;
        int NR = plotSize().y() / 60;

        m_gridRows = NR;
        m_gridCols = NC;

        NR *= 2.0;
        NC *= 2.0;

        m_grid.resize( ( NR + NC )*2 );

        float dc = 2.f / NC;
        for( int c = 0; c < NC; ++c )
        {
            m_grid[ c * 2 +  0 ] = Vec2< float >( -1 + (c+1)*dc, -1 );
            m_grid[ c * 2 +  1 ] = Vec2< float >( -1 + (c+1)*dc,  1 );
        }
        float dr = 2.f / NR;
        for( int r = NC; r < NR+NC; ++r )
        {
            m_grid[ r * 2 +  0 ] = Vec2< float >( -1, -1 + (r-NC+1)*dr );
            m_grid[ r * 2 +  1 ] = Vec2< float >(  1, -1 + (r-NC+1)*dr );
        }
    }

    void initialize(
        const std::string & obj,
        const std::string & xAttr,
        const std::string & yAttr,
        const Vec2< float > & xRange,
        const Vec2< float > & yRange,
        const std::string   & xLabel,
        const std::string   & yLabel,
        const std::string   & title,
        PlotScale xScale,
        PlotScale yScale,
        bool maintainAspectRatio,
        const std::vector< std::string > & backgroundObjects )
    {
        m_valueIsSelected = false;
        m_boxIsSelected = false;
        m_object = obj;
        m_xAttr = xAttr;
        m_yAttr = yAttr;
        m_backgroundObjects = backgroundObjects;
        m_xRange = xRange;
        m_yRange = yRange;
        m_xLabel = xLabel;
        m_yLabel = yLabel;
        m_title  =  title;
        m_xScale = xScale;
        m_yScale = yScale;
        m_ticSpacing = Vec2< float >( .25f, .25f );
        m_topMargin    = 50.f;
        m_bottomMargin = 40.f;
        m_rightMargin  = 24.f;
        m_leftMargin   = 90.f;
        m_maintainAspectRatio = maintainAspectRatio;
        this->m_plotType = PlotType::PLOT2D;
        m_pivotValue = Vec2< float >( m_xRange.a(), m_yRange.a() );

        make();
    }

    Plot2D(
        const std::string & obj,
        const std::string & xAttr,
        const std::string & yAttr,
        const std::unordered_map< std::string, std::unordered_map< std::string, std::pair< Vec2< float > ,std::vector< std::vector< float > > > > > & dataMap,
        const std::unordered_map< std::string, std::pair< Range4< float >, std::vector< Vec2< float > > > > & backgroundObjects2D,
        int index,
        bool maintainAspectRatio = false,
        const std::vector< std::string > & backgroundObjects = std::vector< std::string >(), const std::string & title = "" )
    {

        m_index = index;

        Vec2< float > xRange = dataMap.find( obj )->second.find( xAttr )->second.first;
        Vec2< float > yRange = dataMap.find( obj )->second.find( yAttr )->second.first;

        for( const auto & key : backgroundObjects )
        {
            xRange.a( std::min( xRange.a(), backgroundObjects2D.find( key )->second.first.x1Min ) );
            xRange.b( std::max( xRange.b(), backgroundObjects2D.find( key )->second.first.x1Max ) );
            yRange.a( std::min( yRange.a(), backgroundObjects2D.find( key )->second.first.x2Min ) );
            yRange.b( std::max( yRange.b(), backgroundObjects2D.find( key )->second.first.x2Max ) );
        }

//        xRange += Vec2< float >( -m_plotMargin, m_plotMargin );
//        xRange += Vec2< float >( -m_plotMargin, m_plotMargin );

        initialize(
            obj,
            xAttr,
            yAttr,
            xRange,
            yRange,
            xAttr,
            yAttr,
            title,
            PlotScale::LINEAR_SCALE,
            PlotScale::LINEAR_SCALE,
            maintainAspectRatio,
            backgroundObjects
        );
    }
    Plot2D()
    {
        this->plotType( PlotType::PLOT2D );
    }

    void setPivotValue( const Vec2< float > & v )
    {
        m_pivotValue = v;
    }

    void selectValue( Vec2< float > _v )
    {
        m_selectedValue = _v;
        m_valueIsSelected = true;
    }

    void deselectValue()
    {
        m_valueIsSelected = false;
    }

    void finalizeBoxSelection()
    {
        m_boxIsSelected = true;
    }

    void selectBox( const std::pair< Vec2< float >, Vec2< float > > & corners )
    {

        m_boxSelectionCorners = corners;

        m_boxSelection.resize( 5 );
        m_boxSelection[ 0 ] = m_boxSelectionCorners.first;
        m_boxSelection[ 1 ] =  Vec2< float >( m_boxSelectionCorners.second.x(), m_boxSelectionCorners.first.y() );
        m_boxSelection[ 2 ] = m_boxSelectionCorners.second;
        m_boxSelection[ 3 ] = Vec2< float >( m_boxSelectionCorners.first.x(), m_boxSelectionCorners.second.y() );
        m_boxSelection[ 4 ] = m_boxSelectionCorners.first;
    }

    void selectBoxC1( const Vec2< float > & corner )
    {
        m_boxC1IsSelected = true;
        m_boxSelectionCorners.first = corner;
        m_boxIsSelected = false;
    }

    void selectBoxC2( const Vec2< float > & corner )
    {
        m_boxC2IsSelected = true;
        m_boxSelectionCorners.second = corner;
        selectBox( m_boxSelectionCorners );
    }

    void deselectBox()
    {
        m_boxIsSelected = false;
        m_boxC1IsSelected = false;
        m_boxC2IsSelected = false;
    }

    const std::string   & object() const
    {
        return m_object;
    }
    const std::string   &  xAttr() const
    {
        return  m_xAttr;
    }
    const std::string   &  yAttr() const
    {
        return  m_yAttr;
    }
    const Vec2< float > & xRange() const
    {
        return m_xRange;
    }
    const Vec2< float > & yRange() const
    {
        return m_yRange;
    }

    const std::vector< std::string > & backgroundObjects() const
    {
        return m_backgroundObjects;
    }

    bool maintainAspectRatio() const
    {
        return m_maintainAspectRatio;
    }
    float aspectRatio()        const
    {
        return plotSize().x() / plotSize().y();
    }

    Vec2< float > plotPosition() const
    {
        return position() + Vec2< float >( m_leftMargin, m_topMargin );
    }
    Vec2< float > plotSize() const
    {
        return size() - Vec2< float >( m_leftMargin + m_rightMargin, m_topMargin + m_bottomMargin );
    }
    float plotTop()    const
    {
        return    m_topMargin;
    }
    float plotBottom() const
    {
        return m_bottomMargin;
    }
    float plotLeft()   const
    {
        return   m_leftMargin;
    }
    float plotRight()  const
    {
        return  m_rightMargin;
    }

    const std::vector< Vec2< float > > & axis() const
    {
        return m_axis;
    }
    const std::vector< Vec2< float > > & background() const
    {
        return m_background;
    }
    const Vec3< float > & backgroundColor() const
    {
        return m_backgroundColor;
    }
    const Vec3< float > & plotBackgroundColor() const
    {
        return m_plotBackgroundColor;
    }
    const std::vector< Vec2< float > > & grid() const
    {
        return m_grid;
    }
    const std::string & title() const
    {
        return m_title;
    }

    const std::vector< Vec2< float > > & box() const
    {
        return m_boxSelection;
    }

    int numGridRows() const
    {
        return m_gridRows;
    }
    int numGridCols() const
    {
        return m_gridCols;
    }
    bool valueIsSelected() const
    {
        return m_valueIsSelected;
    }
    Vec2< float > selectedValue() const
    {
        return m_selectedValue;
    }

    bool boxC1IsSelected() const
    {
        return m_boxC1IsSelected;
    }
    bool boxIsBeingSelected() const
    {
        return ( m_boxC1IsSelected && m_boxC2IsSelected );
    }
    bool boxIsSelected() const
    {
        return m_boxIsSelected;
    }
    const std::pair< Vec2< float >, Vec2< float > > & boxSelectionCorners() const
    {
        return m_boxSelectionCorners;
    }

    int index() const
    {
        return m_index;
    }
};

}

#endif // PLOT2D_HPP

