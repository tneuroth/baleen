#ifndef WEIGHTHISTOGRAMWIDGET_HPP
#define WEIGHTHISTOGRAMWIDGET_HPP



#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

#include <unordered_map>
#include <unordered_set>

namespace TN
{

static const std::vector< Vec4 > COLOR_MAP_TF =
{
    {0.40692031947895596, 0.25733647058823528, 0.16388235294117648, 1.0},
    {0.41176460899656259, 0.26039999999999996, 0.16583333333333333, 1.0},
    {0.41660889851416921, 0.26346352941176471, 0.16778431372549021, 1.0},
    {0.42145318803177584, 0.26652705882352939, 0.16973529411764707, 1.0},
    {0.42629747754938246, 0.26959058823529414, 0.17168627450980392, 1.0},
    {0.43114176706698909, 0.27265411764705882, 0.17363725490196077, 1.0},
    {0.4359860565845956, 0.27571764705882351, 0.17558823529411763, 1.0},
    {0.44083034610220229, 0.27878117647058825, 0.17753921568627451, 1.0},
    {0.44567463561980891, 0.28184470588235294, 0.17949019607843136, 1.0},
    {0.45051892513741554, 0.28490823529411763, 0.18144117647058822, 1.0},
    {0.45536321465502216, 0.28797176470588237, 0.1833921568627451, 1.0},
    {0.46020750417262879, 0.29103529411764706, 0.18534313725490195, 1.0},
    {0.46505179369023542, 0.29409882352941175, 0.18729411764705883, 1.0},
    {0.46989608320784204, 0.29716235294117643, 0.18924509803921569, 1.0},
    {0.47474037272544856, 0.30022588235294118, 0.19119607843137254, 1.0},
    {0.47958466224305524, 0.30328941176470592, 0.19314705882352942, 1.0},
    {0.48442895176066186, 0.30635294117647061, 0.19509803921568628, 1.0},
    {0.48927324127826849, 0.30941647058823529, 0.19704901960784313, 1.0},
    {0.49411753079587512, 0.31248000000000004, 0.19900000000000001, 1.0},
    {0.49896182031348174, 0.31554352941176472, 0.20095098039215686, 1.0},
    {0.50380610983108831, 0.31860705882352941, 0.20290196078431372, 1.0},
    {0.50865039934869494, 0.3216705882352941, 0.20485294117647057, 1.0},
    {0.51349468886630156, 0.32473411764705878, 0.20680392156862742, 1.0},
    {0.51833897838390819, 0.32779764705882353, 0.20875490196078431, 1.0},
    {0.52318326790151481, 0.33086117647058821, 0.21070588235294116, 1.0},
    {0.52802755741912144, 0.3339247058823529, 0.21265686274509804, 1.0},
    {0.53287184693672807, 0.33698823529411764, 0.21460784313725492, 1.0},
    {0.53771613645433469, 0.34005176470588239, 0.21655882352941178, 1.0},
    {0.54256042597194132, 0.34311529411764707, 0.21850980392156863, 1.0},
    {0.54740471548954794, 0.34617882352941176, 0.22046078431372548, 1.0},
    {0.55224900500715446, 0.34924235294117645, 0.22241176470588234, 1.0},
    {0.5570932945247612, 0.35230588235294119, 0.22436274509803922, 1.0},
    {0.56193758404236782, 0.35536941176470588, 0.22631372549019607, 1.0},
    {0.56678187355997445, 0.35843294117647057, 0.22826470588235293, 1.0},
    {0.57162616307758107, 0.36149647058823531, 0.23021568627450981, 1.0},
    {0.57647045259518759, 0.36456, 0.23216666666666666, 1.0},
    {0.58131474211279421, 0.36762352941176468, 0.23411764705882351, 1.0},
    {0.58615903163040084, 0.37068705882352943, 0.23606862745098037, 1.0},
    {0.59100332114800747, 0.37375058823529411, 0.23801960784313725, 1.0},
    {0.59584761066561409, 0.37681411764705885, 0.23997058823529413, 1.0},
    {0.60069190018322072, 0.37987764705882354, 0.24192156862745098, 1.0},
    {0.60553618970082734, 0.38294117647058823, 0.24387254901960784, 1.0},
    {0.61038047921843397, 0.38600470588235297, 0.24582352941176472, 1.0},
    {0.61522476873604059, 0.38906823529411766, 0.24777450980392157, 1.0},
    {0.62006905825364722, 0.39213176470588235, 0.24972549019607843, 1.0},
    {0.62491334777125385, 0.39519529411764703, 0.25167647058823528, 1.0},
    {0.62975763728886047, 0.39825882352941172, 0.25362745098039213, 1.0},
    {0.6346019268064671, 0.40132235294117641, 0.25557843137254899, 1.0},
    {0.6394462163240735, 0.40438588235294115, 0.25752941176470584, 1.0},
    {0.64429050584168035, 0.40744941176470589, 0.25948039215686275, 1.0},
    {0.64913479535928698, 0.41051294117647058, 0.2614313725490196, 1.0},
    {0.65397908487689349, 0.41357647058823532, 0.26338235294117646, 1.0},
    {0.65882337439450012, 0.41664000000000001, 0.26533333333333331, 1.0},
    {0.66366766391210674, 0.4197035294117647, 0.26728431372549016, 1.0},
    {0.66851195342971337, 0.42276705882352938, 0.26923529411764702, 1.0},
    {0.67335624294731999, 0.42583058823529407, 0.27118627450980387, 1.0},
    {0.67820053246492662, 0.42889411764705887, 0.27313725490196078, 1.0},
    {0.68304482198253325, 0.43195764705882356, 0.27508823529411769, 1.0},
    {0.68788911150013987, 0.43502117647058824, 0.27703921568627454, 1.0},
    {0.6927334010177465, 0.43808470588235293, 0.2789901960784314, 1.0},
    {0.69757769053535312, 0.44114823529411762, 0.28094117647058825, 1.0},
    {0.70242198005295975, 0.44421176470588236, 0.2828921568627451, 1.0},
    {0.70726626957056637, 0.44727529411764705, 0.28484313725490196, 1.0},
    {0.712110559088173, 0.45033882352941174, 0.28679411764705881, 1.0},
    {0.7169548486057794, 0.45340235294117642, 0.28874509803921566, 1.0},
    {0.72179913812338625, 0.45646588235294122, 0.29069607843137257, 1.0},
    {0.72664342764099277, 0.45952941176470591, 0.29264705882352943, 1.0},
    {0.73148771715859939, 0.4625929411764706, 0.29459803921568628, 1.0},
    {0.73633200667620602, 0.46565647058823528, 0.29654901960784313, 1.0},
    {0.74117629619381264, 0.46871999999999997, 0.29849999999999999, 1.0},
    {0.74602058571141927, 0.47178352941176466, 0.30045098039215684, 1.0},
    {0.7508648752290259, 0.4748470588235294, 0.30240196078431369, 1.0},
    {0.75570916474663252, 0.47791058823529414, 0.3043529411764706, 1.0},
    {0.76055345426423915, 0.48097411764705883, 0.30630392156862746, 1.0},
    {0.76539774378184577, 0.48403764705882357, 0.30825490196078431, 1.0},
    {0.7702420332994524, 0.48710117647058826, 0.31020588235294116, 1.0},
    {0.77508632281705903, 0.49016470588235295, 0.31215686274509802, 1.0},
    {0.77993061233466565, 0.49322823529411763, 0.31410784313725487, 1.0},
    {0.78477490185227228, 0.49629176470588232, 0.31605882352941173, 1.0},
    {0.7896191913698789, 0.49935529411764701, 0.31800980392156858, 1.0},
    {0.79446348088748531, 0.5024188235294117, 0.31996078431372543, 1.0},
    {0.79930777040509216, 0.50548235294117649, 0.32191176470588234, 1.0},
    {0.80415205992269867, 0.50854588235294118, 0.32386274509803925, 1.0},
    {0.8089963494403053, 0.51160941176470587, 0.3258137254901961, 1.0},
    {0.81384063895791192, 0.51467294117647056, 0.32776470588235296, 1.0},
    {0.81868492847551855, 0.51773647058823524, 0.32971568627450981, 1.0},
    {0.82352921799312517, 0.52079999999999993, 0.33166666666666667, 1.0},
    {0.8283735075107318, 0.52386352941176462, 0.33361764705882352, 1.0},
    {0.83321779702833842, 0.52692705882352942, 0.33556862745098043, 1.0},
    {0.83806208654594505, 0.5299905882352941, 0.33751960784313728, 1.0},
    {0.84290637606355168, 0.53305411764705879, 0.33947058823529414, 1.0},
    {0.8477506655811583, 0.53611764705882359, 0.34142156862745099, 1.0},
    {0.85259495509876493, 0.53918117647058827, 0.34337254901960784, 1.0},
    {0.85743924461637155, 0.54224470588235296, 0.3453235294117647, 1.0},
    {0.86228353413397818, 0.54530823529411765, 0.34727450980392155, 1.0},
    {0.86712782365158481, 0.54837176470588234, 0.3492254901960784, 1.0},
    {0.87197211316919121, 0.55143529411764702, 0.35117647058823526, 1.0},
    {0.87681640268679806, 0.55449882352941182, 0.35312745098039217, 1.0},
    {0.88166069220440457, 0.55756235294117651, 0.35507843137254902, 1.0},
    {0.8865049817220112, 0.5606258823529412, 0.35702941176470587, 1.0},
    {0.89134927123961782, 0.56368941176470588, 0.35898039215686273, 1.0},
    {0.89619356075722445, 0.56675294117647057, 0.36093137254901958, 1.0},
    {0.90103785027483108, 0.56981647058823526, 0.36288235294117643, 1.0},
    {0.9058821397924377, 0.57287999999999994, 0.36483333333333329, 1.0},
    {0.91072642931004433, 0.57594352941176474, 0.3667843137254902, 1.0},
    {0.91557071882765095, 0.57900705882352943, 0.36873529411764705, 1.0},
    {0.92041500834525758, 0.58207058823529412, 0.3706862745098039, 1.0},
    {0.92525929786286421, 0.5851341176470588, 0.37263725490196076, 1.0},
    {0.93010358738047083, 0.58819764705882349, 0.37458823529411767, 1.0},
    {0.93494787689807746, 0.59126117647058818, 0.37653921568627452, 1.0},
    {0.93979216641568408, 0.59432470588235287, 0.37849019607843137, 1.0},
    {0.94463645593329071, 0.59738823529411766, 0.38044117647058823, 1.0},
    {0.94948074545089711, 0.60045176470588235, 0.38239215686274508, 1.0},
    {0.95432503496850396, 0.60351529411764704, 0.38434313725490199, 1.0},
    {0.95916932448611048, 0.60657882352941184, 0.38629411764705884, 1.0},
    {0.9640136140037171, 0.60964235294117652, 0.3882450980392157, 1.0},
    {0.96885790352132373, 0.61270588235294121, 0.39019607843137255, 1.0},
    {0.97370219303893035, 0.6157694117647059, 0.3921470588235294, 1.0},
    {0.97854648255653698, 0.61883294117647059, 0.39409803921568626, 1.0},
    {0.9833907720741436, 0.62189647058823527, 0.39604901960784311, 1.0},
    {0.98823506159175023, 0.62496000000000007, 0.39800000000000002, 1.0},
    {0.99307935110935686, 0.62802352941176476, 0.39995098039215687, 1.0},
    {0.99792364062696348, 0.63108705882352945, 0.40190196078431373, 1.0},
    {1.0, 0.63415058823529413, 0.40385294117647058, 1.0},
    {1.0, 0.63721411764705882, 0.40580392156862743, 1.0},
    {1.0, 0.64027764705882351, 0.40775490196078429, 1.0},
    {1.0, 0.64334117647058819, 0.40970588235294114, 1.0},
    {1.0, 0.64640470588235288, 0.411656862745098, 1.0},
    {1.0, 0.64946823529411757, 0.41360784313725485, 1.0},
    {1.0, 0.65253176470588237, 0.41555882352941176, 1.0},
    {1.0, 0.65559529411764705, 0.41750980392156861, 1.0},
    {1.0, 0.65865882352941174, 0.41946078431372547, 1.0},
    {1.0, 0.66172235294117643, 0.42141176470588232, 1.0},
    {1.0, 0.66478588235294112, 0.42336274509803923, 1.0},
    {1.0, 0.6678494117647058, 0.42531372549019608, 1.0},
    {1.0, 0.6709129411764706, 0.42726470588235294, 1.0},
    {1.0, 0.67397647058823529, 0.42921568627450984, 1.0},
    {1.0, 0.67703999999999998, 0.4311666666666667, 1.0},
    {1.0, 0.68010352941176477, 0.43311764705882355, 1.0},
    {1.0, 0.68316705882352946, 0.43506862745098041, 1.0},
    {1.0, 0.68623058823529415, 0.43701960784313726, 1.0},
    {1.0, 0.68929411764705883, 0.43897058823529411, 1.0},
    {1.0, 0.69235764705882352, 0.44092156862745097, 1.0},
    {1.0, 0.69542117647058821, 0.44287254901960782, 1.0},
    {1.0, 0.6984847058823529, 0.44482352941176467, 1.0},
    {1.0, 0.70154823529411769, 0.44677450980392158, 1.0},
    {1.0, 0.70461176470588238, 0.44872549019607844, 1.0},
    {1.0, 0.70767529411764707, 0.45067647058823529, 1.0},
    {1.0, 0.71073882352941176, 0.45262745098039214, 1.0},
    {1.0, 0.71380235294117644, 0.454578431372549, 1.0},
    {1.0, 0.71686588235294113, 0.45652941176470585, 1.0},
    {1.0, 0.71992941176470582, 0.4584803921568627, 1.0},
    {1.0, 0.72299294117647062, 0.46043137254901961, 1.0},
    {1.0, 0.7260564705882353, 0.46238235294117647, 1.0},
    {1.0, 0.72911999999999999, 0.46433333333333332, 1.0},
    {1.0, 0.73218352941176468, 0.46628431372549017, 1.0},
    {1.0, 0.73524705882352936, 0.46823529411764703, 1.0},
    {1.0, 0.73831058823529405, 0.47018627450980388, 1.0},
    {1.0, 0.74137411764705885, 0.47213725490196073, 1.0},
    {1.0, 0.74443764705882354, 0.47408823529411764, 1.0},
    {1.0, 0.74750117647058822, 0.4760392156862745, 1.0},
    {1.0, 0.75056470588235302, 0.47799019607843141, 1.0},
    {1.0, 0.75362823529411771, 0.47994117647058826, 1.0},
    {1.0, 0.7566917647058824, 0.48189215686274511, 1.0},
    {1.0, 0.75975529411764708, 0.48384313725490197, 1.0},
    {1.0, 0.76281882352941177, 0.48579411764705882, 1.0},
    {1.0, 0.76588235294117646, 0.48774509803921567, 1.0},
    {1.0, 0.76894588235294115, 0.48969607843137253, 1.0},
    {1.0, 0.77200941176470594, 0.49164705882352944, 1.0},
    {1.0, 0.77507294117647063, 0.49359803921568629, 1.0},
    {1.0, 0.77813647058823532, 0.49554901960784314, 1.0},
    {1.0, 0.78120000000000001, 0.4975, 1.0}
};

struct RangeSelector : public Widget
{
    std::vector< Vec2< float > > m_geometry;
    std::vector< Vec3< float > > m_colors;

    const int N_BINS = 200;
    float m_maxF;
    Vec2< double > m_wRange;
    Vec2< double > m_wSelectedRange;

    std::vector< float > m_binVals;
    std::vector< std::unordered_set< int > > m_binMap;

    bool logscale;

    void generateGeometry()
    {
        float dx = 2.0 / N_BINS;
        float dy = 2.0;

        for( int bin = 0; bin < N_BINS; ++bin )
        {
            m_geometry[ bin * 6 + 0 ] = Vec2< float >( -1 + bin*dx, -1 + dy );
            m_geometry[ bin * 6 + 1 ] = Vec2< float >( -1 + bin*dx, -1 );
            m_geometry[ bin * 6 + 2 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );

            m_geometry[ bin * 6 + 3 ] = Vec2< float >( -1 +     bin*dx, -1 + dy );
            m_geometry[ bin * 6 + 4 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );
            m_geometry[ bin * 6 + 5 ] = Vec2< float >( -1 + (bin+1)*dx, -1 + dy );

            Vec3< float > cl;

            if( m_maxF > 0 )
            {
                if( m_binVals[ bin ] == 0 )
                {
                    cl = Vec3< float >( 1, 1, 1 );
                }
                else
                {
                    int index = ( COLOR_MAP_TF.size() - 1 ) - std::min( size_t( ( m_binVals[ bin ] / m_maxF )  * COLOR_MAP_TF.size() ), COLOR_MAP_TF.size() - 1 );
                    Vec4 cl4 = COLOR_MAP_TF[ index ];
                    cl = Vec3< float >( cl4.r, cl4.g, cl4.b );
                }
            }
            else
            {
                cl = Vec3< float >( 1, 1, 1 );
            }

            m_colors[ bin * 6 + 0 ] = cl;
            m_colors[ bin * 6 + 1 ] = cl;
            m_colors[ bin * 6 + 2 ] = cl;

            m_colors[ bin * 6 + 3 ] = cl;
            m_colors[ bin * 6 + 4 ] = cl;
            m_colors[ bin * 6 + 5 ] = cl;
        }
    }

    RangeSelector() : Widget()
    {
        m_binVals = std::vector< float >( N_BINS, 0 );
        m_geometry = std::vector< Vec2< float > >( N_BINS*6, Vec2< float >( 0, 0 ) );
        m_colors = std::vector< Vec3< float > >(   N_BINS*6, Vec3< float >( 0, 0, 0 ) );
        m_binMap.resize( N_BINS );
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    void remakeGeometry()
    {
        generateGeometry();
    }

    double shift( double amount )
    {
        double allowed = amount;
        if( ( m_wSelectedRange.a() + amount ) < m_wRange.a() )
        {
            allowed = m_wSelectedRange.a() - m_wRange.a();
        }
        else if( ( m_wSelectedRange.b() + amount ) > m_wRange.b() )
        {
            allowed = m_wRange.b() - m_wSelectedRange.b();
        }
        m_wSelectedRange = m_wSelectedRange + Vec2< double >( allowed, allowed );

        return allowed;
    }

    void mouseDrag( const Vec2< float > & pos )
    {
        double selectedRangeWidth = m_wSelectedRange.b() - m_wSelectedRange.a();
        double fullRangeWidth = m_wRange.b() - m_wRange.a();

        double rangeWidthScreen = ( selectedRangeWidth / fullRangeWidth ) * size().x();

        double newLeftScreen  = pos.x() - rangeWidthScreen / 2.0;
        double newRightScreen = pos.x() + rangeWidthScreen / 2.0;

        if( newLeftScreen < position().x() )
        {
            newLeftScreen = position().x();
            newRightScreen = position().x() + rangeWidthScreen;
        }
        else if( newRightScreen > position().x() + size().x() )
        {
            newRightScreen = position().x() + size().x();
            newLeftScreen = newRightScreen - rangeWidthScreen;
        }

        m_wSelectedRange.a( m_wRange.a() + ( ( newLeftScreen - position().x() ) / ( double ) size().x() ) * fullRangeWidth  );
        m_wSelectedRange.b( m_wRange.a() + ( ( newRightScreen - position().x() ) / ( double ) size().x() ) * fullRangeWidth  );
    }

    void mouseWheel( double degree )
    {
        double fullRangeWidth = m_wRange.b() - m_wRange.a();
        double selectedRangeWidth = m_wSelectedRange.b() - m_wSelectedRange.a();
        double selectedRangeCenter = m_wSelectedRange.a() + selectedRangeWidth / 2.0;

        double newRangeWidth = selectedRangeWidth + ( degree * 2000 ) * fullRangeWidth;

        if( newRangeWidth <= 0 )
        {
            newRangeWidth = fullRangeWidth / 150.0;
        }

        m_wSelectedRange.a( selectedRangeCenter - newRangeWidth / 2.0 );
        m_wSelectedRange.b( selectedRangeCenter + newRangeWidth / 2.0 );

        m_wSelectedRange.a( std::max( m_wRange.a(), m_wSelectedRange.a() ) );
        m_wSelectedRange.b( std::min( m_wRange.b(), m_wSelectedRange.b() ) );
    }

    void resetSelectedRange()
    {
        float rangePad = ( m_wRange.b() - m_wRange.a() ) / 8.f;
        m_wSelectedRange = Vec2< float >( m_wRange.a() + rangePad, m_wRange.b() - rangePad );
    }

    Vec2< float > getRangeSelectionPositions() const
    {
        double fullRangeWidth = m_wRange.b() - m_wRange.a();

        return
        {
            position().x() + ( ( m_wSelectedRange.a() - m_wRange.a() ) / fullRangeWidth ) * size().x(),
            position().x() + ( ( m_wSelectedRange.b() - m_wRange.a() ) / fullRangeWidth ) * size().x(),
        };
    }

    void setRange( const Vec2< double > & r )
    {
        m_wRange = r;
    }

    void update( const std::vector< float > & weights )
    {
        ////qDebug() << "begin update";

        for( auto & mp : m_binMap )
        {
            mp.clear();
        }

        if( weights.size() < 3 )
        {
            return;
        }

        double mn = m_wRange.a();
        double mx = m_wRange.b();

        float dv = mx - mn;

        if( dv <= 0 )
        {
            return;
        }

        // then I want to make sure the zero point is exactly between two bins
        if( mn < 0 && mx > 0 )
        {
            // then extend the range in the positive direction
            float p = ( 0 - mn ) / dv;
            int b = std::min( (int) std::floor( p * N_BINS ), N_BINS - 1 );

            float db = dv / N_BINS;

            // find the distance from zero to nearest previous bin border
            // and increase the bin width by the amount needed so that

            if( b == 0 )
            {
                return;
            }

            float margin = 0 - ( mn + b*db );
            db += margin / b;

            mx = mn + db * N_BINS;
        }

        ////qDebug() << "split at zero";

        dv = mx - mn;

        if( dv <= 0 )
        {
            return;
        }

        for( auto & v : m_binVals )
        {
            v = 0;
        }

        m_maxF = 0.f;
        for( unsigned i = 0, end = weights.size(); i < end; ++i )
        {
            float p = ( weights[ i ] - mn ) / dv;
            int b = std::floor( p * N_BINS );
            if( b == N_BINS )
            {
                b = N_BINS-1;
            }
            else if( b > N_BINS || b  < 0 )
            {
                continue;
            }

            m_binVals[ b ] += 1.0;
            m_maxF = std::max( m_maxF, m_binVals[ b ] );
            m_binMap[ b ].insert( i );
        }

        ////qDebug() << "generating geometry";

        generateGeometry();

        ////qDebug() << "done generating geometry";
    }

    const std::vector< Vec2< float > > & geometry() const
    {
        return m_geometry;
    }
    const std::vector< Vec3< float > > & colors() const
    {
        return m_colors;
    }
};

class WeightHistogramWidget : public Widget
{
    Vec2< float > m_wRange;
    float m_maxF;
    Vec3< float > m_color;
    int m_nbins;
    float m_nSamples;

    bool m_suppressed;
    bool m_hovered;
    bool m_selected;

    std::vector< Vec2< float > > m_geometry;
    std::vector< Vec3< float > > m_colors;
    std::vector< float > m_binVals;
    std::vector< std::unordered_set< int > > m_binMap;

    int m_hoveredBin;
    int m_selectedBin;

    void generateGeometry()
    {
        float dx = 2.0 / m_nbins;
        float dy = 2.0;

        for( int bin = 0; bin < m_nbins; ++bin )
        {
            if( m_maxF > 0 )
            {
                float vh = m_binVals[ bin ] / m_maxF;

                //use at least 3 pixels
                if( vh > 0 )
                {
                    vh = std::max( vh, 3.f / size().y() );
                }

                m_geometry[ bin * 6 + 0 ] = Vec2< float >( -1 + bin*dx, -1 + dy * vh );
                m_geometry[ bin * 6 + 1 ] = Vec2< float >( -1 + bin*dx, -1 );
                m_geometry[ bin * 6 + 2 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );

                m_geometry[ bin * 6 + 3 ] = Vec2< float >( -1 +     bin*dx, -1 + dy * vh );
                m_geometry[ bin * 6 + 4 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );
                m_geometry[ bin * 6 + 5 ] = Vec2< float >( -1 + (bin+1)*dx, -1 + dy * vh );
            }
            else
            {
                m_geometry[ bin * 6 + 0 ] = Vec2< float >( -1 + bin*dx, -1 );
                m_geometry[ bin * 6 + 1 ] = Vec2< float >( -1 + bin*dx, -1 );
                m_geometry[ bin * 6 + 2 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );

                m_geometry[ bin * 6 + 3 ] = Vec2< float >( -1 +     bin*dx, -1  );
                m_geometry[ bin * 6 + 4 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );
                m_geometry[ bin * 6 + 5 ] = Vec2< float >( -1 + (bin+1)*dx, -1  );
            }

            Vec3< float > cl = m_color;
            if( m_hovered && ! m_suppressed )
            {
                if( bin == m_hoveredBin )
                {
                    cl = Vec3< float >( 1, 1, 1 );
                }
            }
            else if ( m_selected && ! m_suppressed )
            {
                if( bin == m_selectedBin )
                {
                    cl = Vec3< float >( 1, 1, 1 );
                }
            }

            m_colors[ bin * 6 + 0 ] = cl;
            m_colors[ bin * 6 + 1 ] = cl;
            m_colors[ bin * 6 + 2 ] = cl;

            m_colors[ bin * 6 + 3 ] = cl;
            m_colors[ bin * 6 + 4 ] = cl;
            m_colors[ bin * 6 + 5 ] = cl;
        }
    }

public:

    RangeSelector rangeSelector;

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    void setNumBins( int num )
    {
        m_nbins = num;
        m_binVals.resize( num );
        m_geometry.resize( num * 6 );
        m_colors.resize( num * 6 );

        m_binMap.clear();
        m_binMap.resize( num );
        for( int i = 0; i < num; ++i )
        {
            m_binMap[ i ].clear();
        }
    }

    void setColor( const Vec3< float > & cl)
    {
        m_color = cl;
    }

    void remakeGeometry()
    {
        generateGeometry();
    }

    void update( const std::vector< float > & weights )
    {
        for( auto & mp : m_binMap )
        {
            mp.clear();
        }

        m_binVals = std::vector< float >( m_nbins, 0 );
        m_geometry = std::vector< Vec2< float > >( m_nbins*6, Vec2< float >( 0, 0 ) );

        if( weights.size() < 3 )
        {
            return;
        }

        m_nSamples = weights.size();

        float mx = 0, mn = 0;

        for( auto w : weights )
        {
            mx = std::max( mx, w );
            mn = std::min( mn, w );
        }

        mn = rangeSelector.m_wSelectedRange.a();
        mx = rangeSelector.m_wSelectedRange.b();

        float dv = mx - mn;

        if( dv <= 0 )
        {
            return;
        }

        // then I want to make sure the zero point is exactly between two bins
        if( mn < 0 && mx > 0 )
        {
            // then extend the range in the positive direction
            //if( std::abs( mn ) > mx )
            //{
            // get number of full bins before 0
            float p = ( 0 - mn ) / dv;
            int b = std::min( (int) std::floor( p * m_nbins ), m_nbins - 1 );

            float db = dv / m_nbins;

            // find the distance from zero to nearest previous bin border
            // and increase the bin width by the amount needed so that
            float margin = 0 - ( mn + b*db );
            db += margin / b;

            mx = mn + db * m_nbins;
            //}
            // then extend the range in the negative direction
//            else
//            {
//                // get the number of full bins after 0
//                float p = ( 0 - mn ) / dv;
//                int b = std::min( (int) std::floor( p * m_nbins ), m_nbins - 1 );
//                int after0 = m_nbins - b - 1;

//                float db = dv / m_nbins;

//                // find the distance from zero to nearest previous bin border
//                // and increase the bin width by the amount needed so that
//                float margin = (b+1)*db - 0;

//                if( after0 == 0 )
//                {
//                    ////qDebug() << "divide by zero extending min";
//                }

//                db +=  margin / after0;
//                mn = mx - db * m_nbins;
//                ////qDebug() << "extened mn";
//            }
        }

        m_wRange = Vec2< float >( mn, mx );
        dv = mx - mn;

        if( dv <= 0 )
        {
            return;
        }

        for( auto & v : m_binVals )
        {
            v = 0;
        }

        m_maxF = 0.f;
        for( unsigned i = 0, end = weights.size(); i < end; ++i )
        {
            float p = ( weights[ i ] - mn ) / dv;
            int b = std::floor( p * m_nbins );
            if( b == m_nbins )
            {
                b = m_nbins-1;
            }
            else if( b > m_nbins || b  < 0 )
            {
                continue;
            }
            m_binVals[ b ] += 1.0;
            m_maxF = std::max( m_maxF, m_binVals[ b ] );
            m_binMap[ b ].insert( i );
        }

        generateGeometry();
    }

    WeightHistogramWidget() {}

    const Vec2< float > & range() const
    {
        return m_wRange;
    }
    const std::vector< Vec2< float > > & geometry() const
    {
        return m_geometry;
    }
    const std::vector< Vec3< float > > & colors() const
    {
        return m_colors;
    }
    const std::vector< float > & bins() const
    {
        return m_binVals;
    }

    float maxF() const
    {
        return m_maxF;
    }

    int binIndex( Vec2< float > pos )
    {
        int bin = ( ( pos.x() - position().x() ) / ( float ) width() ) * m_nbins;
        if( bin == m_nbins )
        {
            bin = m_nbins - 1;
        }
        return bin;
    }

    void hover( Vec2< float > pos )
    {
        m_hoveredBin = binIndex( pos );
        if( m_hoveredBin >= 0 && m_hoveredBin < m_nbins )
        {
            m_hovered = true;
        }
        else
        {
            m_hovered = false;
        }
    }

    void dblck( Vec2< float > pos )
    {
        int bin = binIndex( pos );
        if( m_selectedBin >= 0 && m_selectedBin < m_nbins && ( ( bin != m_selectedBin || m_selected == false ) ) )
        {
            m_selected = true;
        }
        else
        {
            m_selected = false;
        }
        m_selectedBin = bin;
    }

    void mouseOut()
    {
        m_hovered = false;
    }

    void clearSelection()
    {
        m_selected = false;
    }

    bool hovered() const
    {
        return m_hovered && ! m_suppressed;
    }

    bool selected() const
    {
        return m_selected && ! m_suppressed;
    }

    int hoveredBin()  const
    {
        return  m_hoveredBin;
    }

    int selectedBin() const
    {
        return m_selectedBin;
    }

    float binValue( int bin )
    {
        if( bin < m_binVals.size() && bin >= 0 )
        {
            return m_binVals[ bin ];
        }
        return -1;
    }

    float nSamples()
    {
        return m_nSamples;
    }

    void suppressed( bool p )
    {
        m_suppressed = p;
    }

    const std::vector< std::unordered_set< int > > & binMap() const
    {
        return m_binMap;
    }

    Vec2< float > prev;
    void setDragInit( const Vec2< float > & pos )
    {
        prev = pos;
    }

    void mouseDrag( const Vec2< float > & pos )
    {
        double delta = ( pos - prev ).x();
        float s = - ( delta / size().x() ) * ( rangeSelector.m_wSelectedRange.b() - rangeSelector.m_wSelectedRange.a() );
        float actual = rangeSelector.shift( s );
        prev = pos;
    }

    void mouseWheel( double degree )
    {
        float myRangeWidth = m_wRange.b() - m_wRange.a();
        float fullRangeWidth = rangeSelector.m_wRange.b() - rangeSelector.m_wRange.a();

        if( fullRangeWidth <= 0 )
        {
            return;
        }

        rangeSelector.mouseWheel( degree * ( myRangeWidth / fullRangeWidth ) );
    }
};

}

#endif // WEIGHTHISTOGRAMWIDGET_HPP
