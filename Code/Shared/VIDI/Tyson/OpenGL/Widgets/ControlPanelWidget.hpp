


#ifndef TN_CONTROL_PANEL_HPP
#define TN_CONTROL_PANEL_HPP

#include "OpenGL/Widgets/Widget.hpp"

namespace TN
{

class PressButton;
class ComboWidget;
class SliderWidget;

class ControlPanel : public TN::Widget
{
    Widget * m_root;

    Vec2< float > m_maxPosition;
    Vec2< float > m_minSize;

protected :

    std::vector< TN::PressButton  * > m_buttons;
    std::vector< TN::ComboWidget  * > m_combos;
    std::vector< TN::SliderWidget * > m_sliders;
    std::vector< TN::Label        * > m_labels;

public :

    std::vector< TN::PressButton  * > buttons() { return m_buttons; }
    std::vector< TN::ComboWidget  * >  combos() { return m_combos;  }
    std::vector< TN::SliderWidget * > sliders() { return m_sliders; }
    std::vector< TN::Label        * >  labels() { return m_labels;  }

    virtual void activate( bool a ) override
    {
        for( auto & w : buttons() )
        {
            ((Widget*)w)->activate( a );
        }
        for( auto & w : combos() )
        {
            ((Widget*)w)->activate( a );
        }
        for( auto & w : sliders() )
        {
            ((Widget*)w)->activate( a );
        }
        for( auto & w : labels() )
        {
            ((Widget*)w)->activate( a );
        }
        Widget::activate( a );
    }

    ControlPanel(){}

    virtual ~ControlPanel() {}
};

}

#endif
