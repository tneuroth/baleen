#ifndef SAMPLERWIDGET_HPP
#define SAMPLERWIDGET_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Sampler/SamplerPanel.hpp"

namespace TN
{

namespace SamplerParameters
{

enum class ColorScalingMode
{
    Local,
    Global
};

}

struct RegionOfInterest
{
    TN::Vec2< double > xRange;
    TN::Vec2< double > yRange;

    RegionOfInterest( const TN::Vec2< double > & x, const TN::Vec2< double > & y  )
    {
        xRange = x;
        yRange = y;
    }

    TN::Vec2< double > center() const
    {
        return
        {
            xRange.a() + ( xRange.b() - xRange.a() ) / 2.0,
            yRange.a() + ( yRange.b() - yRange.a() ) / 2.0
        };
    }
};

struct SamplerWidget : public TN::Widget
{
    TN::SamplerPanel controlPanel;
    TN::Widget visualizationWindow;

    static const int CONTROL_PANEL_HEIGHT = 70;

    SamplerWidget() : Widget() {}

    virtual void applyLayout()
    {
        controlPanel.setSize( this->size().x(), 10 );
        controlPanel.setPosition( this->position().x(), this->position().y() + this->size().y() - controlPanel.size().y() );

        visualizationWindow.setSize(     this->size().x(), this->size().y() - 2 );// - controlPanel.size().y() );
        visualizationWindow.setPosition( this->position().x(), this->position().y() );

        qDebug() <<  visualizationWindow.size().x() << " " 
                  << visualizationWindow.size().y() << " " 
                  << visualizationWindow.position().x() << " " 
                  << visualizationWindow.position().y();
    }

    virtual void setSize( float width, float height ) override
    {
        TN::Widget::setSize( width, height );
        this->applyLayout();
    }

    SamplerParameters::ColorScalingMode colorScalingMode()
    {
        const std::map< std::string, SamplerParameters::ColorScalingMode > colorScalingOptions =
        {
            { "local",  SamplerParameters::ColorScalingMode::Local  },
            { "global", SamplerParameters::ColorScalingMode::Global }
        };

        return colorScalingOptions.at(
                   controlPanel.colorScalingPanel.colorScalingCombo.selectedItemText() );
    }

    std::vector< TN::PressButton * > visibleButtons()
    {
        std::vector< TN::PressButton * > buttons;
        for( auto & panel : controlPanel.visiblePanels() )
        {
            for( auto & button : panel->buttons() )
            {
                buttons.push_back( button );
            }
        }
        return buttons;
    }

    std::vector< TN::ComboWidget * > visibleCombos()
    {
        std::vector< TN::ComboWidget * > combos;
        for( auto & panel : controlPanel.visiblePanels() )
        {
            for( auto & combo : panel->combos() )
            {
                combos.push_back( combo );
            }
        }
        return combos;
    }

    std::vector< TN::SliderWidget * > visibleSliders()
    {
        std::vector< TN::SliderWidget * > sliders;
        for( auto & panel : controlPanel.visiblePanels() )
        {
            for( auto & slider : panel->sliders() )
            {
                sliders.push_back( slider );
            }
        }
        return sliders;
    }

    std::vector< TN::Label * > visibleLabels()
    {
        std::vector< TN::Label * > labels;
        for( auto & panel : controlPanel.visiblePanels() )
        {
            for( auto & label : panel->labels() )
            {
                labels.push_back( label );
            }
        }
        return labels;
    }

    RegionOfInterest regionOfInterest() const
    {
        return { controlPanel.histSelectionPanel.xRange, controlPanel.histSelectionPanel.yRange };
    }
};

}

#endif // SAMPLERWIDGET_HPP
