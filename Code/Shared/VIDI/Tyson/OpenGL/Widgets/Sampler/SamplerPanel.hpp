#ifndef SAMPLERPANEL_HPP
#define SAMPLERPANEL_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Sampler/SamplerPanelWidgets.hpp"

namespace TN
{

struct SamplerPanel : public TN::Widget
{
    enum class Mode
    {
        PanningWindows,
        RegionOfInterest
    } mode;

    enum class PopulationType
    {
        Particles,
        GridPoints
    } populationType;

    HistviewSelectionPanel histSelectionPanel;
    ROIPanel roiPanel;
    ROIScalePanel roiScalePanel;
    PanningWindowPanel panningWindowPanel;
    ColorScalingPanel colorScalingPanel;
    HistParamsPanel histogramPanel;
    ContextPanel contextPanel;

    static const int PANEL_SPACING = 8;
    static const int BUTTON_HEIGHT = 20;
    static const int X_SPACING = 3;
    static const int Y_SPACING = 5;

    std::vector< PanelWidget * > panels;
    std::vector< int > selectedPanels;

    SamplerPanel() : mode( Mode::RegionOfInterest ), populationType( PopulationType::GridPoints )
    {
        panels =
        {
            & histSelectionPanel,
            & roiPanel,
            & roiScalePanel,
            & panningWindowPanel,
            & colorScalingPanel,
            & histogramPanel,
            & contextPanel
        };

        selectedPanels = { 0 };

        ( ( HistviewSelectionPanel * ) panels[ 0 ] )->useROI( mode == Mode::RegionOfInterest );

        for( auto & panel : panels )
        {
            panel->setButtonHeightAndSpacing( BUTTON_HEIGHT, X_SPACING, Y_SPACING );
        }
    }

    virtual void activate( bool a ) override
    {
        for( auto & panel : panels )
        {
            panel->activate( a );
        }
        Widget::activate( a );
    }

    void setPanningWindowsLayout()
    {
        if( populationType == PopulationType::Particles )
        {
            selectedPanels = { 0, 3, 4, 5 };
        }
        else if( populationType == PopulationType::GridPoints )
        {
            selectedPanels = { 0, 3, 4, 6 };
        }
    }

    void setRegionOfInterestLayout()
    {
        if( populationType == PopulationType::Particles )
        {
            selectedPanels = { 0, 5, 2, 4 };
        }
        else if( populationType == PopulationType::GridPoints )
        {
            selectedPanels = { 0, 2, 4, 6 };
        }
    }

    void applyLayout()
    {
        if( mode == Mode::PanningWindows )
        {
            setPanningWindowsLayout();
        }
        else if( mode == Mode::RegionOfInterest )
        {
            setRegionOfInterestLayout();
        }

        std::set< int > selectionSet( selectedPanels.begin(), selectedPanels.end() );

        int panelWidth = ( this->size().x() - PANEL_SPACING * ( selectedPanels.size() - 1 ) ) / selectedPanels.size();

        for( int i = 0; i < (int) selectedPanels.size(); ++i )
        {
            auto & panel = panels[ selectedPanels[ i ] ];

            panel->setSize( panelWidth, this->size().y() );

            panel->setPosition(
                this->position().x() + i * panelWidth + ( i * PANEL_SPACING ),
                this->position().y() );

            panel->applyLayout();
        }

        // put unselected panels of screen
        for( int i = 0; i < (int) panels.size(); ++i )
        {
            if( ! selectionSet.count( i ) )
            {
                auto & panel = panels[ i ];
                panel->setSize(  100, 100 );
                panel->setPosition( 1000000, 1000000 );
            }
        }
    }

    virtual void setSize( float width, float height ) override
    {
        qDebug() << "setting sampler panel size";
        TN::Widget::setSize( width, BUTTON_HEIGHT*3 + Y_SPACING*4 );
        applyLayout();
    }

    void setPopulationType( PopulationType p )
    {
        populationType = p;
    }

    void setMode( Mode m )
    {
        mode = m;
    }

    std::vector< PanelWidget * > visiblePanels()
    {
        std::vector< PanelWidget * > vp;
        for( int i = 0; i < (int) selectedPanels.size(); ++i )
        {
            vp.push_back( panels[ selectedPanels[ i ] ] );
        }
        return vp;
    }

    bool valid()
    {
        bool v = true;
        for( auto & s : roiPanel.sliders() )
        {
            if( s->isPressed() )
            {
                return false;
            }
        }
        for( auto & s : roiScalePanel.sliders() )
        {
            if( s->isPressed() )
            {
                return false;
            }
        }
        return true;
    }

    std::vector< TN::ComboWidget * > combos()
    {
        std::vector< TN::ComboWidget * > c;

        for( auto & panel : panels )
        {
            auto cms = panel->combos();
            c.insert(
                c.end(),
                cms.begin(),
                cms.end() );
        }

        return c;
    }
};

}

#endif // SAMPLERPANEL_HPP
