#ifndef TN_PANELS_HPP
#define TN_PANELS_HPP

#include "OpenGL/Widgets/ControlPanelWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/SliderWidget.hpp"

#include "Types/Vec.hpp"

#include <QDebug>

#include <iostream>
#include <string>
#include <set>
#include <vector>

namespace TN
{

struct PanelWidget : public TN::ControlPanel
{
    int buttonHeight;
    int ySpacing;
    int xSpacing;

    void setButtonHeightAndSpacing( float bh, int x, int y )
    {
        buttonHeight = bh;
        xSpacing = x;
        ySpacing = y;
    }

    float top()
    {
        return this->position().y() + this->size().y();
    }

    virtual void applyLayout() = 0;
    virtual void setSize( float width, float height ) override
    {
        Widget::setSize( width, buttonHeight * 3 + ySpacing*4 );
        applyLayout();
    }

    virtual void setDefault()
    {
        for( auto & slider : m_sliders )
        {
            slider->setDefault();
        }
    }
};

struct HistviewSelectionPanel : public TN::PanelWidget
{
    TN::ComboWidget layoutModeCombo;

    TN::PressButton addDistributionButton;
    TN::PressButton editDistributionButton;

    TN::ComboWidget distributionCombo;

    TN::PressButton centerButton;
    TN::PressButton resetButton;
    TN::PressButton spatialButton;
    TN::PressButton advancedButton;

    Vec2< double > xRange;
    Vec2< double > yRange;
    TN::ComboWidget roiCombo;
    TN::PressButton addROIButton;
    TN::PressButton editROIButton;

    static constexpr float VIEW_LABEL_WIDTH = 70;

    HistviewSelectionPanel()
    {
        addDistributionButton.text( "+" );
        editDistributionButton.text( "edit" );
        spatialButton.text( "Space" );
        centerButton.text("c");
        resetButton.text("r");
        advancedButton.text("a");

        roiCombo.text( "R.O.I.  " );
        addROIButton.text( "+" );
        editROIButton.text( "edit" );


        layoutModeCombo.addItem( "panning windows" );
        layoutModeCombo.addItem( "reg. of interest" );

        m_buttons =
        {
            & addDistributionButton,
            & editDistributionButton,
//            & centerButton,
//            & resetButton,
//            & advancedButton,
            // & spatialButton,
            & addROIButton,
            & editROIButton
        };

        m_combos =
        {
            & distributionCombo,
            & layoutModeCombo,
            & roiCombo
        };

        m_sliders =
        {
        };
    }

    void setROI(
        const std::string & x,
        const std::string & y,
        const Vec2< float > & xR,
        const Vec2< float > & yR )
    {
        xRange = xR;
        yRange = yR;
    }

    void useROI( bool c )
    {
        // swap out the space button and the roi combo, along with some of the buttons

//        combos.erase( std::remove_if(
//            combos.begin(),
//            combos.end(),
//            [&]( TN::ComboWidget * x )
//            {
//                return x == & roiCombo;
//            }),
//            combos.end() );

//        buttons.erase( std::remove_if(
//            buttons.begin(),
//            buttons.end(),
//            [&]( TN::PressButton * x )
//            {
//                return x == & spatialButton;
//            }),
//            buttons.end() );


//        if( c )
//        {
//            ///combos.push_back( & roiCombo );
//            buttons.push_back( & spatialButton );
//        }
//        else
//        {
//            buttons.push_back( & spatialButton );
//        }
    }

    virtual void applyLayout()
    {
        layoutModeCombo.setSize(
            this->size().x() - VIEW_LABEL_WIDTH - xSpacing * 2,
            buttonHeight
        );

        layoutModeCombo.setPosition(
            this->position().x() + VIEW_LABEL_WIDTH + xSpacing,
            top() - ySpacing - buttonHeight );

        distributionCombo.setSize(
            this->size().x() - xSpacing * 2 - buttonHeight - 40,
            buttonHeight );

        distributionCombo.setPosition(
            this->position().x() + xSpacing + buttonHeight,
            top() - buttonHeight - ySpacing * 2 - buttonHeight );

        editDistributionButton.setSize(
            40,
            buttonHeight );

        editDistributionButton.setPosition(
            distributionCombo.position().x() + distributionCombo.size().x(),
            distributionCombo.position().y() );

        addDistributionButton.setSize(
            buttonHeight,
            buttonHeight );

        addDistributionButton.setPosition(
            distributionCombo.position().x() - buttonHeight,
            distributionCombo.position().y() );

        //

        spatialButton.setSize(
            this->size().x() - xSpacing * 5 - buttonHeight*3,
            buttonHeight );

        spatialButton.setPosition(
            this->position().x() + xSpacing,
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight );

        //

        centerButton.setSize(
            buttonHeight,
            buttonHeight );

        centerButton.setPosition(
            spatialButton.position().x() + spatialButton.size().x() + xSpacing,
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight );

        resetButton.setSize(
            buttonHeight,
            buttonHeight );

        resetButton.setPosition(
            centerButton.position().x() + centerButton.size().x()  + xSpacing,
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight  );

        advancedButton.setSize(
            buttonHeight,
            buttonHeight );

        advancedButton.setPosition(
            resetButton.position().x() + resetButton.size().x() + xSpacing,
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight );

        /***************************/

        addROIButton.setSize( buttonHeight, buttonHeight );
        addROIButton.setPosition(
            this->position().x() + xSpacing + roiCombo.text().size()*7 + xSpacing,
            top() - ySpacing*3 - buttonHeight*3 );

        const int editWidth = 40;

        roiCombo.setSize(
            this->size().x() - roiCombo.text().size()*7 - addROIButton.size().x() - editWidth - 3*xSpacing,
            buttonHeight );

        roiCombo.setPosition(
            addROIButton.position().x() + addROIButton.size().x(),
            addROIButton.position().y() );

        //

        editROIButton.setSize( editWidth, buttonHeight );
        editROIButton.setPosition(
            roiCombo.position().x() + roiCombo.size().x(),
            roiCombo.position().y() );
    }
};

struct ROIPanel : public TN::PanelWidget
{
    TN::SliderWidget xSplitSlider;
    TN::SliderWidget ySplitSlider;

    TN::PressButton linkSplitSlidersButton;

    ROIPanel()
    {
        xSplitSlider.text( "x Div." );
        ySplitSlider.text( "y Div." );

        linkSplitSlidersButton.mode( TN::PressButton::Mode::Toggle );
        linkSplitSlidersButton.style( TN::PressButton::Style::Linker );

        m_buttons =
        {
            & linkSplitSlidersButton
        };

        m_combos =
        {
        };

        m_sliders =
        {
            & xSplitSlider,
            & ySplitSlider
        };
    }

    virtual void applyLayout()
    {
        xSplitSlider.setSize(
            this->size().x() - ySpacing*5 - xSplitSlider.text().size()*7 - TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight );

        xSplitSlider.setPosition(
            this->position().x() + ySpacing * 2 + xSplitSlider.text().size() * 7,
            top() - buttonHeight - ySpacing * 2 - buttonHeight );

        //

        ySplitSlider.setSize( xSplitSlider.size().x(), xSplitSlider.size().y() );
        ySplitSlider.setPosition(
            xSplitSlider.position().x(),
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight );

        linkSplitSlidersButton.setSize(
            TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight  + ySpacing + 4 );

        linkSplitSlidersButton.setPosition(
            xSplitSlider.position().x() + xSplitSlider.size().x() + xSpacing*2,
            ySplitSlider.position().y() + buttonHeight / 2.0 - 2
        );
    }
};

struct ROIScalePanel : public TN::PanelWidget
{
    TN::Label roiScaleLabel;

    TN::PressButton scaleLeftButton;
    TN::PressButton scaleRightButton;
    TN::PressButton scaleUpButton;
    TN::PressButton scaleDownButton;

    TN::PressButton resetButton;

    TN::SliderWidget xScaleSlider;
    TN::SliderWidget yScaleSlider;

    TN::SliderWidget lodSlider;

    TN::PressButton linkScaleSlidersButton;

    ROIScalePanel()
    {
        roiScaleLabel.text = "Scaling Dir.";

        linkScaleSlidersButton.mode(  TN::PressButton::Mode::Toggle );
        linkScaleSlidersButton.style( TN::PressButton::Style::Linker );

        m_buttons =
        {
            & scaleLeftButton,
            & scaleRightButton,
            & scaleUpButton,
            & scaleDownButton,
            //& resetButton,
            & linkScaleSlidersButton
        };

        xScaleSlider.text( "" );
        yScaleSlider.text( "" );
        lodSlider.text( "Partition" );

        scaleLeftButton.text( "l" );
        scaleRightButton.text( "r" );
        scaleUpButton.text( "u" );
        scaleDownButton.text( "d" );
        resetButton.text( "r" );

        scaleLeftButton.mode( TN::PressButton::Mode::Toggle );
        scaleRightButton.mode( TN::PressButton::Mode::Toggle );
        scaleUpButton.mode( TN::PressButton::Mode::Toggle );
        scaleDownButton.mode( TN::PressButton::Mode::Toggle );

        m_combos =
        {
        };

        m_sliders =
        {
            & xScaleSlider,
            & yScaleSlider,
            & lodSlider
        };

        m_labels =
        {
            //    &roiScaleLabel
        };
    }

    virtual void applyLayout()
    {
//        roiScaleLabel.position =
//        {
//            position().x() + xSpacing,
//            top() - ySpacing - buttonHeight + 5
//        };

        lodSlider.setSize( this->size().x() - lodSlider.text().size()*7 - xSpacing*2, buttonHeight );
        lodSlider.setPosition(
            this->position().x() + xSpacing + lodSlider.text().size()*7,
            top() - ySpacing - buttonHeight );

        scaleLeftButton.setSize( buttonHeight, buttonHeight );
        scaleLeftButton.setPosition(
            this->position().x() + xSpacing,
            top() - ySpacing*2 - buttonHeight*2 );

        //

        scaleRightButton.setSize( buttonHeight, buttonHeight );
        scaleRightButton.setPosition(
            scaleLeftButton.position().x() + scaleLeftButton.size().x(),
            scaleLeftButton.position().y() );

        //

        scaleUpButton.setSize( buttonHeight, buttonHeight );
        scaleUpButton.setPosition(
            scaleLeftButton.position().x(),
            scaleLeftButton.position().y() - ySpacing - buttonHeight );

        //

        scaleDownButton.setSize( buttonHeight, buttonHeight );
        scaleDownButton.setPosition(
            scaleUpButton.position().x() + scaleUpButton.size().x(),
            scaleUpButton.position().y() );

        //

        resetButton.setSize( buttonHeight, buttonHeight );
        resetButton.setPosition(
            this->position().x() + this->size().x() - ySpacing - resetButton.size().x(),
            scaleUpButton.position().y() );

        //

        xScaleSlider.setSize(
            this->size().x() - xSpacing*5 - xScaleSlider.text().size()*7 - TN::PressButton::LINK_BUTTON_WIDTH - buttonHeight*2,
            buttonHeight );

        xScaleSlider.setPosition(
            this->position().x() + ySpacing * 2 + xScaleSlider.text().size() * 7 + buttonHeight*2,
            top() - buttonHeight - ySpacing * 2 - buttonHeight );

        //

        yScaleSlider.setSize( xScaleSlider.size().x(), xScaleSlider.size().y() );
        yScaleSlider.setPosition(
            xScaleSlider.position().x(),
            top() - buttonHeight*2 - ySpacing * 3 - buttonHeight );

        linkScaleSlidersButton.setSize(
            TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight  + ySpacing + 4 );

        linkScaleSlidersButton.setPosition(
            xScaleSlider.position().x() + xScaleSlider.size().x() + xSpacing*2,
            yScaleSlider.position().y() + buttonHeight / 2.0 - 2
        );
    }
};

struct ColorScalingPanel : public TN::PanelWidget
{
    TN::ComboWidget colorScalingCombo;
    TN::SliderWidget colorScaleSlider;
    TN::ComboWidget smoothingCombo;

    ColorScalingPanel()
    {
        colorScaleSlider.text( "Factor" );
        colorScalingCombo.text( "Col. Scale" );
        smoothingCombo.text( "Smoothing " );

        colorScalingCombo.addItem( "global" );
        colorScalingCombo.addItem( "local"  );

        smoothingCombo.addItem( "none" );
        smoothingCombo.addItem( "3x3 average" );

        m_buttons =
        {
        };

        m_combos =
        {
            & colorScalingCombo,
            & smoothingCombo
        };

        m_sliders =
        {
            & colorScaleSlider,
        };
    }

    virtual void applyLayout()
    {
        colorScalingCombo.setSize(
            this->size().x() - ySpacing*3 - colorScalingCombo.text().size()*7,
            buttonHeight );

        colorScalingCombo.setPosition(
            this->position().x() + ySpacing + colorScalingCombo.text().size()*7 + ySpacing,
            top() - ySpacing - buttonHeight );

        //

        colorScaleSlider.setSize(
            this->size().x() - ySpacing*3 - colorScaleSlider.text().size()*7,
            buttonHeight );

        colorScaleSlider.setPosition(
            this->position().x() + ySpacing + colorScaleSlider.text().size()*7 + ySpacing,
            top() - ySpacing*2 - buttonHeight - buttonHeight );

        //

        smoothingCombo.setSize(
            this->size().x() - ySpacing*3 - smoothingCombo.text().size()*7,
            buttonHeight );

        smoothingCombo.setPosition(
            this->position().x() + ySpacing + smoothingCombo.text().size()*7 + ySpacing,
            top() - ySpacing*3 - buttonHeight*2 - buttonHeight );
    }
};

struct PanningWindowPanel : public TN::PanelWidget
{
    const int MAX_PARTITION_SIZE = 256;

    TN::SliderWidget levelOfDetailSlider;
    TN::SliderWidget aspectRatioSlider;

    PanningWindowPanel()
    {
        levelOfDetailSlider.text( "partition " );
        aspectRatioSlider.text( "projection" );
        levelOfDetailSlider.invalidates( true );
        aspectRatioSlider.invalidates(   true );

        m_buttons =
        {
        };

        m_combos =
        {
        };

        m_sliders =
        {
            & levelOfDetailSlider,
            & aspectRatioSlider
        };

        levelOfDetailSlider.setSliderPos( 0.25 );
    }

    virtual void applyLayout()
    {
        levelOfDetailSlider.setSize(
            this->size().x() - xSpacing*3 - levelOfDetailSlider.text().size() * 7,
            buttonHeight );

        levelOfDetailSlider.setPosition(
            this->position().x() + levelOfDetailSlider.text().size() * 7 + xSpacing*2,
            top() - ySpacing - buttonHeight );

        aspectRatioSlider.setSize(
            this->size().x() - xSpacing*3 - aspectRatioSlider.text().size() * 7,
            buttonHeight );

        aspectRatioSlider.setPosition(
            this->position().x() + aspectRatioSlider.text().size() * 7 + xSpacing*2,
            top() - ySpacing*2 - buttonHeight - buttonHeight );
    }
};

struct HistParamsPanel : public TN::PanelWidget
{
    SliderWidget resolutionSlider;
    SliderWidget valueWidthSliderX;
    SliderWidget valueWidthSliderY;

    const double RANGE_FACTOR = 10.f;

    PressButton linkValueWidthSlidersButton;

    HistParamsPanel()
    {
        valueWidthSliderX.text( "Hist. Width " );
        valueWidthSliderY.text( "Hist. Height" );
        resolutionSlider.text(  "Resolution  " );

        resolutionSlider.invalidates(  true );
        valueWidthSliderX.invalidates( true );
        valueWidthSliderY.invalidates( true );

        linkValueWidthSlidersButton.style( TN::PressButton::Style::Linker );
        linkValueWidthSlidersButton.mode( TN::PressButton::Mode::Toggle );

        m_buttons =
        {
            & linkValueWidthSlidersButton
        };

        m_combos =
        {
        };

        m_sliders =
        {
            & resolutionSlider,
            & valueWidthSliderX,
            & valueWidthSliderY
        };
    }

    virtual void applyLayout()
    {
        resolutionSlider.setSize(
            this->size().x() - xSpacing * 3 - resolutionSlider.text().size() * 7,
            buttonHeight );

        resolutionSlider.setPosition(
            this->position().x() + resolutionSlider.text().size() * 7 + xSpacing,
            top() - ySpacing - buttonHeight );

        valueWidthSliderX.setSize(
            this->size().x() - valueWidthSliderX.text().size() * 7  -  xSpacing * 5 - TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight );

        valueWidthSliderX.setPosition(
            this->position().x() + valueWidthSliderX.text().size() * 7 + xSpacing,
            top() - ySpacing*2 - buttonHeight*2 );

        valueWidthSliderY.setSize(
            this->size().x() - valueWidthSliderY.text().size() * 7  -  xSpacing * 5 - TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight );

        valueWidthSliderY.setPosition(
            this->position().x() + valueWidthSliderY.text().size() * 7 + xSpacing,
            top() - ySpacing*3 - buttonHeight*3 );

        linkValueWidthSlidersButton.setSize(
            TN::PressButton::LINK_BUTTON_WIDTH,
            buttonHeight  + ySpacing + 4 );

        linkValueWidthSlidersButton.setPosition(
            valueWidthSliderX.position().x() + valueWidthSliderX.size().x() + xSpacing*2,
            valueWidthSliderY.position().y() + buttonHeight / 2.0 - 2
        );
    }
};

struct ContextPanel : public TN::PanelWidget
{
    TN::SliderWidget bkgOpacitySlider;
    TN::SliderWidget histOpacitySlider;

    ContextPanel()
    {
        bkgOpacitySlider.text(  "Bkg.  Opacity" );
        histOpacitySlider.text( "Hist. Opacity" );

        m_buttons =
        {
        };

        m_combos =
        {
        };

        m_sliders =
        {
            & bkgOpacitySlider,
            & histOpacitySlider,
        };
    }

    virtual void applyLayout()
    {
        bkgOpacitySlider.setSize(
            this->size().x() - ySpacing*3 - bkgOpacitySlider.text().size() * 7,
            buttonHeight );

        bkgOpacitySlider.setPosition(
            this->position().x() + bkgOpacitySlider.text().size() * 7 + ySpacing*2,
            top() - ySpacing - buttonHeight );

        histOpacitySlider.setSize(
            this->size().x() - ySpacing*3 - histOpacitySlider.text().size() * 7,
            buttonHeight );

        histOpacitySlider.setPosition(
            this->position().x() + histOpacitySlider.text().size() * 7 + ySpacing*2,
            top() - ySpacing*2 - buttonHeight - buttonHeight );
    }
};

}
#endif // TN_PANELS_HPP
