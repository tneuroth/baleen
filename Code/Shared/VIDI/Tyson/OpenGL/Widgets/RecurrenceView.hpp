#ifndef RECURRENCEVIEW_HPP
#define RECURRENCEVIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/RecurrencePlot.hpp"
#include "OpenGL/Widgets/SimpleTimeSeriesPlot.hpp"
#include "OpenGL/Widgets/WeightHistogramWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/SliderWidget.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"

#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Managers/ParticleDataManager.hpp"

namespace TN
{

class RecurrenceView : public Widget
{
    RecurrencePlot m_reccurencePlot;
    RecurrencePlot m_reccurencePlotSel1;
    RecurrencePlot m_reccurencePlotSel2;

    SimpleTimeSeriesPlot m_timeSeriesPlot;
    TimeSeriesDefinition m_timeWindow;

    std::vector< RangeSelector > m_rangeSelectors;
    std::vector< std::string > m_attributes;
    std::vector< std::uint8_t > m_active;
    std::vector< TexturedPressButton > m_variableSelectors;

    SliderWidget m_epsilonSlider;
    ComboWidget m_normalizationCombo;

    const float DEFAULT_EPSILON = .01;
    const float MIN_EPSILON = .001;
    const float MAX_EPSILON = .1;

    float getEpsilon()
    {
        float epsilon = DEFAULT_EPSILON;
        float rescale = m_epsilonSlider.sliderPosition();

        if( rescale > 0.5 )
        {
            float p = ( ( 1.0 - rescale ) / 0.5 );
            float s  = 1.0 * ( 1.0 - p ) + ( MIN_EPSILON / DEFAULT_EPSILON ) * p;
            epsilon *= s;
        }
        else if( rescale < 0.5 )
        {
            float p = ( ( 0.5 - rescale ) / 0.5 );
            float s  = ( MAX_EPSILON / DEFAULT_EPSILON ) * ( 1.0 - p ) + 1.0 * p;
            epsilon *= s;
        }

        return epsilon;
    }

    void window( const TimeSeriesDefinition & window )
    {
        m_timeWindow = window;
    }
    void compute( ParticleDataManager< float > & dataManager, const std::string & ptype )
    {
        const int N_STEPS = m_timeWindow.numSteps();
        m_reccurencePlot.data.resize( N_STEPS * N_STEPS );

        std::vector< float > distances( N_STEPS * N_STEPS );
        std::vector< std::uint8_t > bitMask( N_STEPS * N_STEPS );

        const size_t T0 = m_timeWindow.firstIdx;
        const size_t TL = m_timeWindow.lastIdx;
        const size_t STRIDE = m_timeWindow.idxStride;

        int nActive = 0;
        for( auto f : m_active )
        {
            if( f )
            {
                ++nActive;
            }
        }

        std::vector< std::string > activeAttributes( nActive );
        std::vector< float > activeAttributesScale( nActive );

        for( int v = 0, v_idx = 0; v < m_attributes.size(); ++v )
        {
            if( m_active[ v ] )
            {
                activeAttributes[ v_idx ] = ( m_attributes[ v ] );
                Vec2< double > r = dataManager.variableRange( ptype, m_attributes[ v ] );
                double width = r.b() - r.a();
                activeAttributesScale[ v_idx ] = 1.0 / width;
                ++v_idx;
            }
        }

        const size_t N_PARTICLES = dataManager.numLoadedParticles( ptype );

        for( int p = 0; p < N_PARTICLES; ++p )
        {
            for( size_t i = T0; i <= TL; i += STRIDE )
            {
                for( size_t j = T0; j < i; j += STRIDE )
                {
                    float dist = 0;
                    for( int v = 0; v < activeAttributes.size(); ++v )
                    {
                        std::vector< float > & v_i = *( dataManager.values( i, ptype, activeAttributes[ v ] ) );
                        std::vector< float > & v_j = *( dataManager.values( j, ptype, activeAttributes[ v ] ) );

                        float dist_v = std::abs( v_i[ p ] - v_j[ p ] ) / activeAttributesScale[ v ];
                        dist += dist_v * dist_v;
                    }
                    distances[ i * N_STEPS + j ] = std::sqrt( dist );
                }
            }
        }
    }
};

}

#endif // RECURRENCEVIEW_HPP
