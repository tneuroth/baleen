

#ifndef HELPLABELWIDGET_HPP
#define HELPLABELWIDGET_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

#include <string>

namespace TN
{

class HelpLabelWidget : public Widget
{
    bool m_isPressed;
    std::string m_text;
    std::string m_htmlReference;

public:

    HelpLabelWidget() : Widget(), m_isPressed( false )
    {}

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    void htmlReference( const std::string & ref )
    {
        m_htmlReference = ref;
    }
    const std::string & htmlReference() const
    {
        return m_htmlReference;
    }

    void text( const std::string & txt )
    {
        m_text = txt;
    }
    const std::string & text() const
    {
        return m_text;
    }
};

}


#endif // HELPLABELWIDGET_HPP

