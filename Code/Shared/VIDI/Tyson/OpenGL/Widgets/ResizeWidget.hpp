


#ifndef RESIZEWIDGET
#define RESIZEWIDGET

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

namespace TN
{

class ResizeWidget : public Widget
{
    bool m_isPressed;

public:

    ResizeWidget() : Widget(), m_isPressed( false )
    {}

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }
};

}

#endif // RESIZEWIDGET

