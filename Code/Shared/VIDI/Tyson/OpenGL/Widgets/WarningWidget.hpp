#ifndef WARNINGWIDGET_HPP
#define WARNINGWIDGET_HPP

#include "Types/Vec.hpp"

#include <string>

namespace TN
{

class WarningWidget : public Widget
{

    std::string m_text;

public:

    WarningWidget() : Widget()
    {
        m_viewPort.bkgColor( Vec4( .95, .95, .95, 1.0 ) );
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    void text( const std::string & text )
    {
        m_text = text;
    }

    std::string text() const
    {
        return m_text;
    }
};

}

#endif // WARNINGWIDGET_HPP
