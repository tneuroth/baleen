

#ifndef CAMERA2D
#define CAMERA2D

#include "Camera.hpp"

#include <QJsonObject>

namespace TN
{

class Camera2D : public Camera
{
    double doZoom( double deltaZ );
    void resetProjection();

public:

    void translate( Vec2< float > dir );

    Camera2D();

    // Interface : pure virtual methods from abstract base class
    double wheel( double degrees );
    void setSize( double width, double height );

    /* later return the cumulitive amount of translation ... */
    Vec2< float > dragRightButton( Vec2< float > delta );
    Vec2< float > dragLeftButton( Vec2< float > delta );
    void setZoom( double z );

    void setTranslation( Vec2< float > trans );
    void reset();

    QJsonObject toJSON() const;
    void fromJSON( const QJsonObject & );

    //

    ~Camera2D();

private:

    Vec3< float >      m_pos;
};

}

#endif // CAMERA2D

