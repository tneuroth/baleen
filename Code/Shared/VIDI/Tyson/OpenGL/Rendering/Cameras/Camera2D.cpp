

#include <QDataStream>
#include <sstream>

#include "OpenGL/Rendering/Cameras/Camera2D.hpp"

namespace TN
{

double Camera2D::doZoom( double deltaZ )
{

    float factor = deltaZ < 0 ? .95 : 1.05;

    m_zoom *= factor;

    if ( m_zoom < 0.0000000005 )
    {
        m_zoom = 0.0000000005;
    }

    resetProjection();

    return 0.01 / m_zoom;
}

void Camera2D::translate( Vec2< float > dir )
{
    m_view.translate( dir.x(), dir.y() );
}

void Camera2D::resetProjection()
{
    m_proj.setToIdentity();
    m_proj.ortho(
        -m_width   * m_zoom,
        m_width  * m_zoom,
        -m_height * m_zoom,
        m_height  * m_zoom,
        0.0, 1.0
    );
}

void Camera2D::setTranslation( Vec2<float> trans )
{
    m_view.setToIdentity();
    m_view.translate( trans.x(), trans.y() );
}

Camera2D::Camera2D()
{

    m_zoom = 0.001;

    m_view.setToIdentity();
    // m_view.translate( -1.725, -0.02 );

    m_proj.setToIdentity();
    m_proj.ortho(-1.0, 1.0, -1.0, 1.0, 0, 100);
}

// Interface : pure virtual methods from abstract base class

double Camera2D::wheel( double degrees )
{
    return doZoom( degrees );
}

void Camera2D::setSize( double width, double height )
{
    m_width  = width;
    m_height = height;
    resetProjection();
}

/* later return the cumulitive amount of translation ... */
Vec2< float > Camera2D::dragRightButton( Vec2< float > delta )
{
    return Vec2< float >( 0.f, 0.f );
}
Vec2< float > Camera2D::dragLeftButton( Vec2< float > delta )
{
    translate( delta );
    return Vec2< float >( 0.f, 0.f );
}

void Camera2D::setZoom( double z )
{
    m_zoom = z;
    resetProjection();
}

void Camera2D::reset()
{
    m_zoom = 0.001;
    m_view.setToIdentity();
    m_proj.setToIdentity();
    m_proj.ortho(-1.0, 1.0, -1.0, 1.0, 0, 100);
}

inline QString mat4x4ToText( const QMatrix4x4 & m )
{
    QString txt = "[";
    for( int r = 0; r < 4; ++r )
    {
        for( int c = 0; c < 4; ++c )
        {
            txt.append( std::to_string( m( r, c ) ).c_str() );
            if( c < 3 )
            {
                txt.append( "," );
            }
        }
        if( r < 3 )
        {
            txt.append( ";" );
        }
        else
        {
            txt.append( "]" );
        }
    }
    return txt;
}

inline void split( const std::string & s, char delim, std::vector< std::string > & result )
{
    std::stringstream ss;
    ss.str( s );
    std::string item;
    while ( std::getline( ss, item, delim ) )
    {
        result.push_back( item );
    }
}

inline QMatrix4x4 mat4x4FromText( const std::string & str )
{
    QMatrix4x4 M;

    std::string s = str.substr( 1, str.size() - 2 );

    std::vector< std::string > rows;
    split( s, ';', rows );

    for( int r = 0; r < 4; ++r )
    {
        std::vector< std::string > cols;
        split( rows[ r ], ',', cols );
        for( int c = 0; c < 4; ++c )
        {
            M( r, c ) = std::stof( cols[ c ] );
        }
    }

    return M;
}

QJsonObject Camera2D::toJSON() const
{
    QJsonObject obj;
    QDataStream str;
    obj.insert( "view", mat4x4ToText( m_view ) );
    obj.insert( "zoom",m_zoom );

    return obj;
}

void Camera2D::fromJSON( const QJsonObject & obj )
{
    m_view = mat4x4FromText( obj[ "view" ].toString().toStdString() );
    m_zoom = obj[ "zoom" ].toDouble();
    resetProjection();
}

//

Camera2D::~Camera2D() {}

}
