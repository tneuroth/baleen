

#include "OpenGL/Rendering/Cameras/TrackBallCamera.hpp"

#include <QVector2D>
#include <QtMath>

TrackBallCam::TrackBallCam()
    : m_eye(0.f, 0.f, 4.f),
      m_center(0.f, 0.f, 0.f),
      m_up(0.f, 1.f, 0.f),
      m_zoom(-60),
      m_mode(PM_Perspective),
      m_aspect(1.f)
{
}

void TrackBallCam::orbit(const QVector2D &dir)
{
    QVector3D u = (m_center - m_eye).normalized();
    QVector3D s = QVector3D::crossProduct(u, m_up).normalized();
    QVector3D t = QVector3D::crossProduct(s, u).normalized();
    QVector3D rotateDir = (dir.x() * s + dir.y() * t).normalized();
    QVector3D rotateAxis = QVector3D::crossProduct(rotateDir, u).normalized();
    float angle = dir.length() * 360.f;
    QMatrix4x4 matRotate;
    matRotate.rotate(-angle, rotateAxis);
    QVector3D view = matRotate * (m_eye - m_center);
    m_eye = m_center + view;
    m_up = QVector3D::crossProduct(view, s).normalized();
}

void TrackBallCam::track(const QVector2D &dir)
{
    QVector3D u = (m_center - m_eye).normalized();
    QVector3D s = QVector3D::crossProduct(u, m_up).normalized();
    QVector3D t = QVector3D::crossProduct(s, u).normalized();
    QVector2D scaleDir = dir * focalPlaneSize();
    QVector3D trackDir = (scaleDir.x() * s + scaleDir.y() * t);
    m_eye = m_eye - trackDir;
    m_center = m_center - trackDir;
}

void TrackBallCam::zoom(const float factor)
{
    m_zoom += factor / 1000.f;
    //qDebug() << m_zoom;
}

void TrackBallCam::reset(const QVector3D &bmin, const QVector3D &bmax)
{
    // reset the camera to at the z directon
    // reset the object to the center of the screen
    // calculate the distance from center so that everything fits in the screen
    //m_center = (bmin + bmax) / 2.f + QVector3D( 0.f, 100.f, 0.f );
    m_up = QVector3D(0.f, 1.f, 0.f);
    //m_zoom = initZoom;
    float height = bmax.y() - bmin.y();
    float distance = (height / 2.f) / tan(fov() / 180.f * M_PI / 2.f);
    distance = distance * 2.f + bmax.z() - m_center.z();
    m_eye = m_center + QVector3D(0.f, 0.f, 1.f) * distance;
}

const QMatrix4x4& TrackBallCam::matView( float scale ) const
{
    static QMatrix4x4 mat;
    mat.setToIdentity();
    mat.lookAt(m_eye * scale, m_center, m_up);
    return mat;
}

const QMatrix4x4& TrackBallCam::matProj() const
{
    static QMatrix4x4 mat;
    mat.setToIdentity();
    if (PM_Perspective == m_mode)
    {
        mat.perspective(fov(), m_aspect, 0.1, 3000);
    }
    else
    {
        QVector2D size = focalPlaneSize() / 2.f;
        mat.ortho(-size.x(), size.x(), -size.y(), size.y(), 0.001, 3000);
    }
    return mat;
}

//
//
//
// helper functions
//
//
//

float TrackBallCam::fov() const
{
    return (qAtan(m_zoom) / M_PI + 0.5f) * 180.f;
}

QVector2D TrackBallCam::focalPlaneSize() const
{
    float viewLen = (m_center - m_eye).length();
    float height = qTan(fov() / 2.f / 180.f * M_PI) * viewLen;
    return QVector2D(height * m_aspect * 2.f, height * 2.f);
}

