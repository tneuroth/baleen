

#ifndef TRACKBALLCAM_H
#define TRACKBALLCAM_H

#include <QJsonObject>

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector2D>

class TrackBallCam
{
    enum ProjMode { PM_Perspective, PM_Orthographic };
    static constexpr float initZoom = -.5;

    QVector3D m_eye;
    QVector3D m_center;
    QVector3D m_up;
    float m_zoom;
    ProjMode m_mode;
    float m_aspect;

    float fov() const;
    QVector2D focalPlaneSize() const;

public:

    TrackBallCam();

    void setCenter( QVector3D center )
    {
        m_center = center;
    }
    void setProjMode(const ProjMode& mode)
    {
        m_mode = mode;
    }
    void setAspectRatio(const float ratio)
    {
        m_aspect = ratio;
    }
    QVector3D getEye() const
    {
        return m_eye;
    }
    void orbit(const QVector2D& dir);
    void track(const QVector2D& dir);
    void zoom(const float factor);
    void reset(const QVector3D& bmin, const QVector3D& bmax);
    const QMatrix4x4& matView( float scale ) const;
    const QMatrix4x4& matProj() const;
};


#endif // TRACKBALLCAM_H

