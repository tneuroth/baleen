

#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "OpenGL/Rendering/AORenderScene.hpp"
#include "OpenGL/Rendering/Texture.hpp"
#include "Types/Vec.hpp"
#include "Algorithms/Standard/Util.hpp"
#include "OpenGL/Rendering/RenderParams.hpp"

#include "OpenGL/Widgets/Viewport/BoxShadowFrame.hpp"
#include "OpenGL/Rendering/Cameras/TrackBallCamera.hpp"

#include <QGLFramebufferObjectFormat>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFramebufferObject>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <QMatrix4x4>
#include <QVector4D>

#include <vector>
#include <memory>

namespace TN
{

class TimeLineWidget;
class SliderWidget;
class TexturedPressButton;
class PressButton;
class ComboWidget;
class ResizeWidget;
class ViewPort;
class ControlPanel;

class Renderer
{
    const std::string RELATIVE_PATH_PREFIX = "../";

    std::unique_ptr<QOpenGLBuffer> m_vbo;
    std::unique_ptr<QOpenGLVertexArrayObject> m_vao;
    std::unique_ptr<QOpenGLBuffer> m_ibo;

    struct Character
    {
        GLuint        TextureID;
        Vec2< float > Size;
        Vec2< float > Bearing;
        GLuint        Advance;
    };

    GLuint quadTexId;

    std::map<GLchar, Character> Characters;
    std::map<GLchar, Character> LargerCharacters;
    std::map<GLchar, Character> VeryLargerCharacters;

    BoxShadowFrame m_boxShadow;

public:

    void init();

    void renderLoadingScreen(
        const std::unique_ptr<QOpenGLShaderProgram> & textProgram,
        const std::unique_ptr<QOpenGLShaderProgram> & bkgProgram,
        const std::unique_ptr<QOpenGLShaderProgram> & outlineProg,
        int screenWidth,
        int screenHeight,
        const std::string & mssg,
        int percentDone );

    void computePixelDensityByAngle( const std::unique_ptr<QOpenGLShaderProgram> & program, std::vector< float > & result );

    void renderText(
        bool enlarge,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        int W_WIDTH,
        int W_HEIGHT,
        const std::string & text,
        GLfloat x,
        GLfloat y,
        GLfloat scale,
        const Vec3< float > & color, bool vertical = false );

    void renderClusterLayer(
        Texture2D1 & tex,
        Texture1D3  & tf,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        const QMatrix4x4 & TR );

    void renderGrid(
        const Vec2< float > & size,
        const Vec2< float > & position,
        int gridSpacing,
        const QVector4D & gridColor,
        const QVector4D & backgroundColor,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        const std::unique_ptr<QOpenGLShaderProgram> & program2 );

    void renderGrid(
        int screenWidth,
        int screenHeight,
        const Vec2< float > & mins,
        const Vec2< float > & maxes,
        int ROWS,
        int COLS,
        const QMatrix4x4 & M,
        const Vec2< float > & gc1,
        const Vec2< float > & gc2,
        const QVector4D & color,
        const QVector4D & color2,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        const std::unique_ptr<QOpenGLShaderProgram> & program2,
        bool cellSelected );

    void renderColorLegend(
        const Vec2< float > & pos,
        const Vec2< float > & size,
        Texture1D3 & tf,
        const std::unique_ptr<QOpenGLShaderProgram> & program );

    void renderHeatMapTexture(
        Texture2D1 & tex,
        Texture1D3 & tf,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        float mn,
        float mx,
        bool fade );

    void renderTriangulationMap(
        const std::vector< float > & x,
        const std::vector< float > & y,
        const std::vector< float > & values,
        const std::vector< unsigned int > & indices,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        Texture1D3 & tf,
        float mn,
        float mx,
        float scale,
        bool signOnly,
        bool zeroCentered,
        const QMatrix4x4 & TR,
        unsigned int mode );

    void renderHeatMapTexture(
        Texture2D1 & tex,
        Texture1D3 & tf,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        float mx,
        bool fade,
        float width,
        float height,
        bool outline,
        bool onlyOutline );

    template< typename T >
    void renderLinesAsHeatMap(
        const std::vector< T > & x,
        const std::vector< T > & y,
        const std::vector< T > & weight,
        QMatrix4x4 & TR,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        unsigned mode
    )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear( GL_COLOR_BUFFER_BIT );

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 3 * sizeof( T )  )
        {
            m_vbo->allocate( x.size() * 3 * sizeof( T )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T ), y.data(), y.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T )*2, weight.data(), weight.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        program->bind();

        GLuint loc = program->uniformLocation( "MVP" );
        program->setUniformValue( loc, TR );

        GLuint xAttr = program->attributeLocation( "xAttr" );
        GLuint yAttr = program->attributeLocation( "yAttr" );
        GLuint wAttr = program->attributeLocation( "wAttr" );

        m_vao->bind();
        m_vbo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );
        program->enableAttributeArray( wAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size()*sizeof( T ), 1 );
        program->setAttributeBuffer( wAttr, GL_FLOAT, x.size()*sizeof( T )*2, 1 );

        glDrawArrays( mode, 0, x.size() );

        program->disableAttributeArray( xAttr );
        program->disableAttributeArray( yAttr );
        program->disableAttributeArray( wAttr );

        m_vbo->release();
        m_vao->release();

        program->release();
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    }

    template< typename T >
    void renderPointsAsHeatMap(
        const std::vector< T > & x,
        const std::vector< T > & y,
        const std::vector< unsigned int > & indices,
        QMatrix4x4 & TR,
        const std::unique_ptr<QOpenGLShaderProgram> & program
    )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear( GL_COLOR_BUFFER_BIT );

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 2 * sizeof( T )  )
        {
            m_vbo->allocate( x.size() * 2 * sizeof( T )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T ), y.data(), y.size() * sizeof( T ) );

        m_ibo->bind();
        if( static_cast< size_t >( m_ibo->size() ) < indices.size() * sizeof( indices[0] ) )
        {
            m_ibo->allocate( indices.size() * sizeof( indices[0] ) );
        }
        m_ibo->write( 0, indices.data(), indices.size() * sizeof( indices[0] ) );

        //

        program->bind();

        GLuint xAttr = program->attributeLocation( "xAttr" );
        GLuint yAttr = program->attributeLocation( "yAttr" );

        GLuint loc = program->uniformLocation( "MVP" );
        program->setUniformValue( loc, TR );

        m_vao->bind();
        m_vbo->bind();
        m_ibo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, sizeof(T) / sizeof(GL_FLOAT) );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size() * sizeof( T ), sizeof(T) / sizeof(GL_FLOAT) );

        glDrawElements( GL_POINTS, indices.size(), GL_UNSIGNED_INT, 0 );

        m_ibo->release();
        m_vbo->release();
        m_vao->release();

        program->release();
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    }

    template< typename T >
    void renderPointsAsHeatMap(
        const std::vector< T > & x,
        const std::vector< T > & y,
        QMatrix4x4 & TR,
        const std::unique_ptr<QOpenGLShaderProgram> & program
    )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear( GL_COLOR_BUFFER_BIT );

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 2 * sizeof( T )  )
        {
            m_vbo->allocate( x.size() * 2 * sizeof( T )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T ), y.data(), y.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        program->bind();

        GLuint loc = program->uniformLocation( "MVP" );
        program->setUniformValue( loc, TR );

        GLuint xAttr = program->attributeLocation( "xAttr" );
        GLuint yAttr = program->attributeLocation( "yAttr" );

        m_vao->bind();
        m_vbo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size()*sizeof( T ), 1 );

        glDrawArrays( GL_POINTS, 0, x.size() );

        program->disableAttributeArray( xAttr );
        program->disableAttributeArray( yAttr );

        m_vbo->release();
        m_vao->release();

        program->release();
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    }

    template< typename T >
    void renderHeatMapToFrameBuffer(
        const std::vector< T > & x,
        const std::vector< T > & y,
        const std::vector< T > & weight,
        QMatrix4x4 & TR,
        int xOffset,
        int yOffset,
        int w,
        int h,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        unsigned mode,
        std::vector< float > & result
    )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 3 * sizeof( T )  )
        {
            m_vbo->allocate( x.size() * 3 * sizeof( T )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T ), y.data(), y.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T )*2, weight.data(), weight.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        program->bind();

        GLuint loc = program->uniformLocation( "MVP" );
        program->setUniformValue( loc, TR );

        GLuint xAttr = program->attributeLocation( "xAttr" );
        GLuint yAttr = program->attributeLocation( "yAttr" );
        GLuint wAttr = program->attributeLocation( "wAttr" );

        m_vao->bind();
        m_vbo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );
        program->enableAttributeArray( wAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size()*sizeof( T ), 1 );
        program->setAttributeBuffer( wAttr, GL_FLOAT, x.size()*sizeof( T )*2, 1 );

        glDrawArrays( mode, 0, x.size() );

        program->disableAttributeArray( xAttr );
        program->disableAttributeArray( yAttr );
        program->disableAttributeArray( wAttr );

        m_vbo->release();
        m_vao->release();

        program->release();

        checkError( "after render to fbo " );

        result.resize( w*h );
        glReadPixels( xOffset, yOffset, w, h, GL_RED, GL_FLOAT, result.data() );

        checkError( "after read pixels " );

        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

        checkError( "after gen heat map" );
    }

    template< typename T >
    void renderPointHeatMapToFrameBuffer(
        const std::vector< T > & x,
        const std::vector< T > & y,
        QMatrix4x4 & TR,
        int xOffset,
        int yOffset,
        int w,
        int h,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        unsigned mode,
        std::vector< float > & result
    )
    {
        glEnable( GL_BLEND );
        glBlendFunc(GL_ONE, GL_ONE);

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 2 * sizeof( T )  )
        {
            m_vbo->allocate( x.size() * 2 * sizeof( T )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size() *  sizeof( T ), y.data(), y.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        program->bind();

        GLuint loc = program->uniformLocation( "MVP" );
        program->setUniformValue( loc, TR );

        GLuint xAttr = program->attributeLocation( "xAttr" );
        GLuint yAttr = program->attributeLocation( "yAttr" );

        m_vao->bind();
        m_vbo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size()*sizeof( T ), 1 );

        glDrawArrays( mode, 0, x.size() );

        program->disableAttributeArray( xAttr );
        program->disableAttributeArray( yAttr );

        m_vbo->release();
        m_vao->release();

        program->release();

        checkError( "after render to fbo " );

        result.resize( w*h );
        glReadPixels( xOffset, yOffset, w, h, GL_RED, GL_FLOAT, result.data() );

        checkError( "after read pixels " );

        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

        checkError( "after gen heat map" );
    }


    void renderGrid(
        int screenWidth,
        int screenHeight,
        const Vec2< float > & mins,
        const Vec2< float > & maxes,
        int ROWS,
        int COLS,
        int TIC_INTERVAL,
        const QMatrix4x4 & M,
        const Vec2< float > & gc1,
        const Vec2< float > & gc2,
        const QVector4D & color,
        const QVector4D & color2,
        const std::unique_ptr<QOpenGLShaderProgram> & program,
        const std::unique_ptr<QOpenGLShaderProgram> & program2,
        bool cellSelected );

    void renderTexturedQuad(
        const std::vector< float > & data,
        int COLS,
        int ROWS,
        const std::unique_ptr<QOpenGLShaderProgram> & program );

    template< typename Color_Type, typename Verts_Type >
    void renderPrimitivesColored(
        const std::vector<Verts_Type> & verts,
        const std::vector<Color_Type> & colors,
        float opacity,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        const QMatrix4x4 & MVP,
        unsigned int type )
    {
        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < verts.size() * sizeof( Verts_Type ) + colors.size() * sizeof( Color_Type ) )
        {
            m_vbo->allocate( verts.size() * sizeof( Verts_Type ) + colors.size() * sizeof( Color_Type ) );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( Verts_Type ) );
        m_vbo->write( verts.size() * sizeof( Verts_Type ), colors.data(), colors.size() * sizeof( Color_Type ) );

        m_vbo->release();
        m_vao->release();

        //

        prog->bind();

        GLuint posAttr = prog->attributeLocation( "posAttr" );
        GLuint colAttr = prog->attributeLocation( "colAttr" );

        GLuint loc = prog->uniformLocation( "MVP" );
        prog->setUniformValue( loc, MVP );

        loc = prog->uniformLocation( "useOpacity" );
        prog->setUniformValue( loc, true );

        loc = prog->uniformLocation( "opacity" );
        prog->setUniformValue( loc, opacity );

        m_vao->bind();
        m_vbo->bind();

        prog->enableAttributeArray( posAttr );
        prog->enableAttributeArray( colAttr );

        prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, sizeof( Verts_Type ) / sizeof( GL_FLOAT ) );
        prog->setAttributeBuffer( colAttr, GL_FLOAT, verts.size() * sizeof( Verts_Type ), sizeof( Color_Type ) / sizeof( GL_FLOAT ) );

        glDrawArrays( type, 0, verts.size() );

        prog->disableAttributeArray( posAttr );
        prog->disableAttributeArray( colAttr );

        m_vbo->release();
        m_vao->release();

        prog->release();
    }

    template< typename Color_Type, typename Verts_Type >
    void renderPrimitivesColored(
        const std::vector<Verts_Type> & verts,
        const std::vector<Color_Type> & colors,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        const QMatrix4x4 & MVP,
        unsigned int type )
    {
        renderPrimitivesColored( verts, colors, 1.f,  prog, MVP, type );
    }

    template<typename T>
    void renderPrimitivesFlat(
        const std::vector<T> & verts,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        const QVector4D & color,
        const QMatrix4x4 & MVP,
        unsigned int type )
    {
        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < verts.size() * sizeof( T ) )
        {
            m_vbo->allocate( verts.size() * sizeof( T ) );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        //

        prog->bind();

        GLuint posAttr = prog->attributeLocation( "posAttr" );

        GLuint loc = prog->uniformLocation( "color" );
        prog->setUniformValue( loc, color );

        loc = prog->uniformLocation( "MVP" );
        prog->setUniformValue( loc, MVP );

        m_vao->bind();
        m_vbo->bind();

        prog->enableAttributeArray( posAttr );
        prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, sizeof(T) / sizeof(GL_FLOAT) );

        glDrawArrays( type, 0, verts.size() );

        prog->disableAttributeArray( posAttr );

        m_vbo->release();
        m_vao->release();

        prog->release();

        checkError("after renderPrimitivesFlat");
    }

    void renderTriangleMesh(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const std::vector< I3 >   & indices,
        const std::vector< Vec3< float > > & colors,
        const Material & _material,
        const Light    & _light,
        const QMatrix4x4 & M,
        const QMatrix4x4 & V,
        const QMatrix4x4 & P,
        const std::unique_ptr< QOpenGLShaderProgram > & prog,
        bool useOpacity );

    void renderTriangleMesh(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const std::vector< Vec3< float > > & colors,
        const Material & _material,
        const Light    & _light,
        const QMatrix4x4 & M,
        const QMatrix4x4 & V,
        const QMatrix4x4 & P,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        bool  useOpacity );

    void renderTriangleMesh(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const QVector4D & color,
        const QMatrix4x4 & M,
        const QMatrix4x4 & V,
        const QMatrix4x4 & P,
        const std::unique_ptr<QOpenGLShaderProgram> & prog );

    void renderPathTubes( const std::vector< Vec3< float > > & verts,
                          const std::vector< unsigned int > & indices,
                          const std::vector< int > & breaks,
                          const Material & material,
                          const Light & light,
                          const TrackBallCam & cam,
                          float radius,
                          const std::unique_ptr< QOpenGLShaderProgram> & prog );

    void renderColorMapped(
        const std::vector<Vec3< float >> & _verts,
        const std::vector<Vec3< float >> & _colors,
        float mx,
        const QMatrix4x4 & transform,
        unsigned int mode,
        float opacity,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        bool fade );

    void boxShadow(
        float height,
        float width,
        const std::unique_ptr<QOpenGLShaderProgram> & prog );

    void clear(
        const QVector4D & color,
        const std::unique_ptr<QOpenGLShaderProgram> & program );

    void renderViewPortBoxShadow( const ViewPort & viewPort, const std::unique_ptr<QOpenGLShaderProgram> & program );

    void clearViewPortInverse( int height, int width, const ViewPort & vp, const QVector4D & cl, const std::unique_ptr<QOpenGLShaderProgram> & program );

    void clearViewPort( const ViewPort & vp, const std::unique_ptr<QOpenGLShaderProgram> & program );

    void renderPopSquare( const std::unique_ptr<QOpenGLShaderProgram> & program, double lineWidth );

    void renderPopSquare( const std::unique_ptr<QOpenGLShaderProgram> & program );

    template<typename T>
    void renderPrimitivesFlatSerial(
        const std::vector<T> & x,
        const std::vector<T> & y,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        const QVector4D & color,
        const QMatrix4x4 & MVP,
        unsigned int type )
    {
        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < ( x.size()+y.size() ) * sizeof( T ) )
        {
            m_vbo->allocate( ( x.size() + y.size() ) * sizeof( T ) );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size()*sizeof(T), y.data(), y.size() * sizeof( T ) );

        m_vbo->release();
        m_vao->release();

        //

        prog->bind();

        GLuint xAttr = prog->attributeLocation( "xAttr" );
        GLuint yAttr = prog->attributeLocation( "yAttr" );

        GLuint loc = prog->uniformLocation( "color" );
        prog->setUniformValue( loc, color );

        loc = prog->uniformLocation( "MVP" );
        prog->setUniformValue( loc, MVP );

        m_vao->bind();
        m_vbo->bind();

        prog->enableAttributeArray( xAttr );
        prog->enableAttributeArray( yAttr );

        prog->setAttributeBuffer( xAttr, GL_FLOAT, 0, sizeof(T) / sizeof(GL_FLOAT) );
        prog->setAttributeBuffer( yAttr, GL_FLOAT, x.size() * sizeof( T ), sizeof(T) / sizeof(GL_FLOAT) );

        glDrawArrays( type, 0, x.size() );

        prog->disableAttributeArray( xAttr );
        prog->disableAttributeArray( yAttr );

        m_vbo->release();
        m_vao->release();

        prog->release();
        checkError("after renderPrimitivesFlatSerial");
    }

    template<typename T>
    void renderPointsIndexed(
        const std::vector<T> & x,
        const std::vector<T> & y,
        const std::vector< unsigned int > & indices,
        const std::unique_ptr<QOpenGLShaderProgram> & prog,
        const QVector4D & color,
        const QMatrix4x4 & MVP )
    {
        m_vao->bind();

        m_vbo->bind();
        if ( static_cast< size_t >( m_vbo->size() ) < ( x.size()+y.size() ) * sizeof( T ) )
        {
            m_vbo->allocate( ( x.size() + y.size() ) * sizeof( T ) );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( T ) );
        m_vbo->write( x.size()*sizeof(T), y.data(), y.size() * sizeof( T ) );
        m_vbo->release();

        m_ibo->bind();
        if( static_cast< size_t >( m_ibo->size() ) < indices.size() * sizeof( indices[0] ) )
        {
            m_ibo->allocate( indices.size() * sizeof( indices[0] ) );
        }
        m_ibo->write( 0, indices.data(), indices.size() * sizeof( indices[0] ) );
        m_ibo->release();

        m_vao->release();

        //

        prog->bind();

        GLuint xAttr = prog->attributeLocation( "xAttr" );
        GLuint yAttr = prog->attributeLocation( "yAttr" );

        GLuint loc = prog->uniformLocation( "color" );
        prog->setUniformValue( loc, color );

        loc = prog->uniformLocation( "MVP" );
        prog->setUniformValue( loc, MVP );

        m_vao->bind();
        m_vbo->bind();
        m_ibo->bind();

        prog->enableAttributeArray( xAttr );
        prog->enableAttributeArray( yAttr );

        prog->setAttributeBuffer( xAttr, GL_FLOAT, 0, sizeof(T) / sizeof(GL_FLOAT) );
        prog->setAttributeBuffer( yAttr, GL_FLOAT, x.size() * sizeof( T ), sizeof(T) / sizeof(GL_FLOAT) );

        glDrawElements( GL_POINTS, indices.size(), GL_UNSIGNED_INT, 0 );

        prog->disableAttributeArray( xAttr );
        prog->disableAttributeArray( yAttr );

        m_ibo->release();
        m_vbo->release();
        m_vao->release();

        prog->release();
        checkError("after renderPrimitivesFlatSerial");
    }

    void renderTimeLinePlot(
        TimeLineWidget & timeLine,
        std::unique_ptr< QOpenGLShaderProgram > & shader1,
        std::unique_ptr< QOpenGLShaderProgram > & shader2,
        const QVector4D & color );

    void renderSlider( 
        SliderWidget & slider,        
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2 );

    void renderSlider( 
        SliderWidget & slider,        
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2,
        std::unique_ptr< QOpenGLShaderProgram > & textProg,
        const Vec3< float > & textColor,
        double windowWidth,
        double windowHeight );

    void renderComboItems( 
        const ComboWidget & combo,
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2,
        std::unique_ptr< QOpenGLShaderProgram > & textProg,
        const Vec3< float > & textColor,
        double windowWidth,
        double windowHeight );

    void renderComboButton( 
        ComboWidget & combo, 
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2 );

    void renderCombo( 
        ComboWidget & combo, 
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2,
        std::unique_ptr< QOpenGLShaderProgram > & textProg,
        const Vec3< float > & textColor,
        double windowWidth,
        double windowHeight );

    void renderResizeWidget( const ResizeWidget & w, std::unique_ptr< QOpenGLShaderProgram > & shader1 );

    void renderPressButton( 
        PressButton & button,
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2 );

    void renderPressButton( 
        PressButton & button,
        std::unique_ptr< QOpenGLShaderProgram > & shader1, 
        std::unique_ptr< QOpenGLShaderProgram > & shader2,
        std::unique_ptr< QOpenGLShaderProgram > & textProg,
        const Vec3< float > & textColor,
        double windowWidth,
        double windowHeight );

    void renderTexturedPressButton( TexturedPressButton & button, std::unique_ptr< QOpenGLShaderProgram > & texShader, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 );

    void renderLinkButton( PressButton & button, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 );

    void renderAORenderObject( AORenderScene & obj );

void renderControlPanel(
    ControlPanel & panel, 
    std::unique_ptr< QOpenGLShaderProgram > & shader1, 
    std::unique_ptr< QOpenGLShaderProgram > & shader2,
    std::unique_ptr< QOpenGLShaderProgram > & textProg,
    const Vec3< float > & textColor,
    double windowWidth,
    double windowHeight,
    bool renderCombos = true );
};

}

#endif // RENDERER_HPP

