#version 400



//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-11-06
//UPDATED: 2015-11-06


layout(location = 0) in vec3 posAttr;
layout(location = 1) in int breakAttr;

out V2G
{
    int breakHere;
} v2g;


void main(void)
{
    v2g.breakHere = breakAttr;
    gl_Position = vec4( posAttr.x, posAttr.y, posAttr.z, 1.0);
    //gl_Position = vec4(1,1,1,1.0);
}
