#version 130



in vec2 posAttr;
in vec3 colAttr;
//in int gl_VertexID;

out vec3 col;

uniform mat4 MVP;
uniform vec4 color;

void main(void)
{
    col = colAttr;

    gl_Position = MVP * vec4(
                      posAttr.x,
                      posAttr.y,
                      -( float( gl_VertexID ) + color.x*0.001 ) / 4294967296.0,
                      1.0
                  );
}
