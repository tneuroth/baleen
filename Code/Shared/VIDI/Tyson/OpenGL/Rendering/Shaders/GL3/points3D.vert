#version 130



in vec3 posAttr;
in vec4 colAttr;

out vec4 color;

uniform mat4 MVP;

void main(void)
{
    gl_Position = MVP * vec4(posAttr,1);
    color = colAttr;
}
