#version 130



in float sX;
in float sY;

in float vX;
in float vY;
in float w;

uniform float vxMin;
uniform float vyMin;

uniform float vxWidth;
uniform float vyWidth;

uniform int histRows;
uniform int histCols;

uniform vec2 xRangeSpatial;
uniform vec2 yRangeSpatial;

uniform float xScaleSpatial;
uniform float yScaleSpatial;

uniform int frameBufferWidth;
uniform int frameBufferHeight;

uniform int frameBufferCols;

uniform int numParticles;

out float wf;

void main(void)
{
    /* If the particle is outside the sampling region
    Then effectively discard it by mapping it outside (-1,1) */
    if( sX * xScaleSpatial < xRangeSpatial.x || sX * xScaleSpatial > xRangeSpatial.y
            || sY * yScaleSpatial < yRangeSpatial.x || sY * yScaleSpatial > yRangeSpatial.y )
    {
        wf = 0;
        gl_Position = vec4( 2.0, 2.0, 0, 1.0 );
        return;
    }

    /* the vertices are consecutive in memory by time step,
      assuming their number is constant in time, dividing the vertext id
      by the number of particles gives us the time step */
    int step = gl_VertexID / numParticles;

    /* Now go from a flat time step id to a 2D according to the predefined layout*/
    int fRow = step / frameBufferCols;
    int fCol = step % frameBufferCols;

    /* Now calculate the 2D histogram bin index */
    int hRow = min( int( floor( ( float( vY - vyMin ) / vyWidth ) * histRows ) ), histRows - 1 );
    int hCol = min( int( floor( ( float( vX - vxMin ) / vxWidth ) * histCols ) ), histCols - 1 );

    /* now compute the corresponding global indices in the 2D texture */
    int tx = fCol * histCols + hCol;
    int ty = fRow * histRows + hRow;

    /* finally map the the position of the center of the computed texel index in nd coordinates (-1,1) */
    gl_Position = vec4(
                      -1.0 + ( 2.0 * tx + 1.0 ) / float( frameBufferWidth ),
                      -1.0 - ( 2.0 * ty + 1.0 ) / float( frameBufferHeight ),
                      0.0,
                      1.0 );

    wf = 1.0;
}

