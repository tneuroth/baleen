#version 130



uniform float opacity;

in  vec4 col;
out vec4 fragColor;

void main(void)
{
    fragColor = vec4( col.xyz, col.w * opacity );
}
