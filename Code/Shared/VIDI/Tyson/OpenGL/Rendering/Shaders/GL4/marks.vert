#version 130



in float xAttr;
in float yAttr;
in vec3 cmAttr;

uniform int cm;

uniform mat4 MVP;

void main(void)
{
    if( int( cmAttr.x ) != cm )
    {
        gl_Position = vec4( 99.0, -99.0, 0, 1 );
    }
    else
    {
        gl_Position = MVP*vec4( xAttr, yAttr, 0, 1.0 );
    }
}
