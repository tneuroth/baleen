#version 130



uniform int mark;
uniform vec4 color;

out vec4 fragColor;

void main()
{
    fragColor = color;
}
