#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;

void main()
{
    float v = texture( tex, uv.st ).r;
    color = vec4( ( 1.0 - v )*( 1.0 - v )*( 1.0 - v ), 1.0 - v,  ( 1.0 - v )*( 1.0 - v ), sqrt( sqrt( v ) ) );
}
