//============================================================================
//  Copyright (c) 2016 University of California Davis
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information
//===========================================================================

#ifndef RENDERPARAMS
#define RENDERPARAMS

#include <QVector3D>

struct Light
{

    enum
    {
        SUN,
        POINT,
        SPOT
    };

    bool enabled;
    int type;

    QVector3D direction;
    QVector3D Ia;
    QVector3D Id;
    QVector3D Is;

    float Ia_scale;
    float Id_scale;
    float Is_scale;

    QVector3D position;

    float at1;
    float at2;
    float cutoff;
    float explonent;
};

struct Material
{
    QVector3D Ka;
    QVector3D Kd;
    QVector3D Ks;
    bool overrideAmbient;
    float Ka_scale;
    float Kd_scale;
    float Ks_scale;
    float shininess;
    float opacity;
};

#endif // RENDERPARAMS

