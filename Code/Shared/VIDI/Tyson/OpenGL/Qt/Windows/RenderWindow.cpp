

#include "OpenGL/Qt/Windows/RenderWindow.hpp"

#include <QOpenGLDebugLogger>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QScreen>

#include <QFileInfo>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QApplication>
#include <QVector4D>

#include <iostream>

RenderWindow::RenderWindow()
{

}

RenderWindow::~RenderWindow()
{

}

void
RenderWindow::render()
{
    glClearColor( 1.f, 1.f, 1.f, 1.f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

// public slots

// Qt event handlers

void RenderWindow::resizeEvent(QResizeEvent *e)
{
}

void RenderWindow::mousePressEvent(QMouseEvent *e)
{

}

void RenderWindow::mouseDoubleClickEvent(QMouseEvent *e)
{

}

void RenderWindow::mouseReleaseEvent(QMouseEvent *e)
{
}

void RenderWindow::keyPressEvent(QKeyEvent *e)
{
}

void RenderWindow::keyReleaseEvent(QKeyEvent *e)
{

}

void RenderWindow::mouseMoveEvent(QMouseEvent *e)
{

}

void RenderWindow::wheelEvent(QWheelEvent *e)
{

}

//////////////////////////////////////////////////////////////////////

