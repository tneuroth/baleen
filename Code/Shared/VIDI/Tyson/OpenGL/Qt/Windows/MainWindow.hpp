

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "OpenGL/Qt/Windows/RenderWindow.hpp"
#include "BALEEN.hpp"

#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class BALEEN;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected :

    virtual void resizeEvent(QResizeEvent *e);

    Ui::MainWindow *ui;
    TN::BALEENWidget baleen;
};

#endif // MAINWINDOW_H
