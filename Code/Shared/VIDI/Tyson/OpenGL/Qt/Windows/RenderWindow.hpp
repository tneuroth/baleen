

#ifndef RENDERWINDOW_H
#define RENDERWINDOW_H

#include "OpenGL/Qt/Windows/OpenGLWindow.hpp"

#include <QWidget>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QPair>
#include <QTime>
#include <QList>

#include <vector>
#include <memory>

class QOpenGLShaderProgram;
class QOpenGLBuffer;
class QOpenGLVertexArrayObject;
class QKeyEvent;
class QMouseEvent;
class Scene;

class RenderWindow : public OpenGLWindow
{
    Q_OBJECT
public:

    RenderWindow();
    ~RenderWindow();

    virtual void render();

protected:

    virtual void resizeEvent(QResizeEvent *e);
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void keyPressEvent( QKeyEvent *e );
    virtual void keyReleaseEvent( QKeyEvent *e );
    virtual void wheelEvent(QWheelEvent *e);
    virtual void mouseDoubleClickEvent( QMouseEvent *e );

private:

    QOpenGLShaderProgram *m_program;
    QOpenGLShaderProgram *m_2DShader;
};

class RenderWindowWidget : public QWidget
{
public:

    RenderWindowWidget(QWidget* parent): QWidget(parent)
    {

        renWin  = new RenderWindow();
    }

    ~RenderWindowWidget() {}

    virtual RenderWindow* GetRenderWindow()
    {
        return renWin;
    }

private:

    RenderWindow* renWin;
};

#endif // RENDERWINDOW_H
