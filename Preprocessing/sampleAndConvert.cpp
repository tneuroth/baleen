#include "hdf5.h"

#include <boost/filesystem.hpp>

#include <map>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <iostream>

const std::map< std::string, int > attrKeyToPhaseIndex =
{
    { "r",            0 },   // Major radius [m]
    { "z",            1 },   // Azimuthal direction [m]
    { "zeta",         2 },   // Toroidal angle
    { "rho_parallel", 3 },   // Parallel Larmor radius [m]
    { "w1",           4 },   // Grid weight 1
    { "w2",           5 },   // Grid weight 2
    { "mu",           6 },   // Magnetic moment
    { "w0",           7 },   // Grid weight
    { "f0",           8 }
}; // Grid distribution function

inline void readStep(
    std::vector< float > & result,
    std::vector< float > & swap,
    const std::string & FILE_PATH,
    const std::string & ptype,
    const std::string & attr,
    const std::map< std::string, std::unordered_map< std::size_t, std::size_t > > & idMaps,
    size_t ts )
{
    std::cout << "reading step: " << FILE_PATH << "\n";
    hid_t file_id = H5Fopen( FILE_PATH.c_str() , H5F_ACC_RDONLY, H5P_DEFAULT );
    hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );
    hid_t dataset_id = H5Dopen2( group, ptype == "ions" ? "iphase" : "ephase", H5P_DEFAULT );
    hid_t dataspace_id = H5Dget_space ( dataset_id );

    int ndims = H5Sget_simple_extent_ndims( dataspace_id );
    hsize_t dims[ ndims ];
    H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

    hsize_t offset[ 2 ];
    hsize_t  count[ 2 ];
    hsize_t step[ 2 ];
    hsize_t  block[ 2 ];

    offset[ 0 ] = 0;
    offset[ 1 ] = attrKeyToPhaseIndex.at( attr );

    count[ 0 ]  = dims[ 0 ];
    count[ 1 ]  = 1;

    step[ 0 ] = 1;
    step[ 1 ] = 1;

    block[ 0 ] = 1;
    block[ 1 ] = 1;

    herr_t status = H5Sselect_hyperslab( dataspace_id, H5S_SELECT_SET, offset, step, count, block );

    hsize_t dimsm[ 1 ];
    dimsm[ 0 ] = dims[ 0 ];
    hid_t memspace_id = H5Screate_simple( 1, dimsm, NULL );

    swap.resize( dims[ 0 ] );
    status = H5Dread( dataset_id, H5T_IEEE_F32LE, memspace_id, dataspace_id, H5P_DEFAULT, swap.data() );
    status = H5Dclose( dataset_id );

    std::vector< long int > ids( dims[ 0 ] );
    dataset_id = H5Dopen2( group, ptype == "ions" ? "igid" : "egid", H5P_DEFAULT );
    status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );
    status = H5Dclose( dataset_id );

    status = H5Gclose( group );
    status = H5Fclose(file_id);

    result = std::vector< float >( idMaps.at( ptype ).size(), NAN );
    const std::unordered_map< std::size_t, std::size_t > & mp = idMaps.at( ptype );

    for( int i = 0; i < dims[ 0 ]; ++i )
    {
        if( mp.count( ids[ i ] ) )
        {
            result[ mp.at( ids[ i ] ) ] = swap[ i ];
        }
    }
}

inline void convertData(
    const std::string & BASE_PATH,
    const std::map< std::string, std::unordered_map< std::size_t, std::size_t > > & idMaps,
    const std::vector< std::int32_t > & simulationTimeSteps )
{
    std::vector< std::string > ptypes = { "ions", "electrons" };
    std::vector< float > values;

    std::vector< float > temp;
    std::vector< float > swap;

    for( auto & p : ptypes )
    {
        const size_t NP = idMaps.at( p ).size();
        values.resize( NP * attrKeyToPhaseIndex.size() );

        std::cout << "creating info file: " << ( BASE_PATH + "/Preprocessed/ParticleData/info." + p + ".txt\n");
        std::ofstream infoFile( BASE_PATH + "/Preprocessed/ParticleData/info." + p + ".txt" );

        infoFile << "NumParticles NumVariables Variable1 Variable2 ... VarableNV\n";
        infoFile << NP << " " << attrKeyToPhaseIndex.size() << " ";
        for( auto & v : attrKeyToPhaseIndex )
        {
            infoFile << v.first <<  " ";
        }
        infoFile.close();

        for( auto & t : simulationTimeSteps )
        {
            std::string tstr = std::to_string( t );

            for( auto & v : attrKeyToPhaseIndex )
            {
                readStep(
                    temp,
                    swap,
                    BASE_PATH + "/ParticleData/xgc.particle." + std::string( 5 - tstr.size(), '0' ) + tstr + ".h5",
                    p,
                    v.first,
                    idMaps,
                    t );

                const int VOFFSET = v.second;

                #pragma omp parallel for
                for( size_t k = 0; k < NP; ++k )
                {
                    values[ VOFFSET*NP + k ] = temp[ k ];
                }
            }

            std::string path =  BASE_PATH + "/Preprocessed/ParticleData/" + "xgc." + p + "." + std::string( 5 - tstr.size(), '0' ) + tstr  +  ".bin";
            std::cout << "writing file: " <<  (BASE_PATH + "/Preprocessed/ParticleData/" + "xgc." + p + "." + std::string( 5 - tstr.size(), '0' ) + tstr  +  ".bin\n");
            std::ofstream outFile( path, std::ios::out | std::ios::binary );
            outFile.write( (char *) values.data(), values.size() * sizeof( float ) );
            outFile.close();
        }

        std::string path =  BASE_PATH + "/Preprocessed/ParticleData/" + "xgc." + p + ".sample.txt";
        std::cout << "writing file: " << (BASE_PATH + "/Preprocessed/ParticleData/" + "xgc." + p + ".sample.txt\n");
        std::ofstream outFile( path );
        for( auto & idx : idMaps.at( p ) )
        {
            outFile << idx.first << ",";
        }
        outFile.close();
    }
}

inline void prepareAndSample(
    std::vector< std::int32_t > & simulationTimeSteps,
    std::vector< double > & realTime,
    std::map< std::string, std::unordered_map< std::size_t, std::size_t > > & idMaps,
    const std::string & BASE_PATH,
    int first, int last, int step )
{
    using namespace boost;

    const std::string PART_PATH = BASE_PATH + "/ParticleData/";
    std::cout << "looking for particle data in " << PART_PATH << "\n";

    filesystem::path p( PART_PATH );

    simulationTimeSteps.clear();
    realTime.clear();
    idMaps.clear();

    if( ! filesystem::is_directory( p ) )
    {
        std::cout << p << " is not a directory";
    }
    else
    {
        // extract available time steps
        filesystem::directory_iterator end_itr;
        for ( filesystem::directory_iterator itr( p ); itr != end_itr; ++itr )
        {
            if ( is_regular_file( itr->path() ) )
            {
                std::string current_file = itr->path().filename().string();
                std::stringstream sstr( current_file );
                std::string a,b,c;
                std::getline( sstr, a, '.' );
                std::getline( sstr, b, '.' );
                std::getline( sstr, c, '.' );

                if( a == "xgc" && b == "particle" )
                {
                    std::cout << "opening" << ( PART_PATH + current_file ) << "\n";
                    hid_t file_id = H5Fopen( ( PART_PATH + current_file ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);
                    hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );

                    int ts = 0;
                    hid_t dataset_id = H5Dopen2( group, "timestep", H5P_DEFAULT );
                    herr_t status = H5Dread( dataset_id, H5T_STD_I32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, & ts );
                    status = H5Dclose( dataset_id );

                    double rt = 0;
                    dataset_id = H5Dopen2( group, "time", H5P_DEFAULT );
                    status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, & rt );
                    status = H5Dclose( dataset_id );

                    status = H5Gclose( group );
                    status = H5Fclose(file_id);

                    if( ( ts - first ) % step == 0 && ts <= last )
                    {
                        simulationTimeSteps.push_back( ts );
                        realTime.push_back( rt );
                    }
                }
            }
        }

        std::sort( simulationTimeSteps.begin(), simulationTimeSteps.end() );
        std::sort( realTime.begin(), realTime.end() );

        std::string nstr = std::to_string( simulationTimeSteps.front() );
        std::string path = BASE_PATH + "/ParticleData/" + "xgc.particle." + std::string( 5 - nstr.size(), '0' ) + nstr  +  ".h5";
        std::cout << "opening " << ( BASE_PATH + "/ParticleData/" + "xgc.particle." + std::string( 5 - nstr.size(), '0' ) + nstr  +  ".h5\n" );
        hid_t file_id = H5Fopen( path.c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);

        ///////////////////////////////////////////////////////////////////////
        //
        // For sampling, you could modify the below sections to insert  only
        // The desired particle ids into the id map
        //
        ///////////////////////////////////////////////////////////////////////

        // get standard order and number of particles for electrons

        hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );
        hid_t dataset_id = H5Dopen2( group, "egid", H5P_DEFAULT );
        hid_t dataspace_id = H5Dget_space ( dataset_id );
        int ndims = H5Sget_simple_extent_ndims( dataspace_id );
        hsize_t dims[ ndims ];
        H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

        std::vector< long int > ids( dims[ 0 ] );

        herr_t status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );

        status = H5Dclose( dataset_id );
        status = H5Gclose( group );

        idMaps.insert( { "electrons", std::unordered_map< std::size_t, std::size_t >() } );
        std::unordered_map< std::size_t, std::size_t > & emp =  idMaps.at( "electrons" );
        emp.reserve( dims[ 0 ]*3/2 );

        for( int i = 0, k = 0; i < dims[ 0 ]; ++i )
        {
            // could be conditional to filter/sample
            if( 1 )
            {
                emp.insert( { ids[ i ], k++ } );
            }
        }

        ////////////////////////////////////////////////////////////
        // get standard order and number of particles for ions

        group = H5Gopen2( file_id, "/", H5P_DEFAULT );
        dataset_id = H5Dopen2( group, "igid", H5P_DEFAULT );
        dataspace_id = H5Dget_space ( dataset_id );
        ndims = H5Sget_simple_extent_ndims( dataspace_id );
        H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

        ids.resize( dims[ 0 ] );

        status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );
        status = H5Dclose( dataset_id );

        idMaps.insert( { "ions", std::unordered_map< std::size_t, std::size_t >() } );
        std::unordered_map< std::size_t, std::size_t > & imp =  idMaps.at( "ions" );
        imp.reserve( dims[ 0 ]*3/2 );

        for( int i = 0, k = 0; i < dims[ 0 ]; ++i )
        {
            if( 1 )
            {
                imp.insert( { ids[ i ], k++ } );
            }
        }

        /////////////////////////////////////////////////////////////

        status = H5Gclose( group );
        status = H5Fclose(file_id);
    }
}

int main( int argc, char *argv[] )
{
    if( argc < 5 )
    {
        std::cout << "invalid arguments, should be: PATH first_step last_step step_size\n";
    }

    const std::string BASE_PATH( argv[ 1 ] );

    const int FIRST = std::stoi( argv[ 2 ] );
    const int LAST = std::stoi( argv[ 3 ] );
    const int STEP = std::stoi( argv[ 4 ] );

    std::vector< std::int32_t > simulationTimeSteps;
    std::vector< double > realTime;
    std::map< std::string, std::unordered_map< std::size_t, std::size_t > > idMaps;

    prepareAndSample(
        simulationTimeSteps,
        realTime,
        idMaps,
        BASE_PATH,
        FIRST, LAST, STEP );

    convertData(
        BASE_PATH,
        idMaps,
        simulationTimeSteps
    );
}
