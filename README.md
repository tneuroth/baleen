# Baleen

A visualization system for multivariate spatial statistical analysis. Baleen has been designed origionally for visualizing phase space particle distributions from particle-in-cell tokamak plasma simulations, but generalizes to other forms of data. 

## Installation and Use

See the user guide for instructions on how to install and use Baleen.  

## Dependencies

This project depends on QT5, under the [LGPLv3 license](https://www.gnu.org/licenses/lgpl-3.0.en.html).

## Authors and acknowledgment

Baleen was written by Tyson Neuroth. Its design was informed through dicussions with Franz Sauer and Kwan-Liu Ma at UC Davis, and Weixing Wang, Stephane Ethier and C.S. Chang at Princeton Plasma Physics Laboratory.

## License

Cirrina is licensed under a modified MIT license which requires explicit perfmission from the copyright holder for commercial use. See LICENSE.txt for details.

## Citations

When citing Baleen, please use the following paper: 

T. Neuroth, F. Sauer, W. Wang, S. Ethier, C. -S. Chang and K. -L. Ma, [Scalable Visualization of Time-varying Multi-parameter Distributions Using Spatially Organized Histograms](https://ieeexplore.ieee.org/abstract/document/7792155/), in IEEE Transactions on Visualization and Computer Graphics, vol. 23, no. 12, pp. 2599-2612, 1 Dec. 2017, doi: 10.1109/TVCG.2016.2642103.